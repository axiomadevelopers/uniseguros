<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Publicidad extends CI_Controller{

		function __construct(){
			parent::__construct();
			$this->load->database();
			$this->load->library('session');
			$this->load->model('Publicidad_model');
			$this->load->model('Auditoria_model');
			$cms = $_SESSION["cms"];
			if (!$cms["login"]) {
						redirect(base_url());
			}
		}

		public function index(){
			//--- Datos de usuario
			$cms = $_SESSION["cms"];
       		$data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
	    	//--
	        $this->load->view('cpanel/header');
	        $this->load->view('cpanel/dashBoard',$data);
	    	$this->load->view('cpanel/menu',$data);
	        $this->load->view('modulos/publicidad/publicidad');
	        $this->load->view('cpanel/footer');
	        //var_dump($this->session->userdata("login"));//die();

	    }

		public function registrarPublicidad(){
			$datos= json_decode(file_get_contents('php://input'), TRUE);
			$posicionar = array(
              'posicion' => $datos["orden"],
              'tipo' => 'insert',
              'id_idioma' =>  $datos['id_idioma'],
            );
            $this->Publicidad_model->posicionar_modulos($posicionar);

			//print_r($datos);die;
			$data = array(
			  'titulo' => trim(mb_strtoupper($datos['titulo'])),
			  'descripcion' => trim(mb_strtoupper($datos['descripcion'])),
			  'id_idioma' => $datos['id_idioma'],
			  'boton' => trim(mb_strtoupper($datos['boton'])),
			  'url' =>trim(mb_strtoupper($datos['url'])),
			  'id_imagen' => trim(mb_strtoupper($datos['id_imagen'])),
			  'estatus' => '1',
  			  'orden' =>  $datos["orden"]
			);
			//var_dump($data);
			$respuesta = $this->Publicidad_model->guardarpublicidad($data);
			
			if($respuesta==true){
				$mensajes["mensaje"] = "registro_procesado";
				//------------------------------------------------------------
					//--Bloque Auditoria 
					$id = $this->Auditoria_model->consultar_max_id("publicidad");
					$accion = "Registro de publicidad id:".$id.",titulo:".trim($datos['titulo']);
					$cms = $_SESSION["cms"];           
			        $data_auditoria = array(
			                                "id_usuario"=>(integer)$cms["id"],
			                                "modulo"=>'1',
			                                "accion"=>$accion,
			                                "ip"=>$this->Auditoria_model->get_client_ip(),
			                                "fecha_hora"=> date("Y-m-d H:i:00")
					);
			        $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
				//------------------------------------------------------------
			}else{
				$mensajes["mensaje"] = "no_registro";
			}
			die(json_encode($mensajes));
		}
		/*
		*	Modificar Publicidad
		*/
		public function modificarPublicidad(){
	        $datos = json_decode(file_get_contents('php://input'), TRUE);
	        //var_dump($datos);die('');
	        $posicionar = array(
              'inicial' => $datos["inicial"],
              'tipo' => 'update',
              'id_idioma' =>  $datos['id_idioma'],
              'final' => $datos["orden"]
            );
            //var_dump($posicionar);die('');
	        //-Verifico si existe una noticia con ese titulo....
	        $existe = $this->Publicidad_model->consultarExiste($datos["id"]);
	        if($existe>0){
	            $data = array(
	              'titulo' => trim(mb_strtoupper($datos['titulo'])),
				  'descripcion' => trim(mb_strtoupper($datos['descripcion'])),
				  'id_idioma' => $datos['id_idioma'],
				  'boton' => trim(mb_strtoupper($datos['boton'])),
				  'url' =>trim(mb_strtoupper($datos['url'])),
				  'id_imagen' => $datos['id_imagen'],
    			  'orden'=>  $posicionar["final"]
	            );
	            $id = $datos['id'];

	            $this->Publicidad_model->posicionar_modulos($posicionar);

	            $respuesta = $this->Publicidad_model->modificarPublicidad($data,$datos['id']);
	            if($respuesta==true){
					$mensajes["mensaje"] = "modificacion_procesada";
					//----------------------------------------------------
						//--Bloque Auditoria 
						$accion = "Actualizacion de publicidad id: ".$id;
						$cms = $_SESSION["cms"];                  
				        $data_auditoria = array(
				                                "id_usuario"=>(integer)$cms["id"],
				                                "modulo"=>'1',
				                                "accion"=>$accion,
				                                "ip"=>$this->Auditoria_model->get_client_ip(),
				                                "fecha_hora"=> date("Y-m-d H:i:00")
				        );
				        $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
					//-----------------------------------------------------
	            }else{
	                $mensajes["mensaje"] = "no_registro";
	            }
	        }else{
	             $mensajes["mensaje"] = "existe";
	        }
	        //--
	        die(json_encode($mensajes));
	    }
		/*
		*	Consultar publicidad
		*/

	    public function consultarPublicidad(){
	    	//--- Datos de usuario
	    	$cms = $_SESSION["cms"];
        	$data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
	    	//--
	        $this->load->view('cpanel/header');
	       	$this->load->view('cpanel/dashBoard',$data);
	    	$this->load->view('cpanel/menu',$data);
	        $this->load->view('modulos/publicidad/consultar_publicidad');
	        $this->load->view('cpanel/footer');
	        //var_dump("aqui6!");

	    }

	    public function consultarPublicidadTodas(){
	        $res = [];
	        $datos= json_decode(file_get_contents('php://input'), TRUE);
	        $respuesta = $this->Publicidad_model->consultarPublicidad($datos);
	        $a = 1;
	        foreach ($respuesta as $key => $value) {
				$valor = $value;

	            //$valor->descripcion_sin_html = strip_tags($value->descripcion);
	            $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";

	            $res[] = $valor;
	            $a++;
	        }
	        $listado = (object)$res;
	        die(json_encode($listado));
	    }

	    public function modificarPublicidadEstatus(){
	    	$datos= json_decode(file_get_contents('php://input'), TRUE);
	        $data = array(
	          //'id' =>$datos['id'],
	          'estatus' => $datos['estatus'],
	        );
	        //var_dump($datos);die;
	        $respuesta = $this->Publicidad_model->modificarPublicidad($data,$datos["id"]);
	        if($respuesta==true){
				$mensajes["mensaje"] = "modificacion_procesada";
					//----------------------------------------------------
					//--Bloque Auditoria 
					switch ($data["estatus"]) {
						case '0':
							$accion="Inactivar publicidad id: ".$datos['id'];
							break;
						case '1':
							$accion="Activar publicidad id: ".$datos['id'];
							break;
						case '2':
							$accion="Eliminar publicidad id: ".$datos['id'];
							break;
					}
					$cms = $_SESSION["cms"];
			        $data_auditoria = array(
			                                "id_usuario"=>(integer)$cms["id"],
			                                "modulo"=>'1',
			                                "accion"=>$accion,
			                                "ip"=>$this->Auditoria_model->get_client_ip(),
			                                "fecha_hora"=> date("Y-m-d H:i:00")
			        );
			        $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
					//-----------------------------------------------------
	        }else{
	            $mensajes["mensaje"] = "no_modifico";
	        }

	        die(json_encode($mensajes));
		}

		public function publicidadVer(){
			//--- Datos de usuario
	    	$cms = $_SESSION["cms"];
        	$data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
	    	//--
	        $datos["id"] = $this->input->post('id_publicidad');
	        $this->load->view('cpanel/header');
	        $this->load->view('cpanel/dashBoard',$data);
	    	$this->load->view('cpanel/menu',$data);
	        $this->load->view('modulos/publicidad/publicidad',$datos);
	        $this->load->view('cpanel/footer');
	    }

	    public function consultar_orden(){
	    	$datos= json_decode(file_get_contents('php://input'), TRUE);
	        $respuesta = $this->Publicidad_model->consultarOrden($datos);
	        //var_dump($respuesta);die('');
	        if(!$respuesta){
	        	$listado2["orden"] = array( "orden"=>1 );
	        	$listado = (object)$listado2;
	        }else{
	        	$c = 1;
	        	//var_dump($respuesta);die('');
	        	foreach($respuesta as $clave => $valor) {
	        		$respuesta2[] = array( "orden"=>$c );
	        		$c++;
	        	}
	        	if($datos["tipo"]=="1")
	        		$respuesta2[] = array( "orden"=>$c );
	        	//var_dump($c);
	        	$listado  = (object)$respuesta2;

	        }
	        die(json_encode($listado));
	    }
	}
?>
