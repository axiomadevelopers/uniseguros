<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class WebProductos extends CI_Controller
{
    function __construct(){
      parent::__construct();
      $this->load->database();
      $this->load->model('WebInicio_model');
      $this->load->model('WebNosotros_model');
      $this->load->model('WebProductos_model');
      
    }
    /*
    * Index
    */
    public function index(){
      $datos["id_idioma"] = 1;
      //---
      $this->load->view('/web/header');
      $this->load->view('/web/menu');
      $this->load->view('/web/productos',$datos);
      $this->load->view('/web/footer');
    }
    public function consultarProductosTodas(){
      $res = [];
      $datos= json_decode(file_get_contents('php://input'), TRUE);
      $respuesta = $this->WebProductos_model->consultarProductos($datos);
      foreach ($respuesta as $key => $value) {

          $valor = $value;
          $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";

          $res[] = $valor;
      }
      $listado = (object)$res;

      die(json_encode($listado));
   }
    /*
    * Para ver el detalle del producto
    */
    public function verTiposProductos($slug_noticia){
        $datos["idioma"] = 1;
        $datos["slug_producto"] = $slug_noticia;  
       
         //---
        #Verifico si existen los slug
        $existe_slug = $this->WebProductos_model->consultarProductoSlugExiste("",$datos["slug_producto"]);

        if($existe_slug>0){

            $this->load->view('web/header');
            $this->load->view('web/menu');
            $this->load->view('web/tiposProductos',$datos);
            $this->load->view('web/footer');

        }else{

            $this->load->view('/web/header');
            $this->load->view('/web/menu');
            $this->load->view('/web/error404');
            $this->load->view('/web/footer');

        }
        //---
    }
    
    public function consultarTproductosTodas(){
      $res = [];
      $datos= json_decode(file_get_contents('php://input'), TRUE);
      //Buscar id con el slug entrante
      $producto = $this->WebProductos_model->consultarProductoSlug($datos);

      //Buscar tipo de productos asociado al id del producto
      $tipos_productos = $this->WebProductos_model->consultarOtrosTproductos($producto['id']);

      $respuesta = $this->WebProductos_model->consultarTproductos($producto['id']);

      foreach ($respuesta as $key => $value) {
      $descripcion = $this->WebProductos_model->consultarDescripcion($value->id,$value->id_productos);
          $valor = $value;
          $valor->descripcion_sin_html = substr(strip_tags($descripcion['descripcion']),0,129)."...";
          $res[] = $valor;
      }
      $listado['tproductos'] = (object)$res;
      $listado['titulo'] = $producto['titulo'];
      $listado['ruta'] = $producto['ruta'];
      $listado['slug'] = $producto['slug'];
      $listado['otros_productos'] = $tipos_productos;

      die(json_encode($listado));
   }
    /*consultarDetalleTproductos
    * Para ver el detalle del producto
    */
    public function verDetalleProductos($slug_base,$slug){
        $datos["idioma"] = 1;
        $datos["slug"] = $slug;
        $datos["slug_base"] = $slug_base;
        //---
        #Verifico si existen los slug
        $existe_slug = $this->WebProductos_model->consultarProductoSlugExiste($datos["slug"],$datos["slug_base"]);
        if($existe_slug>0){

            $this->load->view('web/header');
            $this->load->view('web/menu');
            $this->load->view('web/detalleProductos',$datos);
            $this->load->view('web/footer');

        }else{

            $this->load->view('/web/header');
            $this->load->view('/web/menu');
            $this->load->view('/web/error404');
            $this->load->view('/web/footer');

        }
        //---
       
    }
    /*consultarDetalleTproductos
    * Para ver los detalle del producto
    */
    public function consultarDetalleTproductos(){
      $res = [];
      $datos= json_decode(file_get_contents('php://input'), TRUE);
      $producto_base = $this->WebProductos_model->consultarProductoSlug($datos);
      $tipo_producto = $this->WebProductos_model->consultarProductoSlugSegundario($datos);
      //$otros_productos = $this->WebProductos_model->consultarOtrosDetallesTproductos($producto_base['id'],$tipo_producto['id']);

      $respuesta = $this->WebProductos_model->consultarDetalleTproductos($producto_base['id'],$tipo_producto['id']);
      foreach ($respuesta as $key => $value) {

          $valor = $value;
          //$valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";

          $res[] = $valor;
      }
      $listado['detalles_productos'] = (object)$res;
      $listado['producto_base'] = $producto_base;
      $listado['tipo_producto'] = $tipo_producto;
      //$listado['otros_productos'] = $otros_productos;
      //var_dump($listado);die;

      die(json_encode($listado));
   }
  /*
    * Para los otros tipos de productos que existen
    */
   public function consultarOtrosTproductos(){
    $res = [];
    $datos= json_decode(file_get_contents('php://input'), TRUE);

    $respuesta = $this->WebProductos_model->consultarOtrosDetallesTproductos($datos['id_producto'],$datos['id_tipo_producto']);


    foreach ($respuesta as $key => $value) {

        $valor = $value;
        $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";

        $res[] = $valor;
    }
    $listado = (object)$res;


    die(json_encode($listado));
 }
 /*
    * Para los servicios existen
    */
 public function consultarServiciosTodas(){
  $res = [];
  $datos= json_decode(file_get_contents('php://input'), TRUE);
  $respuesta = $this->WebProductos_model->consultarServicios($datos);
  foreach ($respuesta as $key => $value) {

      $valor = $value;
      $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";

      $res[] = $valor;
  }
  $listado = (object)$res;

  die(json_encode($listado));
}
    
}    