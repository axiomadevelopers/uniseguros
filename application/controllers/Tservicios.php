<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Tservicios extends CI_Controller
{
    function __construct(){
      parent::__construct();
      $this->load->database();
      $this->load->model('Tservicios_model');
      $this->load->model('Auditoria_model');
      $this->load->library('session');
      
      //var_dump($this->session->userdata("login"));die();
        $cms = $_SESSION["cms"];
        if (!$cms["login"]) {  
              redirect(base_url());
        }
    }

    public function index(){
        //--- Datos de usuario
        $cms = $_SESSION["cms"];
        $data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
        //--
        //--
        $this->load->view('cpanel/header');
        $this->load->view('cpanel/dashBoard',$data);
        $this->load->view('cpanel/menu',$data);
        $this->load->view('modulos/servicios/tservicios');
        $this->load->view('cpanel/footer');
    }
    public function consultarServicios(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        if(!$datos){
            $respuesta["mensaje"] = "error";
        }else{
            $respuesta = $this->Tservicios_model->consultarServicios($datos);
        }
        die(json_encode($respuesta));
	}
    public function registrarServicios(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $posicionar = array(
            'posicion' => $datos["orden"],
            'tipo' => 'insert',
            'id_idioma' =>  $datos['id_idioma'],
            'id_servicios' =>  $datos['id_servicios'],
          );
          $this->Tservicios_model->posicionar_modulos($posicionar);
        $cms = $_SESSION["cms"]; 
        $data = array(
          'id' => "",
          'titulo' => trim(ucfirst(mb_strtolower($datos['titulo']))),
          'descripcion' => trim($datos['descripcion']),
          'id_idioma' => $datos['id_idioma'],
          'id_servicios' => $datos['id_servicios'],
          'estatus' => '1',
          'id_imagen' => $datos['id_imagen'],
          'slug' =>$this->generarSlug($datos["titulo"]),
          'orden' => $datos['orden'],

        );
        $existe = $this->Tservicios_model->consultarExisteTitulo($data['id'],$data['titulo']);
        if(!$existe){
            $respuesta = $this->Tservicios_model->guardarServicio($data);

            if($respuesta==true){
                $mensajes["mensaje"] = "registro_procesado";
                //-----------------------------------------------------
                //Bloque de auditoria:
                $id = $this->Auditoria_model->consultar_max_id("seccion_noticias");
                $accion = "Registro noticia id: ".$id." titulo: ".trim(mb_strtoupper($datos['titulo']));
                $cms = $_SESSION["cms"]; 
                $data_auditoria = array(
                                        "id_usuario"=>(integer)$cms["id"],
                                        "modulo"=>'1',
                                        "accion"=>$accion,
                                        "ip"=>$this->Auditoria_model->get_client_ip(),
                                        "fecha_hora"=> date("Y-m-d H:i:00")
                );
                $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
                //-----------------------------------------------------
            }else{
                $mensajes["mensaje"] = "no_registro";
            }
        }else{
            $mensajes["mensaje"] = "existe";

        }

        die(json_encode($mensajes));
    }

    public function modificarServicios(){
        $datos = json_decode(file_get_contents('php://input'), TRUE);
        //-Verifico si existe una noticia con ese titulo....
        $posicionar = array(
            'inicial' => $datos["inicial"],
            'tipo' => 'update',
            'id_idioma' =>  $datos['id_idioma'],
            'final' => $datos["orden"],
            'id_servicios' =>  $datos['id_servicios'],

        );
        $existe_titulo = $this->Tservicios_model->consultarExisteTitulo($datos["id"],trim(ucfirst(mb_strtolower($datos['titulo']))));
        if($existe_titulo==0){
            $data = array(
                'id' => $datos['id'],
                'titulo' => trim(ucfirst(mb_strtolower($datos['titulo']))),
                'descripcion' => trim($datos['descripcion']),
                'id_servicios' => $datos['id_servicios'],
                'estatus' => '1',
                'id_imagen' => $datos['id_imagen'],
                'slug' =>$this->generarSlug($datos["titulo"]),
                'orden' => $datos['orden'],
            );
            $this->Tservicios_model->posicionar_modulos($posicionar);

            $respuesta = $this->Tservicios_model->modificarServicios($data);
            if($respuesta==true){
                $mensajes["mensaje"] = "registro_procesado";
                //-----------------------------------------------------
                //Bloque de auditoria:
                $accion = "Actualizar noticia id: ".$datos['id']." titulo: ".trim(mb_strtoupper($datos['titulo']));
                $cms = $_SESSION["cms"]; 
                $data_auditoria = array(
                                        "id_usuario"=>(integer)$cms["id"],
                                        "modulo"=>'1',
                                        "accion"=>$accion,
                                        "ip"=>$this->Auditoria_model->get_client_ip(),
                                        "fecha_hora"=> date("Y-m-d H:i:00")
                );
                $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
                //-----------------------------------------------------
            }else{
                $mensajes["mensaje"] = "no_registro";
            }
        }else{
             $mensajes["mensaje"] = "existe";
        }
        //--
        die(json_encode($mensajes));
    }

    public function consultar_tservicios(){
        //--- Datos de usuario
        $cms = $_SESSION["cms"];
        $data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
        //--
        $this->load->view('cpanel/header');
        $this->load->view('cpanel/dashBoard',$data);
        $this->load->view('cpanel/menu',$data);
        $this->load->view('modulos/servicios/consultar_tservicios');
        $this->load->view('cpanel/footer');
    }
    public function consultarServiciosTodas(){
        $res = [];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->Tservicios_model->consultarTservicios($datos);
        foreach ($respuesta as $key => $value) {
            $valor = $value;
            $valor->descripcion_sin_html = strip_tags($value->descripcion);
            $res[] = $valor;
        }
        $listado = (object)$res;
        die(json_encode($listado));
    }
    public function generarSlug($cadena){
        $titulo_min = strtolower($this->normaliza($cadena));
        $slug_noticias = str_replace(" ","-",$titulo_min);
        $slug_noticias = preg_replace("/[^a-zA-Z0-9_-]+/", "", $slug_noticias);
        return $slug_noticias;
    }
    public function normaliza ($cadena){
        $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
        $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
        $originales = utf8_decode($originales);
        $cadena = utf8_decode($cadena);
        //$cadena = utf8_decode($cadena);
        //$cadena = strtr($cadena, utf8_decode($originales), $modificadas);
        $cadena = strtr($cadena, $originales, $modificadas);
        $cadena = strtolower($cadena);
        //return utf8_encode($cadena);
        return $cadena;
    }
    public function modificarServiciosEstatus(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $data = array(
          'id' =>$datos['id'],  
          'estatus' => $datos['estatus'],
          'id_servicios' =>  $datos['id_servicios'],
        );
        $respuesta = $this->Tservicios_model->modificarServicios($data);

        if($respuesta==true){
            $mensajes["mensaje"] = "modificacion_procesada";
            //----------------------------------------------------
            //--Bloque Auditoria 
            switch ($data["estatus"]) {
                case '0':
                    $accion="Inactivar servicios: ".$datos['id'];
                    break;
                case '1':
                    $accion="Activar servicios: ".$datos['id'];
                    break;
                case '2':
                    $accion="Eliminar servicios: ".$datos['id'];
                    break;
            }
            $cms = $_SESSION["cms"]; 
            $data_auditoria = array(
                                    "id_usuario"=>(integer)$cms["id"],
                                    "modulo"=>'1',
                                    "accion"=>$accion,
                                    "ip"=>$this->Auditoria_model->get_client_ip(),
                                    "fecha_hora"=> date("Y-m-d H:i:00")
            );
            $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
            //-----------------------------------------------------
        }else{
            $mensajes["mensaje"] = "no_modifico";
        }  
        die(json_encode($mensajes));
    }
    public function tserviciosVer(){
        //--- Datos de usuario
        $cms = $_SESSION["cms"];
        $data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
        //--
        $datos["id"] = $this->input->post('id_tservicio');
        $this->load->view('cpanel/header');
        $this->load->view('cpanel/dashBoard',$data);
        $this->load->view('cpanel/menu',$data);
        $this->load->view('modulos/servicios/tservicios',$datos);
        $this->load->view('cpanel/footer');
    }
    public function consultar_orden(){
    	
        $datos= json_decode(file_get_contents('php://input'), TRUE);

        $respuesta = $this->Tservicios_model->consultarOrden($datos);
        
        if(!$respuesta){
        	$listado2["orden"] = array( "orden"=>1 );
        	$listado = (object)$listado2;
        }else{
        	$c = 1;
        	foreach($respuesta as $clave => $valor) {
        		$respuesta2[] = array( "orden"=>$c );
        		$c++;
        	}
        	if($datos["tipo"]=="1")
        		$respuesta2[] = array( "orden"=>$c );
        	//var_dump($c);
        	$listado  = (object)$respuesta2;
        }

        die(json_encode($listado));

    }
}    
