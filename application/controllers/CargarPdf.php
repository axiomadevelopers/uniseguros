<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class CargarPdf extends CI_Controller
{
    function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->model('CargaPDF_model');
        $this->load->model('Categorias_model');
        $this->load->model('Auditoria_model');
        $cms = $_SESSION["cms"];
        if (!$cms["login"]) {
                redirect(base_url());
        }

    }

    public function index(){
      //--- Datos de usuario
      $cms = $_SESSION["cms"];
      $data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
      //--
      $this->load->view('cpanel/header');
      $this->load->view('cpanel/dashBoard',$data);
      $this->load->view('cpanel/menu',$data);
      $this->load->view('modulos/carga_pdf/pdf_multimedia');
      $this->load->view('cpanel/footer');
    }

    public function upload(){

        /*-----------------------------------------------*/
        /*
        var_dump($_FILES["file"]);
        die('');*/
        if($this->input->post()){
            //---------------------------------------------
            $file = $_FILES["file"]["name"];
            $filetype = $_FILES["file"]["type"];
            $filesize = $_FILES["file"]["size"];
            $id_categoria = $_POST["categoria"];
            $tipo = explode("/", $filetype);
            $descripcion = $this->input->post("descripcion");
            $data["id_categoria"] = $this->input->post("categoria");
            $titulo = $this->input->post("titulo").".".$tipo[1];
            $rs_categorias = $this->Categorias_model->consultarCategoria($data);

            $categoria = strtolower($rs_categorias[0]->descripcion);

            $ruta ="assets/pdf/archivos/".$categoria."/";
            if(!is_dir("assets/pdf/archivos/".$categoria."/")){
                mkdir($ruta,0777);
            }

            $config['upload_path']          = $ruta;
            //$config['allowed_types']        = 'gif|jpg|png|jpeg|JPEG';
            $config['allowed_types']        = 'pdf';
            $config['max_size']             = 10000;
            $config['max_width']            = 6000;
            $config['max_height']           = 6000;
            $config['file_name'] = $titulo;

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('file')){
                $error = array('error' => $this->upload->display_errors());
                die(json_encode($error));
            }
            else{
                $data_img = array('upload_data' => $this->upload->data());

                $archivo1 = str_replace(" ","_",$titulo);
                $data2 = array(
                  'ruta' => $ruta.$archivo1,
                  'id_categoria' =>  $data["id_categoria"],
                  'estatus' => '1',
                  'titulo' =>trim(ucfirst(mb_strtolower($titulo))),
                  'descripcion' => trim(ucfirst(mb_strtolower($descripcion))),
                );
            $existe = $this->CargaPDF_model->consultarExisteTitulo($data['id_categoria'],trim(ucfirst(mb_strtolower($titulo))));

            if(!$existe){
                $respuesta = $this->CargaPDF_model->guardarPdf($data2);
                if($respuesta==true){
                    $mensajes["mensaje"] = "registro_procesado";
                    //-----------------------------------------------------
                    
                    //-----------------------------------------------------
                }else{
                    $mensajes["mensaje"] = "no_registro";
                }
            }else{
                $mensajes["mensaje"] = "existe";
    
            }
                die(json_encode($mensajes));
            }
        //---------------------------------------------
        }
        /*-----------------------------------------------*/
    }

    public function consultarPDF(){
        //--- Datos de usuario
        $cms = $_SESSION["cms"];
        $data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
        //--
        $this->load->view('cpanel/header');
        $this->load->view('cpanel/dashBoard',$data);
        $this->load->view('cpanel/menu',$data);
        $this->load->view('modulos/carga_pdf/consultar_pdf');
        $this->load->view('cpanel/footer');
    }

    public function consultarPdfTodas(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->CargaPDF_model->consultarPDF($datos);
        $listado = [];
        foreach ($respuesta as $key => $value) {

            $valor = $value;
            
            $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";

            $listado[]=$valor;

        }
        $listado2 = $listado;

        $total = count($listado2);
        die(json_encode($listado2));
    }

   

    public function modificarPDFEstatus(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $data = array(
          'id' =>$datos['id'],  
          'estatus' => $datos['estatus'],
        );
        $respuesta = $this->CargaPDF_model->modificarPDF($data);

        if($respuesta==true){
            $mensajes["mensaje"] = "modificacion_procesada";
            //----------------------------------------------------
           
        }else{
            $mensajes["mensaje"] = "no_modifico";
        }  
       
        die(json_encode($mensajes));
    }
    public function pdfVer(){
        //--- Datos de usuario
        $cms = $_SESSION["cms"];
        $data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
        //--
        $datos["id"] = $this->input->post('id_pdf');

        $this->load->view('cpanel/header');
        $this->load->view('cpanel/dashBoard',$data);
        $this->load->view('cpanel/menu',$data);
        $this->load->view('modulos/carga_pdf/pdf_multimedia',$datos);
        $this->load->view('cpanel/footer');
    }

    public function modificarPdf(){
        $datos = json_decode(file_get_contents('php://input'), TRUE);
        //-Verifico si existe una noticia con ese titulo....
        

        $existe_titulo = $this->CargaPDF_model->consultarExiste($datos["id"],$datos["id_categoria"],$datos["titulo"]);

        if($existe_titulo==0){

            $data2 = array(
                'id' =>  $datos["id"],
                'id_categoria' =>  $datos["id_categoria"],
                'titulo' =>trim(ucfirst(mb_strtolower($datos["titulo"]))),
                'descripcion' => trim(ucfirst(mb_strtolower($datos["descripcion"]))),
              );

            $respuesta = $this->CargaPDF_model->modificarPDF($data2);

            if($respuesta==true){
                $mensajes["mensaje"] = "registro_procesado";
            }else{
                $mensajes["mensaje"] = "no_registro";
            }
        }else{
             $mensajes["mensaje"] = "existe";
        }
        //--
        die(json_encode($mensajes));
    }
    
}
