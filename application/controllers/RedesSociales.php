<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class RedesSociales extends CI_Controller{
    function __construct(){
      parent::__construct();
      $this->load->database();
      $this->load->library('session');
      $this->load->model('RedesSociales_model');
      $this->load->model('Auditoria_model');
      $cms = $_SESSION["cms"];
      if (!$cms["login"]) {   
        redirect(base_url());
      }
}

    public function index(){
        //--- Datos de usuario
        $cms = $_SESSION["cms"];
        $data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
        //--
        $this->load->view('cpanel/header');
        $this->load->view('cpanel/dashBoard',$data);
        $this->load->view('cpanel/menu',$data);
        $this->load->view('modulos/redesSociales/RedesSociales');
        $this->load->view('cpanel/footer');
    }

    public function consultar_redes(){
      $datos= json_decode(file_get_contents('php://input'), TRUE);
      $respuesta = $this->RedesSociales_model->consultar_redes($datos);
      die(json_encode($respuesta));
    }

    public function consultar_redes_url(){
      $datos= json_decode(file_get_contents('php://input'), TRUE);
      $respuesta = $this->RedesSociales_model->consultar_redes_url($datos);
      die(json_encode($respuesta));
    }

    public function registrarRedes(){
      $datos= json_decode(file_get_contents('php://input'), TRUE);
      //var_dump($datos);
      $data = array(
        'url_red' => trim(strtolower($datos['url_red'])),
        'id_tipo_red'=>$datos['id_tipo_red']
      );
      $respuesta = $this->RedesSociales_model->guardarRedes($data);
      if($respuesta==true){
          $mensajes["mensaje"] = "registro_procesado";
          //-----------------------------------------------------
          //Bloque de auditoria:
          $id = $this->Auditoria_model->consultar_max_id("red_social");
          $accion = "Registro redes sociales id: ".$id;
          $cms = $_SESSION["cms"];
          $data_auditoria = array(
                                  "id_usuario"=>(integer)$cms["id"],
                                  "modulo"=>'1',
                                  "accion"=>$accion,
                                  "ip"=>$this->Auditoria_model->get_client_ip(),
                                  "fecha_hora"=> date("Y-m-d H:i:00")
          );
          $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
          //-----------------------------------------------------
      }else{
          $mensajes["mensaje"] = "no_registro";
      }
      die(json_encode($mensajes));
    }

    public function modificarRedes(){
      $datos= json_decode(file_get_contents('php://input'), TRUE);
      //var_dump($datos);
      $data = array(
        'id'=>$datos["id_red"],
        'url_red' => trim(strtolower($datos['url_red'])),
        'id_tipo_red'=>$datos['id_tipo_red']
      );

      $respuesta = $this->RedesSociales_model->modificarRedes($data); 

      if($respuesta==true){
          $mensajes["mensaje"] = "modificacion_procesada";
          //-----------------------------------------------------
          //Bloque de auditoria:
          $id = $this->Auditoria_model->consultar_max_id("red_social");
          $accion = "Actualizacion redes sociales id: ".$id;
          $cms = $_SESSION["cms"];
          $data_auditoria = array(
                                  "id_usuario"=>(integer)$cms["id"],
                                  "modulo"=>'1',
                                  "accion"=>$accion,
                                  "ip"=>$this->Auditoria_model->get_client_ip(),
                                  "fecha_hora"=> date("Y-m-d H:i:00")
          );
          $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
          //-----------------------------------------------------
      }else{
          $mensajes["mensaje"] = "no_modifico";
      }  
       
      die(json_encode($mensajes));
    }
}    