<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class WebServicios extends CI_Controller
{
    function __construct(){
      parent::__construct();
      $this->load->database();
      $this->load->model('WebInicio_model');
      $this->load->model('WebNosotros_model');
      $this->load->model('WebServicios_model');
    }
    /*
    * Index
    */
    public function index(){
      $datos["id_idioma"] = 1;
      //---
      $this->load->view('/web/header');
      $this->load->view('/web/menu');
      $this->load->view('/web/servicios',$datos);
      $this->load->view('/web/footer');
    }
    /*
    * Cargar todos los servicios
    */
    public function consultarServiciosTodas(){
      $res = [];
      $datos= json_decode(file_get_contents('php://input'), TRUE);
      $respuesta = $this->WebServicios_model->consultarServicios($datos);
      foreach ($respuesta as $key => $value) {
    
          $valor = $value;
          $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";
    
          $res[] = $valor;
      }
      $listado = (object)$res;
    
      die(json_encode($listado));
    }
    /*
    * Para ver el detalle del producto
    */
    public function verTiposServicios($slug_noticia){
        $datos["idioma"] = 1;
        $datos["slug_producto"] = $slug_noticia;  
        //---
        #Verifico si existen los slug
        $existe_slug = $this->WebServicios_model->consultarServicioSlugExiste("",$datos["slug_producto"]);
        //var_dump($slug_noticia);die;
        if($existe_slug>0){

          $this->load->view('web/header');
          $this->load->view('web/menu');
          $this->load->view('web/tiposServicios',$datos);
          $this->load->view('web/footer');


        }else{

            $this->load->view('/web/header');
            $this->load->view('/web/menu');
            $this->load->view('/web/error404');
            $this->load->view('/web/footer');

        }
        //---
    }
    /*
    * Para consultar tipos de servicios asociados
    */
    public function consultarTserviciosTodas(){
      $res = [];
      $datos= json_decode(file_get_contents('php://input'), TRUE);
      //Buscar id con el slug entrante
      $servicio = $this->WebServicios_model->consultarServicioSlug($datos);

      $respuesta = $this->WebServicios_model->consultarTservicios($servicio['id']);

      foreach ($respuesta as $key => $value) {
      $descripcion = $this->WebServicios_model->consultarDescripcion($value->id,$value->id_servicios);

          $valor = $value;
          $valor->descripcion_sin_html = substr(strip_tags($descripcion['descripcion']),0,140)."...";

          $res[] = $valor;
      }
      $listado['tservicios'] = (object)$res;
      $listado['titulo'] = $servicio['titulo'];
      $listado['slug'] = $servicio['slug'];
      $listado['ruta_servicio'] = $servicio['ruta'];

      die(json_encode($listado));
   }
    /*
    * Para ver algunos productos
    */
   public function consultarProductosTodas(){
    $res = [];
    $datos= json_decode(file_get_contents('php://input'), TRUE);
    $respuesta = $this->WebServicios_model->consultarProductos($datos);
    foreach ($respuesta as $key => $value) {

        $valor = $value;
        $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";

        $res[] = $valor;
    }
    $listado = (object)$res;

    die(json_encode($listado));
 }
    /*
    * Para ver el detalle del producto
    */
    public function verDetalleServicios($slug_base,$slug_servicio){
        $datos["idioma"] = 1;
        $datos["slug_base"] = $slug_base;  
        $datos["slug_servicio"] = $slug_servicio;  

        //---
        #Verifico si existen los slug
        $existe_slug = $this->WebServicios_model->consultarServicioSlugExiste($datos["slug_servicio"],$datos["slug_base"]);
        if($existe_slug>0){

          $this->load->view('web/header');
          $this->load->view('web/menu');
          $this->load->view('web/detalleServicios',$datos);
          $this->load->view('web/footer');

        }else{

            $this->load->view('/web/header');
            $this->load->view('/web/menu');
            $this->load->view('/web/error404');
            $this->load->view('/web/footer');

        }
        //---
       
    }
    /*
    * Para ver el detalle del servicio seleccionado
    */
    public function consultarDetalleTservicios(){
      $res = [];
      $datos= json_decode(file_get_contents('php://input'), TRUE);
      $servicio_base = $this->WebServicios_model->consultarServicioSlug($datos);
      $tipo_servicio = $this->WebServicios_model->consultarServicioSlugSegundario($datos);

      //$otros_servicios = $this->WebServicios_model->consultarOtrosDetallesTservicios($servicio_base['id'],$tipo_servicio['id']);

      $respuesta = $this->WebServicios_model->consultarDetalleTservicios($servicio_base['id'],$tipo_servicio['id']);

      foreach ($respuesta as $key => $value) {

          $valor = $value;
          $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";

          $res[] = $valor;
      }
      $listado['detalles_servicios'] = (object)$res;
      $listado['servicio_base'] = $servicio_base;
      $listado['tipo_servicio'] = $tipo_servicio;
      //$listado['otros_servicios'] = $otros_servicios;

      die(json_encode($listado));
   }
  /*
    * Para ver el otros servicios no seleccionados
    */
   public function consultarOtrosTservicios(){
    $res = [];
    $datos= json_decode(file_get_contents('php://input'), TRUE);

    $respuesta = $this->WebServicios_model->consultarOtrosDetallesTservicios($datos['id_servicio'],$datos['id_tipo_servicio']);


    foreach ($respuesta as $key => $value) {

        $valor = $value;
        $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";

        $res[] = $valor;
    }
    $listado = (object)$res;


    die(json_encode($listado));
 }
}    