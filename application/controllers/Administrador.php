<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Administrador extends CI_Controller
{
    function __construct(){
      parent::__construct();
      $this->load->database();
      $this->load->library('session');
      $this->load->model('Administrador_model');
    }

    public function index(){
        if ($this->session->userdata("login")) {
            $this->load->view('administrador/panelUs');
        } else {
            $this->load->view('administrador/inicio');
        }   
    }
    //Metod para el inicio de sesion

    public function iniciar_sesion(){
        //-----------------------------------------------------
        $data= json_decode(file_get_contents('php://input'), TRUE);

        #Validar campos
        if(($data["login"])&&($data["clave"])){
            $recordset = $this->Administrador_model->iniciar_sesion($data["login"],sha1($data["clave"]));
            if($recordset > 0){
                $mensajes["mensajes"] = "inicio con exito";
                $data = array(
                    'login' => $data["login"],
                    'activo' => true,
                    'id' => '',
                    'nombre_persona' => ''
                );
                $this->session->set_userdata($data);
            }else{
                $mensajes["mensajes"] = "error datos";
            }
            die(json_encode($mensajes));      
        }
     
        //-------------------------------------------------------*/
    }
}  


/*public function registerUser(){
        if($this->input->post("email") && $this->input->post("password") && $this->input->post("nombre"))
        {
            $this->form_validation->set_rules('password', 'password', 'trim|required|xss_clean');
            $this->form_validation->set_rules('nombre', 'nombre', 'trim|required|xss_clean');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|xss_clean');
            if($this->form_validation->run() == false){
                echo json_encode(array("respuesta" => "error_form"));
            }else{
                $this->load->model("register_model");
                $email = $this->input->post("email");
                $password = $this->input->post("password");
                $nombre = $this->input->post("nombre");
                $loginUser = $this->register_model->registerUser($email,$password,$nombre);
                if($loginUser === true){
                    echo json_encode(array("respuesta" => "success"));
                }else{
                    echo json_encode(array("respuesta" => "exists"));
                }
            }
        }else{
            echo json_encode(array("respuesta" => "error_form"));
        }
    }*/