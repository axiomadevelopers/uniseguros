<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class WebContactos extends CI_Controller
{
    function __construct(){
      parent::__construct();
      $this->load->database();
      $this->load->model('WebInicio_model');
      $this->load->model('WebContactos_model');

    }
    /*
    * Index
    */
    public function index(){
      $datos["id_idioma"] = 1;
      //---
      $this->load->view('/web/header');
      $this->load->view('/web/menu');
      $this->load->view('/web/contactanos',$datos);
      $this->load->view('/web/footer');
    }
    /*
    * registrarContactos
    */
    public function registrarContactos(){
      $datos = json_decode(file_get_contents('php://input'), TRUE);
      $data = array(
          "nombres" => $datos["nombres"],
          "telefono" => $datos["telefono"],
          "email" => $datos["email"],
          "mensaje" => $datos["mensaje"]
      );
      $correo_respuesta = $this->enviarEmail($data);

      //die("aqui");
      $respuesta = $this->WebContactos_model->guardarContactos($data);
      if($respuesta==true){
        //--Envio de email
          $correo_respuesta = $this->enviarEmail($data);
          if($correo_respuesta){
              $mensajes["mensaje"] = "registro_procesado";
          }else{
              $mensajes["mensaje"] = "error_correo";
          }
          //$mensajes["mensaje"] = "registro_procesado";
      }else{
          $mensajes["mensaje"] = "no_registro";
      }
      die(json_encode($mensajes));
    }
    //----
    public function enviarEmail($data){
        $this->load->library('email');

        //---------------------------------
        $usuario = "uniseguros@yandex.com";
        $clave="uniseguros.181931";
        $correo_remitente = $usuario;
        $destinatario = "contacto@uniseguros.com";
        
        $this->load->library('email');
        
        $config['protocol'] = 'sendmail';
        $config['smtp_host'] = 'smtp.yandex.com';
        $config['smtp_user'] = $usuario;
        $config['smtp_pass'] = $clave;
        $config['smtp_port'] = '587';
        //$config['smtp_timeout']='30';
        $config['smtp_crypto'] = 'tls';
        $config['charset'] = 'utf-8';
        $config['mailtype'] = 'html';
        $config['wordwrap'] = TRUE;
        $config['newline'] = "\r\n";
        $this->email->initialize($config);

        $nombre_remitente = "WebMaster-Uniseguros";
        
        
        $this->email->from($correo_remitente, $nombre_remitente);

        $this->email->to($destinatario);
        
        $titulo =  "Datos de clientes";
        $encabezado = $data['nombres']." realizó su registro a través nuestro website:<br>";
        $asunto = "Mensaje desde formulario contacto";
        $mensaje =  $this->crear_cuadro($titulo,$encabezado,$data);

        
        $this->email->subject($asunto);
        $this->email->message($mensaje);

        if($this->email->send()){
            return true;
        }else{
            echo $this->email->print_debugger();
            return false;
        }
        //---------------------------------
    }
    //----
     //----
    #Para emails:
    public function crear_cuadro($titulo,$encabezado,$arreglo_datos){

        $estructura_mensaje ="<div stle='margin-top:10px;'>
                                <div style='padding:1%;width:95%;height:auto;background-color:#fff;color:#000;font-weight:bold;font-size:14pt;'>
                                    <style='width:3%'>Mensaje de uniseguros</div>
                                    <div style='padding:1%;''>
                                        <div style='padding1%;width:97%;height:auto;background-color:#fff;color:#000;font-weight:bold;font-size:12pt;'>".$encabezado."</div><br>
                                        <div style='margin-top:5px;;padding1%;width:95%;height:auto;background-color:#fff;color:#000;font-size:12;'>
                                            <div><label style='font-weight:bold;'> Nombres y apellidos : </label>".$arreglo_datos["nombres"]."</div>
                                            <div>
                                                <p><label style='font-weight:bold;'> 
                                                    Telefono:
                                                  </label>".$arreglo_datos["telefono"]."</p>
                                                <p><label style='font-weight:bold;'>Correo: </label>".$arreglo_datos["email"]."</p>
                                                <p style='text-align:justify;'><label style='font-weight:bold;'> Mensaje: </label>".$arreglo_datos["mensaje"]."</p>
                                            </div>
                                        <div style='clear:both'></div>  
                                    </div>
                                    <div style=';width:95%;height:auto;background-color:#fff;color:#000;font-size:10pt;'>Ingrese en el manejador de contenido para verificar estos datos </div>
                            </div>.";                   
        //$estructura_mensaje ="Esto es una prueba de correo".$arreglo_datos["nombres_contacto"]." ".$arreglo_datos["apellidos_contacto"];
        return $estructura_mensaje;                 
    }
    //----
    /*
    * consultaFooter
    */
    public function consultarFooter(){
      $res = [];
      $datos= json_decode(file_get_contents('php://input'), TRUE);
      $respuesta = $this->WebContactos_model->consultarFooter($datos);
      foreach ($respuesta as $key => $value) {
          $valor = $value;
          //$valor->descripcion_sin_html = strip_tags($value->descripcion);
          $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";
          $res[] = $valor;
      }
      $listado = (object)$res;
      die(json_encode($listado));
    }
    /*
    * consultarRedes
    */
    public function consultarRedes(){
        $datos = json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->WebContactos_model->consultarRedes($datos);
        $listado = (object)$respuesta;
        die(json_encode($listado));
    }
    /*
    *   consultarMetaTag
    */
    public function consultarMetaTag(){
        $datos = json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = array("descripcion"=>"","palabras_claves"=>"");
        $arr_descripcion = $this->WebContactos_model->consultarDescripcion();
        $arr_palabras_claves = $this->WebContactos_model->consultarPalabrasClaves();
        
        if(isset($arr_descripcion[0]->descripcion))
             $respuesta["descripcion"]= $arr_descripcion[0]->descripcion;
        
        if(isset($arr_palabras_claves[0]->palabras_claves)) 
            $respuesta["palabras_claves"]= $arr_palabras_claves[0]->palabras_claves;
        
        //$respuesta["lineas"] = $this->WebInicio_model->consultarLinea($datos);
        $listado = (object)$respuesta;
        die(json_encode($listado));
    }
    /*
    * consultarDireccion
    */
    public function consultarDireccion(){
        $res = [];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->WebContactos_model->consultarDireccion($datos);
        $a = 1;
        foreach ($respuesta as $key => $value) {
         $valor = $value;
         $telefonos = $this->WebContactos_model->consultarTelefonos($value->id);
         $valor->telefono = "";
         if($telefonos){
            foreach ($telefonos as $key_tlf => $value_tlf) {
              $tlf_arr[] = $value_tlf->telefono; 
            }
            $valor->telefono = $tlf_arr;
            $tlf_arr = [];
         }

         //$valor->descripcion_sin_html = strip_tags($value->descripcion);
         $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";

         $res[] = $valor;
         $a++;
        }
        $listado = (object)$res;
        die(json_encode($listado));
    }
    /*
    * Consulta de cuentas bancarias
    */
    public function consultarCuentaTodas(){
      $res = [];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
    $respuesta = $this->WebContactos_model->consultarCuentas($datos);
        foreach ($respuesta as $key => $value) {
            $valor = $value;
            //$valor->descripcion_sin_html = strip_tags($value->descripcion);
            $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";
            $res[] = $valor;
        }
        $listado = (object)$res;
        die(json_encode($listado));
    } 
    /*
    * Consulta del pdf relacionado al modulo
    */
    public function consultarPdf(){
        $res = [];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->WebContactos_model->consultarPdf($datos);

        foreach ($respuesta as $key => $value) {
            $valor = $value;
            //$valor->descripcion_sin_html = strip_tags($value->descripcion);
            $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";
            $res[] = $valor;
        }
        $listado = (object)$res;
        die(json_encode($listado));
    } 
    /*
    * Consulta pdf con filtro
    */
    public function consultarPdfLimit(){
        $res = [];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        if(!isset($datos["offset"])){
            $datos["offset"] = 0;
        }
        $respuesta = $this->WebContactos_model->consultarPdfLimit($datos);

        foreach ($respuesta as $key => $value) {
            $valor = $value;
            //$valor->descripcion_sin_html = strip_tags($value->descripcion);
            $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";
            $res[] = $valor;
        }
        $listado = (object)$res;
        die(json_encode($listado));
    }
    /***/
}    