<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Direccion extends CI_Controller{

		function __construct(){
			parent::__construct();
			$this->load->database();
			$this->load->library('session');
			$this->load->model('Direccion_model');
			$this->load->model('Auditoria_model');
			$cms = $_SESSION["cms"];
			if (!$cms["login"]) {
				redirect(base_url());
			}
		}

		public function index(){
			//--- Datos de usuario
	    	$cms = $_SESSION["cms"];
        	$data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
	    	//--
	        $this->load->view('cpanel/header');
	        $this->load->view('cpanel/dashBoard',$data);
	    	$this->load->view('cpanel/menu',$data);
	        $this->load->view('modulos/direccion/direccion');
	        $this->load->view('cpanel/footer');
	    }

		public function registrarDireccion(){
			$datos= json_decode(file_get_contents('php://input'), TRUE);
			$posicionar = array(
              'posicion' => $datos["orden"],
              'tipo' => 'insert',
              'id_idioma' =>  $datos['id_idioma'],
            );
            $this->Direccion_model->posicionar_modulos($posicionar);
			$data = array(
			  'titulo' => trim(mb_strtoupper($datos['titulo'])),
			  'descripcion' => trim(mb_strtoupper($datos['descripcion'])),
			  'orden'=> $datos["orden"],
			  'id_idioma' => $datos['id_idioma'],
			  'latitud'=> $datos['latitud'],
			  'longitud'=> $datos['longitud'],
			  'estatus' => '1',
			  'iframe' =>$datos["iframe"]
		  	);
			$respuesta = $this->Direccion_model->guardarDireccion($data);
			if($respuesta==true){
				$mensajes["mensaje"] = "registro_procesado";
				
				//------------------------------------------------------------
					//--Bloque Auditoria 
					$id = $this->Auditoria_model->consultar_max_id("direcciones");
					$accion = "Registro de direccion id:".$id.",titulo:".trim($datos['titulo']);      
					$cms = $_SESSION["cms"]; 
			        $data_auditoria = array(
			                                "id_usuario"=>(integer)$cms["id"],
			                                "modulo"=>'1',
			                                "accion"=>$accion,
			                                "ip"=>$this->Auditoria_model->get_client_ip(),
			                                "fecha_hora"=> date("Y-m-d H:i:00")
					);
			        $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
				//------------------------------------------------------------
				//Guardar TLF
				//Bloque para registrar telefonos
				$telefono_vector = $datos["telefono"];
				foreach ($telefono_vector as $value) {
					//--
					$existe = $this->Direccion_model->existe_telefono_direccion($id,$value);

					if($existe==0){
						$data = array(
										"id_direccion"=>$id,
										"telefono"=>$value
								);		
						$recordset_telefono = $this->Direccion_model->registrar_telefono($data);
					}
					//--
				}
				//---------------------------------------------------------------
			}else{
				$mensajes["mensaje"] = "no_registro";
			}
			die(json_encode($mensajes));
		}

		public function consultarDireccion(){
			//--- Datos de usuario
	    	$cms = $_SESSION["cms"];
        	$data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
	    	//--
	        $this->load->view('cpanel/header');
	        $this->load->view('cpanel/dashBoard',$data);
	    	$this->load->view('cpanel/menu',$data);
	        $this->load->view('modulos/direccion/consultar_direccion');
	        $this->load->view('cpanel/footer');
	    }

		public function consultarDireccionTodas(){
		   $res = [];
		   $datos= json_decode(file_get_contents('php://input'), TRUE);
		   $respuesta = $this->Direccion_model->consultarDireccion($datos);
		   $a = 1;
		   foreach ($respuesta as $key => $value) {
			   $valor = $value;
			   $telefonos = $this->Direccion_model->consultarTelefonos($value->id);
			   $valor->telefono = "";
			   if($telefonos){
				   	foreach ($telefonos as $key_tlf => $value_tlf) {
				   		$tlf_arr[] = $value_tlf->telefono; 
				   	}
				   	$valor->telefono = $tlf_arr;
			   }
			  
			   //$valor->descripcion_sin_html = strip_tags($value->descripcion);
			   $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";

			   $res[] = $valor;
			   $a++;
		   }
		   $listado = (object)$res;
		   die(json_encode($listado));
	   }

	   public function direccionVer(){
	   	   //--- Datos de usuario
			$cms = $_SESSION["cms"];
        	$data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
	       //--	
		   $datos["id"] = $this->input->post('id_marca');
		   $this->load->view('cpanel/header');
		   $this->load->view('cpanel/dashBoard',$data);
	       $this->load->view('cpanel/menu',$data);
		   $this->load->view('modulos/direccion/direccion',$datos);
		   $this->load->view('cpanel/footer');
	   }

	   public function modificarDireccion(){
		    $datos = json_decode(file_get_contents('php://input'), TRUE);
		    $posicionar = array(
              'inicial' => $datos["inicial"],
              'tipo' => 'update',
              'id_idioma' =>  $datos['id_idioma'],
              'final' => $datos["orden"]
            );
		   //-Verifico si existe una noticia con ese titulo....
		   $existe = $this->Direccion_model->consultarExiste($datos["id"]);
		   if($existe>0){
			   $data = array(
				 'id' =>  $datos['id'],
				 'titulo' => trim(mb_strtoupper($datos['titulo'])),
				 'descripcion' => trim(mb_strtoupper($datos['descripcion'])),
				 'id_idioma' => $datos['id_idioma'],
				 'latitud' => $datos["latitud"],
				 'longitud' => $datos["longitud"],
				 'orden' => $datos["orden"],
				 'iframe' =>$datos["iframe"],
			   );
			   $this->Direccion_model->posicionar_modulos($posicionar);
			   $respuesta = $this->Direccion_model->modificarDireccion($data);
			   if($respuesta==true){
			   		//-------------------------------------------------------
			   		//Bloque para registrar telefonos
					//Elimino los telefonos
					$id_direccion = $datos["id"];
					$recordset_limpiar = $this->Direccion_model->eliminar_telefonos($id_direccion);
					$telefono_vector = $datos["telefono"];
					foreach ($telefono_vector as $value_tlf) {
						//--
						$existe =  $this->Direccion_model->existe_telefono_direccion($id_direccion,$value_tlf);
						//var_dump($existe);die;
						if($existe==0){
							$data = array(
										"id_direccion"=>$id_direccion,
										"telefono"=>$value_tlf
							);	
							$recordset_telefono =  $this->Direccion_model->registrar_telefono($data);
						}
						//--
					}
			   		//-------------------------------------------------------
				   $mensajes["mensaje"] = "modificacion_procesada";
				     //----------------------------------------------------
						//--Bloque Auditoria 
						$accion = "Actualizacion de marca id: ".$datos['id'];    
						$cms = $_SESSION["cms"];        
				        $data_auditoria = array(
				                                "id_usuario"=>(integer)$cms["id"],
				                                "modulo"=>'1',
				                                "accion"=>$accion,
				                                "ip"=>$this->Auditoria_model->get_client_ip(),
				                                "fecha_hora"=> date("Y-m-d H:i:00")
				        );
				        $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
					//-----------------------------------------------------
			   }else{
				   $mensajes["mensaje"] = "no_registro";
			   }
		   }else{
				$mensajes["mensaje"] = "existe";
		   }
		   //--
		   die(json_encode($mensajes));
	   }

	   public function modificarDireccionEstatus(){
		   $datos= json_decode(file_get_contents('php://input'), TRUE);
		   $data = array(
			 'id' =>$datos['id'],
			 'estatus' => $datos['estatus'],
		   );
		   $respuesta = $this->Direccion_model->modificarDireccionEstatus($data);

		   if($respuesta==true){
			   $mensajes["mensaje"] = "modificacion_procesada";
			   //----------------------------------------------------
					//--Bloque Auditoria 
					switch ($data["estatus"]) {
						case '0':
							$accion="Inactivar direccion id: ".$datos['id'];
							break;
						case '1':
							$accion="Activar direccion id: ".$datos['id'];
							break;
						case '2':
							$accion="Eliminar direccion id: ".$datos['id'];
							break;
					}				
					$cms = $_SESSION["cms"];   
			        $data_auditoria = array(
			                                "id_usuario"=>(integer)$cms["id"],
			                                "modulo"=>'1',
			                                "accion"=>$accion,
			                                "ip"=>$this->Auditoria_model->get_client_ip(),
			                                "fecha_hora"=> date("Y-m-d H:i:00")
			        );
			        $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
					//-----------------------------------------------------
		   }else{
			   $mensajes["mensaje"] = "no_modifico";
		   }

		   die(json_encode($mensajes));
	   }

	   public function consultarmarca_idioma(){
		   //print_r("hola");
		   $res = [];
		   $datos= json_decode(file_get_contents('php://input'), TRUE);
		   $respuesta = $this->Marcas_model->consultarMarcas_idioma($datos);
		   //print_r($respuesta);die;
		   $a = 1;
		   foreach ($respuesta as $key => $value) {
			   $valor = $value;
			   $res[] = $valor;
			   $a++;
		   }
		   $listado = (object)$res;
		   die(json_encode($listado));
	   }
	//-------------------------------------------------------------------------
	public function consultar_orden(){
    	
    	$datos= json_decode(file_get_contents('php://input'), TRUE);
    	
        $respuesta = $this->Direccion_model->consultarOrden($datos);
        
        if(!$respuesta){
        	$listado2["orden"] = array( "orden"=>1 );
        	$listado = (object)$listado2;
        }else{
        	$c = 1;
        	foreach($respuesta as $clave => $valor) {
        		$respuesta2[] = array( "orden"=>$c );
        		$c++;
        	}
        	if($datos["tipo"]=="1")
        		$respuesta2[] = array( "orden"=>$c );
        	//var_dump($c);
        	$listado  = (object)$respuesta2;
        }

        die(json_encode($listado));
    }
    //--------------------------------------------------------------------------

	}
?>
