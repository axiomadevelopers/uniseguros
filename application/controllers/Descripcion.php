<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Descripcion extends CI_Controller{

		function __construct(){
			parent::__construct();
			$this->load->database();
			$this->load->library('session');
			$this->load->model('Descripcion_model');
			$this->load->model('Auditoria_model');
			$cms = $_SESSION["cms"];
			if (!$cms["login"]) {
					redirect(base_url());
			}
		}

		public function index(){
			//--- Datos de usuario
        	$cms = $_SESSION["cms"];
        	$data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);

        	//--
	        $this->load->view('cpanel/header');
	        $this->load->view('cpanel/dashBoard',$data);
        	$this->load->view('cpanel/menu',$data);
	        $this->load->view('modulos/descripcion/descripcion');
	        $this->load->view('cpanel/footer');
	    }
	    /*
	    *	registrarDescripcion
	    */
		public function registrarDescripcion(){
			$datos= json_decode(file_get_contents('php://input'), TRUE);
			$data = array(
				'descripcion' => trim(strtolower($datos['descripcion'])) ,
				'estatus' => '1',
              );
			$respuesta = $this->Descripcion_model->guardarDescripcion($data);

			if($respuesta==true){
				$mensajes["id"] = $this->Descripcion_model->consultarIdDescripcion();
				$mensajes["mensaje"] = "registro_procesado";
				//-----------------------------------------------------
	            //Bloque de auditoria:
	            $accion = "Registro descripcion id: ".$mensajes["id"];
	            $cms = $_SESSION["cms"];
	            $data_auditoria = array(
	                                    "id_usuario"=>(integer)$cms["id"],
	                                    "modulo"=>'1',
	                                    "accion"=>$accion,
	                                    "ip"=>$this->Auditoria_model->get_client_ip(),
	                                    "fecha_hora"=> date("Y-m-d H:i:00")
	            );
	            $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
	            //-----------------------------------------------------
			}else{
				$mensajes["mensaje"] = "no_registro";
			}
			//var_dump($existe1,$existe2);die;

			die(json_encode($mensajes));
		}
		/*
		*	modificarDescripcion
		*/
		public function modificarDescripcion(){
	        $datos= json_decode(file_get_contents('php://input'), TRUE);
	        $data = array(
	          	'descripcion' => trim(strtolower($datos['descripcion'])) ,
	        );
	        //verifico si existe la categoria con ese nombre
	        $existe = $this->Descripcion_model->existeDescripcion($datos['id']);
	        if(count($existe)>0){
	                $respuesta = $this->Descripcion_model->modificarDescripcion($datos['id'],$data);
	        }else{
	            $mensajes["mensaje"] = "no_existe";
	        }

	        if($respuesta==true){
	            $mensajes["mensaje"] = "modificacion_procesada";
	            //-----------------------------------------------------
	            //Bloque de auditoria:
	            $accion = "Modificar descripcion id: ".$datos['id'];
	            $cms = $_SESSION["cms"];
	            $data_auditoria = array(
	                                    "id_usuario"=>(integer)$cms["id"],
	                                    "modulo"=>'1',
	                                    "accion"=>$accion,
	                                    "ip"=>$this->Auditoria_model->get_client_ip(),
	                                    "fecha_hora"=> date("Y-m-d H:i:00")
	            );
	            $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
	            //-----------------------------------------------------
	        }else{
	            $mensajes["mensaje"] = "no_modifico";
	        }  
	       
	        die(json_encode($mensajes));
	    }
	    /*
	    * consultarDescripcion
	    */
	    public function consultarDescripcion(){
	    	
	    	$respuesta = $this->Descripcion_model->consultarDescripcion();
	    	$res = [];
	    	
	    	if($respuesta!=""){
	    		//--
	    		foreach ($respuesta as $key => $value) {
		            $valor = $value;
		            $res[] = $valor;
		        }
	    		//--
	    	}
	       
	        $listado = (object)$res;
	        die(json_encode($listado));
		}
	    /***/
	}
