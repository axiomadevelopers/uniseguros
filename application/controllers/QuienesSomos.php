<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 */
class QuienesSomos extends CI_Controller
{

	function __construct(){
      	parent::__construct();
      	$this->load->database();
      	$this->load->library('session');
		$this->load->model('QuienesSomos_model');
		$this->load->model('Idiomas_model');
		$this->load->model('Auditoria_model');
      	$cms = $_SESSION["cms"];
		if (!$cms["login"]) {
			redirect(base_url());
		}
    }
    
    public function consultar_idioma(){
		$datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->Idiomas_model->consultarIdiomas($datos);
        die(json_encode($respuesta));
	}

	public function index(){
		//--- Datos de usuario
    	$cms = $_SESSION["cms"];
        $data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
    	//--
        $this->load->view('cpanel/header');
        $this->load->view('cpanel/dashBoard',$data);
	    $this->load->view('cpanel/menu',$data);
        $this->load->view('modulos/empresa/nosotros');
        $this->load->view('cpanel/footer');
    }

	public function consultar_nosotros(){
		//--- Datos de usuario
    	$cms = $_SESSION["cms"];
        $data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
    	//--
		$this->load->view('cpanel/header');
        $this->load->view('cpanel/dashBoard',$data);
	    $this->load->view('cpanel/menu',$data);
		$this->load->view('modulos/empresa/consultarNosotros');
		$this->load->view('cpanel/footer');
	}

	public function registrarNosotros(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $posicionar = array(
              'posicion' => $datos["orden"],
              'tipo' => 'insert',
              'id_idioma' =>  $datos['id_idioma'],
            );
            $this->QuienesSomos_model->posicionar_modulos($posicionar);

        $data = array(
          'descripcion' => trim($datos['descripcion']),
		  'titulo' => trim($datos['titulo']),
		  'id_idioma' => $datos['id_idioma'],
		  'icono'=>$datos["icono"],
		  'orden'=>$datos["orden"],
          'estatus' => '1',
          'id_imagen' => $datos['id_imagen'],

        );
		//print_r($data);die;
        $respuesta = $this->QuienesSomos_model->guardarNosotros($data);
        if($respuesta==true){
			$mensajes["mensaje"] = "registro_procesado";
				//------------------------------------------------------------
					//--Bloque Auditoria 
					$id = $this->Auditoria_model->consultar_max_id("categoria_producto");
					$accion = "Registro de categoría producto id:".$id.",titulo:".trim($datos['titulo']);
					$cms = $_SESSION["cms"];              
			        $data_auditoria = array(
			                                "id_usuario"=>(integer)$cms["id"],
			                                "modulo"=>'1',
			                                "accion"=>$accion,
			                                "ip"=>$this->Auditoria_model->get_client_ip(),
			                                "fecha_hora"=> date("Y-m-d H:i:00")
					);
			        $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
					//------------------------------------------------------------
        }else{
            $mensajes["mensaje"] = "no_registro";
        }
        die(json_encode($mensajes));
    }

	public function consultarNosotrosTodas(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->QuienesSomos_model->consultarNosotrosTodas($datos);
		//print_r($respuesta);die;
        $res = [];
        $imagen = "";
        foreach ($respuesta as $key => $value) {
            $valor = $value;
            //if (($value->id_imagen!="0")&&($value->id_imagen!="")){
        	$imagen = $this->QuienesSomos_model->consultarImg($value->id_imagen);
        	$value->id_imagen = $imagen[0]->id;
        	$value->imagen = $imagen[0]->ruta;
            //}

			$valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";
            $res[] = $valor;
        }

        $listado = (object)$res;
        die(json_encode($listado));
    }

	public function nosotrosVer(){
		//--- Datos de usuario
    	$cms = $_SESSION["cms"];
        $data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
    	//--
		$datos["id_nosotros"] = $this->input->post('id_nosotros');
		//print_r($datos);die;
		$this->load->view('cpanel/header');
		$this->load->view('cpanel/dashBoard',$data);
	    $this->load->view('cpanel/menu',$data);
		$this->load->view('modulos/empresa/nosotros', $datos);
		$this->load->view('cpanel/footer');
	}

	public function modificarNosotros(){
		$datos = json_decode(file_get_contents('php://input'), TRUE);
		$posicionar = array(
			  'inicial' => $datos["inicial"],
			  'tipo' => 'update',
			  'id_idioma' =>  $datos['id_idioma'],
			  'final' => $datos["orden"]
		);
		//print_r($datos);die;
		//-Verifico si existe una noticia con ese titulo....
		$existe = $this->QuienesSomos_model->consultarExiste($datos["id"]);
		if($existe>0){
			$data = array(
			  'id' =>  $datos['id'],
			  'titulo' => trim($datos['titulo']),
			  'descripcion' => trim($datos['descripcion']),
			  'id_idioma' => $datos['id_idioma'],
			  'id_imagen' => $datos['id_imagen'],
			  'icono'=>$datos["icono"],
			  'orden'=>$datos["orden"],
			);
			//var_dump($data);die('');
			$this->QuienesSomos_model->posicionar_modulos($posicionar);
			$respuesta = $this->QuienesSomos_model->modificarNosotros($data);
			if($respuesta==true){
				$mensajes["mensaje"] = "modificacion_procesada";
				     //----------------------------------------------------
						//--Bloque Auditoria 
						$accion = "Actualizacion de categoría producto id: ".$datos['id'];

						$cms = $_SESSION["cms"];           
           
				        $data_auditoria = array(
				                                "id_usuario"=>(integer)$cms["id"],
				                                "modulo"=>'1',
				                                "accion"=>$accion,
				                                "ip"=>$this->Auditoria_model->get_client_ip(),
				                                "fecha_hora"=> date("Y-m-d H:i:00")
				        );
				        $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
					//-----------------------------------------------------
			}else{
				$mensajes["mensaje"] = "no_registro";
			}
		}else{
			 $mensajes["mensaje"] = "existe";
		}
		//--
		die(json_encode($mensajes));
	}

	public function modificarNosotrosEstatus(){
		$datos= json_decode(file_get_contents('php://input'), TRUE);
		$data = array(
		  'id' =>$datos['id'],
		  'estatus' => $datos['estatus'],
		);
		$respuesta = $this->QuienesSomos_model->modificarNosotrosEstatus($data);

		if($respuesta==true){
			$mensajes["mensaje"] = "modificacion_procesada";
			   //----------------------------------------------------
					//--Bloque Auditoria 
					switch ($data["estatus"]) {
						case '0':
							$accion="Inactivar categoría producto id: ".$datos['id'];
							break;
						case '1':
							$accion="Activar categoría producto id: ".$datos['id'];
							break;
						case '2':
							$accion="Eliminar categoría producto id: ".$datos['id'];
							break;
					}
					$cms = $_SESSION["cms"]; 
			        $data_auditoria = array(
			                                 "id_usuario"=>(integer)$cms["id"],
			                                "modulo"=>'1',
			                                "accion"=>$accion,
			                                "ip"=>$this->Auditoria_model->get_client_ip(),
			                                "fecha_hora"=> date("Y-m-d H:i:00")
			        );
			        $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
					//-----------------------------------------------------
		}else{
			$mensajes["mensaje"] = "no_modifico";
		}

		die(json_encode($mensajes));
	}
	//-----------------------------------------------------------------------------
	public function consultar_orden(){
    	
    	$datos= json_decode(file_get_contents('php://input'), TRUE);
    	
        $respuesta = $this->QuienesSomos_model->consultarOrden($datos);
        
        if(!$respuesta){
        	$listado2["orden"] = array( "orden"=>1 );
        	$listado = (object)$listado2;
        }else{
        	$c = 1;
        	foreach($respuesta as $clave => $valor) {
        		$respuesta2[] = array( "orden"=>$c );
        		$c++;
        	}
        	if($datos["tipo"]=="1")
        		$respuesta2[] = array( "orden"=>$c );
        	//var_dump($c);
        	$listado  = (object)$respuesta2;
        }

        die(json_encode($listado));

    }
    //------------------------------------------------------------------------------
}
?>
