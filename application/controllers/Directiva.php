<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Directiva extends CI_Controller{

		function __construct(){
			parent::__construct();
			$this->load->database();
			$this->load->library('session');
			$this->load->model('Directiva_model');
			$this->load->model('Auditoria_model');
			$cms = $_SESSION["cms"];
			if (!$cms["login"]) {
						redirect(base_url());
			}
		}

		public function index(){
			//--- Datos de usuario
			$cms = $_SESSION["cms"];
        	$data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
	    	//--
	        $this->load->view('cpanel/header');
	        $this->load->view('cpanel/dashBoard',$data);
	    	$this->load->view('cpanel/menu',$data);
	        $this->load->view('modulos/empresa/directiva');
	        $this->load->view('cpanel/footer');
	        //var_dump($this->session->userdata("login"));//die();

	    }

		public function registrarDirectiva(){
			$datos= json_decode(file_get_contents('php://input'), TRUE);
			$posicionar = array(
              'posicion' => $datos["orden"],
              'tipo' => 'insert',
              'id_idioma' =>  $datos['id_idioma'],
            );
            $this->Directiva_model->posicionar_modulos($posicionar);

			//print_r($datos);die;
			$data = array(
			  'titulo' => trim(mb_strtoupper($datos['titulo'])),
			  'descripcion' => trim(mb_strtoupper($datos['descripcion'])),
			  'id_idioma' => $datos['id_idioma'],
			  'id_imagen' => trim(mb_strtoupper($datos['id_imagen'])),
			  'estatus' => '1',
  			  'orden' =>  $datos["orden"]
			);
			//var_dump($data);
			$respuesta = $this->Directiva_model->guardarDirectiva($data);
			
			if($respuesta==true){
				$mensajes["mensaje"] = "registro_procesado";
				//------------------------------------------------------------
					//--Bloque Auditoria 
					$id = $this->Auditoria_model->consultar_max_id("directiva");
					$accion = "Registro de directiva id:".$id.",titulo:".trim($datos['titulo']);
					$cms = $_SESSION["cms"];           
			        $data_auditoria = array(
			                                "id_usuario"=>(integer)$cms["id"],
			                                "modulo"=>'1',
			                                "accion"=>$accion,
			                                "ip"=>$this->Auditoria_model->get_client_ip(),
			                                "fecha_hora"=> date("Y-m-d H:i:00")
					);
			        $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
				//------------------------------------------------------------
			}else{
				$mensajes["mensaje"] = "no_registro";
			}
			die(json_encode($mensajes));
		}
		/*
		*	Modificar directiva
		*/
		public function modificarDirectiva(){
	        $datos = json_decode(file_get_contents('php://input'), TRUE);
	        //var_dump($datos);die('');
	        $posicionar = array(
              'inicial' => $datos["inicial"],
              'tipo' => 'update',
              'id_idioma' =>  $datos['id_idioma'],
              'final' => $datos["orden"]
            );
            //var_dump($posicionar);die('');
	        //-Verifico si existe una noticia con ese titulo....
	        $existe = $this->Directiva_model->consultarExiste($datos["id"]);
	        if($existe>0){
	            $data = array(
	              'titulo' => trim(mb_strtoupper($datos['titulo'])),
				  'descripcion' => trim(mb_strtoupper($datos['descripcion'])),
				  'id_idioma' => $datos['id_idioma'],
				  'id_imagen' => $datos['id_imagen'],
    			  'orden'=>  $posicionar["final"]
	            );
	            $id = $datos['id'];

	            $this->Directiva_model->posicionar_modulos($posicionar);

	            $respuesta = $this->Directiva_model->modificarDirectiva($data,$datos['id']);
	            if($respuesta==true){
					$mensajes["mensaje"] = "modificacion_procesada";
					//----------------------------------------------------
						//--Bloque Auditoria 
						$accion = "Actualizacion de directiva id: ".$id;
						$cms = $_SESSION["cms"];                  
				        $data_auditoria = array(
				                                "id_usuario"=>(integer)$cms["id"],
				                                "modulo"=>'1',
				                                "accion"=>$accion,
				                                "ip"=>$this->Auditoria_model->get_client_ip(),
				                                "fecha_hora"=> date("Y-m-d H:i:00")
				        );
				        $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
					//-----------------------------------------------------
	            }else{
	                $mensajes["mensaje"] = "no_registro";
	            }
	        }else{
	             $mensajes["mensaje"] = "existe";
	        }
	        //--
	        die(json_encode($mensajes));
	    }
		/*
		*	Consultar directiva
		*/

	    public function consultar_directiva(){
	    	//--- Datos de usuario
	    	$cms = $_SESSION["cms"];
        	$data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
	    	//--
	        $this->load->view('cpanel/header');
	       	$this->load->view('cpanel/dashBoard',$data);
	    	$this->load->view('cpanel/menu',$data);
	        $this->load->view('modulos/empresa/consultar_directiva');
	        $this->load->view('cpanel/footer');
	        //var_dump("aqui6!");

	    }

	    public function consultarDirectivaTodas(){
	        $res = [];
	        $datos= json_decode(file_get_contents('php://input'), TRUE);
	        $respuesta = $this->Directiva_model->consultarDirectiva($datos);
	        $a = 1;
	        foreach ($respuesta as $key => $value) {
				$valor = $value;

	            //$valor->descripcion_sin_html = strip_tags($value->descripcion);
	            $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";

	            $res[] = $valor;
	            $a++;
	        }
	        $listado = (object)$res;
	        die(json_encode($listado));
	    }

	    public function modificarDirectivaEstatus(){
	    	$datos= json_decode(file_get_contents('php://input'), TRUE);
	        $data = array(
	          //'id' =>$datos['id'],
	          'estatus' => $datos['estatus'],
	        );
	        $respuesta = $this->Directiva_model->modificarDirectiva($data,$datos["id"]);
	        if($respuesta==true){
				$mensajes["mensaje"] = "modificacion_procesada";
					//----------------------------------------------------
					//--Bloque Auditoria 
					switch ($data["estatus"]) {
						case '0':
							$accion="Inactivar directiva id: ".$datos['id'];
							break;
						case '1':
							$accion="Activar directiva id: ".$datos['id'];
							break;
						case '2':
							$accion="Eliminar directiva id: ".$datos['id'];
							break;
					}
					$cms = $_SESSION["cms"];
			        $data_auditoria = array(
			                                "id_usuario"=>(integer)$cms["id"],
			                                "modulo"=>'1',
			                                "accion"=>$accion,
			                                "ip"=>$this->Auditoria_model->get_client_ip(),
			                                "fecha_hora"=> date("Y-m-d H:i:00")
			        );
			        $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
					//-----------------------------------------------------
	        }else{
	            $mensajes["mensaje"] = "no_modifico";
	        }

	        die(json_encode($mensajes));
		}

		public function directivaVer(){
			//--- Datos de usuario
	    	$cms = $_SESSION["cms"];
       		$data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
	    	//--
	        $datos["id"] = $this->input->post('id_directiva');
	        $this->load->view('cpanel/header');
	        $this->load->view('cpanel/dashBoard',$data);
	    	$this->load->view('cpanel/menu',$data);
	        $this->load->view('modulos/empresa/directiva',$datos);
	        $this->load->view('cpanel/footer');
	    }

	    public function consultar_orden(){
	    	$datos= json_decode(file_get_contents('php://input'), TRUE);
	        $respuesta = $this->Directiva_model->consultarOrden($datos);
	        //var_dump($respuesta);die('');
	        if(!$respuesta){
	        	$listado2["orden"] = array( "orden"=>1 );
	        	$listado = (object)$listado2;
	        }else{
	        	$c = 1;
	        	//var_dump($respuesta);die('');
	        	foreach($respuesta as $clave => $valor) {
	        		$respuesta2[] = array( "orden"=>$c );
	        		$c++;
	        	}
	        	if($datos["tipo"]=="1")
	        		$respuesta2[] = array( "orden"=>$c );
	        	//var_dump($c);
	        	$listado  = (object)$respuesta2;

	        }
	        die(json_encode($listado));
	    }
	}
?>
