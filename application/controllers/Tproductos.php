<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Tproductos extends CI_Controller
{
    function __construct(){
      parent::__construct();
      $this->load->database();
      $this->load->model('Tproductos_model');
      $this->load->model('Auditoria_model');
      $this->load->library('session');
      
      //var_dump($this->session->userdata("login"));die();
        $cms = $_SESSION["cms"];
        if (!$cms["login"]) {  
              redirect(base_url());
        }
    }

    public function index(){
        //--- Datos de usuario
        $cms = $_SESSION["cms"];
        $data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
        //--
        //--
        $this->load->view('cpanel/header');
        $this->load->view('cpanel/dashBoard',$data);
        $this->load->view('cpanel/menu',$data);
        $this->load->view('modulos/productos/tproductos');
        $this->load->view('cpanel/footer');
    }
    public function consultarProductos(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        if(!$datos){
            $respuesta["mensaje"] = "error";
        }else{
            $respuesta = $this->Tproductos_model->consultarProductos($datos);
        }
        die(json_encode($respuesta));
	}
    public function registrarProductos(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $posicionar = array(
            'posicion' => $datos["orden"],
            'tipo' => 'insert',
            'id_idioma' =>  $datos['id_idioma'],
            'id_productos' =>  $datos['id_productos'],
          );
          $this->Tproductos_model->posicionar_modulos($posicionar);
        $cms = $_SESSION["cms"]; 
        $data = array(
          'id' => "",
          'titulo' => trim(ucfirst(mb_strtolower($datos['titulo']))),
          'descripcion' => trim($datos['descripcion']),
          'id_idioma' => $datos['id_idioma'],
          'id_productos' => $datos['id_productos'],
          'estatus' => '1',
          'id_imagen' => $datos['id_imagen'],
          'slug' =>$this->generarSlug($datos["titulo"]),
          'orden' => $datos['orden'],

        );
        $existe = $this->Tproductos_model->consultarExisteTitulo($data['id'],$data['titulo']);
        if(!$existe){
            $respuesta = $this->Tproductos_model->guardarProductos($data);

            if($respuesta==true){
                $mensajes["mensaje"] = "registro_procesado";
                //-----------------------------------------------------
                
                //-----------------------------------------------------
            }else{
                $mensajes["mensaje"] = "no_registro";
            }
        }else{
            $mensajes["mensaje"] = "existe";

        }

        die(json_encode($mensajes));
    }

    public function modificarProductos(){
        $datos = json_decode(file_get_contents('php://input'), TRUE);
        //-Verifico si existe una noticia con ese titulo....
            $posicionar = array(
                'inicial' => $datos["inicial"],
                'tipo' => 'update',
                'id_idioma' =>  $datos['id_idioma'],
                'final' => $datos["orden"],
                'id_productos' =>  $datos['id_productos'],
            );
            $data = array(
                'id' => $datos['id'],
                'titulo' => trim(ucfirst(mb_strtolower($datos['titulo']))),
                'descripcion' => trim($datos['descripcion']),
                'id_productos' => $datos['id_productos'],
                'estatus' => '1',
                'id_imagen' => $datos['id_imagen'],
                'slug' =>$this->generarSlug($datos["titulo"]),
                'orden' => $datos['orden']

            );
            $this->Tproductos_model->posicionar_modulos($posicionar);

            $existe_titulo = $this->Tproductos_model->consultarExisteTitulo($data["id"],$data["titulo"]);
            if($existe_titulo==0){
            $respuesta = $this->Tproductos_model->modificarProductos($data);
            if($respuesta==true){
                $mensajes["mensaje"] = "registro_procesado";
                //-----------------------------------------------------
                //Bloque de auditoria:
                $accion = "Actualizar noticia id: ".$datos['id']." titulo: ".trim(mb_strtoupper($datos['titulo']));
                $cms = $_SESSION["cms"]; 
                $data_auditoria = array(
                                        "id_usuario"=>(integer)$cms["id"],
                                        "modulo"=>'1',
                                        "accion"=>$accion,
                                        "ip"=>$this->Auditoria_model->get_client_ip(),
                                        "fecha_hora"=> date("Y-m-d H:i:00")
                );
                $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
                //-----------------------------------------------------
            }else{
                $mensajes["mensaje"] = "no_registro";
            }
        }else{
             $mensajes["mensaje"] = "existe";
        }
        //--
        die(json_encode($mensajes));
    }

    public function consultar_tproductos(){
        //--- Datos de usuario
        $cms = $_SESSION["cms"];
        $data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
        //--
        $this->load->view('cpanel/header');
        $this->load->view('cpanel/dashBoard',$data);
        $this->load->view('cpanel/menu',$data);
        $this->load->view('modulos/productos/consultar_tproductos');
        $this->load->view('cpanel/footer');
    }
    public function consultarProductosTodas(){
        $res = [];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->Tproductos_model->consultarTproductos($datos);
        foreach ($respuesta as $key => $value) {
            $valor = $value;
            $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";
            $res[] = $valor;
        }
        $listado = (object)$res;
        die(json_encode($listado));
    }
    public function generarSlug($cadena){
        $titulo_min = strtolower($this->normaliza($cadena));
        $slug_noticias = str_replace(" ","-",$titulo_min);
        $slug_noticias = preg_replace("/[^a-zA-Z0-9_-]+/", "", $slug_noticias);
        return $slug_noticias;
    }
    public function normaliza ($cadena){
        $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
        $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
        $originales = utf8_decode($originales);
        $cadena = utf8_decode($cadena);
        //$cadena = utf8_decode($cadena);
        //$cadena = strtr($cadena, utf8_decode($originales), $modificadas);
        $cadena = strtr($cadena, $originales, $modificadas);
        $cadena = strtolower($cadena);
        //return utf8_encode($cadena);
        return $cadena;
    }
    public function modificarProductosEstatus(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $data = array(
          'id' =>$datos['id'],  
          'estatus' => $datos['estatus'],
          'id_productos' =>$datos['id_productos']
        );
        $respuesta = $this->Tproductos_model->modificarProductos($data);

        if($respuesta==true){
            $mensajes["mensaje"] = "modificacion_procesada";
            
        }else{
            $mensajes["mensaje"] = "no_modifico";
        }  
        die(json_encode($mensajes));
    }
    public function tproductosVer(){
        //--- Datos de usuario
        $cms = $_SESSION["cms"];
        $data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
        //--
        $datos["id"] = $this->input->post('id_tproductos');
        $this->load->view('cpanel/header');
        $this->load->view('cpanel/dashBoard',$data);
        $this->load->view('cpanel/menu',$data);
        $this->load->view('modulos/productos/tproductos',$datos);
        $this->load->view('cpanel/footer');
    }
    public function consultar_orden(){
    	
    	$datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->Tproductos_model->consultarOrden($datos);
        
        if(!$respuesta){
        	$listado2["orden"] = array( "orden"=>1 );
        	$listado = (object)$listado2;
        }else{
        	$c = 1;
        	foreach($respuesta as $clave => $valor) {
        		$respuesta2[] = array( "orden"=>$c );
        		$c++;
        	}
        	if($datos["tipo"]=="1")
        		$respuesta2[] = array( "orden"=>$c );
        	//var_dump($c);
        	$listado  = (object)$respuesta2;
        }

        die(json_encode($listado));

    }
   
}    
