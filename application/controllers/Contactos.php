<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Contactos extends CI_Controller{

		function __construct(){
			parent::__construct();
			$this->load->database();
			$this->load->library('session');
			$this->load->model('Contactos_model');
			$cms = $_SESSION["cms"];
			if (!$cms["login"]) {		    
			    redirect(base_url());
			}
		}

		public function index(){
			//--- Datos de usuario
        	$cms = $_SESSION["cms"];
			$data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);

        	//--
			$this->load->view('cpanel/header');
			$this->load->view('cpanel/dashBoard',$data);
        	$this->load->view('cpanel/menu',$data);
			$this->load->view('modulos/contactos/contactos');
			$this->load->view('cpanel/footer');
		}
		/*
		*	Metodo para obtener los datos de los contactos consultados
		*/
		public function consultar_contactos(){
			$respuesta = $this->Contactos_model->consultar_contactos();
			$number = 0;
			foreach ($respuesta as $key => $value) {
				$number = $number+1;
				$mensaje_corto =  substr(strip_tags($value->mensaje),0,150)."...";
	            $valor[] = array(
								"number" => $number,
								"id" => $value->id,
								"nombres" => $value->nombres,
								"telefono" => $value->telefono,
								"email" => $value->email,
								"mensaje" => $value->mensaje,
								"mensaje_corto" => $mensaje_corto,
				);
	        }
	        $listado = (object)$valor;
	        die(json_encode($listado));
		}
	}	