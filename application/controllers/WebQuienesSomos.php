<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class WebQuienesSomos extends CI_Controller
{
    function __construct(){
      parent::__construct();
      $this->load->database();
      $this->load->model('WebInicio_model');
      $this->load->model('WebNosotros_model');
    }
    /*
    * Index
    */
    public function index(){
      $datos["id_idioma"] = 1;
      //---
      $this->load->view('/web/header');
      $this->load->view('/web/menu');
      $this->load->view('/web/quienes_somos',$datos);
      $this->load->view('/web/footer');
    }
    /*
    * consultarNosotros
    */
    public function consultarNosotros(){
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $datos["id_idioma"] = 1;
        $respuesta = $this->WebNosotros_model->consultarNosotros($datos);
        $n = 0;
        $arreglo_nosotros = array(
                                        "titulo_somos"=>"",
                                        "descripcion_somos"=>"",
                                        "img_somos"=>"",
                                        "titulo_valores"=>"",
                                        "descripcion_valores"=>"",
                                        "imagen_valores"=>"",
                                        "titulo_mision"=>"",
                                        "descripcion_mision"=>"",
                                        "imagen_mision"=>"",
                                        "titulo_vision"=>"",
                                        "descripcion_vision"=>"",
                                        "imagen_vision"=>""

                            );  
        //var_dump($respuesta);die;                            
        foreach ($respuesta as $key => $value) {
        //--------------------------------------------------------------------------  
            //--Paso 1: Consultar imagenes
            if (($value->id_imagen!="0")&&($value->id_imagen!="")){
                $imagen = $this->WebNosotros_model->consultarImg($value->id_imagen);
                $value->id_imagen = $imagen[0]->id;
                $value->imagen = $imagen[0]->ruta;
            }
            //--Paso 2 Armar array
            //$sin_html = preg_replace('#<[^>]+>#','', $value->descripcion);
            $sin_html =strip_tags($value->descripcion);
            //$sin_html = $this->agregarEspaciosPuntos($sin_html);
            switch ($n) {
              case 0:
                $arreglo_nosotros["titulo_somos"] = $value->titulo;
                $arreglo_nosotros["descripcion_somos"] = $value->descripcion;
                
                $descripcion_corta = substr($sin_html,0,400)."...";
                $arreglo_nosotros["descripcion_somos_corta"] = $descripcion_corta;
                break;
              case 1:
                $arreglo_nosotros["titulo_valores"] = $value->titulo;
                $arreglo_nosotros["descripcion_valores"] = $value->descripcion;
                (isset($value->imagen))? $arreglo_nosotros["imagen_valores"] = $value->imagen:$arreglo_nosotros["imagen_valores"] ="";

                $descripcion_corta = substr($sin_html,0,227)."...";
                $descripcion_corta = str_replace("COMPROMISO:", "", $descripcion_corta);
                $descripcion_corta = str_replace("RESPETO: Por", "Respeto por ", $descripcion_corta);


                $arreglo_nosotros["descripcion_valores_corta"] = $descripcion_corta;
                break;
              case 2:
                $arreglo_nosotros["titulo_mision"] = $value->titulo;
                $arreglo_nosotros["descripcion_mision"] = $value->descripcion;
                (isset($value->imagen))? $arreglo_nosotros["imagen_mision"] = $value->imagen:$arreglo_nosotros["imagen_mision"] ="";

                $descripcion_corta = substr($sin_html,0,224)."...";
                $arreglo_nosotros["descripcion_mision_corta"] = $descripcion_corta;
               case 3:
                $arreglo_nosotros["titulo_vision"] = $value->titulo;
                $arreglo_nosotros["descripcion_vision"] = $value->descripcion;
                (isset($value->imagen))? $arreglo_nosotros["imagen_vision"] = $value->imagen:$arreglo_nosotros["imagen_vision"] ="";

                $descripcion_corta = substr($sin_html,0,228)."...";
                $arreglo_nosotros["descripcion_vision_corta"] = $descripcion_corta;  
                break;
            }
            $n++;
        //--------------------------------------------------------------------------   
        }
        $listado = (object)$arreglo_nosotros;
        die(json_encode($listado));
    }
    /*
    *   consultarDirectivaTodas
    */ 
    public function consultarDirectiva(){
        $res = [];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->WebNosotros_model->consultarDirectiva($datos);
        $a = 1;
        foreach ($respuesta as $key => $value) {
            $valor = $value;

            //$valor->descripcion_sin_html = strip_tags($value->descripcion);
            $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";

            $res[] = $valor;
            $a++;
        }
        $listado = (object)$res;
        die(json_encode($listado));
    }
    /*
    *   consultaReaseguradoras
    */
    public function consultarReaseguradoras(){
        $res = [];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->WebNosotros_model->consultarReaseguradoras($datos);
        $a = 1;
        foreach ($respuesta as $key => $value) {
            $valor = $value;

            //$valor->descripcion_sin_html = strip_tags($value->descripcion);
            $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";

            $res[] = $valor;
            $a++;
        }
        $listado = (object)$res;
        die(json_encode($listado));
    }
    /*
    *   Agregar Espacios
    */
    public function agregarEspaciosPuntos($texto){
        $nuevo_texto = str_replace(".", ". ", $texto);
        $nuevo_texto = str_replace(",", ". ", $texto);
        return $nuevo_texto;
    }
    /***/

}    