<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Reaseguradoras extends CI_Controller{

		function __construct(){
			parent::__construct();
			$this->load->database();
			$this->load->library('session');
			$this->load->model('Reaseguradoras_model');
			$this->load->model('Auditoria_model');
			$cms = $_SESSION["cms"];
			if (!$cms["login"]) {
						redirect(base_url());
			}
		}

		public function index(){
			//--- Datos de usuario
			$cms = $_SESSION["cms"];
        	$data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
	    	//--
	        $this->load->view('cpanel/header');
	        $this->load->view('cpanel/dashBoard',$data);
	    	$this->load->view('cpanel/menu',$data);
	        $this->load->view('modulos/empresa/reaseguradoras');
	        $this->load->view('cpanel/footer');
	        //var_dump($this->session->userdata("login"));//die();

	    }

		public function registrarReaseguradoras(){
			$datos= json_decode(file_get_contents('php://input'), TRUE);
			$posicionar = array(
              'posicion' => $datos["orden"],
              'tipo' => 'insert',
              'id_idioma' =>  $datos['id_idioma'],
            );
            $this->Reaseguradoras_model->posicionar_modulos($posicionar);

			//print_r($datos);die;
			$data = array(
			  'titulo' => trim(mb_strtoupper($datos['titulo'])),
			  'descripcion' => trim(mb_strtoupper($datos['descripcion'])),
			  'url' => $datos['url'],
			  'id_idioma' => $datos['id_idioma'],
			  'id_imagen' => trim(mb_strtoupper($datos['id_imagen'])),
			  'estatus' => '1',
  			  'orden' =>  $datos["orden"]
			);
			//var_dump($data);
			$respuesta = $this->Reaseguradoras_model->guardarReaseguradoras($data);
			
			if($respuesta==true){
				$mensajes["mensaje"] = "registro_procesado";
				//------------------------------------------------------------
					//--Bloque Auditoria 
					$id = $this->Auditoria_model->consultar_max_id("reaseguradoras");
					$accion = "Registro de reaseguradoras id:".$id.",titulo:".trim($datos['titulo']);
					$cms = $_SESSION["cms"];           
			        $data_auditoria = array(
			                                "id_usuario"=>(integer)$cms["id"],
			                                "modulo"=>'1',
			                                "accion"=>$accion,
			                                "ip"=>$this->Auditoria_model->get_client_ip(),
			                                "fecha_hora"=> date("Y-m-d H:i:00")
					);
			        $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
				//------------------------------------------------------------
			}else{
				$mensajes["mensaje"] = "no_registro";
			}
			die(json_encode($mensajes));
		}
		/*
		*	Modificar reaseguradoras
		*/
		public function modificarReaseguradoras(){
	        $datos = json_decode(file_get_contents('php://input'), TRUE);
	        //var_dump($datos);die('');
	        $posicionar = array(
              'inicial' => $datos["inicial"],
              'tipo' => 'update',
              'id_idioma' =>  $datos['id_idioma'],
              'final' => $datos["orden"]
            );
            //var_dump($posicionar);die('');
	        //-Verifico si existe una noticia con ese titulo....
	        $existe = $this->Reaseguradoras_model->consultarExiste($datos["id"]);
	        if($existe>0){
	            $data = array(
	              'titulo' => trim(mb_strtoupper($datos['titulo'])),
				  'descripcion' => trim(mb_strtoupper($datos['descripcion'])),
				  'url' => $datos['url'],
				  'id_idioma' => $datos['id_idioma'],
				  'id_imagen' => $datos['id_imagen'],
    			  'orden'=>  $posicionar["final"]
	            );
	            $id = $datos['id'];

	            $this->Reaseguradoras_model->posicionar_modulos($posicionar);

	            $respuesta = $this->Reaseguradoras_model->modificarReaseguradoras($data,$datos['id']);
	            if($respuesta==true){
					$mensajes["mensaje"] = "modificacion_procesada";
					//----------------------------------------------------
						//--Bloque Auditoria 
						$accion = "Actualizacion de reaseguradoras id: ".$id;
						$cms = $_SESSION["cms"];                  
				        $data_auditoria = array(
				                                "id_usuario"=>(integer)$cms["id"],
				                                "modulo"=>'1',
				                                "accion"=>$accion,
				                                "ip"=>$this->Auditoria_model->get_client_ip(),
				                                "fecha_hora"=> date("Y-m-d H:i:00")
				        );
				        $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
					//-----------------------------------------------------
	            }else{
	                $mensajes["mensaje"] = "no_registro";
	            }
	        }else{
	             $mensajes["mensaje"] = "existe";
	        }
	        //--
	        die(json_encode($mensajes));
	    }
		/*
		*	Consultar reaseguradoras
		*/

	    public function consultar_reaseguradoras(){
	    	//--- Datos de usuario
	    	$cms = $_SESSION["cms"];
       		$data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
	    	//--
	        $this->load->view('cpanel/header');
	       	$this->load->view('cpanel/dashBoard',$data);
	    	$this->load->view('cpanel/menu',$data);
	        $this->load->view('modulos/empresa/consultar_reaseguradoras');
	        $this->load->view('cpanel/footer');
	        //var_dump("aqui6!");

	    }

	    public function consultarReaseguradorasTodas(){
	        $res = [];
	        $datos= json_decode(file_get_contents('php://input'), TRUE);
	        $respuesta = $this->Reaseguradoras_model->consultarReaseguradoras($datos);
	        $a = 1;
	        foreach ($respuesta as $key => $value) {
				$valor = $value;

	            //$valor->descripcion_sin_html = strip_tags($value->descripcion);
	            $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";

	            $res[] = $valor;
	            $a++;
	        }
	        $listado = (object)$res;
	        die(json_encode($listado));
	    }

	    public function modificarReaseguradorasEstatus(){
	    	$datos= json_decode(file_get_contents('php://input'), TRUE);
	        $data = array(
	          //'id' =>$datos['id'],
	          'estatus' => $datos['estatus'],
	        );
	        $respuesta = $this->Reaseguradoras_model->modificarReaseguradoras($data,$datos["id"]);
	        if($respuesta==true){
				$mensajes["mensaje"] = "modificacion_procesada";
					//----------------------------------------------------
					//--Bloque Auditoria 
					switch ($data["estatus"]) {
						case '0':
							$accion="Inactivar reaseguradoras id: ".$datos['id'];
							break;
						case '1':
							$accion="Activar reaseguradoras id: ".$datos['id'];
							break;
						case '2':
							$accion="Eliminar reaseguradoras id: ".$datos['id'];
							break;
					}
					$cms = $_SESSION["cms"];
			        $data_auditoria = array(
			                                "id_usuario"=>(integer)$cms["id"],
			                                "modulo"=>'1',
			                                "accion"=>$accion,
			                                "ip"=>$this->Auditoria_model->get_client_ip(),
			                                "fecha_hora"=> date("Y-m-d H:i:00")
			        );
			        $respuesta10 = $this->Auditoria_model->guardarAuditoria($data_auditoria);
					//-----------------------------------------------------
	        }else{
	            $mensajes["mensaje"] = "no_modifico";
	        }

	        die(json_encode($mensajes));
		}

		public function reaseguradorasVer(){
			//--- Datos de usuario
	    	$cms = $_SESSION["cms"];
        	$data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
	    	//--
	        $datos["id"] = $this->input->post('id_reaseguradoras');
	        $this->load->view('cpanel/header');
	        $this->load->view('cpanel/dashBoard',$data);
	    	$this->load->view('cpanel/menu',$data);
	        $this->load->view('modulos/empresa/reaseguradoras',$datos);
	        $this->load->view('cpanel/footer');
	    }

	    public function consultar_orden(){
	    	$datos= json_decode(file_get_contents('php://input'), TRUE);
	        $respuesta = $this->Reaseguradoras_model->consultarOrden($datos);
	        //var_dump($respuesta);die('');
	        if(!$respuesta){
	        	$listado2["orden"] = array( "orden"=>1 );
	        	$listado = (object)$listado2;
	        }else{
	        	$c = 1;
	        	//var_dump($respuesta);die('');
	        	foreach($respuesta as $clave => $valor) {
	        		$respuesta2[] = array( "orden"=>$c );
	        		$c++;
	        	}
	        	if($datos["tipo"]=="1")
	        		$respuesta2[] = array( "orden"=>$c );
	        	//var_dump($c);
	        	$listado  = (object)$respuesta2;

	        }
	        die(json_encode($listado));
	    }
	}
?>
