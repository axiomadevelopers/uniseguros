<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Banco extends CI_Controller{

		function __construct(){
			parent::__construct();
			$this->load->database();
			$this->load->library('session');
			$this->load->model('Banco_model');
			$this->load->model('Auditoria_model');
			$cms = $_SESSION["cms"];
			if (!$cms["login"]) {
					redirect(base_url());
			}
		}

		public function index(){
			 //--- Datos de usuario
        	$cms = $_SESSION["cms"];
        	$data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
        	//--
			$this->load->view('cpanel/header');
			$this->load->view('cpanel/dashBoard',$data);
        	$this->load->view('cpanel/menu',$data);
			$this->load->view('modulos/banco/banco');
			$this->load->view('cpanel/footer');
		}

		public function registrarBanco(){
	        $datos= json_decode(file_get_contents('php://input'), TRUE);
	        $data = array(
			  'id' => "",
		      'descripcion' => 	trim(ucwords(mb_strtolower($datos['descripcion']))),
	          'estatus' => '1',
			);

			$existe = $this->Banco_model->consultarExisteBanco($data['id'],$data['descripcion']);

			if(!$existe){

				$respuesta = $this->Banco_model->guardarBanco($data);
				if($respuesta==true){
					$mensajes["mensaje"] = "registro_procesado";
				}else{
					$mensajes["mensaje"] = "no_registro";
				}
			}else{
				$mensajes["mensaje"] = "existe";
	
			}
	
			die(json_encode($mensajes));
		}

	    public function modificarBanco(){
	    	$datos = json_decode(file_get_contents('php://input'), TRUE);
			//-Verifico si existe una noticia con ese titulo....
			$existe_banco = $this->Banco_model->consultarExiste($datos["id"]);

	        if($existe_banco>0){

	            $data = array(
	              'id' =>  $datos['id'],
	              'descripcion' => trim($datos['descripcion']),
	              'estatus' => '1',
	            );
	            $respuesta = $this->Banco_model->modificarBanco($data);
	            if($respuesta==true){
	                $mensajes["mensaje"] = "registro_procesado";
	            }else{
	                $mensajes["mensaje"] = "no_registro";
	            }
	        }else{
	             $mensajes["mensaje"] = "no_existe";
	        }
        	//--
       		 die(json_encode($mensajes));
	    }

	    public function modificarBancoEstatus(){
	    	$datos= json_decode(file_get_contents('php://input'), TRUE);
	        $data = array(
	          'id' =>$datos['id'],  
	          'estatus' => $datos['estatus'],
			);
	        $respuesta = $this->Banco_model->modificarBanco($data);

	        if($respuesta==true){
	            $mensajes["mensaje"] = "modificacion_procesada";
	        }else{
	            $mensajes["mensaje"] = "no_modifico";
	        }  
	        die(json_encode($mensajes));
	    }
	    
	    public function consultarBanco(){
	    	 //--- Datos de usuario
        	$cms = $_SESSION["cms"];
        	$data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
        	//--
	        $this->load->view('cpanel/header');
	        $this->load->view('cpanel/dashBoard',$data);
       		$this->load->view('cpanel/menu',$data);
	        $this->load->view('modulos/banco/consultar_banco');
	        $this->load->view('cpanel/footer');
	    }
	    
	    public function consultarBancoTodas(){
	    	$res = [];
	        $datos= json_decode(file_get_contents('php://input'), TRUE);
			$respuesta = $this->Banco_model->consultarBanco($datos);
	        foreach ($respuesta as $key => $value) {
	            $valor = $value;
	            //$valor->descripcion_sin_html = strip_tags($value->descripcion);
	            $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";
	            $res[] = $valor;
	        }
	        $listado = (object)$res;
	        die(json_encode($listado));
	    }

	    public function bancoVer(){
	    	//--- Datos de usuario
        	$cms = $_SESSION["cms"];
        	$data = array("login"=>strtoupper($cms["login"]),"nombre_persona"=>$cms["nombre_persona"],"tipo_usuario"=>$cms["tipo_usuario"],"ruta_imagen"=>$cms["ruta_imagen"]);
       	 	//--
	        $datos["id"] = $this->input->post('id_banco');
	        $this->load->view('cpanel/header');
	        $this->load->view('cpanel/dashBoard',$data);
        	$this->load->view('cpanel/menu',$data);
	        $this->load->view('modulos/banco/banco',$datos);
	        $this->load->view('cpanel/footer');
	    }

	}	