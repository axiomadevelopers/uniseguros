<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class WebInicio extends CI_Controller
{
    function __construct(){
      parent::__construct();
      $this->load->database();
      $this->load->model('WebInicio_model');
      $this->load->model('WebNosotros_model');
      $this->load->model('WebServicios_model');
    }
    /*
    * Index
    */
    public function index(){
      $idioma = 1;
      $datos["slider"] = $this->consultarSliderPhp($idioma);
      //---
      $this->load->view('/web/header');
      $this->load->view('/web/menu');
      $this->load->view('/web/inicio',$datos);
      $this->load->view('/web/footer');
    }
    /*
    * Consulta Slider PHP
    */
    public function consultarSliderPhp($id_idioma=1){
        $res = [];
        $datos["id_idioma"] = $id_idioma;
        $respuesta = $this->WebInicio_model->consultarSlider($datos);
        $numero = 1;
        $numero2 = 0;
        foreach ($respuesta as $key => $value) {
            $valor = $value;
            
            //$valor->direccion = $value->direccion;
            $valor->url = strtolower($valor->url);
            $valor->num = $numero2;
            switch ($numero) {
                case 1:
                    $valor->clase_titulo = "";
                    break;
                case 2:
                    $valor->clase_titulo = "titulo2";
                    break;
                case 3:
                $valor->clase_titulo = "titulo1";
                break;
                default:
                    $valor->clase_titulo = "";
                    break;
            }

            $numero++;
            $numero2++;
            //$valor->descripcion_sin_html = strip_tags($value->descripcion);
            $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,150)."...";
            $res[] = $valor;
        }
        $listado = (object)$res;
        return $listado;
    }
   
    /*
    * registrarContactos
    */
    public function registrarContactos(){
        $datos = json_decode(file_get_contents('php://input'), TRUE);
        $data = array(
            "nombres" => $datos["nombres"],
            "telefono" => $datos["telefono"],
            "email" => $datos["email"],
            "mensaje" => $datos["mensaje"]
        );
        $respuesta = $this->WebInicio_model->guardarContactos($data);
        if($respuesta==true){
            $mensajes["mensaje"] = "registro_procesado";
        }else{
            $mensajes["mensaje"] = "no_registro";
        }
        die(json_encode($mensajes));
    }
    public function consultarProductosTodas(){
        $res = [];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $respuesta = $this->WebServicios_model->consultarProductos($datos);
        foreach ($respuesta as $key => $value) {
    
            $valor = $value;
            $valor->descripcion_sin_html = substr(strip_tags($value->descripcion),0,130)."...";
    
            $res[] = $valor;
        }
        $listado = (object)$res;
    
        die(json_encode($listado));
     }
     public function consultarUniServiciosTodas(){
        $res = [];
        $datos= json_decode(file_get_contents('php://input'), TRUE);
        $datos["slug_primario"] = "que-hacer-en-caso-de-siniestros";
        //Buscar id con el slug entrante
        $servicio = $this->WebServicios_model->consultarServicioSlug($datos);

        $respuesta = $this->WebServicios_model->consultarTservicios($servicio['id']);
  
        foreach ($respuesta as $key => $value) {
        $descripcion = $this->WebServicios_model->consultarDescripcion($value->id,$value->id_servicios);
  
            $valor = $value;
            $valor->descripcion_sin_html = substr(strip_tags($descripcion['descripcion']),0,130)."...";
  
            $res[] = $valor;
        }
        $listado['tservicios'] = (object)$res;
        $listado['titulo'] = $servicio['titulo'];
        $listado['slug'] = $servicio['slug'];
        $listado['ruta_servicio'] = $servicio['ruta'];
  
        die(json_encode($listado));
     }

    public function error404(){
        $idioma = 1;
        //---
        $datos="";
        $this->load->view('/web/header');
        $this->load->view('/web/menu');
        $this->load->view('/web/error404',$datos);
        $this->load->view('/web/footer');
    }
}    