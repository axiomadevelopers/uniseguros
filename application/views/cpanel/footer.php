
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
 </body>
 <!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
<script src="<?=base_url();?>assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="<?=base_url();?>assets/plugins/popper/popper.min.js"></script>
<script src="<?=base_url();?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?=base_url();?>assets/plugins/moment/moment.js"></script>
<script src="<?=base_url();?>assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
<script src="<?=base_url();?>assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>

<!-- slimscrollbar scrollbar JavaScript -->
<script src="<?=base_url();?>assets/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?=base_url();?>assets/js/waves.js"></script>
<!--Menu sidebar -->
<script src="<?=base_url();?>assets/js/sidebarmenu.js"></script>
<!--stickey kit -->
<script src="<?=base_url();?>assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
<script src="<?=base_url();?>assets/plugins/sparkline/jquery.sparkline.min.js"></script>
<!--Custom JavaScript -->
<!-- <script src="<?=base_url();?>assets/js/custom.min.js"></script> -->
<script src="<?=base_url();?>assets/plugins/toast-master/js/jquery.toast.js"></script>
<script src="<?=base_url();?>assets/js/toastr.js"></script>
<!-- ============================================================== -->
<!-- This page plugins -->
    <!-- ============================================================== -->
    <script src="<?=base_url();?>/assets/plugins/switchery/dist/switchery.min.js"></script>
    <script src="<?=base_url();?>/assets/plugins/select2/dist/js/select2.full.min.js" type="text/javascript"></script>
    <script src="<?=base_url();?>/assets/plugins/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
    <script src="<?=base_url();?>/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
    <script src="<?=base_url();?>/assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js" type="text/javascript"></script>
    <script src="<?=base_url();?>/assets/plugins/dff/dff.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?=base_url();?>/assets/plugins/multiselect/js/jquery.multi-select.js"></script>

<script src="<?=base_url();?>assets/plugins/html5-editor/wysihtml5-0.3.0.js"></script>
<script src="<?=base_url();?>assets/plugins/html5-editor/bootstrap-wysihtml5.js"></script>
<script>
    $(document).ready(function() {

        $('.textarea_editor').wysihtml5();


    });

    $("input[name='precio']").TouchSpin({
    	   min: 0,
    	   max: 100000000000000,
    	   step: 0.1,
    	   decimals: 2,
    	   boostat: 5,
    	   maxboostedstep: 10,
    	   prefix: '$'
       });
</script>
<!-- This page plugins -->
<!-- ============================================================== -->
<!-- chartist chart -->
<!--
<script src="<?=base_url();?>assets/plugins/chartist-js/dist/chartist.min.js"></script>
<script src="<?=base_url();?>assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js"></script>
-->
<!--c3 JavaScript -->
<script src="<?=base_url();?>assets/plugins/d3/d3.min.js"></script>
<script src="<?=base_url();?>assets/plugins/c3-master/c3.min.js"></script>
<!-- Chart JS -->
<!--
<script src="<?=base_url();?>assets/js/dashboard1.js"></script>
-->
<!-- This is data table -->
<script src="<?=base_url();?>assets/plugins/datatables/datatables.min.js"></script>
<!-- start - This is for export functionality only -->
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<!-- ============================================================== -->
<!-- Sweet-Alert  -->
<script src="<?=base_url();?>assets/plugins/sweetalert/sweetalert.min.js"></script>
<script src="<?=base_url();?>assets/plugins/sweetalert/jquery.sweet-alert.custom.js"></script>
<!-- Dropzone Plugin JavaScript -->
<script src="<?=base_url();?>assets/plugins/dropzone-master/dist/dropzone.js"></script>
<!-- ============================================================== -->
<!-- Style switcher -->
<!-- ============================================================== -->
<script src="<?=base_url();?>assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
<script src="<?=base_url();?>assets/js/fbasic.js"></script>
<!-- CORE ANGULAR SCRIPTS -->
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/angular.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/angular-route.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/app.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/angular-sanitize.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/directivas/directives.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/servicios/services.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/mainController.js"></script>


<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/categoriasController.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/categoriasConsultasController.js"></script>

<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/GaleriaMultimediaController.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/GaleriaMultimediaConsultasController.js"></script>

<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/PdfMultimediaController.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/PdfMultimediaConsultasController.js"></script>

<!--SERVICIOS-->
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/serviciosController.js"></script>

<!--PRODUCTOS-->
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/RegistroCMS_Controller.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/registroCMSConsultasController.js"></script>

<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/noticiasController.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/noticiasConsultasController.js"></script>

<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/RedesSocialesController.js"></script>

<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/ContactosController.js"></script>

<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/footerController.js"></script>

<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/footerConsultasController.js"></script>

<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/bancoController.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/bancoConsultasController.js"></script>

<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/cuentaBancariaController.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/cuentaBancariaConsultasController.js"></script>

<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/quienesSomosController.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/quienesSomosConsultaController.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/DirectivaController.js"></script>


<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/SliderController.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/sliderConsultasController.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/descripcionController.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/palabrasClavesController.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/auditoriaController.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/palabrasClavesController.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/PortafolioController.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/portafolioConsultaController.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/DirectivaController.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/DirectivaConsultasController.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/ReaseguradorasController.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/ReaseguradorasConsultasController.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/DireccionController.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/DireccionConsultaController.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/productosController.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/productosConsultaController.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/tproductosController.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/tproductosConsultaController.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/DetalleTtproductosController.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/DetalleTproductosConsultaController.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/serviciosController.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/serviciosConsultaController.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/tserviciosController.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/tserviciosConsultaController.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/DetalleTserviciosController.js"></script>
<script type="text/javascript" src="<?=base_url();?>assets/js/angular/controladores/DetalleTserviciosConsultaController.js"></script>
<!-- -->
<script src="<?=base_url();?>assets/js/custom.min.js"></script>

</html>
