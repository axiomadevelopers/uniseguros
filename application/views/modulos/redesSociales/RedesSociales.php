<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid" ng-controller="RedesSocialesController">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center titulo-dashboard">
                <h3 class="text-themecolor">Contactos</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">Redes Sociales</li>
                </ol>
            </div>
            <!--<div class="col-md-7 col-4 align-self-center">
                <div class="d-flex m-t-10 justify-content-end">
                    <div class="">
                        <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i
                                class="ti-settings text-white"></i></button>
                    </div>
                </div>
            </div>-->
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== --> 
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">{{titulo_pagina}}</h4>
                        <h6 class="card-subtitle">{{subtitulo_pagina}}</h6>
                        <form class="form-material m-t-40" name="formCategorias">
                        	<!-- -->
                        	<div class="row">
                    			<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 padding0_min">
                                    <div class="form-group">
										<div class='asterisco_rojo'><i class='fa fa-asterisk' aria-hidden='true'></i></div>
                                    	<label class="">Red social:</label>
	                                    <select class="form-control custom-select" id="redes" name="redes" ng-options="option.descripcion for option in tipo_red track by option.id" ng-model="id_tipo_red" ng-change="consultarRedesUrl()">
	                                    	<option value="">--Seleccione una red--</option>
	                                    </select>
                                    </div>    
								</div>
                        		<div class="col-lg-6">
                        			<div class="form-group">
		                                <div class='asterisco_rojo'><i class='fa fa-asterisk' aria-hidden='true'></i></div>
		                                <label>Url <span class="help"></span></label>
		                                <input name="url_red" id="url_red" type="text" class="form-control form-control-line" placeholder="Ingrese la url de la re social" ng-model="redes_sociales.url_red" required> 
		                            </div>
                        		</div>
                        	</div>
                           <!-- -->
                            <div class="row button-group">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div id="div_mensaje"></div>
                                    </div>
                                </div>    
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div id="btn-nuevo" class="col-lg-4 col-md-4" ng-if="id_categoria!=''">
                                            <a href="<?=base_url();?>cms/categorias">
                                                <button type="button" class="btn waves-effect waves-light btn-block btn-success">Nuevo</button>
                                            </a>
                                        </div>
                                        <div id="btn-limpiar" class="col-lg-4 col-md-4" ng-if="id_categoria==''">
                                            <button type="button" class="btn waves-effect waves-light btn-block btn-success" ng-click="limpiar_cajas_categorias()">Limpiar</button>
                                        </div>
                                        
                                        <!--<div class="col-lg-4 col-md-4">
                                            <a href="<?=base_url();?>cms/categorias/consultarCategorias">
                                                <button type="button" class="btn waves-effect waves-light btn-block btn-danger" >Consultar</button>
                                            </a>
                                        </div>-->
                                        <div class="col-lg-4 col-md-4">
                                                <button id="btn-registrar" type="button" class="btn waves-effect waves-light btn-block btn-info" ng-click="registrarRedes()">{{titulo_registrar}}</button>
                                        </div>
                                        <input type="hidden" name="id_categoria" id="id_categoria" value="<?php if(isset($id)){echo $id;}?>">
                                        <input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>                  
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
</div>        
