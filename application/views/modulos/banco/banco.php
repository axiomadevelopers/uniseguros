<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid" ng-controller="bancoController">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center titulo-dashboard">
                <h3 class="text-themecolor">Banco</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">Contactos</li>
                    <li class="breadcrumb-item active">Banco</li>
                </ol>
            </div>
            <!--<div class="col-md-7 col-4 align-self-center">
                <div class="d-flex m-t-10 justify-content-end">
                    <div class="">
                        <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i
                                class="ti-settings text-white"></i></button>
                    </div>
                </div>
            </div>-->
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form class="form-material m-t-40" name="formNoticias">
                            <h4 class="card-title">{{titulo_pagina}}</h4>
                            <h6 class="card-subtitle">{{subtitulo_pagina}}</h6>
                           
                           
                            <div class="row">
                                <!--
                                <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 padding0_min " style="padding-top: 5px; ">
                                    <div class="form-group">
                                        <div class='asterisco_rojo'><i class='fa fa-asterisk' aria-hidden='true'></i></div>
                                        <label class="">Idioma:</label>
                                        <select name="idioma" id="idioma" class="form-control m-bot15 select-picker" data-done-button="true" data-actions-box="true"  data-live-search-placeholder="Seleccione tipo de idioma" placholder="Seleccione tipo de idioma" data-style="btn-fff " data-live-search="true" ng-options="option.descripcion for option in idioma track by option.id" ng-model="banco.id_idioma" data-size="3">
                                            <option value="">--Seleccione un idioma--</option>
                                        </select>
                                    </div>    
                                </div>
                                 -->
                                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                    <div class="form-group">
                                        <div class='asterisco_rojo'><i class='fa fa-asterisk' aria-hidden='true'></i></div>
                                        <label class="">Banco:</label>
                                            <div class="controls">
                                                <input type="text" name="banco" id="banco" class="form-control form-control-line" ng-model="banco.descripcion" placeholder="Ingrese la dirección de banco" onKeyPress="return valida(event,this,18,100)" onBlur="valida2(this,18,100)">
                                            </div>
                                    </div>
                                </div>
                               
                                <!-- -->
                            </div>
                                
                            <div class="row button-group">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div id="div_mensaje"></div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4" ng-if="id_banco!=''">
                                            <a href="<?=base_url();?>cms/banco">
                                                <button id="btn-nuevo" type="button" class="btn waves-effect waves-light btn-block btn-success">Nuevo</button>
                                            </a>
                                        </div>

                                        <div class="col-lg-4 col-md-4" ng-if="id_banco==''">
                                            <button id="btn-limpiar" type="button" class="btn waves-effect waves-light btn-block btn-success" ng-click="limpiar_cajas_banco()" >Limpiar</button>
                                        </div>
                                        
                                        <div class="col-lg-4 col-md-4">
                                            <a href="<?=base_url();?>cms/banco/consultarBanco">
                                                <button id="btn-consultar" type="button" class="btn waves-effect waves-light btn-block btn-danger" >Consultar</button>
                                            </a>
                                        </div>
                                        <div class="col-lg-4 col-md-4">
                                                <button id="btn-registrar" type="button" class="btn waves-effect waves-light btn-block btn-info" ng-click="registrarBanco()">{{titulo_registrar}}</button>
                                        </div>
                                        <input type="hidden" name="id_banco" id="id_banco" value="<?php if(isset($id)){echo $id;}?>">
                                        <input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <div class="modal fade" id="wisiModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="exampleModalLabel1">Ingrese el contenido</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="card-body">
                                <form method="post">
                                    <div class="form-group">
                                        <textarea class="textarea_editor form-control text_editor" rows="15" placeholder="Ingrese texto..." id="text_editor"></textarea>
                                    </div>
                                </form>
                            </div>
                        </form>
                    </div>
                    <div class="modal-banco">
                        <button id="cerrarModal" type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-primary" ng-click="agregarWisi()">Agregar</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!--Bloque modal del sistema -->
        <div class="modal fade" id="modal_img1" name="modal_img1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-lg .modal-sm">
              <div class="modal-content">
                <div class="modal-header header_conf">
                    <button type="button" id="cerrar_mensaje2" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <p id="cabecera_mensaje" name="cabecera_mensaje"> Seleccione una imagen</p>
                </div>
                <div class="modal-body" id="cuerpo_mensaje" name="cuerpo_mensaje">
                    <!--Modal body -->
                    <div class="centrar_galeria" ng-show="galery.length ==0">Galería de imágenes noticias</div>
                    <div id="" ng-show="galery.length!=0" class="fade-in-out">
                        <div>
                            <div class="form-group">
                                <input type='text' name='filtro_noticias' id='filtro_noticias' placeholder='Ingrese el valor a filtrar' class="form-control" ng-model="searchNoticias">
                            </div>
                            <div class="col-lg-12" padding="0px;">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 divbiblioteca" ng-repeat="imagen in galery|filter:searchNoticias track by $index">
                                        <img class="imgbiblioteca"  id="img_biblioteca{{$index}}" ng-src="{{base_url}}/{{imagen.ruta}}" height="115" data="{{imagen.id}}|{{imagen.ruta}}" data-ng-click="seleccionar_imagen($event)">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <!-- modal body-->
                </div>
                <div class="modal-banco banco_conf">
                    <!-- Footter del modal -->
                      <button type="button" name="modal_reporte_salir" id="modal_reporte_salir" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                    <!-- Fin footter del modal -->
                </div>
                <div style="clear:both"></div>
              </div>
            </div>
        </div>
        <!-- ============================================================== -->
    </div>
</div>
