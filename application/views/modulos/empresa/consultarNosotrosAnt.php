<div class="page-wrapper">
    <div class="container-fluid" ng-controller="nosotrosConsultaController">
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center titulo-dashboard">
                <h3 class="text-themecolor">Nosotros</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">Empresa</li>
                     <li class="breadcrumb-item active">Nosotros</li>
                     <li class="breadcrumb-item active">Consultar</li>
                </ol>
            </div>
            <!--<div class="col-md-7 col-4 align-self-center">
                <div class="d-flex m-t-10 justify-content-end">
                    <div class="">
                        <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i
                                class="ti-settings text-white"></i></button>
                    </div>
                </div>
            </div>-->
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">{{titulo_pagina}}</h4>
                        <div class="table-responsive m-t-40">
                            <form id="formConsultaNosotros" method="POST" target="_self">
                                <input type="hidden" name="id_nosotros" id="id_nosotros" value="<?php if(isset($id)){echo $id;}?>">
                                <input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                                <div class="col-lg-12 mg-b50">
                                    <a href="{{base_url}}cms/nosotros">
                                        <div class="btn btn-success btn-new" >Nuevo</div>
                                    </a>
                                </div>
                                <table id="myTable" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Acciones</th>
                                            <th>Idioma</th>
                                            <th>Somos</th>
                                            <th>Profesionalismo</th>
                                            <th>Calidad</th>
                                            <th>Creatividad</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat = "nos in nosotros track by $index">
                                            <td class="">{{nos.id}}</td>
                                            <td class="centrado">
                                                <div class="form-group flotar_izquierda">
                                                    <div id="ver{{$index}}" ng-click="ver_nosotros($index)" data="{{nos.id}}" class="btn btn-primary flotar_izquierda2" title="Modificar">
                                                        <i class="fas fa-pencil-alt" aria-hidden="true"></i>
                                                    </div>
                                                    <div id="btn_activar{{$index}}_a" name="btn_activar{{$index}}_a" class="btn btn-warning flotar_izquierda2" ng-class="{visible:nos.estatus=='1',invisible:nos.estatus!=1}" title="Inactivar" data-ng-click="activar_registro($event)" data="{{nos.id}}|{{nos.estatus}}">
                                                    <i class="fa fa-lock" aria-hidden="true"></i>
                                                    </div>
                                                    <div id="btn_activar{{$index}}_b" name="btn_activar{{$index}}_b"  class="btn btn-success flotar_izquierda2" ng-class="{visible:nos.estatus!='1',invisible:nos.estatus==1}" title="Publicar" data-ng-click="activar_registro($event)" data="{{nos.id}}|{{nos.estatus}}">
                                                    <i class="fa fa-check" aria-hidden="true"></i>
                                                    </div>
                                                    <div id="btn_eliminar{{$index}}" name="btn_eliminar{{$index}}"  class="btn btn-danger flotar_izquierda2" title="Eliminar" data-ng-click="eliminar_registro($event)" data="{{nos.id}}|2">
                                                    <i class="fa fa-window-close" aria-hidden="true"></i>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="">{{nos.descripcion_idioma}}</td>
                                            <td class="">{{nos.somos1}}</td>
                                            <td class="">{{nos.profesionalismo1}}</td>
                                            <td class="">{{nos.calidad1}}</td>
                                            <td class="">{{nos.creatividad1}}</td>
                                            <td class="">{{nos.calidad1}}</td>
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
