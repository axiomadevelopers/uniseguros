<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid" ng-controller="quienesSomosController">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center titulo-dashboard">
                <h3 class="text-themecolor">Quienes Somos</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">Quienes Somos</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form class="form-material m-t-40" name="formCategorias" id="formCategorias">
                            <h4 class="card-title">{{titulo_pagina}}</h4>
                            <h6 class="card-subtitle">{{subtitulo_pagina}}</h6>
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs customtab2" role="tablist">
                                <li class="nav-item" ng-class="{'active': currentTab === 'datos_basicos'}" ng-click="currentTab = 'datos_basicos';">
                                    <a class="nav-link active" data-toggle="tab" href="ui-tab.html#home7" role="tab">
                                        <span class="hidden-sm-up">
                                            <i class="ti-pencil-alt"></i>
                                        </span>
                                        <span class="hidden-xs-down">Datos Básicos</span>
                                    </a>
                                </li>
                                <li class="nav-item" ng-class="{'active': currentTab === 'imagenes'}" ng-click="currentTab = 'imagenes'">
                                    <a class="nav-link" data-toggle="tab" href="ui-tab.html#profile7" role="tab">
                                        <span class="hidden-sm-up">
                                            <i class="ti-image"></i>
                                        </span>
                                        <span class="hidden-xs-down">Imagenes</span></a>
                                </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div id="datos_basicos" class="tab-pane active"  role="tabpanel" ng-class="{'active':currentTab === 'datos_basicos'}">
                                    <div class="form-group">
                                        <div class="row p-20">
                                            <div class="col-lg-6 col-md-6 col-xs-12 col-sm-6     padding0_min " style="padding-top: 5px; ">
                                                <div class='asterisco_rojo'><i class='fa fa-asterisk' aria-hidden='true'></i></div>
                                                <label class="">Idioma:</label>
                                                <select name="idioma" id="idioma" class="form-control m-bot15 select-picker" data-done-button="true" data-actions-box="true"  data-live-search-placeholder="Seleccione tipo de idioma" placholder="Seleccione tipo de idioma" data-style="btn-fff " data-live-search="true" ng-options="option.descripcion for option in idioma track by option.id" ng-model="nosotros.id_idioma" ng-change="cargarOrden()"data-size="3">
                                                    <option value="">--Seleccione un idioma--</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 ptop-20-570">
                                                <label class="">Seleccione un icono:</label>
                                                <div type="button" class="btn waves-effect waves-light btn-info" ng-click="icon_modal()">Ver </div>
                                                <div class="iconoSeleccionado" ng-bind-html="lista_iconos"></div>
                                            </div>
                                        </div>
        								<div class="row p-20">
        	                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 ">
                                                <div class='asterisco_rojo'><i class='fa fa-asterisk' aria-hidden='true'></i></div>
        	                                    <label>Título: <span class="help"></span></label>
        	                                    <input name="titulo" id="titulo" type="text" class="form-control form-control-line" placeholder="Ingrese el título"  ng-model="nosotros.titulo"  required>
        	                                </div>

                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 ptop-20-570">
                                                <div class='asterisco_rojo'><i class='fa fa-asterisk' aria-hidden='true'></i></div>
        										<label>Descripción:</label>
        										<div class="div_wisig" id="div_descripcion" name="div_descripcion" data-toggle="modal" data-target="#wisiModal" data-whatever="@mdo" ng-click="wisi_modal('1')">
        											Pulse aquí para ingresar el contenido
        										</div>
        	                                </div>
                                            <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 padding0_min " style="padding-top: 5px; ">
                                                        <div class='asterisco_rojo'><i class='fa fa-asterisk' aria-hidden='true'></i></div>
                                                        <label class="">Orden:</label>
                                                        <select name="orden" id="orden" class="form-control m-bot15 select-picker" data-done-button="true" data-actions-box="true"  data-live-search-placeholder="Seleccione el orden del servicio" placholder="Seleccione el orden del servicio" data-style="btn-fff " data-live-search="true"  ng-model="slider.orden" data-size="3">
                                                            <option value="">--Seleccione el orden--</option>
                                                        </select>
                                                        <!-- 
                                                        ng-options="option.orden for option in ordenes track by option.orden"
                                                        --> 
                                                    </div>
        	                            </div>
                                    </div>
                                </div>
                                <!--Tab de imagenes -->
                                <div class="tab-pane p-20" id="imagenes" role="tabpanel" ng-class="{'active':currentTab === 'imagenes'}">
                                        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                            <div class="img_principal"  ng-click="seleccione_img_principal()">
                                                <div ng-class="{'invisible':activo_img==='activo','visible':activo_img==='inactivo'}">
                                                    <span>
                                                        <i class="fas fa-image" aria-hidden="true"></i>
                                                        Seleccione imagen
                                                    </span>
                                                </div>
                                                <div class="mensaje_img_principal" ng-class="{'visible':activo_img==='activo','invisible':activo_img==='inactivo'}">
                                                        <img class="img_noticias" id="img_seleccionada" ng-src="{{base_url}}{{nosotros.imagen}}" height="115" data="">
                                                </div>
                                            </div>
                                        </div>
                                </div>
                                <!-- -->
                            </div>
                            <!--Cuerpo de botones -->
                            <div class="row button-group">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div id="div_mensaje"></div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4" ng-if="id_nosotros!=''">
                                            <a href="<?=base_url();?>cms/quienes_somos">
                                                <button id="btn-nuevo" type="button" class="btn waves-effect waves-light btn-block btn-success">Nuevo</button>
                                            </a>
                                        </div>
                                        <div class="col-lg-4 col-md-4" ng-if="id_nosotros==''">
                                            <button id="btn-limpiar" type="button" class="btn waves-effect waves-light btn-block btn-success" ng-click="limpiar_cajas_quienes_somos()">Limpiar</button>
                                        </div>

                                        <div class="col-lg-4 col-md-4">
                                            <a href="<?=base_url();?>cms/quienes_somos/consultar_quienes_somos">
                                                <button id="btn-consultar" type="button" class="btn waves-effect waves-light btn-block btn-danger" >Consultar</button>
                                            </a>
                                        </div>
                                        <div class="col-lg-4 col-md-4">
                                                <button id="btn-registrar" type="button" class="btn waves-effect waves-light btn-block btn-info" ng-click="registrarCategoriasProd()">{{titulo_registrar}}</button>
                                        </div>
                                        <input type="hidden" name="id_nosotros" id="id_nosotros" value="<?php if(isset($id_nosotros)){echo $id_nosotros;}?>">
                                        <input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                                    </div>
                                </div>
                            </div>
                            <!-- -->
                        </form>
                    </div>
                </div>
            </div>
        </div>

		<div class="modal fade" id="wisiModal" name="wisiModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="exampleModalLabel1">Ingrese el contenido</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="card-body">
                                <form method="post">
                                    <div class="form-group">
											<textarea id="textarea_editor" name="textarea_editor" class="textarea_editor form-control" rows="15" placeholder="Ingrese texto..."></textarea>
                                    </div>
                                </form>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button id="cerrarModal" type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-primary" ng-click="agregarWisi()">Agregar</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Icon modal -->
        <div class="modal fade" id="iconModal" name="iconModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="exampleModalLabel1">Seleccione un icono</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="card-body card-body-modal-icon">
                                <!-- -->
                                <div class="row inner-top">
                                    <div class="col-xs-12 text-center aos-init aos-animate">
                                        <ul  class="font-icons">
                                            <span id="super_icono{{$index}}" name="super_icono{{$index}}"  ng-repeat="icono in iconos track by $index" data-ng-click="seleccionarIcono($index)" data="{{icono.icono}}">
                                                <li class="iconos li-iconos" ng-bind-html="icono.icono"  
                                            ></li>
                                            </span>
                                        </ul>
                                    </div>
                                </div>
                                <!-- -->
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button id="cerrarModal" type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-primary" ng-click="agregarIcon()">Agregar</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
         <div class="modal fade" id="modal_img1" name="modal_img1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-lg .modal-sm">
              <div class="modal-content">
                <div class="modal-header header_conf">
                    <p id="cabecera_mensaje" name="cabecera_mensaje"> Seleccione una imagen</p>
                    <button type="button" id="cerrar_mensaje2" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body" id="cuerpo_mensaje" name="cuerpo_mensaje">
                    <!--Modal body -->
                    <div class="centrar_galeria" ng-show="galery.length ==0">Galería de imágenes Quienes Somos</div>
                    <div id="" ng-show="galery.length!=0" class="fade-in-out">
                        <div>
                            <div class="form-group">
                                <input type='text' name='filtro_noticias' id='filtro_noticias' placeholder='Ingrese el valor a filtrar' class="form-control" ng-model="searchNoticias">
                            </div>
                            <div class="col-lg-12" padding="0px;">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 divbiblioteca" ng-repeat="imagen in galery|filter:searchNoticias track by $index">
                                        <img class="imgbiblioteca"  id="img_biblioteca{{$index}}" ng-src="{{base_url}}/{{imagen.ruta}}" height="115" data="{{imagen.id}}|{{imagen.ruta}}" data-ng-click="seleccionar_imagen($event)">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <!-- modal body-->
                </div>
                <div class="modal-footer footer_conf">
                    <!-- Footter del modal -->
                      <button type="button" name="modal_reporte_salir" id="modal_reporte_salir" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                    <!-- Fin footter del modal -->
                </div>
                <div style="clear:both"></div>
              </div>
            </div>
        </div>
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
</div>
