<div class="page-wrapper" >
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid" ng-controller="auditoriaController">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center titulo-dashboard">
                <h3 class="text-themecolor">Configuración</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">Configuración</li>
                    <li class="breadcrumb-item active">Auditoría</li>
                </ol>
            </div>
            <!--<div class="col-md-7 col-4 align-self-center">
                <div class="d-flex m-t-10 justify-content-end">
                    <div class="">
                        <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i
                                class="ti-settings text-white"></i></button>
                    </div>
                </div>
            </div>-->
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">{{titulo_pagina}}</h4>
                        <div class="table-responsive m-t-40">
                            <form id="formConsultaNoticias" method="POST" target="_self">
                                <input type="hidden" id="id_noticias" name="id_noticias" ng-model="id_noticias" >
                                 <input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                                <div class="col-lg-12 mg-b30">
                                     
                                </div> 
                                <table id="myTable" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Usuario</th>
                                            <th>Modulo</th>
                                            <th style="width:300px;">Acción</th>
                                            <th>IP</th>
                                            <th>Fecha-Hora</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat = "audi in auditoria track by $index">
                                            <td class="">{{audi.number}}</td>
                                            <td class="">{{audi.nombre_usuario}}</td>
                                            <td class="justificado" width="20%"><div>{{audi.modulo}}</div></td>
                                            <td class="">{{audi.accion}}</td>
                                            <td class="">{{audi.ip}}</td>
                                            <td class="">{{audi.fecha_hora}}</td>
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
      
    </div>
</div>