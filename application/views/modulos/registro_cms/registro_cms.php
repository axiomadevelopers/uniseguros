<div class="page-wrapper">
	<div class="container-fluid" ng-controller="RegistroCMS_Controller">
		<div class="row page-titles">
			<div class="col-md-5 col-8 align-self-center titulo-dashboard">
				<h3 class="text-themecolor">Configuración</h3>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
					<li class="breadcrumb-item active">Configuración</li>
					<li class="breadcrumb-item active">Registro Usuarios CMS</li>
				</ol>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">
						<form class="form-material m-t-40" name="formularioMarca" id="formularioMarca">
							<h4 class="card-title">{{titulo_pagina}}</h4>
							<h6 class="card-subtitle">{{subtitulo_pagina}}</h6>
							<!-- Nav tabs -->
	                        <ul class="nav nav-tabs customtab2" role="tablist">
	                            <li class="nav-item" ng-class="{'active': currentTab === 'datos_basicos'}" ng-click="currentTab = 'datos_basicos';">
	                                <a class="nav-link active" data-toggle="tab" href="ui-tab.html#home7" role="tab">
	                                    <span class="hidden-sm-up">
	                                        <i class="ti-pencil-alt"></i>
	                                    </span>
	                                    <span class="hidden-xs-down">Datos Básicos</span>
	                                </a>
	                            </li>
	                            <li class="nav-item" ng-class="{'active': currentTab === 'imagenes'}" ng-click="currentTab = 'imagenes'">
	                                <a class="nav-link" data-toggle="tab" href="ui-tab.html#profile7" role="tab">
	                                    <span class="hidden-sm-up">
	                                        <i class="ti-image"></i>
	                                    </span>
	                                    <span class="hidden-xs-down">Imagenes</span></a>
	                            </li>
	                        </ul>
	                        <!-- -->
							<!-- Tab panes -->
                            <div class="tab-content">
                            	<!--Datos basicos -->
                                <div id="datos_basicos" class="tab-pane active"  role="tabpanel" ng-class="{'active':currentTab === 'datos_basicos'}">
									<div class="row p-20">
										<div class="col-lg-4 col-md-6 col-xs-12 col-sm-12">
											<div class="form-group">
												<div class='asterisco_rojo'><i class='fa fa-asterisk' aria-hidden='true'></i>
												</div>
												<label class="">Documento de identidad:</label>
												<div class="controls">
													<input type="text" name="cedula" id="cedula"
														class="form-control form-control-line" ng-model="persona.cedula"
														placeholder="Ingrese Documento de identidad"
														onKeyPress="return valida(event,this,10,100)"
														onBlur="valida2(this,10,100);">
												</div>
											</div>
										</div>
										<div class="col-lg-4 col-md-6 col-xs-12 col-sm-12">
											<div class="form-group">
												<div class='asterisco_rojo'><i class='fa fa-asterisk' aria-hidden='true'></i>
												</div>
												<label class="">Login de Usuario:</label>
												<div class="controls">
													<input type="text" name="usuario" id="usuario"
														class="form-control form-control-line" ng-model="persona.login"
														placeholder="Ingrese el nombre de usuario"
														onKeyPress="return valida(event,this,2,100)"
														onBlur="valida2(this,2,100);">
												</div>
											</div>
										</div>
										<div class="col-lg-4 col-md-6 col-xs-12 col-sm-12">
											<div class="form-group">
												<div class='asterisco_rojo'><i class='fa fa-asterisk' aria-hidden='true'></i>
												</div>
												<label class="">Nombre y Apellidos:</label>
												<div class="controls">
													<input type="text" name="nombre_apellido" id="nombre_apellido"
														class="form-control form-control-line"
														ng-model="persona.nombres_apellidos"
														placeholder="Ingrese el nombre completo"
														onKeyPress="return valida(event,this,4,100)"
														onBlur="valida2(this,4,100);">
												</div>
											</div>
										</div>
									</div>
									<div class="row p-20">
										<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
											<div class="form-group">
												<div class='asterisco_rojo'><i class='fa fa-asterisk' aria-hidden='true'></i>
												</div>
												<label class="">Tipo de usuario:</label>
												<div class="controls">
													<select name="tipo_usuario" id="tipo_usuario"
														class="form-control m-bot15 select-picker" data-done-button="true"
														data-actions-box="true"
														data-live-search-placeholder="Seleccione tipo de usuario"
														placholder="Seleccione tipo de idioma" data-style="btn-fff "
														data-live-search="true"
														ng-options="option.descripcion for option in tipo_user track by option.id"
														ng-model="persona.id_tipo_usuario" data-size="3">
														<option value="">--Seleccione un tipo de usuario--</option>
													</select>
												</div>

											</div>
										</div>
										<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
											<div class="form-group">
												<div class='asterisco_rojo'><i class='fa fa-asterisk' aria-hidden='true'></i>
												</div>
												<label class="">Email:</label>
												<div class="controls">
													<input type="text" name="correo" id="correo"
														class="form-control form-control-line" ng-model="persona.email"
														placeholder="Ingrese Correo"
														onKeyPress="return valida(event,this,5,100)"
														onBlur="valida2(this,5,100);">
												</div>
											</div>
										</div>
									</div>
									<div class="row p-20">

										<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
											<div class="form-group">
												<div class='asterisco_rojo'><i class='fa fa-asterisk' aria-hidden='true'></i>
												</div>
												<label class="">Teléfono:</label>
												<div class="controls">
													<input type="text" name="telefono" id="telefono"
														class="form-control form-control-line" ng-model="persona.telefono"
														placeholder="Ingrese el N°Telefono"
														onKeyPress="return valida(event,this,10,11)"
														onBlur="valida2(this,10,11);">
												</div>
											</div>
										</div>
										<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
											<div class="form-group">
												<div class='asterisco_rojo'><i class='fa fa-asterisk' aria-hidden='true'></i>
												</div>
												<label class="">Confirmar Teléfono:</label>
												<div class="controls">
													<input type="text" name="telefono" id="telefono"
														class="form-control form-control-line" ng-model="persona.telefono_confirmar"
														placeholder="Ingrese el N°Telefono"
														onKeyPress="return valida(event,this,10,11)"
														onBlur="valida2(this,10,11);">
												</div>
											</div>
										</div>
									</div>
									<div class="row p-20">
										<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
											<div class="form-group">
												<div class='asterisco_rojo'><i class='fa fa-asterisk' aria-hidden='true'></i>
												</div>
												<label class="">Clave:</label>
												<div class="controls">
													<input type="password" name="clave" id="clave"
														class="form-control form-control-line" ng-model="persona.clave"
														placeholder="Ingrese el la contrañsea">
												</div>
											</div>
										</div>
										<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
											<div class="form-group">
												<div class='asterisco_rojo'><i class='fa fa-asterisk' aria-hidden='true'></i>
												</div>
												<label class="">Repita su Clave:</label>
												<div class="controls">
													<input type="password" name="repite_clave" id="repite_clave"
														class="form-control form-control-line" ng-model="persona.repite_clave"
														placeholder="Repita contraseña">
												</div>
											</div>
										</div>

									</div>
									
								</div>
								<!-- Fin datos basicos -->
								<!-- Tab imagenes -->
								<div class="tab-pane p-20" id="imagenes" role="tabpanel" ng-class="{'active':currentTab === 'imagenes'}">
                                    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                        <div class="img_principal"  ng-click="seleccione_img_principal()">
                                            <div ng-class="{'invisible':activo_img==='activo','visible':activo_img==='inactivo'}">
                                                <span>
                                                    <i class="fas fa-image" aria-hidden="true"></i>
                                                    Seleccione imagen
                                                </span>
                                            </div>
                                            <div class="mensaje_img_principal" ng-class="{'visible':activo_img==='activo','invisible':activo_img==='inactivo'}">
                                                    <img class="img_noticias" id="img_seleccionada" ng-src="{{base_url}}{{persona.imagen}}" height="115" data="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Fin tab imagenes -->
							</div>
							<div class="row button-group">
								<div class="col-lg-6">
									<div class="row">
										<div id="div_mensaje"></div>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="row">
										<div class="col-lg-4 col-md-4" ng-if="id_personas!=''">
											<a href="<?=base_url();?>cms/registro_usuario">
												<button type="button"
													class="btn waves-effect waves-light btn-block btn-success">Nuevo</button>
											</a>
										</div>
										<div class="col-lg-4 col-md-4" ng-if="id_personas==''">
											<button type="button"
												class="btn waves-effect waves-light btn-block btn-success"
												ng-click="limpiar_cajas_usuario()">Limpiar</button>
										</div>

										<div class="col-lg-4 col-md-4">
											<a href="<?=base_url();?>cms/consulta_usuariosCMS">
												<button type="button"
													class="btn waves-effect waves-light btn-block btn-danger">{{titulo_cons}}</button>
											</a>
										</div>
										<div class="col-lg-4 col-md-4">
											<button type="button"
												class="btn waves-effect waves-light btn-block btn-info"
												ng-click="registrarUsuario()">{{titulo_registrar}}</button>
										</div>
										<input type="hidden" name="id_personas" id="id_personas"
											value="<?php if(isset($id)){echo $id;}?>">
										<input type="hidden" name="base_url" id="base_url"
											value="<?php echo base_url(); ?>">
									</div>
								</div>
							</div>		
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="wisiModal" name="wisiModal" tabindex="-1" role="dialog"
			aria-labelledby="exampleModalLabel1">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="exampleModalLabel1">Ingrese el contenido</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
								aria-hidden="true">&times;</span></button>
					</div>
					<div class="modal-body">
						<form>
							<div class="card-body">
								<form method="post">
									<div class="form-group">
										<textarea id="textarea_editor" name="textarea_editor"
											class="textarea_editor form-control" rows="15"
											placeholder="Ingrese texto..."></textarea>
									</div>
								</form>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button id="cerrarModal" type="button" class="btn btn-danger"
							data-dismiss="modal">Cerrar</button>
						<button type="button" class="btn btn-primary" ng-click="agregarWisi()">Agregar</button>
					</div>
				</div>
			</div>
		</div>

		<!-- ============================================================== -->
        <!--Bloque modal del sistema -->
        <div class="modal fade" id="modal_img1" name="modal_img1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog modal-lg .modal-sm">
              <div class="modal-content">
                <div class="modal-header header_conf">
                    <p id="cabecera_mensaje" name="cabecera_mensaje"> Seleccione una imagen</p>
                    <button type="button" id="cerrar_mensaje2" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body" id="cuerpo_mensaje" name="cuerpo_mensaje">
                    <!--Modal body -->
                    <div class="centrar_galeria" ng-show="galery.length ==0">Galería de imágenes usuarios</div>
                    <div id="" ng-show="galery.length!=0" class="fade-in-out">
                        <div>
                            <div class="form-group">
                                <input type='text' name='filtro_noticias' id='filtro_noticias' placeholder='Ingrese el valor a filtrar' class="form-control" ng-model="searchNoticias">
                            </div>
                            <div class="col-lg-12" padding="0px;">
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 divbiblioteca" ng-repeat="imagen in galery|filter:searchNoticias track by $index">
                                        <img class="imgbiblioteca"  id="img_biblioteca{{$index}}" ng-src="{{base_url}}/{{imagen.ruta}}" height="115" data="{{imagen.id}}|{{imagen.ruta}}" data-ng-click="seleccionar_imagen($event)">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <!-- modal body-->
                </div>
                <div class="modal-footer footer_conf">
                    <!-- Footter del modal -->
                      <button type="button" name="modal_reporte_salir" id="modal_reporte_salir" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                    <!-- Fin footter del modal -->
                </div>
                <div style="clear:both"></div>
              </div>
            </div>
        </div>
        <!-- ============================================================== -->
	</div>
</div>