<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid" ng-controller="PdfMultimediaController">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center titulo-dashboard">
                <h3 class="text-themecolor">Configuración</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">Configuración</li>
                    <li class="breadcrumb-item active">PDF</li>
                </ol>
            </div>

        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">{{titulo_pagina}}</h4>
                        <h6 class="card-subtitle">{{subtitulo_pagina}}</h6>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 ">
                                    <div class='asterisco_rojo'><i class='fa fa-asterisk' aria-hidden='true'></i></div>
                                    <label>Título del pdf <span class="help"></span></label>
                                    <input name="titulo" id="titulo" type="text" class="form-control form-control-line"
                                        placeholder="Ingrese el título del pdf" ng-model="pdf.titulo" required>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <div class='asterisco_rojo'><i class='fa fa-asterisk' aria-hidden='true'></i></div>
                                    <label>Categoria<span class="help"></span></label>
                                    <select name="categorias" id="categorias" class="form-control form-control-line"
                                        ng-options="option.descripcion for option in categorias track by option.id"
                                        ng-model="pdf.categoria" required>
                                        <option value="">--Seleccione una categoria--</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row p-20">
                                <!-- -->
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 ">
                                    <!--<label>Título de la imagen <span class="help"></span></label>
                                    <input name="categorias_descripcion" id="categorias_descripcion" type="text" class="form-control form-control-line" placeholder="Ingrese la descripción de la categoría" ng-model="pdf.descripcion" required> -->
                                    <label> <span class="help"></span></label>
                                    <div id=" row">
                                        <div class="form-group">
                                            <input id="file" name="file" type="file" uploader-model="file"
                                                accept="application/pdf" style="display:none">
                                            <!--  uploader-model="file"  ng-file-model="file"-->
                                            <label for="file" style="width: 100%">
                                                <div id="subir_imagenes" class="col-lg-12 btn btn-block btn-info" ng-class="{'btn-warning': id_pdf!=''}"><i
                                                        class="fa fa-cloud-upload" aria-hidden="true"></i> Subir PDF
                                                </div>
                                            </label>
                                            <div class="">
                                                <div id="list" style="padding-bottom:2px;">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                                    <div class="form-group">
                                        <div class='asterisco_rojo'><i class='fa fa-asterisk' aria-hidden='true'></i>
                                        </div> <label class="">Descripción:</label>
                                        <div class="div_wisig" id="div_descripcion" ng-click="wisi_modal()">
                                            Pulse aquí para ingresar la descripción del pdf
                                        </div>
                                        <!--data-toggle="modal" data-target="#wisiModal" data-whatever="@mdo" -->
                                        <!--<div class="controls">
                                                        <textarea class="form-control" placeholder="Ingrese descripción de la noticia" onkeypress="return valida(event,this,18,250)" onblur="valida2(this,18,250);" maxlength="1000" style="resize:none" rows="13" id="noticia_descripcion" name="noticia_descripcion" ng-model="noticias.descripcion"  onkeypress="return valida(event,this,18,250)" onblur="valida2(this,18,250);" maxlength="1000"></textarea>
                                                    </div>-->
                                    </div>
                                </div>
                                <!-- -->
                            </div>
                        </div>
                        <div class="row button-group">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div id="div_mensaje"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4" ng-if="id_pdf!=''">
                                        <a href="<?=base_url();?>cms/cargar_pdf">
                                            <button id="btn-nuevo" type="button"
                                                class="btn waves-effect waves-light btn-block btn-success">Nuevo</button>
                                        </a>
                                    </div>

                                    <div class="col-lg-4 col-md-4" ng-if="id_pdf==''">
                                        <button id="btn-limpiar" type="button"
                                            class="btn waves-effect waves-light btn-block btn-success"
                                            ng-click="limpiar_cajas_productos()">Limpiar</button>
                                    </div>

                                    <div class="col-lg-4 col-md-4">
                                        <a href="<?=base_url();?>cms/cargar_pdf/consultarPDF">
                                            <button id="btn-consultar" type="button"
                                                class="btn waves-effect waves-light btn-block btn-danger">Consultar</button>
                                        </a>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <button id="btn-registrar" type="button"
                                            class="btn waves-effect waves-light btn-block btn-info"
                                            ng-click="registrarPdf()">{{titulo_registrar}}</button>
                                    </div>
                                    <input type="hidden" name="id_pdf" id="id_pdf"
                                        value="<?php if(isset($id)){echo $id;}?>">
                                    <input type="hidden" name="base_url" id="base_url"
                                        value="<?php echo base_url(); ?>">
                                </div>
                            </div>
                        </div>
                        <!-- ============================================== -->
                    </div>
                </div>
            </div>
        </div>

        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <div class="modal fade" id="wisiModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="exampleModalLabel1">Ingrese el contenido</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="card-body">
                                <form method="post">
                                    <div class="form-group">
                                        <textarea class="textarea_editor form-control text_editor" rows="15"
                                            placeholder="Ingrese texto..." id="text_editor"></textarea>
                                    </div>
                                </form>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button id="cerrarModal" type="button" class="btn btn-danger"
                            data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-primary" ng-click="agregarWisi()">Agregar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>">
<script type="text/javascript">
    function archivo(evt) {
        var files = evt.target.files; // FileList object
        //Obtenemos la imagen del campo "file". 
        for (var i = 0, f; f = files[i]; i++) {
            //Solo admitimos imágenes.
            if (!f.type.match('pdf.*')) {
                continue;
            }
            console.log(files);
            var reader = new FileReader();
            reader.onload = (function (theFile) {
                console.log(theFile.name);
                var nombre = theFile.name;
                return function (e) {
                    // Creamos la imagen.
                    //document.getElementById("list").innerHTML = ['<img class="thumb" src="', e.target.result,'" title="', escape(theFile.name), '"/>'].join('');
                    $("#list").css("display", "block");
                    $("#list").html([
                        '<input name="titulo_pdf" id="titulo_pdf" type="text" class="center-text text-center form-control form-control-line"placeholder="Ingrese la descripción de la categoría" ng-model="pdf.descripcion" required value="Archivo cargado" readonly>'
                    ].join('')).fadeIn("slow");
                };
                $("#titulo_pdf").css("display", "none");
            })(f);

            reader.readAsDataURL(f);
        }
    }

    document.getElementById('file').addEventListener('change', archivo, false);
</script>