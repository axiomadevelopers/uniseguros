<div class="page-wrapper" >
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid" ng-controller="DetalleTproductosConsultaController">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 col-8 align-self-center titulo-dashboard">
                <h3 class="text-themecolor">Detalles de productos</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active">Detalles de productos</li>
                    <li class="breadcrumb-item active">Consultar</li>
                </ol>
            </div>
            <!--<div class="col-md-7 col-4 align-self-center">
                <div class="d-flex m-t-10 justify-content-end">
                    <div class="">
                        <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i
                                class="ti-settings text-white"></i></button>
                    </div>
                </div>
            </div>-->
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">{{titulo_pagina}}</h4>
                        <div class="table-responsive m-t-40">
                            <form id="formConsultaProductos" method="POST" target="_self">
                                <input type="hidden" id="id_detalle_productos" name="id_detalle_productos" ng-model="id_detalle_productos" >
                                 <input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                                <div class="col-lg-12 mg-b30">
                                    <a href="{{base_url}}cms/detalle_productos">
                                        <div class="btn btn-success btn-new" >Nuevo</div>
                                    </a>    
                                </div> 
                                <table id="myTable" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Acciones</th>
                                            <th>Orden</th>
                                            <th>Idioma</th>
                                            <th>Producto</th>
                                            <th>Tipo de producto</th>
                                            <th>Título</th>
                                            <th style="width:200px;">Descripción</th>
                                          
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat = "productos in productos_detalles track by $index">
                                        <td class="">{{$index+1}}</td>
                                             <td class="centrado">
                                                <div class="form-group flotar_izquierda">
                                                    <div id="ver{{$index}}" ng-click="ver_producto($index)" data="{{productos.id}}" class="btn btn-primary flotar_izquierda2" title="modificar">
                                                        <i class="fas fa-pencil-alt" aria-hidden="true"></i>
                                                    </div>
                                                    <div id="btn_activar{{$index}}_a" name="btn_activar{{$index}}_a" class="btn btn-warning flotar_izquierda2" ng-class="{visible:productos.estatus=='1',invisible:productos.estatus!=1}" title="Inactivar" data-ng-click="activar_registro($event)" data="{{productos.id}}|{{productos.estatus}}|{{productos.id_productos}}|{{productos.id_tipo_producto}}">
                                                    <i class="fa fa-lock" aria-hidden="true"></i>
                                                    </div>
                                                    <div id="btn_activar{{$index}}_b" name="btn_activar{{$index}}_b"  class="btn btn-success flotar_izquierda2" ng-class="{visible:productos.estatus!='1',invisible:productos.estatus==1}" title="Publicar" data-ng-click="activar_registro($event)" data="{{productos.id}}|{{productos.estatus}}|{{productos.id_productos}}|{{productos.id_tipo_producto}}">
                                                    <i class="fa fa-check" aria-hidden="true"></i>
                                                    </div>
                                                    <div id="btn_eliminar{{$index}}" name="btn_eliminar{{$index}}"  class="btn btn-danger flotar_izquierda2" title="Eliminar" data-ng-click="eliminar_registro($event)" data="{{productos.id}}|2|{{productos.id_productos}}|{{productos.id_tipo_producto}}">
                                                    <i class="fa fa-window-close" aria-hidden="true"></i>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="">{{productos.orden}}</td>
                                            <td class="">{{productos.descripcion_idioma}}</td>
                                            <td class="">{{productos.numero_producto}}-{{productos.descripcion_producto}}</td>
                                            <td class="">{{productos.numero_tipo_producto}}-{{productos.descripcion_tipo_producto}}</td>
                                            <td class="">{{productos.titulo}} </td>
                                            <td class="">{{productos.descripcion_sin_html.substr(0,150)+"..."}}</td>
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
</div>
