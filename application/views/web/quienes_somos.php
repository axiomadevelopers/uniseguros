<!-- Wrapper-->
<div class="wrapper" ng-controller="nosotrosController">
	<!-- Parallax de Quienes Somos-->
	<section class="parallax-cabecera">
		<div id="parallaxServicios" class="dzsparallaxer auto-init height-is-based-on-content use-loading">
	    	<div class="parallax-gradient super_parallax divimage dzsparallaxer--target w-100 g-bg-size-cover g-bg-img-hero g-bg-cover g-bg-black-opacity-0_6--after"
	        style="height: 130%; background-image: url(assets/web/images/parallax/quienes_somos_parallax.png);"></div>

		    <div class="container g-pt-100 g-pb-70">
		        <div class="row2">
		            <div class="col-lg-6 col-md-12 col-sm-12 col-sm-12  align-items-end mt-auto g-mb-50 texto_parallax">
		                <div class="text-center parallax_especial">
		                    <h1 class="d-inline-block g-color-secondary g-font-weight-800 g-font-size-26 mb-0 g-z-index-1 titulos texto-parallax"
		                        style="color:#fff">QUIENES SOMOS </h1>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</section>
	<!--Fin parallax Quienes Somos -->
	<!-- Quienes somos -->
	<section class="module">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="col-lg-8 col-md-12 col-xs-12 col-sm-12 m-auto bloque-inicial">
						<div class="module-title text-center wow fadeInUp letter-spacing-2 ">
							<h1 class="text-uppercase">Somos Uniseguros</h1>
							<p class="font-serif" id="subtitulo-qsomos" >{{nosotros.titulo_somos}}</p>
							<span class="bg-base-color d-inline-block mt-4 sep-line-thick-long"></span>
						</div>
						<div class="wow fadeInUp">
							<p class="texto-parrafos" id="parrafo-qsomos-home" ng-bind-html="nosotros.descripcion_somos">
							</p>
						</div>
					</div>
				</div>
				<!--centrador-->
				<div class="col-lg-12">
					<div class="row">
						<!--card centro-->
						<div id="centro" class="col-lg-6 col-md-6 col-xs-12 col-sm-12 fadeInLeft wow quienesSomosCuadro">
							<center>
								<div class="icon-box-icon">
									<img class="img-responsive img-quienes-somos" ng-src="{{nosotros.imagen_mision}}">
								</div>	
								<div class="icon-box-title letter-spacing-2">
									<h6 id="titulo_clientes2">{{nosotros.titulo_mision}}</h6>
								</div>
									
								<div class="icon-box-content" id="icon-box-content-cen">
									<p id="parrafos_clientes2" class="lista-mision texto-parrafos quienesSomosCuadro"  ng-bind-html="nosotros.descripcion_mision"></p>
									<div class="row centrado hide" >
										<div class="col-md-12">
											<div class="text-center">
												<button class="btn btn-round btn-lg btn-brand" ng-click="mostrar_contenido('1');">Leer mas</button>
											</div>
										</div>
										<div style="clear: both;"></div>
									</div>
								</div>
							</center>
						</div>
						<!--card centro end-->
						<!--card derecha -->
						<div id="derecha" class="col-lg-6 col-md-6 col-xs-12 col-sm-12 fadeInRight wow cuerpo_vision quienesSomosCuadro">
							<center>
								<div class="icon-box-icon">
									<img class="img-responsive img-quienes-somos" ng-src="{{nosotros.imagen_vision}}">
								</div>	
								<div class="icon-box-title letter-spacing-2">
									<h6 id="titulo_clientes3">{{nosotros.titulo_vision}}</h6>
								</div>
								
								<div class="icon-box-content" id="icon-box-content-de">
									<p id="parrafos_clientes3" class="lista-vision texto-parrafos quienesSomosCuadro" ng-bind-html="nosotros.descripcion_vision"></p>
									<div class="row centrado" >
										<div class="col-md-12">
											<div class="text-center hide	">
												<button class="btn btn-round btn-lg btn-brand" ng-click="mostrar_contenido('2');">Leer mas</button>
											</div>
										</div>
										<div style="clear: both;"></div>
									</div>
								</div>
							</center>
						</div>
						<hr>

						<!--Valores-->
						<div id="center" class="col-lg-8 col-md-12 col-xs-12 col-sm-12 fadeInUp wow cuerpo-valores quienesSomosCuadro	">
							<center>
								<div class="icon-box-icon">
									<img class="img-responsive img-quienes-somos" ng-src="{{nosotros.imagen_valores}}">
								</div>
									
								<div class="icon-box-title letter-spacing-2">
									<h6 id="titulo_clientes1" >{{nosotros.titulo_valores}}</h6>
								</div>
								
								<div class="icon-box-content" id="icon-box-content-iz">
									<p id="parrafos_clientes1" class="lista-valores texto-parrafos" ng-bind-html="nosotros.descripcion_valores"></p>
								</div>
							</center>
						</div>

						<!--Fin valores-->
						<!--card derecha end-->
						<div style="clear: both"></div>
					</div>
				</div>	
				<!--centrador end-->
			</div>						
		</div>	
	</section>
	<!--Fin Quienes Somos -->
	<!-- Parallax de Junta Directiva-->
	
	<section class="hide">
		<div id="parallaxServicios" class="dzsparallaxer auto-init height-is-based-on-content use-loading">
	    	<div class="parallax-gradient super_parallax divimage dzsparallaxer--target w-100 g-bg-size-cover g-bg-img-hero g-bg-cover g-bg-black-opacity-0_6--after"
	        style="height: 130%; background-image: url(assets/web/images/parallax/junta_directiva_parallax.png);"></div>

		    <div class="container g-pt-100 g-pb-70">
		        <div class="row2">
		            <div class="col-lg-6 col-md-12 col-sm-12 col-sm-12  align-items-end mt-auto g-mb-50 texto_parallax">
		                <div class="text-center parallax_especial">
		                    <h1 class="d-inline-block g-color-secondary g-font-weight-800 g-font-size-26 mb-0 g-z-index-1 titulos texto-parallax"
		                        style="color:#fff">JUNTA DIRECTIVA </h1>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</section>
	
	<!--Fin parallax Junta directiva -->
	<!-- Junta directiva -->
	
	<section class="module module-gray hide">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-12 col-xs-12 col-sm-12 m-auto bloque-inicial">
					<div class="module-title text-center wow fadeInUp letter-spacing-2 ">
						<h1 class="text-uppercase">Nuestra junta directiva</h1>
						<p class="font-serif" id="subtitulo-qsomos" >Conformada por</p>
						<span class="bg-base-color d-inline-block mt-4 sep-line-thick-long"></span>
					</div>
				</div>
				<div class="col-md-12">
					<div class="owl-carousel carousel-directiva" data-carousel-options="{&quot;pagination&quot;:true, &quot;autoPlay&quot;: &quot;5000&quot;}">
						<div class=""  ng-repeat="dir in directiva track by $index">
							<div class="testimonials-card col-lg-10"  style="float:left">
								<div class="testimonials-card-photo">
									<img ng-src="{{dir.ruta}}" alt="">
								</div>
								<div class="testimonials-card-content">
									<p class="font-serif"></p>
								</div>
								<div class="testimonials-card-author">
									<h4>{{dir.titulo}}</h4>
								</div>
								<div class="testimonials-card-author">
									<p>{{dir.descripcion}}</p>
								</div>
							</div>
						</div>	
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<!--Fin Junta directiva -->
	<!-- Parallax ReAsegurtadoras -->
	
	<section class="">
		<div id="parallaxServicios" class="dzsparallaxer auto-init height-is-based-on-content use-loading">
	    	<div class="parallax-gradient super_parallax divimage dzsparallaxer--target w-100 g-bg-size-cover g-bg-img-hero g-bg-cover g-bg-black-opacity-0_6--after"
	        style="height: 130%; background-image: url(assets/web/images/parallax/reaseguradoras_parallax.png);"></div>

		    <div class="container g-pt-100 g-pb-70">
		        <div class="row2">
		            <div class="col-lg-6 col-md-12 col-sm-12 col-sm-12 align-items-end mt-auto g-mb-50 texto_parallax">
		                <div class="text-center parallax_especial">
		                    <h1 class="d-inline-block g-color-secondary g-font-weight-800 g-font-size-26 mb-0 g-z-index-1 titulos texto-parallax"
		                        style="color:#fff">REASEGURADORAS </h1>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</section>

	<!-- Fin de Parallax ReAseguradoras -->
	<!-- Reaseguradoras-->
	
	<section class="module aliados">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-12 col-xs-12 col-sm-12 m-auto bloque-inicial">
					<div class="module-title text-center wow fadeInUp letter-spacing-2 ">
						<h1 class="text-uppercase">Contamos con el respaldo</h1>
						<p class="font-serif" id="subtitulo-qsomos" >De las siguientes compañias reaseguradoras</p>
						<span class="bg-base-color d-inline-block mt-4 sep-line-thick-long"></span>
					</div>
				</div>
				<div class="col-md-12">
					<div id="reaseguradoras" class="owl-carousel carousel-reaseguradoras" data-carousel-options="{&quot;items&quot;:&quot;4&quot;}">
						<div class="client" ng-repeat="reg in reaseguradoras track by $index">
							<a href="{{reg.url}}" target="_blank">
								<img ng-src="{{reg.ruta}}" alt="" >
							</a>
						</div>
						<div style="clear: both"></div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<!-- Fin de Reaseguradoras-->
	<!-- Parallax Notificaciones -->
	
	<section class="">
		<div id="parallaxServicios" class="dzsparallaxer auto-init height-is-based-on-content use-loading">
	    	<div class="parallax-gradient super_parallax divimage dzsparallaxer--target w-100 g-bg-size-cover g-bg-img-hero g-bg-cover g-bg-black-opacity-0_6--after"
	        style="height: 130%; background-image: url(assets/web/images/parallax/notificaciones_parallax.jpg);"></div>

		    <div class="container g-pt-100 g-pb-70">
		        <div class="row2">
		            <div class="col-lg-6 col-md-12 col-sm-12 col-sm-12 align-items-end mt-auto g-mb-50 texto_parallax">
		                <div class="text-center parallax_especial">
		                    <h1 class="d-inline-block g-color-secondary g-font-weight-800 g-font-size-26 mb-0 g-z-index-1 titulos texto-parallax"
		                        style="color:#fff">NOTIFICACIONES </h1>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</section>

	<!-- Fin de Parallax Notificaciones -->	
	<!-- Notificaciones -->
	
	<section class="module module-gray">
		<div class="container">
			<!-- -->
			<div class="row">
				<div class="col-lg-8 col-md-12 col-xs-12 col-sm-12 m-auto">
					<div class="module-title text-center wow fadeInUp letter-spacing-2 ">
						<h1 class="text-uppercase"></h1>
						<p class="font-serif" id="subtitulo-qsomos" >
							Algunas notificaciones de interés
						</p>
						<span class="bg-base-color d-inline-block mt-4 sep-line-thick-long"></span>
					</div>
				</div>
			</div>	
			<!-- -->
			<ul class="lista-notificaciones col-lg-12" >
				<div class="row">
					<div class="col-lg-4 com-md-4 col-xs-12 col-sm-12 wow fadeInUp mt-5" ng-repeat="archivos in pdf track by $index">
						<li class="mt-2  text-servicios texto-parrafos-cuentas-bancarias"  style="padding-left: 5px;">
							<a class="texto-notificaciones"	title="Pulse aquí para descargar" href="<?=base_url();?>{{archivos.ruta}}" ng-bind-html="archivos.descripcion" download="{{archivos.titulo}}">
							</a>
						</li>
					</div>
					<!--Sub_Pdf 2 -->
					<div class="col-lg-4 com-md-4 col-xs-12 col-sm-12 wow fadeInUp mt-5" ng-repeat="archivos2 in sub_pdf track by $index">
						<li class="mt-2  text-servicios texto-parrafos-cuentas-bancarias"  style="padding-left: 5px;">
							<a class="texto-notificaciones"	title="Pulse aquí para descargar" href="<?=base_url();?>{{archivos2.ruta}}" ng-bind-html="archivos2.descripcion" download="{{archivos2.titulo}}">
							</a>
						</li>
					</div>
					<!-- -->
					<div id="otras_notificaciones" name="otras_notificaciones">
						
					</div>
					<div id="campo_mensaje_notificaciones" name="campo_mensaje_notificaciones" class="col-lg-12 col-md-12 col-xs-12 col-sm-12 mt-5">
					</div>
					<div class="col-md-12 mt-5">
						<div class="text-center">
							<div class="btn btn-round btn-lg btn-brand" ng-click="cargar_mas_subnotificaciones()">
								Ver mas
							</div>
						</div>
					</div>
			</ul>
		</div>
	</section>
	
	<!--Fin Junta directiva -->
	<!-- Cuerpo de modal -->
	
	<div class="modal fade" id="modal-2">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" ng-bind-html="titulo_modal"></h5>
					<button class="close" type="button" data-dismiss="modal" aria-label="Close"><span>&times;</span></button>
				</div>
				<div class="modal-body mensaje-mision texto-parrafos">
					<ul id="ul-modal"></ul>
				</div>
				<div class="modal-footer">
					<button class="btn btn-round btn-brand" type="button"  data-dismiss="modal" aria-label="Close">Cerrar</button>
				</div>
			</div>
		</div>
	</div>
	
	<!-- -->	