<!-- Wrapper-->
<div class="wrapper" ng-controller="productosController">
	<!-- Parallax de Quienes Somos-->
	<section class="parallax-cabecera">
		<div id="parallaxServicios" class="dzsparallaxer auto-init height-is-based-on-content use-loading">
	    	<div class="parallax-gradient super_parallax divimage dzsparallaxer--target w-100 g-bg-size-cover g-bg-img-hero g-bg-cover g-bg-black-opacity-0_6--after"
	        style="height: 130%; background-image: url(assets/web/images/parallax/productos_parallax.png);"></div>

		    <div class="container g-pt-100 g-pb-70">
		        <div class="row2">
		            <div class="col-sm-6 col-lg-6 align-items-end mt-auto g-mb-50 texto_parallax">
		                <div class="text-center">
		                    <h1 class="d-inline-block g-color-secondary g-font-weight-800 g-font-size-26 mb-0 g-z-index-1 titulos texto-parallax"
		                        style="color:#fff">PRODUCTOS </h1>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</section>	
	<section class="module module-gray">
		<div class="container">
			<div class="row justify-content-center pb-5">
                <div class="col-lg-9 pb-lg-4 text-center">
                    <h1 class="letter-spacing-2 text-uppercase module-title wow fadeInUp ">Nuestros Productos</h1>
                    <p class="font-serif">	</p>
                    <span class="bg-base-color d-inline-block mt-4 sep-line-thick-long">
                    </span>
                </div>
            </div>
			<div class="row">
			<!-- -->
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 pt-20" ng-repeat = "productos in productos track by $index">
					<div class="card card-body text-center wow fadeInUp" >
						<div class="card-ribbon card-ribbon-top card-ribbon-right bg-faded ">
							<div class="icon-box-icon">
								<img class="img-responsive img-productos img-productos-h" src="{{productos.ruta}}">
							</div>	
						</div>
						<h4 class="card-title">
						
						</h4>
						<!--
						<p class="card-text">
							{{productos.descripcion_sin_html}}
						</p> 
						-->
						<a href="{{base_url}}productos/{{productos.slug}}" class="btn btn-brand btn-sm mt-3">Ver mas</a> 
		            </div>
		        </div>
		         
				
			<!-- -->
			</div>
			
		</div>
	</section>		