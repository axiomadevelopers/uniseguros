<!-- Wrapper-->
<div class="wrapper" ng-controller="contactanosController">
	<!-- Parallax de Quienes Somos-->
	<section class="parallax-cabecera">
		<div id="parallaxServicios" class="dzsparallaxer auto-init height-is-based-on-content use-loading">
			<div class="parallax-gradient super_parallax divimage dzsparallaxer--target w-100 g-bg-size-cover g-bg-img-hero g-bg-cover g-bg-black-opacity-0_6--after"
				style="height: 130%; background-image: url(assets/web/images/parallax/contactanos_parallax.png);"></div>

			<div class="container g-pt-100 g-pb-70">
				<div class="row2">
					<div class="col-sm-6 col-lg-6 align-items-end mt-auto g-mb-50 texto_parallax">
						<div class="text-center">
							<h1 class="d-inline-block g-color-secondary g-font-weight-800 g-font-size-26 mb-0 g-z-index-1 titulos texto-parallax"
								style="color:#fff">CONTÁCTANOS </h1>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--Contactos -->
	<section class="module module-gray">
		<div class="container">
			<!-- -->
			<div class="col-lg-8 col-md-12 col-xs-12 col-sm-12 m-auto bloque-inicial">
				<div class="module-title text-center wow fadeInUp letter-spacing-2 ">
					<h1 class="text-uppercase">Mantente en contacto</h1>
					<p class="font-serif" id="subtitulo-qsomos">Comunicate con nosotros</p>
					<span class="bg-base-color d-inline-block mt-4 sep-line-thick-long"></span>
				</div>
			</div>
			<!-- -->
			<div class="row m-b-50 wow fadeInDown centrado">
				<div class="col-lg-6 col-md-12 col-xs-12 col-sm-12 m-auto form-contact">
					<div class=" text-center">
						<span class="d-block font-alt letter-spacing-2 mt-6 mb-32 text-uppercase title-medium">Formulario de contacto</span>
					</div>
					<input id="form-nombre" class="form-control form-control-lg m-b-15 campos-form" type="text"
						placeholder="NOMBRE" ng-model="contactos.nombres" onKeyPress="return valida(event,this,24,100)"
						onBlur="valida2(this,24,100);" onpaste="no_pegar('campo_mensaje_clientes');return false;"
						maxlength="100">

					<input id="form-tlf" name="form-tlf" class="form-control form-control-lg m-b-15 campos-form"
						type="text" placeholder="TELÉFONO" ng-model="contactos.telefono"
						onKeyPress="return valida(event,this,23,14)" onBlur="valida2(this,23,14);"
						onpaste="no_pegar('campo_mensaje_clientes');return false;">

					<input id="form-email" name="form-email" class="form-control form-control-lg m-b-15 campos-form"
						type="text" placeholder="EMAIL" ng-model="contactos.email"
						onBlur="valida2(this,5,50);correo(this,'campo_mensaje_clientes')"
						onpaste="no_pegar('campo_mensaje_clientes');return false;">

					<textarea id="form-mensaje" name="form-mensaje" class="form-control campos-form"
						ng-model="contactos.mensaje" rows="5" placeholder="MENSAJE"
						onpaste="no_pegar('campo_mensaje_clientes');return false;"></textarea>

					<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 mensaje-clientes">
						<div id="campo_mensaje_clientes"></div>
					</div>
					<div class="col-md-12 m-auto">
						<div class="m-b-10">
							<div class="btn btn-block btn-round btn-xs btn-brand btn-enviar mt-51"
								ng-click="registrar_contactos()">Enviar</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-12 col-xs-12 col-sm-12 wow" >
						<div class=" text-center card-cuentas-bancarias">
							<span class="d-block font-alt letter-spacing-2 mt-6 mb-32 text-uppercase title-medium">Cuentas
								Bancarias</span>
							<ul class="lista-vision">
								<li class="mt-2  text-servicios texto-parrafos-cuentas-bancarias" style="padding-left: 5px;" ng-repeat = "banco in banco track by $index">
									{{banco.descripcion_banco}}: {{banco.descripcion}}
								</li>
							</ul>
							<p>{{pdf.descripcion}}</p>
							<div class="col-md-12 m-auto">
								<div class="m-b-10">
									<a class="btn btn-block btn-round btn-shadow btn-xs btn-brand"
									href="<?=base_url();?>{{pdf.ruta}}" download="{{pdf.titulo}}">Descargar</a>
								</div>
							</div>							
						</div>
						<div class="text-center">
						<span class="texto-descripcion" ></span>

						</div>
				</div>
			</div>
		</div>
	</section>
	<!--Fin de contactos -->
	<!--Parallax de redes sociales-->
	<!-- -->
	<section>
		<div id="parallaxServicios" class="dzsparallaxer auto-init height-is-based-on-content use-loading">
			<div class="parallax-gradient super_parallax divimage dzsparallaxer--target w-100 g-bg-size-cover g-bg-img-hero g-bg-cover g-bg-black-opacity-0_6--after"
				style="height: 130%; background-image: url(assets/web/images/parallax/redes_sociales_parallax.png);">
			</div>

			<div class="container g-pt-100 g-pb-70">
				<div class="row2">
					<div class="col-sm-6 col-lg-6 align-items-end mt-auto g-mb-50 texto_parallax">
						<div class="text-center">
							<h1 class="d-inline-block g-color-secondary g-font-weight-800 g-font-size-26 mb-0 g-z-index-1 titulos texto-parallax"
								style="color:#fff">REDES SOCIALES </h1>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="redes_sociales">
		<div class="container">
			<div class="row pb-35">
				<p class="introduccion_parrafos" style="text-align: center">
				</p>
				<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 padding0">
					<div style="" class="cuerpo_redes col-lg-12 col-md-12 col-xs-12 col-sm-12">
						<div class="col-4 col-lg-4 col-md-4 col-xs-4 col-sm-4 padding0 redes_individual"
							style="float:left;display: flex; margin: 0 auto;">
							<div class="redes_individual" style="display: flex;margin: 0 auto">
								<a href="{{redes[1].url_red}}" target="_blank">
									<div class="contenedor_icono_pasos">
										<div class="iconos_pasos iconos_redes">
											<i class="fa fa-facebook super-iconos-redes" aria-hidden="true"></i>
										</div>
									</div>
								</a>
							</div>
							<div style="clear:both"></div>
						</div>
						<div class="col-4 col-lg-4 col-md-4 col-xs-4 col-sm-4 padding0 redes_individual"
							style="float:left;display: flex; margin: 0 auto;" title="Facebook">
							<div class="redes_individual" style="display: flex;margin: 0 auto">
								<a href="{{redes[0].url_red}}" target="_blank">
									<div class="contenedor_icono_pasos">
										<div class="iconos_pasos iconos_redes">
											<i class="fa fa-instagram super-iconos-redes" aria-hidden="true"></i>
										</div>
									</div>
								</a>
							</div>
							<div style="clear:both"></div>
						</div>
						<div class="col-4 col-lg-4 col-md-4 col-xs-4 col-sm-4 padding0 redes_individual"
							style="float:left;display: flex; margin: 0 auto;">
							<div class="redes_individual" style="float:left;display: flex; margin: 0 auto;">
								<a href="{{redes[2].url_red}}" target="_blank">
									<div class="contenedor_icono_pasos">
										<div class="iconos_pasos iconos_redes" href="" target="_blank">
											<i class="fa fa-twitter super-iconos-redes" aria-hidden="true"></i>
										</div>
									</div>
								</a>
							</div>
							<div style="clear:both"></div>
						</div>

						<div style="clear:both"></div>
					</div>
				</div>


				<div class="super_contenedor_redes">
					<div class="row" style="padding-bottom: 35px;margin:0px;">

						<div class="fadeInUp wow col-lg-4 redes_cuadro hidden-md hidden-sm hidden-xs centrado"
							id="row_redes">
							<div class=" div_facebook ">
								<iframe
								src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Funisegurosve&tabs=timeline&width=300&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=2080726155276634"
									width="300" height="500" style="border:none;overflow:hidden" scrolling="no"
									frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share" class="centrado"></iframe>
							</div>
						</div>
						<div class="fadeInUp wow col-md-4  redes_cuadro hidden-lg hidden-sm hidden-xs centardo"
							id="row_redes" style="float:left">
							<div class="div_facebook ">
								<iframe
								src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Funisegurosve&tabs=timeline&width=200&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=2080726155276634"
									width="200" height="500" style="border:none;overflow:hidden;float:left"
									scrolling="no" frameborder="0" allowTransparency="true" class="centrado"></iframe>
							</div>
						</div>
						<div class="fadeInUp wow col-sm-12 redes_cuadro hidden-lg hidden-md hidden-xs centrado"
							id="row_redes" style="display: flex">
							<div class="div_facebook">
								<iframe
								src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Funisegurosve&tabs=timeline&width=500&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=2080726155276634"
									width="500" height="500" style="border:none;overflow:hidden;" scrolling="no"
									frameborder="0" allowTransparency="true" class="centrado"></iframe>
							</div>
						</div>
						<div class="fadeInUp wow col-xs-12 redes_cuadro hidden-lg hidden-md hidden-sm centrado"
							id="row_redes" style="padding-left: 20px;">
							<div class="div_facebook">
								<iframe
								src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Funisegurosve&tabs=timeline&width=300&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=2080726155276634"
									width="300" height="500" style="border:none;overflow:hidden" scrolling="no"
									frameborder="0" allowTransparency="true" class="centrado"></iframe>
							</div>
						</div>
						<div class="fadeInUp wow col-lg-4 col-md-4 col-sm-12 col-xs-12 redes_cuadro cuadro-tw"
							style="display: flex">
							<div class=" div_twitter tamano_twitter centrado"
								style="max-height: 500px;overflow-x: hidden">
								<div class="tamano_twitter" style="max-height: 510px;overflow-x: hidden;">
									
									<!-- SnapWidget -->
									<!--
									<iframe src="https://snapwidget.com/embed/855045" class="snapwidget-widget" allowtransparency="true" frameborder="0" scrolling="no" style="border:none; overflow:hidden;  width:100%; height:3300px"></iframe>
									-->	
									<!-- SnapWidget -->
									<!-- SnapWidget -->
									<iframe src="https://snapwidget.com/embed/920164" class="snapwidget-widget" allowtransparency="true" frameborder="0" scrolling="no" style="border:none; overflow:hidden;  width:100%; "></iframe>
								</div>	
							</div>
						</div>
						
						<div class="fadeInUp wow col-lg-4 col-md-4 col-sm-12 col-xs-12  redes_cuadro cuadro-tw"
							style="float:left;">
							<div class="redes_cuadro centrado" id="">
								<div class=" div_twitter tamano_twitter centrado"
									style="max-height: 500px;overflow-x: hidden">
									<a class="twitter-timeline" href="https://twitter.com/uniseguros_ve">Tweets by
										@uniseguros_ve</a>
									<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
								</div>
							</div>
						</div>
						<div style="clear:both"></div>

					</div>
				</div>

				<div style="clear:both"></div>
			</div>
		</div>
	</section>
	<!-- -->
	<!--Fin parallax redes sociales -->

	<!--Parallax de SUCURSALES-->
	<section>
		<div id="parallaxServicios" class="dzsparallaxer auto-init height-is-based-on-content use-loading">
			<div class="parallax-gradient super_parallax divimage dzsparallaxer--target w-100 g-bg-size-cover g-bg-img-hero g-bg-cover g-bg-black-opacity-0_6--after"
				style="height: 130%; background-image: url(assets/web/images/parallax/sucursales_parallax.png);"></div>

			<div class="container g-pt-100 g-pb-70">
				<div class="row2">
					<div class="col-sm-6 col-lg-6 align-items-end mt-auto g-mb-50 texto_parallax">
						<div class="text-center">
							<h1 class="d-inline-block g-color-secondary g-font-weight-800 g-font-size-26 mb-0 g-z-index-1 titulos texto-parallax"
								style="color:#fff">SUCURSALES </h1>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--Fin parallax SUCUSALES -->
	<!-- Sucursales -->
	<section class="module module-gray">
		<div class="container">
			<!-- -->
			<div class="col-lg-8 col-md-12 col-xs-12 col-sm-12 m-auto bloque-inicial">
				<div class="module-title text-center wow fadeInUp letter-spacing-2 ">
					<h1 class="text-uppercase">Localizanos</h1>
					<p class="font-serif" id="subtitulo-qsomos">En nuestra sucursal</p>
					<span class="bg-base-color d-inline-block mt-4 sep-line-thick-long"></span>
				</div>
			</div>
			<!-- -->
			<div class="row m-b-50 wow fadeInDown">
				<div class="col-lg-6 col-md-12 col-xs-12 col-sm-12 hide">
					<section id="contact-names" class="">
						<div class="row text-center">
							<aside class="widget widget_recent_entries sucursales-ul">
								<ul class="sucursales">
									<li class="lista-localizacion alert" id="listaLocalizacion{{$index}}"
										ng-repeat="dir in direccion track by $index"
										ng-mouseover="preSeleccionarRenglon($index)"
										ng-click="seleccionaRenglon($index)"
										ng-mouseleave="desSeleccionarRenglon($index)">{{dir.titulo}}
										<!--
										<div class="alert alert-success">
											<span class="icon-sucursal">
												<img src="<?=base_url();?>assets/web/images/puntero uniseguros-01.png" class="img-map" alt="">
											</span>
											 Caracas
										</div>
										-->
									</li>
								</ul>
							</aside>
						</div>
					</section>
				</div>
				<!-- Cuadro de mapas -->
					<div class=" col-lg-12 col-md-12 col-xs-12 col-sm-12 wow "
						ng-class="{fadeInLeft:dir.par_impar=='par',fadeInRight:dir.par_impar=='impar'}"
						style="float: left">
						<!-- ng-repeat="dir in direccion_f track by $index " -->
						
						<section class="map noMargin" id="map0"  style="border: solid 1px #999999;">
							<div id="direccion_mapas" name="direccion_mapas"></div>
							<!--
							<div id="map_canvas0" style="height: 354px; width:100%;background-color: #f6f6f6;"></div>-->
							<!--
							<img id="mapImg" name="mapImg" style="height:auto; width:100%;background-color: #f6f6f6;" ng-src="{{base_url}}{{mapa1}}">
							
							<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1961.5447227063178!2d-66.851637!3d10.493615!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x374f36283438401!2sUniseguros!5e0!3m2!1ses!2sus!4v1594428023386!5m2!1ses!2sus" width="100%" height="354px" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>-->

						</section>

						<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 cuadro_direccion" style="">
							<address class="g-bg-no-repeat g-line-height-2 g-mt-minus-4 m-g-top20">
								<div class="tamano_dir_desc">
									<span class="icono-add">
										<i class="fa fa-map-marker" aria-hidden="true"></i>
									</span>
									<span class="texto-descripcion" ng-bind-html="direccion_mapas.descripcion">

									</span>
								</div>
								<div class="cuerpo_telefonos">
									<div class="form-group">
										<span class="icono-add">
											<i class="fa fa-phone" aria-hidden="true"></i>
										</span>
										<!-- ng-repeat="tlf in dir.telefono track by $index "-->
										<span ng-repeat="tlf in direccion_mapas_telefono track by $index ">
											<label ng-if="$index>0"
												style="padding-left: 2px;padding-right: 2px; text-decoration: none;">/</label>
											{{tlf}}
										</span>
									</div>
								</div>
							</address>
						</div>
					</div>
					
				<!-- -->
			</div>
		</div>
	</section>
	<!-- Fin Sucursales-->