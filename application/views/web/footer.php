<div name="id_idioma" id="id_idioma" style="display: none"><?php echo $id_idioma;?></div>
<input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>"> 		
		<!-- Footer-->
		<footer class="footer">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-lg-3">
						<!-- Text widget-->
						<aside class="widget widget_text">
							<div class="textwidget">
								<p><img class="imagen-footer" src="<?=base_url();?>assets/web/images/logo_blanco.png" width="150px" alt=""></p>
								<p id="texto_footer" ng-bind-html="footer.descripcion"></p>
							</div>
							<div class="textwidget text-axioma">
								<a href="http://www.axiomadevelopers.com.ve" target="_blank">Powered By Axioma Developers</a>
							</div>	
						</aside>
					</div>
					<div class="col-md-6 col-lg-3">
						<!-- Recent entries widget-->
						<aside class="widget widget_recent_entries">
							<div class="widget-title">
								<h5><span class="footer_text" id="footer_company">Acerca de</span></h5>
							</div>
							<ul class="ul-acerca">
								<li><a href="<?=base_url();?>quienes_somos" target="_self">Quienes Somos</a> </li>
								<li><a class="" href="<?=base_url();?>productos">Productos</a></li>
								<li><a class="" href="<?=base_url();?>servicios">Servicios</a></li>
							</ul>
						</aside>
					</div>
					
					<div class="col-md-6 col-lg-3">
						<!-- Tags widget-->
						<aside class="widget widget_tag_cloud">
							<div class="widget-title">
								<h5 ><span class="footer_text"> Productos</span></h5>
							</div>
							<ul class="ul-productos" ng-repeat = "productos in productos_footer track by $index">
								<li id="footer-comof1">
									<a href="{{base_url}}productos/{{productos.slug}}">
										{{productos.titulo}}
									</a>
								</li>
							</ul>
						</aside>
					</div>
					<div class="col-md-6 col-lg-3">
						<!-- Twitter widget-->
						<aside class="widget twitter-feed-widget">
							<div class="widget-title">
								<h5 ><span class="menu3">Contáctanos</span></h5>
							</div>
							<ul class="ul-contactanos">
								<li>
									<a href="mailto:{{footer.correo}}"><i class="fa fa-envelope-o icono-footer" ></i>  {{footer.correo}}</a> <br/>
								</li>
					          	<li>
						            <a href="{{redes[1].url_red}}" target="_blank">
						              <i class="fa fa-facebook icono-footer" ></i>
						                Facebook
						            </a>
					          	</li>
					         	<li>
						            <a href="{{redes[0].url_red}}" target="_blank">
						              <i class="fa fa-instagram icono-footer" aria-hidden="true"></i>
						                Instagram
						            </a>
					          	</li>
					          	<li>
						            <a href="{{redes[2].url_red}}" target="_blank">
						              <i class="fa fa-twitter icono-footer"></i>
						                Twitter
						            </a>
					          	</li>
					          	<li>
						            <i class="fa fa-phone icono-footer"></i>
						                {{footer.telefono}}
					          	</li>
					          	
							</ul>
						</aside>
					</div>
				</div>
			</div>
			<div class="footer-copyright">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div id="derechos_reservados" class="text-center"><span class="copyright" id="texto-footer">© 2020 - Aseguradora Nacional Unida Uniseguros, S.A. Inscrita en la Superintendencia de la Actividad Aseguradora bajo el Nº 113, Rif J-30166471-0</span></div>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!-- Footer end-->

		<div class="scroll-top" onclick="subir_top()"><i class="fa fa-angle-up"></i></div>
	</div>
	<!-- Wrapper end-->
</div>
		<!-- Layout end-->

		<!-- Off canvas-->
		<div class="off-canvas-sidebar">
			<div class="off-canvas-sidebar-wrapper">
				<div class="off-canvas-header"><a class="close-offcanvas" href="index-21.html#"><span class="arrows arrows-arrows-remove"></span></a></div>
				<div class="off-canvas-content">
					<!-- Text widget-->
					<aside class="widget widget_text">
						<div class="textwidget">
							<!--
							<p class="text-center"><img src="assets/images/logo-light.png" width="100" alt=""></p>-->
						</div>
					</aside>
					<!-- Text widget-->
					<aside class="widget widget_text">
						<div class="textwidget">
							<p class="text-center">
								<!--<img src="assets/images/offcanvas.jpg" alt=""></p>-->
						</div>
					</aside>
					<!-- Navmenu widget-->
					<aside class="widget widget_nav_menu">
						<ul class="menu">
							<li class="menu-item menu-item-has-children"><a href="index-21.html#">Home</a></li>
							<li class="menu-item"><a href="index-21.html#">About Us</a></li>
							<li class="menu-item"><a href="index-21.html#">Services</a></li>
							<li class="menu-item"><a href="index-21.html#">Portfolio</a></li>
							<li class="menu-item"><a href="index-21.html#">Blog</a></li>
							<li class="menu-item"><a href="index-21.html#">Shortcodes</a></li>
						</ul>
					</aside>
					<ul class="social-icons">
						<li><a href="index-21.html#"><i class="fa fa-facebook"></i></a></li>
						<li><a href="index-21.html#"><i class="fa fa-twitter"></i></a></li>
						<li><a href="index-21.html#"><i class="fa fa-google-plus"></i></a></li>
						<li><a href="index-21.html#"><i class="fa fa-linkedin"></i></a></li>
						<li><a href="index-21.html#"><i class="fa fa-vk"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Off canvas end-->

		<!-- Scripts-->
		<script src="<?=base_url();?>assets/web/jquery-2.2.4.min.js"></script>
		<script src="<?=base_url();?>assets/web/popper.min.js"></script>
		<script src="<?=base_url();?>assets/web/bootstrap/js/bootstrap.min.js"></script>
		<!--
		<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
		-->
		<script src="https://snapwidget.com/js/snapwidget.js"></script>
    	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDlYNNs7VBO71qiKFMNiD0R9sd8hOt0wD4"></script>
    	
		<script src="<?=base_url();?>assets/web/js/plugins.min.js"></script>
		<script src="<?=base_url();?>assets/web/js/charts.js"></script>
		<script src="<?=base_url();?>assets/web/js/custom.js"></script>

		<!--Plugins -->
		<script src="<?=base_url();?>assets/web/plugins/dzsparallaxer/dzsparallaxer.js"></script>
		<script src="<?=base_url();?>assets/web/plugins/dzsparallaxer/dzsscroller/scroller.js"></script>
		<script src="<?=base_url();?>assets/web/plugins/dzsparallaxer/advancedscroller/plugin.js"></script>
		<script src="<?=base_url();?>assets/web/plugins/wow/wow.min.js"></script>
		<script src="<?=base_url();?>assets/web/plugins/fancybox/jquery.fancybox.min.js"></script>
		<script src="<?=base_url();?>assets/web/plugins/slick/js/slick.js"></script>
		<script src="<?=base_url();?>assets/web/js/fbasic.js"></script>

		<!--Angular Core -->
		 <!--
    <!--Core Angular JS -->
    <script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/angular.min.js"></script>
    <script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/angular-route.js"></script>
    <script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/angular-sanitize.min.js"></script>
    <script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/app.js"></script>
    <script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/directivas/directives.js"></script>
    <script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/servicios/services.js"></script>
    <!--Controladores Ng -->
    <script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/controladores/mainController.js"></script>
    <script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/controladores/inicioController.js"></script>
 	<script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/controladores/nosotrosController.js"></script>   
 	<script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/controladores/productosController.js"></script>   
 	<script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/controladores/serviciosController.js"></script> 
	<script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/controladores/contactanosController.js"></script> 
	<!-- -->
	<script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/controladores/detallesProductosController.js"></script>   
	<script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/controladores/tiposProductosController.js"></script>   
	<script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/controladores/detallesServiciosController.js"></script>   
	<script type="text/javascript"  src="<?=base_url();?>assets/web/js/angular/controladores/tiposServiciosController.js"></script>
	</body>
</html>
