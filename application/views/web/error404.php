<section class="module">
	<div class="container">
		<div class="row justify-content-center pb-5">
			<div class="col-lg-9 pb-lg-4 text-center">
				<div class="icon-box-icon">
					<div class="">
						<img class="img-responsive img-productos-detalle wow fadeInUp"
							ng-src="<?=base_url();?>assets/web/images/logo-black.png">
					</div>
				</div>
				
				<h1 class="letter-spacing-2 text-uppercase module-title-error wow fadeInUp ">Error 404 
				</h1>
				-
				<p class="font-serif wow fadeInUp">Página no econtrada</p>
				<span class="bg-base-color d-inline-block mt-4 sep-line-thick-long">
				</span>
			</div>
		</div>
	</div>
</section>		