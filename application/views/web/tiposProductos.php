<!-- Wrapper-->
<div class="wrapper" ng-controller="tiposProductosController">
	<!-- Parallax de Quienes Somos-->
	<section class="parallax-cabecera">
		<div id="parallaxServicios" class="dzsparallaxer auto-init height-is-based-on-content use-loading">
			<div class="parallax-gradient super_parallax divimage dzsparallaxer--target w-100 g-bg-size-cover g-bg-img-hero g-bg-cover g-bg-black-opacity-0_6--after"
				style="height: 130%; background-image: url({{base_url}}/assets/web/images/parallax/productos_parallax.png);">
			</div>
			<div class="container g-pt-100 g-pb-70">
				<div class="row2">
					<div class="col-sm-6 col-lg-6 align-items-end mt-auto g-mb-50 texto_parallax">
						<div class="text-center">
							<h1 class="d-inline-block g-color-secondary g-font-weight-800 g-font-size-26 mb-0 g-z-index-1 titulos texto-parallax"
								style="color:#fff">PRODUCTOS</h1>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="module module-gray">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-9 pb-lg-4 text-center">
					<img style="" src="<?=base_url();?>{{ruta}}" alt=""
								class="img-productos-detalle wow fadeInUp">
					<input type="hidden" id="slug_producto" name="slug_producto" ng-model="slug_producto"
						value="<?php echo $slug_producto;?>">
					<!--<p class="font-serif wow fadeInUp"> Nuestro catálogo de {{titulo}} </p>
					<span class="bg-base-color d-inline-block mt-4 sep-line-thick-long">
					</span>
					-->
					
					<div class="col-lg-12 pb-5 centrado">
						<span class="bg-base-color d-inline-block mt-4 sep-line-thick-long centrado">
						</span>
						<div style="clear: both"></div>
					</div>
				</div>
			</div>
			<div class="row">
				<!-- -->
				<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 pt-20"
					ng-repeat="tproductos in tproductos track by $index">
					<div class="card card-body text-center wow fadeInUp">
						<div class="card-ribbon card-ribbon-top card-ribbon-right bg-faded ">
							<div class="icon-box-icon">
								<img class="img-responsive img-servicios img_os" src="{{base_url}}{{tproductos.ruta}}">

							</div>
						</div>
						<h4 class="card-title">
							{{tproductos.titulo}}
						</h4>
						<p class="card-text">
							{{tproductos.descripcion_sin_html}}
						</p>
						<a href="{{base_url}}productos/{{slug_base}}/{{tproductos.slug}}"
							class="btn btn-brand btn-sm mt-3 img_os">Leer
							mas</a>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-12">
					<h3 class="h3-title wow fadeInDown">Otros Productos</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 pt-20"
					ng-repeat="otros_productos in otros_productos track by $index">
					<div class="card card-body text-center wow fadeInDown">
						<div class="card-ribbon card-ribbon-top card-ribbon-right bg-faded ">
							<div class="icon-box-iconr">
								<div class="cursor:pointer">
									<a href="{{base_url}}productos/{{otros_productos.slug}}" class=""><img
											class="img-responsive img-productos img-card-otros-productos"
											src="{{base_url}}{{otros_productos.ruta}}"></a>
								</div>
							</div>
						</div>
						<!--
						<h4 class="card-title">
							{{otros_productos.titulo}}
						</h4>
						-->
					</div>
				</div>


			</div>
			<div style="clear: both"></div>
		</div>
		<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 mt50 wow fadeInUp ">
			<div class="text-center"><a class="btn btn-round btn-lg btn-brand btn-volver"
					href="<?=base_url();?>productos">Volver Productos</a></div>
		</div>
	</section>