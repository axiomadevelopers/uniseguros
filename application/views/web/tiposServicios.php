<!-- Wrapper-->
<div class="wrapper" ng-controller="tiposServiciosController">
	<!-- Parallax de Quienes Somos-->
	<section class="parallax-cabecera">
		<div id="parallaxServicios" class="dzsparallaxer auto-init height-is-based-on-content use-loading">
			<div class="parallax-gradient super_parallax divimage dzsparallaxer--target w-100 g-bg-size-cover g-bg-img-hero g-bg-cover g-bg-black-opacity-0_6--after"
				style="height: 130%; background-image: url(<?=base_url();?>/assets/web/images/parallax/servicios_parallax.png);">
			</div>
			<input type="hidden" id="slug_producto" name="slug_producto" ng-model="slug_producto"
				value="<?php echo $slug_producto;?>">
			<div class="container g-pt-100 g-pb-70">
				<div class="row2">
					<div class="col-sm-6 col-lg-6 align-items-end mt-auto g-mb-50 texto_parallax">
						<div class="text-center">
							<h1 class="d-inline-block g-color-secondary g-font-weight-800 g-font-size-26 mb-0 g-z-index-1 titulos texto-parallax"
								style="color:#fff">SERVICIOS </h1>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="module module-gray">
		<div class="container">
			<!-- -->
			<div class="col-lg-8 col-md-12 col-xs-12 col-sm-12 m-auto bloque-inicial">
				<div class="module-title text-center wow fadeInUp letter-spacing-2 ">
					<h1 class="text-uppercase">{{titulo}}</h1>
					<p class="font-serif" id="subtitulo-qsomos"></p>
					<span class="bg-base-color d-inline-block mt-4 sep-line-thick-long"></span>
				</div>
			</div>
			<!-- -->
			<div class="row">
				<div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 order-md-1">
					<div class="thumbnail wow fadeInLeft">
						<img src="<?=base_url();?>/{{ruta_servicio}}" alt="blog-image">
					</div>

					<div class="caption">
						<div class="row wow fadeInUp">
							<div class="col-lg-12">
								<h3>Servicios asociados</h3>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-3 order-md-2 cuerpoAlgunosProductosServ" >
					<div class="panel panel-default recentBlogPosts">
						<div class="panel-heading wow fadeInRight">
							<h3 class="h3-title title-algunos-pr">Algunos Productos</h3>
						</div>
						<div class="panel-body" ng-repeat="algunos_productos in algunos_productos track by $index">
							<div class="card card-body text-center wow fadeInRight">
								<div class="card-ribbon card-ribbon-top card-ribbon-right bg-faded ">
									<div class="icon-box-icon">
										<div class="">
										<a href="{{base_url}}productos/{{algunos_productos.slug}}" class=""><img class="img-responsive img-productos"
												src="{{base_url}}/{{algunos_productos.ruta}}"></a> 

											
										</div>
									</div>
								</div>
								<!--
								<h4 class="card-title">
									{{algunos_productos.titulo}}
								</h4>
								-->
							</div>
							<!-- -->

						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 pt-20"ng-repeat="tservicios in tservicios track by $index">
					<div class="card card-body text-center wow fadeInUp">
						<div class="card-ribbon card-ribbon-top card-ribbon-right bg-faded ">
							<div class="icon-box-icon">
								<div class="">
									<img class="img-responsive img-quienes-somos"
										src="{{base_url}}{{tservicios.ruta}}">
								</div>
							</div>
						</div>
						<h4 class="card-title">
							{{tservicios.titulo}}
						</h4>
						<p class="card-text">
						{{tservicios.descripcion_sin_html}}
						</p>
						<a href="{{base_url}}servicios/{{slug_base}}/{{tservicios.slug}}"
							class="btn btn-brand btn-sm mt-3">Leer mas</a>
					</div>
				</div>
			</div>
			<!-- -->
		</div>
	</section>