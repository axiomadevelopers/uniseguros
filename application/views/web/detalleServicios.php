<!-- Wrapper-->
<div class="wrapper" ng-controller="detallesServiciosController">
	<!-- Parallax de Quienes Somos-->
	<section class="parallax-cabecera">
		<div id="parallaxServicios" class="dzsparallaxer auto-init height-is-based-on-content use-loading">
	    	<div class="parallax-gradient super_parallax divimage dzsparallaxer--target w-100 g-bg-size-cover g-bg-img-hero g-bg-cover g-bg-black-opacity-0_6--after"
	        style="height: 130%; background-image: url(<?=base_url();?>/assets/web/images/parallax/servicios_parallax.png);"></div>
			<input type="hidden" id="slug" name="slug" ng-model="slug" value="<?php echo $slug_servicio;?>">
			<input type="hidden" id="slug_base" name="slug_base" ng-model="slug_base" value="<?php echo $slug_base;?>">
		    <div class="container g-pt-100 g-pb-70">
		        <div class="row2">
		            <div class="col-sm-6 col-lg-6 text-uppercase align-items-end mt-auto g-mb-50 texto_parallax">
		                <div class="text-center">
		                    <h1 class="d-inline-block g-color-secondary g-font-weight-800 g-font-size-26 mb-0 g-z-index-1 titulos texto-parallax"
		                        style="color:#fff">{{servicio_base.titulo}}</h1>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</section>	
	<section class="module">
		<div class="container">
			<div class="row justify-content-center pb-5">
                <div class="col-lg-9 pb-lg-4 text-center">
                	<div class="icon-box-icon">
						<div class="">
						<img class="img-responsive img-quienes-somos wow fadeInUp"
								ng-src="<?=base_url();?>/{{tipo_servicio.ruta}}">						</div>
					</div>
                    <h1 class="letter-spacing-2 text-uppercase module-title2 wow fadeInUp ">{{tipo_servicio.titulo}}</h1>
                    <p class="font-serif"> </p>
                    <span class="bg-base-color d-inline-block mt-4 sep-line-thick-long">
                    </span>
                </div>
            </div>
			<!-- -->
			<div class="row">
	            <div class="col-lg-9 col-md-12">
	              <div class="row">
	                <div class="col-md-12">
		                <div class="thumbnail wow fadeInLeft">
		                    <img ng-src="<?=base_url();?>/{{servicio_base.ruta}}" alt="blog-image">
		                    <div class="caption cuerpo-tab">
		                      <div class="row">
		                        <div class="col-md-12 order-1">
		                        	<h3 class="h3-title">{{titulo_detalle_servicio}}</h3>
									<p id="descripcion_detalle_servicio" name="descripcion_detalle_servicio">
											</p>
		                        </div>
		                      </div>
		                    </div>
		                    <ul class="pager hide">
		                      <li class="previous"><a href="blog-single-right-sidebar.html#">previous</a></li>
		                      <li class="next float-right"><a href="blog-single-right-sidebar.html#">next</a></li>
		                    </ul>
		                    <div class="cuerpo-tab-movil">
								<div class="display-single_element">
									<!-- Nav Bar Tabs row starts here -->
									<div class="row">
										<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 padding0">
											<div id="accordion">
												<!-- -->
											  	<div class="card" ng-repeat="detalles_servicios in detalles_servicios track by $index">
											    	<div class="card-header" id="heading{{$index}}">
											      		<h5 class="mb-0">
													        <button class="btn btn-link btn-link-productos" data-toggle="collapse" data-target="#collapse{{$index}}" aria-expanded="true" aria-controls="collapse{{$index}}" ng-class="{'acordion-text-selected':$index==0}"
															ng-click="activarAcordeon($index)"
															id="acordeon{{$index}}"	
													        >
													         {{detalles_servicios.titulo}}
													        </button>
											      		</h5>
											    	</div>

												    <div id="collapse{{$index}}" class="collapse" ng-class="{'show':$index==0}"aria-labelledby="heading{{$index}}" data-parent="#accordion">
												      <div class="card-body descripcionAcordeon" id="descripcion_detalle_servicio2-{{$index}}" name="descripcion_detalle_servicio2-{{$index}}" ng-bind-html="detalles_servicios.descripcion">
												      </div>
												    </div>
												</div>
											<!-- --> 
											</div>
										</div>
									</div><!-- Nav Bar Tabs row ends here -->
								</div>
							</div>
		                </div>
	                </div>
	              </div>
	            </div>
	            <div class="col-lg-3 col-md-12 sideBar cuerpo-tab">
		            <div class="panel panel-default">
		                <div class="panel-heading wow fadeInRight">
		                	<h3 class="h3-title">
		                		Detalles de servicios
		                	</h3>
		                </div>
		                <div class="panel-body wow fadeInRight">
		                  <div class="col-lg-12 padding0">
							<ul class="nav nav-tabs nav-stacked">
								<li class="nav-item superItem"
										ng-repeat="detalles_servicios in detalles_servicios track by $index">
										<a id="detalle_prod{{$index}}" name="detalle_prod{{$index}}"
											href="basic-tabs.html#vtab1" class="nav-link"
											data-ng-click="detalle_seleccionado($event)"
											data="{{detalles_servicios.titulo}}|{{detalles_servicios.descripcion}}"
											ng-class="{'active':detalles_servicios.orden==orden_detalles_servicios}"
											data-toggle="tab" aria-expanded="true">
											{{detalles_servicios.titulo}}
										</a>
									</li>
							</ul>
						</div>
		            	</div>
		            </div>
		            <div class="panel panel-default recentBlogPosts cuerpoAlgunosProductosDetalleServicios">
		                <div class="panel-heading wow fadeInUp">
		                	<h3 class="h3-title">Algunos Productos</h3>
		              	</div>
						  <div class="panel-body" ng-repeat="algunos_productos in algunos_productos track by $index">
							<div class="card card-body text-center wow fadeInUp">
								<div class="card-ribbon card-ribbon-top card-ribbon-right bg-faded ">
									<div class="icon-box-icon">
										<div class="">
										<a href="{{base_url}}productos/{{algunos_productos.slug}}" class=""><img class="img-responsive img-productos"
												src="{{base_url}}/{{algunos_productos.ruta}}"></a> 

											
										</div>
									</div>
								</div>
								<!--
								<h4 class="card-title">
									{{algunos_productos.titulo}}
								</h4>
								-->
							</div>
							<!-- -->

						</div>
		            </div>
	            </div>
	        </div>
				<!-- -->
			<div class="row wow fadeInUp">
				<div class="col-lg-12">
					<h3 class="h3-title">Otros Servicios</h3>
				</div>
			</div>
			
            
            <!-- -->
	       <div class="row">
		   <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 pt-20"
					ng-repeat="otros_servicios in otros_servicios track by $index">
					<div class="card card-body text-center wow fadeInUp">
						<div class="card-ribbon card-ribbon-top card-ribbon-right bg-faded ">
							<div class="icon-box-icon">
								<img class="img-responsive img-quienes-somos"
									src="<?=base_url();?>{{otros_servicios.ruta}}">
							</div>
						</div>
						<h4 class="card-title">
							{{otros_servicios.titulo}}
						</h4>

						<a href="{{base_url}}servicios/{{slug_base}}/{{otros_servicios.slug}}"
							class="btn btn-brand btn-sm mt-3">Leer mas</a>
					</div>
				</div>
			</div>
            <!-- -->
		</div>
		<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 mt50 wow fadeInUp ">
			<div class="text-center"><a class="btn btn-round btn-lg btn-brand"
					href="{{base_url}}servicios/{{slug_base}}">Volver</a></div>
		</div>
	</section>	 