<!-- -->
	<div id="preloader_zougzoug" class="" >
      <div class="loading-spiner">
        <!--Servidor -->
        <!-- Local: -->
        <div class="" style="margin: 0 auto;display: flex;">
          <div id="loader-container"
            style="background-image:url(<?=base_url();?>assets/web/images/logo-black.png);background-position:center;background-repeat:no-repeat;">
            <div class="loader"></div>
            <div>
              <img src="<?=base_url();?>/assets/web/images/30.gif" class="img-responsive"
                style="margin: 0 auto;display: flex;">
            </div>
            <div style="clear:both"></div>
          </div>
        </div>
      </div>
    </div>
<!-- Layout-->
<div class="layout">
	
	<!-- Header-->
	<header class="header header-right undefined">
		<div class="container-fluid">
			<!-- Logos-->
			<div class="inner-header">
				<a class="inner-brand a-menu0"> 
					<img class="brand-dark img-header" src="<?=base_url();?>assets/web/images/logo-black.png" alt="">
					<img class="brand-light img-header" src="<?=base_url();?>assets/web/images/logo-black.png" alt="">
					<!-- Core-->
				</a>
			</div>
			<!-- Navigation-->
			<div class="inner-navigation collapse">
				<div class="inner-navigation-inline">
					<div class="inner-nav">
						<ul>
							<!-- Home-->
							<li class="">
								<a href="<?=base_url();?>" class="a-menu0">
									<div id="span-menu0" class="span-menu menu0">Inicio</div>
								</a>
							</li>
							<!-- Home end-->
							<!-- Quienes somos-->
							<li class="">
								<a href="<?=base_url();?>quienes_somos" class="a-menu1">
									<div id="span-menu1" class="span-menu menu1">Quienes somos</div>
								</a>
							</li>
							<!-- Quienes somos end-->
							<!-- Productos-->
							<li class="">
								<a href="<?=base_url();?>productos" class="a-menu2">
									<div id="span-menu2" class="span-menu menu2">Productos</div>
								</a>
							</li>
							<!-- Productos end-->
							<!-- Servicios-->
							<li class="">
								<a href="<?=base_url();?>servicios" class="a-menu4">
									<div id="span-menu3" class="span-menu menu4">Servicios</div>
								</a>
							</li>
							<!-- Servicios end-->
							<!-- Contactanos-->
							<li class="">
								<a href="<?=base_url();?>contactanos" class="a-menu3">
									<div id="span-menu4" class="span-menu menu3">Contáctanos
									</div>	
								</a>
							</li>
							<!-- Contactanos end-->
							
						</ul>
					</div>
				</div>
			</div>
			<!-- Extra menu-->
			<!--<div class="extra-nav">
				<ul>
					<li><a class="btn-header" href="http://admin-tufactoring.000webhostapp.com" target="_blank"><span class="btn btn-primary ">Login</span></a></li>
				</ul>
			</div>-->
			<!-- Mobile menu-->
			<div class="nav-toggle"><a href="index-21.html#" data-toggle="collapse" data-target=".inner-navigation"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></a></div>
		</div>
	</header>