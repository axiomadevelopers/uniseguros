<?php

if (!defined('BASEPATH')) exit ('No direct script access allowed');

Class WebProductos_model extends CI_Model{
	/*
	*	consultarProductos
	*/
	public function consultarProductos($data){
	 $this->db->order_by('a.orden','ASC');
	 $this->db->where('a.estatus!=',2);
	 $this->db->where('a.estatus!=',0);
	 $this->db->where('a.estatus=',1);
	 $this->db->select('a.*, b.id as id_idioma, b.descripcion as descripcion_idioma, c.ruta as ruta, c.id as id_imagen');
	 $this->db->from('productos a');
	 $this->db->join('idioma b', 'b.id = a.id_idioma');
	 $this->db->join('galeria c', 'c.id = a.id_imagen');
	 $res = $this->db->get();
	 
	 if($res){
		 return $res->result();
	 }else{
		 return false;
	 }
	}
	public function consultarServicios($data){
		$this->db->order_by('a.orden','ASC');
		$this->db->where('a.estatus!=',2);
	 $this->db->where('a.estatus!=',0);
	 $this->db->where('a.estatus=',1);
		$this->db->select('a.*, b.id as id_idioma, b.descripcion as descripcion_idioma, c.ruta as ruta, c.id as id_imagen');
		$this->db->from('servicios a');
		$this->db->join('idioma b', 'b.id = a.id_idioma');
		$this->db->join('galeria c', 'c.id = a.id_imagen');
		$this->db->limit(3);

		$res = $this->db->get();
		
		if($res){
			return $res->result();
		}else{
			return false;
		}
	   }
	
	 /*
	*	consultarTproducto
	*/
	public function consultarTproductos($id){
		$this->db->order_by('a.orden','ASC');
		$this->db->where('a.estatus!=',2);
	 $this->db->where('a.estatus!=',0);
	 $this->db->where('a.estatus=',1);
		$this->db->where('a.id_productos', $id);

		$this->db->select('a.*, c.ruta as ruta, c.id as id_imagen');
		$this->db->from('tipo_producto a');
		$this->db->join('galeria c', 'c.id = a.id_imagen');
		$res = $this->db->get();
		
		if($res){
			return $res->result();
		}else{
			return false;
		}
	   }
	/*
	*	Consultar Descripción
	*/
	   public function consultarDescripcion($id,$id_producto){
		$this->db->where('a.estatus!=',2);
	 $this->db->where('a.estatus!=',0);
	 $this->db->where('a.estatus=',1);
		$this->db->where('a.id_productos', $id_producto);
		$this->db->where('a.id_tipo_producto', $id);
		$this->db->where('a.orden', 1);

		$this->db->select('a.descripcion');
		$this->db->from('detalle_productos a');
		$res = $this->db->get();
		
		if($res){
			return $res->row_array();
		}else{
			return false;
		}
	   }
	/*
	*	consultarOtrosProductos
	*/
		public function consultarOtrosTproductos($id){

		$this->db->order_by('a.orden','ASC');
		$this->db->where('a.id!=', $id);
		 $this->db->where('a.estatus!=',2);
	 $this->db->where('a.estatus!=',0);
	 $this->db->where('a.estatus=',1);
		 $this->db->select('a.*, c.ruta as ruta, c.id as id_imagen');
		 $this->db->from('productos a');
		 $this->db->join('galeria c', 'c.id = a.id_imagen');
		 $this->db->limit(3);
		 $res = $this->db->get();
		 
		 if($res){
			 return $res->result();
		 }else{
			 return false;
		 }
		}
	 /*
	*	consultarTproductos == Producto inicial
	*/
	public function consultarProductoSlug($data){
		$this->db->order_by('a.orden','ASC');
		$this->db->where('a.slug', $data["slug_primario"]);

		 $this->db->where('a.estatus!=',2);
		 $this->db->where('a.estatus!=',0);
		 $this->db->where('a.estatus=',1);
		 $this->db->select('a.*, c.ruta as ruta, c.id as id_imagen');
		 $this->db->from('productos a');
		 $this->db->join('galeria c', 'c.id = a.id_imagen');
		 $res = $this->db->get();
		 
		 if($res){
			 return $res->row_array();
		 }else{
			 return false;
		 }
	}
	/*
	*
	*/
	public function consultarProductoSlugExiste($slug,$slug_base){
		
		if($slug!=""){
			$this->db->where('a.slug',$slug);
		}
		
		if($slug_base!=""){
			$this->db->where('b.slug', $slug_base);
	 	}

	 	$this->db->where('a.estatus=',1);
		$this->db->select('a.*');
		$this->db->from('tipo_producto a');
		$this->db->join('productos b', 'b.id = a.id_productos');
		return $this->db->count_all_results();
		//$res = $this->db->get();
		//print_r($this->db->last_query());die;

	}
	 /*
	}
	*	consultarTproductos == Producto segundario
	*/
	public function consultarProductoSlugSegundario($data){
		$this->db->order_by('a.orden','ASC');

	$this->db->where('a.slug', $data["slug_segundario"]);

	 $this->db->where('a.estatus!=',2);
	 $this->db->where('a.estatus!=',0);
	 $this->db->where('a.estatus=',1);
	 $this->db->select('a.*, c.ruta as ruta, c.id as id_imagen');
	 $this->db->from('tipo_producto a');
	 $this->db->join('galeria c', 'c.id = a.id_imagen');
	 $res = $this->db->get();
	 
	 if($res){
		 return $res->row_array();
	 }else{
		 return false;
	 }
	}
	 /*
	*	consultarDetallesTproducto
	*/
	public function consultarDetalleTproductos($producto_base,$tipo_producto){
		$this->db->order_by('a.orden','ASC');
	 	$this->db->where('a.estatus=',1);
		$this->db->where('a.id_productos', $producto_base);
		$this->db->where('a.id_tipo_producto', $tipo_producto);
		$this->db->select('a.*');
		$this->db->from('detalle_productos a');
		$res = $this->db->get();
		
		if($res){
			return $res->result();
		}else{
			return false;
		}
	   }
	   /*
	*	consultarOtrosDetallesTproductos
	*/
		public function consultarOtrosDetallesTproductos($producto_base,$tipo_producto){

			$this->db->order_by('a.orden','ASC');
			$this->db->where('a.id_productos=', $producto_base);
			$this->db->where('a.id!=', $tipo_producto);
			 $this->db->where('a.estatus!=',2);
	 $this->db->where('a.estatus!=',0);
	 $this->db->where('a.estatus=',1);
			 $this->db->select('a.*, c.ruta as ruta, c.id as id_imagen');
			 $this->db->from('tipo_producto a');
			 $this->db->join('galeria c', 'c.id = a.id_imagen');
			 //$this->db->limit(3);
			 $res = $this->db->get();
			 
			 if($res){
				 return $res->result();
			 }else{
				 return false;
			 }
			}

}