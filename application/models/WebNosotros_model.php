<?php

if (!defined('BASEPATH')) exit ('No direct script access allowed');

Class WebNosotros_model extends CI_Model{
	/*
	*	consultarNosotros
	*/
	public function consultarNosotros($data){
		$this->db->where('a.id_idioma', $data["id_idioma"]);
		$this->db->order_by('a.orden');
        $this->db->where('a.estatus!=',2);
		$this->db->select('a.*, b.id as id_idioma, b.descripcion as descripcion_idioma');
		$this->db->from('nosotros a');
		$this->db->join('idioma b', 'b.id = a.id_idioma');
		$res = $this->db->get();

		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	/*
	*	Consultar Imagen
	*/
	public function consultarImg($idImagen){
		$this->db->where('a.id', $idImagen);
		$this->db->order_by('a.id', 'DESC');
		$this->db->select('a.*');
		$this->db->from('galeria a');
		$res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	/*
	*	consultarDirectiva
	*/
	public function consultarDirectiva($data){
			if($data["id_directiva"]!=""){
				$this->db->where('a.id', $data["id_directiva"]);
			}
			$this->db->order_by('a.id_idioma','ASC');
			$this->db->order_by('a.orden','ASC');
			$this->db->order_by('a.id','DESC');
	        $this->db->where('a.estatus!=',2);
			$this->db->select('a.*, b.id as id_idioma, b.descripcion as descripcion_idioma, c.ruta as ruta, c.id as id_imagen');
			$this->db->from('directiva a');
			$this->db->join('idioma b', 'b.id = a.id_idioma');
	        $this->db->join('galeria c', 'c.id = a.id_imagen');
			$res = $this->db->get();
			//print_r($res);die;
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}
	/*
	*	consultarReaseguradoras
	*/
	public function consultarReaseguradoras($data){
		if($data["id_reaseguradoras"]!=""){
			$this->db->where('a.id', $data["id_reaseguradoras"]);
		}
		$this->db->order_by('a.id_idioma','ASC');
		$this->db->order_by('a.orden','ASC');
		$this->db->order_by('a.id','DESC');
        $this->db->where('a.estatus!=',2);
		$this->db->select('a.*, b.id as id_idioma, b.descripcion as descripcion_idioma, c.ruta as ruta, c.id as id_imagen');
		$this->db->from('reaseguradoras a');
		$this->db->join('idioma b', 'b.id = a.id_idioma');
        $this->db->join('galeria c', 'c.id = a.id_imagen');
		$res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}

}