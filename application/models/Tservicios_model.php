<?php 

if (!defined('BASEPATH')) exit ('No direct script access allowed');

Class Tservicios_model extends CI_Model{

	public function iniciar_sesion($login,$clave){
		$this->db->where('login',$login);
		$this->db->where('clave',$clave);
		$this->db->where('estatus','1');
		$this->db->select('*');
		$this->db->from(' usuarios u');
		return $this->db->count_all_results();
	}
	public function consultarServicios($data){
		$this->db->order_by('a.orden','asc');
		if($data["id_idioma"]!=""){
			$this->db->where('a.id_idioma', $data["id_idioma"]);
		}
		$this->db->where('a.estatus!=',2);
		$this->db->select('a.id,a.titulo');
		$this->db->from('servicios a');
		$res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
		
	}
	public function guardarServicio($data){

		if($this->db->insert("tipo_servicios", $data)){
			return true;
		}else{
			return false;
		}
	}

	public function consultarTservicios($data){
		$this->db->order_by('a.id_servicios','asc');
		$this->db->order_by('a.orden','asc');		
		if($data["id_servicios"]!=""){

			$this->db->where('a.id', $data["id_servicios"]);
		}
        $this->db->where('a.estatus!=',2);
		$this->db->select('a.*, b.id as id_idioma, b.descripcion as descripcion_idioma, c.ruta as ruta, c.id as id_imagen, d.titulo as descripcion_servicio, d.orden as numero_producto');
		$this->db->from('tipo_servicios a');
		$this->db->join('idioma b', 'b.id = a.id_idioma');
		$this->db->join('galeria c', 'c.id = a.id_imagen');
		$this->db->join('servicios d', 'd.id = a.id_servicios');

		$res = $this->db->get();
        
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}

	public function modificarServicios($data){
		$this->db->where('id', $data["id"]);
        if($this->db->update("tipo_servicios", $data)){
        //------------------------------------
        	if(isset($data["estatus"])){
        		if($data["estatus"]=="2"){
	        		$this->reiniciar_orden(1,$data["id_servicios"]);
	        	}
        	}
        	//------------------------------------
        	return true;
        }else{
        	return false;
        }
	}
	/*
	*	Reiniciar Orden
	*/
	public function reiniciar_orden($id_idioma,$id_servicios){
		/*
		*	Consulto los slider que esten activos y reinicio el orden
		*/

		$this->db->order_by('a.orden','ASC');
        $this->db->where('a.estatus!=',2);
        $this->db->where('a.id_servicios',$id_servicios);
        $this->db->where('a.id_idioma',$id_idioma);
		$this->db->select('a.*');
		$this->db->from('tipo_servicios a');
		$this->db->join('idioma b', 'b.id = a.id_idioma');
		$res = $this->db->get();
		$recordset = $res->result();
		if($recordset){
			$contador = 1;
			foreach ($recordset as $clave => $valor) {
				$data2 = array("orden"=>$contador);
				$this->db->where('id', $valor->id);
        		$a = $this->db->update("tipo_servicios", $data2);
        		$contador++;	
			}
		}
		/***/
	}

	public function consultarExisteTitulo($id,$titulo){
		$this->db->where('n.id !=',$id);
		$this->db->where('n.titulo',$titulo);
		$this->db->where('estatus','1');
		$this->db->select('*');
		$this->db->from('tipo_servicios n');
		return $this->db->count_all_results();
	}
	public function consultarOrden($data){
		
		if($data["id_idioma"]!=""){
			$this->db->where('a.id_idioma', $data["id_idioma"]);
		}
		if($data["id_productos"]!=""){
        	$this->db->where('a.id_servicios',intVal($data["id_productos"]));
		}
		$this->db->order_by('a.orden','ASC');
        $this->db->where('a.estatus!=',2);
		$this->db->select('a.orden');
		$this->db->from('tipo_servicios	 a');
		$this->db->join('idioma b', 'b.id = a.id_idioma');
		
		$res = $this->db->get();

		if($res->num_rows()>0){
			return $res->result();
		}else{
			return false;
		}
	}
	public function posicionar_modulos($posicionar){
		//var_dump($posicionar);die();
		//---
		if($posicionar['tipo'] == 'insert'){

			$this->db->where('orden >= ' . $posicionar['posicion']);

			$this->db->where('id_idioma = ' . $posicionar['id_idioma']);
			
			$this->db->where('id_servicios = ' . $posicionar['id_servicios']);


			$resultados = $this->db->get("tipo_servicios");

			   //print_r($this->db->last_query());die;

			if($resultados->num_rows() > 0){

				foreach ($resultados->result() as $row){

					$datos=array(
						'orden' => $row->orden + 1,
					);

					$this->db->where('id', $row->id);

					$this->db->update("tipo_servicios",$datos);
				}

			}
		}else if($posicionar['tipo'] == 'update'){

			if($posicionar['final'] > $posicionar['inicial']){

				$this->db->where('orden > ' . $posicionar['inicial'] . ' AND orden <= ' . $posicionar['final']);

				   $this->db->where('id_idioma = ' . $posicionar['id_idioma']);
				   
				$this->db->where('id_servicios = ' . $posicionar['id_servicios']);


				$resultados = $this->db->get("tipo_servicios");
				if($resultados){
					if($resultados->num_rows() > 0){
						foreach ($resultados->result() as $row){
							$datos=array(
								'orden' => $row->orden - 1,
							);
							$this->db->where('id', $row->id);
							$this->db->update("tipo_servicios", $datos);
						}
					}
				}

			}else if($posicionar['final'] < $posicionar['inicial']){

					$this->db->where('orden >= ' . $posicionar['final'] . ' AND orden < ' . $posicionar['inicial']);

					$this->db->where('id_idioma = ' . $posicionar['id_idioma']);
					
					$this->db->where('id_servicios = ' . $posicionar['id_servicios']);


					$resultados = $this->db->get("tipo_servicios");
					if($resultados){
						if($resultados->num_rows() > 0){
							foreach ($resultados->result() as $row){
								$datos=array(
									'orden' => $row->orden + 1,
								);
								$this->db->where('id', $row->id);
								$this->db->update("tipo_servicios", $datos);
							}
						}
					}

			}
		//---
		}
//--
	}

}