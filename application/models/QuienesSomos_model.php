<?php
if (!defined('BASEPATH')) exit ('No direct script access allowed');

class QuienesSomos_model extends CI_Model{

	public function iniciar_sesion($login,$clave){
		$this->db->where('login',$login);
		$this->db->where('clave',$clave);
		$this->db->where('estatus','1');
		$this->db->select('*');
		$this->db->from(' usuarios u');
		return $this->db->count_all_results();
	}

	public function guardarNosotros($data){
		//print_r($data);die;
		if($this->db->insert("nosotros", $data)){
			return true;
		}else{
			return false;
		}
	}

	public function consultarNosotrosTodas($data){

		if($data["id_nosotros"]!=""){
			$this->db->where('a.id', $data["id_nosotros"]);
		}
		$this->db->order_by('a.id', 'DESC');
        $this->db->where('a.estatus!=',2);
		$this->db->select('a.*,b.id as id_idioma, b.descripcion as descripcion_idioma');
		$this->db->from('nosotros a');
		$this->db->join('idioma b', 'b.id = a.id_idioma');
		$res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}

	public function consultarExiste($id){
		if($id!=""){
			$this->db->where('a.id', $id);
		}
		$this->db->select('a.*');
		$this->db->from('nosotros a');
		$res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}

	public function modificarNosotros($data){
		$this->db->where('id', $data["id"]);
		if($this->db->update("nosotros", $data)){
			return true;
		}else{
			return false;
		}
	}

	public function modificarNosotrosEstatus($data){
		$this->db->where('id', $data["id"]);
		if($this->db->update("nosotros", $data)){
			return true;
		}else{
			return false;
		}
	}

	public function consultarOrden($data){
		
		if($data["id_idioma"]!=""){
			$this->db->where('a.id_idioma', $data["id_idioma"]);
		}
		
		$this->db->order_by('a.orden','ASC');
        $this->db->where('a.estatus!=',2);
		$this->db->select('a.orden');
		$this->db->from('nosotros a');
		$this->db->join('idioma b', 'b.id = a.id_idioma');
		
		$res = $this->db->get();

		if($res->num_rows()>0){
			return $res->result();
		}else{
			return false;
		}
	}
	//----
	public function posicionar_modulos($posicionar){
			//var_dump($posicionar);die();
			//---
			if($posicionar['tipo'] == 'insert'){

	            $this->db->where('orden >= ' . $posicionar['posicion']);

	            $this->db->where('id_idioma = ' . $posicionar['id_idioma']);

	            $resultados = $this->db->get("nosotros");

       			//print_r($this->db->last_query());die;

	            if($resultados->num_rows() > 0){

	                foreach ($resultados->result() as $row){

	                    $datos=array(
	                        'orden' => $row->orden + 1,
	                    );

	                    $this->db->where('id', $row->id);

	                    $this->db->update("nosotros",$datos);
	                }

	            }
	        }else if($posicionar['tipo'] == 'update'){

	            if($posicionar['final'] > $posicionar['inicial']){

	                $this->db->where('orden > ' . $posicionar['inicial'] . ' AND orden <= ' . $posicionar['final']);

       	            $this->db->where('id_idioma = ' . $posicionar['id_idioma']);

	                $resultados = $this->db->get("nosotros");
	                if($resultados){
		                if($resultados->num_rows() > 0){
		                    foreach ($resultados->result() as $row){
		                        $datos=array(
		                            'orden' => $row->orden - 1,
		                        );
		                        $this->db->where('id', $row->id);
		                        $this->db->update("nosotros", $datos);
		                    }
		                }
		            }

	            }else if($posicionar['final'] < $posicionar['inicial']){

		                $this->db->where('orden >= ' . $posicionar['final'] . ' AND orden < ' . $posicionar['inicial']);

		                $this->db->where('id_idioma = ' . $posicionar['id_idioma']);

		                $resultados = $this->db->get("nosotros");
		                if($resultados){
		                	if($resultados->num_rows() > 0){
			                    foreach ($resultados->result() as $row){
			                    	$datos=array(
			                            'orden' => $row->orden + 1,
			                        );
			                        $this->db->where('id', $row->id);
			                        $this->db->update("nosotros", $datos);
			                    }
			                }
		                }

			    }
	        //---
			}
	//--
	}
	/*
	*	Consultar Imagen
	*/
	public function consultarImg($idImagen){
		$this->db->where('a.id', $idImagen);
		$this->db->order_by('a.id', 'DESC');
		$this->db->select('a.*');
		$this->db->from('galeria a');
		$res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	/***/
}

?>
