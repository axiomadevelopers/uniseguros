<?php 

if (!defined('BASEPATH')) exit ('No direct script access allowed');

Class Banco_model extends CI_Model{

	
//--- Apartado de bancos ---///
	public function guardarBanco($data){

		if($this->db->insert("bancos", $data)){
			return true;
		}else{
			return false;
		}
	}

	public function consultarBanco($data){
   		$this->db->order_by('a.id','ASC');

		if($data["id_banco"]!=""){
			$this->db->where('a.id', $data["id_banco"]);
		}
        $this->db->where('a.estatus!=',2);
		$this->db->select('a.*');
		$this->db->from('bancos a');
		//$this->db->join('idioma b', 'b.id = a.id_idioma'); b.id as id_idioma, b.descripcion as descripcion_idioma,
		$res = $this->db->get();
        
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}

	public function modificarBanco($data){
		$this->db->where('id', $data["id"]);
        if($this->db->update("bancos", $data)){
        	//------------------------------------
        	//if(isset($data["estatus"])){
        	//	if($data["estatus"]=="2"){
	        //		$this->reiniciar_orden(1);
	        //	}
        	//}
        	//------------------------------------
        	return true;
        }else{
        	return false;
        }
	}
	/*
	*	Reiniciar Orden
	*/
	public function reiniciar_orden($id_idioma){
		/*
		*	Consulto los slider que esten activos y reinicio el orden
		*/

		$this->db->order_by('a.orden','ASC');
	    $this->db->where('a.estatus!=',2);
	    $this->db->where('a.id_idioma',$id_idioma);
		$this->db->select('a.*');
		$this->db->from('bancos a');
		$this->db->join('idioma b', 'b.id = a.id_idioma');
		$res = $this->db->get();
		$recordset = $res->result();
		if($recordset){
			$contador = 1;
			foreach ($recordset as $clave => $valor) {
				$data2 = array("orden"=>$contador);
				$this->db->where('id', $valor->id);
	    		$a = $this->db->update("bancos", $data2);
	    		$contador++;	
			}
		}
		/***/
	}
	public function consultarExisteBanco($id,$descripcion){
		$this->db->where('n.id !=',$id);
		$this->db->where('n.descripcion',$descripcion);
		$this->db->where('estatus','1');
		$this->db->select('*');
		$this->db->from('bancos n');
		//print_r($this->db->last_query());die;

		return $this->db->count_all_results();
	}
	public function consultarExiste($id){
			if($id!=""){
				$this->db->where('a.id', $id);
			}
			$this->db->select('a.*');
			$this->db->from('bancos a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}
	public function consultarOrden($data){
		
		if($data["id_idioma"]!=""){
			$this->db->where('a.id_idioma', $data["id_idioma"]);
		}
		
		$this->db->order_by('a.orden','ASC');
        $this->db->where('a.estatus!=',2);
		$this->db->select('a.orden');
		$this->db->from('bancos a');
		$this->db->join('idioma b', 'b.id = a.id_idioma');
		
		$res = $this->db->get();

		if($res->num_rows()>0){
			return $res->result();
		}else{
			return false;
		}
	}
	public function posicionar_modulos($posicionar){
		//var_dump($posicionar);die();
		//---
		if($posicionar['tipo'] == 'insert'){

			$this->db->where('orden >= ' . $posicionar['posicion']);

			$this->db->where('id_idioma = ' . $posicionar['id_idioma']);

			$resultados = $this->db->get("bancos");

			   //print_r($this->db->last_query());die;

			if($resultados->num_rows() > 0){

				foreach ($resultados->result() as $row){

					$datos=array(
						'orden' => $row->orden + 1,
					);

					$this->db->where('id', $row->id);

					$this->db->update("bancos",$datos);
				}

			}
		}else if($posicionar['tipo'] == 'update'){

			if($posicionar['final'] > $posicionar['inicial']){

				$this->db->where('orden > ' . $posicionar['inicial'] . ' AND orden <= ' . $posicionar['final']);

				   $this->db->where('id_idioma = ' . $posicionar['id_idioma']);

				$resultados = $this->db->get("bancos");
				if($resultados){
					if($resultados->num_rows() > 0){
						foreach ($resultados->result() as $row){
							$datos=array(
								'orden' => $row->orden - 1,
							);
							$this->db->where('id', $row->id);
							$this->db->update("bancos", $datos);
						}
					}
				}

			}else if($posicionar['final'] < $posicionar['inicial']){

					$this->db->where('orden >= ' . $posicionar['final'] . ' AND orden < ' . $posicionar['inicial']);

					$this->db->where('id_idioma = ' . $posicionar['id_idioma']);

					$resultados = $this->db->get("bancos");
					if($resultados){
						if($resultados->num_rows() > 0){
							foreach ($resultados->result() as $row){
								$datos=array(
									'orden' => $row->orden + 1,
								);
								$this->db->where('id', $row->id);
								$this->db->update("bancos", $datos);
							}
						}
					}

			}
		//---
		}
//--
}
}