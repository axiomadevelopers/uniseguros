<?php
	if (!defined('BASEPATH')) exit ('No direct script access allowed');

	class RegistroCMS_model extends CI_Model{

		public function iniciar_sesion($login,$clave){
			$this->db->where('login',$login);
			$this->db->where('clave',$clave);
			$this->db->where('estatus','1');
			$this->db->select('*');
			$this->db->from(' usuarios u');
			return $this->db->count_all_results();
		}

		public function guardarPersonas($data){
			//print_r ($data);die;
			if($this->db->insert("personas",$data)){
				return true;
			}else{
				return false;
			}
		}
		public function guardarUsuarios($data){
			//print_r ($data);die;
			if($this->db->insert("usuarios",$data)){
				return true;
			}else{
				return false;
			}
		}

		public function consultar_personas($data){
			if($data["id_personas"]!=""){
				$this->db->where('a.id', $data['id_personas']);
			}
			$this->db->order_by('a.id','DESC');
	        $this->db->where('a.estatus!=',2);
			//$this->db->select('a.*, b.id as id_idioma, b.descripcion as descripcion_idioma, c.ruta as ruta, c.id as id_imagen');
			$this->db->select('a.*');
			$this->db->from('personas a');
	        //$this->db->join('galeria c', 'c.id = a.id_imagen');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}
		public function consultar_usuarios($id){

			$this->db->where('a.id_persona',$id);
			$this->db->where('a.estatus!=',2);
			$this->db->select('a.*, b.id as id_tipo_usuario, b.descripcion as tipo_usuario,c.ruta as ruta, c.id as id_imagen');
			$this->db->from('usuarios a');
			$this->db->join('galeria c', 'c.id = a.id_imagen');
	        $this->db->join('tipos_usuarios b', 'b.id = a.id_tipo_usuario');
			$res = $this->db->get();

			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function existePersonas($cedula,$id){
			if($id!=""){
				$this->db->where('a.id', $id);
			}
			$this->db->where('a.estatus!=',2);
			$this->db->where('a.cedula', $cedula);
			$this->db->select('a.*');
			$this->db->from('personas a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}
		public function existeUsuarios($login,$id){
			if($id!=""){
				$this->db->where('a.id_persona', $id);
			}
			$this->db->where('a.estatus!=',2);
			$this->db->where('a.login', $login);
			$this->db->select('a.*');
			$this->db->from('usuarios a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function modificarPersonas($data){
			$this->db->where('id', $data["id"]);
	        if($this->db->update("personas", $data)){
	        	return true;
	        }else{
	        	return false;
	        }
		}
		public function modificarUsuarios($data){
			$this->db->where('id_persona', $data["id_persona"]);
	        if($this->db->update("usuarios", $data)){
	        	return true;
	        }else{
	        	return false;
	        }
		}

		public function modificarUsuarioEstatusUsuario($data){
			$this->db->where('id_persona', $data["id"]);
	        if($this->db->update("usuarios", $data)){
	        	return true;
	        }else{
	        	return false;
	        }
		}
		public function modificarUsuarioEstatusPersona($data){
			$this->db->where('id', $data["id"]);
	        if($this->db->update("personas", $data)){
	        	return true;
	        }else{
	        	return false;
	        }
		}
		public function existeModificarPersonas($cedula,$id){
			if($id!=""){

				$this->db->where('a.id!=', $id);
			}
			$this->db->where('a.estatus!=',2);
			$this->db->where('a.cedula', $cedula);
			$this->db->select('a.*');
			$this->db->from('personas a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}
		public function existeModificarUsuarios($login,$id){
			if($id!=""){
				$this->db->where('a.id_persona!=', $id);
			}
			$this->db->where('a.estatus!=',2);
			$this->db->where('a.login', $login);
			$this->db->select('a.*');
			$this->db->from('usuarios a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}
		
		
		
		public function consultarTiposUsuarios($data){
		
		
			$this->db->order_by('a.id');
			$this->db->select('a.id,a.descripcion');
			$this->db->from('tipos_usuarios a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
			
		}
		public function tomarID($cedula){
			$this->db->where('a.cedula', $cedula);
					$this->db->select('a.id');
			$this->db->from('personas a');
			$res = $this->db->get();

			if($res){
				return $res->result();
			}else{
				return false;
			}
		}
		
	}

?>
