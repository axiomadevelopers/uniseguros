<?php
	if (!defined('BASEPATH')) exit ('No direct script access allowed');

	class Portafolio_model extends CI_Model{

		public function iniciar_sesion($login,$clave){
			$this->db->where('login',$login);
			$this->db->where('clave',$clave);
			$this->db->where('estatus','1');
			$this->db->select('*');
			$this->db->from(' usuarios u');
			return $this->db->count_all_results();
		}
		/*
		*	Guardar Detalle de producto
		*/
		public function guardarPortafolio($datos,$id_imagen){

			if ($this->db->insert("portafolio", $datos)){
				$id_portafolio = $this->db->insert_id();
				$id_imagenes = $id_imagen;
				for ($i=0;$i<count($id_imagenes);$i++){
					$arreglo = ["id_portafolio"=>$id_portafolio, "id_imagen" => $id_imagenes[$i]];
					/* $arreglo1 = ["id_det_prod"=>$id_det_prod, "cantidad" => $cantidad]; */
					
					$this->db->insert("portafolio_imag", $arreglo);
					/* $this->db->insert("cantidad_producto", $arreglo1); */
				}
				return true;
			}else{
			 	return false;
			}
		}
		
		/*
		*
		*/
		public function consultarPortafolio($data){
			if($data["id_portafolio"]!=""){
				$this->db->where('a.id', $data["id_portafolio"]);
			}
			$this->db->order_by('a.id','DESC');
	        $this->db->where('a.estatus!=',2);
	        $this->db->select('a.id, a.titulo, a.descripcion,a.codigo,a.cliente,a.descripcion,a.url,a.fecha,a.estatus,
							   b.id as id_idioma, b.descripcion as descripcion_idioma,
							   c.id as id_servicios, c.titulo as titulo_servicio
							');

							  /*  co.id as color, co.descripcion as descripcion_color,
							   ta.id as talla, ta.descripcion as descripcion_talla, */


			/*$this->db->select('a.id, a.titulo, a.descripcion,
							   b.id as id_idioma, b.descripcion as descripcion_idioma,
							   c.id as categoria_prod, c.titulo as titulo_catprod,
							   t.id as tipo_prod, t.titulo as titulo_tipoprod,
							   m.id as marca, m.titulo as titulo_marca,
							   co.id as color, co.descripcion as descripcion_color,
							   ta.id as talla, ta.descripcion as descripcion_talla,
							   a.precio,a.estatus, ct.cantidad,a.clonado');*/
			$this->db->from('portafolio a');
			$this->db->join('idioma b', 'b.id = a.id_idioma');
			$this->db->join('servicios c', 'c.id = a.id_servicio');
			
			$res = $this->db->get();
			//print_r($this->db->last_query());die;
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}
		/*
		*	Metodo que busca las cantidades segun el producto este clonado o no
		*/
		public function consultarCantidad($id){
			$this->db->where('a.id_det_prod', $id);
	        $this->db->select('a.cantidad');
	        $this->db->from('cantidad_producto a');
	        $res = $this->db->get();
			$recordset = $res->result();
			return $recordset[0]->cantidad;
		}
		public function consultarImgPortafolio($id){
			if($id!=""){
				$this->db->where('g.id_portafolio', $id);
			}
			$this->db->order_by('a.id','ASC');
			$this->db->select('g.id_portafolio,a.id, a.ruta');
			$this->db->from('galeria a');
			$this->db->join('portafolio_imag g', 'g.id_imagen = a.id');
			$res = $this->db->get();
			//print_r($this->db->last_query());die;

			if($res){
				return $res->result();
			}else{
				return false;
			}
		}


		public function consultarExiste($id){
			if($id!=""){
				$this->db->where('a.id', $id);
			}
			$this->db->select('a.*');
			$this->db->from('portafolio a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}
		public function consultarExisteCodigo($id,$codigo,$idioma){
			$this->db->where('a.id!=', $id);
			$this->db->where('a.id_idioma', $idioma);
			$this->db->where('a.codigo', $codigo);
			$this->db->select('a.*');
			$this->db->from('portafolio a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function modificarPortafolio($data,$id_imagen){
			$this->db->where('id', $data["id"]);
			if($this->db->update("portafolio", $data)){
					$this->db->delete('portafolio_imag', array('id_portafolio' =>$data["id"]));
					$id_imagenes = $id_imagen;
					for ($i=0;$i<count($id_imagenes);$i++) {
					$arreglo = ["id_portafolio"=>$data["id"], "id_imagen" => $id_imagenes[$i]];
					/* $arreglo1 = ["id_det_prod"=>$data["id"], "cantidad" => $cantidad]; */
					$this->db->insert("portafolio_imag", $arreglo);
					/* $this->db->insert("cantidad_producto", $arreglo1); */
				}
				return true;
			}else{
			 	return false;
			}
		}

		public function consultarTallas($data){
			$this->db->where('a.estatus', '1');
			$this->db->select('a.id,a.descripcion');
			$this->db->from('tallas a');
			$res = $this->db->get();
			if($res){
				return $res->result();
			}else{
				return false;
			}
		}

		public function consultarcolor($data){
			if($data["id_idioma"]!=""){
				$this->db->where('a.id_idioma', $data["id_idioma"]);
			}
			$this->db->where('a.estatus', '1');
	        //$this->db->where('a.estatus!=',2);
			$this->db->select('a.id, a.descripcion');
			$this->db->from('colores a');
			$res = $this->db->get();

			if($res){
				return $res->result();
			}else{
				return false;
			}
		}
		//--
		public function modificarPotafolioEstatus($data){
			//----------------------------------------------------------
			$data_arr = array(
			  'estatus' => $data['estatus'],
			);
			///---------------------------------------------------------------
			$this->db->where('id', $data["id"]);
	        if($this->db->update("portafolio", $data_arr)){
	        	return true;
	        }else{
	        	return false;
	        }
		}
		//--Verifica si existe un registro con ese titulo
		public function consultarExisteTitulo($id,$titulo){
			if($id!=""){
				$this->db->where('d.id !=',$id);
			}
			$this->db->where('d.titulo',$titulo);
			$this->db->select('*');
			$this->db->from('portafolio d');
			//$res = $this->db->get();
			//print_r($this->db->last_query());die;
			return $this->db->count_all_results();
		
		}
		//--Verifica si existe un registro con ese codigo
		public function ExisteCodigo($id,$codigo){
			if($id!=""){
				$this->db->where('d.id !=',$id);
			}
			$this->db->where('d.codigo',$codigo);
			$this->db->select('*');
			$this->db->from('portafolio d');
			return $this->db->count_all_results();
		}
		
		

	}

?>
