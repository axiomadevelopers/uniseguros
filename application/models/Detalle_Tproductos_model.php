<?php 

if (!defined('BASEPATH')) exit ('No direct script access allowed');

Class Detalle_Tproductos_model extends CI_Model{

	public function iniciar_sesion($login,$clave){
		$this->db->where('login',$login);
		$this->db->where('clave',$clave);
		$this->db->where('estatus','1');
		$this->db->select('*');
		$this->db->from(' usuarios u');
		return $this->db->count_all_results();
	}
	public function consultarProductos($data){
		$this->db->order_by('a.id');
		if($data["id_productos"]!=""){
			$this->db->where('a.id_productos', $data["id_productos"]);
		}
		$this->db->where('a.estatus!=',2);
		$this->db->select('a.id,a.titulo');
		$this->db->from('tipo_producto a');
		$res = $this->db->get();
		if($res){
			return $res->result();
		}else{
			return false;
		}
		
	}
	public function guardarProductos($data){

		if($this->db->insert("detalle_productos", $data)){
			return true;
		}else{
			return false;
		}
	}

	public function consultarTproductos($data){
		$this->db->order_by('a.id_productos','asc');
		$this->db->order_by('a.id_tipo_producto','asc');
		$this->db->order_by('a.orden','asc');		
		$this->db->where('a.id_idioma', 1);
		
		if($data["id_productos"]!=""){
			$this->db->where('a.id', $data["id_productos"]);
		}
		
        $this->db->where('a.estatus!=',2);
		$this->db->select('a.*, b.id as id_idioma, b.descripcion as descripcion_idioma, d.titulo as descripcion_producto, e.titulo as descripcion_tipo_producto,e.id as id_tproducto, d.orden as numero_producto, e.orden as numero_tipo_producto');
		$this->db->from('detalle_productos a');
		$this->db->join('idioma b', 'b.id = a.id_idioma');
		$this->db->join('productos d', 'd.id = a.id_productos');
		$this->db->join('tipo_producto e', 'e.id = a.id_tipo_producto');
		$res = $this->db->get();
		//print_r($this->db->last_query());die;

		if($res){
			return $res->result();
		}else{
			return false;
		}
	}

	public function modificarProductos($data){
		$this->db->where('id', $data["id"]);
        if($this->db->update("detalle_productos", $data)){
        	//------------------------------------
        	if(isset($data["estatus"])){
        		if($data["estatus"]=="2"){
	        		$this->reiniciar_orden(1,$data["id_productos"],$data["id_tipo_producto"]);
	        	}
        	}
        	//------------------------------------
        	return true;
        }else{
        	return false;
        }
	}
	/*
	*	Reiniciar Orden
	*/
	public function reiniciar_orden($id_idioma,$id_productos,$id_tipo_producto){
		/*
		*	Consulto los slider que esten activos y reinicio el orden
		*/

		$this->db->order_by('a.orden','ASC');
        $this->db->where('a.estatus!=',2);
        $this->db->where('a.id_productos',$id_productos);
        $this->db->where('a.id_tipo_producto',$id_tipo_producto);
        $this->db->where('a.id_idioma',$id_idioma);
		$this->db->select('a.*');
		$this->db->from('detalle_productos a');
		$this->db->join('idioma b', 'b.id = a.id_idioma');
		$res = $this->db->get();
		$recordset = $res->result();
		if($recordset){
			$contador = 1;
			foreach ($recordset as $clave => $valor) {
				$data2 = array("orden"=>$contador);
				$this->db->where('id', $valor->id);
        		$a = $this->db->update("detalle_productos", $data2);
        		$contador++;	
			}
		}
		/***/
	}

	public function consultarExisteTitulo($id,$titulo,$id_productos,$id_tipo_producto){
		$this->db->where('n.id !=',$id);
		$this->db->where('n.id_productos',$id_productos);
		$this->db->where('n.titulo',$titulo);
		$this->db->where('n.id_tipo_producto',$id_tipo_producto);
		$this->db->where('estatus','1');
		$this->db->select('*');
		$this->db->from('detalle_productos n');
		return $this->db->count_all_results();
	}
	public function consultarOrden($data){
		if($data["id_idioma"]!=""){
			$this->db->where('a.id_idioma', $data["id_idioma"]);
		}
		if($data["id_productos"]!=""){
        	$this->db->where('a.id_productos',intVal($data["id_productos"]));
		}
		if($data["id_tipo_producto"]!=""){
        	$this->db->where('a.id_tipo_producto',intVal($data["id_tipo_producto"]));
		}
		$this->db->order_by('a.orden','ASC');
        $this->db->where('a.estatus!=',2);
		$this->db->select('a.orden');
		$this->db->from('detalle_productos	 a');
		$this->db->join('idioma b', 'b.id = a.id_idioma');
		$res = $this->db->get();
		//print_r($this->db->last_query());die;
		if($res->num_rows()>0){
			return $res->result();
		}else{
			return false;
		}
	}
	public function posicionar_modulos($posicionar){
		//var_dump($posicionar);die();
		//---
		if($posicionar['tipo'] == 'insert'){

			$this->db->where('orden >= ' . $posicionar['posicion']);

			$this->db->where('id_idioma = ' . $posicionar['id_idioma']);

			$this->db->where('id_productos = ' . $posicionar['id_productos']);

			$this->db->where('id_tipo_producto = ' . $posicionar['id_tipo_producto']);

			$resultados = $this->db->get("detalle_productos");

			   //print_r($this->db->last_query());die;

			if($resultados->num_rows() > 0){

				foreach ($resultados->result() as $row){

					$datos=array(
						'orden' => $row->orden + 1,
					);

					$this->db->where('id', $row->id);

					$this->db->update("detalle_productos",$datos);
				}

			}
		}else if($posicionar['tipo'] == 'update'){

			if($posicionar['final'] > $posicionar['inicial']){

				   $this->db->where('orden > ' . $posicionar['inicial'] . ' AND orden <= ' . $posicionar['final']);

				   $this->db->where('id_idioma = ' . $posicionar['id_idioma']);

				   $this->db->where('id_productos = ' . $posicionar['id_productos']);

				   $this->db->where('id_tipo_producto = ' . $posicionar['id_tipo_producto']);


				$resultados = $this->db->get("detalle_productos");
				if($resultados){
					if($resultados->num_rows() > 0){
						foreach ($resultados->result() as $row){
							$datos=array(
								'orden' => $row->orden - 1,
							);
							$this->db->where('id', $row->id);
							$this->db->update("detalle_productos", $datos);
						}
					}
				}

			}else if($posicionar['final'] < $posicionar['inicial']){

					$this->db->where('orden >= ' . $posicionar['final'] . ' AND orden < ' . $posicionar['inicial']);

					$this->db->where('id_idioma = ' . $posicionar['id_idioma']);

					$this->db->where('id_productos = ' . $posicionar['id_productos']);

					$this->db->where('id_tipo_producto = ' . $posicionar['id_tipo_producto']);

					$resultados = $this->db->get("detalle_productos");
					if($resultados){
						if($resultados->num_rows() > 0){
							foreach ($resultados->result() as $row){
								$datos=array(
									'orden' => $row->orden + 1,
								);
								$this->db->where('id', $row->id);
								$this->db->update("detalle_productos", $datos);
							}
						}
					}

			}
		//---
		}
//--
	}
}