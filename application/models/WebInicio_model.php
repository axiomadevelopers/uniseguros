<?php

if (!defined('BASEPATH')) exit ('No direct script access allowed');

Class WebInicio_model extends CI_Model{
	/*
	*	consultarSlider
	*/
	public function consultarSlider($data){
		//var_dump($data);echo "<br>";
		$this->db->where('a.id_idioma', $data["id_idioma"]);
		$this->db->order_by('a.orden');
        $this->db->where('a.estatus',1);
		$this->db->select('a.*, b.id as id_idioma, b.descripcion as descripcion_idioma, c.ruta as ruta, c.id as id_imagen');
		$this->db->from('slider a');
		$this->db->join('idioma b', 'b.id = a.id_idioma');
        $this->db->join('galeria c', 'c.id = a.id_imagen');
		$res = $this->db->get();
		/*var_dump($res);echo"<br>";
		var_dump($this->db->last_query());echo"<br>";*/
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	/***/
}	