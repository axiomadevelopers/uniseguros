<?php 

if (!defined('BASEPATH')) exit ('No direct script access allowed');

Class Servicios_model extends CI_Model{

	public function iniciar_sesion($login,$clave){
		$this->db->where('login',$login);
		$this->db->where('clave',$clave);
		$this->db->where('estatus','1');
		$this->db->select('*');
		$this->db->from(' usuarios u');
		return $this->db->count_all_results();
	}

	public function guardarProductos($data){
		if($this->db->insert("servicios", $data)){
			return true;
		}else{
			return false;
		}
	}

	public function consultarServicios($data){
   		$this->db->order_by('a.orden','asc');
		if($data["id_servicios"]!=""){
			$this->db->where('a.id', $data["id_servicios"]);
		}
        $this->db->where('a.estatus!=',2);
		$this->db->select('a.*, b.id as id_idioma, b.descripcion as descripcion_idioma, c.ruta as ruta, c.id as id_imagen');
		$this->db->from('servicios a');
		$this->db->join('idioma b', 'b.id = a.id_idioma');
        $this->db->join('galeria c', 'c.id = a.id_imagen');
		$res = $this->db->get();
        
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}

	public function modificarServicios($data){
		$this->db->where('id', $data["id"]);
        if($this->db->update("servicios", $data)){
        	//------------------------------------
        	if(isset($data["estatus"])){
        		if($data["estatus"]=="2"){
	        		$this->reiniciar_orden(1);
	        	}
        	}
        	//------------------------------------
        	return true;
        }else{
        	return false;
        }
	}
	/*
	*	Reiniciar Orden
	*/
	public function reiniciar_orden($id_idioma){
		/*
		*	Consulto los slider que esten activos y reinicio el orden
		*/

		$this->db->order_by('a.orden','ASC');
	    $this->db->where('a.estatus!=',2);
	    $this->db->where('a.id_idioma',$id_idioma);
		$this->db->select('a.*');
		$this->db->from('servicios a');
		$this->db->join('idioma b', 'b.id = a.id_idioma');
		$res = $this->db->get();
		$recordset = $res->result();
		if($recordset){
			$contador = 1;
			foreach ($recordset as $clave => $valor) {
				$data2 = array("orden"=>$contador);
				$this->db->where('id', $valor->id);
	    		$a = $this->db->update("servicios", $data2);
	    		$contador++;	
			}
		}
		/***/
	}

	public function consultarExisteTitulo($id,$titulo){
		$this->db->where('n.id !=',$id);
		$this->db->where('n.titulo',$titulo);
		$this->db->where('estatus','1');
		$this->db->select('*');
		$this->db->from('servicios n');
		return $this->db->count_all_results();
	}
	public function consultarOrden($data){
		
		if($data["id_idioma"]!=""){
			$this->db->where('a.id_idioma', $data["id_idioma"]);
		}
		
		$this->db->order_by('a.orden','ASC');
        $this->db->where('a.estatus!=',2);
		$this->db->select('a.orden');
		$this->db->from('servicios	 a');
		$this->db->join('idioma b', 'b.id = a.id_idioma');
		
		$res = $this->db->get();

		if($res->num_rows()>0){
			return $res->result();
		}else{
			return false;
		}
	}
	public function posicionar_modulos($posicionar){
		//var_dump($posicionar);die();
		//---
		if($posicionar['tipo'] == 'insert'){

			$this->db->where('orden >= ' . $posicionar['posicion']);

			$this->db->where('id_idioma = ' . $posicionar['id_idioma']);

			$resultados = $this->db->get("servicios");

			   //print_r($this->db->last_query());die;

			if($resultados->num_rows() > 0){

				foreach ($resultados->result() as $row){

					$datos=array(
						'orden' => $row->orden + 1,
					);

					$this->db->where('id', $row->id);

					$this->db->update("servicios",$datos);
				}

			}
		}else if($posicionar['tipo'] == 'update'){

			if($posicionar['final'] > $posicionar['inicial']){

				$this->db->where('orden > ' . $posicionar['inicial'] . ' AND orden <= ' . $posicionar['final']);

				   $this->db->where('id_idioma = ' . $posicionar['id_idioma']);

				$resultados = $this->db->get("servicios");
				if($resultados){
					if($resultados->num_rows() > 0){
						foreach ($resultados->result() as $row){
							$datos=array(
								'orden' => $row->orden - 1,
							);
							$this->db->where('id', $row->id);
							$this->db->update("servicios", $datos);
						}
					}
				}

			}else if($posicionar['final'] < $posicionar['inicial']){

					$this->db->where('orden >= ' . $posicionar['final'] . ' AND orden < ' . $posicionar['inicial']);

					$this->db->where('id_idioma = ' . $posicionar['id_idioma']);

					$resultados = $this->db->get("servicios");
					if($resultados){
						if($resultados->num_rows() > 0){
							foreach ($resultados->result() as $row){
								$datos=array(
									'orden' => $row->orden + 1,
								);
								$this->db->where('id', $row->id);
								$this->db->update("servicios", $datos);
							}
						}
					}

			}
		//---
		}
//--
	}

}