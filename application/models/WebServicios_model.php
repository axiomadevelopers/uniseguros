<?php

if(!defined('BASEPATH')) exit('No direct script access allowed');

Class WebServicios_model extends CI_Model {
	public function consultarServicios($data){
		$this->db->order_by('a.orden','ASC');
		$this->db->where('a.estatus!=',2);
		$this->db->where('a.estatus!=',0);
		$this->db->where('a.estatus=',1);
		$this->db->select('a.*, b.id as id_idioma, b.descripcion as descripcion_idioma, c.ruta as ruta, c.id as id_imagen');
		$this->db->from('servicios a');
		$this->db->join('idioma b', 'b.id = a.id_idioma');
		$this->db->join('galeria c', 'c.id = a.id_imagen');

		$res = $this->db->get();
		
		if($res){
			return $res->result();
		}else{
			return false;
		}
	   }
	/*
	 *	consultarProductos
	 */
	public
	function consultarProductos($data) {
		$this->db-> order_by('a.orden', 'ASC');
		$this->db-> where('a.estatus!=', 2);
		$this->db-> where('a.estatus!=', 0);
		$this->db-> where('a.estatus=', 1);
		$this->db-> select('a.*, b.id as id_idioma, b.descripcion as descripcion_idioma, c.ruta as ruta, c.id as id_imagen');
		$this->db-> from('productos a');
		$this->db-> join('idioma b', 'b.id = a.id_idioma');
		$this->db-> join('galeria c', 'c.id = a.id_imagen');
		$this->db-> limit(3);

		$res = $this->db-> get();

		if($res) {
			return $res-> result();
		} else {
			return false;
		}
	}

	/*
	 *	consultarTproducto
	 */
	public
	function consultarTservicios($id) {
		$this->db-> order_by('a.orden', 'ASC');
		$this->db-> where('a.estatus', 1);
		$this->db-> where('a.id_servicios', $id);
		$this->db-> where('a.estatus!=', 2);
		$this->db-> where('a.estatus!=', 0);
		$this->db-> where('a.estatus=', 1);

		$this->db-> select('a.*, c.ruta as ruta, c.id as id_imagen');
		$this->db-> from('tipo_servicios a');
		$this->db-> join('galeria c', 'c.id = a.id_imagen');
		$res = $this->db-> get();

		if($res) {
			return $res-> result();
		} else {
			return false;
		}
	}
	/*
	 *	Consultar Descripción
	 */
	public
	function consultarDescripcion($id, $id_servicio) {

		$this->db-> where('a.estatus=', 1);
		$this->db-> where('a.id_servicios', $id_servicio);
		$this->db-> where('a.id_tipo_servicio', $id);
		$this->db-> where('a.orden', 1);

		$this->db-> select('a.descripcion');
		$this->db-> from('detalle_servicios a');
		$res = $this->db-> get();

		if($res) {
			return $res-> row_array();
		} else {
			return false;
		}
	}
	/*
	 *	consultarOtrosProductos
	 */
	public
	function consultarOtrosTproductos($id) {

		$this->db-> order_by('a.orden', 'ASC');
		$this->db-> where('a.id!=', $id);
		$this->db-> where('a.estatus!=', 2);
		$this->db-> where('a.estatus!=', 0);
		$this->db-> where('a.estatus=', 1);
		$this->db-> select('a.*, c.ruta as ruta, c.id as id_imagen');
		$this->db-> from('productos a');
		$this->db-> join('galeria c', 'c.id = a.id_imagen');
		$this->db-> limit(3);
		$res = $this->db-> get();

		if($res) {
			return $res-> result();
		} else {
			return false;
		}
	}
	/*
	 *	consultarTproductos == Producto inicial
	 */
	public
	function consultarServicioSlug($data) {
		$this->db-> order_by('a.orden', 'ASC');

		$this->db-> where('a.slug', $data["slug_primario"]);

		$this->db-> where('a.estatus!=', 2);
		$this->db-> where('a.estatus!=', 0);
		$this->db-> where('a.estatus=', 1);
		$this->db-> select('a.*, c.ruta as ruta, c.id as id_imagen');
		$this->db-> from('servicios a');
		$this->db-> join('galeria c', 'c.id = a.id_imagen');
		$res = $this->db-> get();
		if($res) {
			return $res-> row_array();
		} else {
			return false;
		}
	}
	/*
	 *	consultarTproductos == Producto segundario
	 */
	public
	function consultarServicioSlugSegundario($data) {
		$this->db-> order_by('a.orden', 'ASC');

		$this->db-> where('a.slug', $data["slug_segundario"]);
		$this->db-> where('a.estatus!=', 2);
		$this->db-> where('a.estatus!=', 0);
		$this->db-> where('a.estatus=', 1);
		$this->db-> select('a.*, c.ruta as ruta, c.id as id_imagen');
		$this->db-> from('tipo_servicios a');
		$this->db-> join('galeria c', 'c.id = a.id_imagen');
		$res = $this->db-> get();

		if($res) {
			return $res-> row_array();
		} else {
			return false;
		}
	}
	/*
	 *	consultarDetallesTproducto
	 */
	public
	function consultarDetalleTservicios($servicio_base, $tipo_servicio) {
		$this->db-> order_by('a.orden', 'ASC');
		$this->db-> where('a.estatus!=', 2);
		$this->db-> where('a.estatus!=', 0);
		$this->db-> where('a.estatus=', 1);
		$this->db-> where('a.id_servicios', $servicio_base);
		$this->db-> where('a.id_tipo_servicio', $tipo_servicio);
		$this->db-> select('a.*');
		$this->db-> from('detalle_servicios a');
		$res = $this->db-> get();

		if($res) {
			return $res-> result();
		} else {
			return false;
		}
	}
	/*
	 *	consultarOtrosDetallesTproductos
	 */
	public
	function consultarOtrosDetallesTservicios($servicio_base, $tipo_servicio) {

		$this->db-> order_by('a.orden', 'ASC');
		$this->db-> where('a.id_servicios=', $servicio_base);
		$this->db-> where('a.id!=', $tipo_servicio);
		$this->db-> where('a.estatus!=', 2);
		$this->db-> where('a.estatus!=', 0);
		$this->db-> where('a.estatus=', 1);
		$this->db-> select('a.*, c.ruta as ruta, c.id as id_imagen');
		$this->db-> from('tipo_servicios a');
		$this->db-> join('galeria c', 'c.id = a.id_imagen');
		//$this->db->limit(3);
		$res = $this->db-> get();

		if($res) {
			return $res-> result();
		} else {
			return false;
		}
	}
	/*
	*
	*/
	public function consultarServicioSlugExiste($slug,$slug_base){
		
		if($slug!=""){
			$this->db->where('a.slug',$slug);
		}
		
		if($slug_base!=""){
			$this->db->where('b.slug', $slug_base);
	 	}

	 	$this->db->where('a.estatus=',1);
		$this->db->select('a.*');
		$this->db->from('tipo_servicios a');
		$this->db->join('servicios b', 'b.id = a.id_servicios');
		return $this->db->count_all_results();
		//$res = $this->db->get();
		//print_r($this->db->last_query());die;

	}

}