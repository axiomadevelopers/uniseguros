<?php

if (!defined('BASEPATH')) exit ('No direct script access allowed');

Class WebContactos_model extends CI_Model{

	/*
	*	consultarFooter
	*/
	
	public function consultarFooter($data){
		$this->db->order_by('id','asc');
		if($data["id_footer"]!=""){
			$this->db->where('a.id', $data["id_footer"]);
		}
        $this->db->where('a.estatus!=',2);
		$this->db->select('a.*, b.id as id_idioma, b.descripcion as descripcion_idioma');
		$this->db->from('footer a');
		$this->db->join('idioma b', 'b.id = a.id_idioma');
		$res = $this->db->get();
        
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	/*
	*	consultarRedes
	*/
	public function consultarRedes($data){
		$this->db->order_by('a.id');
		$this->db->select('a.id, a.url_red, c.descripcion');
		$this->db->from('red_social a');
		$this->db->join('tipo_red c', 'c.id = a.id_tipo_red');

		$res = $this->db->get();
        //print_r($this->db->last_query());die;

		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	/*
	*	consultarPalabrasClaves
	*/
	public function consultarPalabrasClaves(){
        $this->db->where('a.estatus!=',2);
		$this->db->select('a.descripcion as palabras_claves');
		$this->db->from('palabras_claves a');

		$res = $this->db->get();
        //print_r($this->db->last_query());die;

		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	/*
	*	consultarDescripcion
	*/
	public function consultarDescripcion(){
        $this->db->where('a.estatus!=',2);
		$this->db->select('a.descripcion as descripcion');
		$this->db->from('descripcion a');

		$res = $this->db->get();
        //print_r($this->db->last_query());die;

		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	/*
	*	guardarContactos
	*/
	public function guardarContactos($data){
		if($this->db->insert("contactos", $data)){
			return true;
		}else{
			return false;
		}
	}
	/*
	*	consultarDireccion
	*/
	public function consultarDireccion($data){
		if($data["id_direccion"]!=""){
			$this->db->where('a.id', $data["id_direccion"]);
		}
		$this->db->order_by('a.orden','ASC');
        $this->db->where('a.estatus!=',2);
		//$this->db->select('a.*, b.id as id_idioma, b.descripcion as descripcion_idioma, c.ruta as ruta, c.id as id_imagen');
		$this->db->select('a.*, b.id as id_idioma, b.descripcion as descripcion_idioma');
		$this->db->from('direcciones a');
		$this->db->join('idioma b', 'b.id = a.id_idioma');
        //$this->db->join('galeria c', 'c.id = a.id_imagen');
		$res = $this->db->get();

		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	/*
	*	consultarTelefonos
	*/
	public function consultarTelefonos($id_direccion){
		$this->db->order_by('a.id','ASC');
        $this->db->where('a.id_direccion',$id_direccion);
		$this->db->select('a.*');
		$this->db->from('telefonos a');
		
		$res = $this->db->get();
		
		//print_r($this->db->last_query());die;
		
		if($res->num_rows()>0){
			return $res->result();
		}else{
			return false;
		}
	}
	/*
	*	consultarCuentasBancarias
	*/
	public function consultarCuentas($data){
	
	 $this->db->where('a.estatus',1);
	 $this->db->select('a.*,b.id as id_banco, b.descripcion as descripcion_banco');
	 $this->db->from('cuentas_bancarias a');
	 $this->db->join('bancos b', 'b.id = a.id_banco'); 
	 $res = $this->db->get();
	 
	 if($res){
		 return $res->result();
	 }else{
		 return false;
	 }
 }
	 /*
	*	consultarPdf
	*/
	public function consultarPdf($data){
		$this->db->order_by('a.id','DESC');
		$this->db->where('a.estatus',1);
		$this->db->where('a.id_categoria',$data['id_categoria']);
		$this->db->select('a.*');
		$this->db->from('carga_pdfs a');
		$res = $this->db->get();
		
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	/*
	*	COnsultar Pdf Limit
	*/
	public function consultarPdfLimit($data){
		$this->db->order_by('a.id','DESC');
		$this->db->where('a.estatus',1);
		$this->db->where('a.id_categoria',$data['id_categoria']);
		$this->db->select('a.*');
		$this->db->from('carga_pdfs a');
		$this->db->limit($data["limit"],$data["offset"]);
		$res = $this->db->get();
		//print_r($this->db->last_query());die;
		if($res){
			return $res->result();
		}else{
			return false;
		}
	}
	//------------------------------------------------------------

}