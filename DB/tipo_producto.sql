-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 26-04-2020 a las 00:12:20
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.2.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bd_cms_uniseguros`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_producto`
--

CREATE TABLE `tipo_producto` (
  `id` int(11) NOT NULL,
  `titulo` varchar(500) NOT NULL,
  `descripcion` varchar(500) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `id_productos` int(11) NOT NULL,
  `estatus` int(11) NOT NULL,
  `id_imagen` int(11) NOT NULL,
  `slug` varchar(200) NOT NULL,
  `orden` int(14) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_producto`
--

INSERT INTO `tipo_producto` (`id`, `titulo`, `descripcion`, `id_idioma`, `id_productos`, `estatus`, `id_imagen`, `slug`, `orden`) VALUES
(1, 'Fianza de anticipo', '', 1, 2, 1, 219, 'fianza-de-anticipo', 1),
(2, 'Fianza de Fiel Cumplimiento', '', 1, 2, 1, 219, 'fianza-de-fiel-cumplimiento', 2),
(3, 'Fianza de Buena Calidad', '', 1, 2, 1, 219, 'fianza-de-buena-calidad', 3),
(4, 'Fianza de Anticipo', '', 1, 2, 3, 219, 'fianza-de-anticipo', 4),
(5, 'Fianza Laboral', '', 1, 2, 1, 219, 'fianza-laboral', 5),
(6, 'Fianza Aduanal', '', 1, 2, 1, 219, 'fianza-aduanal', 6),
(7, 'Uniauto Inteligente', '', 1, 3, 1, 220, 'Uniauto-inteligente', 7),
(8, 'Accidentes personales', '', 1, 4, 1, 219, 'accidentes-personales', 8),
(9, 'Póliza de vida', '', 1, 4, 1, 219, 'poliza-de-vida', 9),
(10, 'Póliza funeraria', '', 1, 4, 1, 219, 'poliza-funeraria', 10),
(11, 'Póliza funeraria', '', 1, 4, 1, 219, 'poliza-funeraria', 11),
(12, 'Incendio', '', 1, 5, 1, 219, 'incendio', 12),
(13, 'Lucro cesante', '', 1, 5, 1, 219, 'lucro-cesante', 13),
(14, 'Robo', '', 1, 5, 1, 219, 'robo', 14),
(15, 'Dinero y/o valores', '', 1, 5, 1, 219, 'dinero-y-o-valores', 15),
(16, 'Riesgos Especiales', '', 1, 5, 1, 219, 'riesgo_especiales', 16),
(17, 'Fildeilad 3d', '', 1, 5, 1, 219, 'fildelidad-3d', 17),
(18, 'Equipo de contratistas', '', 1, 5, 1, 219, 'equipo-de-contratistas', 18),
(19, 'Rotura de maquinarias', '', 1, 5, 1, 219, 'rotura-de-maquinarias', 19),
(20, 'Equipo electrónico', '', 1, 5, 1, 219, 'equipo-electronico', 20),
(21, 'Transporte terrestre', '', 1, 5, 1, 219, 'transporte-terrestre', 21),
(22, 'Transporte Marítimo y Aéreo', '', 1, 5, 1, 219, 'transporte-maritimo-y-aereo', 22),
(23, 'Embarcaciones', '', 1, 5, 1, 219, 'embarcaciones', 23),
(24, 'Incendio', '', 1, 5, 1, 219, 'incendio', 24),
(25, 'Lucro cesante', '', 1, 5, 1, 219, 'lucro-cesante', 25),
(26, 'Robo', '', 1, 5, 1, 219, 'robo', 26),
(27, 'Dinero y/o valores', '', 1, 5, 1, 219, 'dinero-y-o-valores', 27),
(28, 'Riesgos Especiales', '', 1, 5, 1, 219, 'riesgo_especiales', 28),
(29, 'Fildeilad 3d', '', 1, 5, 1, 219, 'fildelidad-3d', 29),
(30, 'Equipo de contratistas', '', 1, 5, 1, 219, 'equipo-de-contratistas', 30),
(31, 'Rotura de maquinarias', '', 1, 5, 1, 219, 'rotura-de-maquinarias', 31),
(32, 'Equipo electrónico', '', 1, 5, 1, 219, 'equipo-electronico', 32),
(33, 'Transporte terrestre', '', 1, 5, 1, 219, 'transporte-terrestre', 33),
(34, 'Transporte Marítimo y Aéreo', '', 1, 5, 1, 219, 'transporte-maritimo-y-aereo', 34),
(35, 'Embarcaciones', '', 1, 5, 1, 219, 'embarcaciones', 35),
(36, 'Aviación', '', 1, 5, 1, 219, 'aviacion', 36),
(37, 'Responsabilidad civil generales', '', 1, 5, 1, 219, 'responsabilidad-civil-generales', 37),
(38, 'Fianza de licitación', '.', 1, 2, 1, 219, 'fianza-de-licitacirn', 38);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tipo_producto`
--
ALTER TABLE `tipo_producto`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tipo_producto`
--
ALTER TABLE `tipo_producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
