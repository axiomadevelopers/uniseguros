-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 28-07-2020 a las 18:06:57
-- Versión del servidor: 10.1.38-MariaDB-0+deb9u1
-- Versión de PHP: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bd_cms_uniseguros`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auditoria`
--

CREATE TABLE `auditoria` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `modulo` int(11) NOT NULL,
  `accion` text NOT NULL,
  `ip` varchar(200) NOT NULL,
  `fecha_hora` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `auditoria`
--

INSERT INTO `auditoria` (`id`, `id_usuario`, `modulo`, `accion`, `ip`, `fecha_hora`) VALUES
(1, 8, 2, 'Agrego al carrito producto: Zoug Zoug Masculino Talla:S Color:BICOLOR', '190.103.28.234', '2019-10-24 09:53:00'),
(2, 8, 2, 'Agrego al carrito producto: Zoug Zoug Masculino Talla:L Color:BICOLOR', '190.103.28.234', '2019-10-24 09:59:00'),
(3, 8, 2, 'Se modifico la cantidad del producto: Zoug Zoug Masculino Talla:S Color:BICOLOR cantidad ingresada:1 cantidad anterior:2 cantidad en tabla: 98 cantidad nueva calculo: 1 cantidad total: 99', '190.103.28.234', '2019-10-24 10:00:00'),
(4, 8, 2, 'Se modifico la cantidad del producto: Zoug Zoug Masculino Talla:L Color:BICOLOR cantidad ingresada:2 cantidad anterior:3 cantidad en tabla: 197 cantidad nueva calculo: 1 cantidad total: 198', '190.103.28.234', '2019-10-24 10:00:00'),
(5, 8, 2, 'Creación de orden #id:86 con los siguientes items:  producto: Zoug Zoug Masculino Talla:L Color:BICOLOR, producto: Zoug Zoug Masculino Talla:S Color:BICOLOR,', '190.103.28.234', '2019-10-24 10:02:00'),
(6, 8, 2, 'Agrego al carrito producto: Zoug Zoug Masculino Talla:S Color:BICOLOR', '190.103.28.234', '2019-10-24 10:15:00'),
(7, 8, 2, 'Agrego al carrito producto: Zoug Zoug Masculino Talla:L Color:BICOLOR', '190.103.28.234', '2019-10-24 10:16:00'),
(8, 8, 2, 'Agrego al carrito producto: Zoug Zoug Femenino Talla:M Color:BICOLOR', '190.103.28.234', '2019-10-24 10:19:00'),
(9, 8, 2, 'Creación de orden #id:87 con los siguientes items:  producto: Zoug Zoug Femenino Talla:M Color:BICOLOR, producto: Zoug Zoug Masculino Talla:L Color:BICOLOR, producto: Zoug Zoug Masculino Talla:S Color:BICOLOR,', '190.103.28.234', '2019-10-24 10:20:00'),
(10, 4, 2, 'Agrego al carrito producto: Premium Masculino Talla:S Color:BICOLOR', '190.103.28.234', '2019-10-24 10:23:00'),
(11, 4, 2, 'Agrego al carrito producto: Premium Femenino Talla:S Color:BICOLOR', '190.103.28.234', '2019-10-24 10:24:00'),
(12, 4, 2, 'Creación de orden #id:88 con los siguientes items:  producto: Premium Femenino Talla:S Color:BICOLOR, producto: Premium Masculino Talla:S Color:BICOLOR,', '190.103.28.234', '2019-10-24 10:25:00'),
(13, 1, 1, 'Inicio de sesion', '190.103.28.234', '2019-10-24 10:28:00'),
(14, 1, 1, 'Inactivar cantidad producto: 199', '190.103.28.234', '2019-10-24 10:28:00'),
(15, 1, 1, 'Activar cantidad producto: 199', '190.103.28.234', '2019-10-24 10:28:00'),
(16, 4, 2, 'Agrego al carrito producto: Zoug Zoug Femenino II Talla:S Color:BICOLOR', '190.103.28.234', '2019-10-24 10:55:00'),
(17, 1, 1, 'Inicio de sesion', '190.103.28.234', '2019-10-24 11:10:00'),
(18, 4, 2, 'Agrego al carrito producto: Zoug Zoug Femenino II Talla:S Color:BICOLOR', '190.79.77.173', '2019-11-01 16:11:00'),
(19, 4, 2, 'Elimino producto Zoug Zoug Femenino II de talla:S , color: BICOLOR del carrito con id:65', '190.79.77.173', '2019-11-01 16:11:00'),
(20, 4, 2, 'Agrego al carrito producto: Zoug Zoug Femenino II Talla:S Color:BICOLOR', '190.79.77.173', '2019-11-01 16:12:00'),
(21, 4, 2, 'Creación de orden #id:89 con los siguientes items:  producto: Zoug Zoug Femenino II Talla:S Color:BICOLOR,', '190.79.77.173', '2019-11-01 16:13:00'),
(22, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-11-01 16:22:00'),
(23, 1, 1, 'Registro cantidad producto id: 200', '190.79.77.173', '2019-11-01 16:25:00'),
(24, 1, 1, 'Inactivar cantidad producto: 200', '190.79.77.173', '2019-11-01 16:25:00'),
(25, 1, 1, 'Eliminar cantidad producto: 200', '190.79.77.173', '2019-11-01 16:25:00'),
(26, 1, 1, 'Cerrar de sesion', '190.79.77.173', '2019-11-01 16:26:00'),
(27, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-11-11 17:19:00'),
(28, 1, 1, 'Actualizacion de empresa nosotros id: 20', '190.79.77.173', '2019-11-11 17:22:00'),
(29, 1, 1, 'Inactivar empresa nosotros id: 20', '190.79.77.173', '2019-11-11 17:23:00'),
(30, 1, 1, 'Activar empresa nosotros id: 20', '190.79.77.173', '2019-11-11 17:23:00'),
(31, 1, 1, 'Actualizacion de slider id: 264', '190.79.77.173', '2019-11-11 17:23:00'),
(32, 1, 1, 'Actualizacion de slider id: 264', '190.79.77.173', '2019-11-11 17:23:00'),
(33, 1, 1, 'Inactivar slider id: 264', '190.79.77.173', '2019-11-11 17:24:00'),
(34, 1, 1, 'Activar slider id: 264', '190.79.77.173', '2019-11-11 17:24:00'),
(35, 1, 1, 'Actualizacion de marca id: 14', '190.79.77.173', '2019-11-11 17:24:00'),
(36, 1, 1, 'Inactivar marca id: 14', '190.79.77.173', '2019-11-11 17:25:00'),
(37, 1, 1, 'Activar marca id: 14', '190.79.77.173', '2019-11-11 17:25:00'),
(38, 1, 1, 'Actualizacion de categoría producto id: 11', '190.79.77.173', '2019-11-11 17:25:00'),
(39, 1, 1, 'Inactivar categoría producto id: 11', '190.79.77.173', '2019-11-11 17:25:00'),
(40, 1, 1, 'Activar categoría producto id: 11', '190.79.77.173', '2019-11-11 17:25:00'),
(41, 1, 1, 'Activar tipo de producto id: 45', '190.79.77.173', '2019-11-11 17:26:00'),
(42, 1, 1, 'Inactivar tipo de producto id: 45', '190.79.77.173', '2019-11-11 17:26:00'),
(43, 1, 1, 'Actualizacion tipo de producto id: 45', '190.79.77.173', '2019-11-11 17:27:00'),
(44, 1, 1, 'Inactivar color id: 11', '190.79.77.173', '2019-11-11 17:27:00'),
(45, 1, 1, 'Activar color id: 11', '190.79.77.173', '2019-11-11 17:27:00'),
(46, 1, 1, 'Actualizacion de color id: 11', '190.79.77.173', '2019-11-11 17:27:00'),
(47, 1, 1, 'Actualizacion de talla id: 8', '190.79.77.173', '2019-11-11 17:47:00'),
(48, 1, 1, 'Inactivar talla id: 8', '190.79.77.173', '2019-11-11 17:47:00'),
(49, 1, 1, 'Activar talla id: 8', '190.79.77.173', '2019-11-11 17:47:00'),
(50, 1, 1, 'Inactivar categorías idiomas id: 28', '190.79.77.173', '2019-11-11 17:47:00'),
(51, 1, 1, 'Inactivar categorías idiomas id: 39', '190.79.77.173', '2019-11-11 17:47:00'),
(52, 1, 1, 'Activar categorías idiomas id: 39', '190.79.77.173', '2019-11-11 17:47:00'),
(53, 1, 1, 'Activar categorías idiomas id: 28', '190.79.77.173', '2019-11-11 17:48:00'),
(54, 1, 1, 'Actualizacion de producto id: 122', '190.79.77.173', '2019-11-11 17:51:00'),
(55, 1, 1, 'Actualizacion de producto id: 122', '190.79.77.173', '2019-11-11 17:59:00'),
(56, 1, 1, 'Inactivar producto: 122', '190.79.77.173', '2019-11-11 17:59:00'),
(57, 1, 1, 'Activar producto: 122', '190.79.77.173', '2019-11-11 17:59:00'),
(58, 1, 1, 'Inactivar cantidad producto: 199', '190.79.77.173', '2019-11-11 18:00:00'),
(59, 1, 1, 'Activar cantidad producto: 199', '190.79.77.173', '2019-11-11 18:00:00'),
(60, 1, 1, 'Actualizar noticia id: 15 titulo: PRUEBA 4', '190.79.77.173', '2019-11-11 18:00:00'),
(61, 1, 1, 'Inactivar noticia: 15', '190.79.77.173', '2019-11-11 18:01:00'),
(62, 1, 1, 'Activar noticia: 15', '190.79.77.173', '2019-11-11 18:01:00'),
(63, 1, 1, 'Inactivar noticia: 15', '190.79.77.173', '2019-11-11 18:01:00'),
(64, 1, 1, 'Activar noticia: 15', '190.79.77.173', '2019-11-11 18:01:00'),
(65, 1, 1, 'Actualizacion redes sociales id: 11', '190.79.77.173', '2019-11-11 18:02:00'),
(66, 1, 1, 'Actualizar footer id: 4', '190.79.77.173', '2019-11-11 18:03:00'),
(67, 1, 1, 'Inactivar footer: 3', '190.79.77.173', '2019-11-11 18:03:00'),
(68, 1, 1, 'Activar footer: 3', '190.79.77.173', '2019-11-11 18:03:00'),
(69, 1, 1, 'Modificar categtorias cms id: 22', '190.79.77.173', '2019-11-11 18:03:00'),
(70, 1, 1, 'Inactivar categorias: 22', '190.79.77.173', '2019-11-11 18:03:00'),
(71, 1, 1, 'Activar categorias: 22', '190.79.77.173', '2019-11-11 18:03:00'),
(72, 1, 1, 'Modificar categtorias cms id: 22', '190.79.77.173', '2019-11-11 18:06:00'),
(73, 1, 1, 'Inactivar categorias: 22', '190.79.77.173', '2019-11-11 18:06:00'),
(74, 1, 1, 'Activar categorias: 22', '190.79.77.173', '2019-11-11 18:06:00'),
(75, 1, 1, 'Eliminar galeria  id: 113', '190.79.77.173', '2019-11-11 18:08:00'),
(76, 1, 1, 'Modificar usuarios id: 1', '190.79.77.173', '2019-11-11 18:08:00'),
(77, 1, 1, 'Inactivar usuarios: 1', '190.79.77.173', '2019-11-11 18:08:00'),
(78, 1, 1, 'Activar usuarios: 1', '190.79.77.173', '2019-11-11 18:08:00'),
(79, 1, 1, 'Modificar descripcion id: 1', '190.79.77.173', '2019-11-11 18:12:00'),
(80, 1, 1, 'Registro palabras claves id: 103', '190.79.77.173', '2019-11-11 18:14:00'),
(81, 1, 1, 'Modificar palabras claves id: 66', '190.79.77.173', '2019-11-11 18:28:00'),
(82, 1, 1, 'Cerrar de sesion', '190.79.77.173', '2019-11-11 18:28:00'),
(83, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-11-12 09:11:00'),
(84, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-11-20 11:13:00'),
(85, 1, 1, 'Cerrar de sesion', '190.79.77.173', '2019-11-20 12:38:00'),
(86, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-11-20 12:38:00'),
(87, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-11-20 12:46:00'),
(88, 1, 1, 'Inicio de sesion', '75.51.30.41', '2019-11-20 12:53:00'),
(89, 1, 1, 'Cerrar de sesion', '75.51.30.41', '2019-11-20 13:32:00'),
(90, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-11-21 09:03:00'),
(91, 1, 1, 'Cerrar de sesion', '190.79.77.173', '2019-11-21 09:14:00'),
(92, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-11-21 09:17:00'),
(93, 1, 1, 'Cerrar de sesion', '190.79.77.173', '2019-11-21 09:24:00'),
(94, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-11-21 09:24:00'),
(95, 1, 1, 'Cerrar de sesion', '190.79.77.173', '2019-11-21 09:40:00'),
(96, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-11-21 09:41:00'),
(97, 1, 1, 'Registro usuarios id: 10', '190.79.77.173', '2019-11-21 09:46:00'),
(98, 1, 1, 'Registro usuarios id: 11', '190.79.77.173', '2019-11-21 09:47:00'),
(99, 1, 1, 'Modificar usuarios id: 11', '190.79.77.173', '2019-11-21 09:47:00'),
(100, 1, 1, 'Cerrar de sesion', '190.79.77.173', '2019-11-21 09:47:00'),
(101, 11, 1, 'Inicio de sesion', '190.79.77.173', '2019-11-21 09:48:00'),
(102, 11, 1, 'Activar tipo de producto id: 45', '190.79.77.173', '2019-11-21 09:49:00'),
(103, 11, 1, 'Activar tipo de producto id: 42', '190.79.77.173', '2019-11-21 09:49:00'),
(104, 11, 1, 'Cerrar de sesion', '190.79.77.173', '2019-11-21 09:54:00'),
(105, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-11-21 09:54:00'),
(106, 1, 1, 'Cerrar de sesion', '190.79.77.173', '2019-11-21 10:12:00'),
(107, 11, 1, 'Inicio de sesion', '190.79.77.173', '2019-11-21 10:13:00'),
(108, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-11-21 10:13:00'),
(109, 1, 1, 'Cerrar de sesion', '190.79.77.173', '2019-11-21 10:15:00'),
(110, 11, 1, 'Inicio de sesion', '190.79.77.173', '2019-11-21 10:15:00'),
(111, 8, 2, 'Agrego al carrito producto: Zoug Zoug Masculino Talla:S Color:BICOLOR', '190.79.77.173', '2019-11-21 10:23:00'),
(112, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-11-21 10:24:00'),
(113, 1, 2, 'Agrego al carrito producto: Zoug Zoug Masculino Talla:S Color:BICOLOR', '190.79.77.173', '2019-11-21 10:28:00'),
(114, 1, 2, 'Creación de orden #id:90 con los siguientes items:  producto: Zoug Zoug Masculino Talla:S Color:BICOLOR,', '190.79.77.173', '2019-11-21 10:30:00'),
(115, 1, 2, 'Agrego al carrito producto: Zoug Zoug Femenino II Talla:S Color:BICOLOR', '190.79.77.173', '2019-11-21 10:35:00'),
(116, 1, 2, 'Se modifico la cantidad del producto: Zoug Zoug Femenino II Talla:S Color:BICOLOR cantidad ingresada:1 cantidad anterior:2 cantidad en tabla: 93 cantidad nueva calculo: 1 cantidad total: 94', '190.79.77.173', '2019-11-21 10:35:00'),
(117, 1, 2, 'Agrego al carrito producto: Zoug Zoug Femenino Talla:S Color:BICOLOR', '190.79.77.173', '2019-11-21 10:36:00'),
(118, 1, 2, 'Se modifico la cantidad del producto: Zoug Zoug Femenino II Talla:S Color:BICOLOR cantidad ingresada:2 cantidad anterior:1 cantidad en tabla: 94 cantidad nueva calculo: 1 cantidad total: 93', '190.79.77.173', '2019-11-21 10:36:00'),
(119, 1, 2, 'Se modifico la cantidad del producto: Zoug Zoug Femenino Talla:S Color:BICOLOR cantidad ingresada:3 cantidad anterior:2 cantidad en tabla: 96 cantidad nueva calculo: 1 cantidad total: 95', '190.79.77.173', '2019-11-21 10:36:00'),
(120, 1, 2, 'Elimino producto Zoug Zoug Femenino II de talla:S , color: BICOLOR del carrito con id:69', '190.79.77.173', '2019-11-21 10:37:00'),
(121, 1, 2, 'Creación de orden #id:91 con los siguientes items:  producto: Zoug Zoug Femenino Talla:S Color:BICOLOR,', '190.79.77.173', '2019-11-21 10:44:00'),
(122, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-11-21 10:45:00'),
(123, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-11-27 09:43:00'),
(124, 4, 2, 'Agrego al carrito producto: Zoug Zoug Femenino II Talla:S Color:BICOLOR', '190.79.77.173', '2019-11-28 07:02:00'),
(125, 4, 2, 'Creación de orden #id:92 con los siguientes items:  producto: Zoug Zoug Femenino II Talla:S Color:BICOLOR,', '190.79.77.173', '2019-11-28 07:03:00'),
(126, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-12-03 16:20:00'),
(127, 1, 1, 'Actualizacion de slider id: 264', '190.79.77.173', '2019-12-03 16:21:00'),
(128, 1, 1, 'Actualizacion de slider id: 264', '190.79.77.173', '2019-12-03 16:25:00'),
(129, 1, 1, 'Actualizacion de slider id: 268', '190.79.77.173', '2019-12-03 16:25:00'),
(130, 1, 1, 'Actualizacion de slider id: 267', '190.79.77.173', '2019-12-03 16:25:00'),
(131, 1, 1, 'Actualizacion de slider id: 269', '190.79.77.173', '2019-12-03 16:26:00'),
(132, 1, 1, 'Actualizacion de slider id: 265', '190.79.77.173', '2019-12-03 16:26:00'),
(133, 1, 1, 'Actualizacion de slider id: 266', '190.79.77.173', '2019-12-03 16:26:00'),
(134, 0, 1, 'Actualizacion de slider id: 266', '190.79.77.173', '2019-12-03 16:35:00'),
(135, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-12-04 08:22:00'),
(136, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-12-04 08:28:00'),
(137, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-12-04 08:31:00'),
(138, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-12-04 08:37:00'),
(139, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-12-04 08:40:00'),
(140, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-12-04 08:56:00'),
(141, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-12-04 09:01:00'),
(142, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-12-04 09:01:00'),
(143, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-12-04 09:01:00'),
(144, 1, 1, 'Cerrar de sesion', '190.79.77.173', '2019-12-04 09:02:00'),
(145, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-12-04 09:03:00'),
(146, 1, 1, 'Cerrar de sesion', '190.79.77.173', '2019-12-04 09:04:00'),
(147, 11, 1, 'Inicio de sesion', '190.79.77.173', '2019-12-04 09:04:00'),
(148, 10, 2, 'Agrego al carrito producto: Liqui Liqui Masculino Talla:S Color:BICOLOR', '190.79.77.173', '2019-12-04 10:19:00'),
(149, 10, 2, 'Creación de orden #id:93 con los siguientes items:  producto: Liqui Liqui Masculino Talla:S Color:BICOLOR,', '190.79.77.173', '2019-12-04 10:22:00'),
(150, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-12-05 19:28:00'),
(151, 1, 1, 'Cerrar de sesion', '190.79.77.173', '2019-12-05 19:31:00'),
(152, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-12-05 19:31:00'),
(153, 1, 1, 'Cerrar de sesion', '190.79.77.173', '2019-12-05 19:32:00'),
(154, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-12-05 19:32:00'),
(155, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-12-09 12:15:00'),
(156, 1, 1, 'Actualizacion de empresa nosotros id: 20', '190.79.77.173', '2019-12-09 12:17:00'),
(157, 1, 1, 'Actualizacion de empresa nosotros id: 21', '190.79.77.173', '2019-12-09 12:18:00'),
(158, 0, 1, 'Cerrar de sesion', '190.79.77.173', '2019-12-09 13:31:00'),
(159, 8, 2, 'Agrego al carrito producto: Liqui Liqui Masculino Talla:S Color:BICOLOR', '190.79.77.173', '2019-12-11 15:43:00'),
(160, 8, 2, 'Se modifico la cantidad del producto: Liqui Liqui Masculino Talla:S Color:BICOLOR cantidad ingresada:1 cantidad anterior:2 cantidad en tabla: 96 cantidad nueva calculo: 1 cantidad total: 97', '190.79.77.173', '2019-12-11 15:44:00'),
(161, 8, 2, 'Se modifico la cantidad del producto: Zoug Zoug Masculino Talla:S Color:BICOLOR cantidad ingresada:0 cantidad anterior:1 cantidad en tabla: 94 cantidad nueva calculo: 1 cantidad total: 94', '190.79.77.173', '2019-12-11 15:44:00'),
(162, 8, 2, 'Creación de orden #id:94 con los siguientes items:  producto: Liqui Liqui Masculino Talla:S Color:BICOLOR, producto: Zoug Zoug Masculino Talla:S Color:BICOLOR,', '190.79.77.173', '2019-12-11 15:45:00'),
(163, 8, 2, 'Agrego al carrito producto: Zoug Zoug Femenino II Talla:S Color:BICOLOR', '190.79.77.173', '2019-12-11 16:38:00'),
(164, 8, 2, 'Creación de orden #id:95 con los siguientes items:  producto: Zoug Zoug Femenino II Talla:S Color:BICOLOR,', '190.79.77.173', '2019-12-11 16:40:00'),
(165, 4, 2, 'Agrego al carrito producto: Premium Masculino Talla:S Color:BICOLOR', '190.79.77.173', '2019-12-11 16:49:00'),
(166, 4, 2, 'Se modifico la cantidad del producto: Premium Masculino Talla:S Color:BICOLOR cantidad ingresada:0 cantidad anterior:1 cantidad en tabla: 98 cantidad nueva calculo:  cantidad total: 98', '190.79.77.173', '2019-12-11 16:49:00'),
(167, 4, 2, 'Creación de orden #id:96 con los siguientes items:  producto: Premium Masculino Talla:S Color:BICOLOR,', '190.79.77.173', '2019-12-11 16:51:00'),
(168, 8, 2, 'Agrego al carrito producto: Liqui Liqui Femenino Talla:M Color:BICOLOR', '190.79.77.173', '2019-12-11 16:59:00'),
(169, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-12-11 17:02:00'),
(170, 1, 2, 'Agrego al carrito producto: Liqui Liqui Femenino Talla:M Color:BICOLOR', '190.79.77.173', '2019-12-11 17:22:00'),
(171, 1, 2, 'Se modifico la cantidad del producto: Liqui Liqui Femenino Talla:M Color:BICOLOR cantidad ingresada:1 cantidad anterior:2 cantidad en tabla: 98 cantidad nueva calculo: 1 cantidad total: 99', '190.79.77.173', '2019-12-11 17:23:00'),
(172, 1, 2, 'Agrego al carrito producto: Liqui Liqui Femenino Talla:M Color:BICOLOR', '190.79.77.173', '2019-12-11 17:30:00'),
(173, 1, 2, 'Se modifico la cantidad del producto: Liqui Liqui Femenino Talla:M Color:BICOLOR cantidad ingresada:0 cantidad anterior:3 cantidad en tabla: 248 cantidad nueva calculo:  cantidad total: 248', '190.79.77.173', '2019-12-11 17:32:00'),
(174, 1, 2, 'Creación de orden #id:97 con los siguientes items:  producto: Liqui Liqui Femenino Talla:M Color:BICOLOR,', '190.79.77.173', '2019-12-11 17:52:00'),
(175, 1, 1, 'Cerrar de sesion', '190.79.77.173', '2019-12-11 18:00:00'),
(176, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-12-12 06:20:00'),
(177, 1, 1, 'Registro galeria  id: 143', '190.79.77.173', '2019-12-12 06:21:00'),
(178, 1, 1, 'Registro galeria  id: 144', '190.79.77.173', '2019-12-12 06:22:00'),
(179, 1, 1, 'Registro galeria  id: 145', '190.79.77.173', '2019-12-12 06:23:00'),
(180, 1, 1, 'Actualizacion de slider id: 264', '190.79.77.173', '2019-12-12 06:24:00'),
(181, 1, 1, 'Actualizacion de slider id: 267', '190.79.77.173', '2019-12-12 06:24:00'),
(182, 0, 1, 'Actualizacion de slider id: 265', '190.79.77.173', '2019-12-12 06:25:00'),
(183, 0, 1, 'Actualizacion de slider id: 265', '190.79.77.173', '2019-12-12 06:25:00'),
(184, 0, 1, 'Actualizacion de slider id: 264', '190.79.77.173', '2019-12-12 06:26:00'),
(185, 0, 1, 'Actualizacion de slider id: 267', '190.79.77.173', '2019-12-12 06:28:00'),
(186, 0, 1, 'Actualizacion de slider id: 265', '190.79.77.173', '2019-12-12 06:29:00'),
(187, 0, 1, 'Actualizacion de slider id: 265', '190.79.77.173', '2019-12-12 06:30:00'),
(188, 0, 1, 'Actualizacion de slider id: 265', '190.79.77.173', '2019-12-12 06:32:00'),
(189, 0, 1, 'Actualizacion de slider id: 264', '190.79.77.173', '2019-12-12 06:34:00'),
(190, 0, 1, 'Actualizacion de slider id: 267', '190.79.77.173', '2019-12-12 06:34:00'),
(191, 0, 1, 'Actualizacion de slider id: 267', '190.79.77.173', '2019-12-12 06:35:00'),
(192, 0, 1, 'Actualizacion de slider id: 267', '190.79.77.173', '2019-12-12 06:38:00'),
(193, 0, 1, 'Actualizacion de slider id: 267', '190.79.77.173', '2019-12-12 06:56:00'),
(194, 1, 1, 'Inicio de sesion', '190.79.77.173', '2019-12-12 08:41:00'),
(195, 1, 1, 'Actualizacion de slider id: 268', '190.79.77.173', '2019-12-12 08:42:00'),
(196, 1, 1, 'Inicio de sesion', '190.202.241.91', '2019-12-12 12:53:00'),
(197, 1, 1, 'Registro galeria  id: 146', '190.202.241.91', '2019-12-12 12:55:00'),
(198, 1, 1, 'Inicio de sesion', '190.202.241.91', '2019-12-12 13:06:00'),
(199, 1, 1, 'Actualizacion de slider id: 268', '190.202.241.91', '2019-12-12 13:07:00'),
(200, 1, 1, 'Actualizacion de slider id: 264', '190.202.241.91', '2019-12-12 13:08:00'),
(201, 1, 1, 'Actualizacion de slider id: 265', '190.202.241.91', '2019-12-12 13:08:00'),
(202, 1, 1, 'Actualizacion de slider id: 266', '190.202.241.91', '2019-12-12 13:08:00'),
(203, 1, 1, 'Actualizacion de slider id: 267', '190.202.241.91', '2019-12-12 13:09:00'),
(204, 1, 1, 'Actualizacion de slider id: 269', '190.202.241.91', '2019-12-12 13:09:00'),
(205, 1, 1, 'Eliminar galeria  id: 146', '190.202.241.91', '2019-12-12 13:10:00'),
(206, 1, 1, 'Eliminar galeria  id: 145', '190.202.241.91', '2019-12-12 13:11:00'),
(207, 1, 1, 'Eliminar galeria  id: 144', '190.202.241.91', '2019-12-12 13:11:00'),
(208, 1, 1, 'Eliminar galeria  id: 143', '190.202.241.91', '2019-12-12 13:11:00'),
(209, 1, 1, 'Eliminar galeria  id: 111', '190.202.241.91', '2019-12-12 13:11:00'),
(210, 1, 1, 'Eliminar galeria  id: 110', '190.202.241.91', '2019-12-12 13:11:00'),
(211, 1, 1, 'Eliminar galeria  id: 109', '190.202.241.91', '2019-12-12 13:11:00'),
(212, 1, 1, 'Registro galeria  id: 147', '190.202.241.91', '2019-12-12 13:12:00'),
(213, 1, 1, 'Registro galeria  id: 148', '190.202.241.91', '2019-12-12 13:12:00'),
(214, 1, 1, 'Registro galeria  id: 149', '190.202.241.91', '2019-12-12 13:13:00'),
(215, 1, 1, 'Actualizacion de slider id: 267', '190.202.241.91', '2019-12-12 13:14:00'),
(216, 1, 1, 'Actualizacion de slider id: 269', '190.202.241.91', '2019-12-12 13:14:00'),
(217, 1, 1, 'Actualizacion de slider id: 265', '190.202.241.91', '2019-12-12 13:14:00'),
(218, 1, 1, 'Actualizacion de slider id: 266', '190.202.241.91', '2019-12-12 13:14:00'),
(219, 1, 1, 'Actualizacion de slider id: 264', '190.202.241.91', '2019-12-12 13:14:00'),
(220, 1, 1, 'Actualizacion de slider id: 268', '190.202.241.91', '2019-12-12 13:15:00'),
(221, 0, 1, 'Actualizacion de slider id: 267', '190.202.241.91', '2019-12-12 13:15:00'),
(222, 0, 1, 'Actualizacion de slider id: 269', '190.202.241.91', '2019-12-12 13:15:00'),
(223, 0, 1, 'Actualizacion de slider id: 269', '190.202.241.91', '2019-12-12 13:16:00'),
(224, 0, 1, 'Actualizacion de slider id: 264', '190.202.241.91', '2019-12-12 13:16:00'),
(225, 0, 1, 'Actualizacion de slider id: 266', '190.202.241.91', '2019-12-12 13:16:00'),
(226, 0, 1, 'Actualizacion de slider id: 265', '190.202.241.91', '2019-12-12 13:17:00'),
(227, 0, 1, 'Actualizacion de slider id: 266', '190.202.241.91', '2019-12-12 13:18:00'),
(228, 0, 1, 'Actualizacion de slider id: 264', '190.202.241.91', '2019-12-12 13:18:00'),
(229, 0, 1, 'Cerrar de sesion', '190.103.28.234', '2019-12-12 15:24:00'),
(230, 1, 1, 'Inicio de sesion', '190.103.28.234', '2019-12-12 15:24:00'),
(231, 1, 1, 'Inicio de sesion', '190.103.28.234', '2019-12-13 08:08:00'),
(232, 1, 1, 'Inicio de sesion', '190.103.28.234', '2019-12-13 08:22:00'),
(233, 1, 1, 'Registro galeria  id: 150', '190.103.28.234', '2019-12-13 08:22:00'),
(234, 1, 1, 'Eliminar galeria  id: 149', '190.103.28.234', '2019-12-13 08:23:00'),
(235, 1, 1, 'Actualizacion de slider id: 265', '190.103.28.234', '2019-12-13 08:23:00'),
(236, 1, 1, 'Actualizacion de slider id: 266', '190.103.28.234', '2019-12-13 08:24:00'),
(237, 1, 1, 'Inicio de sesion', '190.103.28.234', '2019-12-13 16:31:00'),
(238, 1, 1, 'Actualizacion de slider id: 267', '190.103.28.234', '2019-12-13 16:32:00'),
(239, 1, 1, 'Actualizacion de slider id: 269', '190.103.28.234', '2019-12-13 16:32:00'),
(240, 1, 1, 'Actualizacion de slider id: 269', '190.103.28.234', '2019-12-13 16:32:00'),
(241, 1, 1, 'Registro de slider id:272,titulo:prueba', '190.103.28.234', '2019-12-13 16:33:00'),
(242, 1, 1, 'Inactivar slider id: 0', '190.103.28.234', '2019-12-13 16:33:00'),
(243, 1, 1, 'Actualizacion de slider id: 0', '190.103.28.234', '2019-12-13 16:33:00'),
(244, 1, 1, 'Inicio de sesion', '190.103.28.234', '2019-12-13 16:54:00'),
(245, 1, 1, 'Actualizacion de slider id: 267', '190.103.28.234', '2019-12-13 16:56:00'),
(246, 1, 1, 'Eliminar slider id: 0', '190.103.28.234', '2019-12-13 16:57:00'),
(247, 1, 1, 'Actualizacion de empresa nosotros id: 20', '190.103.28.234', '2019-12-13 17:02:00'),
(248, 1, 1, 'Actualizacion de empresa nosotros id: 20', '190.103.28.234', '2019-12-13 17:02:00'),
(249, 1, 1, 'Actualizacion de empresa nosotros id: 20', '190.103.28.234', '2019-12-13 17:02:00'),
(250, 1, 1, 'Actualizacion de marca id: 14', '190.103.28.234', '2019-12-13 17:08:00'),
(251, 1, 1, 'Actualizacion de categoría producto id: 11', '190.103.28.234', '2019-12-13 17:13:00'),
(252, 1, 1, 'Actualizacion de categoría producto id: 11', '190.103.28.234', '2019-12-13 17:13:00'),
(253, 1, 1, 'Actualizacion de categoría producto id: 11', '190.103.28.234', '2019-12-13 17:13:00'),
(254, 1, 1, 'Actualizacion de categoría producto id: 11', '190.103.28.234', '2019-12-13 17:13:00'),
(255, 1, 1, 'Actualizacion de categoría producto id: 11', '190.103.28.234', '2019-12-13 17:13:00'),
(256, 1, 1, 'Actualizacion de categoría producto id: 11', '190.103.28.234', '2019-12-13 17:13:00'),
(257, 1, 1, 'Actualizacion de categoría producto id: 11', '190.103.28.234', '2019-12-13 17:13:00'),
(258, 1, 1, 'Actualizacion de marca id: 14', '190.103.28.234', '2019-12-13 17:13:00'),
(259, 1, 1, 'Actualizacion tipo de producto id: 45', '190.103.28.234', '2019-12-13 17:16:00'),
(260, 1, 1, 'Actualizacion tipo de producto id: 45', '190.103.28.234', '2019-12-13 17:16:00'),
(261, 1, 1, 'Actualizacion tipo de producto id: 42', '190.103.28.234', '2019-12-13 17:16:00'),
(262, 1, 1, 'Actualizacion de categoría producto id: 11', '190.103.28.234', '2019-12-13 17:17:00'),
(263, 1, 1, 'Actualizacion de categoría producto id: 11', '190.103.28.234', '2019-12-13 17:18:00'),
(264, 1, 1, 'Actualizacion de categoría producto id: 11', '190.103.28.234', '2019-12-13 17:18:00'),
(265, 1, 1, 'Actualizacion tipo de producto id: 45', '190.103.28.234', '2019-12-13 17:18:00'),
(266, 1, 1, 'Actualizacion tipo de producto id: 45', '190.103.28.234', '2019-12-13 17:18:00'),
(267, 1, 1, 'Actualizacion tipo de producto id: 45', '190.103.28.234', '2019-12-13 17:18:00'),
(268, 1, 1, 'Actualizacion de color id: 11', '190.103.28.234', '2019-12-13 17:19:00'),
(269, 1, 1, 'Actualizacion de color id: 11', '190.103.28.234', '2019-12-13 17:29:00'),
(270, 1, 1, 'Actualizacion tipo de producto id: 45', '190.103.28.234', '2019-12-13 17:29:00'),
(271, 1, 1, 'Modificar descripcion id: 1', '190.103.28.234', '2019-12-13 18:08:00'),
(272, 1, 1, 'Modificar descripcion id: 1', '190.103.28.234', '2019-12-13 18:08:00'),
(273, 1, 1, 'Modificar descripcion id: 1', '190.103.28.234', '2019-12-13 18:08:00'),
(274, 1, 1, 'Inicio de sesion', '190.103.28.234', '2019-12-16 13:41:00'),
(275, 1, 1, 'Registro galeria  id: 151', '190.103.28.234', '2019-12-16 13:43:00'),
(276, 1, 1, 'Registro galeria  id: 152', '190.103.28.234', '2019-12-16 13:45:00'),
(277, 1, 1, 'Registro galeria  id: 153', '190.103.28.234', '2019-12-16 13:48:00'),
(278, 1, 1, 'Eliminar galeria  id: 148', '190.103.28.234', '2019-12-16 13:50:00'),
(279, 1, 1, 'Eliminar galeria  id: 150', '190.103.28.234', '2019-12-16 13:50:00'),
(280, 1, 1, 'Eliminar galeria  id: 147', '190.103.28.234', '2019-12-16 13:50:00'),
(281, 1, 1, 'Actualizacion de slider id: 267', '190.103.28.234', '2019-12-16 13:52:00'),
(282, 1, 1, 'Actualizacion de slider id: 267', '190.103.28.234', '2019-12-16 13:52:00'),
(283, 1, 1, 'Actualizacion de slider id: 269', '190.103.28.234', '2019-12-16 13:53:00'),
(284, 1, 1, 'Actualizacion de slider id: 265', '190.103.28.234', '2019-12-16 13:53:00'),
(285, 1, 1, 'Actualizacion de slider id: 266', '190.103.28.234', '2019-12-16 13:53:00'),
(286, 1, 1, 'Actualizacion de slider id: 264', '190.103.28.234', '2019-12-16 13:53:00'),
(287, 1, 1, 'Actualizacion de slider id: 268', '190.103.28.234', '2019-12-16 13:54:00'),
(288, 44, 2, 'Agrego al carrito producto: Zoug Zoug Masculino Talla:S Color:BICOLOR', '190.103.28.234', '2020-01-02 16:09:00'),
(289, 46, 2, 'Agrego al carrito producto: Zoug Zoug Femenino Talla:S Color:BICOLOR', '190.103.28.234', '2020-01-20 13:31:00'),
(290, 46, 2, 'Creación de orden #id:98 con los siguientes items:  producto: Zoug Zoug Femenino Talla:S Color:BICOLOR,', '190.103.28.234', '2020-01-20 13:33:00'),
(291, 46, 2, 'Agrego al carrito producto: Zoug Zoug Masculino Talla:S Color:BICOLOR', '190.103.28.234', '2020-01-20 13:34:00'),
(292, 46, 2, 'Creación de orden #id:99 con los siguientes items:  producto: Zoug Zoug Masculino Talla:S Color:BICOLOR,', '190.103.28.234', '2020-01-20 13:36:00'),
(293, 44, 2, 'Agrego al carrito producto: Premium Masculino Talla:S Color:BICOLOR', '186.167.248.187', '2020-01-20 21:30:00'),
(294, 44, 2, 'Creación de orden #id:100 con los siguientes items:  producto: Premium Masculino Talla:S Color:BICOLOR, producto: Zoug Zoug Masculino Talla:S Color:BICOLOR,', '186.167.248.187', '2020-01-20 21:32:00'),
(295, 1, 1, 'Inicio de sesion', '190.103.28.234', '2020-01-27 19:51:00'),
(296, 1, 1, 'Registro de slider id:272,titulo:Prueba slider 4', '190.103.28.234', '2020-01-27 20:02:00'),
(297, 1, 1, 'Eliminar slider id: 0', '190.103.28.234', '2020-01-27 20:03:00'),
(298, 1, 1, 'Inicio de sesion', '190.103.28.234', '2020-02-05 09:12:00'),
(299, 1, 1, 'Registro galeria  id: 154', '190.103.28.234', '2020-02-05 09:13:00'),
(300, 1, 1, 'Registro galeria  id: 155', '190.103.28.234', '2020-02-05 09:13:00'),
(301, 1, 1, 'Registro galeria  id: 156', '190.103.28.234', '2020-02-05 09:14:00'),
(302, 1, 1, 'Registro galeria  id: 157', '190.103.28.234', '2020-02-05 09:15:00'),
(303, 1, 1, 'Registro galeria  id: 158', '190.103.28.234', '2020-02-05 09:15:00'),
(304, 1, 1, 'Registro galeria  id: 159', '190.103.28.234', '2020-02-05 09:16:00'),
(305, 1, 1, 'Registro galeria  id: 160', '190.103.28.234', '2020-02-05 09:19:00'),
(306, 1, 1, 'Registro galeria  id: 161', '190.103.28.234', '2020-02-05 09:19:00'),
(307, 1, 1, 'Registro galeria  id: 162', '190.103.28.234', '2020-02-05 09:20:00'),
(308, 1, 1, 'Registro galeria  id: 163', '190.103.28.234', '2020-02-05 09:21:00'),
(309, 1, 1, 'Registro galeria  id: 164', '190.103.28.234', '2020-02-05 09:21:00'),
(310, 1, 1, 'Registro galeria  id: 165', '190.103.28.234', '2020-02-05 09:21:00'),
(311, 1, 1, 'Registro galeria  id: 166', '190.103.28.234', '2020-02-05 09:22:00'),
(312, 1, 1, 'Registro galeria  id: 167', '190.103.28.234', '2020-02-05 09:22:00'),
(313, 1, 1, 'Inicio de sesion', '190.103.28.234', '2020-02-05 11:18:00'),
(314, 1, 1, 'Inicio de sesion', '190.103.28.234', '2020-02-05 11:50:00'),
(315, 1, 1, 'Inicio de sesion', '190.103.28.234', '2020-02-05 18:33:00'),
(316, 1, 1, 'Inicio de sesion', '190.103.28.234', '2020-02-05 18:43:00'),
(317, 1, 1, 'Actualizacion de producto id: 122', '190.103.28.234', '2020-02-05 18:50:00'),
(318, 1, 1, 'Actualizacion de producto id: 121', '190.103.28.234', '2020-02-05 18:51:00'),
(319, 1, 1, 'Actualizacion de producto id: 120', '190.103.28.234', '2020-02-05 18:52:00'),
(320, 1, 1, 'Actualizacion de producto id: 120', '190.103.28.234', '2020-02-05 18:56:00'),
(321, 1, 1, 'Actualizacion de producto id: 119', '190.103.28.234', '2020-02-05 18:56:00'),
(322, 1, 1, 'Actualizacion de producto id: 118', '190.103.28.234', '2020-02-05 18:57:00'),
(323, 1, 1, 'Actualizacion de producto id: 118', '190.103.28.234', '2020-02-05 18:57:00'),
(324, 1, 1, 'Actualizacion de producto id: 121', '190.103.28.234', '2020-02-05 18:58:00'),
(325, 1, 1, 'Actualizacion de producto id: 117', '190.103.28.234', '2020-02-05 18:58:00'),
(326, 1, 1, 'Actualizacion de producto id: 118', '190.103.28.234', '2020-02-05 18:59:00'),
(327, 1, 1, 'Actualizacion de producto id: 116', '190.103.28.234', '2020-02-05 18:59:00'),
(328, 1, 1, 'Actualizacion de producto id: 115', '190.103.28.234', '2020-02-05 18:59:00'),
(329, 1, 1, 'Actualizacion de producto id: 116', '190.103.28.234', '2020-02-05 19:00:00'),
(330, 1, 1, 'Actualizacion de producto id: 115', '190.103.28.234', '2020-02-05 19:00:00'),
(331, 1, 1, 'Actualizacion de producto id: 114', '190.103.28.234', '2020-02-05 19:01:00'),
(332, 1, 1, 'Actualizacion de producto id: 113', '190.103.28.234', '2020-02-05 19:02:00'),
(333, 1, 1, 'Actualizacion de producto id: 112', '190.103.28.234', '2020-02-05 19:02:00'),
(334, 1, 1, 'Actualizacion de producto id: 111', '190.103.28.234', '2020-02-05 19:03:00'),
(335, 1, 1, 'Registro galeria  id: 168', '190.103.28.234', '2020-02-05 19:05:00'),
(336, 1, 1, 'Actualizacion de producto id: 110', '190.103.28.234', '2020-02-05 19:29:00'),
(337, 1, 1, 'Actualizacion de producto id: 109', '190.103.28.234', '2020-02-05 19:30:00'),
(338, 1, 1, 'Inicio de sesion', '190.103.28.234', '2020-02-05 19:42:00'),
(339, 1, 1, 'Inicio de sesion', '::1', '2020-02-05 20:57:00'),
(340, 1, 1, 'Registro cantidad producto id: 201', '::1', '2020-02-05 20:57:00'),
(341, 1, 1, 'Registro cantidad producto id: 202', '::1', '2020-02-05 20:57:00'),
(342, 1, 1, 'Registro cantidad producto id: 203', '::1', '2020-02-05 20:57:00'),
(343, 1, 1, 'Registro cantidad producto id: 204', '::1', '2020-02-05 20:57:00'),
(344, 1, 1, 'Registro cantidad producto id: 205', '::1', '2020-02-05 20:58:00'),
(345, 1, 1, 'Registro cantidad producto id: 206', '::1', '2020-02-05 20:58:00'),
(346, 1, 1, 'Registro cantidad producto id: 207', '::1', '2020-02-05 20:58:00'),
(347, 1, 1, 'Registro cantidad producto id: 208', '::1', '2020-02-05 20:58:00'),
(348, 1, 1, 'Registro cantidad producto id: 209', '::1', '2020-02-05 20:58:00'),
(349, 1, 1, 'Registro cantidad producto id: 210', '::1', '2020-02-05 20:59:00'),
(350, 1, 1, 'Registro cantidad producto id: 211', '::1', '2020-02-05 20:59:00'),
(351, 1, 1, 'Registro cantidad producto id: 212', '::1', '2020-02-05 20:59:00'),
(352, 1, 1, 'Registro cantidad producto id: 213', '::1', '2020-02-05 20:59:00'),
(353, 1, 1, 'Registro cantidad producto id: 214', '::1', '2020-02-05 21:00:00'),
(354, 1, 1, 'Registro cantidad producto id: 215', '::1', '2020-02-05 21:00:00'),
(355, 1, 1, 'Registro cantidad producto id: 216', '::1', '2020-02-05 21:00:00'),
(356, 1, 1, 'Registro cantidad producto id: 217', '::1', '2020-02-05 21:00:00'),
(357, 1, 1, 'Registro cantidad producto id: 218', '::1', '2020-02-05 21:00:00'),
(358, 1, 1, 'Registro cantidad producto id: 219', '::1', '2020-02-05 21:00:00'),
(359, 1, 1, 'Registro cantidad producto id: 220', '::1', '2020-02-05 21:01:00'),
(360, 1, 1, 'Registro cantidad producto id: 221', '::1', '2020-02-05 21:01:00'),
(361, 1, 1, 'Registro cantidad producto id: 222', '::1', '2020-02-05 21:01:00'),
(362, 1, 1, 'Registro cantidad producto id: 223', '::1', '2020-02-05 21:02:00'),
(363, 1, 1, 'Registro cantidad producto id: 224', '::1', '2020-02-05 21:02:00'),
(364, 1, 1, 'Registro cantidad producto id: 225', '::1', '2020-02-05 21:02:00'),
(365, 1, 1, 'Registro cantidad producto id: 226', '::1', '2020-02-05 21:02:00'),
(366, 1, 1, 'Registro cantidad producto id: 227', '::1', '2020-02-05 21:02:00'),
(367, 1, 1, 'Registro cantidad producto id: 228', '::1', '2020-02-05 21:02:00'),
(368, 1, 1, 'Registro cantidad producto id: 229', '::1', '2020-02-05 21:06:00'),
(369, 1, 1, 'Registro cantidad producto id: 230', '::1', '2020-02-05 21:06:00'),
(370, 1, 1, 'Registro cantidad producto id: 231', '::1', '2020-02-05 21:06:00'),
(371, 1, 1, 'Registro cantidad producto id: 232', '::1', '2020-02-05 21:06:00'),
(372, 1, 1, 'Inicio de sesion', '::1', '2020-02-06 14:04:00'),
(373, 1, 1, 'Registro cantidad producto id: 233', '::1', '2020-02-06 14:04:00'),
(374, 1, 1, 'Registro cantidad producto id: 234', '::1', '2020-02-06 14:04:00'),
(375, 1, 1, 'Registro cantidad producto id: 235', '::1', '2020-02-06 14:04:00'),
(376, 1, 1, 'Registro cantidad producto id: 236', '::1', '2020-02-06 14:04:00'),
(377, 1, 1, 'Registro cantidad producto id: 237', '::1', '2020-02-06 14:05:00'),
(378, 1, 1, 'Registro cantidad producto id: 238', '::1', '2020-02-06 14:05:00'),
(379, 1, 1, 'Registro cantidad producto id: 239', '::1', '2020-02-06 14:05:00'),
(380, 1, 1, 'Registro cantidad producto id: 240', '::1', '2020-02-06 14:06:00'),
(381, 1, 1, 'Registro cantidad producto id: 241', '::1', '2020-02-06 14:06:00'),
(382, 1, 1, 'Registro cantidad producto id: 242', '::1', '2020-02-06 14:06:00'),
(383, 1, 1, 'Registro cantidad producto id: 243', '::1', '2020-02-06 14:06:00'),
(384, 1, 1, 'Registro cantidad producto id: 244', '::1', '2020-02-06 14:06:00'),
(385, 1, 1, 'Registro cantidad producto id: 245', '::1', '2020-02-06 14:07:00'),
(386, 1, 1, 'Registro cantidad producto id: 246', '::1', '2020-02-06 14:07:00'),
(387, 1, 1, 'Registro cantidad producto id: 247', '::1', '2020-02-06 14:07:00'),
(388, 1, 1, 'Registro cantidad producto id: 248', '::1', '2020-02-06 14:07:00'),
(389, 1, 1, 'Registro cantidad producto id: 249', '::1', '2020-02-06 14:07:00'),
(390, 1, 1, 'Registro cantidad producto id: 250', '::1', '2020-02-06 14:07:00'),
(391, 1, 1, 'Registro cantidad producto id: 251', '::1', '2020-02-06 14:09:00'),
(392, 1, 1, 'Registro cantidad producto id: 252', '::1', '2020-02-06 14:09:00'),
(393, 1, 1, 'Registro cantidad producto id: 253', '::1', '2020-02-06 14:09:00'),
(394, 44, 2, 'Agrego al carrito producto: Premium Femenino Talla:S Color:BICOLOR', '::1', '2020-02-06 14:18:00'),
(395, 44, 2, 'Se modifico la cantidad del producto: Premium Femenino Talla:S Color:BICOLOR cantidad ingresada:0 cantidad anterior:2 cantidad en tabla: 98 cantidad nueva calculo:  cantidad total: 98', '::1', '2020-02-06 14:18:00'),
(396, 44, 2, 'Creación de orden #id:101 con los siguientes items:  producto: Premium Femenino Talla:S Color:BICOLOR,', '::1', '2020-02-06 14:29:00'),
(397, 1, 1, 'Inicio de sesion', '::1', '2020-02-10 18:07:00'),
(398, 1, 1, 'Registro de categoría producto id:11,titulo:rain', '::1', '2020-02-10 18:48:00'),
(399, 1, 1, 'Registro galeria  id: 169', '::1', '2020-02-10 18:53:00'),
(400, 1, 1, 'Registro de slider id:0,titulo:SLIDER', '::1', '2020-02-10 18:53:00'),
(401, 1, 1, 'Registro de categoría producto id:11,titulo:play', '::1', '2020-02-10 18:56:00'),
(402, 1, 1, 'Registro galeria  id: 170', '::1', '2020-02-10 19:00:00'),
(403, 1, 1, 'Registro de portafolio id:10,titulo:proyecto 1', '::1', '2020-02-10 20:31:00'),
(404, 1, 1, 'Registro de portafolio id:11,titulo:proyecto 1', '::1', '2020-02-10 20:37:00'),
(405, 1, 1, 'Inactivar portafolio: 11', '::1', '2020-02-10 20:37:00'),
(406, 1, 1, 'Inactivar portafolio: 11', '::1', '2020-02-10 20:37:00'),
(407, 1, 1, 'Activar portafolio: 11', '::1', '2020-02-10 20:41:00'),
(408, 1, 1, 'Inicio de sesion', '::1', '2020-02-28 09:06:00'),
(409, 1, 1, 'Eliminar galeria  id: 169', '::1', '2020-02-28 09:39:00'),
(410, 1, 1, 'Eliminar galeria  id: 170', '::1', '2020-02-28 09:39:00'),
(411, 1, 1, 'Registro galeria  id: 171', '::1', '2020-02-28 09:40:00'),
(412, 1, 1, 'Registro galeria  id: 172', '::1', '2020-02-28 09:40:00'),
(413, 1, 1, 'Registro galeria  id: 173', '::1', '2020-02-28 09:40:00'),
(414, 1, 1, 'Registro de slider id:1,titulo:Con Uniseguros', '::1', '2020-02-28 09:41:00'),
(415, 1, 1, 'Registro de slider id:2,titulo:Sabemos', '::1', '2020-02-28 09:42:00'),
(416, 1, 1, 'Actualizacion de slider id: 1', '::1', '2020-02-28 09:45:00'),
(417, 1, 1, 'Actualizacion de slider id: 1', '::1', '2020-02-28 09:48:00'),
(418, 1, 1, 'Actualizacion de slider id: 2', '::1', '2020-02-28 09:48:00'),
(419, 1, 1, 'Actualizacion de slider id: 2', '::1', '2020-02-28 09:49:00'),
(420, 1, 1, 'Actualizacion de slider id: 2', '::1', '2020-02-28 09:56:00'),
(421, 1, 1, 'Actualizacion de slider id: 2', '::1', '2020-02-28 09:56:00'),
(422, 1, 1, 'Inicio de sesion', '::1', '2020-02-28 13:12:00'),
(423, 1, 1, 'Registro de slider id:3,titulo:Ahora', '::1', '2020-02-28 13:13:00'),
(424, 1, 1, 'Actualizacion de slider id: 2', '::1', '2020-02-28 13:56:00'),
(425, 1, 1, 'Inicio de sesion', '::1', '2020-02-28 14:31:00'),
(426, 1, 1, 'Modificar categtorias cms id: 20', '::1', '2020-02-28 14:42:00'),
(427, 1, 1, 'Registro galeria  id: 174', '::1', '2020-02-28 14:42:00'),
(428, 1, 1, 'Eliminar galeria  id: 174', '::1', '2020-02-28 14:47:00'),
(429, 1, 1, 'Registro galeria  id: 175', '::1', '2020-02-28 14:51:00'),
(430, 1, 1, 'Registro de categoría producto id:11,titulo:Sabemos lo que es importante para ti', '::1', '2020-02-28 15:00:00'),
(431, 1, 1, 'Actualizacion de categoría producto id: 2', '::1', '2020-02-28 16:36:00'),
(432, 1, 1, 'Inicio de sesion', '::1', '2020-02-28 18:46:00'),
(433, 1, 1, 'Actualizacion de categoría producto id: 2', '::1', '2020-02-28 19:49:00'),
(434, 1, 1, 'Actualizacion de categoría producto id: 2', '::1', '2020-02-28 19:49:00'),
(435, 1, 1, 'Actualizacion de categoría producto id: 2', '::1', '2020-02-28 19:51:00'),
(436, 1, 1, 'Registro galeria  id: 176', '::1', '2020-02-28 19:54:00'),
(437, 1, 1, 'Registro de categoría producto id:11,titulo:Valores', '::1', '2020-02-28 19:55:00'),
(438, 1, 1, 'Actualizacion de categoría producto id: 3', '::1', '2020-02-28 19:55:00'),
(439, 1, 1, 'Actualizacion de categoría producto id: 2', '::1', '2020-02-28 19:55:00'),
(440, 1, 1, 'Actualizacion de categoría producto id: 2', '::1', '2020-02-28 19:55:00'),
(441, 1, 1, 'Inactivar categoría producto id: 1', '::1', '2020-02-28 20:00:00'),
(442, 1, 1, 'Inactivar categoría producto id: 3', '::1', '2020-02-28 20:00:00'),
(443, 1, 1, 'Eliminar categoría producto id: 1', '::1', '2020-02-28 20:01:00'),
(444, 1, 1, 'Inicio de sesion', '::1', '2020-03-02 08:45:00'),
(445, 1, 1, 'Actualizacion de categoría producto id: 2', '::1', '2020-03-02 08:46:00'),
(446, 1, 1, 'Actualizacion de slider id: 2', '::1', '2020-03-02 09:00:00'),
(447, 1, 1, 'Registro de categoría producto id:11,titulo:Misión', '::1', '2020-03-02 09:11:00'),
(448, 1, 1, 'Registro de categoría producto id:11,titulo:Visión', '::1', '2020-03-02 09:11:00'),
(449, 1, 1, 'Activar categoría producto id: 3', '::1', '2020-03-02 09:11:00'),
(450, 1, 1, 'Inicio de sesion', '::1', '2020-03-03 09:00:00'),
(451, 1, 1, 'Actualizacion de categoría producto id: 2', '::1', '2020-03-03 09:00:00'),
(452, 1, 1, 'Actualizacion de categoría producto id: 4', '::1', '2020-03-03 09:08:00'),
(453, 1, 1, 'Actualizacion de categoría producto id: 4', '::1', '2020-03-03 09:08:00'),
(454, 1, 1, 'Actualizacion de categoría producto id: 5', '::1', '2020-03-03 09:11:00'),
(455, 1, 1, 'Actualizacion de categoría producto id: 3', '::1', '2020-03-03 09:12:00'),
(456, 1, 1, 'Inicio de sesion', '::1', '2020-03-03 12:40:00'),
(457, 1, 1, 'Actualizacion de categoría producto id: 3', '::1', '2020-03-03 12:40:00'),
(458, 1, 1, 'Actualizacion de categoría producto id: 3', '::1', '2020-03-03 12:40:00'),
(459, 1, 1, 'Inicio de sesion', '::1', '2020-03-04 07:51:00'),
(460, 1, 1, 'Actualizacion de categoría producto id: 3', '::1', '2020-03-04 07:52:00'),
(461, 1, 1, 'Actualizacion de categoría producto id: 3', '::1', '2020-03-04 07:56:00'),
(462, 1, 1, 'Actualizacion de categoría producto id: 5', '::1', '2020-03-04 07:59:00'),
(463, 1, 1, 'Eliminar categorias: 21', '::1', '2020-03-04 09:10:00'),
(464, 1, 1, 'Registro categtorias cms id: 23', '::1', '2020-03-04 09:10:00'),
(465, 1, 1, 'Registro galeria  id: 177', '::1', '2020-03-04 11:01:00'),
(466, 1, 1, 'Registro galeria  id: 178', '::1', '2020-03-04 11:01:00'),
(467, 1, 1, 'Registro galeria  id: 179', '::1', '2020-03-04 11:01:00'),
(468, 1, 1, 'Registro de publicidad id:2,titulo:Rod Montana', '::1', '2020-03-04 11:03:00'),
(469, 1, 1, 'Registro de publicidad id:3,titulo:Rod Montana', '::1', '2020-03-04 11:05:00'),
(470, 1, 1, 'Registro de publicidad id:4,titulo:Rod Montana', '::1', '2020-03-04 11:05:00'),
(471, 1, 1, 'Registro de publicidad id:5,titulo:Rod Montana', '::1', '2020-03-04 11:06:00'),
(472, 1, 1, 'Inactivar directiva id: 5', '::1', '2020-03-04 13:40:00'),
(473, 1, 1, 'Activar directiva id: 5', '::1', '2020-03-04 13:42:00'),
(474, 1, 1, 'Eliminar directiva id: 5', '::1', '2020-03-04 13:42:00'),
(475, 1, 1, 'Actualizacion de directiva id: 5', '::1', '2020-03-04 13:54:00'),
(476, 1, 1, 'Actualizacion de directiva id: 5', '::1', '2020-03-04 13:54:00'),
(477, 1, 1, 'Actualizacion de directiva id: 5', '::1', '2020-03-04 13:54:00'),
(478, 1, 1, 'Registro de directiva id:6,titulo:Wilmer PErez', '::1', '2020-03-04 13:57:00'),
(479, 1, 1, 'Actualizacion de directiva id: 5', '::1', '2020-03-04 13:57:00'),
(480, 1, 1, 'Registro galeria  id: 180', '::1', '2020-03-04 14:20:00'),
(481, 1, 1, 'Registro galeria  id: 181', '::1', '2020-03-04 14:20:00'),
(482, 1, 1, 'Registro galeria  id: 182', '::1', '2020-03-04 14:20:00'),
(483, 1, 1, 'Actualizacion de categoría producto id: 3', '::1', '2020-03-04 14:20:00'),
(484, 1, 1, 'Actualizacion de categoría producto id: 4', '::1', '2020-03-04 14:20:00'),
(485, 1, 1, 'Actualizacion de categoría producto id: 5', '::1', '2020-03-04 14:21:00'),
(486, 1, 1, 'Actualizacion de categoría producto id: 5', '::1', '2020-03-04 14:35:00'),
(487, 1, 1, 'Inicio de sesion', '::1', '2020-03-05 08:02:00'),
(488, 1, 1, 'Actualizacion de categoría producto id: 4', '::1', '2020-03-05 08:03:00'),
(489, 1, 1, 'Actualizacion de directiva id: 6', '::1', '2020-03-05 09:47:00'),
(490, 1, 1, 'Actualizacion de directiva id: 5', '::1', '2020-03-05 09:48:00'),
(491, 1, 1, 'Registro de directiva id:7,titulo:Ricardo Sosa Branger', '::1', '2020-03-05 09:48:00'),
(492, 1, 1, 'Registro de directiva id:8,titulo:Jesús Alberto Ortega', '::1', '2020-03-05 09:49:00'),
(493, 1, 1, 'Registro de directiva id:9,titulo:Enrique Villa Seriña', '::1', '2020-03-05 09:49:00'),
(494, 1, 1, 'Inicio de sesion', '::1', '2020-03-05 12:47:00'),
(495, 1, 1, 'Registro categtorias cms id: 24', '::1', '2020-03-05 12:48:00'),
(496, 1, 1, 'Registro galeria  id: 183', '::1', '2020-03-05 12:49:00'),
(497, 1, 1, 'Registro galeria  id: 184', '::1', '2020-03-05 12:49:00'),
(498, 1, 1, 'Registro galeria  id: 185', '::1', '2020-03-05 12:49:00'),
(499, 1, 1, 'Registro galeria  id: 186', '::1', '2020-03-05 12:49:00'),
(500, 1, 1, 'Registro galeria  id: 187', '::1', '2020-03-05 12:49:00'),
(501, 1, 1, 'Eliminar galeria  id: 183', '::1', '2020-03-05 12:49:00'),
(502, 1, 1, 'Eliminar galeria  id: 187', '::1', '2020-03-05 12:50:00'),
(503, 1, 1, 'Registro galeria  id: 188', '::1', '2020-03-05 12:50:00'),
(504, 1, 1, 'Registro galeria  id: 189', '::1', '2020-03-05 12:50:00'),
(505, 1, 1, 'Registro galeria  id: 190', '::1', '2020-03-05 12:50:00'),
(506, 1, 1, 'Registro galeria  id: 191', '::1', '2020-03-05 12:51:00'),
(507, 1, 1, 'Registro de reaseguradoras id:1,titulo:uno', '::1', '2020-03-05 13:15:00'),
(508, 1, 1, 'Registro de reaseguradoras id:2,titulo:dos', '::1', '2020-03-05 13:19:00'),
(509, 1, 1, 'Inactivar reaseguradoras id: 2', '::1', '2020-03-05 13:45:00'),
(510, 1, 1, 'Eliminar reaseguradoras id: 2', '::1', '2020-03-05 13:45:00'),
(511, 1, 1, 'Actualizacion de reaseguradoras id: 1', '::1', '2020-03-05 13:48:00'),
(512, 1, 1, 'Actualizacion de reaseguradoras id: 1', '::1', '2020-03-05 13:53:00'),
(513, 1, 1, 'Actualizacion de reaseguradoras id: 1', '::1', '2020-03-05 13:55:00'),
(514, 1, 1, 'Actualizacion de reaseguradoras id: 1', '::1', '2020-03-05 13:56:00'),
(515, 1, 1, 'Registro de reaseguradoras id:3,titulo:dos', '::1', '2020-03-05 13:57:00'),
(516, 1, 1, 'Actualizacion de reaseguradoras id: 3', '::1', '2020-03-05 13:58:00'),
(517, 1, 1, 'Actualizacion de reaseguradoras id: 3', '::1', '2020-03-05 14:24:00'),
(518, 1, 1, 'Actualizacion de reaseguradoras id: 1', '::1', '2020-03-05 14:24:00'),
(519, 1, 1, 'Registro de reaseguradoras id:4,titulo:tres', '::1', '2020-03-05 14:24:00'),
(520, 1, 1, 'Registro de reaseguradoras id:5,titulo:cuatro', '::1', '2020-03-05 14:24:00'),
(521, 1, 1, 'Registro de reaseguradoras id:6,titulo:cinco', '::1', '2020-03-05 14:24:00'),
(522, 1, 1, 'Registro de reaseguradoras id:7,titulo:seis', '::1', '2020-03-05 14:25:00'),
(523, 1, 1, 'Registro de reaseguradoras id:8,titulo:siete', '::1', '2020-03-05 14:25:00'),
(524, 1, 1, 'Inicio de sesion', '::1', '2020-03-06 08:46:00'),
(525, 1, 1, 'Eliminar galeria  id: 180', '::1', '2020-03-06 08:59:00'),
(526, 1, 1, 'Eliminar galeria  id: 181', '::1', '2020-03-06 09:10:00'),
(527, 1, 1, 'Registro galeria  id: 193', '::1', '2020-03-06 09:35:00'),
(528, 1, 1, 'Registro galeria  id: 194', '::1', '2020-03-06 09:35:00'),
(529, 1, 1, 'Actualizacion de categoría producto id: 3', '::1', '2020-03-06 09:36:00'),
(530, 1, 1, 'Actualizacion de categoría producto id: 4', '::1', '2020-03-06 09:36:00'),
(531, 1, 1, 'Eliminar galeria  id: 190', '::1', '2020-03-06 09:38:00'),
(532, 1, 1, 'Registro galeria  id: 195', '::1', '2020-03-06 09:40:00'),
(533, 1, 1, 'Actualizacion de reaseguradoras id: 1', '::1', '2020-03-06 09:41:00'),
(534, 1, 1, 'Eliminar galeria  id: 177', '::1', '2020-03-06 09:43:00'),
(535, 1, 1, 'Registro galeria  id: 196', '::1', '2020-03-06 09:44:00'),
(536, 1, 1, 'Registro galeria  id: 197', '::1', '2020-03-06 09:44:00'),
(537, 1, 1, 'Registro galeria  id: 198', '::1', '2020-03-06 09:44:00'),
(538, 1, 1, 'Registro galeria  id: 199', '::1', '2020-03-06 09:44:00'),
(539, 1, 1, 'Actualizacion de directiva id: 6', '::1', '2020-03-06 09:45:00'),
(540, 1, 1, 'Actualizacion de directiva id: 5', '::1', '2020-03-06 09:45:00'),
(541, 1, 1, 'Actualizacion de directiva id: 7', '::1', '2020-03-06 09:45:00'),
(542, 1, 1, 'Actualizacion de directiva id: 8', '::1', '2020-03-06 09:45:00'),
(543, 1, 1, 'Inicio de sesion', '::1', '2020-03-09 07:53:00'),
(544, 1, 1, 'Actualizacion de directiva id: 5', '::1', '2020-03-09 07:54:00'),
(545, 1, 1, 'Actualizacion de directiva id: 6', '::1', '2020-03-09 08:05:00'),
(546, 1, 1, 'Actualizacion de directiva id: 6', '::1', '2020-03-09 08:05:00'),
(547, 1, 1, 'Actualizacion de categoría producto id: 2', '::1', '2020-03-09 08:08:00'),
(548, 1, 1, 'Actualizacion de reaseguradoras id: 3', '::1', '2020-03-09 08:09:00'),
(549, 1, 1, 'Actualizacion de slider id: 1', '::1', '2020-03-09 08:10:00'),
(550, 1, 1, 'Registro de direccion id:15,titulo:Prueba direccion 1', '::1', '2020-03-09 09:01:00'),
(551, 1, 1, 'Registro footer id: 5', '::1', '2020-03-09 10:06:00'),
(552, 1, 1, 'Actualizar footer id: 5', '::1', '2020-03-09 11:14:00'),
(553, 1, 1, 'Actualizacion redes sociales id: 11', '::1', '2020-03-09 11:35:00'),
(554, 1, 1, 'Actualizacion redes sociales id: 11', '::1', '2020-03-09 11:35:00'),
(555, 1, 1, 'Actualizacion redes sociales id: 11', '::1', '2020-03-09 11:36:00'),
(556, 1, 1, 'Modificar palabras claves id: 66', '::1', '2020-03-09 11:45:00'),
(557, 1, 1, 'Inicio de sesion', '::1', '2020-03-09 15:08:00'),
(558, 1, 1, 'Actualizar footer id: 5', '::1', '2020-03-09 15:09:00'),
(559, 1, 1, 'Actualizar footer id: 5', '::1', '2020-03-09 15:10:00'),
(560, 1, 1, 'Inicio de sesion', '::1', '2020-03-10 08:15:00'),
(561, 1, 1, 'Eliminar direccion id: 12', '::1', '2020-03-10 08:15:00'),
(562, 1, 1, 'Eliminar direccion id: 14', '::1', '2020-03-10 08:15:00'),
(563, 1, 1, 'Eliminar direccion id: 15', '::1', '2020-03-10 08:15:00'),
(564, 1, 1, 'Registro de direccion id:16,titulo:Caracas', '::1', '2020-03-10 08:20:00'),
(565, 1, 1, 'Registro de direccion id:17,titulo:San Antonio', '::1', '2020-03-10 08:26:00'),
(566, 1, 1, 'Actualizacion de marca id: 16', '::1', '2020-03-10 08:29:00');
INSERT INTO `auditoria` (`id`, `id_usuario`, `modulo`, `accion`, `ip`, `fecha_hora`) VALUES
(567, 1, 1, 'Actualizacion de marca id: 16', '::1', '2020-03-10 08:34:00'),
(568, 1, 1, 'Actualizacion de marca id: 17', '::1', '2020-03-10 08:35:00'),
(569, 1, 1, 'Registro de direccion id:18,titulo:Maracay', '::1', '2020-03-10 08:40:00'),
(570, 1, 1, 'Inicio de sesion', '::1', '2020-03-10 14:23:00'),
(571, 1, 1, 'Actualizacion de marca id: 16', '::1', '2020-03-10 14:30:00'),
(572, 1, 1, 'Actualizacion de marca id: 17', '::1', '2020-03-10 14:35:00'),
(573, 1, 1, 'Actualizacion de marca id: 17', '::1', '2020-03-10 14:48:00'),
(574, 1, 1, 'Actualizacion de marca id: 18', '::1', '2020-03-10 14:54:00'),
(575, 1, 1, 'Actualizacion de marca id: 18', '::1', '2020-03-10 14:54:00'),
(576, 1, 1, 'Actualizacion de marca id: 17', '::1', '2020-03-10 14:56:00'),
(577, 1, 1, 'Actualizacion de marca id: 16', '::1', '2020-03-10 14:56:00'),
(578, 1, 1, 'Inicio de sesion', '::1', '2020-03-16 15:44:00'),
(579, 1, 1, 'Eliminar galeria  id: 191', '::1', '2020-03-16 15:44:00'),
(580, 1, 1, 'Eliminar galeria  id: 195', '::1', '2020-03-16 15:44:00'),
(581, 1, 1, 'Eliminar galeria  id: 186', '::1', '2020-03-16 15:44:00'),
(582, 1, 1, 'Eliminar galeria  id: 185', '::1', '2020-03-16 15:45:00'),
(583, 1, 1, 'Eliminar galeria  id: 184', '::1', '2020-03-16 15:45:00'),
(584, 1, 1, 'Eliminar galeria  id: 188', '::1', '2020-03-16 15:45:00'),
(585, 1, 1, 'Eliminar galeria  id: 189', '::1', '2020-03-16 15:45:00'),
(586, 1, 1, 'Registro galeria  id: 200', '::1', '2020-03-16 15:51:00'),
(587, 1, 1, 'Registro galeria  id: 201', '::1', '2020-03-16 15:52:00'),
(588, 1, 1, 'Registro galeria  id: 202', '::1', '2020-03-16 15:52:00'),
(589, 1, 1, 'Registro galeria  id: 203', '::1', '2020-03-16 15:52:00'),
(590, 1, 1, 'Registro galeria  id: 204', '::1', '2020-03-16 15:53:00'),
(591, 1, 1, 'Actualizacion de reaseguradoras id: 3', '::1', '2020-03-16 15:53:00'),
(592, 1, 1, 'Actualizacion de reaseguradoras id: 1', '::1', '2020-03-16 15:54:00'),
(593, 1, 1, 'Actualizacion de reaseguradoras id: 4', '::1', '2020-03-16 15:54:00'),
(594, 1, 1, 'Actualizacion de reaseguradoras id: 5', '::1', '2020-03-16 15:55:00'),
(595, 1, 1, 'Actualizacion de reaseguradoras id: 6', '::1', '2020-03-16 15:55:00'),
(596, 1, 1, 'Eliminar reaseguradoras id: 7', '::1', '2020-03-16 15:55:00'),
(597, 1, 1, 'Eliminar reaseguradoras id: 8', '::1', '2020-03-16 15:56:00'),
(598, 1, 1, 'Actualizacion de reaseguradoras id: 4', '::1', '2020-03-16 16:59:00'),
(599, 1, 1, 'Actualizacion de reaseguradoras id: 1', '::1', '2020-03-16 16:59:00'),
(600, 1, 1, 'Actualizacion de reaseguradoras id: 3', '::1', '2020-03-16 16:59:00'),
(601, 1, 1, 'Actualizacion de reaseguradoras id: 3', '::1', '2020-03-16 17:00:00'),
(602, 1, 1, 'Actualizacion de reaseguradoras id: 5', '::1', '2020-03-16 17:00:00'),
(603, 1, 1, 'Actualizacion de reaseguradoras id: 6', '::1', '2020-03-16 17:01:00'),
(604, 1, 1, 'Registro galeria  id: 205', '::1', '2020-03-16 17:10:00'),
(605, 1, 1, 'Actualizacion de directiva id: 6', '::1', '2020-03-16 17:10:00'),
(606, 1, 1, 'Registro galeria  id: 206', '::1', '2020-03-16 17:12:00'),
(607, 1, 1, 'Actualizacion de directiva id: 5', '::1', '2020-03-16 17:12:00'),
(608, 1, 1, 'Actualizacion de directiva id: 6', '::1', '2020-03-16 17:15:00'),
(609, 1, 1, 'Actualizacion de directiva id: 7', '::1', '2020-03-16 17:15:00'),
(610, 1, 1, 'Actualizacion de directiva id: 8', '::1', '2020-03-16 17:15:00'),
(611, 1, 1, 'Actualizacion de directiva id: 9', '::1', '2020-03-16 17:15:00'),
(612, 1, 1, 'Inicio de sesion', '::1', '2020-03-21 14:09:00'),
(613, 1, 1, 'Cerrar de sesion', '::1', '2020-03-21 14:12:00'),
(614, 1, 1, 'Inicio de sesion', '::1', '2020-03-30 09:20:00'),
(615, 1, 1, 'Registro galeria  id: 207', '::1', '2020-03-30 09:20:00'),
(616, 1, 1, 'Registro galeria  id: 208', '::1', '2020-03-30 09:20:00'),
(617, 1, 1, 'Actualizacion de categoría producto id: 5', '::1', '2020-03-30 09:21:00'),
(618, 1, 1, 'Eliminar galeria  id: 208', '::1', '2020-03-30 09:25:00'),
(619, 1, 1, 'Eliminar galeria  id: 207', '::1', '2020-03-30 09:25:00'),
(620, 1, 1, 'Registro galeria  id: 209', '::1', '2020-03-30 09:29:00'),
(621, 1, 1, 'Registro galeria  id: 210', '::1', '2020-03-30 09:29:00'),
(622, 1, 1, 'Actualizacion de categoría producto id: 5', '::1', '2020-03-30 09:30:00'),
(623, 1, 1, 'Registro galeria  id: 211', '::1', '2020-03-30 09:32:00'),
(624, 1, 1, 'Registro galeria  id: 212', '::1', '2020-03-30 09:32:00'),
(625, 1, 1, 'Registro galeria  id: 213', '::1', '2020-03-30 09:32:00'),
(626, 1, 1, 'Registro galeria  id: 214', '::1', '2020-03-30 09:32:00'),
(627, 1, 1, 'Registro galeria  id: 215', '::1', '2020-03-30 09:33:00'),
(628, 1, 1, 'Actualizacion de reaseguradoras id: 3', '::1', '2020-03-30 09:33:00'),
(629, 1, 1, 'Actualizacion de reaseguradoras id: 1', '::1', '2020-03-30 09:33:00'),
(630, 1, 1, 'Actualizacion de reaseguradoras id: 5', '::1', '2020-03-30 09:34:00'),
(631, 1, 1, 'Actualizacion de reaseguradoras id: 6', '::1', '2020-03-30 09:34:00'),
(632, 1, 1, 'Eliminar galeria  id: 214', '::1', '2020-03-30 09:34:00'),
(633, 1, 1, 'Registro galeria  id: 216', '::1', '2020-03-30 09:35:00'),
(634, 1, 1, 'Actualizacion de reaseguradoras id: 4', '::1', '2020-03-30 09:35:00'),
(635, 1, 1, 'Actualizacion de categoría producto id: 5', '::1', '2020-03-30 09:39:00'),
(636, 1, 1, 'Eliminar galeria  id: 204', '::1', '2020-03-30 11:21:00'),
(637, 1, 1, 'Eliminar galeria  id: 203', '::1', '2020-03-30 11:21:00'),
(638, 1, 1, 'Inicio de sesion', '::1', '2020-03-30 16:07:00'),
(639, 1, 1, 'Inicio de sesion', '::1', '2020-03-30 20:43:00'),
(640, 1, 1, 'Inicio de sesion', '::1', '2020-03-31 13:47:00'),
(641, 1, 1, 'Inicio de sesion', '::1', '2020-03-31 14:56:00'),
(642, 1, 1, 'Cerrar de sesion', '::1', '2020-03-31 14:57:00'),
(643, 0, 1, 'Cerrar de sesion', '::1', '2020-03-31 14:57:00'),
(644, 1, 1, 'Inicio de sesion', '::1', '2020-03-31 14:58:00'),
(645, 1, 1, 'Cerrar de sesion', '::1', '2020-03-31 15:27:00'),
(646, 1, 1, 'Inicio de sesion', '::1', '2020-03-31 15:32:00'),
(647, 1, 1, 'Inicio de sesion', '::1', '2020-03-31 15:36:00'),
(648, 1, 1, 'Inicio de sesion', '::1', '2020-04-01 10:29:00'),
(649, 1, 1, 'Registro categtorias cms id: 25', '::1', '2020-04-01 10:31:00'),
(650, 1, 1, 'Registro galeria  id: 217', '::1', '2020-04-01 10:33:00'),
(651, 1, 1, 'Inicio de sesion', '::1', '2020-04-01 10:35:00'),
(652, 1, 1, 'Modificar usuarios id: 1', '::1', '2020-04-01 10:38:00'),
(653, 1, 1, 'Modificar usuarios id: 1', '::1', '2020-04-01 10:43:00'),
(654, 1, 1, 'Modificar usuarios id: 1', '::1', '2020-04-01 11:03:00'),
(655, 1, 1, 'Registro usuarios id: 12', '::1', '2020-04-01 11:23:00'),
(656, 1, 1, 'Modificar usuarios id: 13', '::1', '2020-04-01 11:23:00'),
(657, 1, 1, 'Inicio de sesion', '::1', '2020-04-01 11:42:00'),
(658, 1, 1, 'Modificar usuarios id: 13', '::1', '2020-04-01 11:45:00'),
(659, 1, 1, 'Registro usuarios id: 13', '::1', '2020-04-01 11:46:00'),
(660, 1, 1, 'Modificar usuarios id: 14', '::1', '2020-04-01 11:46:00'),
(661, 1, 1, 'Eliminar galeria  id: 217', '::1', '2020-04-01 11:48:00'),
(662, 1, 1, 'Registro galeria  id: 218', '::1', '2020-04-01 11:48:00'),
(663, 1, 1, 'Modificar usuarios id: 14', '::1', '2020-04-01 11:49:00'),
(664, 1, 1, 'Cerrar de sesion', '::1', '2020-04-01 12:05:00'),
(665, 1, 1, 'Inicio de sesion', '::1', '2020-04-01 12:06:00'),
(666, 1, 1, 'Modificar usuarios id: 1', '::1', '2020-04-01 12:08:00'),
(667, 1, 1, 'Cerrar de sesion', '::1', '2020-04-01 12:09:00'),
(668, 1, 1, 'Inicio de sesion', '::1', '2020-04-01 12:09:00'),
(669, 1, 1, 'Inicio de sesion', '::1', '2020-04-01 19:37:00'),
(670, 1, 1, 'Cerrar de sesion', '::1', '2020-04-01 19:38:00'),
(671, 1, 1, 'Inicio de sesion', '::1', '2020-04-01 20:26:00'),
(672, 1, 1, 'Cerrar de sesion', '::1', '2020-04-01 20:47:00'),
(673, 1, 1, 'Inicio de sesion', '::1', '2020-04-20 15:24:00'),
(674, 1, 1, 'Inicio de sesion', '::1', '2020-04-21 18:12:00'),
(675, 1, 1, 'Registro categtorias cms id: 26', '::1', '2020-04-21 18:12:00'),
(676, 1, 1, 'Registro galeria  id: 219', '::1', '2020-04-21 18:14:00'),
(677, 1, 1, 'Registro galeria  id: 220', '::1', '2020-04-21 18:14:00'),
(678, 1, 1, 'Registro galeria  id: 221', '::1', '2020-04-21 18:14:00'),
(679, 1, 1, 'Registro galeria  id: 222', '::1', '2020-04-21 18:15:00'),
(680, 1, 1, 'Registro galeria  id: 223', '::1', '2020-04-21 18:17:00'),
(681, 1, 1, 'Inicio de sesion', '::1', '2020-04-23 10:38:00'),
(682, 1, 1, 'Registro noticia id: 15 titulo: EJEMPLO', '::1', '2020-04-23 10:47:00'),
(683, 1, 1, 'Inactivar producto: 6', '::1', '2020-04-23 11:30:00'),
(684, 1, 1, 'Eliminar producto: 6', '::1', '2020-04-23 11:30:00'),
(685, 1, 1, 'Actualizar noticia id: 2 titulo: FIANZA', '::1', '2020-04-23 11:30:00'),
(686, 1, 1, 'Registro noticia id: 15 titulo: TIPO', '::1', '2020-04-23 11:31:00'),
(687, 1, 1, 'Inactivar producto: 38', '::1', '2020-04-23 11:31:00'),
(688, 1, 1, 'Eliminar producto: 38', '::1', '2020-04-23 11:31:00'),
(689, 1, 1, 'Inicio de sesion', '::1', '2020-04-23 11:40:00'),
(690, 1, 1, 'Actualizar noticia id: 2 titulo: FIANZA', '::1', '2020-04-23 11:40:00'),
(691, 1, 1, 'Inactivar producto: 2', '::1', '2020-04-23 11:40:00'),
(692, 1, 1, 'Activar producto: 2', '::1', '2020-04-23 11:40:00'),
(693, 1, 1, 'Actualizar noticia id: 2 titulo: FIANZA', '::1', '2020-04-23 11:40:00'),
(694, 1, 1, 'Registro noticia id: 15 titulo: AUTO', '::1', '2020-04-23 11:41:00'),
(695, 1, 1, 'Inactivar producto: 39', '::1', '2020-04-23 11:41:00'),
(696, 1, 1, 'Activar producto: 39', '::1', '2020-04-23 11:41:00'),
(697, 1, 1, 'Eliminar producto: 39', '::1', '2020-04-23 11:41:00'),
(698, 1, 1, 'Registro noticia id: 15 titulo: PRUEBAS', '::1', '2020-04-23 11:46:00'),
(699, 1, 1, 'Inactivar producto: 125', '::1', '2020-04-23 11:47:00'),
(700, 1, 1, 'Eliminar producto: 125', '::1', '2020-04-23 11:48:00'),
(701, 1, 1, 'Registro noticia id: 15 titulo: UNI AUTO DE EJEMPLO', '::1', '2020-04-23 11:52:00'),
(702, 1, 1, 'Actualizar noticia id: 40 titulo: UNI AUTO DE EJEM', '::1', '2020-04-23 11:53:00'),
(703, 1, 1, 'Eliminar producto: 40', '::1', '2020-04-23 11:53:00'),
(704, 1, 1, 'Registro noticia id: 15 titulo: UNI AUTO DE EJMPLO', '::1', '2020-04-23 11:53:00'),
(705, 1, 1, 'Actualizar noticia id: 7 titulo: UNI AUTO DE EJMP', '::1', '2020-04-23 11:54:00'),
(706, 1, 1, 'Eliminar producto: 7', '::1', '2020-04-23 11:54:00'),
(707, 1, 1, 'Registro noticia id: 15 titulo: PRUEBA', '::1', '2020-04-23 11:56:00'),
(708, 1, 1, 'Inactivar producto: 126', '::1', '2020-04-23 11:56:00'),
(709, 1, 1, 'Eliminar producto: 126', '::1', '2020-04-23 11:56:00'),
(710, 1, 1, 'Registro noticia id: 15 titulo: FIANZA DE LICITACIÓN', '::1', '2020-04-23 12:21:00'),
(711, 1, 1, 'Cerrar de sesion', '::1', '2020-04-23 16:10:00'),
(712, 1, 1, 'Inicio de sesion', '::1', '2020-04-23 16:12:00'),
(713, 1, 1, 'Registro noticia id: 15 titulo: DESCRIPCIÓN', '::1', '2020-04-23 16:13:00'),
(714, 1, 1, 'Registro noticia id: 15 titulo: VIGENCIA', '::1', '2020-04-23 16:13:00'),
(715, 1, 1, 'Registro noticia id: 15 titulo: DECRETO DE OBRAS', '::1', '2020-04-23 16:14:00'),
(716, 1, 1, 'Registro noticia id: 15 titulo: ANTICIPOS DE LAS FIANZAS DE SUMINISTRO', '::1', '2020-04-23 16:15:00'),
(717, 1, 1, 'Registro noticia id: 15 titulo: ANTICIPOS ESPECIALES', '::1', '2020-04-23 16:15:00'),
(718, 1, 1, 'Registro noticia id: 15 titulo: REQUISITOS', '::1', '2020-04-23 16:16:00'),
(719, 1, 1, 'Inicio de sesion', '::1', '2020-04-23 22:03:00'),
(720, 1, 1, 'Registro noticia id: 15 titulo: ¿QUÉ HACER EN CASO DE SINIESTROS?', '::1', '2020-04-23 22:08:00'),
(721, 1, 1, 'Inactivar producto: 1', '::1', '2020-04-23 22:08:00'),
(722, 1, 1, 'Actualizar noticia id: 1 titulo: ¿QUÉ HACER EN CASO DE SINIESTROS?', '::1', '2020-04-23 22:08:00'),
(723, 1, 1, 'Eliminar producto: 1', '::1', '2020-04-23 22:08:00'),
(724, 1, 1, 'Registro noticia id: 15 titulo: ¿QUÉ HACER EN CASO DE SINIESTROS?', '::1', '2020-04-23 22:09:00'),
(725, 1, 1, 'Registro noticia id: 15 titulo: UNIAUTO', '::1', '2020-04-23 22:10:00'),
(726, 1, 1, 'Inactivar servicios: 1', '::1', '2020-04-23 22:10:00'),
(727, 1, 1, 'Eliminar servicios: 1', '::1', '2020-04-23 22:11:00'),
(728, 1, 1, 'Registro noticia id: 15 titulo: UNIAUTO', '::1', '2020-04-23 22:11:00'),
(729, 1, 1, 'Registro noticia id: 15 titulo: PASOS A SEGUIR EN CASO DE UN SINIESTRO:', '::1', '2020-04-23 22:13:00'),
(730, 1, 1, 'Inactivar producto: 1', '::1', '2020-04-23 22:13:00'),
(731, 1, 1, 'Activar producto: 1', '::1', '2020-04-23 22:13:00'),
(732, 1, 1, 'Eliminar producto: 1', '::1', '2020-04-23 22:13:00'),
(733, 1, 1, 'Inicio de sesion', '::1', '2020-04-24 11:23:00'),
(734, 1, 1, 'Registro noticia id: 15 titulo: DESCRIPCIÓN', '::1', '2020-04-24 11:32:00'),
(735, 1, 1, 'Registro noticia id: 15 titulo: COBERTURA ADICIONAL', '::1', '2020-04-24 11:33:00'),
(736, 1, 1, 'Actualizar noticia id: 64 titulo: COBERTURA BÁSICA', '::1', '2020-04-24 11:34:00'),
(737, 1, 1, 'Registro noticia id: 15 titulo: COBERTURA ADICIONAL', '::1', '2020-04-24 11:34:00'),
(738, 1, 1, 'Registro noticia id: 15 titulo: PERSONAS ASEGURABLES', '::1', '2020-04-24 11:35:00'),
(739, 1, 1, 'Registro noticia id: 15 titulo: DESCRIPCIÓN GENERAL', '::1', '2020-04-24 11:36:00'),
(740, 1, 1, 'Registro noticia id: 15 titulo: UNIAPOYO', '::1', '2020-04-24 11:36:00'),
(741, 1, 1, 'Registro noticia id: 15 titulo: DESCRIPCIÓN GENERAL', '::1', '2020-04-24 11:37:00'),
(742, 1, 1, 'Registro noticia id: 15 titulo: PERSONAS ASEGURABLES', '::1', '2020-04-24 11:37:00'),
(743, 1, 1, 'Registro noticia id: 15 titulo: CONDICIONES DE ASEGURABILIDAD', '::1', '2020-04-24 11:38:00'),
(744, 1, 1, 'Registro noticia id: 15 titulo: DESCRIPCIÓN GENERAL', '::1', '2020-04-24 11:39:00'),
(745, 1, 1, 'Registro noticia id: 15 titulo: COBERTURAS ADICIONALES', '::1', '2020-04-24 11:39:00'),
(746, 1, 1, 'Registro noticia id: 15 titulo: DESCRIPCIÓN', '::1', '2020-04-24 12:05:00'),
(747, 1, 1, 'Registro noticia id: 15 titulo: DESCRIPCIÓN', '::1', '2020-04-24 12:06:00'),
(748, 1, 1, 'Registro noticia id: 15 titulo: DESCRIPCIÓN', '::1', '2020-04-24 12:07:00'),
(749, 1, 1, 'Registro noticia id: 15 titulo: DESCRIPCIÓN', '::1', '2020-04-24 12:07:00'),
(750, 1, 1, 'Inicio de sesion', '::1', '2020-04-24 15:53:00'),
(751, 1, 1, 'Inicio de sesion', '::1', '2020-04-24 16:40:00'),
(752, 1, 1, 'Inicio de sesion', '::1', '2020-04-24 16:50:00'),
(753, 1, 1, 'Inicio de sesion', '::1', '2020-04-24 17:06:00'),
(754, 1, 1, 'Inicio de sesion', '::1', '2020-04-24 17:07:00'),
(755, 1, 1, 'Inicio de sesion', '::1', '2020-04-24 21:50:00'),
(756, 1, 1, 'Registro noticia id: 15 titulo: DESCRIPCIÓN', '::1', '2020-04-24 21:59:00'),
(757, 1, 1, 'Registro noticia id: 15 titulo: DESCRIPCIÓN', '::1', '2020-04-24 22:00:00'),
(758, 1, 1, 'Registro noticia id: 15 titulo: DESCRIPCIÓN', '::1', '2020-04-24 22:00:00'),
(759, 1, 1, 'Registro noticia id: 15 titulo: DESCRIPCIÓN', '::1', '2020-04-24 22:01:00'),
(760, 1, 1, 'Registro noticia id: 15 titulo: COBERTURAS ADICIONALES:', '::1', '2020-04-24 22:02:00'),
(761, 1, 1, 'Registro noticia id: 15 titulo: DESCRIPCIÓN', '::1', '2020-04-24 22:04:00'),
(762, 1, 1, 'Registro noticia id: 15 titulo: COBERTURAS', '::1', '2020-04-24 22:05:00'),
(763, 1, 1, 'Registro noticia id: 15 titulo: DESCRIPCIÓN', '::1', '2020-04-24 22:13:00'),
(764, 1, 1, 'Registro noticia id: 15 titulo: COBERTUARS ADICIONALES', '::1', '2020-04-24 22:13:00'),
(765, 1, 1, 'Registro noticia id: 15 titulo: DESCRIPCIÓN', '::1', '2020-04-24 22:14:00'),
(766, 1, 1, 'Registro noticia id: 15 titulo: COBERTURAS ADICIONALES', '::1', '2020-04-24 22:15:00'),
(767, 1, 1, 'Registro noticia id: 15 titulo: DESCRIPCIÓN', '::1', '2020-04-24 22:16:00'),
(768, 1, 1, 'Registro noticia id: 15 titulo: RAMOS TÉCNICOS DE INGENIERÍA', '::1', '2020-04-24 22:17:00'),
(769, 1, 1, 'Registro noticia id: 15 titulo: DESCRIPCIÓN', '::1', '2020-04-24 22:17:00'),
(770, 1, 1, 'Registro noticia id: 15 titulo: SEGURO DE MONTAJE', '::1', '2020-04-24 22:18:00'),
(771, 1, 1, 'Registro noticia id: 15 titulo: SEGURO DE TODO RIESGO DE CONSTRUCCIÓN', '::1', '2020-04-24 22:19:00'),
(772, 1, 1, 'Registro noticia id: 15 titulo: DESCRIPCIÓN', '::1', '2020-04-24 22:26:00'),
(773, 1, 1, 'Registro noticia id: 15 titulo: PASOS A SEGUIR EN CASO DE UN SINIESTRO', '::1', '2020-04-24 22:28:00'),
(774, 1, 1, 'Registro noticia id: 15 titulo: RECAUDOS EN CASO DE UN SINIESTRO', '::1', '2020-04-24 22:31:00'),
(775, 1, 1, 'Registro noticia id: 15 titulo: PERSONAS', '::1', '2020-04-24 22:31:00'),
(776, 1, 1, 'Registro noticia id: 15 titulo: PASOS A SEGUIR EN CASO DE UN SINIESTRO', '::1', '2020-04-24 22:32:00'),
(777, 1, 1, 'Registro noticia id: 15 titulo: RECAUDOS EN CASO DE SINIESTRO', '::1', '2020-04-24 22:32:00'),
(778, 1, 1, 'Registro noticia id: 15 titulo: PATRIMONIALES', '::1', '2020-04-24 22:33:00'),
(779, 1, 1, 'Registro noticia id: 15 titulo: PASOS A SEGUIR EN CASO DE UN SINIESTRO', '::1', '2020-04-24 22:36:00'),
(780, 1, 1, 'Registro noticia id: 15 titulo: RECAUDOS EN CASO DE SINIESTROS', '::1', '2020-04-24 22:36:00'),
(781, 1, 1, 'Eliminar producto: 2', '::1', '2020-04-24 22:37:00'),
(782, 1, 1, 'Inicio de sesion', '::1', '2020-04-26 14:20:00'),
(783, 1, 1, 'Actualizar noticia id: 2 titulo: FIANZA', '::1', '2020-04-26 14:24:00'),
(784, 1, 1, 'Actualizar noticia id: 3 titulo: UNIAUTO', '::1', '2020-04-26 14:24:00'),
(785, 1, 1, 'Actualizar noticia id: 2 titulo: FIANZA', '::1', '2020-04-26 14:24:00'),
(786, 1, 1, 'Actualizar noticia id: 4 titulo: PERSONAS', '::1', '2020-04-26 14:24:00'),
(787, 1, 1, 'Actualizar noticia id: 2 titulo: FIANZA', '::1', '2020-04-26 14:25:00'),
(788, 1, 1, 'Actualizar noticia id: 2 titulo: FIANZA', '::1', '2020-04-26 14:25:00'),
(789, 1, 1, 'Actualizar noticia id: 2 titulo: FIANZA', '::1', '2020-04-26 14:30:00'),
(790, 1, 1, 'Actualizar noticia id: 2 titulo: FIANZA', '::1', '2020-04-26 14:30:00'),
(791, 1, 1, 'Actualizar noticia id: 2 titulo: FIANZA', '::1', '2020-04-26 14:30:00'),
(792, 1, 1, 'Actualizar noticia id: 2 titulo: FIANZA', '::1', '2020-04-26 14:31:00'),
(793, 1, 1, 'Actualizacion de slider id: 1', '::1', '2020-04-26 14:32:00'),
(794, 1, 1, 'Actualizacion de slider id: 1', '::1', '2020-04-26 14:32:00'),
(795, 1, 1, 'Actualizacion de slider id: 3', '::1', '2020-04-26 14:33:00'),
(796, 1, 1, 'Actualizacion de slider id: 1', '::1', '2020-04-26 14:33:00'),
(797, 1, 1, 'Actualizacion de slider id: 2', '::1', '2020-04-26 14:33:00'),
(798, 1, 1, 'Actualizar noticia id: 2 titulo: FIANZA', '::1', '2020-04-26 14:38:00'),
(799, 1, 1, 'Actualizar noticia id: 2 titulo: FIANZA', '::1', '2020-04-26 14:38:00'),
(800, 1, 1, 'Actualizar noticia id: 2 titulo: FIANZA', '::1', '2020-04-26 14:38:00'),
(801, 1, 1, 'Actualizar noticia id: 2 titulo: FIANZA', '::1', '2020-04-26 14:40:00'),
(802, 1, 1, 'Actualizar noticia id: 2 titulo: FIANZA', '::1', '2020-04-26 14:43:00'),
(803, 1, 1, 'Actualizar noticia id: 2 titulo: FIANZA', '::1', '2020-04-26 14:43:00'),
(804, 1, 1, 'Actualizar noticia id: 2 titulo: FIANZA', '::1', '2020-04-26 14:44:00'),
(805, 1, 1, 'Actualizar noticia id: 2 titulo: FIANZA', '::1', '2020-04-26 14:44:00'),
(806, 1, 1, 'Actualizar noticia id: 38 titulo: FIANZA DE LICITACIÓN', '::1', '2020-04-26 18:08:00'),
(807, 1, 1, 'Actualizar noticia id: 38 titulo: FIANZA DE LICITACIÓN', '::1', '2020-04-26 18:09:00'),
(808, 1, 1, 'Actualizar noticia id: 9 titulo: PÓLIZA DE VIDA', '::1', '2020-04-26 18:11:00'),
(809, 1, 1, 'Actualizar noticia id: 8 titulo: ACCIDENTES PERSONALES', '::1', '2020-04-26 18:11:00'),
(810, 1, 1, 'Registro noticia id: 15 titulo: FIANZA DE PRUEBAS', '::1', '2020-04-26 18:33:00'),
(811, 1, 1, 'Actualizar noticia id: 40 titulo: FIANZA DE PRUEBAS', '::1', '2020-04-26 18:34:00'),
(812, 1, 1, 'Actualizar noticia id: 38 titulo: FIANZA DE LICITACIÓN', '::1', '2020-04-26 19:04:00'),
(813, 1, 1, 'Actualizar noticia id: 38 titulo: FIANZA DE LICITACIÓN', '::1', '2020-04-26 19:05:00'),
(814, 1, 1, 'Actualizar noticia id: 8 titulo: ACCIDENTES PERSONALES', '::1', '2020-04-26 19:09:00'),
(815, 1, 1, 'Actualizar noticia id: 8 titulo: ACCIDENTES PERSONALES', '::1', '2020-04-26 19:10:00'),
(816, 1, 1, 'Inicio de sesion', '::1', '2020-04-26 21:10:00'),
(817, 1, 1, 'Actualizar noticia id: 9 titulo: PÓLIZA DE VIDA', '::1', '2020-04-26 21:10:00'),
(818, 1, 1, 'Actualizar noticia id: 9 titulo: PÓLIZA DE VIDA', '::1', '2020-04-26 21:11:00'),
(819, 1, 1, 'Registro noticia id: 15 titulo: PRODUCTO NUEVO', '::1', '2020-04-26 21:36:00'),
(820, 1, 1, 'Actualizar noticia id: 6 titulo: PRODUCTO NUEVO', '::1', '2020-04-26 21:36:00'),
(821, 1, 1, 'Eliminar producto: 6', '::1', '2020-04-26 21:37:00'),
(822, 1, 1, 'Inactivar producto: 2', '::1', '2020-04-26 21:45:00'),
(823, 1, 1, 'Activar producto: 2', '::1', '2020-04-26 21:45:00'),
(824, 1, 1, 'Inicio de sesion', '::1', '2020-04-27 08:49:00'),
(825, 1, 1, 'Inicio de sesion', '::1', '2020-04-28 10:04:00'),
(826, 1, 1, 'Inicio de sesion', '::1', '2020-04-28 13:39:00'),
(827, 1, 1, 'Actualizacion de directiva id: 6', '::1', '2020-04-28 13:45:00'),
(828, 1, 1, 'Actualizacion de directiva id: 6', '::1', '2020-04-28 13:45:00'),
(829, 1, 1, 'Actualizacion de directiva id: 5', '::1', '2020-04-28 13:45:00'),
(830, 1, 1, 'Actualizacion de directiva id: 6', '::1', '2020-04-28 13:46:00'),
(831, 1, 1, 'Actualizacion de directiva id: 6', '::1', '2020-04-28 13:46:00'),
(832, 1, 1, 'Actualizacion de directiva id: 6', '::1', '2020-04-28 13:46:00'),
(833, 1, 1, 'Actualizacion de reaseguradoras id: 3', '::1', '2020-04-28 13:49:00'),
(834, 1, 1, 'Actualizacion de reaseguradoras id: 3', '::1', '2020-04-28 13:49:00'),
(835, 1, 1, 'Actualizacion de reaseguradoras id: 1', '::1', '2020-04-28 13:49:00'),
(836, 1, 1, 'Actualizacion de reaseguradoras id: 1', '::1', '2020-04-28 13:49:00'),
(837, 1, 1, 'Actualizacion de reaseguradoras id: 1', '::1', '2020-04-28 13:50:00'),
(838, 1, 1, 'Actualizacion de reaseguradoras id: 3', '::1', '2020-04-28 13:50:00'),
(839, 1, 1, 'Actualizacion de reaseguradoras id: 3', '::1', '2020-04-28 13:50:00'),
(840, 1, 1, 'Registro de reaseguradoras id:9,titulo:prueba', '::1', '2020-04-28 13:52:00'),
(841, 1, 1, 'Eliminar reaseguradoras id: 9', '::1', '2020-04-28 13:52:00'),
(842, 1, 1, 'Registro de slider id:4,titulo:prueba', '::1', '2020-04-28 13:55:00'),
(843, 1, 1, 'Eliminar slider id: 4', '::1', '2020-04-28 13:55:00'),
(844, 1, 1, 'Registro de direccion id:19,titulo:prueba de direccion', '::1', '2020-04-28 14:02:00'),
(845, 1, 1, 'Eliminar direccion id: 19', '::1', '2020-04-28 14:02:00'),
(846, 1, 1, 'Eliminar direccion id: 19', '::1', '2020-04-28 14:02:00'),
(847, 1, 1, 'Eliminar direccion id: 19', '::1', '2020-04-28 14:02:00'),
(848, 1, 1, 'Eliminar direccion id: 19', '::1', '2020-04-28 14:03:00'),
(849, 1, 1, 'Cerrar de sesion', '::1', '2020-04-28 14:04:00'),
(850, 1, 1, 'Inicio de sesion', '::1', '2020-04-29 13:05:00'),
(851, 1, 1, 'Actualizar noticia id: 2 titulo: FIANZA', '::1', '2020-04-29 13:06:00'),
(852, 1, 1, 'Actualizar noticia id: 2 titulo: FIANZA', '::1', '2020-04-29 13:06:00'),
(853, 1, 1, 'Actualizar noticia id: 38 titulo: FIANZA DE LICITACIÓN', '::1', '2020-04-29 13:06:00'),
(854, 1, 1, 'Actualizar noticia id: 38 titulo: FIANZA DE LICITACIÓN', '::1', '2020-04-29 13:06:00'),
(855, 1, 1, 'Actualizar noticia id: 63 titulo: DESCRIPCIÓN', '::1', '2020-04-29 13:07:00'),
(856, 1, 1, 'Actualizar noticia id: 63 titulo: DESCRIPCIÓN', '::1', '2020-04-29 13:07:00'),
(857, 1, 1, 'Inicio de sesion', '::1', '2020-04-29 14:26:00'),
(858, 1, 1, 'Actualizar noticia id: 3 titulo: REQUISITOS', '::1', '2020-04-29 14:35:00'),
(859, 1, 1, 'Actualizar noticia id: 3 titulo: REQUISITOS', '::1', '2020-04-29 14:36:00'),
(860, 1, 1, 'Actualizar noticia id: 3 titulo: REQUISITOS', '::1', '2020-04-29 14:36:00'),
(861, 1, 1, 'Actualizar noticia id: 3 titulo: REQUISITOS', '::1', '2020-04-29 14:36:00'),
(862, 1, 1, 'Registro noticia id: 15 titulo: PRUEBA DE FIANZA DE LICITACIÓN', '::1', '2020-04-29 14:36:00'),
(863, 1, 1, 'Eliminar producto: 94', '::1', '2020-04-29 14:37:00'),
(864, 1, 1, 'Registro noticia id: 15 titulo: PRUEBA DE FIANZA DE LICITACIÓN', '::1', '2020-04-29 14:37:00'),
(865, 1, 1, 'Inactivar producto: 95', '::1', '2020-04-29 14:38:00'),
(866, 1, 1, 'Activar producto: 95', '::1', '2020-04-29 14:38:00'),
(867, 1, 1, 'Eliminar producto: 95', '::1', '2020-04-29 14:38:00'),
(868, 1, 1, 'Registro noticia id: 15 titulo: SERVICIO DE PRUEBA', '::1', '2020-04-29 14:40:00'),
(869, 1, 1, 'Inactivar producto: 3', '::1', '2020-04-29 14:40:00'),
(870, 1, 1, 'Activar producto: 3', '::1', '2020-04-29 14:40:00'),
(871, 1, 1, 'Actualizar noticia id: 2 titulo: ¿QUÉ HACER EN CASO DE SINIESTROS?', '::1', '2020-04-29 14:41:00'),
(872, 1, 1, 'Registro noticia id: 15 titulo: OTRA PRUEBA', '::1', '2020-04-29 14:41:00'),
(873, 1, 1, 'Actualizar noticia id: 3 titulo: SERVICIO DE PRUEBA', '::1', '2020-04-29 14:41:00'),
(874, 1, 1, 'Actualizar noticia id: 3 titulo: SERVICIO DE PRUEBA', '::1', '2020-04-29 14:41:00'),
(875, 1, 1, 'Actualizar noticia id: 4 titulo: OTRA PRUEBA', '::1', '2020-04-29 14:41:00'),
(876, 1, 1, 'Eliminar producto: 4', '::1', '2020-04-29 14:41:00'),
(877, 1, 1, 'Eliminar producto: 3', '::1', '2020-04-29 14:42:00'),
(878, 1, 1, 'Eliminar producto: 4', '::1', '2020-04-29 14:43:00'),
(879, 1, 1, 'Eliminar producto: 3', '::1', '2020-04-29 14:43:00'),
(880, 1, 1, 'Registro noticia id: 15 titulo: TIPO SERVICIO DE PRUEBA', '::1', '2020-04-29 14:48:00'),
(881, 1, 1, 'Registro noticia id: 15 titulo: SERVICIO DE PRUEBA 2', '::1', '2020-04-29 14:49:00'),
(882, 1, 1, 'Inactivar servicios: 3', '::1', '2020-04-29 14:49:00'),
(883, 1, 1, 'Activar servicios: 3', '::1', '2020-04-29 14:49:00'),
(884, 1, 1, 'Eliminar servicios: 6', '::1', '2020-04-29 14:49:00'),
(885, 1, 1, 'Actualizar noticia id: 4 titulo: PATRIMONIALES', '::1', '2020-04-29 14:55:00'),
(886, 1, 1, 'Actualizar noticia id: 4 titulo: PATRIMONIALES', '::1', '2020-04-29 14:55:00'),
(887, 1, 1, 'Registro noticia id: 15 titulo: PRUEBA', '::1', '2020-04-29 14:58:00'),
(888, 1, 1, 'Registro noticia id: 15 titulo: PRUEBA 2', '::1', '2020-04-29 14:59:00'),
(889, 1, 1, 'Inactivar producto: 8', '::1', '2020-04-29 15:04:00'),
(890, 1, 1, 'Activar producto: 8', '::1', '2020-04-29 15:04:00'),
(891, 1, 1, 'Registro noticia id: 15 titulo: SERVICIO DE PRUEBA 3', '::1', '2020-04-29 15:05:00'),
(892, 1, 1, 'Actualizar noticia id: 10 titulo: SERVICIO DE PRUEBA 3', '::1', '2020-04-29 15:05:00'),
(893, 1, 1, 'Actualizar noticia id: 10 titulo: SERVICIO DE PRUEBA 3', '::1', '2020-04-29 15:05:00'),
(894, 1, 1, 'Actualizar noticia id: 10 titulo: SERVICIO DE PRUEBA 3', '::1', '2020-04-29 15:05:00'),
(895, 1, 1, 'Eliminar producto: 10', '::1', '2020-04-29 15:06:00'),
(896, 1, 1, 'Eliminar producto: 9', '::1', '2020-04-29 15:06:00'),
(897, 1, 1, 'Eliminar producto: 8', '::1', '2020-04-29 15:06:00'),
(898, 1, 1, 'Eliminar producto: 10', '::1', '2020-04-29 15:18:00'),
(899, 1, 1, 'Registro noticia id: 15 titulo: PRUEBA 2', '::1', '2020-04-29 15:19:00'),
(900, 1, 1, 'Inactivar producto: 11', '::1', '2020-04-29 15:19:00'),
(901, 1, 1, 'Activar producto: 11', '::1', '2020-04-29 15:19:00'),
(902, 1, 1, 'Eliminar producto: 11', '::1', '2020-04-29 15:19:00'),
(903, 1, 1, 'Registro categtorias cms id: 27', '::1', '2020-04-29 15:30:00'),
(904, 1, 1, 'Registro categtorias cms id: 28', '::1', '2020-04-29 15:30:00'),
(905, 1, 1, 'Registro galeria  id: 224', '::1', '2020-04-29 15:30:00'),
(906, 1, 1, 'Registro galeria  id: 225', '::1', '2020-04-29 15:31:00'),
(907, 1, 1, 'Registro galeria  id: 226', '::1', '2020-04-29 15:31:00'),
(908, 1, 1, 'Registro galeria  id: 227', '::1', '2020-04-29 15:32:00'),
(909, 1, 1, 'Registro galeria  id: 228', '::1', '2020-04-29 15:32:00'),
(910, 1, 1, 'Registro galeria  id: 229', '::1', '2020-04-29 15:32:00'),
(911, 1, 1, 'Registro galeria  id: 230', '::1', '2020-04-29 15:33:00'),
(912, 1, 1, 'Actualizar noticia id: 2 titulo: FIANZA', '::1', '2020-04-29 15:43:00'),
(913, 1, 1, 'Actualizar noticia id: 38 titulo: FIANZA DE LICITACIÓN', '::1', '2020-04-29 15:44:00'),
(914, 1, 1, 'Actualizar noticia id: 2 titulo: FIANZA DE FIEL CUMPLIMIENTO', '::1', '2020-04-29 15:44:00'),
(915, 1, 1, 'Actualizar noticia id: 3 titulo: FIANZA DE BUENA CALIDAD', '::1', '2020-04-29 15:45:00'),
(916, 1, 1, 'Actualizar noticia id: 1 titulo: FIANZA DE ANTICIPO', '::1', '2020-04-29 15:45:00'),
(917, 1, 1, 'Actualizar noticia id: 5 titulo: FIANZA LABORAL', '::1', '2020-04-29 15:45:00'),
(918, 1, 1, 'Actualizar noticia id: 6 titulo: FIANZA ADUANAL', '::1', '2020-04-29 15:45:00'),
(919, 1, 1, 'Registro galeria  id: 231', '::1', '2020-04-29 15:48:00'),
(920, 1, 1, 'Registro galeria  id: 232', '::1', '2020-04-29 15:49:00'),
(921, 1, 1, 'Registro galeria  id: 233', '::1', '2020-04-29 15:49:00'),
(922, 1, 1, 'Registro galeria  id: 234', '::1', '2020-04-29 15:49:00'),
(923, 1, 1, 'Registro galeria  id: 235', '::1', '2020-04-29 15:49:00'),
(924, 1, 1, 'Registro galeria  id: 236', '::1', '2020-04-29 15:50:00'),
(925, 1, 1, 'Registro galeria  id: 237', '::1', '2020-04-29 15:50:00'),
(926, 1, 1, 'Actualizar noticia id: 7 titulo: UNIAUTO INTELIGENTE', '::1', '2020-04-29 15:51:00'),
(927, 1, 1, 'Actualizar noticia id: 8 titulo: ACCIDENTES PERSONALES', '::1', '2020-04-29 15:51:00'),
(928, 1, 1, 'Actualizar noticia id: 9 titulo: PÓLIZA DE VIDA', '::1', '2020-04-29 15:51:00'),
(929, 1, 1, 'Actualizar noticia id: 10 titulo: PÓLIZA FUNERARIA', '::1', '2020-04-29 15:52:00'),
(930, 1, 1, 'Eliminar galeria  id: 235', '::1', '2020-04-29 15:52:00'),
(931, 1, 1, 'Actualizar noticia id: 9 titulo: PÓLIZA DE VIDA', '::1', '2020-04-29 15:54:00'),
(932, 1, 1, 'Eliminar galeria  id: 237', '::1', '2020-04-29 15:56:00'),
(933, 1, 1, 'Registro galeria  id: 238', '::1', '2020-04-29 15:58:00'),
(934, 1, 1, 'Actualizar noticia id: 9 titulo: PÓLIZA DE VIDA', '::1', '2020-04-29 15:59:00'),
(935, 1, 1, 'Eliminar galeria  id: 238', '::1', '2020-04-29 15:59:00'),
(936, 1, 1, 'Registro galeria  id: 239', '::1', '2020-04-29 15:59:00'),
(937, 1, 1, 'Actualizar noticia id: 9 titulo: PÓLIZA DE VIDA', '::1', '2020-04-29 16:00:00'),
(938, 1, 1, 'Inicio de sesion', '::1', '2020-04-30 09:41:00'),
(939, 1, 1, 'Registro galeria  id: 240', '::1', '2020-04-30 09:41:00'),
(940, 1, 1, 'Registro galeria  id: 241', '::1', '2020-04-30 09:41:00'),
(941, 1, 1, 'Registro galeria  id: 242', '::1', '2020-04-30 09:42:00'),
(942, 1, 1, 'Registro galeria  id: 243', '::1', '2020-04-30 09:42:00'),
(943, 1, 1, 'Registro galeria  id: 244', '::1', '2020-04-30 09:42:00'),
(944, 1, 1, 'Registro galeria  id: 245', '::1', '2020-04-30 09:42:00'),
(945, 1, 1, 'Registro galeria  id: 246', '::1', '2020-04-30 09:43:00'),
(946, 1, 1, 'Actualizar noticia id: 12 titulo: INCENDIO', '::1', '2020-04-30 09:44:00'),
(947, 1, 1, 'Actualizar noticia id: 13 titulo: LUCRO CESANTE', '::1', '2020-04-30 09:44:00'),
(948, 1, 1, 'Actualizar noticia id: 14 titulo: ROBO', '::1', '2020-04-30 09:44:00'),
(949, 1, 1, 'Actualizar noticia id: 15 titulo: DINERO Y/O VALORES', '::1', '2020-04-30 09:44:00'),
(950, 1, 1, 'Actualizar noticia id: 16 titulo: RIESGOS ESPECIALES', '::1', '2020-04-30 09:45:00'),
(951, 1, 1, 'Actualizar noticia id: 17 titulo: FILDEILAD 3D', '::1', '2020-04-30 09:45:00'),
(952, 1, 1, 'Actualizar noticia id: 5 titulo: PATRIMONIALES', '::1', '2020-04-30 09:46:00'),
(953, 1, 1, 'Cerrar de sesion', '::1', '2020-04-30 09:46:00'),
(954, 1, 1, 'Inicio de sesion', '::1', '2020-04-30 14:00:00'),
(955, 1, 1, 'Inicio de sesion', '::1', '2020-04-30 19:44:00'),
(956, 1, 1, 'Inicio de sesion', '::1', '2020-05-01 13:42:00'),
(957, 1, 1, 'Inicio de sesion', '::1', '2020-05-03 17:51:00'),
(958, 1, 1, 'Actualizar noticia id: 4 titulo: PERSONAS', '::1', '2020-05-03 18:17:00'),
(959, 1, 1, 'Actualizar noticia id: 3 titulo: UNIAUTO', '::1', '2020-05-03 18:17:00'),
(960, 1, 1, 'Cerrar de sesion', '::1', '2020-05-03 19:50:00'),
(961, 1, 1, 'Inicio de sesion', '::1', '2020-05-03 20:59:00'),
(962, 1, 1, 'Registro galeria  id: 247', '::1', '2020-05-03 21:00:00'),
(963, 1, 1, 'Registro galeria  id: 248', '::1', '2020-05-03 21:00:00'),
(964, 1, 1, 'Registro galeria  id: 249', '::1', '2020-05-03 21:01:00'),
(965, 1, 1, 'Registro galeria  id: 250', '::1', '2020-05-03 21:01:00'),
(966, 1, 1, 'Actualizar noticia id: 2 titulo: FIANZA', '::1', '2020-05-03 21:01:00'),
(967, 1, 1, 'Actualizar noticia id: 3 titulo: UNIAUTO', '::1', '2020-05-03 21:01:00'),
(968, 1, 1, 'Actualizar noticia id: 4 titulo: PERSONAS', '::1', '2020-05-03 21:02:00'),
(969, 1, 1, 'Actualizar noticia id: 5 titulo: PATRIMONIALES', '::1', '2020-05-03 21:02:00'),
(970, 1, 1, 'Eliminar galeria  id: 240', '::1', '2020-05-03 21:03:00'),
(971, 1, 1, 'Eliminar galeria  id: 231', '::1', '2020-05-03 21:03:00'),
(972, 1, 1, 'Eliminar galeria  id: 233', '::1', '2020-05-03 21:03:00'),
(973, 1, 1, 'Eliminar galeria  id: 224', '::1', '2020-05-03 21:04:00'),
(974, 1, 1, 'Eliminar galeria  id: 219', '::1', '2020-05-03 21:04:00'),
(975, 1, 1, 'Eliminar galeria  id: 221', '::1', '2020-05-03 21:05:00'),
(976, 1, 1, 'Eliminar galeria  id: 222', '::1', '2020-05-03 21:05:00'),
(977, 1, 1, 'Registro galeria  id: 251', '::1', '2020-05-03 21:07:00'),
(978, 1, 1, 'Eliminar galeria  id: 251', '::1', '2020-05-03 21:08:00'),
(979, 1, 1, 'Eliminar categorias: 28', '::1', '2020-05-03 21:15:00'),
(980, 1, 1, 'Registro categtorias cms id: 29', '::1', '2020-05-03 21:15:00'),
(981, 1, 1, 'Registro galeria  id: 252', '::1', '2020-05-03 21:17:00'),
(982, 1, 1, 'Registro galeria  id: 253', '::1', '2020-05-03 21:18:00'),
(983, 1, 1, 'Registro galeria  id: 254', '::1', '2020-05-03 21:18:00'),
(984, 1, 1, 'Actualizar noticia id: 2 titulo: UNIAUTO', '::1', '2020-05-03 21:19:00'),
(985, 1, 1, 'Actualizar noticia id: 3 titulo: PERSONAS', '::1', '2020-05-03 21:19:00'),
(986, 1, 1, 'Actualizar noticia id: 4 titulo: PATRIMONIALES', '::1', '2020-05-03 21:19:00'),
(987, 1, 1, 'Registro galeria  id: 255', '::1', '2020-05-03 21:20:00'),
(988, 1, 1, 'Actualizar noticia id: 2 titulo: ¿QUÉ HACER EN CASO DE SINIESTROS?', '::1', '2020-05-03 21:21:00'),
(989, 1, 1, 'Eliminar direccion id: 17', '::1', '2020-05-03 22:03:00'),
(990, 1, 1, 'Eliminar direccion id: 18', '::1', '2020-05-03 22:03:00'),
(991, 1, 1, 'Actualizar noticia id: 77 titulo: DESCRIPCIÓN', '::1', '2020-05-03 22:10:00'),
(992, 1, 1, 'Inicio de sesion', '::1', '2020-05-05 16:39:00'),
(993, 1, 1, 'Inicio de sesion', '::1', '2020-05-07 15:02:00'),
(994, 1, 1, 'Inicio de sesion', '::1', '2020-05-07 15:02:00'),
(995, 1, 1, 'Inicio de sesion', '::1', '2020-05-08 20:30:00'),
(996, 1, 1, 'Registro galeria  id: 256', '::1', '2020-05-08 20:32:00'),
(997, 1, 1, 'Registro galeria  id: 257', '::1', '2020-05-08 20:32:00'),
(998, 1, 1, 'Registro galeria  id: 258', '::1', '2020-05-08 20:32:00'),
(999, 1, 1, 'Registro galeria  id: 259', '::1', '2020-05-08 20:32:00'),
(1000, 1, 1, 'Registro galeria  id: 260', '::1', '2020-05-08 20:33:00'),
(1001, 1, 1, 'Actualizar noticia id: 21 titulo: TRANSPORTE TERRESTRE', '::1', '2020-05-08 20:33:00'),
(1002, 1, 1, 'Actualizar noticia id: 22 titulo: TRANSPORTE MARÍTIMO Y AÉREO', '::1', '2020-05-08 20:34:00'),
(1003, 1, 1, 'Actualizar noticia id: 19 titulo: ROTURA DE MAQUINARIAS', '::1', '2020-05-08 20:34:00'),
(1004, 1, 1, 'Actualizar noticia id: 18 titulo: EQUIPO DE CONTRATISTAS', '::1', '2020-05-08 20:34:00'),
(1005, 1, 1, 'Eliminar galeria  id: 257', '::1', '2020-05-08 20:35:00'),
(1006, 1, 1, 'Registro galeria  id: 261', '::1', '2020-05-08 20:36:00'),
(1007, 1, 1, 'Actualizar noticia id: 20 titulo: EQUIPO ELECTRÓNICO', '::1', '2020-05-08 20:36:00'),
(1008, 1, 1, 'Actualizar noticia id: 20 titulo: EQUIPO ELECTRÓNICO', '::1', '2020-05-08 20:36:00'),
(1009, 1, 1, 'Cerrar de sesion', '::1', '2020-05-08 20:36:00'),
(1010, 1, 1, 'Inicio de sesion', '::1', '2020-05-12 14:11:00'),
(1011, 1, 1, 'Inicio de sesion', '::1', '2020-05-12 14:12:00'),
(1012, 1, 1, 'Actualizar noticia id: 3 titulo: REQUISITOS', '::1', '2020-05-12 14:20:00'),
(1013, 1, 1, 'Actualizar noticia id: 5 titulo: VIGENCIA', '::1', '2020-05-12 14:46:00'),
(1014, 1, 1, 'Actualizar noticia id: 6 titulo: REQUISITOS', '::1', '2020-05-12 14:48:00'),
(1015, 1, 1, 'Actualizar noticia id: 9 titulo: DESCRIPCIÓN', '::1', '2020-05-12 15:03:00'),
(1016, 1, 1, 'Actualizar noticia id: 10 titulo: REQUISITOS', '::1', '2020-05-12 15:08:00'),
(1017, 1, 1, 'Actualizar noticia id: 62 titulo: REQUISITOS', '::1', '2020-05-12 15:11:00'),
(1018, 1, 1, 'Actualizar noticia id: 62 titulo: REQUISITOS', '::1', '2020-05-12 15:11:00'),
(1019, 1, 1, 'Actualizar noticia id: 13 titulo: REQUISITOS', '::1', '2020-05-12 15:15:00'),
(1020, 1, 1, 'Actualizar noticia id: 14 titulo: DESCRIPCIÓN', '::1', '2020-05-12 15:17:00'),
(1021, 1, 1, 'Actualizar noticia id: 14 titulo: DESCRIPCIÓN', '::1', '2020-05-12 15:18:00'),
(1022, 1, 1, 'Actualizar noticia id: 25 titulo: PERFECCIONAMIENTO ACTIVO', '::1', '2020-05-12 15:18:00'),
(1023, 1, 1, 'Actualizar noticia id: 34 titulo: DAÑOS A TERCEROS POR LA LEY DE LA MARINA MERCANTE', '::1', '2020-05-12 15:20:00'),
(1024, 1, 1, 'Actualizar noticia id: 34 titulo: DAÑOS A TERCEROS POR LA LEY DE LA MARINA MERCANTE', '::1', '2020-05-12 15:20:00'),
(1025, 1, 1, 'Actualizar noticia id: 34 titulo: DAÑOS A TERCEROS POR LA LEY DE LA MARINA MERCANTE', '::1', '2020-05-12 15:20:00'),
(1026, 1, 1, 'Actualizar noticia id: 35 titulo: REQUISITOS', '::1', '2020-05-12 15:23:00'),
(1027, 1, 1, 'Actualizar noticia id: 36 titulo: DESCRIPCIÓN', '::1', '2020-05-12 16:33:00'),
(1028, 1, 1, 'Actualizar noticia id: 63 titulo: DESCRIPCIÓN', '::1', '2020-05-12 16:35:00'),
(1029, 1, 1, 'Actualizar noticia id: 67 titulo: DESCRIPCIÓN GENERAL', '::1', '2020-05-12 16:56:00'),
(1030, 1, 1, 'Actualizar noticia id: 69 titulo: DESCRIPCIÓN GENERAL', '::1', '2020-05-12 16:58:00'),
(1031, 1, 1, 'Actualizar noticia id: 70 titulo: PERSONAS ASEGURABLES', '::1', '2020-05-12 17:01:00'),
(1032, 1, 1, 'Actualizar noticia id: 74 titulo: DESCRIPCIÓN', '::1', '2020-05-12 17:22:00'),
(1033, 1, 1, 'Actualizar noticia id: 75 titulo: DESCRIPCIÓN', '::1', '2020-05-12 17:23:00'),
(1034, 1, 1, 'Actualizar noticia id: 76 titulo: DESCRIPCIÓN', '::1', '2020-05-12 17:25:00'),
(1035, 1, 1, 'Actualizar noticia id: 77 titulo: DESCRIPCIÓN', '::1', '2020-05-12 17:25:00'),
(1036, 1, 1, 'Actualizar noticia id: 77 titulo: DESCRIPCIÓN', '::1', '2020-05-12 17:40:00'),
(1037, 1, 1, 'Actualizar noticia id: 77 titulo: DESCRIPCIÓN', '::1', '2020-05-12 17:42:00'),
(1038, 1, 1, 'Actualizar noticia id: 78 titulo: DESCRIPCIÓN', '::1', '2020-05-12 17:49:00'),
(1039, 1, 1, 'Actualizar noticia id: 80 titulo: DESCRIPCIÓN', '::1', '2020-05-12 17:52:00'),
(1040, 1, 1, 'Actualizar noticia id: 93 titulo: DESCRIPCIÓN', '::1', '2020-05-12 17:53:00'),
(1041, 1, 1, 'Actualizar noticia id: 81 titulo: DESCRIPCIÓN', '::1', '2020-05-12 17:56:00'),
(1042, 1, 1, 'Actualizar noticia id: 84 titulo: COBERTURAS', '::1', '2020-05-12 17:57:00'),
(1043, 1, 1, 'Actualizar noticia id: 91 titulo: SEGURO DE MONTAJE', '::1', '2020-05-12 18:01:00'),
(1044, 1, 1, 'Actualizar noticia id: 92 titulo: SEGURO DE TODO RIESGO DE CONSTRUCCIÓN', '::1', '2020-05-12 18:02:00'),
(1045, 1, 1, 'Actualizar noticia id: 91 titulo: SEGURO DE MONTAJE', '::1', '2020-05-12 18:03:00'),
(1046, 1, 1, 'Inactivar producto: 3', '::1', '2020-05-12 18:13:00'),
(1047, 1, 1, 'Actualizar noticia id: 1 titulo: PASOS A SEGUIR EN CASO DE UN SINIESTRO:', '::1', '2020-05-12 18:35:00'),
(1048, 1, 1, 'Actualizar noticia id: 3 titulo: RECAUDOS EN CASO DE UN SINIESTRO', '::1', '2020-05-12 18:36:00'),
(1049, 1, 1, 'Actualizar noticia id: 1 titulo: PASOS A SEGUIR EN CASO DE UN SINIESTRO:', '::1', '2020-05-12 18:38:00'),
(1050, 1, 1, 'Actualizar noticia id: 1 titulo: PASOS A SEGUIR EN CASO DE UN SINIESTRO:', '::1', '2020-05-12 18:38:00'),
(1051, 1, 1, 'Actualizar noticia id: 1 titulo: PASOS A SEGUIR EN CASO DE UN SINIESTRO:', '::1', '2020-05-12 18:38:00'),
(1052, 1, 1, 'Actualizar noticia id: 3 titulo: RECAUDOS EN CASO DE UN SINIESTRO', '::1', '2020-05-12 18:39:00'),
(1053, 1, 1, 'Actualizar noticia id: 3 titulo: RECAUDOS EN CASO DE UN SINIESTRO', '::1', '2020-05-12 18:41:00'),
(1054, 1, 1, 'Actualizar noticia id: 4 titulo: PASOS A SEGUIR EN CASO DE UN SINIESTRO', '::1', '2020-05-12 18:46:00'),
(1055, 1, 1, 'Actualizar noticia id: 5 titulo: RECAUDOS EN CASO DE SINIESTRO', '::1', '2020-05-12 18:47:00'),
(1056, 1, 1, 'Actualizar noticia id: 6 titulo: PASOS A SEGUIR EN CASO DE UN SINIESTRO', '::1', '2020-05-12 18:49:00'),
(1057, 1, 1, 'Actualizar noticia id: 7 titulo: RECAUDOS EN CASO DE SINIESTROS', '::1', '2020-05-12 18:49:00'),
(1058, 1, 1, 'Inicio de sesion', '::1', '2020-05-13 10:31:00'),
(1059, 1, 1, 'Registro noticia id: 15 titulo: ¿QUÉ ES , PRUEBA SLUG?', '::1', '2020-05-13 10:32:00'),
(1060, 1, 1, 'Registro noticia id: 15 titulo: QUÉ ES ESTA PRUEBA DE SLUG', '::1', '2020-05-13 10:58:00'),
(1061, 1, 1, 'Eliminar producto: 97', '::1', '2020-05-13 10:58:00'),
(1062, 1, 1, 'Registro noticia id: 15 titulo: ¿SLUG: QUÉ HACER EN CASO DE SINIESTRÓ?', '::1', '2020-05-13 10:59:00'),
(1063, 1, 1, 'Registro noticia id: 15 titulo: ¿QUÉ HACER EN CASO DE SLUG-?', '::1', '2020-05-13 11:00:00'),
(1064, 1, 1, 'Registro noticia id: 15 titulo: ¿QUÉ HACER EN CASO DE SLUG?', '::1', '2020-05-13 11:07:00'),
(1065, 1, 1, 'Registro noticia id: 15 titulo: ¿QUÉ HACER EN CASO DE SLUG?', '::1', '2020-05-13 11:08:00'),
(1066, 1, 1, 'Registro noticia id: 15 titulo: ¿QUÉ HACER EN CASO DE SLUG:?', '::1', '2020-05-13 11:09:00'),
(1067, 1, 1, 'Inicio de sesion', '::1', '2020-05-13 13:57:00'),
(1068, 1, 1, 'Registro galeria  id: 262', '::1', '2020-05-13 13:58:00'),
(1069, 1, 1, 'Registro galeria  id: 263', '::1', '2020-05-13 13:59:00'),
(1070, 1, 1, 'Registro galeria  id: 264', '::1', '2020-05-13 14:00:00'),
(1071, 1, 1, 'Registro galeria  id: 265', '::1', '2020-05-13 14:00:00'),
(1072, 1, 1, 'Registro galeria  id: 266', '::1', '2020-05-13 14:01:00'),
(1073, 1, 1, 'Actualizar noticia id: 23 titulo: EMBARCACIONES', '::1', '2020-05-13 14:01:00'),
(1074, 1, 1, 'Actualizar noticia id: 36 titulo: AVIACIÓN', '::1', '2020-05-13 14:01:00'),
(1075, 1, 1, 'Actualizar noticia id: 37 titulo: RESPONSABILIDAD CIVIL GENERALES', '::1', '2020-05-13 14:02:00'),
(1076, 1, 1, 'Actualizar noticia id: 39 titulo: RAMOS TÉCNICOS DE INGENIERÍA', '::1', '2020-05-13 14:02:00'),
(1077, 1, 1, 'Actualizar noticia id: 2 titulo: ¿QUÉ HACER EN CASO DE SINIESTROS?', '::1', '2020-05-13 14:02:00'),
(1078, 1, 1, 'Cerrar de sesion', '::1', '2020-05-13 14:03:00'),
(1079, 1, 1, 'Inicio de sesion', '::1', '2020-05-13 14:09:00'),
(1080, 1, 1, 'Actualizar noticia id: 85 titulo: DESCRIPCIÓN', '::1', '2020-05-13 14:10:00'),
(1081, 1, 1, 'Inicio de sesion', '::1', '2020-05-13 14:56:00'),
(1082, 1, 1, 'Actualizar noticia id: 3 titulo: RECAUDOS EN CASO DE UN SINIESTRO', '::1', '2020-05-13 14:59:00'),
(1083, 1, 1, 'Actualizar noticia id: 4 titulo: PASOS A SEGUIR EN CASO DE UN SINIESTRO', '::1', '2020-05-13 14:59:00'),
(1084, 1, 1, 'Actualizar noticia id: 6 titulo: PASOS A SEGUIR EN CASO DE UN SINIESTRO', '::1', '2020-05-13 14:59:00'),
(1085, 1, 1, 'Actualizar noticia id: 7 titulo: RECAUDOS EN CASO DE SINIESTROS', '::1', '2020-05-13 15:00:00'),
(1086, 1, 1, 'Actualizar noticia id: 6 titulo: PASOS A SEGUIR EN CASO DE UN SINIESTRO', '::1', '2020-05-13 15:00:00'),
(1087, 1, 1, 'Actualizar noticia id: 2 titulo: UNIAUTO', '::1', '2020-05-13 15:00:00'),
(1088, 1, 1, 'Actualizar noticia id: 4 titulo: PATRIMONIALES', '::1', '2020-05-13 15:01:00'),
(1089, 1, 1, 'Actualizar noticia id: 3 titulo: PERSONAS', '::1', '2020-05-13 15:01:00'),
(1090, 1, 1, 'Eliminar servicios: 5', '::1', '2020-05-13 15:01:00'),
(1091, 1, 1, 'Actualizar noticia id: 2 titulo: ¿QUÉ HACER EN CASO DE SINIESTROS?', '::1', '2020-05-13 15:01:00'),
(1092, 1, 1, 'Eliminar producto: 3', '::1', '2020-05-13 15:01:00'),
(1093, 1, 1, 'Actualizar noticia id: 2 titulo: FIANZA', '::1', '2020-05-13 15:01:00'),
(1094, 1, 1, 'Actualizar noticia id: 3 titulo: UNIAUTO', '::1', '2020-05-13 15:02:00'),
(1095, 1, 1, 'Actualizar noticia id: 4 titulo: PERSONAS', '::1', '2020-05-13 15:02:00'),
(1096, 1, 1, 'Actualizar noticia id: 5 titulo: PATRIMONIALES', '::1', '2020-05-13 15:02:00'),
(1097, 1, 1, 'Actualizar noticia id: 38 titulo: FIANZA DE LICITACIÓN', '::1', '2020-05-13 15:03:00'),
(1098, 1, 1, 'Actualizar noticia id: 2 titulo: FIANZA DE FIEL CUMPLIMIENTO', '::1', '2020-05-13 15:03:00'),
(1099, 1, 1, 'Actualizar noticia id: 3 titulo: FIANZA DE BUENA CALIDAD', '::1', '2020-05-13 15:03:00'),
(1100, 1, 1, 'Actualizar noticia id: 1 titulo: FIANZA DE ANTICIPO', '::1', '2020-05-13 15:03:00'),
(1101, 1, 1, 'Actualizar noticia id: 5 titulo: FIANZA LABORAL', '::1', '2020-05-13 15:03:00'),
(1102, 1, 1, 'Actualizar noticia id: 6 titulo: FIANZA ADUANAL', '::1', '2020-05-13 15:04:00'),
(1103, 1, 1, 'Cerrar de sesion', '::1', '2020-05-13 15:09:00'),
(1104, 1, 1, 'Inicio de sesion', '::1', '2020-05-13 15:09:00'),
(1105, 1, 1, 'Cerrar de sesion', '::1', '2020-05-13 15:09:00'),
(1106, 1, 1, 'Inicio de sesion', '::1', '2020-05-14 15:02:00'),
(1107, 1, 1, 'Inicio de sesion', '::1', '2020-05-14 15:37:00'),
(1108, 1, 1, 'Actualizar noticia id: 2 titulo: FIANZAS', '::1', '2020-05-14 15:37:00'),
(1109, 1, 1, 'Actualizar noticia id: 38 titulo: FIANZA DE MANTENIMIENTO DE LA OFERTA', '::1', '2020-05-14 15:38:00'),
(1110, 1, 1, 'Actualizar noticia id: 11 titulo: DESCRIPCIÓN', '::1', '2020-05-14 15:39:00'),
(1111, 1, 1, 'Actualizar noticia id: 1 titulo: DESCRIPCIÓN', '::1', '2020-05-14 15:40:00'),
(1112, 1, 1, 'Actualizar noticia id: 2 titulo: VIGENCIA', '::1', '2020-05-14 15:41:00'),
(1113, 1, 1, 'Actualizar noticia id: 14 titulo: DESCRIPCIÓN', '::1', '2020-05-14 15:42:00'),
(1114, 1, 1, 'Actualizar noticia id: 57 titulo: DESCRIPCIÓN', '::1', '2020-05-14 15:42:00'),
(1115, 1, 1, 'Actualizar noticia id: 9 titulo: DESCRIPCIÓN', '::1', '2020-05-14 15:43:00'),
(1116, 1, 1, 'Inactivar producto: 5', '::1', '2020-05-14 15:47:00'),
(1117, 1, 1, 'Actualizar noticia id: 3 titulo: FIANZA DE BUENA CALIDAD', '::1', '2020-05-14 15:56:00'),
(1118, 1, 1, 'Inactivar producto: 59', '::1', '2020-05-14 15:59:00'),
(1119, 1, 1, 'Inactivar producto: 60', '::1', '2020-05-14 16:00:00'),
(1120, 1, 1, 'Inactivar producto: 61', '::1', '2020-05-14 16:00:00'),
(1121, 1, 1, 'Inactivar producto: 12', '::1', '2020-05-14 16:01:00'),
(1122, 1, 1, 'Inactivar producto: 15', '::1', '2020-05-14 16:02:00'),
(1123, 1, 1, 'Inactivar producto: 16', '::1', '2020-05-14 16:02:00'),
(1124, 1, 1, 'Inactivar producto: 17', '::1', '2020-05-14 16:03:00'),
(1125, 1, 1, 'Inactivar producto: 18', '::1', '2020-05-14 16:03:00'),
(1126, 1, 1, 'Inactivar producto: 19', '::1', '2020-05-14 16:03:00'),
(1127, 1, 1, 'Inactivar producto: 24', '::1', '2020-05-14 16:03:00'),
(1128, 1, 1, 'Inactivar producto: 25', '::1', '2020-05-14 16:04:00'),
(1129, 1, 1, 'Inactivar producto: 26', '::1', '2020-05-14 16:04:00'),
(1130, 1, 1, 'Inactivar producto: 31', '::1', '2020-05-14 16:04:00'),
(1131, 1, 1, 'Inactivar producto: 32', '::1', '2020-05-14 16:04:00'),
(1132, 1, 1, 'Inactivar producto: 33', '::1', '2020-05-14 16:04:00'),
(1133, 1, 1, 'Inactivar producto: 34', '::1', '2020-05-14 16:05:00'),
(1134, 1, 1, 'Inicio de sesion', '::1', '2020-05-18 09:56:00'),
(1135, 1, 1, 'Inicio de sesion', '::1', '2020-05-27 18:08:00'),
(1136, 1, 1, 'Actualizar noticia id: 3 titulo: UNIAUTO', '::1', '2020-05-27 18:08:00'),
(1137, 1, 1, 'Actualizar noticia id: 5 titulo: PATRIMONIALES', '::1', '2020-05-27 18:08:00'),
(1138, 1, 1, 'Actualizar noticia id: 2 titulo: FIANZAS', '::1', '2020-05-27 18:08:00'),
(1139, 1, 1, 'Inactivar producto: 68', '::1', '2020-05-27 18:43:00'),
(1140, 1, 1, 'Actualizar noticia id: 69 titulo: DESCRIPCIÓN GENERAL', '::1', '2020-05-27 18:44:00'),
(1141, 1, 1, 'Inicio de sesion', '::1', '2020-05-28 11:54:00'),
(1142, 1, 1, 'Actualizar footer id: 5', '::1', '2020-05-28 11:54:00'),
(1143, 1, 1, 'Actualizar footer id: 5', '::1', '2020-05-28 11:55:00'),
(1144, 1, 1, 'Actualizar footer id: 5', '::1', '2020-05-28 11:58:00'),
(1145, 1, 1, 'Cerrar de sesion', '::1', '2020-05-28 13:42:00'),
(1146, 1, 1, 'Registro footer id: 5', '::1', '2020-05-28 20:09:00'),
(1147, 1, 1, 'Registro noticia id: 15 titulo: ', '::1', '2020-05-28 20:45:00'),
(1148, 1, 1, 'Registro noticia id: 15 titulo: ', '::1', '2020-05-28 20:47:00'),
(1149, 1, 1, 'Inicio de sesion', '::1', '2020-05-29 06:11:00'),
(1150, 1, 1, 'Inicio de sesion', '::1', '2020-06-02 10:37:00'),
(1151, 1, 1, 'Cerrar de sesion', '::1', '2020-06-02 11:27:00'),
(1152, 1, 1, 'Inicio de sesion', '::1', '2020-06-02 13:04:00'),
(1153, 1, 1, 'Inicio de sesion', '::1', '2020-06-02 15:37:00'),
(1154, 1, 1, 'Inicio de sesion', '::1', '2020-06-02 16:10:00'),
(1155, 1, 1, 'Registro galeria  id: 267', '::1', '2020-06-02 16:22:00'),
(1156, 1, 1, 'Registro galeria  id: 268', '::1', '2020-06-02 16:28:00'),
(1157, 1, 1, 'Registro galeria  id: 269', '::1', '2020-06-02 16:30:00'),
(1158, 1, 1, 'Registro galeria  id: 270', '::1', '2020-06-02 16:32:00'),
(1159, 1, 1, 'Registro galeria  id: 271', '::1', '2020-06-02 16:32:00'),
(1160, 1, 1, 'Eliminar galeria  id: 271', '::1', '2020-06-02 16:32:00'),
(1161, 1, 1, 'Eliminar galeria  id: 270', '::1', '2020-06-02 16:33:00'),
(1162, 1, 1, 'Eliminar galeria  id: 269', '::1', '2020-06-02 16:33:00'),
(1163, 1, 1, 'Eliminar galeria  id: 268', '::1', '2020-06-02 16:33:00'),
(1164, 1, 1, 'Eliminar galeria  id: 267', '::1', '2020-06-02 16:33:00'),
(1165, 1, 1, 'Cerrar de sesion', '::1', '2020-06-02 16:55:00'),
(1166, 1, 1, 'Inicio de sesion', '::1', '2020-06-03 12:09:00'),
(1167, 1, 1, 'Inicio de sesion', '::1', '2020-06-04 12:56:00'),
(1168, 1, 1, 'Registro galeria  id: 272', '::1', '2020-06-04 13:00:00'),
(1169, 1, 1, 'Registro galeria  id: 273', '::1', '2020-06-04 13:00:00'),
(1170, 1, 1, 'Eliminar galeria  id: 273', '::1', '2020-06-04 13:01:00'),
(1171, 1, 1, 'Registro galeria  id: 274', '::1', '2020-06-04 13:01:00'),
(1172, 1, 1, 'Actualizar noticia id: 2 titulo: FIANZAS', '::1', '2020-06-04 13:01:00'),
(1173, 1, 1, 'Eliminar galeria  id: 272', '::1', '2020-06-04 13:02:00'),
(1174, 1, 1, 'Registro galeria  id: 275', '::1', '2020-06-04 13:03:00'),
(1175, 1, 1, 'Actualizar noticia id: 14 titulo: ROBO', '::1', '2020-06-04 13:03:00'),
(1176, 1, 1, 'Inicio de sesion', '::1', '2020-06-04 15:06:00'),
(1177, 1, 1, 'Registro categtorias cms id: 30', '::1', '2020-06-04 16:12:00'),
(1178, 1, 1, 'Inicio de sesion', '::1', '2020-06-05 11:10:00'),
(1179, 1, 1, 'Actualizar noticia id: 18 titulo: EQUIPO DE CONTRATISTAS', '::1', '2020-06-05 12:54:00'),
(1180, 1, 1, 'Inicio de sesion', '::1', '2020-06-05 15:06:00'),
(1181, 1, 1, 'Actualizar noticia id: 7 titulo: UNIAUTO INTELIGENTE', '::1', '2020-06-05 15:12:00'),
(1182, 1, 1, 'Actualizar noticia id: 18 titulo: EQUIPO DE CONTRATISTAS', '::1', '2020-06-05 15:12:00'),
(1183, 1, 1, 'Actualizar noticia id: 18 titulo: EQUIPO DE CONTRATISTAS', '::1', '2020-06-05 15:18:00'),
(1184, 1, 1, 'Actualizar noticia id: 3 titulo: UNIAUTO', '::1', '2020-06-05 15:21:00'),
(1185, 1, 1, 'Actualizar noticia id: 36 titulo: DESCRIPCIÓN', '::1', '2020-06-05 15:21:00'),
(1186, 1, 1, 'Actualizar noticia id: 7 titulo: UNIAUTO INTELIGENTE', '::1', '2020-06-05 15:28:00'),
(1187, 1, 1, 'Actualizar noticia id: 12 titulo: INCENDIO', '::1', '2020-06-05 15:36:00'),
(1188, 1, 1, 'Actualizar noticia id: 36 titulo: DESCRIPCIÓN', '::1', '2020-06-05 15:36:00');
INSERT INTO `auditoria` (`id`, `id_usuario`, `modulo`, `accion`, `ip`, `fecha_hora`) VALUES
(1189, 1, 1, 'Actualizar noticia id: 2 titulo: UNIAUTO', '::1', '2020-06-05 15:51:00'),
(1190, 1, 1, 'Actualizar noticia id: 1 titulo: PASOS A SEGUIR EN CASO DE UN SINIESTRO:', '::1', '2020-06-05 15:51:00'),
(1191, 1, 1, 'Registro galeria  id: 276', '::1', '2020-06-05 15:56:00'),
(1192, 1, 1, 'Actualizar noticia id: 19 titulo: ROTURA DE MAQUINARIAS', '::1', '2020-06-05 15:57:00'),
(1193, 1, 1, 'Inicio de sesion', '::1', '2020-06-10 09:54:00'),
(1194, 1, 1, 'Registro noticia id: 15 titulo: DESAPARICIÓN O DESTRUCCIÓN TOTAL', '::1', '2020-06-10 09:56:00'),
(1195, 1, 1, 'Actualizar noticia id: 98 titulo: DESAPARICIÓN O DESTRUCCIÓN TOTAL', '::1', '2020-06-10 09:56:00'),
(1196, 1, 1, 'Actualizar noticia id: 37 titulo: COBERTURA DE RESPONSABILIDAD CIVIL', '::1', '2020-06-10 09:57:00'),
(1197, 1, 1, 'Actualizar noticia id: 37 titulo: COBERTURA DE RESPONSABILIDAD CIVIL', '::1', '2020-06-10 09:57:00'),
(1198, 1, 1, 'Actualizar noticia id: 38 titulo: COBERTURA DE EXCESOS DE LIMITES SOBRE R.C.V', '::1', '2020-06-10 10:03:00'),
(1199, 1, 1, 'Actualizar noticia id: 51 titulo: COBERTURA DE ASISTENCIA LEGAL Y DEFENSA PENAL', '::1', '2020-06-10 10:03:00'),
(1200, 1, 1, 'Actualizar noticia id: 51 titulo: COBERTURA DE ASISTENCIA LEGAL Y DEFENSA PENAL', '::1', '2020-06-10 10:03:00'),
(1201, 1, 1, 'Actualizar noticia id: 56 titulo: COBERTURA DE ACCIDENTES PERSONALES PARA LOS OCUPANTES DEL VEHÍCULOS TERRESTRES( APOV)', '::1', '2020-06-10 10:05:00'),
(1202, 1, 1, 'Registro noticia id: 15 titulo: REQUISITOS NECESARIOS PARA LA EMISIÓN AUTOMÓVIL', '::1', '2020-06-10 10:17:00'),
(1203, 1, 1, 'Registro noticia id: 15 titulo: PASOS A SEGUIR PARA LA EMISIÓN DE AUTOMÓVIL', '::1', '2020-06-10 10:17:00'),
(1204, 1, 1, 'Registro noticia id: 15 titulo: PASOS A SEGUIR EN CASO DE SINIESTRO', '::1', '2020-06-10 10:19:00'),
(1205, 1, 1, 'Actualizar noticia id: 92 titulo: SEGURO DE TODO RIESGO DE CONSTRUCCIÓN', '::1', '2020-06-10 10:30:00'),
(1206, 1, 1, 'Registro noticia id: 15 titulo: ROTURA DE MAQUINARIA', '::1', '2020-06-10 10:32:00'),
(1207, 1, 1, 'Actualizar noticia id: 102 titulo: ROTURA DE MAQUINARIA', '::1', '2020-06-10 10:33:00'),
(1208, 1, 1, 'Registro noticia id: 15 titulo: EQUIPOS ELECTRÓNICOS', '::1', '2020-06-10 10:35:00'),
(1209, 1, 1, 'Registro noticia id: 15 titulo: EQUIPOS DE CONTRATISTA', '::1', '2020-06-10 10:37:00'),
(1210, 1, 1, 'Inactivar producto: 79', '::1', '2020-06-10 10:37:00'),
(1211, 1, 1, 'Eliminar producto: 79', '::1', '2020-06-10 10:37:00'),
(1212, 1, 1, 'Eliminar producto: 80', '::1', '2020-06-10 10:38:00'),
(1213, 1, 1, 'Eliminar producto: 93', '::1', '2020-06-10 10:39:00'),
(1214, 1, 1, 'Actualizar noticia id: 92 titulo: SEGURO DE TODO RIESGO DE CONSTRUCCIÓN', '::1', '2020-06-10 10:40:00'),
(1215, 0, 1, 'Cerrar de sesion', '::1', '2020-06-10 19:39:00'),
(1216, 1, 1, 'Inicio de sesion', '::1', '2020-06-16 10:19:00'),
(1217, 1, 1, 'Actualizar noticia id: 7 titulo: UNIAUTO PLAN AMIGO', '::1', '2020-06-16 10:24:00'),
(1218, 1, 1, 'Eliminar producto: 36', '::1', '2020-06-16 10:26:00'),
(1219, 1, 1, 'Actualizar noticia id: 98 titulo: DESCRIPCIÓN', '::1', '2020-06-16 10:27:00'),
(1220, 1, 1, 'Cerrar de sesion', '::1', '2020-06-16 11:12:00'),
(1221, 1, 1, 'Inicio de sesion', '::1', '2020-06-17 09:21:00'),
(1222, 1, 1, 'Inicio de sesion', '::1', '2020-06-17 16:26:00'),
(1223, 1, 1, 'Inicio de sesion', '::1', '2020-06-20 19:25:00'),
(1224, 1, 1, 'Inicio de sesion', '::1', '2020-06-26 11:18:00'),
(1225, 1, 1, 'Inicio de sesion', '::1', '2020-07-06 09:50:00'),
(1226, 1, 1, 'Cerrar de sesion', '::1', '2020-07-06 09:51:00'),
(1227, 1, 1, 'Inicio de sesion', '::1', '2020-07-06 13:44:00'),
(1228, 1, 1, 'Cerrar de sesion', '::1', '2020-07-06 13:44:00'),
(1229, 1, 1, 'Inicio de sesion', '::1', '2020-07-12 12:26:00'),
(1230, 1, 1, 'Inicio de sesion', '::1', '2020-07-12 16:07:00'),
(1231, 1, 1, 'Actualizacion de marca id: 16', '::1', '2020-07-12 16:19:00'),
(1232, 1, 1, 'Actualizacion de marca id: 16', '::1', '2020-07-12 16:19:00'),
(1233, 1, 1, 'Actualizacion de marca id: 16', '::1', '2020-07-12 16:21:00'),
(1234, 1, 1, 'Actualizacion de marca id: 16', '::1', '2020-07-12 16:22:00'),
(1235, 1, 1, 'Actualizacion de marca id: 16', '::1', '2020-07-12 16:24:00'),
(1236, 1, 1, 'Actualizacion de marca id: 16', '::1', '2020-07-12 16:24:00'),
(1237, 1, 1, 'Actualizacion de marca id: 16', '::1', '2020-07-12 16:25:00'),
(1238, 1, 1, 'Actualizacion de marca id: 18', '::1', '2020-07-12 16:25:00'),
(1239, 1, 1, 'Registro de direccion id:20,titulo:prueba', '::1', '2020-07-12 16:26:00'),
(1240, 1, 1, 'Registro de direccion id:21,titulo:prueba', '::1', '2020-07-12 16:26:00'),
(1241, 1, 1, 'Registro de direccion id:22,titulo:prueba', '::1', '2020-07-12 16:26:00'),
(1242, 1, 1, 'Actualizacion de marca id: 18', '::1', '2020-07-12 16:26:00'),
(1243, 1, 1, 'Eliminar direccion id: 20', '::1', '2020-07-12 16:26:00'),
(1244, 1, 1, 'Eliminar direccion id: 21', '::1', '2020-07-12 16:26:00'),
(1245, 1, 1, 'Eliminar direccion id: 22', '::1', '2020-07-12 16:27:00'),
(1246, 1, 1, 'Registro de direccion id:23,titulo:Prueba de dirección', '::1', '2020-07-12 16:28:00'),
(1247, 1, 1, 'Actualizacion de marca id: 23', '::1', '2020-07-12 16:28:00'),
(1248, 1, 1, 'Eliminar direccion id: 23', '::1', '2020-07-12 16:28:00'),
(1249, 1, 1, 'Registro de direccion id:24,titulo:Dirección prueba', '::1', '2020-07-12 16:29:00'),
(1250, 1, 1, 'Actualizacion de marca id: 24', '::1', '2020-07-12 16:29:00'),
(1251, 1, 1, 'Eliminar direccion id: 24', '::1', '2020-07-12 16:34:00'),
(1252, 1, 1, 'Registro de direccion id:25,titulo:prueba Y', '::1', '2020-07-12 16:35:00'),
(1253, 1, 1, 'Actualizacion de marca id: 25', '::1', '2020-07-12 16:42:00'),
(1254, 1, 1, 'Actualizacion de marca id: 16', '::1', '2020-07-12 16:59:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bancos`
--

CREATE TABLE `bancos` (
  `id` int(50) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `estatus` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `bancos`
--

INSERT INTO `bancos` (`id`, `descripcion`, `estatus`) VALUES
(1, 'Mercantil', 1),
(5, 'Banesco Banco Universal', 2),
(6, 'Banesco', 1),
(8, 'Exterior', 1),
(9, 'Provincial', 1),
(10, 'Bod', 1),
(11, 'Caroní', 1),
(12, 'Del Sur', 1),
(13, 'Banco De Venezuela', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cambio_contraseña`
--

CREATE TABLE `cambio_contraseña` (
  `id` int(100) NOT NULL,
  `id_usuario` int(100) NOT NULL,
  `codigo` varchar(100) NOT NULL,
  `id_identificador` varchar(100) NOT NULL,
  `estatus` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cambio_contraseña`
--

INSERT INTO `cambio_contraseña` (`id`, `id_usuario`, `codigo`, `id_identificador`, `estatus`) VALUES
(10, 44, '', '98fbc42faedc02492397cb5962ea3a3ffc0a9243', 2),
(11, 47, '', '827bfc458708f0b442009c9c9836f7e4b65557fb', 2),
(12, 48, '', '64e095fe763fc62418378753f9402623bea9e227', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cantidad_producto`
--

CREATE TABLE `cantidad_producto` (
  `id` int(11) NOT NULL,
  `id_det_prod` int(11) NOT NULL,
  `cantidad` text NOT NULL,
  `id_color` int(11) NOT NULL,
  `id_talla` int(11) NOT NULL,
  `estatus` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cantidad_producto`
--

INSERT INTO `cantidad_producto` (`id`, `id_det_prod`, `cantidad`, `id_color`, `id_talla`, `estatus`) VALUES
(253, 121, '100', 10, 2, 1),
(252, 121, '100', 10, 3, 1),
(251, 121, '100', 10, 4, 1),
(250, 119, '100', 10, 2, 1),
(249, 119, '100', 10, 3, 1),
(248, 119, '100', 10, 4, 1),
(247, 117, '100', 10, 2, 1),
(246, 117, '100', 10, 3, 1),
(245, 117, '100', 10, 4, 1),
(244, 115, '100', 10, 2, 1),
(243, 115, '100', 10, 3, 1),
(242, 115, '100', 10, 4, 1),
(241, 113, '100', 10, 2, 1),
(240, 113, '100', 10, 3, 1),
(239, 113, '100', 10, 4, 1),
(238, 111, '100', 10, 2, 1),
(237, 111, '100', 10, 3, 1),
(236, 111, '100', 10, 4, 1),
(235, 109, '100', 10, 2, 1),
(234, 109, '100', 10, 3, 1),
(233, 109, '100', 10, 4, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carga_pdfs`
--

CREATE TABLE `carga_pdfs` (
  `id` int(11) NOT NULL,
  `titulo` varchar(500) NOT NULL,
  `descripcion` text NOT NULL,
  `ruta` varchar(500) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `estatus` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `carga_pdfs`
--

INSERT INTO `carga_pdfs` (`id`, `titulo`, `descripcion`, `ruta`, `id_categoria`, `estatus`) VALUES
(1, 'Instrucciones_transferencias_divisas.pdf', '0', 'assets/pdf/archivos/quienes_somos/instrucciones_transferencias_divisas.pdf', 3, 2),
(2, 'Undefined.pdf', 'Dsfdfds', 'assets/pdf/archivos/productos/undefined.pdf', 26, 2),
(3, 'Pdf_prueba.pdf', 'Prueba de registro', 'assets/pdf/archivos/marcas/pdf_prueba.pdf', 2, 2),
(4, 'Titulo.pdf', 'Prueba de pdf', 'assets/pdf/archivos/marcas/titulo.pdf', 2, 2),
(5, 'Prueba.pdf', 'Prueba', 'assets/pdf/archivos/slider/PRUEBA.pdf', 1, 2),
(6, 'Titulo2.pdf', 'Pruebas', 'assets/pdf/archivos/marcas/titulo2.pdf', 2, 2),
(7, 'Instructivo_transferencias_internacionales.pdf', 'Pulse aquí para descargar instructivo de&nbsp; transferencias internacionales', 'assets/pdf/archivos/tipos_servicios/instructivo_transferencias_internacionales.pdf', 29, 2),
(8, 'Instructivo_transferencias_internacionales.pdf', 'Instructivo para transferencias internacionales', 'assets/pdf/archivos/contactanos/instructivo_transferencias_internacionales.pdf', 30, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carrito_encabezado`
--

CREATE TABLE `carrito_encabezado` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `estatus` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `carrito_encabezado`
--

INSERT INTO `carrito_encabezado` (`id`, `id_usuario`, `estatus`) VALUES
(79, 44, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carrito_productos`
--

CREATE TABLE `carrito_productos` (
  `id` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `monto` float NOT NULL,
  `monto_total` float NOT NULL,
  `fecha` date NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `estatus` int(11) NOT NULL,
  `id_carrito` int(11) NOT NULL,
  `id_cantidad_producto` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `carrito_productos`
--

INSERT INTO `carrito_productos` (`id`, `id_producto`, `cantidad`, `monto`, `monto_total`, `fecha`, `id_usuario`, `id_idioma`, `estatus`, `id_carrito`, `id_cantidad_producto`) VALUES
(115, 115, 2, 200, 400, '2020-02-06', 44, 2, 2, 79, 244);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(200) NOT NULL,
  `estatus` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id`, `descripcion`, `estatus`) VALUES
(1, 'SLIDER', 1),
(2, 'Marcas', 0),
(3, 'quienes_somos', 1),
(4, 'noticias', 0),
(5, 'Detalles negocios', 0),
(6, 'imagenes', 1),
(7, 'SERVICIOS', 1),
(8, 'Portafolio', 0),
(9, 'Clientes', 0),
(18, 'PRUEBA DE DESCRIPCION', 2),
(19, 'PRUEBA SLIDER DOS', 2),
(21, '123', 2),
(22, 'PRODUCTOS', 2),
(23, 'DIRECTIVA', 1),
(24, 'REASEGURADORAS', 1),
(25, 'USUARIOS', 1),
(26, 'PRODUCTOS', 1),
(27, 'TIPOS_PRODUCTOS', 1),
(28, 'TIPOS_SERVICIOS', 2),
(29, 'TIPOS_SERVICIOS', 1),
(30, 'CONTACTANOS', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias_productos_idiomas`
--

CREATE TABLE `categorias_productos_idiomas` (
  `id` int(11) NOT NULL,
  `nombre_tabla` text NOT NULL,
  `id_categoria_es` int(11) NOT NULL,
  `id_categoria_en` int(11) NOT NULL,
  `estatus` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `categorias_productos_idiomas`
--

INSERT INTO `categorias_productos_idiomas` (`id`, `nombre_tabla`, `id_categoria_es`, `id_categoria_en`, `estatus`) VALUES
(28, 'Colores', 1, 3, 1),
(29, 'Colores', 2, 4, 1),
(30, 'Colores', 8, 9, 1),
(31, 'Marcas', 7, 9, 1),
(32, 'Marcas', 8, 10, 1),
(33, 'Categoria Productos', 9, 11, 1),
(34, 'Tipo Producto', 42, 45, 1),
(36, 'Categoria Productos', 8, 10, 1),
(37, 'Marcas', 13, 14, 1),
(38, 'Colores', 10, 11, 1),
(39, 'Marcas', 11, 12, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria_producto`
--

CREATE TABLE `categoria_producto` (
  `id` int(11) NOT NULL,
  `titulo` varchar(250) NOT NULL,
  `descripcion` varchar(250) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `estatus` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categoria_producto`
--

INSERT INTO `categoria_producto` (`id`, `titulo`, `descripcion`, `id_idioma`, `estatus`) VALUES
(8, 'MASCULINO', 'ROPA MASCULINA', 1, 1),
(9, 'FEMENINO', 'ROPA FEMENINA', 1, 1),
(10, 'MALE', '<U></U><DIV><SPAN>MEN\'S CLOTHES</SPAN><U><BR></U></DIV>', 2, 1),
(11, 'FEMALE', '<DIV><BR></DIV><DIV><SPAN>WOMENSWEAR</SPAN><BR></DIV>', 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `estatus` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id`, `descripcion`, `id_idioma`, `estatus`) VALUES
(2, '<p><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; line-height: 20px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis fermentum libero libero, quis iaculis est venenatis quis. Quisque sodales id ligula eget gravida. Nulla scelerisque arcu at eleifend maximus. Cras cursus, quam at lacinia iaculis, ex mauris pretium nibh, sed cursus urna augue vitae nisi. Nulla nulla augue, placerat a magna vel, semper placerat turpis. Ut ut velit ornare, mollis odio nec, fringilla tortor. Praesent vitae ligula bibendum, accumsan ante sed, pretium magna.</span></p>', 1, 1),
(3, '<p><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; line-height: 20px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis fermentum libero libero, quis iaculis est venenatis quis. Quisque sodales id ligula eget gravida. Nulla scelerisque arcu at eleifend maximus. Cras cursus, quam at lacinia iaculis, ex mauris pretium nibh, sed cursus urna augue vitae nisi. Nulla nulla augue, placerat a magna vel, semper placerat turpis. Ut ut velit ornare, mollis odio nec, fringilla tortor. Praesent vitae ligula bibendum, accumsan ante sed, pretium magna.</span></p>', 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `colores`
--

CREATE TABLE `colores` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `estatus` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `colores`
--

INSERT INTO `colores` (`id`, `descripcion`, `id_idioma`, `estatus`) VALUES
(1, 'BLANCO', 1, 1),
(2, 'NEGRO', 1, 1),
(3, 'WHITE', 2, 1),
(4, 'BLACK', 2, 1),
(8, 'GRIS', 1, 1),
(7, 'AZUL', 1, 2),
(9, 'GREY', 2, 1),
(10, 'BICOLOR', 1, 1),
(11, 'BI COLOR', 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contactos`
--

CREATE TABLE `contactos` (
  `id` int(11) NOT NULL,
  `nombres` text NOT NULL,
  `telefono` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mensaje` text NOT NULL,
  `razon` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `contactos`
--

INSERT INTO `contactos` (`id`, `nombres`, `telefono`, `email`, `mensaje`, `razon`) VALUES
(142, 'Gianni', '4168092388', 'gianni.d.santucci@gmail.com', 'prueba dese el home', ''),
(141, 'Gianni', '4168092388', 'gianni.d.santucci@gmail.com', 'prueba de otro mensaje', ''),
(140, 'gianni', '04124561122', 'gianni.d.santucci@gmail.com', 'prueba de mensaje', ''),
(139, 'strong man trrk', '4168092388', 'gianni.d.santucci@gmail.com', 'str', ''),
(138, 'strong man tarrako', '04168092388', 'gianni.d.santucci@gmail.com', 'prueba', ''),
(137, 'strongman', '04168092388', 'gianni.d.santucci@gmail.com', 'Prueba de ejem', ''),
(136, 'Gianni', '04125641122', 'gianni.d.santucci@gmail.com', 'prueba de mensaje', ''),
(135, 'José peseiro', '04124564411', 'jpeseiro@gmail.com', 'Ejemplo en inicio', ''),
(132, 'Gianni', '4168092388', 'gianni.d.santucci@gmail.com', 'prueba', ''),
(133, 'Gianni', '4168092388', 'gianni.d.santucci@gmail.com', 'prueba', ''),
(134, 'Gustavo ocanto', '02124444444444', 'gjocanto@gmail.com', 'prueba de mensaje en contactanos', ''),
(131, 'Gianni', '4168092388', 'gianni.d.santucci@gmail.com', 'prueba de mensaje form inicio', ''),
(143, 'Gianni', '4168092388', 'gianni.d.santucci@gmail.com', 'Esto es una prueba', ''),
(144, 'Gianni', '4168092388', 'gianni.d.santucci@gmail.com', 'prueba', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contactos_empleo`
--

CREATE TABLE `contactos_empleo` (
  `id` int(11) NOT NULL,
  `nombres` varchar(400) NOT NULL,
  `email` varchar(400) NOT NULL,
  `area` varchar(400) NOT NULL,
  `mensaje` text NOT NULL,
  `cv` text NOT NULL,
  `telefono` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `contactos_empleo`
--

INSERT INTO `contactos_empleo` (`id`, `nombres`, `email`, `area`, `mensaje`, `cv`, `telefono`) VALUES
(36, 'gianni', 'gianni.d.santucci@gmail.com', 'prueba', 'prueba de registro', '../administrador/site_media/images/archivos/cv/cv_gianni.pdf', '04167485648'),
(47, 'registro', 'registro@gmail.com', 'prueba', 'prueba', '../administrador/site_media/images/archivos/cv/cv_registro.pdf', '04167485648'),
(48, 'registrodos', 'registrodos@gmail.com', 'prueba', 'prueba', '../administrador/site_media/images/archivos/cv/cv_registrodos.pdf', '04167485648'),
(49, 'Miguel', 'mporro@cantv.net', 'desarrollo', 'mensaje de prueba', '../administrador/site_media/images/archivos/cv/cv_Miguel.pdf', '04122473595'),
(50, 'Gianni santucci', 'asdf@gmail.com', 'prueba', 'esto es una prueba de envío de email desde itssca.net', '../administrador/site_media/images/archivos/cv/cv_Gianni_santucci.pdf', '04129877777'),
(51, 'Gianni Santucci', 'gianni.d.santucci@gmail.com', 'prueba', 'Prueba de envio de email. desde la web.', '../administrador/site_media/images/archivos/cv/cv_Gianni_Santucci.pdf', '04126666666'),
(52, 'Prueba de cv', 'cv@gmail.com', 'Prueba', 'Prueba de cv desde seccion contactanos', '../administrador/site_media/images/archivos/cv/cv_Prueba_de cv.pdf', '02129999999'),
(53, 'Gustavo Ocanto', 'gjocanto@gmail.com', 'vendedor de', 'sos chevere', '../administrador/site_media/images/archivos/cv/cv_Gustavo_Ocanto.pdf', '02125455122'),
(54, 'Prueba de cv', 'prueba@gmail.com', 'Prueba', 'Prueba adjuntando archivos', '../administrador/site_media/images/archivos/cv/cv_Prueba_de cv.pdf', '02123333333'),
(55, 'prueba', 'prueba@gmail.com', 'prueba', 'prueba', '../administrador/site_media/images/archivos/cv/cv_prueba.pdf', '02121444444'),
(56, 'prueba ultima del dia', 'asdf@gmail.com', 'prueba', 'prueba', '../administrador/site_media/images/archivos/cv/cv_prueba_ultima del dia.pdf', '04125555555'),
(57, 'Gianni Snatucci', 'prueba@gmail.com', 'prueba', 'Prueba de viernes 17-11-2017', '../administrador/site_media/images/archivos/cv/cv_Gianni_Snatucci.pdf', '04167415588'),
(58, 'Prueba', 'prueba@gmail.com', 'prueba', 'Prueba viernes 17 11 2014', '../administrador/site_media/images/archivos/cv/cv_Prueba.pdf', '04167485648'),
(59, 'Gianni Santucci', 'gianni.d.santucci@gmail.com', 'Prueba', 'Prueba de registro de cv página web', '../administrador/site_media/images/archivos/cv/cv_Gianni_Santucci.pdf', '04167485648'),
(60, 'Abimael Milano', 'abi_milano10@hotmail.com', 'Telecomunicaciones', 'Buenos días, me interesaría conocerlos mas a fondo y tener la oportunidad de crecer junto a la empre', '../administrador/site_media/images/archivos/cv/cv_Abimael_Milano.pdf', '04241752935'),
(61, 'Luis Eduardo Carpintero', 'luise2612@gmail.com', 'Consultor de redes y telecomunicaciones', 'Proactivo, creativo y disciplinado; Luis Carpintero es un consultor de redes que disfruta de encontrar soluciones creativas para redes complejas, utilizando las mejores prácticas. Con una comprensión rápida, solución y viabilidad en el aprendizaje de nuevas tecnologías', '../administrador/site_media/images/archivos/cv/cv_Luis_Eduardo Carpintero.pdf', '04241671246'),
(62, 'Luigi Toro', 'toroluigi123@gmail.com', 'soporte', 'Actualmente soy analista de soporte en venevision plus, me gustaría ingresar en una área relacionado', '../administrador/site_media/images/archivos/cv/cv_Luigi_Toro.pdf', '04140203600');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `correos`
--

CREATE TABLE `correos` (
  `id` int(11) NOT NULL,
  `correo` varchar(100) NOT NULL,
  `titulo` text NOT NULL,
  `estatus` int(11) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `posicion` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `correos`
--

INSERT INTO `correos` (`id`, `correo`, `titulo`, `estatus`, `id_idioma`, `posicion`) VALUES
(1, 'ventas@itssca.net', 'Asistencia Comercial', 1, 1, 4),
(2, 'soporte@itssca.net', 'Asistencia Técnica', 1, 1, 1),
(3, 'mercadeo@itssca.net', 'Mercadeo', 1, 1, 2),
(4, 'rrhh@itssca.net', 'Empleos', 1, 1, 3),
(5, 'webmaster@itssca.net', 'Webmaster', 0, 1, 1),
(6, 'prueba@gmail.com', 'prueba', 0, 1, 2),
(7, 'support@itssca.net', 'Technical Support', 1, 2, 0),
(8, 'sales@itssca.net', 'Sales', 1, 2, 0),
(9, 'marketing@itssca.net', 'Marketing', 1, 2, 0),
(10, 'rrhh@itssca.net', 'Work with us', 1, 2, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuentas_bancarias`
--

CREATE TABLE `cuentas_bancarias` (
  `id` int(50) NOT NULL,
  `id_banco` int(50) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `estatus` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cuentas_bancarias`
--

INSERT INTO `cuentas_bancarias` (`id`, `id_banco`, `descripcion`, `estatus`) VALUES
(1, 1, 'Fdvfdg', 2),
(2, 6, 'dfgdfg13123 123144 34 34 erfsdf', 2),
(3, 1, '4545-32343434-343-4-34-3-4-34-3-4-34-34', 2),
(4, 8, '0102-05254020--20.6-', 2),
(5, 1, '0105-0077-0510-7741-3483', 2),
(6, 8, '0102-0202-0304-05', 2),
(7, 6, '0134-0032-6303-2100-5527', 2),
(8, 13, '0102-0141-1000-0101-4668', 1),
(9, 8, '0115-0020-0430-0020-7017', 1),
(10, 9, '0108-0027-7901-0031-2422', 1),
(11, 6, '0134-0032-6303-2100-5527', 1),
(12, 10, '0116-0118-9201-8023-0824', 1),
(13, 11, '0128-0030-4930-0046-5108', 1),
(14, 1, '0105-0077-0510-7741-3483', 1),
(15, 12, '0157-0030-1038-3000-0166', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `descripcion`
--

CREATE TABLE `descripcion` (
  `id` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  `estatus` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `descripcion`
--

INSERT INTO `descripcion` (`id`, `descripcion`, `estatus`) VALUES
(1, 'prueba de registros descripcion', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_productos`
--

CREATE TABLE `detalle_productos` (
  `id` int(11) NOT NULL,
  `id_productos` int(11) NOT NULL,
  `id_tipo_producto` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `descripcion` text NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `estatus` int(11) NOT NULL DEFAULT '1',
  `slug` varchar(300) NOT NULL,
  `orden` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `detalle_productos`
--

INSERT INTO `detalle_productos` (`id`, `id_productos`, `id_tipo_producto`, `titulo`, `descripcion`, `id_idioma`, `estatus`, `slug`, `orden`) VALUES
(1, 2, 38, 'Descripción', '<span>Se trata de un concurso en el que el cliente oferta sus obras, servicios o suministros. Esta fianza garantiza que el cliente firmará el contrato en las condiciones ofertadas, en caso de adjudicación de la obra, servicio o suministro.</span>', 1, 1, 'descripcion', 1),
(2, 2, 38, 'Vigencia', 'La&nbsp;<span>fianza de mantenimiento de la oferta&nbsp;</span>está vigente hasta la firma del contrato o hasta que caduque su período de vigencia. Lo que ocurra primero.\nHoy día de acuerdo a la nueva ley de licitaciones, la fianza deberá estar vigente hasta la firma del contrato y la entrega por parte del afianzado de la fianza de fiel cumplimiento.', 1, 1, 'vigencia', 2),
(3, 2, 38, 'Requisitos', '<b>Del Solicitante:\n</b><br><ol><li>Planilla de solicitud debidamente completada.</li><li>\nLlenar ficha identificación cliente.</li><li>\nCopia estatutos.</li><li>Modificaciones a&nbsp; estatutos, venta de acciones, cambio de razon social, etc.</li><li>\nEstados financieros&nbsp; auditados&nbsp; de los tres últimos ejercicios financieros (original), visados por el colegio de contadores públicos.</li><li>Relación de obras ejecutadas y por ejecutar con monto de ellas, indicando compañias que otorgaron las fianzas.</li><li>\nDos (2) referencias comerciales y Dos (2) bancarias.</li><li>\nCopia del R.I.F.</li><li>\nCopia de la ultima declaración del impuesto sobre la renta (ISLR)\n</li></ol><br><b>Del Contragarante:\n</b><br><br><b>Persona Natural:</b><br><ol><li>Copia de la cedula de identidad.</li><li>\nCopia de la cedula de identidad esposa(o).</li><li>Copia del poder otorgado por esta(e).</li><li>Estados financieros actualizados (original), visado por el colegio de contadores publicos con vigencia no superior a tres (03) meses.</li><li>Copia de los documentos de inmuebles reflejados en los estados financieros.</li><li>Dos (2) referencias bancarias y dos (2) comerciales.</li><li> Copia de la última declaración del impuesto sobre la renta (ISLR).</li></ol>', 1, 1, 'requisitos', 3),
(4, 2, 2, 'Descripción', ' La fianza de Fiel Cumplimiento garantiza, el fiel, cabal y oportuno cumplimiento por parte del Afianzado de todas y cada una de las obligaciones que resulten a su cargo y a favor del Acreedor, según el contrato entre ellos suscrito.', 1, 1, 'descripcion', 1),
(5, 2, 2, 'Vigencia', 'Hasta la recepción Definitiva o hasta que esta se considere realizada de acuerdo al contrato. Adicionalmente si ha transcurrido un año desde la recepción provisional sin que se haya incoado la correspondiente demanda por ante los Tribunales competentes, caducan las acciones frente al fiador (Compañía).\nEn el caso de los contratos de obra hay tres momentos sobre los cuales debemos hacer mención especial.\n<br><ul><li>Terminación de la Obra.</li><li>\nLa recepción provisional.</li><li>\nActa de Recepción Definitiva.</li></ul>', 1, 0, 'vigencia', 2),
(6, 2, 2, 'Requisitos', '<b>Del Solicitante:\n</b><br><ol><li>Planilla de solicitud debidamente completada.</li><li>\nLlenar ficha identificación cliente.</li><li>\nCopia estatutos.</li><li>\nModificaciones a&nbsp; estatutos, venta de acciones, cambio de razon social, etc.</li><li>Estados financieros&nbsp; auditados&nbsp; de los tres últimos ejercicios financieros (original), visados por el colegio de contadores publicos.</li><li>\nRelacion de obras ejecutadas y por ejecutar con monto de ellas, indicando compañias que otorgaron las fianzas.</li><li>\nDos (2) referencias comerciales y Dos (2) bancarias.</li><li>\nCopia del R.I.F.</li><li>\nCopia de la ultima declaración del impuesto sobre la renta (ISLR).</li></ol><br><b>Del Contragarante:\n</b><br><b>Persona Natural:\n</b><br><ol><li>Copia de la cedula de identidad.</li><li>\nCopia de la cedula de identidad esposa(o).</li><li>\nCopia del poder otorgado por esta(e).</li><li>\nEstados financieros actualizados (original), visado por el colegio de contadores públicos con vigencia no superior a tres (03) meses.</li><li>\nCopia de los documentos de inmuebles reflejados en los estados financieros.</li><li>\nDos (2) referencias bancarias y dos (2) comerciales.</li><li>\nCopia de la última declaración del impuesto sobre la renta (ISLR).</li></ol>', 1, 1, 'requisitos', 3),
(64, 4, 8, 'Cobertura básica', '<span>Muerte Accidental.</span>', 1, 1, 'cobertura-basica', 2),
(65, 4, 8, 'Cobertura adicional', '<ul><li><span>Invalidez\n	Total y Permanente,</span>\n	</li><li>\nIncapacidad\n	Temporal y Permanente,\n	</li><li>\nIncapacidad\n	temporal, por semana,\n	</li><li>\nGastos\n	Médicos y Farmacéuticos por Accidente.\n</li></ul>\n<span><br>\n\n</span>', 1, 1, 'cobertura-adicional', 3),
(63, 4, 8, 'Descripción', 'El objetivo de este seguro es garantizarle a la persona asegurada o al beneficiario designado, el pago de las indemnizaciones estipuladas en la Póliza en los casos de accidentes corporales, ocurridos en el <b>ejercicio de su profesión</b> o <b>en el curso de la vida privada</b>, que hayan tenido como causa real, directa y exclusiva, las heridas o lesiones corporales ocasionadas por la acción fortuita, repentina y violenta de una fuerza o agente externo ajeno a la voluntad o intención del Asegurado y que pueda ser determinado por los médicos de una manera cierta.', 1, 1, 'descripcirn', 1),
(9, 2, 3, 'Descripción', '<span>La Fianza de Buena Calidad garantiza que durante el período existente entre la recepción provisional de la obra y la recepción&nbsp; definitiva, los materiales utilizados fueron de calidad y condiciones ofrecidas por el afianzado</span>', 1, 1, 'descripcion', 1),
(10, 2, 3, 'Requisitos', '<b>Del Solicitante:\n</b><br><ol><li>Planilla de solicitud debidamente completada.</li><li>\nLlenar ficha identificación cliente.</li><li>\nCopia estatutos.</li><li>\nModificaciones a&nbsp; estatutos, venta de acciones, cambio de razon social, etc.</li><li>\nEstados financieros&nbsp; auditados&nbsp; de los tres últimos ejercicios financieros (original), visados por el colegio de contadores públicos.</li><li>\nRelación de obras ejecutadas y por ejecutar con monto de ellas, indicando compañías que otorgaron las fianzas.</li><li>\nDos (2) referencias comerciales y Dos (2) bancarias.</li><li>Copia del R.I.F.</li><li>\nCopia de la ultima declaración del impuesto sobre la renta (ISLR).</li></ol><b>Del Contragarante:</b><div><br></div><b>Persona Natural:<br></b><ol><li>Copia de la cédula de identidad.</li><li>Copia de la cédula de identidad esposa(o).</li><li>Copia del poder otorgado por esta(e)</li><li>.Estados financieros actualizados (original), visado por el colegio de contadores públicos con vigencia no superior a tres (03) meses.</li><li>Copia de los documentos de inmuebles reflejados en los estados financieros.</li><li>Dos (2) referencias bancarias y dos (2) comerciales.</li><li>Copia de la última declaración del impuesto sobre la renta (ISLR).</li></ol>', 1, 1, 'requisitos', 2),
(11, 2, 5, 'Descripción', '<span>La Fianza Laboral garantiza el pago de las prestaciones sociales e indemnizaciones de los trabajadores que intervienen en la ejecución de una obra o servicio.</span>', 1, 1, 'descripcion', 1),
(12, 2, 5, 'Duración', 'Hasta que el Acreedor expida el finiquito correspondiente es decir Acta de Aceptación Definitiva, o un (1) año después de otorgada la Recepción Provisional de la Obra, según sea el caso. Con la nueva Ley Orgánica del Trabajo se ha ampliado este plazo y la duración es hasta 14 meses después de la recepción provisional.', 1, 0, 'duracion', 2),
(13, 2, 5, 'Requisitos', '<b>Del Solicitante:\n</b><br><ol><li>Planilla de solicitud debidamente completada.</li><li>Llenar ficha identificación cliente.</li><li>Copia estatutos.</li><li>Modificaciones a&nbsp; estatutos, venta de acciones, cambio de razón social, etc.</li><li>Estados financieros&nbsp; auditados&nbsp; de los tres últimos ejercicios financieros (original), visados por el colegio de contadores públicos.</li><li>Relación de obras ejecutadas y por ejecutar con monto de ellas, indicando compañías que otorgaron las fianzas.</li><li>Dos (2) referencias comerciales y Dos (2) bancarias.\ncopia del R.I.F.</li><li>Copia de la ultima declaración del impuesto sobre la renta (ISLR).</li></ol><br><b>Del Contragarante:</b>\n<br><br><b>Persona Natural:</b>\n<br><ol><li>Copia de la cédula de identidad.</li><li>\nCopia de la cédula de identidad esposa(o).</li><li>\nCopia del poder otorgado por esta(e).</li><li>Estados financieros actualizados (original), visado por el colegio de contadores públicos con vigencia no superior a tres (03) meses.</li><li>\nCopia de los documentos de inmuebles reflejados en los estados financieros.</li><li>\nDos (2) referencias bancarias y dos (2) comerciales.</li><li>\nCopia de la última declaración del impuesto sobre la renta (ISLR).</li></ol>', 1, 1, 'requisitos', 3),
(14, 2, 6, 'Descripción', '<span>Garantiza las operaciones y derechos de importación o exportación, que de acuerdo a la Ley pueden ser garantizadas con caución. Se otorgan a favor de la República Bolivariana de Venezuela, a través de las Aduanas Nacionales.</span>', 1, 1, 'descripcion', 1),
(15, 2, 6, 'Diferencia de aforo', 'Garantizan los impuestos de importación diferenciales causados con motivo de las divergencias de aforo que, se susciten en el acto de reconocimiento de mercancías y penas pecuniarias que, puedan ser impuestas con ocasión de dichas divergencias, correspondientes a mercancías llegadas a cualquier puerto, consignadas por el Afianzado o cuyos manifiestos sean presentados por éste en virtud de designación hecha por el consignatario, así como también por los manifiestos que presente nuestro cliente actuando a nombre del consignatario en virtud de poder autenticado o carta poder. Literales A, B, C, D y E del artículo 120 de la LOA.', 1, 0, 'difrencia_de_aforo', 2),
(16, 2, 6, 'Falta de entrega', 'Cubre las cantidades afianzables con motivo de la entrega de mercancías sin acompañar el respectivo manifiesto o conocimiento de embarque original, correspondiente a mercaderías llegadas a la aduana respectiva. Tal fianza se otorga de acuerdo a lo establecido por el artículo 103 del Reglamento de la LOA (RLOA).', 1, 0, 'falta_de_entrega', 3),
(17, 2, 6, 'Transito Aduanero', 'Cubre todas las obligaciones que resulten a cargo del afianzado y a favor del Fisco Nacional, por la operación de tránsito aduanero que se efectúe entre una aduana y otra, de conformidad con lo regulado por el Art. 39 de la LOA.', 1, 0, 'transito_aduanero', 4),
(18, 2, 6, 'Valor de Cargamento', 'Garantiza el valor de los cargamentos y demás obligaciones, derivados del transporte por vía terrestre en vehículos suficientemente acondicionados, que efectúe el afianzado en su condición de empresa especializada del transporte multimodal o combinado, previamente registrada en el Ministerio de Hacienda, llegados a través de la aduana respectiva, en tránsito nacional con destino a otras oficinas aduaneras del país, habilitadas para la importación, conforme a la autorización dada por el referido ministerio, todo de conformidad con el Art. 87 del RLOA.', 1, 0, 'valor_de_cargamento', 5),
(19, 2, 6, 'Exportación Temporal', 'Responde de los impuestos de exportación y derechos arancelarios, relativos a la exportación temporal de cualquier equipo o mercancías que van a ser reparados o utilizados, según sea el caso, en el exterior y que luego retornarán al país, de conformidad al Art. 93 de la LOA.', 1, 0, 'exportacion_temporal', 6),
(24, 2, 6, 'Importación temporal', 'Respalda el pago de los impuestos de importación y derechos arancelarios, relativos a la importación temporal de equipos o mercancías, de conformidad con lo regulado por el Art.94 de la LOA.', 1, 0, 'importacion_temporal', 7),
(25, 2, 6, 'Perfeccionamiento activo', 'Cubre el pago de los impuestos de importación y derechos arancelarios, relativos a la importación temporal para perfeccionamiento activo de materias primas, de conformidad al citado Art. 94 de la LOA.\n<br>Existen muchos casos de fianzas para importación, que tomarán el nombre según la importación que se realice, como son los casos consagrados en los artículos 56 LOA en concordancia con los Arts. 174 y 178 RLOA., Art. 87, 89 letra “G” LOA.', 1, 0, 'perfeccionamiento-activo', 8),
(26, 2, 6, 'Agente Aduanal', 'Garantiza la actividad del agente aduanal, de acuerdo a los artículos 28 y 29 de la LOA y 131 de su Reglamento y la reciente Resolución No. 2170 del 03/03/93, publicada en la Gaceta Oficial No. 35.164 de fecha 04/03/93, para operar en la aduana respectiva.', 1, 0, 'agente_aduanal', 9),
(62, 2, 1, 'Requisitos', '<span><b>Del\nSolicitante:</b></span>\n<ol>\n	<li>\n<span>\n	Planilla\n	de solicitud debidamente completada.</span>\n	</li><li>\n<span>\n	Llenar\n	ficha identificación cliente.</span>\n	</li><li>\n<span>\n	Copia\n	estatutos.</span>\n	</li><li>\n<span>\n	Modificaciones\n	a&nbsp; estatutos, venta de acciones, cambio de razon social, etc.</span>\n	</li><li>\n<span>\n	Estados\n	financieros&nbsp; auditados&nbsp; de los tres últimos ejercicios\n	financieros (original), visados por el colegio de contadores\n	publicos.</span>\n	</li><li>\n<span>\n	Relacion\n	de obras ejecutadas y por ejecutar con monto de ellas, indicando\n	compañias que otorgaron las fianzas.</span>\n	</li><li>\n<span>\n	Dos\n	(2) referencias comerciales y Dos (2) bancarias.</span>\n	</li><li>\n<span>\n	copia\n	del R.I.F.</span>\n	</li><li>\n<span>\n	Copia\n	de la ultima declaración del impuesto sobre la renta (ISLR)</span>\n</li></ol>\n<span>\n<b>Del\nContragarante:</b></span><u>\n<br><br></u><span><u>\n</u><b>Persona\nNatural:</b></span><u><b>\n</b></u><ol><u><b>\n	</b></u><li>\n<span>\n	Copia\n	de la cedula de identidad.</span>\n	</li><li>\nCopia de la\n	cedula de identidad esposa(o).\n	</li><li>\n<span>\n	Copia\n	del poder otorgado por esta(e).</span>\n	</li><li>\n<span>\n	Estados\n	financieros actualizados (original), visado por el colegio de\n	contadores publicos con vigencia no superior a tres (03) meses.</span>\n	</li><li>\n<span>\n	Copia\n	de los documentos de inmuebles reflejados en los estados\n	financieros.</span>\n	</li><li>\n<span>\n	Dos\n	(2) referencias bancarias y dos (2) comerciales.</span>\n	</li><li>\n<span>\n	Copia\n	de la última declaración del impuesto sobre la renta (ISLR).</span>\n</li></ol>\n<span>\n<br>\n\n</span>', 1, 1, 'requisitos', 6),
(60, 2, 1, 'Anticipos de las fianzas de suministro', 'Los contratos de suministro (Orden de Compra), aunque son de corta duración, se requiere de grandes anticipos.', 1, 0, 'anticipos-de-las-fianzas-de-suministro', 4),
(61, 2, 1, 'Anticipos especiales', 'Los anticipos especiales, son anticipos no previstos en el contrato generalmente, que por alguna variación imprevista se hacen necesarios. En las condiciones generales de contratación para la ejecución de obras, (Art. 55).', 1, 0, 'anticipos-especiales', 5),
(59, 2, 1, 'Decreto de obras', 'En las condiciones generales de contratación para la ejecución de obras, se establece la obligatoriedad en las obras que contrata el estado, de que el contratista presente una fianza otorgada por un banco o una compañía de seguros.', 1, 0, 'decreto-de-obras', 3),
(57, 2, 1, 'Descripción', '<span>Garantiza que el AFIANZADO reintegrará la suma de dinero que le fue otorgada por adelantado por “EL ACREEDOR” para su inversión en la obra, servicio o suministro objeto de la Fianza.</span>', 1, 1, 'descripcion', 1),
(58, 2, 1, 'Vigencia', 'Comienza a regir a partir de la fecha en que el fiador reciba el aludido anticipo y permanecerá en vigencia hasta cuando este se haya reintegrado, mediante las deducciones del porcentaje establecido en el contrato.', 1, 1, 'vigencia', 2),
(31, 2, 6, 'Almacenes General de depósitos', 'Responden del pago de los impuestos suspendidos y penas pecuniarias, en que pueda incurrir el afianzado, relativos a los Manifiestos de Importación de las mercancías autorizadas por el Despacho respectivo para ser trasladadas hasta el Almacén General de Depósito de nuestro cliente. En el texto de la póliza se debe hacer mención al Art. 146 de la LOA, parágrafo único del Art. 4to. de la Ley de Almacenes Generales de Depósitos y Art. 12 del Reglamento de esta última.', 1, 0, 'almacenes_general_de_depositos', 10),
(32, 2, 6, 'Depósitos aduaneros', 'Cubre el pago de los impuestos del afianzado, en su calidad de Depósito Aduanero, de conformidad a lo pautado por el literal d) del artículo 377 del RLOA. Se define como Depósitos Aduaneros, el régimen mediante el cual, las mercancías extranjeras, nacionales o nacionalizadas, son depositadas en un lugar, bajo el control y potestad de la aduana, sin estar sujetas al pago de impuestos de importación ni tasas aduaneras, para su venta en los mercados internacionales y nacionales.', 1, 0, 'depositos_aduaneros', 11),
(33, 2, 6, 'Servicios Portuarios', 'Cubre las obligaciones del afianzado (impuestos, penas tasas, derechos de uso de las instalaciones portuarias y equipos, y demás tributos), en razón del ejercicio de las actividades y prestación de los servicios que haga y para los que esté autorizado por el acto de registro respectivo y el contrato de autorización suscrito con el acreedor.', 1, 0, 'servicios_portuarios', 12),
(34, 2, 6, 'Daños a terceros por la ley de la marina mercante', 'La presente garantía se aplica al cumplimiento de las obligaciones que puedan ser contraídas por la afianzada por daños causados a terceros; para dar cumplimiento a los artículos cuarto (4to.) de la Ley de Protección y Desarrollo de la Marina Mercante Nacional y Noveno (9o) de su Reglamento, según lo establece el Oficio Número DCRC02942 de fecha 02 de julio de 1.976, emanado de la Dirección de Transporte y Tránsito Marítimo.<br><br>Responde por las sanciones por infracciones de las Leyes o Reglamentos Fiscales en que puedan incurrir los Capitanes de los buques que, lleguen a los Puertos bajo la jurisdicción de la Aduana Marítima respectiva, y de los cuales sea agente en dicho puerto la afianzada, así como también de los impuestos, tasas y contribuciones que se causen; de conformidad con los artículos 13, 144 y 146 de la Ley Orgánica de Aduanas.', 1, 0, 'daros-a-terceros-por-la-ley-de-la-marina-mercante', 13),
(35, 2, 6, 'Requisitos', '<b>Del Solicitante:<br></b><ol><li>Planilla de solicitud debidamente completada.</li><li>Llenar ficha identificación cliente.\nCopia estatutos.</li><li>\nModificaciones a&nbsp; estatutos, venta de acciones, cambio de razón social, etc.</li><li>Estados financieros&nbsp; auditados&nbsp; de los tres últimos ejercicios financieros (original), visados por el colegio de contadores públicos.</li><li>Relación de obras ejecutadas y por ejecutar con monto de ellas, indicando compañías que otorgaron las fianzas.</li><li>Dos (2) referencias comerciales y Dos (2) bancarias.\ncopia del R.I.F.</li><li>\nCopia de la ultima declaración del impuesto sobre la renta (ISLR).</li></ol><br><b>Del Contragarante:\n<br></b><br><b>Persona Natural:\n<br></b><ol><li>Copia de la cédula de identidad.</li><li>Copia de la cédula de identidad esposa(o).</li><li>\nCopia del poder otorgado por esta(e).</li><li>Estados financieros actualizados (original), visado por el colegio de contadores públicos con vigencia no superior a tres (03) meses.</li><li>\nCopia de los documentos de inmuebles reflejados en los estados financieros.</li><li>Dos (2) referencias bancarias y dos (2) comerciales.</li><li>Copia de la última declaración del impuesto sobre la renta (ISLR).</li></ol>', 1, 1, 'requisitos', 14),
(36, 3, 7, 'Descripción', 'En Uniseguros nos inventamos una, es por ello que ponemos a tu disposición&nbsp;<b>Uniauto Inteligente</b>, un plan combinado en donde tú decides la cobertura de tu vehículo, ofreciéndole múltiples ventajas:<br><ul><li>El asegurado selecciona el monto de la cobertura de acuerdo a su capacidad de pago.\nRestitución y aumento de la suma asegurada en el momento que deseé el asegurado.</li><li>\nOpción de contratación con deducible para disminuir la prima.</li><li>\nEl asegurado decide la forma de indemnización por pérdida parcial: reembolso o reemplazo\nAbarca vehiculos de hasta 20 años de antigüedad.</li></ul><b>Desaparición o destruccion total.</b><br>Esta póliza indemniza la suma asegurada convenida entre las partes por una pérdida no recuperable de la estructura del vehículo, es decir, por su desaparición o su destrucción absoluta, por cualquiera de los siguientes hechos:<ul><li>Incendio.</li><li>Accidente.</li><li>Motín, disturbios populares, daños maliciosos.</li><li>desaparición a consecuencia de robo, hurto, asalto o atraco.</li></ul>', 1, 2, 'descripcion', 1),
(37, 3, 7, 'Cobertura de responsabilidad civil', 'Ofrece\nla indemnización a uno o varios terceros o externos que puedan verse\nafectados por el vehículo incluyendo daños a personas o cosas,\ncomplementándose con una variedad de servicios para una atractiva y\neficiente protección.', 1, 1, 'cobertura-de-responsabilidad-civil', 2),
(38, 3, 7, 'Cobertura de excesos de limites sobre R.C.V', 'Es\nuna ampliación del límite de la Cobertura Básica de\nResponsabilidad Civil obligatoria.', 1, 1, 'cobertura-de-excesos-de-limites-sobre-rcv', 3),
(77, 5, 16, 'Descripción', '<span>Esta\npóliza va dirigida a aquellos bienes y/u objetos los cuales dadas\nsus características especiales se encuentran excluidos del amparo en\notras pólizas.</span>\n<br><span>El\namparo de los mismos puede efectuarse mediante coberturas a Riesgos\nNombrados o a Todo Riesgo sujeto a exclusiones nombradas.</span>\n<br><span>Todo\nlo anterior sujeto a las condiciones y exclusiones especificadas en\npóliza.</span>', 1, 1, 'descripcirn', 1),
(75, 5, 14, 'Descripción', '<span><span>Ampara\nlas pérdidas pecuniarias que se produzcan como consecuencia directa\ndel&nbsp;</span><b>robo\nde los bienes muebles asegurados.</b>Se\npodrá contratar como cobertura adicional los riesgos de Asalto o\nAtraco.<br></span><span>Todo\nlo anterior sujeto a las condiciones y exclusiones especificadas en\npóliza.</span>', 1, 1, 'descripcirn', 1),
(76, 5, 15, 'Descripción', '<span><span>Ampara\nla pérdida de dinero y/o valores dentro y fuera de los predios del\nasegurado, causados directamente por robo, asalto o atraco.</span></span>\n<br><span>Todo\nlo anterior sujeto a las condiciones y exclusiones especificadas en\npóliza.</span>', 1, 1, 'descripcirn', 1),
(74, 5, 13, 'Descripción', '<span>Esta\ncobertura ampara la pérdida de utilidad ocasionada por un siniestro\namparado bajo la póliza de Incendio o Rotura de Maquinaria y el cual\nafecte la capacidad productiva de la industria o negocio asegurado.</span>\n<br><span>Todo\nlo anterior sujeto a las condiciones y exclusiones especificadas en\npóliza.</span>', 1, 1, 'descripcirn', 1),
(73, 5, 12, 'Coberturas adicionales', '<ul><li><span>Daños por\n	agua</span>\n	</li><li>\nInundaciones\n	</li><li>\nTerremoto o\n	temblor de tierra\n	</li><li>\nRotura de\n	vidrios y anuncios\n	</li><li>\nBienes\n	refrigerados\n	</li><li>\nMotines,\n	disturbios laborales, daños maliciosos\n	</li><li>\nPérdidas\n	Indirectas\n	</li><li>\nPérdida de\n	Rentas\n	</li><li>\nReconstrucción\n	de Archivos\n	</li><li>\nHonorarios\n	Profesionales (Arquitectos, topógrafos e Ingenieros)\n	</li><li>\nGastos por\n	demolición, remoción o limpieza de escombros\n</li></ul>\nTodo lo anterior\nsujeto a las condiciones y exclusiones especificadas en póliza.', 1, 1, 'coberturas-adicionales', 2),
(72, 5, 12, 'Descripción general', '<span>Esta póliza ofrece cobertura por  los daños materiales que pudiera\nsufrir el Asegurado en sus bienes muebles e inmuebles  por la acción\ndirecta e indirecta   de incendios, el término de incendio se\nextiende a las pérdidas y/o daños causados por rayo, explosiones,\nagua u otros agentes de extinción utilizados para apagar un\nincendio, en los predios ocupados por el asegurado o en predios\nadyacentes, El humo de un incendio originado en los predios ocupados\npor el Asegurado o en predios adyacentes, Impacto de aeronaves,\nsatélites, cohetes u otros aparatos aéreos o de los objetos\ndesprendidos de los mismos. Daños por agua, inundaciones,\nterremotos, motines, disturbios laborales, daños maliciosos, rotura\nde vidrios y anuncios, entre otros.</span>\n<span><br>\n\n</span>', 1, 1, 'descripcirn-general', 1),
(71, 4, 10, 'Condiciones de asegurabilidad', 'La edad máxima de permanencia de los hijos será de veintiséis (26) años y permanecerá en este seguro, hasta finalizar el año póliza en que haya cumplido esta edad. La edad de permanencia para los otros ASEGURADOS será hasta los sesenta y cinco (65) años de edad.', 1, 1, 'condiciones-de-asegurabilidad', 3),
(70, 4, 10, 'Personas asegurables', '<ol><li>El ASEGURADO Titular, el (la) cónyuge o la persona con quien haga\nvida marital, cuya edad al momento de la contratación de este\nseguro, sea menor a sesenta y cinco (65) años.</li><li>Los hijos legítimos o adoptados del ASEGURADO Titular o de su\ncónyuge, solteros y cuya edad al momento de la contratación de este\nseguro, sean menores de veintiséis (26) años.</li><li>\nPadres del ASEGURADO Titular, cuya edad al momento de la\ncontratación de este seguro, sea menor a sesenta y cinco (65) años.</li></ol>', 1, 1, 'personas-asegurables', 2),
(51, 3, 7, 'Cobertura de asistencia legal y defensa penal', '<span>Bajo\nesta cobertura se asiste y defiende legalmente al conductor del\nvehículo en caso de su detención, la del vehículo o ambos, a\nconsecuencia de accidentes de tránsito con lesionados o muertos y\nasume los gastos que de ello se deriven, incluyendo los de\nliberación.</span>', 1, 1, 'cobertura-de-asistencia-legal-y-defensa-penal', 4),
(67, 4, 9, 'Descripción general', '<b>Es un seguro de vida\ntemporal a un año renovable</b>, cuyo objetivo es indemnizar a los\nbeneficiarios designados por El Asegurado Titular o a sus herederos\nlegales la suma asegurada contratada por fallecimiento de éste por\ncausas naturales y/o accidentales.', 1, 1, 'descripcirn-general', 1),
(68, 4, 9, 'Uniapoyo', 'Atención Telefónica\na través del número telefónico 0500-UNISEGU (0500-8647348) las 24\nhoras del día y los 365 días del año.', 1, 0, 'uniapoyo', 2),
(69, 4, 10, 'Descripción general', 'Mediante este Seguro Funerario, <b>Uniseguros, S.A</b>. se compromete a cubrir los gastos incurridos a consecuencia de servicios prestados por la funeraria legalmente establecida, tales como: Oficios religiosos, servicios de capilla y cafetín, vehículos fúnebres para el traslado del fallecido, vehículos de acompañamiento de los familiares, preparación y arreglo del fallecido, ataúd, aviso de prensa, servicio de traslado de Caracas al interior o del interior a Caracas, realización de las diligencias legales, cremación o parcelas en el cementerio (Privado o Municipal) y cruz de flores u otros servicios prestados dentro de la funeraria, y hasta la suma asegurada contratada. La cobertura de la Parcela está referida a indemnizar el costo de ésta en un cementerio público o municipal, a escogencia del Asegurado, y no a la compra de la misma por parte de <b>Uniseguros, S.A</b>. para su posterior adjudicación, y hasta agotar el monto de la cobertura contratada. La Suma Asegurada contratada en este seguro será indemnizada a la funeraria hasta por el valor de los servicios prestados por ésta.<span id=\"_wysihtml5-undo\" class=\"_wysihtml5-temp\"></span>', 1, 1, 'descripcion-general', 1),
(66, 4, 8, 'Personas asegurables', '<ul><li>Titular,\n	cónyuge, padres e hijos.\n	</li><li>\nCónyuge:\n	menor de 65 años.\n	</li><li>\n<b>Padres:\n	menor de 65 años, deben depender económicamente de El Asegurado.</b>\n	</li><li>\nHijos:\n	menores de 18 años, solteros, edad de permanencia 25 años de edad,\n	siempre que se mantengan solteros y dependientes económicamente del\n	asegurado.\n</li></ul>\n<span><br>\n\n</span>', 1, 1, 'personas-asegurables', 4),
(56, 3, 7, 'Cobertura de accidentes personales para los ocupantes del vehículos terrestres(APOV)', 'Ampara\nal conductor y los ocupantes del vehículo asegurado, en caso de\nmuerte, invalidez permanente (total  o parcial), gastos médicos como\nconsecuencia de un accidente de tránsito.', 1, 1, 'cobertura-de-accidentes-personales-para-los-ocupantes-del-vehiculos-terrestres-apov', 5),
(99, 3, 7, 'Requisitos necesarios para la emisión automóvil', '<ol>\n	<li>\n<span>\n	<span>Planilla\n	de solicitud debidamente llena y firmada por el Contratante y por el\n	Intermediario.</span></span>\n	</li><li>\n<span>\n	Copia\n	de la cédula de identidad o RIF del contratante.</span>\n	</li><li>\n<span>\n	En\n	caso de ser vehículo nuevo: Copia de la factura de compra y del\n	certificado de origen del vehículo.</span>\n	</li><li>\n<span>\n	Vehículo\n	usado, nacional o importado: Título de propiedad y documento de\n	traspaso notariado. No se aceptan documentos notariados con más de\n	dos (2) endosos, bajo ningún concepto se aceptará como documento\n	de Traspaso, documento de opción a compra.</span>\n	</li><li>\n<span>\n	Vehículo\n	importado: Factura de compra, Certificado de origen del vehículo,\n	Documento de importación con el correspondiente sello del Resguardo\n	Nacional, Liquidación de Aranceles y el permiso de circulación\n	emitido por la Guardia Nacional.</span>\n</li></ol>', 1, 1, 'requisitos-necesarios-para-la-emision-automovil', 6),
(78, 5, 17, 'Descripción', '<div>Esta póliza ofrece cobertura para los siguientes Convenios de seguros:</div><div><br></div><div><b>Convenio I</b>: Deshonestidad de Empleados</div><div><br></div><div>Ampara la pérdida de dinero, valores y otras propiedades que afecten al Asegurado por actos de fraude, desfalco, falsificación o apropiación indebida y mala fè cometido por cualquier miembro del personal del asegurado</div><div><br></div><div><b>Convenio II</b>: Dinero y/o valores en local:</div><div><br></div><div>Ampara las pérdidas por destrucción o desaparición ilícita o como consecuencia de Robo, asalto o atraco mientras se encuentren dentro del local asegurado.</div><div><br></div><div><b>Convenio III</b>: Dinero y/o valores en tránsito:</div><div><br></div><div>Ampara las pérdidas por destrucción o desaparición ilícita o como consecuencia de Robo, asalto o atraco fuera del local del asegurado y mientras sean trasladados por sus empleados o mensajeros debidamente autorizados.</div><div><br></div><div><b>Convenio de Seguro IV</b>: Falsificación de giros postales y/o papel moneda:</div><div><br></div><div>Ampara las pérdidas causadas por la aceptación de buena fè por parte del asegurado de cualquier giro emitido o supuestamente emitido por cualquier oficina postal en pago de dinero, mercancía o servicios. Así como papel moneda falsificado.</div><div><br></div><div><b>Convenio de Seguro V</b>: Falsificación de depósitos y retiros bancarios:</div><div><br></div><div>Ampara las pérdidas sufridas como consecuencia de la falsificación o alteración de cualquier cheque, giro, pagaré, letra de cambio o documentos similar, orden de pago o instrucción para el pago de una suma determinada librado o girado por alguien que actúe como agente del Asegurado.</div><div><br></div><div>Todo lo anterior sujeto a las condiciones y exclusiones especificadas en póliza.</div><div><br></div>', 1, 1, 'descripcirn', 1),
(79, 5, 18, 'Descripción', '<div>Ampara los equipos contra pérdidas y/o daños físicos&nbsp; por causas accidentales externas, súbitas e imprevistas que ocurran en los predios indicados en pòliza&nbsp; .Coberturas adicionales. Huelga, motín y conmoción civil, Responsabilidad civil extracontractual&nbsp; y traslados terrestres.</div><div><br></div><div>Todo lo anterior sujeto a las condiciones y exclusiones especificadas en póliza.</div>', 1, 2, 'descripcion', 1),
(80, 5, 19, 'Descripción', 'Ampara\nla maquinaria fija (instalada contra todo riesgo de pérdida o daño\nfísico por cualquier causa accidental interna, eléctrica o\nmecánica, súbita o imprevista, dentro de los predios especificados.\nCoberturas\nadicionales.&nbsp;Huelga,\nmotín y conmoción civil\nResponsabilidad\ncivil extracontractual  solo para calderas\nLucro\ncesante para rotura de maquinaria.\n<br>Todo\nlo anterior sujeto a las condiciones y exclusiones especificadas en\npóliza.', 1, 2, 'descripcirn', 1),
(81, 5, 21, 'Descripción', '<div>Ampara la mercancía propiedad del asegurado y/o de terceros por la cual sea legalmente responsable mientras sea transportada a bordo de vehículos de carga propiedad del asegurado y/o transportistas terceros responsables&nbsp; &nbsp;contra pérdidas o daños materiales causados directa y accidentalmente por cualquiera de los siguientes riesgos que sufra el vehículo transportado:</div><div><br></div><ul><li>Choque, vuelco, embarrancamiento, descarrilamiento.</li></ul><ul><li>Incendio, rayo, explosión y huracán, ciclón y tornado.</li></ul><ul><li>Desplome de puentes, alcantarilla, muelles y plataformas.</li></ul><ul><li>Deslizamiento de tierra y por la creciente de aguas navegables.</li></ul><ul><li>Varadura, encalladura o hundimiento de embarcaciones lacustres, fluviales o marítimas de servicio regular.</li></ul><div>Coberturas adicionales: Robo, asalto, atraco, hurto simple, carga y descarga, huelga motín y conmoción civil, Falta de entrega por terceros responsables, daños a mercancía&nbsp; por paralización de la maquinaria de refrigeración a consecuencia de un riesgo cubierto en póliza.</div><div><br></div><div>Todo lo anterior sujeto a las condiciones y exclusiones especificadas en póliza.</div>', 1, 1, 'descripcirn', 1),
(82, 5, 20, 'Coberturas adicionales:', '<ul><li>Terremoto, huelga, motín,</li></ul><ul><li>Portadores externos de datos (costos para reponer la información dañada)</li></ul><ul><li>Gastos adicionales en que&nbsp; incurra el asegurado para arrendar otros equipos.</li></ul><ul><li>Todo lo anterior sujeto a las condiciones y exclusiones especificadas en póliza.</li></ul><div><br></div>', 1, 1, 'coberturas-adicionales', 1),
(83, 5, 22, 'Descripción', 'Ampara las\nimportaciones y/o exportaciones de mercancías por vía aérea y/o\nmarítima, su transporte conectante por vía terrestre en vehículos\nde carga y/o por vía férrea y su almacenaje temporal  en aduanas\naéreas y/o marítimas mientras se efectúa su proceso de Exportación\ny/o Nacionalización.<span id=\"_wysihtml5-undo\" class=\"_wysihtml5-temp\"></span>', 1, 1, 'descripcirn', 1),
(84, 5, 22, 'Coberturas', '<div>Cláusula de Carga del Instituto “A” ampara todo riesgo de pérdida y/o daño físico salvo lo expresamente excluido.</div><div><br></div><div>Cláusulas de carga del Instituto&nbsp; B y C: Ampara solo los riesgos nombrados en c/u de estas Coberturas adicionales:</div><ul><li>Guerra,</li></ul><ul><li>Huelga,</li></ul><ul><li>Motín,</li></ul><ul><li>Conmoción Civil y</li></ul><ul><li>Daños Maliciosos</li></ul><div>Todo lo anterior sujeto a las condiciones y exclusiones especificadas en póliza.</div>', 1, 1, 'coberturas', 2),
(85, 5, 23, 'Descripción', '<div>Ampara el casco y la maquinaria contra pérdida o daño físico&nbsp; por cualquier causa accidental externa&nbsp; incluyendo&nbsp; la Responsabilidad civil ante terceros( excluye carga, pasajeros y tripulación), mientras la misma se encuentre navegando, o fondeada en su varadero o&nbsp; estacionamiento.</div><div><br></div>', 1, 1, 'descripcion', 1),
(86, 5, 23, 'Cobertuars adicionales', '<ul><li><span><span>Traslados\n	terrestres</span></span>\n	</li><li>\n<span>\n	Accidentes\n	personales Pasajeros y</span>\n	</li><li>\n<span>\n	Tripulación</span>\n</li></ul>\n<span>\nTodo\nlo anterior sujeto a las condiciones y exclusiones especificadas en\npóliza.</span>', 1, 1, 'cobertuars-adicionales', 2),
(87, 5, 36, 'Descripción', '<span>Ampara\n la aeronave mientras esta se encuentre en vuelo, carreteo y tierra \ncontra pérdida o daño físico por cualquier causa accidental\nexterna y por cualquiera de las siguientes coberturas requeridas:</span>\n<span><br>\n\n</span>\n<ul>\n	<li>\nCasco\n	</li><li>\n\n	Responsabilidad Civil ante terceros\n	</li><li>\nResponsabilidad\n	civil ante pasajeros\n	</li><li>\n\n	Accidentes Personales pasajeros y tripulación\n	</li><li>\nGastos\n	médicos a pasajeros y tripulación\n</li></ul>', 1, 1, 'descripcirn', 1),
(88, 5, 36, 'Coberturas adicionales', '<ul><li>Riesgo\n	de Guerra, Huelga\n	</li><li>\nSeguro\n	de deducibles\n	</li><li>\nVuelos\n	</li><li>\nFerry\n	</li><li>\nCoberturas\n	en Pistas No Autorizadas\n</li></ul>\nTodo\nlo anterior sujeto a las condiciones y exclusiones especificadas en\npóliza', 1, 1, 'coberturas-adicionales', 2),
(89, 5, 37, 'Descripción', 'Esta\npóliza ampara la responsabilidad civil legal extracontractual del\nasegurado a consecuencia de  daños causados a  terceros (materiales\ny/o lesiones corporales) en forma negligente o imprudente  y que se\nderiven de las situaciones de riesgo propias del desarrollo de la\nactividad  declarada por el Asegurado  dentro del territorio nacional\nde la República Bolivariana de Venezuela.\n<span><br>\n\n</span>\nTodo\nlo anterior sujeto a las condiciones y exclusiones especificadas en\npóliza', 1, 1, 'descripcion', 1),
(90, 5, 39, 'Descripción', 'En este ramo se\nagrupan varias pólizas, seleccione en el menú lateral derecho el\ntipo de cobertura que desee información.<br>', 1, 1, 'descripcion', 1),
(91, 5, 39, 'Seguro de montaje', 'Póliza de carácter\ntemporal que puede ser prorrogada y ofrece una cobertura completa\npara los riesgos comprendidos en el “Montaje y/o Instalación” de\nmaquinarias, equipos, aparatos y estructuras metálicas dentro de los\npredios indicados en póliza.\n<br><span><br>\n\n</span>\n<b>Cobertura Básica:\n</b><span><br>\n\n</span>\nAMPARO “A”\nIncendio, explosión, rayo, caída de aeronaves, errores de montaje,\nimpericia descuido, actos malintencionados, caída de partes, robo\ncon violencia, cortocircuito, pruebas de resistencia u operación.\n<br><span><br>\n\n</span>\n<b>Coberturas\nAdicionales:\n</b><span><br><b>\n\n</b></span><b>\n</b><ul><b>\n	</b><li>\nAmparo “B”\n	Terremoto\n	</li><li>\nAmparo “C”\n	Ciclón, Huracán, tempestad, vientos, inundación\n	</li><li>\nAmparo “D”\n	Período de mantenimiento\n	</li><li>\nAmparo “E”\n	R.C. Extracontractual por daños a bienes de terceros\n	</li><li>\nAmparo “F”\n	R.C. Extracontractual por lesiones, incluyendo la muerte de terceros\n</li></ul>\n<b>Otras Coberturas:\n</b><span><br><b>\n\n</b></span><b>\n</b><ul><b>\n	</b><li>\nHonorarios de\n	arquitectos e ingenieros\n	</li><li>\nHuelga, Motín\n	y Conmoción Civil\n	</li><li>\nOtras\n	Propiedades adyacentes (OPA)\n	</li><li>\nRemoción de\n	escombros\n	</li><li>\nPruebas de\n	mantenimiento\n</li></ul>\nTodo lo anterior\nsujeto a las condiciones y exclusiones especificadas en póliza.\n<span><br>\n\n</span>', 1, 1, 'seguro-de-montaje', 5),
(92, 5, 39, 'Seguro de todo riesgo de construcción', '<span>Póliza de carácter\ntemporal que puede ser prorrogada y ampara las obras civiles durante\nsu período de construcción. También se extiende a cubrir las\nmaterias primas almacenadas en el lugar de la construcción,\nedificaciones provisionales y maquinaria de construcción.</span>\n<br><span><br>\n\n</span>\n<b>Cobertura Básica:\n</b><span><br>\n\n</span>\nAmparo “A”\n<span><br>\n\n</span>\nPérdidas y/o daños\nfísicos ocasionados por causa accidental externa, súbita e\nimprevista, con excepción de los casos expresamente excluidos en la\nmisma.\n<br><span><br>\n\n</span>\n<b>Coberturas\nAdicionales:\n</b><span><br><b>\n\n</b></span><b>\n</b><ul><b>\n	</b><li>\nAmparo “B”\n	Terremoto\n	</li><li>\nAmparo “C”\n	Ciclón, Huracán, tempestad, vientos, inundación\n	</li><li>\nAmparo “D”\n	Período de mantenimiento\n	</li><li>\nAmparo “E”\n	R.C. Extracontractual por daños a bienes de terceros\n	</li><li>\nAmparo “F”\n	R.C. Extracontractual por lesiones, incluyendo la muerte de terceros</li></ul>\n<b>Otras Coberturas:\n</b><span><br><b>\n\n</b></span><b>\n</b><ul><b>\n	</b><li>\nHonorarios de\n	arquitectos e ingenieros\n	</li><li>\nHuelga, Motín\n	y Conmoción Civil\n	</li><li>\nOtras\n	Propiedades adyacentes (OPA)\n	</li><li>\nRemoción de\n	escombros\n	</li><li>\nPruebas de\n	mantenimiento\n</li></ul>\nTodo lo anterior\nsujeto a las condiciones y exclusiones especificadas en póliza.\n<span><br>\n\n</span>', 1, 1, 'seguro-de-todo-riesgo-de-construccion', 2),
(93, 5, 20, 'Descripción', '<span>Ampara\nlos equipos fijos y en operación contra cualquier causa accidental\ninterna y/o externa súbita e imprevista dentro de los predios\nindicados en póliza.<br><br></span><div><b>Coberturas adicionales:</b></div><ul><li>Terremoto, huelga, motín.</li></ul><ul><li>Portadores externos de datos (costos para reponer la información dañada).</li></ul><ul><li>Gastos adicionales en que&nbsp; incurra el asegurado para arrendar otros equipos.</li></ul><div>Todo lo anterior sujeto a las condiciones y exclusiones especificadas en póliza.</div>', 1, 2, 'descripcirn', 1),
(97, 5, 12, 'Qué es esta prueba de slug', 'Prueba de slug', 1, 2, 'que-es-esta-prueba-de-slug', 1),
(98, 3, 7, 'Descripción', '<b><span>Desaparición o destrucción total.<u><br></u><br></span></b>Esta\npóliza indemniza la suma asegurada convenida entre las partes por\nuna pérdida no recuperable de la estructura del vehículo, es decir,\npor su desaparición o su destrucción absoluta, por cualquiera de\nlos siguientes hechos:\n<ul>\n	<li>\nIncendio.\n	</li><li>\nAccidente.\n	</li><li>\nMotín,\n	disturbios populares, daños maliciosos.\n	</li><li>\ndesaparición\n	a consecuencia de robo, hurto, asalto o atraco.\n</li></ul>', 1, 1, 'descripcion', 1),
(100, 3, 7, 'Pasos a seguir para la emisión de automóvil', '<ol>\n	<li>\n<span>\n	<span>El\n	cliente, Intermediario o relacionado, que manifieste su deseo de\n	suscribir una póliza de automóvil, debe dirigirse a la sucursal\n	correspondiente, entregar la solicitud y los requisitos exigidos por\n	la empresa antes mencionados al personal autorizado para tal fin.</span></span>\n	</li><li>\n<span>\n	La\n	persona autorizada por la empresa (Perito) realizara la inspección\n	del vehículo, con el fin de constatar el perfecto estado y buen\n	funcionamiento del mismo.</span>\n	</li><li>\n<span>\n	Al\n	recibir la solicitud y la inspección, el analista verificara que se\n	encuentre completa; colocará el sello de recibido y procederá con\n	la emisión de la póliza, de acuerdo a las condiciones estipuladas.</span>\n</li></ol>', 1, 1, 'pasos-a-seguir-para-la-emision-de-automovil', 7),
(101, 3, 7, 'Pasos a seguir en caso de siniestro', '<span>La\nDeclaración de Siniestro es un requisito para continuar con la\natención de su reclamo. Esta debe realizarse dentro del plazo\nestablecido en su póliza.<br><br></span>\n<span>\nEl\ncliente debe dirigirse a las oficinas de Uniseguros o notificar por\nteléfono o correo electrónico la ocurrencia del  accidente, o a\ntravés de su intermediario de seguros.<br><br></span><span>Formalizar\nel siniestro según las indicaciones recibidas en el momento de la\ndeclaración.<br><br></span>\n<span>\n<span>Deberá\nLlenar y firmar el formulario Declaración de Siniestro en formato de\n<b>UNISEGUROS</b>.<br></span></span>\n<span>\n<br>El\najustador de daños designado por&nbsp;</span><b>UNISEGUROS</b>&nbsp;realizara la inspección\nde los daños para procesar el reclamo.', 1, 1, 'pasos-a-seguir-en-caso-de-siniestro', 8),
(102, 5, 39, 'Rotura de maquinaria', '<span>Ampara\nla maquinaria fija (instalada contra todo riesgo de pérdida o daño\nfísico por cualquier causa accidental interna, eléctrica o\nmecánica, súbita o imprevista, dentro de los predios especificados.</span>\n<br><span><br>\n\n</span>\n<b>Coberturas\nadicionales:\n<br></b><span><br>\n\n</span>\nHuelga,\nmotín y conmoción civil\nResponsabilidad\ncivil extracontractual  solo para calderas\nLucro\ncesante para rotura de maquinaria.\nTodo\nlo anterior sujeto a las condiciones y exclusiones especificadas en\npóliza.\n<span><br>\n\n</span>', 1, 1, 'rotura-de-maquinaria', 4),
(103, 5, 39, 'Equipos electrónicos', '<span>Ampara\nlos equipos fijos y en operación contra cualquier causa accidental\ninterna y/o externa súbita e imprevista dentro de los predios\nindicados en póliza.<br></span>\n<span>\n<i><b><br></b></i><b>Coberturas\nadicionales:</b></span>\n<ul>\n	<li>\n<span>\n	Terremoto,\n	huelga, motín,</span>\n	</li><li>\n<span>\n	Portadores\n	externos de datos (costos para reponer la información dañada)</span>\n	</li><li>\n<span>\n	Gastos\n	adicionales en que&nbsp; incurra el asegurado para arrendar otros\n	equipos.</span>\n</li></ul>\n<span>\nTodo\nlo anterior sujeto a las condiciones y exclusiones especificadas en\npóliza.</span>', 1, 1, 'equipos-electronicos', 6),
(104, 5, 39, 'Equipos de contratista', '<span>Ampara\nlos equipos contra pérdidas y/o daños físicos  por causas\naccidentales externas, súbitas e imprevistas que ocurran en los\npredios indicados en póliza  .Coberturas adicionales. Huelga, motín\ny conmoción civil, Responsabilidad civil extracontractual  y\ntraslados terrestres.</span>\n<br><span><br>\n\n</span>\nTodo\nlo anterior sujeto a las condiciones y exclusiones especificadas en\npóliza.', 1, 1, 'equipos-de-contratista', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_productos_imag`
--

CREATE TABLE `detalle_productos_imag` (
  `id` int(11) NOT NULL,
  `id_det_prod` int(11) NOT NULL,
  `id_imagen` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_servicios`
--

CREATE TABLE `detalle_servicios` (
  `id` int(11) NOT NULL,
  `id_servicios` int(11) NOT NULL,
  `id_tipo_servicio` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `descripcion` text NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `estatus` int(11) NOT NULL DEFAULT '1',
  `slug` varchar(300) NOT NULL,
  `orden` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `detalle_servicios`
--

INSERT INTO `detalle_servicios` (`id`, `id_servicios`, `id_tipo_servicio`, `titulo`, `descripcion`, `id_idioma`, `estatus`, `slug`, `orden`) VALUES
(1, 2, 2, 'Pasos a seguir en caso de un siniestro:', '<div>1.El cliente debe dirigirse a las oficinas de uniseguros a Notificar, en un plazo no mayor de 5 días hábiles desde la ocurrencia del accidente.</div><div><br></div><div>2.Deberá Llenar y firmar el formulario Declaración de Siniestro en formato de UNISEGUROS.</div><div><br></div><div>3.El ajustador de daños designado por Uniseguros realizara la inspección de los daños para procesar el reclamo.</div><div><br></div><div>4.<span>Una vez recibidos todos los recaudos e inspeccionado el vehículo, La Compañía emitirá Orden de Reparación. En los casos de Rotura de Vidrio y daños menores que no impliquen sustitución de repuestos, la emisión de la Orden de Reparación se tramita en un lapso no mayor de 24 horas.</span></div>', 1, 1, 'pasos-a-seguir-en-caso-de-un-siniestro', 1),
(3, 2, 2, 'Recaudos en caso de un siniestro', 'Requisitos necesarios para la emisión Automóvil:<br><br><ol><li>Planilla de solicitud debidamente llena y firmada por el Contratante\n	y por el Intermediario.</li><li>Copia de la cédula de identidad o RIF del contratante.</li><li>En caso de ser vehículo nuevo: Copia de la factura de compra y del\n	certificado de origen del vehículo.</li><li>Vehículo usado, nacional o importado: Título de propiedad y documento de traspaso notariado. No se aceptan documentos notariados con más de dos (2) endosos, bajo ningún concepto se aceptará como documento de Traspaso, documento de opción a compra.</li><li>Vehículo importado: Factura de compra, Certificado de origen del vehículo, Documento de importación con el correspondiente sello del Resguardo Nacional, Liquidación de Aranceles y el permiso de circulación emitido por la Guardia Nacional.</li></ol>Pasos a seguir para la emisión de Automóvil:<br><ol><li>El cliente, Intermediario o relacionado, que manifieste su deseo de suscribir una póliza de automóvil, debe dirigirse a la sucursal correspondiente, entregar la solicitud y los requisitos exigidos por la empresa antes mencionados al personal autorizado para tal fin.</li><li>La persona autorizada por la empresa (Perito) realizara la inspección del vehículo, con el fin de constatar el perfecto estado y buen funcionamiento del mismo.</li><li>Al recibir la solicitud y la inspección, el analista verificara que se encuentre completa; colocará el sello de recibido y procederá con la emisión de la póliza, de acuerdo a las condiciones estipuladas.</li></ol>', 1, 1, 'recaudos-en-caso-de-un-siniestro', 2),
(4, 2, 3, 'Pasos a seguir en caso de un siniestro', '<ol><li>Dirigirse directamente a su productor, corredor y/o sociedad de\n	corretaje de seguros. En caso de no poseer intermediario de seguros,\n	deberá realizar la notificación directamente a la ASEGURADORA.\n	</li><li>\n\n	Deberá presentar la notificación de la ocurrencia del siniestro\n	por escrito inmediatamente o a más tardar dentro de los cinco (05)\n	días hábiles siguientes a la fecha de haber tenido conocimiento\n	del siniestro.\n	</li><li>Si posee otra póliza de seguros en otra compañía, deberá indicar la existencia de la misma y los riesgos cubiertos por dicha póliza.</li><li>Dentro de los treinta (30) días siguientes a la fecha de la hospitalización y/o intervención quirúrgica , o fallecimiento del ASEGURADO, deberá presentar en original y copia los siguientes recaudos.<br></li></ol>', 1, 1, 'pasos-a-seguir-en-caso-de-un-siniestro', 1),
(5, 2, 3, 'Recaudos en caso de siniestro', '<b>Reembolso\n</b><ul><b>\n	</b><li>\n\n	Original y 2 copias del informe médico amplio y detallado con\n	diagnóstico definitivo del médico tratante. (menor a 6 meses de\n	emisión)\n	</li><li>\n\n	Original y 2 copias de los récipes y facturas de medicina (con la\n	respectivas especificaciones del seniat)\n	</li><li>\n\n	Original y 2 copias de los resultados de los exámenes y/o estudios\n	practicados (informe de Rx, informe de T.A.C. , laboratorios e\n	informe de ecosonograma)\n	</li><li>Original y 2 copias de la declaración de siniestros de HCM llena en todas sus partes.</li></ul><b>En caso de Muerte Natural</b><ul></ul><ul><li>Planilla de declaración de siniestro llena en todas sus partes y debidamente firmada por los BENEFICIARIOS.</li><li>C.I. del asegurado y titular.</li><li>Acta de defunción de ASEGURADO</li><li>Declaración y/o informe del médico que atendió al ASEGURADO</li><li>Certificación de la medicatura forense ( si fuese el caso): “certificación de defunción” en la que conste la causa de muerte y el n° de CI. Con la que fue identificado el cuerpo.</li><li>Declaración de herederos legales, en caso de que no hubiere BENEFICIARIOS designados.</li><li>Autorización del juez del tribunal del niño, niña y del adolescentes, nombrando a la persona que deberá retirar la prestación correspondiente, cuando los BENEFICIARIOS sean menores de edad.</li><li>Registro de nacimiento o documento(s) de identidad de BENEFICIARIO(S) o heredero(s) legal(es).</li></ul><b>En caso de Muerte Accidental</b><ul></ul><b></b><ul><li>Todos los documentos señalados en caso de Muerte Natural.</li><li>Carta narrativa de cómo se produjo el siniestro, indicando lugar y hora.</li><li>Informe de la autoridad competente que intervino en el accidente (si fuese el caso).</li><li>Informe médico amplio y detallado.</li><li>En los casos de desaparición del ASEGURADO, se aplicará lo dispuesto en el Libro Primero “De las Personas, Título XII de los no presentes y de los ausentes, del Código Civil de la República Bolivariana de Venezuela”.</li></ul><b>En caso de Invalidez Permanente<br></b><ul><li>Informe del médico tratante, en el cual conste el grado de invalidez o incapacidad y duración de la incapacidad del asegurado.<br></li></ul><b>Documentación Adicional<br></b><ul><li>En los casos en que la ASEGURADORA requiera alguna documentación adicional para la evaluación del siniestro, podrá solicitarlos por escrito y por una (01) sola vez siempre que dicha solicitud se efectúe en un máximo de quince (15) días hábiles siguientes a la fecha en que se entregó, a satisfacción de la ASEGURADORA, el último de los documentos requeridos en los párrafos anteriores de este artículo. En este caso, se establece un plazo de diez (10) días hábiles para la presentación de los recaudos solicitados por la ASEGURADORA contados a partir de la fecha de solicitud de los mismos.</li><li>En caso tal de que los gastos hayan incurridos en el exterior, toda la documentación a presentar, deberá ser autenticada en el Consulado de la República Bolivariana de Venezuela que se encuentre en el país donde se originó el gasto, y, el pago de los servicios causados será reembolsado en moneda de curso legal y considerando el cambio oficial de la fecha del pago.</li></ul><ul></ul><b></b><br><ul></ul><b></b><br>', 1, 1, 'recaudos-en-caso-de-siniestro', 2),
(6, 2, 4, 'Pasos a seguir en caso de un siniestro', '<ol><li>El cliente debe dirigirse a las oficinas de Uniseguros a Notificar, en un plazo no mayor de 5 días hábiles de haber tenido conocimiento de la ocurrencia del accidente.</li><li>Deberá entregar carta de notificación de siniestro contentiva de una breve narrativa de los hechos, nombre y números telefónicos de la persona contacto en caso de requerirse ajustador.</li><li>Si el caso lo requiere se solicita la participación de un ajustador de pérdida, quién se comunicará con el asegurado para pactar una visitas al riesgo.</li><li>Si el caso no requiere ajustador, Uniseguros emite carta de solicitud de recaudos.</li><li>Una vez recibidos todos los recaudos por parte del asegurado y/o el ajuste final por parte de ajustador de pérdidas, se procede al análisis del siniestro, lo que dará origen a la indemnización o al rechazo según el caso.</li></ol>', 1, 1, 'pasos-a-seguir-en-caso-de-un-siniestro', 1),
(7, 2, 4, 'Recaudos en caso de siniestros', 'Para la notificación de todos\nlos siniestros Patrimoniales:\n\nLa notificación de siniestros,\nsalvo pacto en contrario, debe efectuarse dentro de los cinco (5)\ndías siguientes de haber tenido conocimiento de la ocurrencia del\nsiniestro y cada notificación deberá contener los siguientes datos\nbásicos:\n<span>\n<br>\n\n</span>\n<ul>\n	<li>\n\n	Nombre del asegurado.\n	</li><li>\n\n	Ramo y número de póliza\n	afectada.\n	</li><li>Dirección de ocurrencia del evento.</li><li>Fecha de ocurrencia.</li><li>Tipo de siniestro.</li><li>Monto estimado de la pérdida.</li><li>Número telefónico y persona contacto.</li><li>Breve narración de los hechos.</li></ul>', 1, 1, 'recaudos-en-caso-de-siniestros', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_tipo_negocio`
--

CREATE TABLE `detalle_tipo_negocio` (
  `id` int(11) NOT NULL,
  `titulo` text NOT NULL,
  `descripcion1` text NOT NULL,
  `descripcion2` text NOT NULL,
  `descripcion3` text NOT NULL,
  `id_tipo_negocio` int(11) NOT NULL,
  `id_imagen` int(50) NOT NULL,
  `estatus` int(11) NOT NULL,
  `slug` text NOT NULL,
  `folleto` text NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `posicion` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `detalle_tipo_negocio`
--

INSERT INTO `detalle_tipo_negocio` (`id`, `titulo`, `descripcion1`, `descripcion2`, `descripcion3`, `id_tipo_negocio`, `id_imagen`, `estatus`, `slug`, `folleto`, `id_idioma`, `posicion`) VALUES
(3, 'SAMF', '<p>Es una plataforma de gestión de identidad ultra segura, de nueva generación , orientada al sector financiero. Autentica y gestiona claves estáticas y dinámicas, posee facilidades para autenticación positiva, es fácil de integrar con cualquier canal electrónico de atención de clientes del banco en un ambiente SOA. </p><p>Aplicado correctamente, SAMF fortalece la autenticación a través de los distintos canales tales como banca en línea, banca móvil, cajeros automáticos,&nbsp; Pagos Móviles P2P y P2B,&nbsp; call center y otro tipo de canales.&nbsp; SAMF es una solución de autenticación multicanal. Esta permite implementar mecanismos avanzados de&nbsp; autenticación usando múltiples factores de forma centralizada. </p><p>Permite la creación y administración de las políticas de autenticación de cada canal a través de una sola interfaz unificada de administración. Facilita la administración de perfiles y usuarios del sistema, incluye una poderosa herramienta forensica con registro, almacenamiento y visualización de logs de auditoria. </p>', '<h4 style=\"text-align: center;\"><br></h4><h4><ul style=\"color: rgb(118, 118, 118); font-size: 14px;\"><li><sup>Total cumplimiento de Resolución 641-10 en la República Bolivariana de Venezuela.</sup><br></li><li><sup>Administración centralizada del proceso de autenticación </sup></li><li><sup>Comunicación segura entre SAMF y Canales Fácil integración con canales electrónicos de atención al cliente</sup><br></li><li><sup>Escalabilidad ilimitada</sup><br></li><li><sup>Rápida adaptación a cambios de normativas y políticas</sup><br></li><li><sup>Poderosa herramienta de auditoria y forensica</sup></li></ul></h4>', '<h4 style=\"text-align: center;\"><br></h4>', 2, 24, 1, 'samf', '../administrador/site_media/images/archivos/folletos/folleto_SAMF.pdf', 1, 1),
(4, 'SERVICIOS DE SEGURIDAD ADMINISTRADA', '<h4><span style=\"text-align: justify;\"><sup>IT Security Solutions a través de su gerencia de Servicios Profesionales pone a su disposición un avanzado servicio de Seguridad Administrada con la finalidad de Administrar y Monitorear en tiempo real 7x24x365 su infraestructura de Telecomunicaciones y Seguridad.</sup></span><br></h4><br>', '<h4><strong>LOS SERVICIOS DE SEGURIDAD ADMINISTRADA PUEDEN SER EN LAS ÁREAS DE:&nbsp;</strong></h4><p style=\"text-align: justify; \">Monitoreo de Disponibilidad Administración y Monitoreo de Seguridad Detección de Vulnerabilidades</p><br>', '<h4 style=\"text-align: justify;\"><sup>Con este servicio, nuestros clientes se garantizan que su infraestructura va a ser monitoreada de forma pro-activa permitiéndoles enfocar sus esfuerzos a su ámbito de negocio.&nbsp;<br></sup></h4><h4 style=\"text-align: justify;\"><sup>Este servicio está especialmente diseñado para organizaciones que carecen de recursos humanos con experiencia y/o capacidades de monitoreo y seguridad, no disponen de suficiente personal, o no desean realizar gastos en soluciones de seguridad, bajo esta última premisa, ponemos a su disposición el servicio de Servicio Administrado de Seguridad Total, mediante el cual IT Security Solutions instala en las facilidades del cliente los sistemas necesarios los cuales contaran con pólizas de soporte post-venta por un canon mensual.</sup></h4><br>', 3, 19, 1, 'servicios-de-seguridad-administrada', '', 1, 0),
(5, 'SATM', '<blockquote><h4><span style=\"text-align: justify;\">Es una solución de monitoreo de seguridad proactiva para redes de cajeros automáticos, ayuda a mitigar el fraude electrónico, monitorea los ATM en forma permanente y alerta ante la presencia de actividades sospechosas.&nbsp;</span><br></h4></blockquote><blockquote><h4>SATM consiste de tres elemento básicos: OSSIM, S-Server, S-Agente</h4></blockquote><br>', '<p><h4 style=\"text-align: center;\"><strong>FUNCIONALIDADES</strong></h4><ul><li style=\"text-align: justify;\">Administración Centralizada&nbsp;<br></li></ul></p><p><ul><li style=\"text-align: justify;\">Monitoreo en Tiempo Real (HW y SW del ATM)<br></li><li style=\"text-align: justify;\">Recopilación de Logs de Auditoria<br></li><li style=\"text-align: justify;\">&nbsp;Auto Protección de Agente del ATM&nbsp;<br></li><li style=\"text-align: justify;\">Análisis de Vulnerabilidades Herramientas Forenses&nbsp;<br></li><li style=\"text-align: justify;\">Cumplimiento Normas PCI<br></li></ul></p>', '<h4 style=\"text-align: center;\"><span style=\"text-align: justify;\"><strong>Características Clave del SATM&nbsp;</strong></span></h4><p style=\"text-align: center; \"></p><ul><li style=\"text-align: left;\"><span style=\"text-align: justify;\">Acceso seguro vía HTTPS&nbsp;</span><br></li><li style=\"text-align: left;\"><span style=\"text-align: justify;\">Administración de Usuarios y Perfiles</span><br></li><li style=\"text-align: left;\"><span style=\"text-align: justify;\">Logs de actividad de usuarios</span><br></li><li style=\"text-align: left;\"><span style=\"text-align: justify;\">Análisis de Vulnerabilidades de los ATM</span><br></li><li style=\"text-align: left;\"><span style=\"text-align: justify;\">Visor de Disponibilidad de ATM y Servicios Configuración de Acciones ante la presencia de actividad sospechosa&nbsp;</span><br></li><li style=\"text-align: left;\"><span style=\"text-align: justify;\">Visor de Alarmas, Trafico, Riesgo y Eventos&nbsp;</span><br></li><li style=\"text-align: left;\"><span style=\"text-align: justify;\">Reportes Forenses Edición de Directivas de Correlación Dashboard del Sistema</span><br></li></ul><p></p><br>', 2, 25, 1, 'satm', '../administrador/site_media/images/archivos/folletos/folleto_SATM.pdf', 1, 4),
(6, 'SOPORTE POST-VENTA Y ASISTENCIA', '<h4 style=\"text-align: left;\"><span style=\"text-align: justify; color: rgb(118, 118, 118); font-size: 14px; line-height: 23px;\">A través de nuestra Gerencia de Servicios Profesionales ponemos a su disposición servicios de soporte y asistencia técnica al software desarrollado por nuestra empresa, y a productos de terceras partes comercializados que incluyen soporte técnico de software, hardware, mantenimiento de equipos y reemplazo de equipos (RMA).</span><br></h4><p><strong>Características Clave del Servicio de Asistencia Técnica &nbsp;</strong> </p><ul><li>Soporte ilimitado a incidentes vía teléfono, email y website &nbsp; </li><li>El Cliente puede designar hasta 2 personas (coordinadores de soporte) que pueden contactar a nuestro personal para soporte y asistencia durante la vigencia del contrato, estos serán dotados de una cuenta para acceso al SAT – Sistema de Asistencia Técnica</li><li>Actualizaciones y mejoras para todos los productos comercializados por nuestra organización y que son de libre uso, es decir, que sean sin costo alguno o que estén amparados por contratos de mantenimiento del fabricante &nbsp;</li><li>Soporte remoto para todos los productos comercializados por nuestra organización. &nbsp;&nbsp;</li></ul><br>', '<p>Ofrecemos polizas de soporte y asistencia tecnica en las siguientes modalidades:</p><ul><li>Standard 8x5&nbsp;<br></li><li>Standard 12x7&nbsp;<br></li><li>Premium 12x7&nbsp;<br></li><li>Premium 24x7<br></li></ul><br>', '<br>', 3, 23, 1, 'soporte-post-venta-y-asistencia', '', 1, 0),
(7, 'DESARROLLO DE SOFTWARE', '<p><span style=\"text-align: justify;\">Contamos con un equipo de desarrollo especializado en aplicaciones de seguridad, web y de pago electrónico que posee las habilidades para crear software de calidad colocando a la seguridad siempre como prioridad.</span><br></p><p>Nuestro departamento de Desarrollo IT cuenta con experiencia en Servicios Web seguros, aplicaciones de autenticación robusta, criptografía avanzada, aplicaciones móviles blindadas y todo un conjunto de soluciones con las cuales puedes complementar tus aplicaciones actuales o crear toda una red de aplicaciones y telecomunicaciones segura para tu empresa. &nbsp; Toda empresa requiere seguridad a nivel de infraestructura y software utilizado y en nuestro departamento de Desarrollo IT encontrarás el aliado perfecto para conseguirlo. Además, ofrecemos soporte post producción y acompañamiento en todo el proceso de implementación de nuestros productos complementado con soporte 24/7 con especialistas dedicados a atender las solicitudes favoreciendo los tiempos de respuesta y calidad lograda.</p>', '<p><span style=\"text-align: justify;\">Todos nuestros desarrollos son realizados bajo las mejores practicas internacionales, y cada trabajo es gerenciado por un gerente de proyectos que asegura la calidad y tiempo acordado &nbsp;</span></p>', '<p><br></p><br>', 3, 18, 1, 'desarrollo-de-software', '', 1, 0),
(8, 'SERVICIOS DE CONSULTORÍA', '<p style=\"text-align: justify; \">Con la experiencia ganada a lo largo de nuestra trayectoria hemos logrado un elevado nivel de conocimientos en las áreas de seguridad, telecomunicaciones y servicios, la cual ponemos a disposición de nuestros clientes a través de nuestros Servicios de Consultoría</p><p style=\"text-align: justify; \">Contamos con profesionales debidamente entrenados, de amplia experiencia, en las áreas de arquitecturas de seguridad y comunicaciones, sistemas operativos, análisis de vulnerabilidades, análisis y administración de riesgo, monitoreo de seguridad, definición e implementación de políticas de seguridad informática y capacitación, todo ello con la finalidad de proteger los activos digitales de nuestros apreciados clientes. &nbsp;&nbsp;</p><p style=\"text-align: justify; \">Tenemos asociaciones estratégicas con las principales empresas internacionales fabricantes de software y hardware de seguridad e inteligencia informática, y nuestros ingenieros se encuentran constantemente explorando, evaluando y analizando nuevos productos, a fin de lograr asociaciones que le garanticen a nuestros clientes que tienen a su disposición productos y servicios de clase mundial.</p>', '<blockquote><p>Garantizamos total confidencialidad a nuestros clientes en cuanto al diseño, recomendaciones, e implantación de las soluciones de seguridad y comunicaciones.&nbsp;</p></blockquote><br>', '<p><br></p>', 3, 20, 1, 'servicios-de-consultoria', '', 1, 0),
(9, 'Desarrollo', 'Contamos con un área especializada en el desarrollo de soluciones de seguridad IT y financiera, en ella trabajamos a diario para ayudar a la banca a satisfacer las necesidades de sus clientes, acercándolos a ellos gracias a la tecnología. Nuestro portafolio incluye software de monitoreo de seguridad, generadores de tokens para smartphones y sistemas de pago P2P y P2B.', 'Contamos con un área especializada en el desarrollo de soluciones de seguridad IT y financiera, en ella trabajamos a diario para ayudar a la banca a satisfacer las necesidades de sus clientes, acercándolos a ellos gracias a la tecnología. Nuestro portafolio incluye software de monitoreo de seguridad, generadores de tokens para smartphones y sistemas de pago P2P y P2B.', 'Contamos con un área especializada en el desarrollo de soluciones de seguridad IT y financiera, en ella trabajamos a diario para ayudar a la banca a satisfacer las necesidades de sus clientes, acercándolos a ellos gracias a la tecnología. Nuestro portafolio incluye software de monitoreo de seguridad, generadores de tokens para smartphones y sistemas de pago P2P y P2B.', 2, 43, 1, 'desarrollo', '../administrador/site_media/images/archivos/folletos/folleto_Desarrollo.pdf', 1, 0),
(10, 'SAMF Pago Móvil P2P Interbancario', '<p class=\"MsoNormal\"><span lang=\"ES-VE\" style=\"font-size: 10.5pt; line-height: 107%; font-family: &quot;Open Sans&quot;; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Solución\nque</span>&nbsp;<span lang=\"ES\" style=\"font-size: 10.5pt; line-height: 107%; font-family: &quot;Open Sans&quot;; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">permite realizar transacciones financieras&nbsp;de pagos P2P Persona a\nPersona haciendo uso de teléfonos móviles inteligentes. Facilita pagos de\nproductos y servicios, y transferencia de dinero de persona a persona en\ncuestión de segundos, única aplicación del mercado que incorpora seguridad a través\nde claves dinámicas. Disponible para sistemas Android y IOS.</span><span lang=\"ES\"><o:p></o:p></span></p>', '<p class=\"MsoNormal\" style=\"margin-bottom:0cm;margin-bottom:.0001pt;line-height:\nnormal\"><ul><li>Soporta autenticación con clava Estática y Dinámica (OTP)<br></li><li>Integrable a cualquier Switch Transaccional<br></li><li>Aplicación móvil nativa para IOS y Android<br></li><li>Cumple normas 641.10 de SUDEBAN<br></li><li>Pago rápido mediante código QR cifrado<br></li></ul></p>\n\n\n\n\n\n', '<p><span style=\"font-family: &quot;Open Sans&quot;;\">SAMF Pago Móvil P2P Interbancario se adapta a los requerimientos e imagen corporativa de la institución bancaria, cumple al 100% con la normativa 641.10 de SUDEBAN y a los requerimientos de ASOBANCA.</span></p>', 2, 43, 1, 'samf-pago-movil-p2p-interbancario', '../administrador/site_media/images/archivos/folletos/folleto_SAMF_Pago Móvil P2P Interbancario.pdf', 1, 3),
(11, 'SAMF Token Móvil', '<p class=\"MsoNormal\" style=\"margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 10.5pt; font-family: &quot;Open Sans&quot;;\">Generador de Tokens (OTPs) para dispositivos móviles\nAndroid y IOS que se integra al sistema de gestión de identidad SAMF para\nasegurar el ingreso a portales de Internet Bankng y para asegurar transacciones\nfinancieras mediante un proceso único de aseguramiento de transacciones\nmediante OTP compuestas que evita ataques de hombre en el medio\n(Man-in-the-middle) y hombre en el Browser (Man-in-the-Browser).<o:p></o:p></span></p>\n\n<p class=\"MsoNormal\" style=\"margin-bottom: 7.5pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 10.5pt; font-family: &quot;Open Sans&quot;;\">Se adapta a la imagen corporativa de la institución\nbancaria.<o:p></o:p></span></p>', '<p style=\"margin-bottom: 0.0001pt; line-height: 17.25pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><ul><li>Seguridad\nsuperior a contraseña estática<br></li><li>Código de\nautenticación dinámico propio de la aplicación móvil vinculado a los\ncomponentes del dispositivo<br></li><li>Se vincula\nel usuario con un PIN de desbloqueo<br></li><li>Las\naplicaciones no pueden ser duplicadas en otro teléfono o dispositivo móvil.<br></li><li>Protocolo de\naprovisionamiento usando claves asimétricas de forma Online u Offline<br></li><li>Disponible para sistemas Android y IOS<br></li></ul></p>\n\n\n\n\n\n\n\n', '', 2, 18, 1, 'samf-token-movil', '../administrador/site_media/images/archivos/folletos/folleto_SAMF_Token Móvil.pdf', 1, 2),
(12, 'SAMF P2P Mobile Payment', '<p>Solution that allows financial transactions of P2P payments Person to Person using smart mobile phones. It facilitates payments of products and services, and transfer of money from person to person in a matter of seconds, the only market application that incorporates security through dynamic keys. Available for Android and IOS systems.</p>', '<ul><li>Supports authentication with Static Password and Dynamic OTP</li><li>Integrable to any Transactional Switch&nbsp; </li><li>Native mobile application for IOS and Android &nbsp;&nbsp; </li><li>Fast payment by encrypted QR code</li></ul>', '<p>SAMF Mobile Payment P2P Interbancario adapts to the requirements and corporate image of the banking institution, meet strict 2FA security requirements.</p>', 5, 43, 1, 'samf-p2p-mobile-payment', '../administrador/site_media/images/archivos/folletos/folleto_SAMF_P2P Mobile Payment.pdf', 2, 3),
(13, 'MANAGED SECURITY SERVICES', '<p>IT Security Solutions through its management of Professional Services puts at your disposal an advanced service of Managed Security with the purpose of Manage and Monitor in real time 7x24x365 its infrastructure of Telecommunications and Security.</p>', '<p>With this service, our clients guarantee that their infrastructure will be proactively monitored, allowing them to focus their efforts on their business scope. This service is specially designed for organizations that lack human resources with experience and / or monitoring and security capabilities, do not have enough personnel, or do not want to spend on security solutions, under this last premise, we offer the service of Managed Total Security Service, through which IT Security Solutions installs in the client&quot;s facilities the necessary systems which will have post-sale support policies for a monthly fee.</p>', '', 6, 23, 1, 'managed-security-services', '', 2, 4),
(14, 'SAMF - Multifactor Autentication Engine', '<p>It is an ultra-secure, next-generation identity management platform, geared towards the financial sector. Authentic and manages static and dynamic keys, has facilities for positive authentication, it is easy to integrate with any electronic channel of customer service of the bank in an SOA environment. &nbsp;&nbsp;</p><p> Applied correctly, SAMF strengthens the authentication through the different channels such as online banking, mobile banking, ATMs, P2P and P2B Mobile Payments, call center and other types of channels. SAMF is a multichannel authentication solution. This allows to implement advanced authentication mechanisms using multiple factors in a centralized manner. &nbsp;&nbsp; </p><p>It allows the creation and administration of the authentication policies of each channel through a single unified administration interface. It facilitates the administration of profiles and users of the system, it includes a powerful forensic tool with registration, storage and visualization of audit logs.</p>', '<ul><li>Centralized administration of the authentication process&nbsp; </li><li>Secure communication between SAMF and Channels&nbsp; </li><li>Easy integration with electronic customer service channels&nbsp; </li><li>Unlimited scalability&nbsp; </li><li>Rapid adaptation to changes in regulations and policies&nbsp; </li><li>Powerful audit and forensic tool</li></ul>', '', 5, 24, 1, 'samf---multifactor-autentication-engine', '../administrador/site_media/images/archivos/folletos/folleto_SAMF_-_Multifactor_Autentication_Engine.pdf', 2, 1),
(15, 'SAMF Token Mobile', '<p>Token Generator (OTPs) for Android and IOS mobile devices that is integrated into the SAMF identity management system to ensure access to Internet Bankng portals and to ensure financial transactions through a single process of securing transactions through composite OTPs that avoid attacks of man in the middle (Man-in-the-middle) and man in the Browser (Man-in-the-Browser). </p><p>It adapts to the corporate image of the banking institution. &nbsp;</p>', '<ul><li>Security above static password</li><li>Dynamic authentication code of the mobile application linked to the components of the device</li><li>The user is linked with an unlock PIN </li><li>Applications can not be duplicated on another phone or mobile device</li><li>Provisioning protocol using asymmetric keys Online or Offline</li><li>Available for Android and IOS systems</li></ul>', '', 5, 18, 1, 'samf-token-mobile', '../administrador/site_media/images/archivos/folletos/folleto_SAMF_Token Mobile.pdf', 2, 2),
(16, 'SATM - ATM Security Monitoring', '<p>It is a proactive security monitoring solution for ATMs networks, helps mitigate electronic fraud, monitors ATMs permanently and alerts to the presence of suspicious activities. </p><p>SATM consists of three basic elements: OSSIM, S-Server, S-Agent</p>', '<p>FUNCTIONALITIES&nbsp; </p><ul><li>Centralized Management </li><li>Real-Time Monitoring (HW and ATM SW) </li><li>Compilation of Audit Logs </li><li>Auto Protection of ATM Agent </li><li>Vulnerability Analysis </li><li>Forensic Tools </li><li>Compliance PCI Standards</li></ul>', '<p>KEY FEAUTURES</p><ul><li>Secure access via HTTPS </li><li>Administration of Users and Profiles</li><li>Logs of user activity</li><li>Vulnerability Analysis of ATM</li><li>Visor of Availability of ATM and Services </li><li>Configuration of Actions in the presence of suspicious activity </li><li>Viewer of Alarms, Traffic, Risk and Events </li><li>Forensic Reports Edition of Correlation Directives Dashboard of the system</li></ul>', 5, 25, 1, 'satm---atm-security-monitoring', '', 2, 4),
(17, 'CONSULTING SERVICES', '<p>In the experience gained throughout our career we have achieved a high level of knowledge in the areas of security, telecommunications and services, which we make available to our customers through our Consulting Services. </p><p>We have trained professionals with extensive experience in the areas of security and communications architectures, operating systems, vulnerability analysis, risk analysis and management, security monitoring, definition and implementation of IT security policies and training, all of which in order to protect the digital assets of our valued customers. </p><p>We have strategic partnerships with leading international software and hardware security and information technology companies, and our engineers are constantly exploring, evaluating and analyzing new products, in order to achieve partnerships that guarantee our customers that they have products at their disposal. and world class services.</p>', '<p>We guarantee total confidentiality to our clients regarding the design, recommendations, and implementation of security and communications solutions.</p>', '', 6, 20, 1, 'consulting-services', '', 2, 1),
(18, 'SOFTWARE DEVELOPMENT', '<p>We have a development team specialized in security, web and electronic payment applications that possess the skills to create quality software, always placing security as a priority. &nbsp;&nbsp; </p><p>Our IT Development group has experience in secure Web Services, robust authentication applications, advanced cryptography, shielded mobile applications and a whole set of solutions with which you can complement your current applications or create a secure network of applications and telecommunications for your company. &nbsp;&nbsp; </p><p>Every company requires security at the level of infrastructure and software used, in our IT Development department you will find the perfect partner to achieve it. In addition, we offer post production support and accompaniment throughout the implementation process of our products supplemented with 24/7 support with specialists dedicated to answering requests, favoring response times and achieved quality.</p>', '<p>All our developments are carried out under the best international practices, and each job is managed by a project manager who ensures the quality and time agreed.</p>', '', 6, 23, 1, 'software-development', '', 2, 2),
(19, 'TECHNICAL SUPPORT', '<p>Through our Professional Services Management, we offer support services and technical assistance to the software developed by our company, and third-party products marketed, including technical support for software, hardware, equipment maintenance and equipment replacement (RMA).</p><p><strong>Key Characteristics of the Technical Assistance Service</strong>: &nbsp;&nbsp; </p><ul><li>Unlimited support to incidents via phone, email and website&nbsp; </li><li>The Client can designate up to 2 people (support coordinators) who can contact our staff for support and assistance during the term of the contract they will be provided with an account for access to the SAT - Technical Assistance System&nbsp; </li><li>Updates and improvements for all products marketed by our organization and that are free to use, that is, free of charge or that are covered by manufacturer maintenance contracts&nbsp; </li><li>Remote support for all products marketed by our organization</li></ul>', '<p>We offer support policies and technical assistance in the following ways:&nbsp; </p><ul><li>Standard 8x5&nbsp; </li><li>Standard 12x7&nbsp; </li><li>Premium 12x7&nbsp; </li><li>Premium 24x7</li></ul>', '', 6, 23, 1, 'technical-support', '', 2, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `direcciones`
--

CREATE TABLE `direcciones` (
  `id` int(11) NOT NULL,
  `titulo` varchar(200) NOT NULL,
  `descripcion` text NOT NULL,
  `orden` int(11) NOT NULL,
  `estatus` int(11) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `latitud` varchar(30) NOT NULL,
  `longitud` varchar(30) NOT NULL,
  `iframe` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `direcciones`
--

INSERT INTO `direcciones` (`id`, `titulo`, `descripcion`, `orden`, `estatus`, `id_idioma`, `latitud`, `longitud`, `iframe`) VALUES
(16, 'CARACAS', '<SPAN>AV. PRINCIPAL DE BELLO CAMPO, C. C. BELLO CAMPO, PISO 1, CARACAS.</SPAN><BR>', 1, 1, 1, '10.493615', '-66.851637', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7846.207559150498!2d-66.85065802800503!3d10.492484701543722!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x374f36283438401!2sUniseguros!5e0!3m2!1ses!2sve!4v1594588775551!5m2!1ses!2sve\" width=\"100%\" height=\"354\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\" aria-hidden=\"false\" tabindex=\"0\"></iframe>'),
(17, 'SAN ANTONIO', '<SPAN>C.C. CLUB DE CAMPO, PISO 2, LOCAL 2-1, KM 16, CARRETERA PANAMERICANA, SECTOR LAS MINAS, SAN ANTONIO DE LOS ALTOS.</SPAN>', 2, 1, 1, '10.370692', '-66.980689', ''),
(18, 'MARACAY', 'AV. SUCRE FRENTE AL PARQUE LAS BALLENAS, C. C. LA ARBOLEDA, PISO 1, LOCAL 14, MARACAY, EDO. ARAGUA.<DIV><BR></DIV>', 3, 1, 1, '10.264992', '-67.594782', 'x'),
(19, 'PRUEBA DE DIRECCION', 'PRUEBA DE DIRECCION', 1, 2, 1, '', '', ''),
(20, 'PRUEBA', 'PRUEBA', 9, 2, 1, '', '', ''),
(21, 'PRUEBA', 'PRUEBA', 8, 2, 1, '', '', ''),
(22, 'PRUEBA', 'PRUEBA', 7, 2, 1, '', '', ''),
(23, 'PRUEBA DE DIRECCIÓN', 'DIRECCIÓN # 1', 0, 2, 1, '', '', ''),
(24, 'DIRECCIÓN PRUEBA', 'DIRECCIÓN PRUEBA', 0, 2, 1, '', '', ''),
(25, 'PRUEBA Y', 'PRUEBA Y', 4, 1, 1, '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `direccion_mapas`
--

CREATE TABLE `direccion_mapas` (
  `id` int(11) NOT NULL,
  `latitud` text NOT NULL,
  `longitud` text NOT NULL,
  `id_direccion` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `direccion_mapas`
--

INSERT INTO `direccion_mapas` (`id`, `latitud`, `longitud`, `id_direccion`) VALUES
(2, '25.961511', '-80.39116', 2),
(3, '10.4914821', '-66.8294758', 1),
(4, '53.397291', '-2.171772', 3),
(5, '10.4914821', '-66.8294758', 4),
(6, '25.961511', '-80.39116', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `directiva`
--

CREATE TABLE `directiva` (
  `id` int(11) NOT NULL,
  `titulo` text NOT NULL,
  `descripcion` text NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `id_imagen` int(11) NOT NULL,
  `estatus` int(11) NOT NULL,
  `orden` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `directiva`
--

INSERT INTO `directiva` (`id`, `titulo`, `descripcion`, `id_idioma`, `id_imagen`, `estatus`, `orden`) VALUES
(5, 'VICENTE VILLAVICENSIO', 'LOREM IPSUM', 1, 206, 1, 2),
(6, 'RONALD PEÑALVER', 'LOREM IPSUM', 1, 206, 1, 1),
(7, 'RICARDO SOSA BRANGER', 'LOREM IPSUM', 1, 206, 1, 3),
(8, 'JESÚS ALBERTO ORTEGA', 'LOREM IPSUM', 1, 206, 1, 4),
(9, 'ENRIQUE VILLA SERIÑA', 'LOREM IPSUM', 1, 206, 1, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estatus`
--

CREATE TABLE `estatus` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estatus`
--

INSERT INTO `estatus` (`id`, `descripcion`) VALUES
(1, 'Activo'),
(2, 'Inactivo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `footer`
--

CREATE TABLE `footer` (
  `id` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  `correo` varchar(200) NOT NULL,
  `rif` varchar(30) NOT NULL,
  `telefono` varchar(200) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `estatus` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `footer`
--

INSERT INTO `footer` (`id`, `descripcion`, `correo`, `rif`, `telefono`, `id_idioma`, `estatus`) VALUES
(5, 'Sabemos lo que es importante para ti.', 'contacto@uniseguros.com', '', '0800-UNISEGU(8647348)', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `galeria`
--

CREATE TABLE `galeria` (
  `id` int(11) NOT NULL,
  `ruta` varchar(500) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `estatus` int(11) NOT NULL,
  `titulo` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `galeria`
--

INSERT INTO `galeria` (`id`, `ruta`, `id_categoria`, `estatus`, `titulo`) VALUES
(172, 'assets/images/archivos/slider/slider2.png', 1, 1, 'slider2'),
(171, 'assets/images/archivos/slider/slider1.png', 1, 1, 'slider1'),
(173, 'assets/images/archivos/slider/slider3.png', 1, 1, 'slider3'),
(175, 'assets/images/archivos/quienes_somos/nosotros1.jpeg', 3, 1, 'nosotros1'),
(176, 'assets/images/archivos/quienes_somos/friends.jpeg', 3, 1, 'friends'),
(196, 'assets/images/archivos/directiva/directiva-presidente.jpeg', 23, 1, 'directiva-presidente'),
(178, 'assets/images/archivos/directiva/directiva-tesorero.jpeg', 23, 1, 'directiva-tesorero'),
(179, 'assets/images/archivos/directiva/directiva-administrador.jpeg', 23, 1, 'directiva-administrador'),
(0, 'assets/images/delete-trash.png', 0, 1, ''),
(193, 'assets/images/archivos/quienes_somos/mision1.png', 3, 1, 'mision1'),
(182, 'assets/images/archivos/quienes_somos/vision1.png', 3, 1, 'vision1'),
(200, 'assets/images/archivos/reaseguradoras/rs_on_reinsurance.png', 24, 1, 'rs_on_reinsurance'),
(201, 'assets/images/archivos/reaseguradoras/rs_patria.png', 24, 1, 'rs_patria'),
(202, 'assets/images/archivos/reaseguradoras/rs_provincial.png', 24, 1, 'rs_provincial'),
(218, 'assets/images/archivos/usuarios/user.png', 25, 1, 'user'),
(254, 'assets/images/archivos/tipos_servicios/patrimoniales_serv.png', 29, 1, 'patrimoniales_serv'),
(220, 'assets/images/archivos/productos/productos2.png', 22, 1, 'productos2'),
(205, 'assets/images/archivos/directiva/directivo.png', 23, 1, 'directivo'),
(194, 'assets/images/archivos/quienes_somos/valores1.png', 3, 1, 'valores1'),
(197, 'assets/images/archivos/directiva/directiva-4.jpeg', 23, 1, 'directiva-4'),
(198, 'assets/images/archivos/directiva/directiva-5.jpeg', 23, 1, 'directiva-5'),
(199, 'assets/images/archivos/directiva/directiva-6.jpeg', 23, 1, 'directiva-6'),
(206, 'assets/images/archivos/directiva/directiva2.png', 23, 1, 'directiva2'),
(209, 'assets/images/archivos/quienes_somos/vision3.png', 3, 1, 'vision3'),
(210, 'assets/images/archivos/quienes_somos/vision4.png', 3, 1, 'vision4'),
(211, 'assets/images/archivos/reaseguradoras/kairos-color.png', 24, 1, 'kairos-color'),
(212, 'assets/images/archivos/reaseguradoras/onre-color.png', 24, 1, 'onre-color'),
(213, 'assets/images/archivos/reaseguradoras/venezuela-re-color.png', 24, 1, 'venezuela-re-color'),
(216, 'assets/images/archivos/reaseguradoras/provincial-color.png', 24, 1, 'provincial-color'),
(215, 'assets/images/archivos/reaseguradoras/patri.png', 24, 1, 'patri'),
(252, 'assets/images/archivos/tipos_servicios/uniauto.png', 29, 1, 'uniauto'),
(253, 'assets/images/archivos/tipos_servicios/personas_serv.png', 29, 1, 'personas_serv'),
(223, 'assets/images/archivos/servicios/servicios1.jpeg', 7, 1, 'servicios1'),
(225, 'assets/images/archivos/tipos_productos/fianza_licitacion1.png', 27, 1, 'fianza_licitacion1'),
(226, 'assets/images/archivos/tipos_productos/fianza_fiel_cumplimiento2.png', 27, 1, 'fianza_fiel_cumplimiento2'),
(227, 'assets/images/archivos/tipos_productos/fianza_buena_calidad3.png', 27, 1, 'fianza_buena_calidad3'),
(228, 'assets/images/archivos/tipos_productos/fianza_anticipo4.png', 27, 1, 'fianza_anticipo4'),
(229, 'assets/images/archivos/tipos_productos/fianza_laboral5.png', 27, 1, 'fianza_laboral5'),
(230, 'assets/images/archivos/tipos_productos/fianza_aduanal6.png', 27, 1, 'fianza_aduanal6'),
(256, 'assets/images/archivos/tipos_productos/equipos_contratistas1.png', 27, 1, 'equipos_contratistas1'),
(232, 'assets/images/archivos/tipos_productos/uniauto_inteligente.png', 27, 1, 'uniauto_inteligente'),
(255, 'assets/images/archivos/servicios/servicios3.png', 7, 1, 'servicios3'),
(234, 'assets/images/archivos/tipos_productos/accidentes_personales.png', 27, 1, 'accidentes_personales'),
(236, 'assets/images/archivos/tipos_productos/poliza_funeraria.png', 27, 1, 'poliza_funeraria'),
(239, 'assets/images/archivos/tipos_productos/poliza_vida.png', 27, 1, 'poliza_vida'),
(241, 'assets/images/archivos/tipos_productos/lucro_cesante1.png', 27, 1, 'lucro_cesante1'),
(242, 'assets/images/archivos/tipos_productos/incendio1.png', 27, 1, 'incendio1'),
(243, 'assets/images/archivos/tipos_productos/robo1.png', 27, 1, 'robo1'),
(244, 'assets/images/archivos/tipos_productos/dinero_y_o_valores1.png', 27, 1, 'dinero_y_o_valores1'),
(245, 'assets/images/archivos/tipos_productos/riesgos_especiales1.png', 27, 1, 'riesgos_especiales1'),
(246, 'assets/images/archivos/tipos_productos/fidelidad3d.png', 27, 1, 'fidelidad3d'),
(247, 'assets/images/archivos/productos/fianza-logo.png', 26, 1, 'fianza-logo'),
(248, 'assets/images/archivos/productos/personas-logo.png', 26, 1, 'personas-logo'),
(249, 'assets/images/archivos/productos/uniauto-logo.png', 26, 1, 'uniauto-logo'),
(250, 'assets/images/archivos/productos/patrimoniales-logo.png', 26, 1, 'patrimoniales-logo'),
(261, 'assets/images/archivos/tipos_productos/equipo_electronico1.png', 27, 1, 'equipo_electronico1'),
(258, 'assets/images/archivos/tipos_productos/rotura_maquinarias1.png', 27, 1, 'rotura_maquinarias1'),
(259, 'assets/images/archivos/tipos_productos/transporte_maritimo1.png', 27, 1, 'transporte_maritimo1'),
(260, 'assets/images/archivos/tipos_productos/transporte_terrestre1.png', 27, 1, 'transporte_terrestre1'),
(262, 'assets/images/archivos/tipos_productos/embarcaciones_productos1.png', 27, 1, 'embarcaciones_productos1'),
(263, 'assets/images/archivos/tipos_productos/aviacion1.png', 27, 1, 'aviacion1'),
(264, 'assets/images/archivos/tipos_productos/responsibilidad_civil1.png', 27, 1, 'responsibilidad_civil1'),
(265, 'assets/images/archivos/tipos_productos/ramos_tecnicos_ingenieria1.png', 27, 1, 'ramos_tecnicos_ingenieria1'),
(266, 'assets/images/archivos/servicios/que_hacer_en_caso_de_siniestro1.png', 7, 1, 'que_hacer_en_caso_de_siniestro1'),
(275, 'assets/images/archivos/tipos_productos/ROBO2.png', 27, 1, 'ROBO2'),
(274, 'assets/images/archivos/productos/FIANZAS.png', 26, 1, 'FIANZAS'),
(276, 'assets/images/archivos/tipos_productos/rotura_maquinarias2.png', 27, 1, 'rotura_maquinarias2');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `galeria_clientes`
--

CREATE TABLE `galeria_clientes` (
  `id` int(11) NOT NULL,
  `id_galeria` int(11) NOT NULL,
  `id_clientes` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `idioma`
--

CREATE TABLE `idioma` (
  `id` int(11) NOT NULL,
  `descripcion` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `idioma`
--

INSERT INTO `idioma` (`id`, `descripcion`) VALUES
(1, 'Español'),
(2, 'Inglés');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventario_carrito`
--

CREATE TABLE `inventario_carrito` (
  `id` int(11) NOT NULL,
  `id_det_prod` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `id_color` int(11) NOT NULL,
  `id_talla` int(50) NOT NULL,
  `estatus` int(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `inventario_carrito`
--

INSERT INTO `inventario_carrito` (`id`, `id_det_prod`, `cantidad`, `id_color`, `id_talla`, `estatus`) VALUES
(253, 121, 100, 10, 2, 1),
(252, 121, 100, 10, 3, 1),
(251, 121, 100, 10, 4, 1),
(250, 119, 100, 10, 2, 1),
(249, 119, 100, 10, 3, 1),
(248, 119, 100, 10, 4, 1),
(247, 117, 100, 10, 2, 1),
(246, 117, 100, 10, 3, 1),
(245, 117, 100, 10, 4, 1),
(244, 115, 98, 10, 2, 1),
(243, 115, 100, 10, 3, 1),
(242, 115, 100, 10, 4, 1),
(241, 113, 100, 10, 2, 1),
(240, 113, 100, 10, 3, 1),
(239, 113, 100, 10, 4, 1),
(238, 111, 100, 10, 2, 1),
(237, 111, 100, 10, 3, 1),
(236, 111, 100, 10, 4, 1),
(235, 109, 100, 10, 2, 1),
(234, 109, 100, 10, 3, 1),
(233, 109, 100, 10, 4, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventario_ordenes`
--

CREATE TABLE `inventario_ordenes` (
  `id` int(20) NOT NULL,
  `id_det_prod` int(20) NOT NULL,
  `cantidad` int(20) NOT NULL,
  `id_color` int(20) NOT NULL,
  `id_talla` int(20) NOT NULL,
  `estatus` int(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `inventario_ordenes`
--

INSERT INTO `inventario_ordenes` (`id`, `id_det_prod`, `cantidad`, `id_color`, `id_talla`, `estatus`) VALUES
(253, 121, 100, 10, 2, 1),
(252, 121, 100, 10, 3, 1),
(251, 121, 100, 10, 4, 1),
(250, 119, 100, 10, 2, 1),
(249, 119, 100, 10, 3, 1),
(248, 119, 100, 10, 4, 1),
(247, 117, 100, 10, 2, 1),
(246, 117, 100, 10, 3, 1),
(245, 117, 100, 10, 4, 1),
(244, 115, 98, 10, 2, 1),
(243, 115, 100, 10, 3, 1),
(242, 115, 100, 10, 4, 1),
(241, 113, 100, 10, 2, 1),
(240, 113, 100, 10, 3, 1),
(239, 113, 100, 10, 4, 1),
(238, 111, 100, 10, 2, 1),
(237, 111, 100, 10, 3, 1),
(236, 111, 100, 10, 4, 1),
(235, 109, 100, 10, 2, 1),
(234, 109, 100, 10, 3, 1),
(233, 109, 100, 10, 4, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marcas`
--

CREATE TABLE `marcas` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `descripcion` varchar(500) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `id_imagen` int(11) NOT NULL,
  `estatus` int(11) NOT NULL,
  `orden` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `marcas`
--

INSERT INTO `marcas` (`id`, `titulo`, `descripcion`, `id_idioma`, `id_imagen`, `estatus`, `orden`) VALUES
(7, 'LIQUI LIQUI', 'LIQUI LIQUI', 1, 0, 1, 3),
(8, 'ZOUG ZOUG', 'ZOUG ZOUG', 1, 0, 1, 4),
(9, 'LIQUI LIQUI', 'LIQUI LIQUI', 2, 0, 1, 3),
(10, 'ZOUG ZOUG', 'ZOUG ZOUG', 2, 0, 1, 4),
(11, 'PREMIUM', 'PREMIUM', 1, 0, 1, 2),
(12, 'PREMIUM', 'PREMIUM', 2, 0, 1, 2),
(13, 'PANTALONES', 'PANTALONES', 1, 0, 1, 1),
(14, 'PANTS', 'PANTS', 2, 0, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modulos`
--

CREATE TABLE `modulos` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(200) NOT NULL,
  `estatus` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `modulos`
--

INSERT INTO `modulos` (`id`, `descripcion`, `estatus`) VALUES
(1, 'cms', 1),
(2, 'web', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `negocios`
--

CREATE TABLE `negocios` (
  `id` int(11) DEFAULT NULL,
  `objetivo_negocio` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nosotros`
--

CREATE TABLE `nosotros` (
  `id` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  `titulo` varchar(200) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `icono` varchar(200) NOT NULL,
  `id_imagen` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `estatus` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `nosotros`
--

INSERT INTO `nosotros` (`id`, `descripcion`, `titulo`, `id_idioma`, `icono`, `id_imagen`, `orden`, `estatus`) VALUES
(1, 'RAIN', 'RAIN', 1, '<i class=\"icon-umbrella\"></i>', 0, 5, 2),
(2, '<div>Nace en Valencia el 01 de diciembre de 1993, como SEGUROS CONTINENTE, S.A. y registrada en la Superintendencia de la Actividad Aseguradora Venezolana Bajo el N°113. El 09 de Noviembre de 1995 cambia de domicilio para la ciudad de Caracas, cambiando también su denominación social NACIONAL DE SEGUROS, S.A. pero no es sino hasta el año 1996 cuando comienza su actividad comercial asumiendo su nueva razón social por ASEGURADORA NACIONAL UNIDA UNISEGUROS, S.A.</div><div><br></div><div>Su Junta Directiva se encuentra integrada por personal de la Banca y Seguros que poseen una amplia experiencia en la industria del seguro, conocedores de las más profundas necesidades del mercado y del país.</div><div><br></div><div>Desde su fundación, ASEGURADORA NACIONAL UNIDA UNISEGUROS, S.A., con el capital requerido según licencia otorgada por las autoridades locales, y con el apoyo de Reaseguradores Nacionales e Internacionales catalogados como de Primera Línea, ha orientado su gestión en todas las líneas de seguro comercializables para las cuales está autorizada, dirigidos principalmente al negocio de líneas personales, comercios y mediana industria, introduciendo coberturas innovadoras y servicios de calidad entre las cuales se destacan los Seguros para el Hogar, coberturas adecuadas diseñadas para el gremio médico y de ingenieros, así como servicios de Asistencia Legal en caso de accidentes de tránsito, además de ofrecer las cobertura comunes en el mercado para pólizas de automóviles, incendios, H.C.M., entre otros.</div>', 'Sabemos lo que es importante para ti', 1, '', 175, 1, 1),
(3, '<ul><li><b>COMPROMISO:</b> Cumplimiento incondicional de la misión y objetivos establecidos en Uniseguros para satisfacer a nuestros clientes, relacionados y accionistas.</li></ul><ul><li><b>RESPETO: </b>Por los derechos e ideas de la colectividad, diferenciándonos por; el buen trato, la tolerancia, la equidad y el libre discernimiento.</li></ul><ul><li><b>TRABAJO EN EQUIPO:</b> Integración, esfuerzo y apoyo del recurso humano, para lograr los objetivos de forma eficiente y creativa.</li></ul><ul><li><b>RESPONSABILIDAD:</b> Cumplimiento de los objetivos íntegramente. Tomando decisiones conscientemente y asumiendo las consecuencias de sus actos, siempre en cohesión con la Misión y Visión de Uniseguros.</li></ul><ul><li><b>VOCACIÓN DE SERVICIO:</b> Pasión y entrega por lo que hacemos para lograr la visión y la misión de la empresa.</li></ul><ul><li><b>CONCIENCIA ECOLÓGICA:</b> Contribución a la preservación de los recursos ambientales.</li></ul><div><br></div>', 'Valores', 1, '', 194, 2, 1),
(4, '<div>Somos la empresa aseguradora venezolana de puertas abiertas, con importante participación, permanencia en el mercado y una marca que genera confianza. Unificamos esfuerzos para proporcionar tranquilidad a nuestros clientes, brindándoles soluciones integrales e innovadoras, contribuyendo así al bienestar social.</div><div><br></div><div>Para ello contamos con:</div><div><br></div><ul><li>Talento humano capacitado, comprometido, orientado a dar atención personalizada y soluciones justas.</li></ul><ul><li>Amplia red de aliados comerciales y colaboradores.</li></ul><ul><li>Reconocidos reaseguradores de excelente calificación.</li></ul><ul><li>Tecnología innovadora para apoyar los procesos y obtener resultados óptimos.</li></ul><ul><li>Solvencia financiera y patrimonial, capaz de hacerle frente a los compromisos adquiridos.</li></ul><ul><li>Aspectos todos que ajustados al marco legal vigente, garantizan una rentabilidad acorde con las expectativas de los accionistas.</li></ul>', 'Misión', 1, '', 193, 3, 1),
(5, '<div>Ser la aseguradora venezolana, líder en servicios de protección personal y patrimonial, con calidad innovadora, donde lo más importante es brindar tranquilidad, soluciones justas y oportunas a nuestros clientes y relacionados.</div><div><br></div><div>Contando Con:<br></div><ul><li><b>Capital Humano: </b>Personal altamente calificado, identificado y motivado, en un ambiente de participación creativa y alineada con el propósito de la empresa.</li></ul><ul><li><b>Clientes/Comercialización: </b>Red selecta de intermediarios, leales, satisfechos y comprometidos con el desarrollo de la empresa, en una relación “ganar-ganar”. Una cartera de negocios segmentada y diversificada.</li></ul><ul><li><b>Procesos:</b> Proveedores de servicios de reconocida ética y trayectoria comercial. Procesos certificados y tecnología de punta, adecuada con la estrategia del negocio.</li></ul><ul><li><b>Accionistas:</b> Fortaleza y solvencia patrimonial.</li></ul><div><br></div>', 'Visión', 1, '', 210, 4, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orden_compra`
--

CREATE TABLE `orden_compra` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `numero_orden_compra` text NOT NULL,
  `fecha` date NOT NULL,
  `estatus` int(11) NOT NULL,
  `id_carrito` int(11) NOT NULL,
  `payment_id` text NOT NULL,
  `payer_id` text NOT NULL,
  `payment_token` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `orden_compra`
--

INSERT INTO `orden_compra` (`id`, `id_usuario`, `numero_orden_compra`, `fecha`, `estatus`, `id_carrito`, `payment_id`, `payer_id`, `payment_token`) VALUES
(101, 44, 'f0a5d0b4f5ceff00c44284a02b12585e', '2020-02-06', 1, 79, 'PAYID-LY6HOEI41P5257089117605S', 'VKD9GMWBRNE84', 'EC-4JL59682J4300362A');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orden_compra_detalle`
--

CREATE TABLE `orden_compra_detalle` (
  `id` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `monto` float NOT NULL,
  `monto_total` float NOT NULL,
  `id_orden` int(11) NOT NULL,
  `estatus` int(11) NOT NULL,
  `id_cantidad_producto` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `orden_compra_detalle`
--

INSERT INTO `orden_compra_detalle` (`id`, `id_producto`, `cantidad`, `monto`, `monto_total`, `id_orden`, `estatus`, `id_cantidad_producto`) VALUES
(126, 115, 2, 200, 400, 101, 1, 244);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `palabras_claves`
--

CREATE TABLE `palabras_claves` (
  `id` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  `estatus` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `palabras_claves`
--

INSERT INTO `palabras_claves` (`id`, `descripcion`, `estatus`) VALUES
(66, 'uniseguros', 1),
(67, ' qr', 1),
(68, ' pago con qr', 1),
(69, ' token movil', 1),
(70, ' P2P', 1),
(71, ' pago movil p2p', 1),
(72, ' pago movil sms', 1),
(73, ' pago movil p2p sms', 1),
(74, ' pago movil interbancario', 1),
(75, ' pago movil p2p interbancario', 1),
(76, ' p2c', 1),
(77, ' pago movil p2c', 1),
(78, '  pago movil p2c interbancario', 1),
(79, ' p2b', 1),
(80, ' pago p2b qr', 1),
(81, ' pago qr', 1),
(82, ' mobile payment', 1),
(83, ' qr payment', 1),
(84, ' p2p payment', 1),
(85, ' p2c payment', 1),
(86, ' p2b payment', 1),
(87, ' secure payment', 1),
(88, ' secure qr payment', 1),
(89, ' interbank payment', 1),
(90, ' p2p mobile payment', 1),
(91, ' p2c mobile payment', 1),
(92, ' p2b mobile payment', 1),
(93, ' secure mobile payment', 1),
(94, ' ultra secure mobile payment', 1),
(95, ' soft token', 1),
(96, ' soft token android', 1),
(97, ' soft token ios', 1),
(98, ' identity management', 1),
(99, ' identitity management financial', 1),
(100, ' media payment', 1),
(101, ' media payment secure solutions', 1),
(102, ' mobile media payment', 1),
(103, 'ejemplo', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personas`
--

CREATE TABLE `personas` (
  `id` int(11) NOT NULL,
  `cedula` int(11) NOT NULL,
  `nombres_apellidos` varchar(100) NOT NULL,
  `estatus` int(11) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `personas`
--

INSERT INTO `personas` (`id`, `cedula`, `nombres_apellidos`, `estatus`, `telefono`, `email`) VALUES
(1, 9999999, 'ADMINISTRADOR', 1, '04164561122', 'content.manager@gmail.com'),
(12, 2000000, 'USUARIO COMUN', 1, '04161112223', 'usuario_comun@gmail.com'),
(11, 3000000, 'EDITOR', 1, '04164564445', 'editor@gmail.com'),
(13, 174561233, 'NORMAN PEREZ', 1, '4168092388', 'zormanvideos@gmail.com'),
(14, 1245698777, 'LED VARELA', 1, '4168092388', 'led@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `portafolio`
--

CREATE TABLE `portafolio` (
  `id` int(11) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `id_servicio` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `fecha` date NOT NULL,
  `codigo` varchar(10) NOT NULL,
  `url` varchar(155) NOT NULL,
  `cliente` varchar(255) NOT NULL,
  `estatus` int(11) NOT NULL,
  `slug` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `portafolio`
--

INSERT INTO `portafolio` (`id`, `id_idioma`, `id_servicio`, `titulo`, `descripcion`, `fecha`, `codigo`, `url`, `cliente`, `estatus`, `slug`) VALUES
(11, 1, 17, 'proyecto 1', 'axioma', '2020-11-02', 'PRY1', 'AXIOMAPRUEBAS.EPIZY.COM', 'axioma', 1, 'proyecto-1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `portafolio_imag`
--

CREATE TABLE `portafolio_imag` (
  `id` int(11) NOT NULL,
  `id_portafolio` int(11) NOT NULL,
  `id_imagen` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id` int(11) NOT NULL,
  `titulo` varchar(200) NOT NULL,
  `descripcion` text NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `estatus` int(11) NOT NULL,
  `id_imagen` int(11) NOT NULL,
  `slug` varchar(200) NOT NULL,
  `orden` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `titulo`, `descripcion`, `id_idioma`, `estatus`, `id_imagen`, `slug`, `orden`) VALUES
(2, 'Fianzas', '', 1, 1, 274, 'fianzas', 4),
(3, 'Uniauto', '', 1, 1, 249, 'uniauto', 1),
(4, 'Personas', '', 1, 1, 248, 'personas', 3),
(5, 'Patrimoniales', '', 1, 1, 250, 'patrimoniales', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos_clonados`
--

CREATE TABLE `productos_clonados` (
  `id` int(11) NOT NULL,
  `id_det_prod` int(11) NOT NULL COMMENT 'id del producto clonado',
  `id_producto_original` int(11) NOT NULL COMMENT 'id del producto original'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `productos_clonados`
--

INSERT INTO `productos_clonados` (`id`, `id_det_prod`, `id_producto_original`) VALUES
(8, 93, 92),
(9, 95, 94),
(10, 98, 97),
(11, 99, 97),
(12, 102, 101),
(13, 104, 103),
(14, 106, 105),
(15, 108, 107),
(16, 110, 109),
(17, 112, 111),
(18, 114, 113),
(19, 116, 115),
(20, 118, 117),
(21, 120, 119),
(22, 122, 121),
(23, 124, 123);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reaseguradoras`
--

CREATE TABLE `reaseguradoras` (
  `id` int(11) NOT NULL,
  `titulo` text NOT NULL,
  `descripcion` text NOT NULL,
  `url` text NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `id_imagen` int(11) NOT NULL,
  `estatus` int(11) NOT NULL,
  `orden` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `reaseguradoras`
--

INSERT INTO `reaseguradoras` (`id`, `titulo`, `descripcion`, `url`, `id_idioma`, `id_imagen`, `estatus`, `orden`) VALUES
(1, 'REASEGURADORA PATRIA, S.A.B.', '<SPAN>REASEGURADORA PATRIA, S.A.B.</SPAN>', 'https://www.patriare.com.mx/', 1, 215, 1, 2),
(2, 'DOS', 'DOS', 'dos.com', 1, 0, 2, 3),
(3, 'ON REINSURANCE N.V', '<SPAN>ON REINSURANCE N.V</SPAN>', 'http://www.on-re.com/', 1, 212, 1, 1),
(4, 'PROVINCIAL DE REASEGUROS, C.A.', '<SPAN>PROVINCIAL DE REASEGUROS, C.A.</SPAN><BR>', 'https://www.provincialre.com/', 1, 216, 1, 3),
(5, 'C.A. REASEGURADORA INTERNACIONAL DE VENEZUELA.', '<SPAN>C.A. REASEGURADORA INTERNACIONAL DE VENEZUELA.</SPAN><BR>', 'http://venezuelare.com/', 1, 213, 1, 4),
(6, 'KAIRIOS DE REASEGUROS, C.A', '<DIV>KAIRIOS DE REASEGUROS, C.A<BR></DIV>', 'https://kairosdereaseguros.com/', 1, 211, 1, 5),
(7, 'SEIS', '', '', 1, 0, 2, 7),
(8, 'SIETE', '', '', 1, 0, 2, 7),
(9, 'PRUEBA', 'PRUEBA', 'prueba', 1, 200, 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `redes`
--

CREATE TABLE `redes` (
  `id` int(11) NOT NULL,
  `enlace` varchar(200) NOT NULL,
  `id_tipo_redes` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `red_social`
--

CREATE TABLE `red_social` (
  `id` int(11) NOT NULL,
  `url_red` varchar(100) NOT NULL,
  `id_tipo_red` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `red_social`
--

INSERT INTO `red_social` (`id`, `url_red`, `id_tipo_red`) VALUES
(5, 'https://www.instagram.com/uniseguros1', 3),
(6, 'https://www.facebook.com/uniseguros1', 1),
(11, 'https://www.twitter.com/uniseguros1', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seccion_galeria`
--

CREATE TABLE `seccion_galeria` (
  `id` int(11) NOT NULL,
  `id_imagen` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  `estatus` int(11) NOT NULL,
  `id_idioma` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seccion_noticias`
--

CREATE TABLE `seccion_noticias` (
  `id` int(11) NOT NULL,
  `titulo` varchar(300) NOT NULL,
  `id_imagen` int(11) NOT NULL,
  `descripcion` text NOT NULL,
  `estatus` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `slug` text NOT NULL,
  `id_idioma` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `seccion_noticias`
--

INSERT INTO `seccion_noticias` (`id`, `titulo`, `id_imagen`, `descripcion`, `estatus`, `fecha`, `id_usuario`, `slug`, `id_idioma`) VALUES
(9, 'NOTICIA PRINCIPAL', 106, '<span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras a porttitor risus. Pellentesque a nibh elementum libero vehicula molestie. Suspendisse sed odio facilisis, eleifend purus eget, suscipit lorem. Donec scelerisque nisi sed sem laoreet semper. Ut aliquet rhoncus mi ac hendrerit. Suspendisse sodales est dui, non consequat libero pellentesque malesuada. Etiam vel justo risus. Sed mollis condimentum nunc sit amet dignissim. In enim eros, molestie eu elementum a, feugiat quis ligula.</span><br>', 1, '2019-02-28', 1, 'noticia-principal', 1),
(10, 'NOTICIA DOS', 104, '<span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras a porttitor risus. Pellentesque a nibh elementum libero vehicula molestie. Suspendisse sed odio facilisis, eleifend purus eget, suscipit lorem. Donec scelerisque nisi sed sem laoreet semper. Ut aliquet rhoncus mi ac hendrerit. Suspendisse sodales est dui, non consequat libero pellentesque malesuada. Etiam vel justo risus. Sed mollis condimentum nunc sit amet dignissim. In enim eros, molestie eu elementum a, feugiat quis ligula.</span><br>', 1, '2019-02-28', 1, 'noticia-dos', 1),
(11, 'NOTICIA 3', 108, '<span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras a porttitor risus. Pellentesque a nibh elementum libero vehicula molestie. Suspendisse sed odio facilisis, eleifend purus eget, suscipit lorem. Donec scelerisque nisi sed sem laoreet semper. Ut aliquet rhoncus mi ac hendrerit. Suspendisse sodales est dui, non consequat libero pellentesque malesuada. Etiam vel justo risus. Sed mollis condimentum nunc sit amet dignissim. In enim eros, molestie eu elementum a, feugiat quis ligula.</span><br>', 1, '2019-02-28', 1, 'noticia-3', 1),
(12, 'NEWS 1', 106, '<span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras a porttitor risus. Pellentesque a nibh elementum libero vehicula molestie. Suspendisse sed odio facilisis, eleifend purus eget, suscipit lorem. Donec scelerisque nisi sed sem laoreet semper. Ut aliquet rhoncus mi ac hendrerit. Suspendisse sodales est dui, non consequat libero pellentesque malesuada. Etiam vel justo risus. Sed mollis condimentum nunc sit amet dignissim. In enim eros, molestie eu elementum a, feugiat quis ligula</span>', 1, '2019-02-28', 1, 'news-1', 2),
(13, 'NEWS 2', 107, '<span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras a porttitor risus. Pellentesque a nibh elementum libero vehicula molestie. Suspendisse sed odio facilisis, eleifend purus eget, suscipit lorem. Donec scelerisque nisi sed sem laoreet semper. Ut aliquet rhoncus mi ac hendrerit. Suspendisse sodales est dui, non consequat libero pellentesque malesuada. Etiam vel justo risus. Sed mollis condimentum nunc sit amet dignissim. In enim eros, molestie eu elementum a, feugiat quis ligula.</span>', 1, '2019-02-28', 1, 'news-2', 2),
(14, 'NEWS 3', 108, '<span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras a porttitor risus. Pellentesque a nibh elementum libero vehicula molestie. Suspendisse sed odio facilisis, eleifend purus eget, suscipit lorem. Donec scelerisque nisi sed sem laoreet semper. Ut aliquet rhoncus mi ac hendrerit. Suspendisse sodales est dui, non consequat libero pellentesque malesuada. Etiam vel justo risus. Sed mollis condimentum nunc sit amet dignissim. In enim eros, molestie eu elementum a, feugiat quis ligula.</span>', 1, '2019-02-28', 1, 'news-3', 2),
(15, 'PRUEBA 4', 107, '<span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc ac ornare dui. Sed libero sapien, commodo non accumsan non, egestas a sem. Etiam ac diam in nibh efficitur pellentesque id quis eros. Cras rhoncus libero quis cursus dignissim. Praesent suscipit, magna quis malesuada consectetur, risus ante faucibus purus, nec pharetra odio ex tempor dui. Fusce sed velit tincidunt, hendrerit lacus suscipit, bibendum massa. Phasellus elit ante, pulvinar vel mauris et, aliquet euismod erat. Aliquam suscipit vestibulum lacus eget gravida. Etiam interdum tincidunt ligula, vel eleifend tortor malesuada eget. Nulla sit amet enim et magna hendrerit dictum. Fusce tortor massa, porta at dignissim ut, tempus sit amet arcu. Suspendisse ornare, ante in sollicitudin vestibulum, nisi est ullamcorper mauris, quis lacinia ex magna quis sem. Sed et enim dolor. In eget tempus lorem. Pellentesque quis vehicula est, vel congue felis.</span>', 1, '2019-11-11', 1, 'prueba-4', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicios`
--

CREATE TABLE `servicios` (
  `id` int(11) NOT NULL,
  `titulo` varchar(200) NOT NULL,
  `descripcion` text NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `estatus` int(11) NOT NULL,
  `id_imagen` int(11) NOT NULL,
  `slug` varchar(200) NOT NULL,
  `orden` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `servicios`
--

INSERT INTO `servicios` (`id`, `titulo`, `descripcion`, `id_idioma`, `estatus`, `id_imagen`, `slug`, `orden`) VALUES
(2, '¿qué hacer en caso de siniestros?', '<span>¿qué hacer en caso de siniestros?<br><br></span>', 1, 1, 266, 'que-hacer-en-caso-de-siniestros', 1),
(3, 'Servicio de prueba', 'servicio de prueba', 1, 2, 223, 'servicio-de-prueba', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `titulo` text NOT NULL,
  `descripcion` text NOT NULL,
  `imagen` varchar(300) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `boton` varchar(200) NOT NULL,
  `id_imagen` int(200) NOT NULL,
  `estatus` int(11) NOT NULL,
  `url` varchar(300) NOT NULL,
  `direccion` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `vertical` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `slider`
--

INSERT INTO `slider` (`id`, `titulo`, `descripcion`, `imagen`, `id_idioma`, `boton`, `id_imagen`, `estatus`, `url`, `direccion`, `orden`, `vertical`) VALUES
(1, 'Con Uniseguros', 'Protegerte está a tu alcance', '', 1, 'VER SERVICIOS', 173, 1, 'SERVICIOS', 2, 1, 2),
(2, 'Sabemos', 'Lo que es importante para ti', '', 1, 'CONTACTANOS', 171, 1, 'CONTACTANOS', 2, 2, 1),
(3, 'Ahora', 'Tu vehículo estará asegurado', '', 1, 'VER SERVICIOS', 172, 1, '', 1, 3, 1),
(4, 'Prueba', '', '', 1, '', 171, 2, '', 1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `social_login`
--

CREATE TABLE `social_login` (
  `id` int(200) NOT NULL,
  `nombre_apellido` varchar(200) NOT NULL,
  `correo` varchar(200) NOT NULL,
  `clave` varchar(50) NOT NULL,
  `codigo` varchar(100) NOT NULL,
  `tp_login` int(10) NOT NULL,
  `estatus` int(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `social_login`
--

INSERT INTO `social_login` (`id`, `nombre_apellido`, `correo`, `clave`, `codigo`, `tp_login`, `estatus`) VALUES
(44, 'nelly moreno', 'nlmdiaz@gmail.com', '7c222fb2927d828af22f592134e8932480637c0d', 'uLRPt9CnXAkl', 1, 1),
(45, 'axioma developers', 'contacto.axioma.dvlp@gmail.com', '', '', 2, 1),
(46, 'gianni santucci', 'gianni.d.santucci@gmail.com', '', '', 2, 1),
(47, 'dario carrillo', 'gdsc120987@hotmail.com', '7c222fb2927d828af22f592134e8932480637c0d', 'ZqT1bp9WvRKc', 1, 2),
(48, 'Pedro perez', 'pperez@gmail.com', '7c222fb2927d828af22f592134e8932480637c0d', 'nXBRGahstuy4', 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tallas`
--

CREATE TABLE `tallas` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `estatus` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tallas`
--

INSERT INTO `tallas` (`id`, `descripcion`, `estatus`) VALUES
(1, 'XS', 0),
(2, 'S', 1),
(3, 'M', 1),
(4, 'L', 1),
(5, 'XL', 0),
(6, 'TRTR', 2),
(7, 'AZUL', 2),
(8, 'U', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `telefonos`
--

CREATE TABLE `telefonos` (
  `id` int(11) NOT NULL,
  `id_direccion` int(11) NOT NULL,
  `telefono` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `telefonos`
--

INSERT INTO `telefonos` (`id`, `id_direccion`, `telefono`) VALUES
(15, 1, '+58-212-234-24-44'),
(16, 1, '+58-412-219-48-77'),
(23, 3, '+5609998877'),
(26, 4, '+58-212-234-24-44'),
(27, 4, '+58-412-219-48-77'),
(32, 5, '+1-786-288-01-75'),
(33, 5, '+1-305-262-28-15'),
(34, 2, '+1-786-288-01-75'),
(35, 2, '+1-305-262-28-15'),
(36, 15, '4168092388'),
(62, 17, '60.66'),
(61, 17, '(0212)372.60.27'),
(81, 18, '35.78'),
(80, 18, '26.01'),
(89, 16, '(0212)600.26.00'),
(64, 19, '4168092388'),
(79, 18, '(0243)241.50.10'),
(76, 20, '04124567788'),
(77, 21, '04124567788'),
(78, 22, '04124567788'),
(82, 18, '10.60'),
(84, 23, '4168092388'),
(86, 24, '04167485648'),
(88, 25, '4168092388');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos_usuarios`
--

CREATE TABLE `tipos_usuarios` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipos_usuarios`
--

INSERT INTO `tipos_usuarios` (`id`, `descripcion`) VALUES
(1, 'administrador'),
(2, 'editor');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_negocio`
--

CREATE TABLE `tipo_negocio` (
  `id` int(11) NOT NULL,
  `titulo` text NOT NULL,
  `descripcion` text NOT NULL,
  `estatus` int(11) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `posicion` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_negocio`
--

INSERT INTO `tipo_negocio` (`id`, `titulo`, `descripcion`, `estatus`, `id_idioma`, `posicion`) VALUES
(1, 'Integración de Soluciones', '<p style=\"margin-bottom: 0.0001pt; line-height: 17.25pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size:10.5pt;font-family:&quot;Open Sans&quot;;\nmso-bidi-font-family:Helvetica;color:#333333;mso-ansi-language:ES\" lang=\"ES\">Contamos con\namplia experiencia en el área de integraci</span><span style=\"font-size:11.0pt;font-family:&quot;Calibri&quot;,sans-serif;color:black;\nmso-ansi-language:ES\" lang=\"ES\">ó</span><span style=\"font-size:10.5pt;font-family:\n&quot;Open Sans&quot;;mso-bidi-font-family:Helvetica;color:#333333;mso-ansi-language:\nES\" lang=\"ES\">n de soluciones de seguridad y telecomunicaciones, a la fecha hemos\nrealizado innumerables implementaciones en los sectores de Finanzas, Gobierno,\nTelecomunicaciones, Seguros, Industria y Comercio en general.<o:p></o:p></span></p>\n\n<span style=\"font-size:10.5pt;font-family:&quot;Open Sans&quot;;\nmso-bidi-font-family:Helvetica;color:#333333;mso-ansi-language:ES\" lang=\"ES\">&nbsp;</span>\n\n<p style=\"margin-bottom: 0.0001pt; line-height: 17.25pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size:10.5pt;font-family:&quot;Open Sans&quot;;\nmso-bidi-font-family:Helvetica;color:#333333;mso-ansi-language:ES\" lang=\"ES\">Nuestra\nfortaleza se basa en un equipo humano integrado por ingenieros debidamente\nentrenados y certificados en las marcas que comercializamos, que tienen como misión\nprestar servicios de preventa y postventa de <strong>excelencia.</strong><o:p></o:p></span></p>', 1, 1, 3),
(2, 'Portafolio de Soluciones', '<p>Contamos con un área especializada en el desarrollo de soluciones de seguridad IT y financiera, en ella trabajamos a diario para ayudar a la banca a satisfacer las necesidades de sus clientes, acercándolos a ellos gracias a la tecnología. Nuestro portafolio incluye software de gestión de identidad, generadores de tokens para smartphones y sistemas de pago P2P, P2C y P2B ultra seguros.</p>', 1, 1, 1),
(3, 'Servicios Profesionales', '<p>Ponemos a disposición de nuestros clientes una amplia gama de servicios de soporte técnico, entrenamiento, consultoría, control de proyectos y pólizas de soporte en variadas modalidades que se ajustan según sus necesidades.</p>', 1, 1, 2),
(4, 'Ejemplo de modificación', ' Lorem ipsum dolor', 0, 1, 0),
(5, 'Solutions Portfolio', '<p>We have an area specialized in the development of IT and financial security solutions, in which we work daily to help banks meet the needs of their customers, bringing them closer to them thanks to technology. Our portfolio includes identity management software, token generators for smartphones and P2P, P2C and P2B ultra secure payment systems.</p>', 1, 2, 0),
(6, 'Professional Services', '<p>We offer our clients a wide range of technical support services, training, consulting, project control and support policies in various ways that are adjusted according to your needs.</p>', 1, 2, 0),
(7, 'Integration Services', '<p>We have extensive experience in the area of integration of security and telecommunications solutions, to date we have made countless implementations in the sectors of Finance, Government, Telecommunications, Insurance, Industry and Commerce in general. &nbsp; &nbsp;&nbsp; Our strength is based on a human team made up of engineers duly trained and certified in the brands we market, whose mission is to provide pre-sales and after-sales services of excellence. &nbsp; &nbsp; &nbsp; &nbsp;</p>', 1, 2, 0),
(8, 'prueba de registro cargar  modificar', 'Esto es una prueba de registro de modificación, el registro ha sido modificado<br>', 0, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_producto`
--

CREATE TABLE `tipo_producto` (
  `id` int(11) NOT NULL,
  `titulo` varchar(500) NOT NULL,
  `descripcion` varchar(500) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `id_productos` int(11) NOT NULL,
  `estatus` int(11) NOT NULL,
  `id_imagen` int(11) NOT NULL,
  `slug` varchar(200) NOT NULL,
  `orden` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_producto`
--

INSERT INTO `tipo_producto` (`id`, `titulo`, `descripcion`, `id_idioma`, `id_productos`, `estatus`, `id_imagen`, `slug`, `orden`) VALUES
(1, 'Fianza de anticipo', '', 1, 2, 1, 228, 'fianza-de-anticipo', 3),
(2, 'Fianza de fiel cumplimiento', '', 1, 2, 1, 226, 'fianza-de-fiel-cumplimiento', 2),
(3, 'Fianza de buena calidad', '', 1, 2, 1, 227, 'fianza-de-buena-calidad', 6),
(5, 'Fianza laboral', '', 1, 2, 1, 229, 'fianza-laboral', 4),
(6, 'Fianza aduanal', '', 1, 2, 1, 230, 'fianza-aduanal', 5),
(7, 'Uniauto plan amigo', '', 1, 3, 1, 232, 'uniauto-plan-amigo', 1),
(8, 'Accidentes personales', '', 1, 4, 1, 234, 'accidentes-personales', 1),
(9, 'Póliza de vida', '', 1, 4, 1, 239, 'prliza-de-vida', 2),
(10, 'Póliza funeraria', '', 1, 4, 1, 236, 'poliza-funeraria', 3),
(12, 'Incendio', '', 1, 5, 1, 242, 'incendio', 1),
(13, 'Lucro cesante', '', 1, 5, 1, 241, 'lucro-cesante', 2),
(14, 'Robo', '', 1, 5, 1, 275, 'robo', 3),
(15, 'Dinero y/o valores', '', 1, 5, 1, 244, 'dinero-yo-valores', 4),
(16, 'Riesgos especiales', '', 1, 5, 1, 245, 'riesgos-especiales', 5),
(17, 'Fildelidad 3d', '', 1, 5, 1, 246, 'fildelilad-3d', 6),
(18, 'Equipo de contratistas', '', 1, 5, 2, 258, 'equipo-de-contratistas', 7),
(19, 'Rotura de maquinarias', '', 1, 5, 2, 276, 'rotura-de-maquinarias', 8),
(20, 'Equipo electrónico', '', 1, 5, 2, 261, 'equipo-electrrnico', 8),
(21, 'Transporte terrestre', '', 1, 5, 1, 260, 'transporte-terrestre', 7),
(22, 'Transporte marítimo y aéreo', '', 1, 5, 1, 259, 'transporte-marrtimo-y-arreo', 8),
(23, 'Embarcaciones', '', 1, 5, 1, 262, 'embarcaciones', 9),
(36, 'Aviación', '', 1, 5, 1, 263, 'aviacion', 10),
(37, 'Responsabilidad civil generales', '', 1, 5, 1, 264, 'responsabilidad-civil-generales', 11),
(38, 'Fianza de mantenimiento de la oferta', '.', 1, 2, 1, 225, 'fianza-de-mantenimiento-de-la-oferta', 1),
(39, 'Ramos técnicos de ingeniería', '', 1, 5, 1, 265, 'ramos-tecnicos-de-ingenieria', 12);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_red`
--

CREATE TABLE `tipo_red` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_red`
--

INSERT INTO `tipo_red` (`id`, `descripcion`) VALUES
(1, 'Facebook'),
(2, 'Twitter'),
(3, 'Instagram'),
(4, 'Linkedin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_redes`
--

CREATE TABLE `tipo_redes` (
  `id` int(11) NOT NULL,
  `descripcion` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_redes`
--

INSERT INTO `tipo_redes` (`id`, `descripcion`) VALUES
(1, 'Facebook'),
(2, 'Twitter');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_servicios`
--

CREATE TABLE `tipo_servicios` (
  `id` int(11) NOT NULL,
  `titulo` varchar(500) NOT NULL,
  `descripcion` varchar(500) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `id_servicios` int(11) NOT NULL,
  `estatus` int(11) NOT NULL,
  `id_imagen` int(11) NOT NULL,
  `slug` varchar(200) NOT NULL,
  `orden` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_servicios`
--

INSERT INTO `tipo_servicios` (`id`, `titulo`, `descripcion`, `id_idioma`, `id_servicios`, `estatus`, `id_imagen`, `slug`, `orden`) VALUES
(2, 'Uniauto', '', 1, 2, 1, 252, 'uniauto', 1),
(3, 'Personas', '', 1, 2, 1, 253, 'personas', 3),
(4, 'Patrimoniales', '', 1, 2, 1, 254, 'patrimoniales', 2),
(5, 'Tipo servicio de prueba', 'Tipo de servicio de prueba', 1, 3, 2, 223, 'tipo-servicio-de-prueba', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `id_persona` int(11) NOT NULL,
  `id_tipo_usuario` int(11) NOT NULL,
  `login` varchar(150) NOT NULL,
  `clave` varchar(150) NOT NULL,
  `estatus` int(11) NOT NULL,
  `id_imagen` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `id_persona`, `id_tipo_usuario`, `login`, `clave`, `estatus`, `id_imagen`) VALUES
(1, 1, 1, 'administrador', '7c4a8d09ca3762af61e59520943dc26494f8941b', 1, 218),
(11, 12, 2, 'user', 'a0320a043f8b41c9a3d29dd8dc947efdf0980fb1', 1, 0),
(10, 11, 2, 'editor', '489c6ca4fa44d6733a85a2f158a19e35491b264a', 1, 0),
(12, 13, 1, 'zorman', '7c4a8d09ca3762af61e59520943dc26494f8941b', 1, 0),
(13, 14, 2, 'led', '7c4a8d09ca3762af61e59520943dc26494f8941b', 1, 218);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videos`
--

CREATE TABLE `videos` (
  `id` int(11) NOT NULL,
  `titulo` text NOT NULL,
  `link` text NOT NULL,
  `estatus` int(11) NOT NULL,
  `id_idioma` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `videos`
--

INSERT INTO `videos` (`id`, `titulo`, `link`, `estatus`, `id_idioma`) VALUES
(1, 'SAMF Pago movil, noticias Globovisión tecnología', 'https://www.youtube.com/watch?v=8QmIyF7q3vI', 1, 1),
(2, 'Pago P2P Noticias globovisión tecnología', 'https://www.youtube.com/watch?v=NcS4g3lN_eU&t=53s', 0, 1),
(3, 'BanescoPagoMovil', 'https://youtu.be/jBFZXhpvYKo', 0, 1),
(4, 'Pago Móvil Banesco', 'https://www.youtube.com/watch?v=jBFZXhpvYKo', 1, 1),
(5, 'Entrevista a  Deiniel Cardenas Banesco Pago Móvil', 'https://www.youtube.com/watch?v=z33RIoHKKBY&feature=youtu.be', 1, 1),
(6, 'BANESCO PAGO MOVIL', 'https://youtu.be/JVUT2YBJTw0', 0, 1),
(7, 'BANESCO PAGO MÓVIL', 'https://www.youtube.com/watch?v=JVUT2YBJTw0&feature=youtu.be', 1, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `auditoria`
--
ALTER TABLE `auditoria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `bancos`
--
ALTER TABLE `bancos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cambio_contraseña`
--
ALTER TABLE `cambio_contraseña`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cantidad_producto`
--
ALTER TABLE `cantidad_producto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `carga_pdfs`
--
ALTER TABLE `carga_pdfs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `carrito_encabezado`
--
ALTER TABLE `carrito_encabezado`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `carrito_productos`
--
ALTER TABLE `carrito_productos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categorias_productos_idiomas`
--
ALTER TABLE `categorias_productos_idiomas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categoria_producto`
--
ALTER TABLE `categoria_producto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `colores`
--
ALTER TABLE `colores`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `contactos`
--
ALTER TABLE `contactos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `contactos_empleo`
--
ALTER TABLE `contactos_empleo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `correos`
--
ALTER TABLE `correos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cuentas_bancarias`
--
ALTER TABLE `cuentas_bancarias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `descripcion`
--
ALTER TABLE `descripcion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `detalle_productos`
--
ALTER TABLE `detalle_productos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `detalle_productos_imag`
--
ALTER TABLE `detalle_productos_imag`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `detalle_servicios`
--
ALTER TABLE `detalle_servicios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `detalle_tipo_negocio`
--
ALTER TABLE `detalle_tipo_negocio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `direcciones`
--
ALTER TABLE `direcciones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `direccion_mapas`
--
ALTER TABLE `direccion_mapas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `directiva`
--
ALTER TABLE `directiva`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `estatus`
--
ALTER TABLE `estatus`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `footer`
--
ALTER TABLE `footer`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `galeria`
--
ALTER TABLE `galeria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `galeria_clientes`
--
ALTER TABLE `galeria_clientes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `idioma`
--
ALTER TABLE `idioma`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `inventario_carrito`
--
ALTER TABLE `inventario_carrito`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `inventario_ordenes`
--
ALTER TABLE `inventario_ordenes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `marcas`
--
ALTER TABLE `marcas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `modulos`
--
ALTER TABLE `modulos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `negocios`
--
ALTER TABLE `negocios`
  ADD KEY `id` (`id`);

--
-- Indices de la tabla `nosotros`
--
ALTER TABLE `nosotros`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `orden_compra`
--
ALTER TABLE `orden_compra`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `orden_compra_detalle`
--
ALTER TABLE `orden_compra_detalle`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `palabras_claves`
--
ALTER TABLE `palabras_claves`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `personas`
--
ALTER TABLE `personas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `portafolio`
--
ALTER TABLE `portafolio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `portafolio_imag`
--
ALTER TABLE `portafolio_imag`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `productos_clonados`
--
ALTER TABLE `productos_clonados`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `reaseguradoras`
--
ALTER TABLE `reaseguradoras`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `redes`
--
ALTER TABLE `redes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `red_social`
--
ALTER TABLE `red_social`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `seccion_galeria`
--
ALTER TABLE `seccion_galeria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `seccion_noticias`
--
ALTER TABLE `seccion_noticias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `servicios`
--
ALTER TABLE `servicios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `social_login`
--
ALTER TABLE `social_login`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tallas`
--
ALTER TABLE `tallas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `telefonos`
--
ALTER TABLE `telefonos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipos_usuarios`
--
ALTER TABLE `tipos_usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_negocio`
--
ALTER TABLE `tipo_negocio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_producto`
--
ALTER TABLE `tipo_producto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_redes`
--
ALTER TABLE `tipo_redes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_servicios`
--
ALTER TABLE `tipo_servicios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `auditoria`
--
ALTER TABLE `auditoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1255;

--
-- AUTO_INCREMENT de la tabla `bancos`
--
ALTER TABLE `bancos`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `cambio_contraseña`
--
ALTER TABLE `cambio_contraseña`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `cantidad_producto`
--
ALTER TABLE `cantidad_producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=254;

--
-- AUTO_INCREMENT de la tabla `carga_pdfs`
--
ALTER TABLE `carga_pdfs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `carrito_encabezado`
--
ALTER TABLE `carrito_encabezado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT de la tabla `carrito_productos`
--
ALTER TABLE `carrito_productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de la tabla `categorias_productos_idiomas`
--
ALTER TABLE `categorias_productos_idiomas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT de la tabla `categoria_producto`
--
ALTER TABLE `categoria_producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `colores`
--
ALTER TABLE `colores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `contactos`
--
ALTER TABLE `contactos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=145;

--
-- AUTO_INCREMENT de la tabla `contactos_empleo`
--
ALTER TABLE `contactos_empleo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT de la tabla `correos`
--
ALTER TABLE `correos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `cuentas_bancarias`
--
ALTER TABLE `cuentas_bancarias`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `descripcion`
--
ALTER TABLE `descripcion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `detalle_productos`
--
ALTER TABLE `detalle_productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;

--
-- AUTO_INCREMENT de la tabla `detalle_productos_imag`
--
ALTER TABLE `detalle_productos_imag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=368;

--
-- AUTO_INCREMENT de la tabla `detalle_servicios`
--
ALTER TABLE `detalle_servicios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `detalle_tipo_negocio`
--
ALTER TABLE `detalle_tipo_negocio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `direcciones`
--
ALTER TABLE `direcciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de la tabla `direccion_mapas`
--
ALTER TABLE `direccion_mapas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `directiva`
--
ALTER TABLE `directiva`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `estatus`
--
ALTER TABLE `estatus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `footer`
--
ALTER TABLE `footer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `galeria`
--
ALTER TABLE `galeria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=277;

--
-- AUTO_INCREMENT de la tabla `galeria_clientes`
--
ALTER TABLE `galeria_clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `idioma`
--
ALTER TABLE `idioma`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `marcas`
--
ALTER TABLE `marcas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `modulos`
--
ALTER TABLE `modulos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `nosotros`
--
ALTER TABLE `nosotros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `orden_compra`
--
ALTER TABLE `orden_compra`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT de la tabla `orden_compra_detalle`
--
ALTER TABLE `orden_compra_detalle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=127;

--
-- AUTO_INCREMENT de la tabla `palabras_claves`
--
ALTER TABLE `palabras_claves`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- AUTO_INCREMENT de la tabla `personas`
--
ALTER TABLE `personas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `portafolio`
--
ALTER TABLE `portafolio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `portafolio_imag`
--
ALTER TABLE `portafolio_imag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `productos_clonados`
--
ALTER TABLE `productos_clonados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `reaseguradoras`
--
ALTER TABLE `reaseguradoras`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `redes`
--
ALTER TABLE `redes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `red_social`
--
ALTER TABLE `red_social`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `seccion_galeria`
--
ALTER TABLE `seccion_galeria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `seccion_noticias`
--
ALTER TABLE `seccion_noticias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `servicios`
--
ALTER TABLE `servicios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `social_login`
--
ALTER TABLE `social_login`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT de la tabla `tallas`
--
ALTER TABLE `tallas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `telefonos`
--
ALTER TABLE `telefonos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;

--
-- AUTO_INCREMENT de la tabla `tipos_usuarios`
--
ALTER TABLE `tipos_usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tipo_negocio`
--
ALTER TABLE `tipo_negocio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `tipo_producto`
--
ALTER TABLE `tipo_producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT de la tabla `tipo_redes`
--
ALTER TABLE `tipo_redes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tipo_servicios`
--
ALTER TABLE `tipo_servicios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
