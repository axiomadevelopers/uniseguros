angular.module("ContentManagerApp")
	.controller("portafolioConsultaController", function($scope,$http,$location,serverDataMensajes,portafolioFactory,sesionFactory,idiomaFactory,portafolioFactory){
		$(".li-menu").removeClass("active");
		$("#li_productos").addClass("active");
		$(".a-menu").removeClass("active");
		$("#detalle_prod").addClass("active");

		$scope.titulo_pagina = "Portafolio";
		$scope.subtitulo_pagina = "Registro";
		$scope.titulo_registrar = "Registrar";
		$scope.titulo_cons = "Consultar";
		$scope.base_url = $("#base_url").val();

		$scope.id_portafolio = "";
		$scope.portafolio = {
						'id': '',
						'titulo':'',
						'descripcion':'',
						'codigo':'',
						'cliente':'',
						'descripcion':'',
						'url':'',
						'id_idioma' : '',
						'descripcion_idioma':'',
						'id_servicios':'',
						'titulo_servicio': '',
						'url' : '',
						'fecha' : '',
						'id_imagen': '',
		}
		////////////////////////////////////////////////////////////////////////////////////////
		$scope.iniciar_datatable = function(){
			//--Iniciar DataTable
			$('#myTable').DataTable({
			    language: {
			        "decimal": "",
			        "emptyTable": "No hay información",
			        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
			        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
			        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
			        "infoPostFix": "",
			        "thousands": ",",
			        "lengthMenu": "Mostrar _MENU_ Entradas",
			        "loadingRecords": "Cargando...",
			        "processing": "Procesando...",
			        "search": "Buscar:",
			        "zeroRecords": "Sin resultados encontrados",
			        "paginate": {
			            "first": "Primero",
			            "last": "Ultimo",
			            "next": "Siguiente",
			            "previous": "Anterior"
			        }
			    },
			    "order": [[ 0, "desc" ]]
			});
		}
		////////////////////////////////////////////////////////////////////////////////////////
		$scope.consultar_portafolio = function(){
			portafolioFactory.asignar_valores("","",$scope.base_url)
			portafolioFactory.cargar_portafolio(function(data){
				$scope.portafolio=data;
				//console.log($scope.portafolio);
			});
		}
		////////////////////////////////////////////////////////////////////////////////////////
		$scope.ver_portafolio = function(valor){
			var id = $("#ver"+valor).attr("data");
			$scope.id_portafolio = id;
			$("#id_portafolio").val($scope.id_portafolio)
			let form = document.getElementById('formConsultaPortafolio');
			form.action = "./portafolioVer";
			form.submit();
		}
		////////////////////////////////////////////////////////////////////////////////////////
		$scope.activar_registro = function(event){
			$scope.id_seleccionado_portafolio = []
			$scope.estatus_seleccionado_portafolio = []
			var caja = event.currentTarget.id;
			var atributos = $("#"+caja).attr("data");
			var arreglo_atributos = atributos.split("|");
			$scope.id_seleccionado_portafolio = arreglo_atributos[0];
			$scope.estatus_seleccionado_portafolio = arreglo_atributos[1];
			$("#cabecera_mensaje").text("Información:");
			//--
			if ($scope.estatus_seleccionado_portafolio==0){
				mensaje = "Desea modificar el estatus de este registro a publicado? ";
				$scope.estatus_seleccionado_portafolio=1;
			}else{
				mensaje = "Desea modificar el estatus de este registro a inactivo? ";
				$scope.estatus_seleccionado_portafolio=0
			}
			//($scope.estatus_seleccionado_categorias==1) ? $scope.estatus_seleccionado_categorias=0:$scope.estatus_seleccionado_categorias=1;

			//--
			$scope.modificar_estatus("activar_inactivar",mensaje)
		}
		//----------------------------------------------------------------
		$scope.modificar_estatus = function(opcion,mensaje){
			 swal({
				  title: 'Esta seguro?',
				  text: mensaje,
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Si!',
				  cancelButtonText: 'No',
				}).then((result) => {
					  if (result.value) {
						$scope.accion_estatus()
					  }
				})
		}
		//----------------------------------------------------------------
		$scope.accion_estatus = function(){
			//alert($scope.id_seleccionado_portafolio)
			$http.post($scope.base_url+"/Portafolio/modificarPortafolioEstatus",
			{
				 'id':$scope.id_seleccionado_portafolio,
				 'estatus':$scope.estatus_seleccionado_portafolio,

			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "modificacion_procesada"){
					Swal(
						  'Realizado!',
						  'El proceso fue ejecutado.',
						  'success'
					).then((result) => {
						  if (result.value) {
								let form = document.getElementById('formConsultaPortafolio');
								form.action = "./consultar_portafolio";
								form.submit();
						  }

					});
				}else if($scope.mensajes.mensaje == "existe_carrito"){
					Swal(
						  'No realizado!',
						  'El proceso no pudo ser ejecutado ya que el pŕoducto se encuentra asociado a una compra ',
						  'warning'
					)
				}else{
					Swal(
						  'No realizado!',
						  'El proceso no pudo ser ejecutado.',
						  'warning'
					)
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}
		////////////////////////////////////////////////////////////////////////////////////////
		$scope.eliminar_registro = function(event){
			$scope.id_seleccionado_portafolio = []
			$scope.estatus_seleccionado_portafolio = []
			var caja = event.currentTarget.id;
			//alert(caja);
			var atributos = $("#"+caja).attr("data");
			var arreglo_atributos = atributos.split("|");
			$scope.id_seleccionado_portafolio = arreglo_atributos[0];
			$scope.estatus_seleccionado_portafolio = arreglo_atributos[1];
			//console.log($scope.estatus_seleccionado_marca);
			$("#cabecera_mensaje").text("Información:");
			if ($scope.estatus_seleccionado_portafolio==2){
				mensaje = "Desea eliminar este registro? ";
			}
			$scope.modificar_estatus("eliminar",mensaje)
		}
		////////////////////////////////////////////////////////////////////////////////////////
		setTimeout(function(){
				$scope.iniciar_datatable();
		},500);
		////////////////////////////////////////////////////////////////////////////////////////
		$scope.consultar_portafolio();
	})
