angular.module("ContentManagerApp")
    .controller("auditoriaController", function($scope,$http,$location,serverDataMensajes,categoriasFactory,sesionFactory,idiomaFactory,auditoriaFactory){
       
        $(".li-menu").removeClass("active");
        $(".a-menu").removeClass("active");
        $("#auditoria").addClass("active");   
        $scope.titulo_pagina = "Consulta de auditoria";
        $scope.activo_img = "inactivo";
        //---------------------------------------------
        $scope.contactos = {
                                'id':'',
                                'number':'',
                                'nombre_usuario':'',
                                'modulo':'',
                                'accion':'',
                                'ip':'',
                                'fecha_hora':''
        }
      
        $scope.titulo_mensaje = [];
        //-----------------------------------------------
        $scope.base_url = $("#base_url").val();

        $scope.iniciar_datatable = function(){
            //--Iniciar DataTable
            $('#myTable').DataTable({
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                    "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
            });
        }
        $scope.consultar_auditoria = function(){
            auditoriaFactory.asignar_valores($scope.base_url)
            auditoriaFactory.cargar_auditoria(function(data){
                $.when(
                    $scope.auditoria=data
                ).then(function(){
                    $scope.iniciar_datatable();
                })
                //console.log($scope.auditoria)
            });
        }
     
        $scope.consultar_auditoria();

        //----------------------------------------------------------------
        /*setTimeout(function(){
            $scope.iniciar_datatable();
        },500);*/
        //----------------------------------------------------------------
    })
    .controller("bancoConsultasController", function($scope,$http,$location,serverDataMensajes,sesionFactory,idiomaFactory,bancosFactory){
        //Cuerpo declaraciones
        $(".li-menu").removeClass("active");
        $("#li_contactos").addClass("active");    
        $(".a-menu").removeClass("active");
        $("#banco").addClass("active");    
        $scope.titulo_pagina = "Consulta de Banco";
        $scope.banco = {
                                'id':'',
                                'id_idioma':'',
                                'correo':'',
                                'descripcion':'',
                                'estatus':'',
        }
        $scope.categorias_menu = "1";
        $scope.id_banco = "";
        $scope.base_url = $("#base_url").val();
        //-----------------------------------------------------
        //--Cuerpo de metodos  --/
        $scope.iniciar_datatable = function(){
            //--Iniciar DataTable
            $('#myTable').DataTable({
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                    "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
                "order": [[ 0, "asc" ]]
            });
            //console.log($scope.categorias);
        }
        $scope.consultar_banco = function(){
            bancosFactory.asignar_valores("","",$scope.base_url)
            bancosFactory.cargar_banco(function(data){
                $scope.banco=data;
                //console.log($scope.banco);                
            });
        }
        //---------------------------------------------------------------
        $scope.ver_banco = function(valor){
            var id = $("#ver"+valor).attr("data");
            $scope.id_banco = id;    
            $("#id_banco").val($scope.id_banco)
            let form = document.getElementById('formConsultaBanco');
            form.action = "./bancoVer";
            form.submit();
        }
        //----------------------------------------------------------------
        $scope.activar_registro = function(event){
            $scope.id_seleccionado_banco = []
            $scope.estatus_seleccionado_banco = []
            var caja = event.currentTarget.id;
            var atributos = $("#"+caja).attr("data");
            var arreglo_atributos = atributos.split("|");
            $scope.id_seleccionado_banco = arreglo_atributos[0];
            $scope.estatus_seleccionado_banco = arreglo_atributos[1];
            $("#cabecera_mensaje").text("Información:");            
            //--
            if ($scope.estatus_seleccionado_banco==0){
                mensaje = "Desea modificar el estatus de este registro a publicado? ";
                $scope.estatus_seleccionado_banco=1;
            }else{
                mensaje = "Desea modificar el estatus de este registro a inactivo? ";
                $scope.estatus_seleccionado_banco=0
            }
            //($scope.estatus_seleccionado_categorias==1) ? $scope.estatus_seleccionado_categorias=0:$scope.estatus_seleccionado_categorias=1;

            //--
            $scope.modificar_estatus("activar_inactivar",mensaje)
        }
        //----------------------------------------------------------------
        $scope.modificar_estatus = function(opcion,mensaje){
             swal({
                  title: 'Esta seguro?',
                  text: mensaje,
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Si!',
                  cancelButtonText: 'No',
                }).then((result) => {
                      if (result.value) {
                          $scope.accion_estatus()
                      }
                })
        }
        //----------------------------------------------------------------
        $scope.accion_estatus = function(){

            $http.post($scope.base_url+"/Banco/modificarBancoEstatus",
            {
                 'id':$scope.id_seleccionado_banco,    
                 'estatus':$scope.estatus_seleccionado_banco,     
            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "modificacion_procesada"){
                    Swal(
                          'Realizado!',
                          'El proceso fue ejecutado.',
                          'success'
                    ).then((result) => {
                          if (result.value) {
                                let form = document.getElementById('formConsultaBanco');
                                form.action = "./consultarBanco";
                                form.submit();
                          }
                    
                    });
                }else{
                    Swal(
                          'No realizado!',
                          'El proceso no pudo ser ejecutado.',
                          'warning'
                    )
                }
            }).error(function(data,estatus){
                console.log(data);
            });    
        }
        //----------------------------------------------------------------
        $scope.eliminar_registro = function(event){
            $scope.id_seleccionado_banco = []
            $scope.estatus_seleccionado_banco = []
            var caja = event.currentTarget.id;
            //alert(caja);
            var atributos = $("#"+caja).attr("data");
            var arreglo_atributos = atributos.split("|");
            $scope.id_seleccionado_banco = arreglo_atributos[0];
            $scope.estatus_seleccionado_banco = arreglo_atributos[1];
            //console.log($scope.estatus_seleccionado_banco);
            $("#cabecera_mensaje").text("Información:");
            if ($scope.estatus_seleccionado_banco==2){
                mensaje = "Desea eliminar este registro? ";
            }
            $scope.modificar_estatus("eliminar",mensaje)
        }
        //----------------------------------------------------------------
        //-- Cuerpo de funciones--/
        setTimeout(function(){
                $scope.iniciar_datatable();
        },500);
        $scope.consultar_banco();
        
        //-----------------------------------------------------
    })
    .controller("bancoController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,sesionFactory,idiomaFactory,bancosFactory){
        //Cuerpo declaraciones
        $(".li-menu").removeClass("active");
        $("#li_contactos").addClass("active");    
        $(".a-menu").removeClass("active");
        $("#banco").addClass("active");        
        $scope.titulo_pagina = "Banco";
        $scope.subtitulo_pagina  = "Registrar banco";
        $scope.activo_img = "inactivo";
        $scope.banco = {
                                'id':'',
                                'id_idioma':'',
                                'descripcion':'',
        }
        $scope.id_banco = ""
        $scope.searchBanco = []
        $scope.borrar_imagen = []
        $scope.currentTab = 'datos_basicos'
        $scope.titulo_registrar = "Registrar";
        $scope.titulo_text = "Pulse aquí para ingresar el contenido  del banco"
        $scope.base_url = $("#base_url").val();
        //--------------------------------------------------------
        //Cuerpo de metodos
        $scope.consultar_idioma = function(){
            idiomaFactory.asignar_valores("",$scope.base_url)
            idiomaFactory.cargar_idioma(function(data){
                $scope.idioma=data;
                //console.log($scope.idioma);
            });
        }
        $scope.agregarWisi = function(){
            //$('#text_editor').data("wysihtml5").editor.clear();
            $scope.banco.descripcion = $(".textarea_editor").val()
            $("#div_descripcion").html($scope.banco.descripcion)
            $("#cerrarModal").click();
        }
        $scope.wisi_modal = function(){
            $("#wisiModal").modal("show")
            $(".textarea_editor").data("wysihtml5").editor.setValue($scope.banco.descripcion)
        }
        //--
        $scope.registrarBanco = function(){

            //uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
            console.log($scope.banco);

            if($scope.validar_form()==true){
                //Para guardar
                if(($scope.banco.id!="")&&($scope.banco.id!=undefined)){
                    $scope.modificar_banco();
                }else{
                    $scope.insertar_banco();
                }

            }
            
            //desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
        }

        $scope.insertar_banco = function(){
            $http.post($scope.base_url+"/Banco/registrarBanco",
            {
                'descripcion': $scope.banco.descripcion,
                //'id_idioma'  : $scope.banco.id_idioma.id,
            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "registro_procesado"){
                    mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
                    $scope.limpiar_cajas_banco();
                }else if($scope.mensajes.mensaje == "existe"){
                    mostrar_notificacion("Mensaje","Ya existe un banco con ese titulo","warning");
                    $scope.limpiar_cajas_banco();
                }
                else{
                    mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
                }
            }).error(function(data,estatus){
                console.log(data);
            });
        }

        $scope.modificar_banco = function(){
            $http.post($scope.base_url+"/Banco/modificarBanco",
            {
                'id'         : $scope.banco.id,
                'descripcion': $scope.banco.descripcion,
                //'id_idioma'  : $scope.banco.id_idioma,
            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "registro_procesado"){
                    mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
                }else if($scope.mensajes.mensaje == "no_existe"){
                    mostrar_notificacion("Mensaje","No existe el banco","warning");
                }
                else{
                    mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
                }
            }).error(function(data,estatus){
                console.log(data);
            });
        }

        $scope.validar_form = function(){
            //console.log($scope.noticias)
            //if(($scope.banco.id_idioma=="NULL")||($scope.banco.id_idioma=="")){
            //    mostrar_notificacion("Campos no validos","Debe seleccionar el idioma","warning");
            //    return false;
            //}else 
            if($scope.banco.descripcion==""){
                mostrar_notificacion("Campos no validos","Debe ingresar la descripción del banco","warning");
                return false;
            }
            else{
                return true;
            }
        }

        $scope.limpiar_cajas_banco = function(){
            $scope.banco = {
                                'id':'',
                                'id_idioma':'',
                                'descripcion':'',
            }

            $('#text_editor').data("wysihtml5").editor.clear();
            $("#div_descripcion").html($scope.titulo_text);
        }
        //--
        $scope.consultarBancoIndividual = function(){

            uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

            bancosFactory.asignar_valores("",$scope.id_banco,$scope.base_url)
            bancosFactory.cargar_banco(function(data){
                $scope.banco=data[0];
                //console.log(data[0]);
                $("#div_descripcion").html($scope.banco.descripcion)
                $scope.titulo_registrar = "Modificar";
                $scope.subtitulo_pagina  = "Modificar banco";
                setTimeout(function(){
                    $('#idioma > option[value="'+$scope.banco.id_idioma+'"]').prop('selected', true);
                },300);
                $("#idioma").prop('disabled', true);
            });

            desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

        }
        //---
        //--------------------------------------------------------
        //Cuerpo de llamados
        $scope.consultar_idioma();
        //--Si tiene id categoria .... consulto los datos relacionados a ese id, esto es cuando proviene de una consulta
        $scope.id_banco  = $("#id_banco").val();
        if($scope.id_banco){
            $scope.consultarBancoIndividual();
        }else{
            $("#idioma").prop('disabled', false);
        }
        //--------------------------------------------------------
    })
    .controller("categoriasConsultasController", function($scope,$http,$location,serverDataMensajes,categoriasFactory,sesionFactory,idiomaFactory){
        //Cuerpo declaraciones
        $(".li-menu").removeClass("active");
        $("#li_configuracion").addClass("active");
        $(".a-menu").removeClass("active");
        $("#categoria").addClass("active");
        $scope.titulo_pagina = "Consulta de Categorías";
        $scope.activo_img = "inactivo";
        $scope.categorias = {
                                'id':'',
                                'descripcion':'',
                                'estatus':''
        }
        $scope.categorias_menu = "1";
        $scope.id_categoria = "";
        $scope.base_url = $("#base_url").val();
        //-----------------------------------------------------
        //--Cuerpo de metodos  --/
        $scope.iniciar_datatable = function(){
            //--Iniciar DataTable
            $('#myTable').DataTable({
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                    "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
                 "order": [[ 0, "desc" ]]    
            });
            //console.log($scope.categorias);
        }
        $scope.consultar_categorias = function(){
            categoriasFactory.asignar_valores("","",$scope.base_url)
            categoriasFactory.cargar_categorias(function(data){
                $scope.categorias=data;
            });
        }
        //---------------------------------------------------------------
        $scope.ver_categoria = function(valor){
            var id = $("#ver"+valor).attr("data");
            $scope.id_categoria = id;
            $("#id_categoria").val($scope.id_categoria)
            let form = document.getElementById('formConsultaCategorias');
            form.action = "./categoriasVer";
            form.submit();
        }
        //----------------------------------------------------------------
        $scope.activar_registro = function(event){
            $scope.id_seleccionado_categorias = []
            $scope.estatus_seleccionado_categorias = []
            var caja = event.currentTarget.id;
            var atributos = $("#"+caja).attr("data");
            var arreglo_atributos = atributos.split("|");
            $scope.id_seleccionado_categorias = arreglo_atributos[0];
            $scope.estatus_seleccionado_categorias = arreglo_atributos[1];
            $("#cabecera_mensaje").text("Información:");
            //--
            if ($scope.estatus_seleccionado_categorias==0){
                mensaje = "Desea modificar el estatus de este registro a publicado? ";
                $scope.estatus_seleccionado_categorias=1;
            }else{
                mensaje = "Desea modificar el estatus de este registro a inactivo? ";
                $scope.estatus_seleccionado_categorias=0
            }
            //($scope.estatus_seleccionado_categorias==1) ? $scope.estatus_seleccionado_categorias=0:$scope.estatus_seleccionado_categorias=1;

            //--
            $scope.modificar_estatus("activar_inactivar",mensaje)
        }
        //----------------------------------------------------------------
        $scope.modificar_estatus = function(opcion,mensaje){
             swal({
                  title: 'Esta seguro?',
                  text: mensaje,
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Si!',
                  cancelButtonText: 'No',
                }).then((result) => {
                      if (result.value) {
                          $scope.accion_estatus()
                      }
                })
        }
        //----------------------------------------------------------------
        $scope.accion_estatus = function(){

            $http.post($scope.base_url+"/Categorias/modificarCategoriasEstatus",
            {
                 'id':$scope.id_seleccionado_categorias,
                 'estatus':$scope.estatus_seleccionado_categorias,

            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "modificacion_procesada"){
                    Swal(
                          'Realizado!',
                          'El proceso fue ejecutado.',
                          'success'
                    ).then((result) => {
                          if (result.value) {
                                let form = document.getElementById('formConsultaCategorias');
                                form.action = "./consultarCategorias";
                                form.submit();
                          }

                    });
                }else{
                    Swal(
                          'No realizado!',
                          'El proceso no pudo ser ejecutado.',
                          'warning'
                    )
                }
            }).error(function(data,estatus){
                console.log(data);
            });
        }
        //----------------------------------------------------------------
        $scope.eliminar_registro = function(event){
            $scope.id_seleccionado_categorias = []
            $scope.estatus_seleccionado_categorias = []
            var caja = event.currentTarget.id;
            //alert(caja);
            var atributos = $("#"+caja).attr("data");
            var arreglo_atributos = atributos.split("|");
            $scope.id_seleccionado_categorias = arreglo_atributos[0];
            $scope.estatus_seleccionado_categorias = arreglo_atributos[1];
            //console.log($scope.estatus_seleccionado_categorias);
            $("#cabecera_mensaje").text("Información:");
            if ($scope.estatus_seleccionado_categorias==2){
                mensaje = "Desea eliminar este registro? ";
            }
            $scope.modificar_estatus("eliminar",mensaje)
        }
        //----------------------------------------------------------------
        //-- Cuerpo de funciones--/
        setTimeout(function(){
                $scope.iniciar_datatable();
        },500);
        $scope.consultar_categorias();

        //-----------------------------------------------------
    })
    .controller("categoriasController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,sesionFactory,idiomaFactory,categoriasFactory){
        //Cuerpo declaraciones
        $(".li-menu").removeClass("active");
        $("#li_configuracion").addClass("active");
        $(".a-menu").removeClass("active");
        $("#categoria").addClass("active");

        $scope.titulo_pagina = "Categorias";
        $scope.subtitulo_pagina  = "Registrar categorias";
        $scope.activo_img = "inactivo";
        $scope.categorias = {
                                'id':'',
                                'descripcion':'',
                                'estatus':''
        }
        $scope.titulo_registrar = "Registrar";
        $scope.categorias_menu = "1";
        $scope.base_url = $("#base_url").val();
        //Cuerpo de metodos
        //---------------------------------------------------------------
        $scope.limpiar_cajas_categorias = function(){
            $scope.categorias = {
                                'id':'',
                                'descripcion':'',
                                'estatus':''
            }
        }
        //--
        $scope.validar_form = function(){
            if($scope.categorias.descripcion==""){
                mostrar_notificacion("Campos no validos","Debe ingresar la descripcion","warning");
                return false;
            }else{
                return true;
            }
        }
        //--
        $scope.consultarCategoriaIndividual = function(){
            
            uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

            categoriasFactory.asignar_valores("",$scope.id_categoria,$scope.base_url)
            categoriasFactory.cargar_categorias(function(data){
                $scope.categorias=data[0];
                $scope.titulo_registrar = "Modificar";
                $scope.subtitulo_pagina  = "Modificar categorías";
            });

            desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

        }
        //--
        $scope.registrarCategorias = function(){

            uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

            if($scope.validar_form()==true){
                //Para guardar
                //alert("id_personas:"+$scope.doctor.id_personas);
                //alert("id_doctor:"+$scope.doctor.id);
                $scope.categorias.id = $scope.id_categoria;
                if(($scope.categorias.id!=undefined)&&($scope.categorias.id!="")){
                    $scope.modificar_categorias();
                }else{
                    $scope.insertar_categorias();
                }
            }
            
            //desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
        
        }
        //---
        $scope.insertar_categorias = function(){
            $http.post($scope.base_url+"/Categorias/registrarCategorias",
            {
                 'descripcion':$scope.categorias.descripcion,
            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "registro_procesado"){
                    mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
                    $scope.limpiar_cajas_categorias();
                }else if($scope.mensajes.mensaje == "existe"){
                    mostrar_notificacion("Mensaje","Ya existe una categoría con esa descripción","warning");
                }
                else{
                    mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
                }
            }).error(function(data,estatus){
                console.log(data);
            });
        }
        //---
        $scope.modificar_categorias = function(){
            $http.post($scope.base_url+"/Categorias/modificarCategorias",
            {
                 'id':$scope.id_categoria,
                 'descripcion':$scope.categorias.descripcion,

            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "modificacion_procesada"){
                    mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
                }else if($scope.mensajes.mensaje == "existe_nombre"){
                    mostrar_notificacion("Mensaje","Ya existe una categoria con ese nombre","warning");
                }
                else{
                    mostrar_notificacion("Mensaje","Ocurrió un error inesperado","warning");
                }
            }).error(function(data,estatus){
                console.log(data);
            });
        }

        $(".tst1").click(function(){


     });
    //---------------------------------------------------------------
    //Cuerpo de Llamados a metodos
    //--Si tiene id categoria .... consulto los datos relacionados a ese id, esto es cuando proviene de una consulta
    $scope.id_categoria  = $("#id_categoria").val();
    if($scope.id_categoria){
        $scope.consultarCategoriaIndividual();
    }
    //---------------------------------------------------------
    })
    .controller("ContactosController", function($scope,$http,$location,serverDataMensajes,categoriasFactory,sesionFactory,idiomaFactory,contactosFactory){
       
        $(".li-menu").removeClass("active");
        $("#li_contactos").addClass("active");
        $(".a-menu").removeClass("active");
        $("#contactos").addClass("active");   
        $scope.titulo_pagina = "Consulta de Contactos";
        $scope.activo_img = "inactivo";
        //---------------------------------------------
        $scope.contactos = {
                                'id':'',
                                'nombres':'',
                                'email':'',
                                'telefono':'',
                                'mensaje':''
        }
        $scope.detalle = {
                                'id':'',
                                'nombres':'',
                                'email':'',
                                'telefono':'',
                                'mensaje':''
        }
        $scope.titulo_mensaje = [];
        //-----------------------------------------------
        $scope.nosotros_menu = "1";
        $scope.id_nosotros = "";
        $scope.base_url = $("#base_url").val();

        $scope.iniciar_datatable = function(){
            //--Iniciar DataTable
            $('#myTable').DataTable({
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                    "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
            });
        }
        $scope.consultar_contactos = function(){
            contactosFactory.asignar_valores($scope.base_url)
            contactosFactory.cargar_contactos(function(data){
                $scope.contactos=data;
                //console.log($scope.contactos)
            });
        }
        //--Para visualizar resumen
        $scope.ver_detalle = function(index){
            $("#modal_mensaje").modal("show");
            $scope.titulo_mensaje = "Resumén de contacto"
            $scope.detalle = $scope.contactos[index]
        }
        
        //----------------------------------------------------------------
        setTimeout(function(){
            $scope.iniciar_datatable();
        },500);

        $scope.consultar_contactos();

        //----------------------------------------------------------------
    })
    .controller("cuentaBancariaConsultasController", function($scope,$http,$location,serverDataMensajes,sesionFactory,idiomaFactory,bancosFactory,cuentaBancariaFactory){
        //Cuerpo declaraciones
        $(".li-menu").removeClass("active");
        $("#li_contactos").addClass("active");    
        $(".a-menu").removeClass("active");
        $("#cuenta_bancaria").addClass("active");    
        $scope.titulo_pagina = "Consulta Cuentas Bancarias";
        $scope.banco = {
                                'id':'',
                                'id_idioma':'',
                                'correo':'',
                                'descripcion':'',
                                'estatus':'',
        }
        $scope.categorias_menu = "1";
        $scope.id_cuenta = "";
        $scope.base_url = $("#base_url").val();
        //-----------------------------------------------------
        //--Cuerpo de metodos  --/
        $scope.iniciar_datatable = function(){
            //--Iniciar DataTable
            $('#myTable').DataTable({
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                    "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
                "order": [[ 0, "asc" ]]
            });
            //console.log($scope.categorias);
        }
        $scope.consultar_banco = function(){
            cuentaBancariaFactory.asignar_valores("","",$scope.base_url)
            cuentaBancariaFactory.cargar_banco(function(data){
                $scope.banco=data;
                //console.log($scope.banco);                
            });
        }
        //---------------------------------------------------------------
        $scope.ver_banco = function(valor){
            var id = $("#ver"+valor).attr("data");
            $scope.id_cuenta = id;    
            $("#id_cuenta").val($scope.id_cuenta)
            let form = document.getElementById('formConsultaBanco');
            form.action = "./cuentasVer";
            form.submit();
        }
        //----------------------------------------------------------------
        $scope.activar_registro = function(event){
            $scope.id_seleccionado_banco = []
            $scope.estatus_seleccionado_banco = []
            var caja = event.currentTarget.id;
            var atributos = $("#"+caja).attr("data");
            var arreglo_atributos = atributos.split("|");
            $scope.id_seleccionado_banco = arreglo_atributos[0];
            $scope.estatus_seleccionado_banco = arreglo_atributos[1];
            $("#cabecera_mensaje").text("Información:");            
            //--
            if ($scope.estatus_seleccionado_banco==0){
                mensaje = "Desea modificar el estatus de este registro a publicado? ";
                $scope.estatus_seleccionado_banco=1;
            }else{
                mensaje = "Desea modificar el estatus de este registro a inactivo? ";
                $scope.estatus_seleccionado_banco=0
            }
            //($scope.estatus_seleccionado_categorias==1) ? $scope.estatus_seleccionado_categorias=0:$scope.estatus_seleccionado_categorias=1;

            //--
            $scope.modificar_estatus("activar_inactivar",mensaje)
        }
        //----------------------------------------------------------------
        $scope.modificar_estatus = function(opcion,mensaje){
             swal({
                  title: 'Esta seguro?',
                  text: mensaje,
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Si!',
                  cancelButtonText: 'No',
                }).then((result) => {
                      if (result.value) {
                          $scope.accion_estatus()
                      }
                })
        }
        //----------------------------------------------------------------
        $scope.accion_estatus = function(){

            $http.post($scope.base_url+"/CuentaBancaria/modificarBancoEstatus",
            {
                 'id':$scope.id_seleccionado_banco,    
                 'estatus':$scope.estatus_seleccionado_banco,     
            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "modificacion_procesada"){
                    Swal(
                          'Realizado!',
                          'El proceso fue ejecutado.',
                          'success'
                    ).then((result) => {
                          if (result.value) {
                                let form = document.getElementById('formConsultaBanco');
                                form.action = "./consultarCuentas";
                                form.submit();
                          }
                    
                    });
                }else{
                    Swal(
                          'No realizado!',
                          'El proceso no pudo ser ejecutado.',
                          'warning'
                    )
                }
            }).error(function(data,estatus){
                console.log(data);
            });    
        }
        //----------------------------------------------------------------
        $scope.eliminar_registro = function(event){
            $scope.id_seleccionado_banco = []
            $scope.estatus_seleccionado_banco = []
            var caja = event.currentTarget.id;
            //alert(caja);
            var atributos = $("#"+caja).attr("data");
            var arreglo_atributos = atributos.split("|");
            $scope.id_seleccionado_banco = arreglo_atributos[0];
            $scope.estatus_seleccionado_banco = arreglo_atributos[1];
            //console.log($scope.estatus_seleccionado_banco);
            $("#cabecera_mensaje").text("Información:");
            if ($scope.estatus_seleccionado_banco==2){
                mensaje = "Desea eliminar este registro? ";
            }
            $scope.modificar_estatus("eliminar",mensaje)
        }
        //----------------------------------------------------------------
        //-- Cuerpo de funciones--/
        setTimeout(function(){
                $scope.iniciar_datatable();
        },500);
        $scope.consultar_banco();
        
        //-----------------------------------------------------
    })
    .controller("cuentaBancariaController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,sesionFactory,idiomaFactory,bancosFactory,cuentaBancariaFactory){
        //Cuerpo declaraciones
        $(".li-menu").removeClass("active");
        $("#li_contactos").addClass("active");    
        $(".a-menu").removeClass("active");
        $("#cuenta_bancaria").addClass("active");        
        $scope.titulo_pagina = "Cuentas Bancarias";
        $scope.subtitulo_pagina  = "Registrar banco";
        $scope.activo_img = "inactivo";
        $scope.cuenta_bancaria = {
                                'id':'',
                                'id_banco':'',
                                'descripcion':'',
        }
        $scope.id_cuenta = ""
        $scope.searchBanco = []
        $scope.borrar_imagen = []
        $scope.currentTab = 'datos_basicos'
        $scope.titulo_registrar = "Registrar";
        $scope.titulo_text = "Pulse aquí para ingresar el contenido  del banco"
        $scope.base_url = $("#base_url").val();
        //--------------------------------------------------------
        //Cuerpo de metodos
        $scope.consultar_idioma = function(){
            idiomaFactory.asignar_valores("",$scope.base_url)
            idiomaFactory.cargar_idioma(function(data){
                $scope.idioma=data;
                //console.log($scope.idioma);
            });
        }
        //Bancos registrados
        $scope.consultar_bancos = function(){
            bancosFactory.asignar_valores("","",$scope.base_url)
            bancosFactory.cargar_bancos_registrados(function(data){
                $scope.bancos_registrados=data;
                //console.log($scope.idioma);
            });
        }
        $scope.agregarWisi = function(){
            //$('#text_editor').data("wysihtml5").editor.clear();
            $scope.cuenta_bancaria.descripcion = $(".textarea_editor").val()
            $("#div_descripcion").html($scope.cuenta_bancaria.descripcion)
            $("#cerrarModal").click();
        }
        $scope.wisi_modal = function(){
            $("#wisiModal").modal("show")
            $(".textarea_editor").data("wysihtml5").editor.setValue($scope.cuenta_bancaria.descripcion)
        }
        //--
        $scope.registrarBanco = function(){

            uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

            if($scope.validar_form()==true){
                //Para guardar
                if(($scope.cuenta_bancaria.id!="")&&($scope.cuenta_bancaria.id!=undefined)){
                    $scope.modificar_banco();
                }else{
                    $scope.insertar_banco();
                }

            }
            
            //desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
        }

        $scope.insertar_banco = function(){
            $http.post($scope.base_url+"/CuentaBancaria/registrarBanco",
            {
                'descripcion': $scope.cuenta_bancaria.descripcion,
                'id_banco'  : $scope.cuenta_bancaria.id_banco.id,
            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "registro_procesado"){
                    mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
                    $scope.limpiar_cajas_banco();
                }else if($scope.mensajes.mensaje == "existe"){
                    mostrar_notificacion("Mensaje","Ya existe la cuenta bancaria","warning");
                    $scope.limpiar_cajas_banco();
                }
                else{
                    mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
                }
            }).error(function(data,estatus){
                console.log(data);
            });
        }

        $scope.modificar_banco = function(){
            $http.post($scope.base_url+"/CuentaBancaria/modificarBanco",
            {
                'id'         : $scope.cuenta_bancaria.id,
                'descripcion': $scope.cuenta_bancaria.descripcion,
                //'id_idioma'  : $scope.cuenta_bancaria.id_idioma,
            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "registro_procesado"){
                    mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
                }else if($scope.mensajes.mensaje == "no_existe"){
                    mostrar_notificacion("Mensaje","No existe el banco","warning");
                }
                else{
                    mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
                }
            }).error(function(data,estatus){
                console.log(data);
            });
        }

        $scope.validar_form = function(){
            console.log($scope.cuenta_bancaria);

            if(($scope.cuenta_bancaria.id_banco=="NULL")||($scope.cuenta_bancaria.id_banco=="")){
                mostrar_notificacion("Campos no validos","Debe seleccionar el banco","warning");
                return false;
            }else if($scope.cuenta_bancaria.descripcion==""){
                mostrar_notificacion("Campos no validos","Debe ingresar la cuenta bancaria","warning");
                return false;
            }
            else{
                return true;
            }
        }

        $scope.limpiar_cajas_banco = function(){
            $scope.cuenta_bancaria = {
                                'id':'',
                                'id_banco':'',
                                'descripcion':'',
            }

            $('#text_editor').data("wysihtml5").editor.clear();
            $("#div_descripcion").html($scope.titulo_text);
        }
        //--
        $scope.consultarBancoIndividual = function(){

            uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

            cuentaBancariaFactory.asignar_valores("",$scope.id_cuenta,$scope.base_url)
            cuentaBancariaFactory.cargar_banco(function(data){
                $scope.cuenta_bancaria=data[0];
                $("#div_descripcion").html($scope.cuenta_bancaria.descripcion)
                $scope.titulo_registrar = "Modificar";
                $scope.subtitulo_pagina  = "Modificar banco";
                setTimeout(function(){
                    $('#banco_registrado > option[value="'+$scope.cuenta_bancaria.id_banco+'"]').prop('selected', true);
                },300);
                console.log($scope.cuenta_bancaria);
                $("#banco_registrado").prop('disabled', true);        
                console.log($scope.cuenta_bancaria);

            });

            desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

        }
        //---
        //--------------------------------------------------------
        //Cuerpo de llamados
        $scope.consultar_idioma();
        $scope.consultar_bancos();
        //--Si tiene id categoria .... consulto los datos relacionados a ese id, esto es cuando proviene de una consulta
        $scope.id_cuenta  = $("#id_cuenta").val();
        if($scope.id_cuenta){
            $scope.consultarBancoIndividual();
        }
        //--------------------------------------------------------
    })
    .controller("descripcionController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,sesionFactory,idiomaFactory,descripcionFactory){
        //Cuerpo declaraciones
        $(".li-menu").removeClass("active");
        $("#li_configuracion").addClass("active");    
        $(".a-menu").removeClass("active");
        $("#meta_description").addClass("active");        
        $scope.titulo_pagina = "Meta Descripción";
        $scope.subtitulo_pagina  = "Registrar descripción";
        $scope.descripcion = {
                                'id':'',
                                'descripcion':''
        }
        $scope.id = ""
        $scope.searchFooter = []
        $scope.titulo_registrar = "Registrar";
        $scope.base_url = $("#base_url").val();
        //--------------------------------------------------------
        //Cuerpo de metodos

        $scope.registrarDescripcion = function(){

            uploader_reg("#div_mensaje","#btn-limpiar,#btn-registrar");

            if($scope.validar_form()==true){
                //Para guardar
                if(($scope.descripcion.id!="")&&($scope.descripcion.id!=undefined)){
                    $scope.modificar_descripcion();
                }else{
                    $scope.insertar_descripcion();
                }

            }
            
            //desbloquear_pantalla("#div_mensaje","#btn-registrar");
        }

        $scope.insertar_descripcion = function(){
            $http.post($scope.base_url+"/descripcion/registrarDescripcion",
            {
                'descripcion': $scope.descripcion.descripcion,
            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "registro_procesado"){
                    $scope.descripcion.id = $scope.mensajes.id
                    mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
                    $scope.limpiar_cajas_descripcion();
                }
                else{
                    mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
                }
            }).error(function(data,estatus){
                console.log(data);
            });
        }

        $scope.modificar_descripcion = function(){
            $http.post($scope.base_url+"/descripcion/modificarDescripcion",
            {
                'id'         : $scope.descripcion.id,
                'descripcion': $scope.descripcion.descripcion
            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "modificacion_procesada"){
                    mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
                }else if($scope.mensajes.mensaje == "no_existe"){
                    mostrar_notificacion("Mensaje","No existe el registro","warning");
                }
                else{
                    mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
                }
            }).error(function(data,estatus){
                console.log(data);
            });
        }

        $scope.validar_form = function(){
            if($scope.descripcion.descripcion==""){
                mostrar_notificacion("Campos no validos","Debe ingresar la descripción","warning");
                return false;
            }
            else{
                return true;
            }
        }

        $scope.limpiar_cajas_descripcion = function(){
            $scope.footer = {
                                'id':'',
                                'id_idioma':'',
                                'correo':'',
                                'descripcion':'',
                                'estatus':'',
            }

            $('#text_editor').data("wysihtml5").editor.clear();
            $("#div_descripcion").html($scope.titulo_text);
        }
        //--
        $scope.consultarDescripcion = function(){

            uploader_reg("#div_mensaje","#btn-registrar");

            descripcionFactory.asignar_valores("",$scope.base_url)

            descripcionFactory.cargar_descripcion(function(data){
                if(data[0]){
                    $scope.descripcion=data[0];
                    $scope.titulo_registrar = "Modificar";
                    $scope.subtitulo_pagina  = "Modificar descripción";
                }
            });
            desbloquear_pantalla("#div_mensaje","#btn-registrar");
        }
        //---
        //--------------------------------------------------------
        //Cuerpo de llamados
        $scope.consultarDescripcion()
        //--------------------------------------------------------
    })
    .controller("DetalleTproductosConsultaController", function($scope,$http,$location,serverDataMensajes,categoriasFactory,sesionFactory,idiomaFactory,detalleproductosFactory){
        $(".li-menu").removeClass("active");
        $("#li_productos1").addClass("active");    
        $(".a-menu").removeClass("active");
        $("#detalle_t_producto").addClass("active");    
        $scope.titulo_pagina = "Consulta de Detalles de Producto";
        $scope.activo_img = "inactivo";
        $scope.productos = {
                            'id': '',
                            'idioma': '',
                            'id_idioma' : '',
                            'somos': '',
                            'digital_agency': '',
                            'estatus' : '',
                            'id_imagen' : '',
                            'imagen' : ''
        }
        $scope.nosotros_menu = "1";
        $scope.id_nosotros = "";
        $scope.base_url = $("#base_url").val();

        $scope.iniciar_datatable = function(){
            //--Iniciar DataTable
            $('#myTable').DataTable({
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                    "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
                "order": [[4, "asc" ],[5, "asc" ]]
            });
        }

        $scope.consultar_detalles = function(){
            detalleproductosFactory.asignar_valores("","",$scope.base_url)
            detalleproductosFactory.cargar_productos(function(data){
                $.when(
                    $scope.productos_detalles = data)
                .then(function(){
                    $scope.iniciar_datatable()
                });
                //console.log($scope.productos);
            });
        }

        $scope.ver_producto = function(valor){
            var id = $("#ver"+valor).attr("data");
            $scope.id_detalle_productos = id;
            //console.log($scope.id_detalle_productos);
            $("#id_detalle_productos").val($scope.id_detalle_productos);
            let form = document.getElementById('formConsultaProductos');
            form.action = "./productosVer";
            form.submit();
        }

        $scope.activar_registro = function(event){
                    $scope.id_seleccionado_productos = []
                    $scope.estatus_seleccionado_productos = []
                    $scope.id_seleccionado_tipo_productos = []
                    $scope.id_seleccionado___productos = []

                    var caja = event.currentTarget.id;
                    var atributos = $("#"+caja).attr("data");
                    var arreglo_atributos = atributos.split("|");

                    $scope.id_seleccionado_productos = arreglo_atributos[0];
                    $scope.estatus_seleccionado_productos = arreglo_atributos[1];
                    $scope.id_seleccionado___productos = arreglo_atributos[2];
                    $scope.id_seleccionado_tipo_productos = arreglo_atributos[3];


                    
                    $("#cabecera_mensaje").text("Información:");

                    if ($scope.estatus_seleccionado_productos==0){
                        mensaje = "Desea modificar el estatus de este registro a publicado? ";
                        $scope.estatus_seleccionado_productos=1;
                    }else{
                        mensaje = "Desea modificar el estatus de este registro a inactivo? ";
                        $scope.estatus_seleccionado_productos=0
                    }
                    $scope.modificar_estatus("activar_inactivar",mensaje)
                }
                //----------------------------------------------------------------
                $scope.modificar_estatus = function(opcion,mensaje){
                     swal({
                          title: 'Esta seguro?',
                          text: mensaje,
                          type: 'warning',
                          showCancelButton: true,
                          confirmButtonColor: '#3085d6',
                          cancelButtonColor: '#d33',
                          confirmButtonText: 'Si!',
                          cancelButtonText: 'No',
                        }).then((result) => {
                              if (result.value) {
                                  $scope.accion_estatus()
                              }
                        })
                }
                //----------------------------------------------------------------
                $scope.accion_estatus = function(){

                    $http.post($scope.base_url+"/DetalleTproductos/modificarProductosEstatus",
                    {
                         'id':$scope.id_seleccionado_productos,
                         'estatus':$scope.estatus_seleccionado_productos,
                         'id_productos':$scope.id_seleccionado___productos,
                         'id_tipo_producto':$scope.id_seleccionado_tipo_productos


                    }).success(function(data, estatus, headers, config){
                        $scope.mensajes  = data;
                        
                        if($scope.mensajes.mensaje == "modificacion_procesada"){
                            Swal(
                                  'Realizado!',
                                  'El proceso fue ejecutado.',
                                  'success'
                            ).then((result) => {
                                  if (result.value) {
                                        let form = document.getElementById('formConsultaProductos');
                                        form.action = "./consultar_productos";
                                        form.submit();
                                  }

                            });
                        }else{
                            Swal(
                                  'No realizado!',
                                  'El proceso no pudo ser ejecutado.',
                                  'warning'
                            )
                        }
                    }).error(function(data,estatus){
                        console.log(data);
                    });
                }
                //----------------------------------------------------------------
                //----------------------------------------------------------------
        $scope.eliminar_registro = function(event){

            $scope.id_seleccionado_productos = []
            $scope.estatus_seleccionado_productos = []
            $scope.id_seleccionado_tipo_productos = []
            $scope.id_seleccionado___productos = []
            var caja = event.currentTarget.id;
            //alert(caja);
            var atributos = $("#"+caja).attr("data");
            var arreglo_atributos = atributos.split("|");
            $scope.id_seleccionado_productos = arreglo_atributos[0];
            $scope.estatus_seleccionado_productos = arreglo_atributos[1];
            $scope.id_seleccionado___productos = arreglo_atributos[2];
            $scope.id_seleccionado_tipo_productos = arreglo_atributos[3];
            //console.log($scope.estatus_seleccionado_productos);
            $("#cabecera_mensaje").text("Información:");
            if ($scope.estatus_seleccionado_productos==2){
                mensaje = "Desea eliminar este registro? ";
            }
            $scope.modificar_estatus("eliminar",mensaje)
        }
        //----------------------------------------------------------------
        $scope.consultar_detalles()
        /*
        setTimeout(function(){
            $scope.iniciar_datatable()
        },5)*/
    })
    .controller("DetalleTserviciosConsultaController", function($scope,$http,$location,serverDataMensajes,categoriasFactory,sesionFactory,idiomaFactory,detalleserviciosFactory){
        $(".li-menu").removeClass("active");
        $("#li_servicios1").addClass("active");    
        $(".a-menu").removeClass("active");
        $("#detalle_t_servicio").addClass("active");    
        $scope.titulo_pagina = "Consulta de Detalles de Servicios";
        $scope.activo_img = "inactivo";
        $scope.servicios = {
                            'id': '',
                            'idioma': '',
                            'id_idioma' : '',
                            'somos': '',
                            'digital_agency': '',
                            'estatus' : '',
                            'id_imagen' : '',
                            'imagen' : ''
        }
        $scope.nosotros_menu = "1";
        $scope.id_nosotros = "";
        $scope.base_url = $("#base_url").val();

        $scope.iniciar_datatable = function(){
            //--Iniciar DataTable
            $('#myTable').DataTable({
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                    "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
                "order": [[4 ,"asc" ],[5, "asc" ],[0, "asc" ]],

            });
        }

        $scope.consultar_detalles = function(){
            detalleserviciosFactory.asignar_valores("","",$scope.base_url)
            detalleserviciosFactory.cargar_servicios(function(data){
                $scope.servicios = data;
                //console.log($scope.servicios);
            });
        }

        $scope.ver_producto = function(valor){
            var id = $("#ver"+valor).attr("data");
            $scope.id_detalle_servicios = id;
            //console.log($scope.id_detalle_servicios);
            $("#id_detalle_servicios").val($scope.id_detalle_servicios);
            let form = document.getElementById('formConsultaProductos');
            form.action = "./serviciosVer";
            form.submit();
        }

        $scope.activar_registro = function(event){
                    $scope.id_seleccionado_productos = []
                    $scope.estatus_seleccionado_productos = []
                    $scope.id_seleccionado_tipo_productos = []
                    $scope.id_seleccionado___productos = []
                    
                    var caja = event.currentTarget.id;
                    var atributos = $("#"+caja).attr("data");
                    var arreglo_atributos = atributos.split("|");

                    $scope.id_seleccionado_productos = arreglo_atributos[0];
                    $scope.estatus_seleccionado_productos = arreglo_atributos[1];
                    $scope.id_seleccionado___productos = arreglo_atributos[2];
                    $scope.id_seleccionado_tipo_productos = arreglo_atributos[3];

                    $("#cabecera_mensaje").text("Información:");

                    if ($scope.estatus_seleccionado_productos==0){
                        mensaje = "Desea modificar el estatus de este registro a publicado? ";
                        $scope.estatus_seleccionado_productos=1;
                    }else{
                        mensaje = "Desea modificar el estatus de este registro a inactivo? ";
                        $scope.estatus_seleccionado_productos=0
                    }
                    $scope.modificar_estatus("activar_inactivar",mensaje)
                }
                //----------------------------------------------------------------
                $scope.modificar_estatus = function(opcion,mensaje){
                     swal({
                          title: 'Esta seguro?',
                          text: mensaje,
                          type: 'warning',
                          showCancelButton: true,
                          confirmButtonColor: '#3085d6',
                          cancelButtonColor: '#d33',
                          confirmButtonText: 'Si!',
                          cancelButtonText: 'No',
                        }).then((result) => {
                              if (result.value) {
                                  $scope.accion_estatus()
                              }
                        })
                }
                //----------------------------------------------------------------
                $scope.accion_estatus = function(){

                    $http.post($scope.base_url+"/DetalleTservicios/modificarServiciosEstatus",
                    {
                         'id':$scope.id_seleccionado_productos,
                         'estatus':$scope.estatus_seleccionado_productos,
                         'id_servicios':$scope.id_seleccionado___productos,
                         'id_tipo_servicio':$scope.id_seleccionado_tipo_productos

                    }).success(function(data, estatus, headers, config){
                        $scope.mensajes  = data;
                        
                        if($scope.mensajes.mensaje == "modificacion_procesada"){
                            Swal(
                                  'Realizado!',
                                  'El proceso fue ejecutado.',
                                  'success'
                            ).then((result) => {
                                  if (result.value) {
                                        let form = document.getElementById('formConsultaProductos');
                                        form.action = "./consultar_servicios";
                                        form.submit();
                                  }

                            });
                        }else{
                            Swal(
                                  'No realizado!',
                                  'El proceso no pudo ser ejecutado.',
                                  'warning'
                            )
                        }
                    }).error(function(data,estatus){
                        console.log(data);
                    });
                }
                //----------------------------------------------------------------
                //----------------------------------------------------------------
        $scope.eliminar_registro = function(event){
            $scope.id_seleccionado_productos = []
            $scope.estatus_seleccionado_productos = []
            $scope.id_seleccionado_tipo_productos = []
            $scope.id_seleccionado___productos = []

            var caja = event.currentTarget.id;
            //alert(caja);
            var atributos = $("#"+caja).attr("data");
            var arreglo_atributos = atributos.split("|");
            $scope.id_seleccionado_productos = arreglo_atributos[0];
            $scope.estatus_seleccionado_productos = arreglo_atributos[1];
            $scope.id_seleccionado___productos = arreglo_atributos[2];
            $scope.id_seleccionado_tipo_productos = arreglo_atributos[3];
            //console.log($scope.estatus_seleccionado_productos);
            $("#cabecera_mensaje").text("Información:");
            if ($scope.estatus_seleccionado_productos==2){
                mensaje = "Desea eliminar este registro? ";
            }
            $scope.modificar_estatus("eliminar",mensaje)
        }
        //----------------------------------------------------------------

        setTimeout(function(){
                        $scope.iniciar_datatable();
                },500);

        $scope.consultar_detalles();
    })
    .controller("DetalleTserviciosController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,ordenProductosFactory,idiomaFactory,tproductosFactory,buscarserviciosFactory,buscartserviciosFactory,detalleserviciosFactory ){
        //Cuerpo declaraciones
        $(".li-menu").removeClass("active");
        $("#li_servicio1").addClass("active");    
        $(".a-menu").removeClass("active");
        $("#detalle_t_servicio").addClass("active");    
        $scope.titulo_pagina = "Detalles de servicio";
        $scope.subtitulo_pagina  = "Registrar Detalles de servicio";
        $scope.activo_img = "inactivo";
        $scope.servicios = {
                                'id':'',
                                'id_idioma':'',
                                'titulo':'',
                                'descripcion':'',
                                'estatus':'',
                                'id_servicios':'',
                                'id_tipo_servicio':''
        }
        $scope.searchProductos = []
        $scope.borrar_imagen = []
        $scope.currentTab = 'datos_basicos'
        $scope.titulo_registrar = "Registrar";
        $scope.base_url = $("#base_url").val();
        $scope.titulo_text = "Pulse aquí para ingresar el contenido del producto"


        //--------------------------------------------------------
        //Cuerpo de metodos
        $scope.consultar_idioma = function(){
            idiomaFactory.asignar_valores("",$scope.base_url)
            idiomaFactory.cargar_idioma(function(data){
                $scope.idioma=data;
                $.each( $scope.idioma, function( indice, elemento ){
                      agregarOptions("#idioma", elemento.id, elemento.descripcion)
                });
                $('#idioma > option[value="0"]').prop('selected', true);

                //console.log($scope.idioma);
            });
        }
        $scope.buscarProducto = function(){
            //console.log($scope.servicios.id_idioma);            

            if(!$scope.servicios.id_idioma){
                $("#serviciosTodos option").remove();
                agregarOptions("#serviciosTodos", "", "--Seleccione un servicio--")
                $("#tipos_servicios option").remove();
                agregarOptions("#tipos_servicios", "", "--Seleccione tipo de servicio--")

            }
            buscarserviciosFactory.asignar_valores($scope.servicios.id_idioma.id,$scope.base_url)
            buscarserviciosFactory.cargar_servicio(function(data){
                //console.log(data);            
                $scope.mensajes = data;
                if($scope.mensajes.mensaje == "error"){
                    $("#serviciosTodos option").remove();
                    agregarOptions("#serviciosTodos", "", "--Seleccione un servicio--")
                    $("#tipos_servicios option").remove();
                    agregarOptions("#tipos_servicios", "", "--Seleccione tipo de servicio--")
                }else{
                    if($scope.condicional=$scope.servicios.id_idioma){}
                    $("#serviciosTodos option").remove();
                    $("#tipos_servicios option").remove();
                    $scope.serviciosTodos=data;
                    agregarOptions("#serviciosTodos", "", "--Seleccione un servicio--")
                    agregarOptions("#tipos_servicios", "", "--Seleccione tipo de servicio--")
                    $.each( $scope.serviciosTodos, function( indice, elemento ){
                        agregarOptions("#serviciosTodos", elemento.id, elemento.titulo)
                    });
                $scope.condicional = $scope.servicios.id_idioma;
            }});
            
        }
        $scope.buscarTservicios = function(){
            
            if(!$scope.servicios.id_servicios){
                $("#tipos_servicios option").remove();
                agregarOptions("#tipos_servicios", "", "--Seleccione tipo de servicio--")            
            }
            buscartserviciosFactory.asignar_valores($scope.servicios.id_servicios.id,$scope.base_url)
            buscartserviciosFactory.cargar_servicios(function(data){
                $scope.mensajes = data;
                if($scope.mensajes.mensaje == "error"){
                    $("#tipos_servicios option").remove();
                    agregarOptions("#tipos_servicios", "", "--Seleccione tipo de servicio--")
                }else{
                    $("#tipos_servicios option").remove();
                    agregarOptions("#tipos_servicios", "", "--Seleccione tipo de servicio--")
                    $scope.tipos_servicios=data;

                $.each( $scope.tipos_servicios, function( indice, elemento ){
                    agregarOptions("#tipos_servicios", elemento.id, elemento.titulo)
                });
                $('#tipos_servicios > option[value="0"]').prop('selected', true);
            }});
            
        }
        $scope.agregarWisi = function(){
            //$('#text_editor').data("wysihtml5").editor.clear();
            $scope.servicios.descripcion = $(".textarea_editor").val()
            $("#div_descripcion").html($scope.servicios.descripcion)
            $("#cerrarModal").click();
        }
        $scope.wisi_modal = function(){
            $("#wisiModal").modal("show")
            $(".textarea_editor").data("wysihtml5").editor.setValue($scope.servicios.descripcion)
        }
        //--
        $scope.consultar_galeria_img = function(){
            galeriaFactory.asignar_valores('22','',$scope.base_url);//ya que es galeria de imagenes
            galeriaFactory.cargar_galeria_fa(function(data){
                $scope.galery=data;
                //console.log($scope.galery);
            });
        }

        $scope.seleccione_img_principal = function(){
            $("#modal_img1").modal("show");
        }

        $scope.seleccionar_imagen = function(event){
            var imagen = event.target.id;//Para capturar id
            var vec = $("#"+imagen).attr("data");
            var vector_data = vec.split("|")
            var id_imagen = vector_data[0];
            var ruta = vector_data[1];
            //console.log(vector_data[1]);

            $scope.borrar_imagen.push(id_imagen);
            $scope.activo_img = "activo"
            $scope.servicios.id_imagen = id_imagen
            $scope.servicios.imagen = ruta
            //--
            $("#modal_img1").modal("hide");
            //--
        }

        $scope.registrarServicios = function(){
            
            uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
            $scope.servicios.orden = $("#orden").val();

            if($scope.validar_form()==true){
                //Para guardar
                if(($scope.servicios.id!="")&&($scope.servicios.id!=undefined)){
                    $scope.modificar_productos();
                }else{
                    $scope.insertar_productos();
                }

            }

            //desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

        }

        $scope.insertar_productos = function(){

            $http.post($scope.base_url+"/DetalleTservicios/registrarServicios",
            {
                'id'         : $scope.servicios.id,
                'titulo'     : $scope.servicios.titulo,
                'descripcion': $scope.servicios.descripcion,
                'id_idioma'  : $scope.servicios.id_idioma.id,
                'id_servicios'  : $scope.servicios.id_servicios.id,
                'id_tipo_servicio'  : $scope.servicios.id_tipo_servicio.id,
                'orden'  : $scope.servicios.orden,

            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "registro_procesado"){
                    mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
                    $scope.limpiar_cajas_servicios();
                }else if($scope.mensajes.mensaje == "existe"){
                    mostrar_notificacion("Mensaje","Ya existe un detalle con ese titulo","warning");
                }
                else{
                    mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
                }
            }).error(function(data,estatus){
                console.log(data);
            });
        }

        $scope.modificar_productos = function(){
            //console.log($scope.servicios);

            $http.post($scope.base_url+"/DetalleTservicios/modificarServicios",
            {
                'id'         : $scope.servicios.id,
                'titulo'     : $scope.servicios.titulo,
                'descripcion': $scope.servicios.descripcion,
                'id_idioma'  : $scope.servicios.id_idioma,
                'id_servicios'  : $scope.servicios.id_servicios,
                'id_tipo_servicio'  : $scope.servicios.id_tipo_servicio,
                'orden'  : $scope.servicios.orden,
                'inicial'  : $scope.servicios.inicial,
            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "registro_procesado"){
                    mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
                    $scope.servicios.inicial = $scope.servicios.orden
                }else if($scope.mensajes.mensaje == "existe"){
                    mostrar_notificacion("Mensaje","Ya existe una servicio con ese titulo","warning");
                }
                else{
                    mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
                }
            }).error(function(data,estatus){
                console.log(data);
            });
        }

        $scope.validar_form = function(){
            //console.log($scope.servicios)
            if(($scope.servicios.id_idioma=="NULL")||($scope.servicios.id_idioma=="")||(!$scope.servicios.id_idioma)){
                mostrar_notificacion("Campos no validos","Debe seleccionar el idioma","warning");
                return false;
            }else if(($scope.servicios.id_servicios=="NULL")||($scope.servicios.id_servicios=="")||(!$scope.servicios.id_servicios)){
                mostrar_notificacion("Campos no validos","Debe seleccionar un servicio","warning");
                return false;
            }else if(($scope.servicios.id_tipo_servicio=="NULL")||($scope.servicios.id_tipo_servicio=="")||(!$scope.servicios.id_tipo_servicio)){
                mostrar_notificacion("Campos no validos","Debe seleccionar tipo de servicio","warning");
                return false;
            }else if($scope.servicios.titulo==""){
                mostrar_notificacion("Campos no validos","Debe ingresar el titulo","warning");
                return false;
            }else if(($scope.servicios.orden=="NULL")||($scope.servicios.orden=="")){
                mostrar_notificacion("Campos no validos","Debe seleccionar el orden del servicio","warning");
                return false;
            }else if($scope.servicios.descripcion==""){
                mostrar_notificacion("Campos no validos","Debe ingresar la descripción","warning");
                return false;
            }
            else{
                return true;
            }
        }

        $scope.limpiar_cajas_servicios = function(){
            $scope.servicios = {
                'id':'',
                'id_idioma':'',
                'titulo':'',
                'descripcion':'',
                'estatus':'',
                'id_producto':'',
                'id_tipo_servicio':''
            }
            $scope.activo_img = "inactivo";

            $('#text_editor').data("wysihtml5").editor.clear();
            $("#div_descripcion").html($scope.titulo_text);
            $('#orden > option[value=""]').prop('selected', true);
            eliminarOptions("serviciosTodos")
            $('#serviciosTodos > option[value=""]').prop('selected', true);
            eliminarOptions("tipos_servicios")
            $('#tipos_servicios > option[value=""]').prop('selected', true);
        }
        //--
        $scope.consultarProductoIndividual = function(){

            uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

            detalleserviciosFactory.asignar_valores("",$scope.id_detalle_servicios,$scope.base_url)
            detalleserviciosFactory.cargar_servicios(function(data){
                $scope.servicios=data[0];
                //console.log(data[0]);
                $("#div_descripcion").html($scope.servicios.descripcion)
                $scope.borrar_imagen.push($scope.servicios.id_imagen);
                $scope.activo_img = "activo"
                //-Cambio 10022020
                buscarserviciosFactory.asignar_valores($scope.servicios.id_idioma,$scope.base_url)
                buscarserviciosFactory.cargar_servicio(function(data){
                    $scope.serviciosTodos=data;
                    $.when(
                        $.each($scope.serviciosTodos, function( indice, elemento ){
                            agregarOptions("#serviciosTodos", elemento.id, elemento.titulo)
                        })
                    ).then(function(){
                        $('#serviciosTodos > option[value="'+$scope.servicios.id_servicios+'"]').prop('selected', true);
                    })
                    
                    //$('#serviciosTodos > option[value="0"]').prop('selected', true);
                });
                buscartserviciosFactory.asignar_valores($scope.servicios.id_servicios,$scope.base_url)
                buscartserviciosFactory.cargar_servicios(function(data){
                    $scope.tipos_servicios=data;
                    $.when(
                        $.each($scope.tipos_servicios, function( indice, elemento ){
                            agregarOptions("#tipos_servicios", elemento.id, elemento.titulo)
                        })
                    ).then(function(){
                        $('#tipos_servicios > option[value="'+$scope.servicios.id_tservicio+'"]').prop('selected', true);
                    })
                    //$('#tipos_servicios > option[value="0"]').prop('selected', true);
                    //console.log($scope.tipos_servicios);
                });
                ordenProductosFactory.asignar_valores($scope.servicios.id_idioma,$scope.base_url,"2",$scope.servicios.id_servicios,$scope.servicios.id_tipo_servicio)
                ordenProductosFactory.cargar_orden_detalle_servicios(function(data){
                    $scope.ordenes=data;
                    //console.log($scope.ordenes)
                    //-Elimino el select
                    eliminarOptions("orden")
                    $.when(
                        $.each( $scope.ordenes, function( indice, elemento ){
                              agregarOptions("#orden", elemento.orden, elemento.orden)
                        })
                    ).then(function(){
                        $('#orden > option[value="'+$scope.servicios.orden+'"]').prop('selected', true)
                    })
                    
                });
                $scope.servicios.imagen = $scope.servicios.ruta
                $scope.titulo_registrar = "Modificar";
                $scope.subtitulo_pagina  = "Modificar detalle de tipo de servicio";
                setTimeout(function(){
                    $('#idioma > option[value="'+$scope.servicios.id_idioma+'"]').prop('selected', true);
                    //$('#orden > option[value="'+$scope.servicios.orden+'"]').prop('selected', true);
                    $scope.servicios.inicial = $scope.servicios.orden;
                },300);
                $("#idioma").prop('disabled', true);
                /*
                setTimeout(function(){
                    $('#serviciosTodos > option[value="'+$scope.servicios.id_servicios+'"]').prop('selected', true);
                },300);
                */
                $("#serviciosTodos").prop('disabled', true);
                /*
                setTimeout(function(){
                    $('#tipos_servicios > option[value="'+$scope.servicios.id_tservicio+'"]').prop('selected', true);
                },300);
                */
                $scope.servicios.id_tservicio.id = $scope.servicios.id_tservicio;
                $("#tipos_servicios").prop('disabled', true);
                //console.log($scope.tipos_servicios);

            });
            
            desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

        }
        //---
        $scope.cargarOrden = function(){
            //console.log($scope.servicios);

            if($scope.servicios.id_servicios.id){
                $scope.id_servicios=$scope.servicios.id_servicios.id
                $scope.id_tipo_servicio=$scope.servicios.id_tipo_servicio.id
            }else{
                $scope.id_tipo_servicio=$scope.servicios.id_tipo_servicio
                $scope.id_servicios=$scope.servicios.id_servicios

            }
            ordenProductosFactory.asignar_valores($scope.servicios.id_idioma.id,$scope.base_url,"1",$scope.id_servicios,$scope.id_tipo_servicio)
            ordenProductosFactory.cargar_orden_detalle_servicios(function(data){
                $scope.ordenes=data;
                //-Elimino el select
                eliminarOptions("orden")
                $.each( $scope.ordenes, function( indice, elemento ){
                      agregarOptions("#orden", elemento.orden, elemento.orden)
                });
                $('#orden > option[value=""]').prop('selected', true);

            });
        }
        //--------------------------------------------------------
        //Cuerpo de llamados
        $scope.consultar_idioma();
        $scope.consultar_galeria_img();
        //--Si tiene id categoria .... consulto los datos relacionados a ese id, esto es cuando proviene de una consulta
        $scope.id_detalle_servicios  = $("#id_detalle_servicios").val();
        //.id_detalle_servicios);

        if($scope.id_detalle_servicios){
            $scope.consultarProductoIndividual();
        }
        //--------------------------------------------------------

    })
    .controller("DetalleTtproductosController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,ordenProductosFactory,idiomaFactory,tproductosFactory,buscarproductosFactory,buscartproductosFactory,detalleproductosFactory){
        //Cuerpo declaraciones
        $(".li-menu").removeClass("active");
        $("#li_productos1").addClass("active");    
        $(".a-menu").removeClass("active");
        $("#detalle_t_producto").addClass("active");    
        $scope.titulo_pagina = "Detalles de productos";
        $scope.subtitulo_pagina  = "Registrar Detalles de productos";
        $scope.activo_img = "inactivo";
        $scope.productos = {
                                'id':'',
                                'id_idioma':'',
                                'titulo':'',
                                'descripcion':'',
                                'estatus':'',
                                'id_productos':'',
                                'id_tipo_producto':''
        }
        $scope.searchProductos = []
        $scope.borrar_imagen = []
        $scope.currentTab = 'datos_basicos'
        $scope.titulo_registrar = "Registrar";
        $scope.base_url = $("#base_url").val();
        $scope.titulo_text = "Pulse aquí para ingresar el contenido del producto"


        //--------------------------------------------------------
        //Cuerpo de metodos
        $scope.consultar_idioma = function(){
            idiomaFactory.asignar_valores("",$scope.base_url)
            idiomaFactory.cargar_idioma(function(data){
                $scope.idioma=data;
                $.each( $scope.idioma, function( indice, elemento ){
                      agregarOptions("#idioma", elemento.id, elemento.descripcion)
                });
                $('#idioma > option[value="0"]').prop('selected', true);

                //console.log($scope.idioma);
            });
        }
        $scope.buscarProducto = function(){
            //console.log($scope.productos.id_idioma);            

            if(!$scope.productos.id_idioma){
                $("#productosTodos option").remove();
                agregarOptions("#productosTodos", "", "--Seleccione un producto--")
                $("#tipos_producto option").remove();
                agregarOptions("#tipos_producto", "", "--Seleccione tipo de producto--")

            }
            buscarproductosFactory.asignar_valores($scope.productos.id_idioma.id,$scope.base_url)
            buscarproductosFactory.cargar_productos(function(data){
                //console.log(data);            
                $scope.mensajes = data;
                if($scope.mensajes.mensaje == "error"){
                    $("#productosTodos option").remove();
                    agregarOptions("#productosTodos", "", "--Seleccione un producto--")
                    $("#tipos_producto option").remove();
                    agregarOptions("#tipos_producto", "", "--Seleccione tipo de producto--")
                }else{
                    if($scope.condicional=$scope.productos.id_idioma){}
                    $("#productosTodos option").remove();
                    $("#tipos_producto option").remove();
                    $scope.productosTodos=data;
                    agregarOptions("#productosTodos", "", "--Seleccione un producto--")
                    agregarOptions("#tipos_producto", "", "--Seleccione tipo de producto--")
                    $.each( $scope.productosTodos, function( indice, elemento ){
                        agregarOptions("#productosTodos", elemento.id, elemento.titulo)
                    });
                $scope.condicional = $scope.productos.id_idioma;
            }});
            
        }
        $scope.buscarTproducto = function(){
            
            if(!$scope.productos.id_productos){
                $("#tipos_producto option").remove();
                agregarOptions("#tipos_producto", "", "--Seleccione tipo de producto--")            
            }
            buscartproductosFactory.asignar_valores($scope.productos.id_productos.id,$scope.base_url)
            buscartproductosFactory.cargar_productos(function(data){
                $scope.mensajes = data;
                if($scope.mensajes.mensaje == "error"){
                    $("#tipos_producto option").remove();
                    agregarOptions("#tipos_producto", "", "--Seleccione tipo de producto--")
                }else{
                    $("#tipos_producto option").remove();
                    agregarOptions("#tipos_producto", "", "--Seleccione tipo de producto--")
                    $scope.tipos_producto=data;

                $.each( $scope.tipos_producto, function( indice, elemento ){
                    agregarOptions("#tipos_producto", elemento.id, elemento.titulo)
                });
                $('#tipos_producto > option[value="0"]').prop('selected', true);
            }});
            
        }
        $scope.agregarWisi = function(){
            //$('#text_editor').data("wysihtml5").editor.clear();
            $scope.productos.descripcion = $(".textarea_editor").val()
            $("#div_descripcion").html($scope.productos.descripcion)
            $("#cerrarModal").click();
        }
        $scope.wisi_modal = function(){
            $("#wisiModal").modal("show")
            $(".textarea_editor").data("wysihtml5").editor.setValue($scope.productos.descripcion)
        }
        //--
        $scope.consultar_galeria_img = function(){
            galeriaFactory.asignar_valores('22','',$scope.base_url);//ya que es galeria de imagenes
            galeriaFactory.cargar_galeria_fa(function(data){
                $scope.galery=data;
                //console.log($scope.galery);
            });
        }

        $scope.seleccione_img_principal = function(){
            $("#modal_img1").modal("show");
        }

        $scope.seleccionar_imagen = function(event){
            var imagen = event.target.id;//Para capturar id
            var vec = $("#"+imagen).attr("data");
            var vector_data = vec.split("|")
            var id_imagen = vector_data[0];
            var ruta = vector_data[1];
            //console.log(vector_data[1]);

            $scope.borrar_imagen.push(id_imagen);
            $scope.activo_img = "activo"
            $scope.productos.id_imagen = id_imagen
            $scope.productos.imagen = ruta
            //--
            $("#modal_img1").modal("hide");
            //--
        }

        $scope.registrarproductos = function(){
            
            uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
            $scope.productos.orden = $("#orden").val();

            if($scope.validar_form()==true){
                //Para guardar
                if(($scope.productos.id!="")&&($scope.productos.id!=undefined)){
                    $scope.modificar_productos();
                }else{
                    $scope.insertar_productos();
                }

            }

            //desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

        }

        $scope.insertar_productos = function(){

            $http.post($scope.base_url+"/DetalleTproductos/registrarProductos",
            {
                'id'         : $scope.productos.id,
                'titulo'     : $scope.productos.titulo,
                'descripcion': $scope.productos.descripcion,
                'id_idioma'  : $scope.productos.id_idioma.id,
                'id_productos'  : $scope.productos.id_productos.id,
                'id_tipo_producto'  : $scope.productos.id_tipo_producto.id,
                'orden'  : $scope.productos.orden,

            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "registro_procesado"){
                    mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
                    $scope.limpiar_cajas_productos();
                }else if($scope.mensajes.mensaje == "existe"){
                    mostrar_notificacion("Mensaje","Ya existe un detalle con ese titulo","warning");
                }
                else{
                    mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
                }
            }).error(function(data,estatus){
                console.log(data);
            });
        }

        $scope.modificar_productos = function(){
            //console.log($scope.productos);

            $http.post($scope.base_url+"/DetalleTproductos/modificarProductos",
            {
                'id'         : $scope.productos.id,
                'titulo'     : $scope.productos.titulo,
                'descripcion': $scope.productos.descripcion,
                'id_idioma'  : $scope.productos.id_idioma,
                'id_productos'  : $scope.productos.id_productos,
                'id_tipo_producto'  : $scope.productos.id_tipo_producto,
                'orden':$scope.productos.orden,
                'inicial':$scope.productos.inicial,
            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "registro_procesado"){
                    mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
                    $scope.productos.inicial = $scope.productos.orden

                }else if($scope.mensajes.mensaje == "existe"){
                    mostrar_notificacion("Mensaje","Ya existe una producto con ese titulo","warning");
                }
                else{
                    mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
                }
            }).error(function(data,estatus){
                console.log(data);
            });
        }

        $scope.validar_form = function(){
            //console.log($scope.productos)
            if(($scope.productos.id_idioma=="NULL")||($scope.productos.id_idioma=="")||(!$scope.productos.id_idioma)){
                mostrar_notificacion("Campos no validos","Debe seleccionar el idioma","warning");
                return false;
            }else if(($scope.productos.id_productos=="NULL")||($scope.productos.id_productos=="")||(!$scope.productos.id_productos)){
                mostrar_notificacion("Campos no validos","Debe seleccionar un producto","warning");
                return false;
            }else if(($scope.productos.id_tipo_producto=="NULL")||($scope.productos.id_tipo_producto=="")||(!$scope.productos.id_tipo_producto)){
                mostrar_notificacion("Campos no validos","Debe seleccionar tipo de producto","warning");
                return false;
            }else if($scope.productos.titulo==""){
                mostrar_notificacion("Campos no validos","Debe ingresar el titulo","warning");
                return false;
            }else if(($scope.productos.orden=="NULL")||($scope.productos.orden=="")){
                mostrar_notificacion("Campos no validos","Debe seleccionar el orden del servicio","warning");
                return false;
            }else if($scope.productos.descripcion==""){
                mostrar_notificacion("Campos no validos","Debe ingresar la descripción","warning");
                return false;
            }
            else{
                return true;
            }
        }

        $scope.limpiar_cajas_productos = function(){
            $scope.productos = {
                'id':'',
                'id_idioma':'',
                'titulo':'',
                'descripcion':'',
                'estatus':'',
                'id_producto':'',
                'id_tipo_producto':''
            }
            $scope.activo_img = "inactivo";

            $('#text_editor').data("wysihtml5").editor.clear();
            $("#div_descripcion").html($scope.titulo_text);
            eliminarOptions("orden")
            $('#orden > option[value=""]').prop('selected', true);
            eliminarOptions("productosTodos")
            $('#productosTodos > option[value=""]').prop('selected', true);
            eliminarOptions("tipos_producto")
            $('#tipos_producto > option[value=""]').prop('selected', true);
            
        }
        //--
        $scope.consultarProductoIndividual = function(){

            uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

            detalleproductosFactory.asignar_valores("",$scope.id_detalle_productos,$scope.base_url)
            detalleproductosFactory.cargar_productos(function(data){
                $scope.productos=data[0];
                //console.log(data[0]);
                $("#div_descripcion").html($scope.productos.descripcion)
                $scope.borrar_imagen.push($scope.productos.id_imagen);
                $scope.activo_img = "activo"
                ordenProductosFactory.asignar_valores($scope.productos.id_idioma,$scope.base_url,"2",$scope.productos.id_productos,$scope.productos.id_tipo_producto)
                ordenProductosFactory.cargar_orden_detalle_productos(function(data){
                    $scope.ordenes=data;
                    //console.log($scope.ordenes)
                    //-Elimino el select
                    eliminarOptions("orden")
                    $.when(
                        //---
                        $.each( $scope.ordenes, function( indice, elemento ){
                              agregarOptions("#orden", elemento.orden, elemento.orden)
                        })
                        //---
                    ).then(function(){
                        $('#orden > option[value="'+$scope.productos.orden+'"]').prop('selected', true)
                    });
                    
                });
                //-Cambio 10022020
                buscarproductosFactory.asignar_valores($scope.productos.id_idioma,$scope.base_url)
                buscarproductosFactory.cargar_productos(function(data){
                    $scope.productosTodos=data;
                    $.when(
                        $.each($scope.productosTodos, function( indice, elemento ){
                            agregarOptions("#productosTodos", elemento.id, elemento.titulo)
                        })
                    ).then(function(){
                        $('#productosTodos > option[value="'+$scope.productos.id_productos+'"]').prop('selected', true);
                    })
                    
                });
                buscartproductosFactory.asignar_valores($scope.productos.id_productos,$scope.base_url)
                buscartproductosFactory.cargar_productos(function(data){
                    $scope.tipos_producto=data;
                    $.when(
                        $.each($scope.tipos_producto, function( indice, elemento ){
                            agregarOptions("#tipos_producto", elemento.id, elemento.titulo)
                        })
                    ).then(function(){
                        $('#tipos_producto > option[value="'+$scope.productos.id_tproducto+'"]').prop('selected', true)
                    })
                    
                    $('#tipos_producto > option[value="0"]').prop('selected', true);
                    //console.log($scope.tipos_producto);
                });

                $scope.productos.imagen = $scope.productos.ruta
                $scope.titulo_registrar = "Modificar";
                $scope.subtitulo_pagina  = "Modificar detalle de tipo de producto";
                setTimeout(function(){
                    $('#idioma > option[value="'+$scope.productos.id_idioma+'"]').prop('selected', true);
                    //$('#orden > option[value="'+$scope.productos.orden+'"]').prop('selected', true);
                    //$('#productosTodos > option[value="'+$scope.productos.id_productos+'"]').prop('selected', true);
                    //$('#tipos_producto > option[value="'+$scope.productos.id_tproducto+'"]').prop('selected', true);
                    $scope.productos.inicial = $scope.productos.orden;
                },300);
                
                $scope.productos.id_tproducto.id = $scope.productos.id_tproducto;
                $("#tipos_producto").prop('disabled', true);
                //console.log($scope.tipos_producto);

            });
            
            desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

        }
        //---
        $scope.cargarOrden = function(){
            //console.log($scope.productos);

            if($scope.productos.id_productos.id){
                $scope.id_productos=$scope.productos.id_productos.id
                $scope.id_tipo_producto=$scope.productos.id_tipo_producto.id
            }else{
                $scope.id_tipo_producto=$scope.productos.id_tipo_producto
                $scope.id_productos=$scope.productos.id_productos

            }
            ordenProductosFactory.asignar_valores($scope.productos.id_idioma.id,$scope.base_url,"1",$scope.id_productos,$scope.id_tipo_producto)
            ordenProductosFactory.cargar_orden_detalle_productos(function(data){
                $scope.ordenes=data;
                //-Elimino el select
                eliminarOptions("orden")
                $.each( $scope.ordenes, function( indice, elemento ){
                      agregarOptions("#orden", elemento.orden, elemento.orden)
                });
                $('#orden > option[value=""]').prop('selected', true);

            });
        }
        //--------------------------------------------------------
        //Cuerpo de llamados
        $scope.consultar_idioma();
        $scope.consultar_galeria_img();
        //--Si tiene id categoria .... consulto los datos relacionados a ese id, esto es cuando proviene de una consulta
        $scope.id_detalle_productos  = $("#id_detalle_productos").val();
        //.id_detalle_productos);

        if($scope.id_detalle_productos){
            $scope.consultarProductoIndividual();
        }
        //--------------------------------------------------------

    })
    .controller("DireccionConsultasController", function($scope,$http,$location,serverDataMensajes,sesionFactory,idiomaFactory,direccionFactory){
        $(".li-menu").removeClass("active");
        $("#li_contactos").addClass("active");
        $(".a-menu").removeClass("active");
        $("#direccion").addClass("active");

        $scope.titulo_pagina = "Consulta de Dirección";
        $scope.activo_img = "inactivo";

        $scope.direccion = {
                        'id': '',
                        'idioma': '',
                        'id_idioma' : '',
                        'titulo' : '',
                        'descripcion' : '',
                        'telefono' : [],
                        'latitud':'',
                        'longitud':'',
                        'estatus' : '',
                        'orden':'',
        }

        $scope.categorias_menu = "2";
        $scope.id_marca = "";
        $scope.base_url = $("#base_url").val();

        $scope.iniciar_datatable = function(){
            //--Iniciar DataTable
            $('#myTable').DataTable({
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                    "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
                "order": [[ 2, "asc" ]]

            });
            //console.log($scope.direccion);
        }
        //////////////////////////////////////////////////////////////////////////////////////
        $scope.consultar_direccion = function(){
            direccionFactory.asignar_valores("","",$scope.base_url)
            direccionFactory.cargar_direccion(function(data){
                $scope.direccion=data;
                //console.log($scope.direccion);
            });
        }
        //////////////////////////////////////////////////////////////////////////////////////
        $scope.ver_direccion = function(valor){
            var id = $("#ver"+valor).attr("data");
            $scope.id_marca = id;
            //console.log($scope.id_marca);
            $("#id_marca").val($scope.id_marca)
            let form = document.getElementById('formConsultaDireccion');
            form.action = "./direccionVer";
            form.submit();
        }
        //////////////////////////////////////////////////////////////////////////////////////
        $scope.activar_registro = function(event){
            $scope.id_seleccionado_direccion = []
            $scope.estatus_seleccionado_direccion = []
            var caja = event.currentTarget.id;
            var atributos = $("#"+caja).attr("data");
            var arreglo_atributos = atributos.split("|");
            $scope.id_seleccionado_direccion = arreglo_atributos[0];
            $scope.estatus_seleccionado_direccion = arreglo_atributos[1];
            $("#cabecera_mensaje").text("Información:");
            //--
            if ($scope.estatus_seleccionado_direccion==0){
                mensaje = "Desea modificar el estatus de este registro a publicado? ";
                $scope.estatus_seleccionado_direccion=1;
            }else{
                mensaje = "Desea modificar el estatus de este registro a inactivo? ";
                $scope.estatus_seleccionado_direccion=0
            }
            //($scope.estatus_seleccionado_categorias==1) ? $scope.estatus_seleccionado_categorias=0:$scope.estatus_seleccionado_categorias=1;

            //--
            $scope.modificar_estatus("activar_inactivar",mensaje)
        }
        //----------------------------------------------------------------
        $scope.modificar_estatus = function(opcion,mensaje){
             swal({
                  title: 'Esta seguro?',
                  text: mensaje,
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Si!',
                  cancelButtonText: 'No',
                }).then((result) => {
                      if (result.value) {
                        $scope.accion_estatus()
                      }
                })
        }
        //----------------------------------------------------------------
        $scope.accion_estatus = function(){

            $http.post($scope.base_url+"/Direccion/modificarDireccionEstatus",
            {
                 'id':$scope.id_seleccionado_direccion,
                 'estatus':$scope.estatus_seleccionado_direccion,

            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "modificacion_procesada"){
                    Swal(
                          'Realizado!',
                          'El proceso fue ejecutado.',
                          'success'
                    ).then((result) => {
                          if (result.value) {
                                let form = document.getElementById('formConsultaDireccion');
                                form.action = $scope.base_url+"cms/direccion/consultarDireccion";
                                form.submit();
                          }

                    });
                }else{
                    Swal(
                          'No realizado!',
                          'El proceso no pudo ser ejecutado.',
                          'warning'
                    )
                }
            }).error(function(data,estatus){
                console.log(data);
            });
        }
        //////////////////////////////////////////////////////////////////////////////////////
        $scope.eliminar_registro = function(event){
            $scope.id_seleccionado_direccion = []
            $scope.estatus_seleccionado_direccion = []
            var caja = event.currentTarget.id;
            //alert(caja);
            var atributos = $("#"+caja).attr("data");
            var arreglo_atributos = atributos.split("|");
            $scope.id_seleccionado_direccion = arreglo_atributos[0];
            $scope.estatus_seleccionado_direccion = arreglo_atributos[1];
            //console.log($scope.estatus_seleccionado_direccion);
            $("#cabecera_mensaje").text("Información:");
            if ($scope.estatus_seleccionado_direccion==2){
                mensaje = "Desea eliminar este registro? ";
            }
            $scope.modificar_estatus("eliminar",mensaje)
        }
        //////////////////////////////////////////////////////////////////////////////////////
        setTimeout(function(){
                $scope.iniciar_datatable();
        },500);
        $scope.consultar_direccion();
    })
    .controller("DireccionController", function($scope,$compile,$http,$location,serverDataMensajes,sesionFactory,direccionFactory,idiomaFactory,ordenFactory){

        $(".li-menu").removeClass("active");
        $("#li_contactos").addClass("active");
        $(".a-menu").removeClass("active");
        $("#direccion").addClass("active");

        $scope.titulo_pagina = "Dirección";
        $scope.subtitulo_pagina = "Registro";
        $scope.titulo_registrar = "Registrar";
        $scope.titulo_cons = "Consultar";
        $scope.id_direccion = "";
        $scope.base_url = $("#base_url").val();
        $scope.titulo_text = "Pulse aquí para ingresar el contenido"
        $scope.activo_img = "inactivo";
        $scope.searchMarcas = []
        $scope.borrar_imagen = []
        $scope.cuantos = 1;
        $scope.direccion = {
                        'id': '',
                        'idioma': '',
                        'id_idioma' : '',
                        'titulo' : '',
                        'descripcion' : '',
                        'telefono' : [],
                        'latitud':'',
                        'longitud':'',
                        'estatus' : '',
                        'orden':'',
        }

        $scope.consultar_idioma = function(){
            idiomaFactory.asignar_valores("",$scope.base_url)
            idiomaFactory.cargar_idioma(function(data){
                $scope.idioma=data;

                $.each( $scope.idioma, function( indice, elemento ){
                      agregarOptions("#idioma", elemento.id, elemento.descripcion)
                });
            });
        }
        $scope.agregar_telefono_valores = function(valor){
            $scope.cuantos = $scope.cuantos+1;

            if($scope.cuantos==1){
                $("#telefono_direccion_0").val(valor)
            }else{
                //---    
                number = $scope.cuantos
                number2 = number
                var bloque_telefono = '<div id="fila_'+number+'" class="col-lg-6 col-md-6 col-sm-12 col-xs-12 tlf_direccion_cuadro">\
                                        <div class="form-group">\
                                            <div class="asterisco_rojo"><i class="fa fa-asterisk" aria-hidden="true"></i></div>\
                                            <label>Teléfono #'+number2+'</label>\
                                            <input type="text" name="telefono_direccion" id="telefono_direccion" class="form-control  tlf_direccion" placeholder="Formato:+cod-xxx-xxx-xx-xx" onKeyPress="return valida(event,this,21,20)" onBlur="valida2(this,21,20);" maxlength="50" value="'+valor+'">\
                                        </div>\
                                    </div>'
                $("#super_contenedor").append($compile(bloque_telefono)($scope)).fadeIn("slow");      
                //---
            }
        }
        $scope.quitar_telefono = function(){
            if($scope.cuantos>1){
                var ene = $scope.cuantos
                $("#fila_"+ene).remove()
                $scope.cuantos = parseInt($scope.cuantos)-1;
            }else{
                mostrar_notificacion("Campos no validos","Debe agregar al menos 2 teléfonos","warning");
            }    
        }
        //-------------------------------------------------------------
        $scope.cargarOrden = function(){
            ordenFactory.asignar_valores($scope.direccion.id_idioma,$scope.base_url,"1")
            ordenFactory.cargar_orden_direccion(function(data){
                $scope.ordenes=data;
                //console.log($scope.ordenes);
                //-Elimino el select
                eliminarOptions("orden")
                $.each( $scope.ordenes, function( indice, elemento ){
                      agregarOptions("#orden", elemento.orden, elemento.orden)
                });
                $('#orden > option[value=""]').prop('selected', true);

            });
        }
        //-------------------------------------------------------------
        $scope.agregarWisi = function(){
            //$('#text_editor').data("wysihtml5").editor.clear();
            $scope.direccion.descripcion = $(".textarea_editor").val()
            $("#div_descripcion").html($scope.direccion.descripcion)
            $("#cerrarModal").click();
        }
        $scope.wisi_modal = function(){
            $("#wisiModal").modal("show")
            $(".textarea_editor").data("wysihtml5").editor.setValue($scope.direccion.descripcion)
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        $scope.limpiar_cajas_direcciones = function(){
            $scope.direccion = {
                                    'id': '',
                                    'idioma': '',
                                    'id_idioma' : '',
                                    'titulo' : '',
                                    'descripcion' : '',
                                    'telefono' : [],
                                    'latitud':'',
                                    'longitud':'',
                                    'estatus' : '',
                                    'orden':'',
            }
            $scope.cuantos = 1;
            $(".tlf_direccion").val("");
            $(".tlf_direccion_cuadro").remove();
            $("#div_descripcion").html($scope.titulo_text);
            $('#textarea_editor').data("wysihtml5").editor.clear();
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        $scope.registrarDirecciones = function(){
            uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
            $scope.direccion.orden = $("#orden").val();
            $scope.recorrer_form_telefono()

            if($scope.validar_form()==true){
                //Para guardar
                if(($scope.direccion.id!="")&&($scope.direccion.id!=undefined)){
                    $scope.modificar_direcciones();
                }else{
                    //console.log($scope.direccion)
                    $scope.insertar_direcciones();
                }

            }

            //desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        $scope.validar_form = function(){
            
            var validado_telefono = 0;

            var a = 0;

            $( ".tlf_direccion" ).each(function( index ) {
               if($( this ).val()==""){
                     a = index +1;
                     mostrar_notificacion("Campos no validos","Debe ingresar el valor del teléfonos #"+a,"warning");
                     return false;    
               }else{
                       validado_telefono = validado_telefono+1;    
               }
            });

            if(($scope.direccion.id_idioma=="")||($scope.direccion.id_idioma=="")){
                mostrar_notificacion("Campos no validos","Debe seleccionar el idioma","warning");
                return false;
            }else if($scope.direccion.titulo==""){
                mostrar_notificacion("Campos no validos","Debe ingresar el titulo","warning");
                return false;
            }else if($scope.direccion.descripcion==""){
                mostrar_notificacion("Campos no validos","Debe ingresar la descripción","warning");
                return false;
            }else if($scope.direccion.orden==""){
                mostrar_notificacion("Campos no validos","Debe seleccionar el orden","warning");
                return false;
            }else if(validado_telefono==$scope.cuantos){
                return true;
            }else{
                //alert(validado_telefono+"-"+$scope.cuantos)
                mostrar_notificacion("Campos no validos","Debe ingresar al menos un número de teléfono","warning");
                return false;
            }
            //else if(($scope.marcas.id_imagen=="NULL")||($scope.marcas.id_imagen=="")){
            //     mostrar_notificacion("Campos no validos","Debe seleccionar la imagen","warning");
            //     return false;
            // }
        }
        //--
        $scope.recorrer_form_telefono = function(){
            

            var data
        
            var telefono_vector = new Array()
            $scope.cuantos = 0
            //--Recorro telefono
            $(".tlf_direccion").each(function(index){
                telefono = $(this).val()
                telefono_vector.push(telefono)
                $scope.cuantos++
            });
            //--
            $scope.direccion.telefono = telefono_vector
            //console.log($scope.direccion)
        }
        //--
        $scope.insertar_direcciones = function(){
            $http.post($scope.base_url+"/Direccion/registrarDireccion",
            {
                'id': $scope.direccion.id,
                'titulo':$scope.direccion.titulo,
                'descripcion':$scope.direccion.descripcion,
                'telefono':$scope.direccion.telefono,
                'orden':$scope.direccion.orden,
                'id_idioma':$scope.direccion.id_idioma,
                'latitud':$scope.direccion.latitud,
                'longitud':$scope.direccion.longitud,
            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "registro_procesado"){
                    mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
                    $scope.limpiar_cajas_direcciones();
                    desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
                }else if($scope.mensajes.mensaje == "existe"){
                    mostrar_notificacion("Mensaje","Ya existe una noticia con ese titulo","warning");
                    desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

                }
                else{
                    mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
                    desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
                }
            }).error(function(data,estatus){
                console.log(data);
            });
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        $scope.consultarDireccionIndividual = function(){
            uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

            direccionFactory.asignar_valores("",$scope.id_direccion,$scope.base_url)
            direccionFactory.cargar_direccion(function(data){
                $scope.direccion=data[0];
                //console.log(data[0]);
                //--
                ordenFactory.asignar_valores($scope.direccion.id_idioma,$scope.base_url,"2")
                ordenFactory.cargar_orden(function(data){
                    $scope.ordenes=data;
                    //console.log($scope.ordenes)
                    //-Elimino el select
                    eliminarOptions("orden")
                    $.each( $scope.ordenes, function( indice, elemento ){
                          agregarOptions("#orden", elemento.orden, elemento.orden)
                    });
                });
                //--
                $scope.cuantos = 0;
                //console.log($scope.direccion.telefono)
                $.each($scope.direccion.telefono, function( index, value ) {
                      $scope.agregar_telefono_valores(value)
                });
                //--
                $("#div_descripcion").html($scope.direccion.descripcion)
                $scope.activo_img = "activo"
                $scope.titulo_registrar = "Modificar";
                $scope.subtitulo_pagina  = "Modificar dirección";
                setTimeout(function(){
                    $('#idioma > option[value="'+$scope.direccion.id_idioma+'"]').prop('selected', true);
                    $('#orden > option[value="'+$scope.direccion.orden+'"]').prop('selected', true);
                    $scope.direccion.inicial = $scope.direccion.orden;
                },300);
                $("#idioma").prop('disabled', true);
            });
            desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        $scope.modificar_direcciones = function(){
            $http.post($scope.base_url+"/Direccion/modificarDireccion",
            {
                'id': $scope.direccion.id,
                'titulo':$scope.direccion.titulo,
                'descripcion':$scope.direccion.descripcion,
                'telefono':$scope.direccion.telefono,
                'orden':$scope.direccion.orden,
                'id_idioma':$scope.direccion.id_idioma,
                'latitud':$scope.direccion.latitud,
                'longitud':$scope.direccion.longitud,
                'inicial'    : $scope.direccion.inicial,
            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "modificacion_procesada"){
                    mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
                    //$scope.limpiar_cajas_marcas();
                    $scope.direccion.inicial = $scope.direccion.orden
                    desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
                }else{
                    mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
                    desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
                }
            }).error(function(data,estatus){
                //console.log(data);
            });
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        $scope.consultar_idioma();

        $scope.id_direccion = $("#id_direccion").val();
            if($scope.id_direccion){
                $scope.consultarDireccionIndividual();
            }else{
                $("#idioma").removeAttr("disabled");
            }
    })
    .controller("DirectivaConsultasController", function($scope,$http,$location,serverDataMensajes,directivaFactory,sesionFactory,idiomaFactory){
        //Cuerpo declaraciones
        $(".li-menu").removeClass("active");
        $(".a-menu").removeClass("active");
        
        $("#li_empresa").addClass("active");    
        $("#directiva").addClass("active");    

        $scope.titulo_pagina = "Consulta de Directiva";
        $scope.activo_img = "inactivo";
        $scope.directiva = {
                        'id': '',
                        'idioma': '',
                        'id_idioma' : '',
                        'titulo' : '',
                        'descripcion' : '',
                        'id_imagen' : '',
                        'imagen' : 'assets/images/logo_peque.png',
                        'orden':''
        }
        $scope.categorias_menu = "1";
        $scope.id_directiva = "";
        $scope.base_url = $("#base_url").val();
        //-----------------------------------------------------
        //--Cuerpo de metodos  --/
        $scope.iniciar_datatable = function(){
            //--Iniciar DataTable
            $('#myTable').DataTable({
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                    "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
            });
            //console.log($scope.categorias);
        }
        $scope.consultar_directiva = function(){
            directivaFactory.asignar_valores("","",$scope.base_url)
            directivaFactory.cargar_directiva(function(data){
                $scope.directiva=data;
                //console.log($scope.directiva);                
            });
        }
        //---------------------------------------------------------------
        $scope.ver_directiva = function(valor){
            var id = $("#ver"+valor).attr("data");
            $scope.id_directiva = id;    
            $("#id_directiva").val($scope.id_directiva)
            let form = document.getElementById('formConsultaDirectiva');
            form.action = "./directiva_ver";
            form.submit();
        }
        //----------------------------------------------------------------
        $scope.activar_registro = function(event){
            $scope.id_seleccionado_directiva = []
            $scope.estatus_seleccionado_directiva = []
            var caja = event.currentTarget.id;
            var atributos = $("#"+caja).attr("data");
            var arreglo_atributos = atributos.split("|");
            $scope.id_seleccionado_directiva = arreglo_atributos[0];
            $scope.estatus_seleccionado_directiva = arreglo_atributos[1];
            $("#cabecera_mensaje").text("Información:");            
            //--
            if ($scope.estatus_seleccionado_directiva==0){
                mensaje = "Desea modificar el estatus de este registro a publicado? ";
                $scope.estatus_seleccionado_directiva=1;
            }else{
                mensaje = "Desea modificar el estatus de este registro a inactivo? ";
                $scope.estatus_seleccionado_directiva=0
            }
            //($scope.estatus_seleccionado_categorias==1) ? $scope.estatus_seleccionado_categorias=0:$scope.estatus_seleccionado_categorias=1;

            //--
            $scope.modificar_estatus("activar_inactivar",mensaje)
        }
        //----------------------------------------------------------------
        $scope.modificar_estatus = function(opcion,mensaje){
             swal({
                  title: 'Esta seguro?',
                  text: mensaje,
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Si!',
                  cancelButtonText: 'No',
                }).then((result) => {
                      if (result.value) {
                          $scope.accion_estatus()
                      }
                })
        }
        //----------------------------------------------------------------
        $scope.accion_estatus = function(){
            $http.post($scope.base_url+"/directiva/modificarDirectivaEstatus",
            {
                 'id':$scope.id_seleccionado_directiva,    
                 'estatus':$scope.estatus_seleccionado_directiva,     

            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                //console.log($scope.mensajes)
                if($scope.mensajes.mensaje == "modificacion_procesada"){
                    Swal(
                          'Realizado!',
                          'El proceso fue ejecutado.',
                          'success'
                    ).then((result) => {
                          if (result.value) {
                                let form = document.getElementById('formConsultaDirectiva');
                                form.action = "./consultar_directiva";
                                form.submit();
                          }
                    
                    });
                }else{
                    Swal(
                          'No realizado!',
                          'El proceso no pudo ser ejecutado.',
                          'warning'
                    )
                }
            }).error(function(data,estatus){
                console.log(data);
            });    
        }
        //----------------------------------------------------------------
        $scope.eliminar_registro = function(event){
            $scope.id_seleccionado_directiva = []
            $scope.estatus_seleccionado_directiva = []
            var caja = event.currentTarget.id;
            //alert(caja);
            var atributos = $("#"+caja).attr("data");
            var arreglo_atributos = atributos.split("|");
            $scope.id_seleccionado_directiva = arreglo_atributos[0];
            $scope.estatus_seleccionado_directiva = arreglo_atributos[1];
            //console.log($scope.estatus_seleccionado_directiva);
            $("#cabecera_mensaje").text("Información:");
            if ($scope.estatus_seleccionado_directiva==2){
                mensaje = "Desea eliminar este registro? ";
            }
            $scope.modificar_estatus("eliminar",mensaje)
        }
        //----------------------------------------------------------------
        //-- Cuerpo de funciones--/
        setTimeout(function(){
                $scope.iniciar_datatable();
        },500);
        $scope.consultar_directiva();
        
        //-----------------------------------------------------
    })
    .controller("DirectivaController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,sesionFactory,idiomaFactory,directivaFactory,ordenFactory){
        //---------------------------------------------------------------------
        //Cuerpo declaraciones
        $(".li-menu").removeClass("active");
        $(".a-menu").removeClass("active");
        $("#li_empresa").addClass("active");    
        $("#directiva").addClass("active");    

        $scope.titulo_pagina = "Directiva";
        $scope.subtitulo_pagina  = "Registrar Directiva";
        $scope.activo_img = "inactivo";

        $scope.directiva = {
                        'id': '',
                        'idioma': '',
                        'id_idioma' : '',
                        'titulo' : '',
                        'descripcion' : '',
                        'id_imagen' : '',
                        'imagen' : 'assets/images/logo_peque.png',
                        'orden':''
        }
        
        
        $scope.titulo_registrar = "Registrar";
        $scope.titulo_cons = "Consultar";
        $scope.borrar_imagen = []
        $scope.currentTab = 'datos_basicos'
        $scope.titulo_text = "Pulse aquí para ingresar la descripción de la directiva"

        $scope.opcion = ''
        $scope.seccion = ''
        $scope.base_url = $("#base_url").val();
        
        //alert($scope.base_url);
        //Cuerpo de metodos
        $scope.consultar_idioma = function(){
            idiomaFactory.asignar_valores("",$scope.base_url)
            idiomaFactory.cargar_idioma(function(data){
                $scope.idioma=data;

                $.each( $scope.idioma, function( indice, elemento ){
                      agregarOptions("#idioma", elemento.id, elemento.descripcion)
                });
                $('#idioma > option[value="'+$scope.directiva.id_idioma+'"]').prop('selected', true);
            });
        }
        //---------------------------------
        //
        $scope.cargarOrden = function(){
            ordenFactory.asignar_valores($scope.directiva.id_idioma,$scope.base_url,"1")
            ordenFactory.cargar_orden_directiva(function(data){
                $scope.ordenes=data;
                //console.log($scope.ordenes);
                //-Elimino el select
                eliminarOptions("orden")
                $.each( $scope.ordenes, function( indice, elemento ){
                      agregarOptions("#orden", elemento.orden, elemento.orden)
                });
                $('#orden > option[value=""]').prop('selected', true);

            });
        }
        //WISIMODAL
        $scope.agregarWisi = function(){
            //$('#text_editor').data("wysihtml5").editor.clear();
            $scope.directiva.descripcion = $(".textarea_editor").val()
            $("#div_descripcion").html($scope.directiva.descripcion)
            $("#cerrarModal").click();
        }
        //--
        
        $scope.wisi_modal = function(){
            $("#wisiModal").modal("show")
            $(".textarea_editor").data("wysihtml5").editor.setValue($scope.directiva.descripcion)
        }
        //--
        $scope.agregar_contenido = function(){
            if ($scope.directiva==undefined) {
                    $scope.directiva = {
                        'id': '',
                        'idioma': '',
                        'id_idioma' : '',
                        'titulo' : '',
                        'descripcion' : '',
                        'id_imagen' : '',
                        'imagen' : 'assets/images/logo_peque.png',
                        'orden':''
                    }
            }

        }
        //------------------------------------------CONSULTA DE IMAGEN SEGUN ID DE CATEGORIA
        $scope.consultar_galeria_img = function(){
            galeriaFactory.asignar_valores('23','',$scope.base_url);//ya que es galeria de imagenes
            galeriaFactory.cargar_galeria_fa(function(data){
                $scope.galery=data;
                //console.log($scope.galery);
            });
        }
        //MODAL DE IMG
        $scope.seleccione_img_principal = function(){
            $("#modal_img1").modal("show");
        }
        //PARA SELECCIONAR UNA IMAGEN Y MOSTRARLA EN EL CAMPO DE IMG
        $scope.seleccionar_imagen = function(event){
                var imagen = event.target.id;//Para capturar id
                //console.log(imagen);
                var vec = $("#"+imagen).attr("data");
                var vector_data = vec.split("|")
                var id_imagen = vector_data[0];
                var ruta = vector_data[1];

                $scope.borrar_imagen.push(id_imagen);
                $scope.activo_img = "activo"
                $scope.directiva.id_imagen = id_imagen
                $scope.directiva.imagen = ruta
                //alert($scope.directiva.id_imagen);
                //--
                $("#modal_img1").modal("hide");
                //--
        }
        /////////////////////////////////////////////////////////////////////////////////////
        $scope.registrarDirectiva = function(){
            $scope.directiva.orden = $("#orden").val();
            uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
            if($scope.validar_form()==true){
                //Para guardar
                if(($scope.directiva.id!="")&&($scope.directiva.id!=undefined)){
                    $scope.modificar_directiva();
                }else{
                    $scope.insertar_directiva();
                }
            }
            //desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

        }
        ///////////////////////////////////////////////////////////////////////////////////
        $scope.insertar_directiva = function(){
            $http.post($scope.base_url+"/Directiva/registrarDirectiva",
            {
                'id'          : $scope.directiva.id,
                'id_idioma'  : $scope.directiva.id_idioma,
                'titulo'     : $scope.directiva.titulo,
                'descripcion': $scope.directiva.descripcion,
                'id_imagen'  : $scope.directiva.id_imagen,
                'orden':  $scope.directiva.orden
            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "registro_procesado"){
                    mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
                    $scope.limpiar_cajas_directiva();
                }else if($scope.mensajes.mensaje == "existe"){
                    mostrar_notificacion("Mensaje","Ya existe una noticia con ese titulo","warning");
                }
                else{
                    mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
                }

            }).error(function(data,estatus){
                console.log(data);
            });
        }
        //////////////////////////
        $scope.modificar_directiva = function(){
            $scope.directiva.orden = $("#orden").val();
            $http.post($scope.base_url+"/Directiva/modificarDirectiva",
            {
                'id'          : $scope.directiva.id,
                'id_idioma'  : $scope.directiva.id_idioma,
                'titulo'     : $scope.directiva.titulo,
                'descripcion': $scope.directiva.descripcion,
                'id_imagen'  : $scope.directiva.id_imagen,
                'boton'      : $scope.directiva.boton,
                'url'        : $scope.directiva.url,
                'direccion' : $scope.directiva.direccion,
                'orden'     : $scope.directiva.orden,
                'inicial'    : $scope.directiva.inicial
            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "modificacion_procesada"){
                    mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
                    $scope.directiva.inicial = $scope.directiva.orden;
                    desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

                }else{
                    mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
                    desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

                }
                //desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

            }).error(function(data,estatus){
                console.log(data);
            });
        }
        //////////////////////////
        $scope.validar_form = function(){
            //console.log($scope.directiva)
            if(($scope.directiva.id_idioma=="")||($scope.directiva.id_idioma=="")){
                mostrar_notificacion("Campos no validos","Debe seleccionar el idioma","warning");
                return false;
            }/*else if(($scope.directiva.descripcion=="")||($scope.directiva.descripcion=="")){
                mostrar_notificacion("Campos no validos","Debe seleccionar la dirección del texto del slide","warning");
                return false;
            }*/ else if($scope.directiva.titulo==""){
                mostrar_notificacion("Campos no validos","Debe ingresar el titulo","warning");
                return false;
            }else if(($scope.directiva.orden=="NULL")||($scope.directiva.orden=="")){
                mostrar_notificacion("Campos no validos","Debe seleccionar el orden","warning");
                return false;
            }
            else if(($scope.directiva.id_imagen=="NULL")||($scope.directiva.id_imagen=="")){
                mostrar_notificacion("Campos no validos","Debe seleccionar la imagen","warning");
                return false;
            }
            else{
                return true;
            }
        }
        /////////////////////////
        $scope.limpiar_cajas_directiva = function(){
            
            $scope.directiva = {
                        'id': '',
                        'idioma': '',
                        'id_idioma' : '',
                        'titulo' : '',
                        'descripcion' : '',
                        'id_imagen' : '',
                        'imagen' : 'assets/images/logo_peque.png',
                        'orden':''
            }    

            $scope.activo_img = "inactivo";
            $("#div_descripcion").html($scope.titulo_text);
            $('#textarea_editor').data("wysihtml5").editor.clear();
            $("#idioma").removeAttr("disabled");
            $scope.titulo_registrar = "Registrar";
            $scope.subtitulo_pagina  = "Registrar directiva";
            $("#nuevo").css({"display":"none"})
        }
        //--
        $scope.consultarDirectivaIndividual = function(){
            uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
            directivaFactory.asignar_valores("",$scope.id_directiva,$scope.base_url)
            directivaFactory.cargar_directiva(function(data){
                
                $scope.directiva=data[0];
                                
                
                //alert($scope.directiva.orden)
                $scope.directiva=data[0];
                //--
                ordenFactory.asignar_valores($scope.directiva.id_idioma,$scope.base_url,"2")
                ordenFactory.cargar_orden_directiva(function(data){
                    $scope.ordenes=data;
                    //console.log($scope.ordenes)
                    //-Elimino el select
                    eliminarOptions("orden")
                    $.each( $scope.ordenes, function( indice, elemento ){
                          agregarOptions("#orden", elemento.orden, elemento.orden)
                    });
                });
                //--
                //console.log(data[0]);
                $("#div_descripcion").html($scope.directiva.descripcion)
                $scope.borrar_imagen.push($scope.directiva.id_imagen);
                $scope.activo_img = "activo"
                $scope.directiva.imagen = $scope.directiva.ruta
                $scope.titulo_registrar = "Modificar";
                $scope.subtitulo_pagina  = "Modificar directiva";
            
                setTimeout(function(){
                    $('#idioma > option[value="'+$scope.directiva.id_idioma+'"]').prop('selected', true);
                    $('#orden > option[value="'+$scope.directiva.orden+'"]').prop('selected', true);
                    $scope.directiva.inicial = $scope.directiva.orden;
                },500);
                
                //$("#idioma").attr("disabled");
                $("#idioma").prop('disabled', true);
            
                /*setTimeout(function(){
                    $('#idioma > option[value="'+$scope.directiva.id_idioma+'"]').prop('selected', true);
                    $('#direccion_slide > option[value="'+$scope.directiva.direccion+'"]').prop('selected', true);

                },300);*/
                //$("#idioma").attr("disabled");
                //$("#idioma").prop('disabled', true);
                desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
            });
        }
        //---
        ///////////////////////////////////////////////////
        $scope.consultar_idioma();
        $scope.consultar_galeria_img();

        $scope.id_directiva = $("#id_directiva").val();
            if($scope.id_directiva){
                $scope.consultarDirectivaIndividual();
            }else{
                $("#idioma").removeAttr("disabled");
            }
        //-----------------------------------------------------------------------
    })
    .controller("footerConsultasController", function($scope,$http,$location,serverDataMensajes,footerFactory,sesionFactory,idiomaFactory){
        //Cuerpo declaraciones
        $(".li-menu").removeClass("active");
        $("#li_contactos").addClass("active");    
        $(".a-menu").removeClass("active");
        $("#footer").addClass("active");    
        $scope.titulo_pagina = "Consulta de Footer";
        $scope.footer = {
                                'id':'',
                                'id_idioma':'',
                                'correo':'',
                                'descripcion':'',
                                'estatus':'',
        }
        $scope.categorias_menu = "1";
        $scope.id_footer = "";
        $scope.base_url = $("#base_url").val();
        //-----------------------------------------------------
        //--Cuerpo de metodos  --/
        $scope.iniciar_datatable = function(){
            //--Iniciar DataTable
            $('#myTable').DataTable({
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                    "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
                "order": [[ 0, "desc" ]]
            });
            //console.log($scope.categorias);
        }
        $scope.consultar_footer = function(){
            footerFactory.asignar_valores("","",$scope.base_url)
            footerFactory.cargar_footer(function(data){
                $scope.footer=data;
                //console.log($scope.footer);                
            });
        }
        //---------------------------------------------------------------
        $scope.ver_footer = function(valor){
            var id = $("#ver"+valor).attr("data");
            $scope.id_footer = id;    
            $("#id_footer").val($scope.id_footer)
            let form = document.getElementById('formConsultaFooter');
            form.action = "./footerVer";
            form.submit();
        }
        //----------------------------------------------------------------
        $scope.activar_registro = function(event){
            $scope.id_seleccionado_footer = []
            $scope.estatus_seleccionado_footer = []
            var caja = event.currentTarget.id;
            var atributos = $("#"+caja).attr("data");
            var arreglo_atributos = atributos.split("|");
            $scope.id_seleccionado_footer = arreglo_atributos[0];
            $scope.estatus_seleccionado_footer = arreglo_atributos[1];
            $("#cabecera_mensaje").text("Información:");            
            //--
            if ($scope.estatus_seleccionado_footer==0){
                mensaje = "Desea modificar el estatus de este registro a publicado? ";
                $scope.estatus_seleccionado_footer=1;
            }else{
                mensaje = "Desea modificar el estatus de este registro a inactivo? ";
                $scope.estatus_seleccionado_footer=0
            }
            //($scope.estatus_seleccionado_categorias==1) ? $scope.estatus_seleccionado_categorias=0:$scope.estatus_seleccionado_categorias=1;

            //--
            $scope.modificar_estatus("activar_inactivar",mensaje)
        }
        //----------------------------------------------------------------
        $scope.modificar_estatus = function(opcion,mensaje){
             swal({
                  title: 'Esta seguro?',
                  text: mensaje,
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Si!',
                  cancelButtonText: 'No',
                }).then((result) => {
                      if (result.value) {
                          $scope.accion_estatus()
                      }
                })
        }
        //----------------------------------------------------------------
        $scope.accion_estatus = function(){

            $http.post($scope.base_url+"/Footer/modificarFooterEstatus",
            {
                 'id':$scope.id_seleccionado_footer,    
                 'estatus':$scope.estatus_seleccionado_footer,     
            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "modificacion_procesada"){
                    Swal(
                          'Realizado!',
                          'El proceso fue ejecutado.',
                          'success'
                    ).then((result) => {
                          if (result.value) {
                                let form = document.getElementById('formConsultaFooter');
                                form.action = "./consultarFooter";
                                form.submit();
                          }
                    
                    });
                }else{
                    Swal(
                          'No realizado!',
                          'El proceso no pudo ser ejecutado.',
                          'warning'
                    )
                }
            }).error(function(data,estatus){
                console.log(data);
            });    
        }
        //----------------------------------------------------------------
        $scope.eliminar_registro = function(event){
            $scope.id_seleccionado_footer = []
            $scope.estatus_seleccionado_footer = []
            var caja = event.currentTarget.id;
            //alert(caja);
            var atributos = $("#"+caja).attr("data");
            var arreglo_atributos = atributos.split("|");
            $scope.id_seleccionado_footer = arreglo_atributos[0];
            $scope.estatus_seleccionado_footer = arreglo_atributos[1];
            //console.log($scope.estatus_seleccionado_footer);
            $("#cabecera_mensaje").text("Información:");
            if ($scope.estatus_seleccionado_footer==2){
                mensaje = "Desea eliminar este registro? ";
            }
            $scope.modificar_estatus("eliminar",mensaje)
        }
        //----------------------------------------------------------------
        //-- Cuerpo de funciones--/
        setTimeout(function(){
                $scope.iniciar_datatable();
        },500);
        $scope.consultar_footer();
        
        //-----------------------------------------------------
    })
    .controller("footerController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,sesionFactory,idiomaFactory,footerFactory){
        //Cuerpo declaraciones
        $(".li-menu").removeClass("active");
        $("#li_contactos").addClass("active");    
        $(".a-menu").removeClass("active");
        $("#footer").addClass("active");        
        $scope.titulo_pagina = "Footer";
        $scope.subtitulo_pagina  = "Registrar footer";
        $scope.activo_img = "inactivo";
        $scope.footer = {
                                'id':'',
                                'id_idioma':'',
                                'correo':'',
                                'telefono':'',
                                'descripcion':'',
                                'estatus':'',
        }
        $scope.id_footer = ""
        $scope.searchFooter = []
        $scope.borrar_imagen = []
        $scope.currentTab = 'datos_basicos'
        $scope.titulo_registrar = "Registrar";
        $scope.titulo_text = "Pulse aquí para ingresar el contenido  del footer"
        $scope.base_url = $("#base_url").val();
        //--------------------------------------------------------
        //Cuerpo de metodos
        $scope.consultar_idioma = function(){
            idiomaFactory.asignar_valores("",$scope.base_url)
            idiomaFactory.cargar_idioma(function(data){
                $scope.idioma=data;
                //console.log($scope.idioma);
            });
        }
        $scope.agregarWisi = function(){
            //$('#text_editor').data("wysihtml5").editor.clear();
            $scope.footer.descripcion = $(".textarea_editor").val()
            $("#div_descripcion").html($scope.footer.descripcion)
            $("#cerrarModal").click();
        }
        $scope.wisi_modal = function(){
            $("#wisiModal").modal("show")
            $(".textarea_editor").data("wysihtml5").editor.setValue($scope.footer.descripcion)
        }
        //--
        $scope.registrarFooter = function(){

            uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

            if($scope.validar_form()==true){
                //Para guardar
                if(($scope.footer.id!="")&&($scope.footer.id!=undefined)){
                    $scope.modificar_footer();
                }else{
                    $scope.insertar_footer();
                }

            }
            
            //desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
        }

        $scope.insertar_footer = function(){
            $http.post($scope.base_url+"/Footer/registrarFooter",
            {
                'correo'     : $scope.footer.correo,
                'descripcion': $scope.footer.descripcion,
                'id_idioma'  : $scope.footer.id_idioma.id,
                'telefono': $scope.footer.telefono,
            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "registro_procesado"){
                    mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
                    $scope.limpiar_cajas_footer();
                }
                else{
                    mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
                }
            }).error(function(data,estatus){
                console.log(data);
            });
        }

        $scope.modificar_footer = function(){
            $http.post($scope.base_url+"/Footer/modificarFooter",
            {
                'id'         : $scope.footer.id,
                'correo'     : $scope.footer.correo,
                'descripcion': $scope.footer.descripcion,
                'id_idioma'  : $scope.footer.id_idioma,
                'telefono': $scope.footer.telefono,
            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "registro_procesado"){
                    mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
                }else if($scope.mensajes.mensaje == "no_existe"){
                    mostrar_notificacion("Mensaje","No existe el footer","warning");
                }
                else{
                    mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
                }
            }).error(function(data,estatus){
                console.log(data);
            });
        }

        $scope.validar_form = function(){
            //console.log($scope.noticias)
            if(($scope.footer.id_idioma=="NULL")||($scope.footer.id_idioma=="")){
                mostrar_notificacion("Campos no validos","Debe seleccionar el idioma","warning");
                return false;
            } else if($scope.footer.correo==""){
                mostrar_notificacion("Campos no validos","Debe ingresar el correo","warning");
                return false;
            }else if($scope.footer.descripcion==""){
                mostrar_notificacion("Campos no validos","Debe ingresar la descripción","warning");
                return false;
            }
            else{
                return true;
            }
        }

        $scope.limpiar_cajas_footer = function(){
            $scope.footer = {
                                'id':'',
                                'id_idioma':'',
                                'correo':'',
                                'telefono':'',
                                'descripcion':'',
                                'estatus':'',
            }

            $('#text_editor').data("wysihtml5").editor.clear();
            $("#div_descripcion").html($scope.titulo_text);
        }
        //--
        $scope.consultarFooterIndividual = function(){

            uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

            footerFactory.asignar_valores("",$scope.id_footer,$scope.base_url)
            footerFactory.cargar_footer(function(data){
                $scope.footer=data[0];
                //console.log(data[0]);
                $("#div_descripcion").html($scope.footer.descripcion)
                $scope.titulo_registrar = "Modificar";
                $scope.subtitulo_pagina  = "Modificar footer";
                setTimeout(function(){
                    $('#idioma > option[value="'+$scope.footer.id_idioma+'"]').prop('selected', true);
                },300);
                $("#idioma").prop('disabled', true);
            });

            desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

        }
        //---
        //--------------------------------------------------------
        //Cuerpo de llamados
        $scope.consultar_idioma();
        //--Si tiene id categoria .... consulto los datos relacionados a ese id, esto es cuando proviene de una consulta
        $scope.id_footer  = $("#id_footer").val();
        if($scope.id_footer){
            $scope.consultarFooterIndividual();
        }else{
            $("#idioma").prop('disabled', false);
        }
        //--------------------------------------------------------
    })
    .controller("GaleriaMultimediaController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,sesionFactory,idiomaFactory,categoriasFactory,upload){
        //Cuerpo declaraciones
        $(".li-menu").removeClass("active");
        $("#li_configuracion").addClass("active");    
        $(".a-menu").removeClass("active");
        $("#galeria").addClass("active");    
        $scope.titulo_pagina = "Galería Multimedia";
        $scope.subtitulo_pagina  = "Administrar galería";
        $scope.activo_img = "inactivo";
        $scope.galeria = {
                            "id":"",
                            "descripcion":"",
                            "categoria":"",
                            "url":""
        }
        $scope.titulo_registrar = "Registrar";
        $scope.categorias_menu = "1";
        $scope.base_url = $("#base_url").val();
        $scope.currentTab = 'datos_basicos'

        //Cuerpo de metodos
        //--
        $scope.consultarCategoriaIndividual = function(){
            categoriasFactory.asignar_valores("","",$scope.base_url)
            categoriasFactory.cargar_categorias(function(data){
                $scope.categorias=data;
                //console.log($scope.categorias);
            });
        }
        $("#formDropZone").dropzone({ url: $scope.base_url+"/GaleriaMultimedia/Upload" });
        //--
        $scope.validar_form = function(){
            console.log($scope.file)
            if($scope.galeria.descripcion==""){
                mostrar_notificacion("Campos no validos","Debe ingresar el título de la imagen","warning");
                return false;
            }else if(($scope.galeria.categoria=="")||($scope.galeria.categoria==undefined)||$scope.galeria.categoria==0){
                mostrar_notificacion("Campos no validos","Debe seleccionar la categoría","warning");
                return false;
            }else if(($scope.file==undefined)||($scope.file=="")){
                mostrar_notificacion("Campos no validos","Debe Seleccionar una imagen","warning");
                return false;
            }
            else{
                return true;
            }
        }
        //--
        $scope.registrarGaleria = function(){

            uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

            if($scope.validar_form()==true){
                //Para guardar
                //alert("id_personas:"+$scope.doctor.id_personas);
                //alert("id_doctor:"+$scope.doctor.id);
                $scope.galeria.id = $scope.id_galeria;
                if(($scope.galeria.id!=undefined)&&($scope.galeria.id!="")){
                    $scope.modificar_galeria();    
                }else{
                    $scope.insertar_galeria();
                }        
            }

            //desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

        }
        //--
        $scope.insertar_galeria = function (){
            $scope.uploadFile();
        }
        //---
        //---------------------------------------
        //--Metodo para subir acrhivos
        $scope.uploadFile = function(){
            var file = $scope.file;
            var categoria = $scope.galeria.categoria.id;
            var nombre_archivo = $scope.galeria.descripcion;
            var base_url = $scope.base_url;
            //-
            upload.uploadFile(file,categoria,nombre_archivo,base_url).then(function(res){
                console.log(res.data.mensaje);
                if(res.data.mensaje=="registro_procesado"){
                    mostrar_notificacion("Mensaje","La imagen fue cargada de manera exitosa!","info");
                    //$scope.consultar_galeria();
                    $scope.limpiar_imagen();
                }else{
                    mostrar_notificacion("Mensaje"," Error al subir tipo de archivo, solo puede subir imagenes .jpg con un peso menor a 1 mb con medidas entre w:5000 h:5000    ","warning");
                }
                //--------------------------------
            });
            //-
        }
        //--
        $scope.limpiar_imagen = function(){
            $scope.galeria = {
                            "id":"",
                            "descripcion":"",
                            "categoria":"",
                            "url":""
            }
            $("#list").css("display","block");
            $(".imgbiblioteca_principal").css("display","none");
            $("#file").val("");
            $("#categorias").val("");
            $("#previa").css("display","block");
            $scope.file = "";
            //$("#select_categoria").selectpicker('refresh');
        }
        //---------------------------------------
        //---
        //Cuerpo de llamados
        $scope.consultarCategoriaIndividual();
    })
    .controller("mainController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,sesionFactory,idiomaFactory,categoriasFactory){
        //Cuerpo declaraciones
        $(".li-menu").removeClass("active");
        $(".a-menu").removeClass("active");
        $scope.inicio = {
                                'usuario':'',
                                'clave':'',
        }

        $scope.base_url = $("#base_url").val();
        
        $scope.ingresar = function(){
            if($scope.validar_is()==true){
                uploader_reg("#mensaje_is","#usuario,#clave")
                $http.post($scope.base_url+"/Login/iniciarSession",
                {
                    'usuario':$scope.inicio.usuario,
                    'clave':$scope.inicio.clave,
                }).success(function(data, estatus, headers, config){
                    $scope.mensajes  = data;
                    if($scope.mensajes.mensajes == "inicio_exitoso"){
                        $scope.ir_dashboard();
                    }
                    else{
                        mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
                        desbloquear_pantalla("#mensaje_is","#usuario,#clave")
                    }

                }).error(function(data,estatus){
                    console.log(data);
                });
            }
        }
        //--
        $scope.validar_is =  function(){
            if($scope.inicio.usuario==""){
                mostrar_notificacion("Campos no validos","Debe ingresar el nombre de usuario","warning");
                return false;
            }else if($scope.inicio.clave==""){
                mostrar_notificacion("Campos no validos","Debe ingresar la clave","warning");
                return false;
            }
            else{
                return true;
            }
        }
        //--
        $scope.ir_dashboard = function(){
            let form = document.getElementById('loginform');
            form.action = "./cms/dashboard";
            form.submit();
        }
    })
    .controller("noticiasConsultasController", function($scope,$http,$location,serverDataMensajes,noticiasFactory,sesionFactory,idiomaFactory){
        //Cuerpo declaraciones
        $(".li-menu").removeClass("active");
        $("#li_noticias").addClass("active");    
        $(".a-menu").removeClass("active");
        $("#noticias").addClass("active");    
        $scope.titulo_pagina = "Consulta de Noticias";
        $scope.activo_img = "inactivo";
        $scope.categorias = {
                                'id':'',
                                'descripcion':'',
                                'estatus':''
        }
        $scope.categorias_menu = "1";
        $scope.id_categoria = "";
        $scope.base_url = $("#base_url").val();
        //-----------------------------------------------------
        //--Cuerpo de metodos  --/
        $scope.iniciar_datatable = function(){
            //--Iniciar DataTable
            $('#myTable').DataTable({
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                    "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
                "order": [[ 0, "desc" ]]
            });
            //console.log($scope.categorias);
        }
        $scope.consultar_noticias = function(){
            noticiasFactory.asignar_valores("","",$scope.base_url)
            noticiasFactory.cargar_noticias(function(data){
                $scope.noticias=data;
                //console.log($scope.noticias);
                $(".sorting_desc").click();                
            });
        }
        //---------------------------------------------------------------
        $scope.ver_noticia = function(valor){
            var id = $("#ver"+valor).attr("data");
            $scope.id_noticias = id;    
            $("#id_noticias").val($scope.id_noticias)
            let form = document.getElementById('formConsultaNoticias');
            form.action = "./noticiasVer";
            form.submit();
        }
        //----------------------------------------------------------------
        $scope.activar_registro = function(event){
            $scope.id_seleccionado_noticias = []
            $scope.estatus_seleccionado_noticias = []
            var caja = event.currentTarget.id;
            var atributos = $("#"+caja).attr("data");
            var arreglo_atributos = atributos.split("|");
            $scope.id_seleccionado_noticias = arreglo_atributos[0];
            $scope.estatus_seleccionado_noticias = arreglo_atributos[1];
            $("#cabecera_mensaje").text("Información:");            
            //--
            if ($scope.estatus_seleccionado_noticias==0){
                mensaje = "Desea modificar el estatus de este registro a publicado? ";
                $scope.estatus_seleccionado_noticias=1;
            }else{
                mensaje = "Desea modificar el estatus de este registro a inactivo? ";
                $scope.estatus_seleccionado_noticias=0
            }
            //($scope.estatus_seleccionado_categorias==1) ? $scope.estatus_seleccionado_categorias=0:$scope.estatus_seleccionado_categorias=1;

            //--
            $scope.modificar_estatus("activar_inactivar",mensaje)
        }
        //----------------------------------------------------------------
        $scope.modificar_estatus = function(opcion,mensaje){
             swal({
                  title: 'Esta seguro?',
                  text: mensaje,
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Si!',
                  cancelButtonText: 'No',
                }).then((result) => {
                      if (result.value) {
                          $scope.accion_estatus()
                      }
                })
        }
        //----------------------------------------------------------------
        $scope.accion_estatus = function(){

            $http.post($scope.base_url+"/Noticias/modificarNoticiasEstatus",
            {
                 'id':$scope.id_seleccionado_noticias,    
                 'estatus':$scope.estatus_seleccionado_noticias,     

            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "modificacion_procesada"){
                    Swal(
                          'Realizado!',
                          'El proceso fue ejecutado.',
                          'success'
                    ).then((result) => {
                          if (result.value) {
                                let form = document.getElementById('formConsultaNoticias');
                                form.action = "./consultarNoticias";
                                form.submit();
                          }
                    
                    });
                }else{
                    Swal(
                          'No realizado!',
                          'El proceso no pudo ser ejecutado.',
                          'warning'
                    )
                }
            }).error(function(data,estatus){
                console.log(data);
            });    
        }
        //----------------------------------------------------------------
        $scope.eliminar_registro = function(event){
            $scope.id_seleccionado_noticias = []
            $scope.estatus_seleccionado_noticias = []
            var caja = event.currentTarget.id;
            //alert(caja);
            var atributos = $("#"+caja).attr("data");
            var arreglo_atributos = atributos.split("|");
            $scope.id_seleccionado_noticias = arreglo_atributos[0];
            $scope.estatus_seleccionado_noticias = arreglo_atributos[1];
            //console.log($scope.estatus_seleccionado_noticias);
            $("#cabecera_mensaje").text("Información:");
            if ($scope.estatus_seleccionado_noticias==2){
                mensaje = "Desea eliminar este registro? ";
            }
            $scope.modificar_estatus("eliminar",mensaje)
        }
        //----------------------------------------------------------------
        //-- Cuerpo de funciones--/
        setTimeout(function(){
                $scope.iniciar_datatable();
        },500);
        $scope.consultar_noticias();
        
        //-----------------------------------------------------
    })
    .controller("noticiasController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,sesionFactory,idiomaFactory,noticiasFactory){
        //Cuerpo declaraciones
        $(".li-menu").removeClass("active");
        $("#li_noticias").addClass("active");    
        $(".a-menu").removeClass("active");
        $("#noticias").addClass("active");    
        $scope.titulo_pagina = "Noticias";
        $scope.subtitulo_pagina  = "Registrar noticias";
        $scope.activo_img = "inactivo";
        $scope.noticias = {
                                'id':'',
                                'id_idioma':'',
                                'titulo':'',
                                'descripcion':'',
                                'estatus':'',
                                'id_imagen':''
        }
        $scope.id_noticias = ""
        $scope.searchNoticias = []
        $scope.borrar_imagen = []
        $scope.currentTab = 'datos_basicos'
        $scope.titulo_registrar = "Registrar";
        $scope.titulo_text = "Pulse aquí para ingresar el contenido  de la noticia"
        $scope.base_url = $("#base_url").val();

        //--------------------------------------------------------
        //Cuerpo de metodos
        $scope.consultar_idioma = function(){
            idiomaFactory.asignar_valores("",$scope.base_url)
            idiomaFactory.cargar_idioma(function(data){
                $scope.idioma=data;
                $.each( $scope.idioma, function( indice, elemento ){
                      agregarOptions("#idioma", elemento.id, elemento.descripcion)
                });
                $('#idioma > option[value="0"]').prop('selected', true);

                //console.log($scope.idioma);
            });
        }
        $scope.agregarWisi = function(){
            //$('#text_editor').data("wysihtml5").editor.clear();
            $scope.noticias.descripcion = $(".textarea_editor").val()
            $("#div_descripcion").html($scope.noticias.descripcion)
            $("#cerrarModal").click();
        }
        $scope.wisi_modal = function(){
            $("#wisiModal").modal("show")
            $(".textarea_editor").data("wysihtml5").editor.setValue($scope.noticias.descripcion)
        }
        //--
        $scope.consultar_galeria_img = function(){
            galeriaFactory.asignar_valores('4','',$scope.base_url);//ya que es galeria de imagenes
            galeriaFactory.cargar_galeria_fa(function(data){
                $scope.galery=data;
                //console.log($scope.galery);
            });
        }

        $scope.seleccione_img_principal = function(){
            $("#modal_img1").modal("show");
        }

        $scope.seleccionar_imagen = function(event){
            var imagen = event.target.id;//Para capturar id
            var vec = $("#"+imagen).attr("data");
            var vector_data = vec.split("|")
            var id_imagen = vector_data[0];
            var ruta = vector_data[1];

            $scope.borrar_imagen.push(id_imagen);
            $scope.activo_img = "activo"
            $scope.noticias.id_imagen = id_imagen
            $scope.noticias.imagen = ruta
            //--
            $("#modal_img1").modal("hide");
            //--
        }

        $scope.registrarNoticias = function(){
            
            uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

            if($scope.validar_form()==true){
                //Para guardar
                if(($scope.noticias.id!="")&&($scope.noticias.id!=undefined)){
                    $scope.modificar_noticias();
                }else{
                    $scope.insertar_noticias();
                }

            }

            //desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

        }

        $scope.insertar_noticias = function(){
            $http.post($scope.base_url+"/Noticias/registrarNoticias",
            {
                'id'         : $scope.noticias.id,
                'titulo'     : $scope.noticias.titulo,
                'id_imagen'  : $scope.noticias.id_imagen,
                'descripcion': $scope.noticias.descripcion,
                'id_idioma'  : $scope.noticias.id_idioma,
            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "registro_procesado"){
                    mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
                    $scope.limpiar_cajas_noticias();
                }else if($scope.mensajes.mensaje == "existe"){
                    mostrar_notificacion("Mensaje","Ya existe una noticia con ese titulo","warning");
                }
                else{
                    mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
                }
            }).error(function(data,estatus){
                console.log(data);
            });
        }

        $scope.modificar_noticias = function(){
            $http.post($scope.base_url+"/Noticias/modificarNoticias",
            {
                'id':$scope.noticias.id,
                'titulo': $scope.noticias.titulo,
                'id_imagen':$scope.noticias.id_imagen,
                'descripcion':$scope.noticias.descripcion,
                'id_idioma':$scope.noticias.id_idioma,
            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "registro_procesado"){
                    mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
                }else if($scope.mensajes.mensaje == "existe"){
                    mostrar_notificacion("Mensaje","Ya existe una noticia con ese titulo","warning");
                }
                else{
                    mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
                }
            }).error(function(data,estatus){
                console.log(data);
            });
        }

        $scope.validar_form = function(){
            //console.log($scope.noticias)
            if(($scope.noticias.id_idioma=="NULL")||($scope.noticias.id_idioma=="")){
                mostrar_notificacion("Campos no validos","Debe seleccionar el idioma","warning");
                return false;
            } else if($scope.noticias.titulo==""){
                mostrar_notificacion("Campos no validos","Debe ingresar el titulo","warning");
                return false;
            }else if($scope.noticias.descripcion==""){
                mostrar_notificacion("Campos no validos","Debe ingresar la descripción","warning");
                return false;
            }else if(($scope.noticias.id_imagen=="NULL")||($scope.noticias.id_imagen=="")){
                mostrar_notificacion("Campos no validos","Debe seleccionar la imagen","warning");
                return false;
            }
            else{
                return true;
            }
        }

        $scope.limpiar_cajas_noticias = function(){
            $scope.noticias = {
                                'id':'',
                                'id_idioma':'',
                                'titulo':'',
                                'descripcion':'',
                                'estatus':'',
                                'id_imagen':''
            }

            $scope.activo_img = "inactivo";

            $('#text_editor').data("wysihtml5").editor.clear();
            $("#div_descripcion").html($scope.titulo_text);
        }
        //--
        $scope.consultarNoticiasIndividual = function(){

            uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

            noticiasFactory.asignar_valores("",$scope.id_noticias,$scope.base_url)
            noticiasFactory.cargar_noticias(function(data){
                $scope.noticias=data[0];
                //console.log(data[0]);
                $("#div_descripcion").html($scope.noticias.descripcion)
                $scope.borrar_imagen.push($scope.noticias.id_imagen);
                $scope.activo_img = "activo"
                //-Cambio 10022020
                
                $scope.noticias.imagen = $scope.noticias.ruta
                $scope.titulo_registrar = "Modificar";
                $scope.subtitulo_pagina  = "Modificar categorías";
                setTimeout(function(){
                    $('#idioma > option[value="'+$scope.noticias.id_idioma+'"]').prop('selected', true);
                },300);
                $("#idioma").prop('disabled', true);
            });

            desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

        }
        //---
        //--------------------------------------------------------
        //Cuerpo de llamados
        $scope.consultar_idioma();
        $scope.consultar_galeria_img();
        //--Si tiene id categoria .... consulto los datos relacionados a ese id, esto es cuando proviene de una consulta
        $scope.id_noticias  = $("#id_noticias").val();
        if($scope.id_noticias){
            $scope.consultarNoticiasIndividual();
        }
        //--------------------------------------------------------

    })
    .controller("noticiasDetallesController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,sesionFactory,idiomaFactory,noticiasFactory){
        $scope.base_url = $("#base_url").val();
        $scope.idioma = $("#id_idioma").val();
        //console.log($scope.idioma);
        $(".menu_web").removeClass("active")
        $("#menu4").addClass("active")
        alert("Noticias Detalles!")
        //------------------------------------------------------------
        /*
        *    Bloque obligatorio para todo controlador
        */
        /*
        *    Define segun el valor del idioma los titulos
        */
        $scope.definir_url_home = function(){
            $scope.titulos_home = multIdioma.cambiar_idioma_home()
            //console.log($scope.btn);
        }
        /*
        *
        */
        //-----------------------------------------------------------
    
        //---
        $scope.definir_url_home();
        //
    })
    .controller("palabrasClavesController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,sesionFactory,idiomaFactory,palabrasClavesFactory){
        //Cuerpo declaraciones
        $(".li-menu").removeClass("active");
        $("#li_configuracion").addClass("active");    
        $(".a-menu").removeClass("active");
        $("#palabras_claves").addClass("active");        
        $scope.titulo_pagina = "Palabras Claves";
        $scope.subtitulo_pagina  = "Registrar palabras claves";
        $scope.palabras_claves = {
                                'id':'',
                                'descripcion':''
        }
        $scope.id = ""
        $scope.searchFooter = []
        $scope.titulo_registrar = "Registrar";
        $scope.base_url = $("#base_url").val();
        //--------------------------------------------------------
        //Cuerpo de metodos

        $scope.registrarDescripcion = function(){

            uploader_reg("#div_mensaje","#btn-limpiar,#btn-registrar");

            if($scope.validar_form()==true){
                //Para guardar
                if(($scope.descripcion.id!="")&&($scope.descripcion.id!=undefined)){
                    $scope.modificar_descripcion();
                }else{
                    $scope.insertar_descripcion();
                }

            }
            
            //desbloquear_pantalla("#div_mensaje","#btn-registrar");
        }

        $scope.insertar_descripcion = function(){
            $http.post($scope.base_url+"/palabras_claves/registrarDescripcion",
            {
                'descripcion': $scope.descripcion.descripcion,
            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "registro_procesado"){
                    $scope.descripcion.id = $scope.mensajes.id
                    mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
                    $scope.limpiar_cajas_descripcion();
                }
                else{
                    mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
                }
            }).error(function(data,estatus){
                console.log(data);
            });
        }

        $scope.modificar_descripcion = function(){
            $http.post($scope.base_url+"/palabras_claves/modificarDescripcion",
            {
                'id'         : $scope.descripcion.id,
                'descripcion': $scope.descripcion.descripcion
            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "modificacion_procesada"){
                    mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
                }else if($scope.mensajes.mensaje == "no_existe"){
                    mostrar_notificacion("Mensaje","No existe el registro","warning");
                }
                else{
                    mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
                }
            }).error(function(data,estatus){
                console.log(data);
            });
        }

        $scope.validar_form = function(){
            if($scope.descripcion.descripcion==""){
                mostrar_notificacion("Campos no validos","Debe ingresar la descripción","warning");
                return false;
            }
            else{
                return true;
            }
        }

        $scope.limpiar_cajas_descripcion = function(){
            $scope.footer = {
                                'id':'',
                                'id_idioma':'',
                                'correo':'',
                                'descripcion':'',
                                'estatus':'',
            }

            $('#text_editor').data("wysihtml5").editor.clear();
            $("#div_descripcion").html($scope.titulo_text);
        }
        //--
        $scope.consultarDescripcion = function(){

            uploader_reg("#div_mensaje","#btn-registrar");

            palabrasClavesFactory.asignar_valores("",$scope.base_url)

            palabrasClavesFactory.cargar_descripcion(function(data){
                if(data[0]){
                    $scope.descripcion=data[0];
                    $scope.titulo_registrar = "Modificar";
                    $scope.subtitulo_pagina  = "Modificar palabras claves";
                }
            });
            desbloquear_pantalla("#div_mensaje","#btn-registrar");
        }
        //---
        //--------------------------------------------------------
        //Cuerpo de llamados
        $scope.consultarDescripcion()
        //--------------------------------------------------------
    })
    .controller("PdfMultimediaConsultasController", function($scope,$http,$location,serverDataMensajes,categoriasFactory,sesionFactory,idiomaFactory,pdfsFactory){
        $(".li-menu").removeClass("active");
        $("#li_configuracion").addClass("active");    
        $(".a-menu").removeClass("active");
        $("#carga_pdf").addClass("active");    
        $scope.titulo_pagina = "Consulta de pdf";
        $scope.activo_img = "inactivo";
        $scope.pdf = {
                            'id': '',
                            'idioma': '',
                            'id_idioma' : '',
                            'somos': '',
                            'digital_agency': '',
                            'estatus' : '',
                            'id_imagen' : '',
                            'imagen' : ''
        }
        $scope.nosotros_menu = "1";
        $scope.id_nosotros = "";
        $scope.base_url = $("#base_url").val();

        $scope.iniciar_datatable = function(){
            //--Iniciar DataTable
            $('#myTable').DataTable({
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                    "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
                "order": [[0, "asc" ],[2, "asc" ]]


            });
            //console.log($scope.nosotros);
        }

        $scope.consultar_pdfs = function(){
            pdfsFactory.asignar_valores("","",$scope.base_url)
            pdfsFactory.cargar_pdf(function(data){
                $scope.pdfs = data;
                //console.log($scope.productos);
            });
        }

        $scope.ver_producto = function(valor){
            var id = $("#ver"+valor).attr("data");
            $scope.id_pdf = id;
            //console.log($scope.id_pdf);
            $("#id_pdf").val($scope.id_pdf);
            let form = document.getElementById('formConsultaProductos');
            form.action = "./pdfVer";
            form.submit();
        }

        $scope.activar_registro = function(event){
                    $scope.id_seleccionado_productos = []
                    $scope.estatus_seleccionado_productos = []

                    var caja = event.currentTarget.id;
                    var atributos = $("#"+caja).attr("data");
                    var arreglo_atributos = atributos.split("|");

                    $scope.id_seleccionado_productos = arreglo_atributos[0];
                    $scope.estatus_seleccionado_productos = arreglo_atributos[1];

                    $("#cabecera_mensaje").text("Información:");

                    if ($scope.estatus_seleccionado_productos==0){
                        mensaje = "Desea modificar el estatus de este registro a publicado? ";
                        $scope.estatus_seleccionado_productos=1;
                    }else{
                        mensaje = "Desea modificar el estatus de este registro a inactivo? ";
                        $scope.estatus_seleccionado_productos=0
                    }
                    $scope.modificar_estatus("activar_inactivar",mensaje)
                }
                //----------------------------------------------------------------
                $scope.modificar_estatus = function(opcion,mensaje){
                     swal({
                          title: 'Esta seguro?',
                          text: mensaje,
                          type: 'warning',
                          showCancelButton: true,
                          confirmButtonColor: '#3085d6',
                          cancelButtonColor: '#d33',
                          confirmButtonText: 'Si!',
                          cancelButtonText: 'No',
                        }).then((result) => {
                              if (result.value) {
                                  $scope.accion_estatus()
                              }
                        })
                }
                //----------------------------------------------------------------
                $scope.accion_estatus = function(){
                    $http.post($scope.base_url+"/CargarPdf/modificarPDFEstatus",
                    {
                         'id':$scope.id_seleccionado_productos,
                         'estatus':$scope.estatus_seleccionado_productos,

                    }).success(function(data, estatus, headers, config){
                        $scope.mensajes  = data;
                        
                        if($scope.mensajes.mensaje == "modificacion_procesada"){
                            Swal(
                                  'Realizado!',
                                  'El proceso fue ejecutado.',
                                  'success'
                            ).then((result) => {
                                  if (result.value) {
                                        let form = document.getElementById('formConsultaProductos');
                                        form.action = "./consultarPDF";
                                        form.submit();
                                  }
                            });
                        }else{
                            Swal(
                                  'No realizado!',
                                  'El proceso no pudo ser ejecutado.',
                                  'warning'
                            )
                        }
                    }).error(function(data,estatus){
                        console.log(data);
                    });
                }
                //----------------------------------------------------------------
                //----------------------------------------------------------------
        $scope.eliminar_registro = function(event){
            $scope.id_seleccionado_productos = []
            $scope.estatus_seleccionado_productos = []
            var caja = event.currentTarget.id;
            //alert(caja);
            var atributos = $("#"+caja).attr("data");
            var arreglo_atributos = atributos.split("|");
            $scope.id_seleccionado_productos = arreglo_atributos[0];
            $scope.estatus_seleccionado_productos = arreglo_atributos[1];
            //console.log($scope.estatus_seleccionado_productos);
            $("#cabecera_mensaje").text("Información:");
            if ($scope.estatus_seleccionado_productos==2){
                mensaje = "Desea eliminar este registro? ";
            }
            $scope.modificar_estatus("eliminar",mensaje)
        }
        //----------------------------------------------------------------

        setTimeout(function(){
                        $scope.iniciar_datatable();
                },500);

        $scope.consultar_pdfs
        ();
    })
    .controller("PdfMultimediaController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,sesionFactory,idiomaFactory,categoriasFactory,upload,pdfsFactory){
        //Cuerpo declaraciones
        $(".li-menu").removeClass("active");
        $("#li_configuracion").addClass("active");    
        $(".a-menu").removeClass("active");
        $("#carga_pdf").addClass("active");    
        $scope.titulo_pagina = "Pdf Multimedia";
        $scope.subtitulo_pagina  = "Administrar Pdf";
        $scope.activo_img = "inactivo";
        $scope.pdf = {
                            "id":"",
                            "descripcion":"",
                            "categoria":"",
                            "titulo":"",
                            "url":""
        }
        $scope.id_pdf = ""

        $scope.titulo_registrar = "Registrar";
        $scope.categorias_menu = "1";
        $scope.base_url = $("#base_url").val();
        $scope.currentTab = 'datos_basicos'
        $scope.titulo_text = "Pulse aquí para ingresar la descripción del pdf"


        $scope.wisi_modal = function(){
            $("#wisiModal").modal("show")
            $(".textarea_editor").data("wysihtml5").editor.setValue($scope.pdf.descripcion)
        }
        $scope.agregarWisi = function(){
            //$('#text_editor').data("wysihtml5").editor.clear();
            $scope.pdf.descripcion = $(".textarea_editor").val()
            $("#div_descripcion").html($scope.pdf.descripcion)
            $("#cerrarModal").click();
        }
        //Cuerpo de metodos
        //--
        $scope.consultarCategoriaIndividual = function(){
            categoriasFactory.asignar_valores("","",$scope.base_url)
            categoriasFactory.cargar_categorias(function(data){
                $scope.categorias=data;
                //console.log($scope.file);
            });
        }
        $("#formDropZone").dropzone({ url: $scope.base_url+"/GaleriaMultimedia/Upload" });
        //--
        $scope.validar_form = function(){
            if(($scope.pdf.titulo=="NULL")||($scope.pdf.titulo=="")||(!$scope.pdf.titulo)){
                mostrar_notificacion("Campos no validos","Debe ingresar el titulo","warning");
                return false;
            }else if(($scope.pdf.categoria=="")||($scope.pdf.categoria==undefined)||($scope.pdf.categoria==0)){
                mostrar_notificacion("Campos no validos","Debe seleccionar la categoría","warning");
                return false;
            }else if($scope.pdf.descripcion==""){
                mostrar_notificacion("Campos no validos","Debe ingresar la descripción","warning");
                return false;
            }else if(($scope.file==undefined)||($scope.file=="")){
                mostrar_notificacion("Campos no validos","Debe Seleccionar un archivo pdf","warning");
                return false;
            }
            else{
                return true;
            }
        }
        //--
        $scope.registrarPdf = function(){

            //uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
            console.log($scope.pdf);
            console.log($scope.file);

            if($scope.validar_form()==true){
                //Para guardar
                //alert("id_personas:"+$scope.doctor.id_personas);
                //alert("id_doctor:"+$scope.doctor.id);
                if(($scope.pdf.id!=undefined)&&($scope.pdf.id!="")){
                    $scope.modificar_pdf();    
                }else{
                    $scope.insertar_pdf();
                }        
            }

            //desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

        }
        //--

        $scope.modificar_pdf = function(){
            
            $http.post($scope.base_url+"/CargarPdf/modificarPdf",
            {
                'id':$scope.pdf.id,
                'id_categoria': $scope.pdf.categoria.id,
                'descripcion':$scope.pdf.descripcion,
                'titulo':$scope.pdf.titulo,

            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "registro_procesado"){
                    mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
                }else if($scope.mensajes.mensaje == "existe"){
                    mostrar_notificacion("Mensaje","Ya existe un PDF con ese titulo","warning");
                }
                else{
                    mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
                }
            }).error(function(data,estatus){
                console.log(data);
            });
        }


        $scope.insertar_pdf = function (){
            $scope.uploadFile();
            
            console.log($scope.pdf);

        }
        //---
        //---------------------------------------
        //--Metodo para subir acrhivos
        $scope.uploadFile = function(){
            var file = $scope.file;
            var categoria = $scope.pdf.categoria.id;
            var descripcion = $scope.pdf.descripcion;
            var titulo = $scope.pdf.titulo;
            var base_url = $scope.base_url;
            //-
            //console.log(res.data.mensaje);

            upload.uploadFilePDF(file,categoria,descripcion,titulo,base_url).then(function(res){
                //console.log(res.data.mensaje);
                if(res.data.mensaje=="registro_procesado"){
                    mostrar_notificacion("Mensaje","El PDF fue cargada de manera exitosa!","info");
                    //$scope.consultar_galeria();
                    $scope.limpiar_imagen();
                }else if(res.data.mensaje=="existe"){
                    mostrar_notificacion("Mensaje","Existe un pdf con ese titulo!","warning");
                    //$scope.consultar_galeria();
                }else{
                    mostrar_notificacion("Mensaje"," Error al subir tipo de archivo, solo puede subir pdf con un peso menor a 1 mb","warning");
                }
                //--------------------------------
            });
            //-
        }
        //--
        $scope.consultarPdfIndividual = function(){

            uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

            pdfsFactory.asignar_valores("",$scope.id_pdf,$scope.base_url)
            pdfsFactory.cargar_pdf(function(data){
                $scope.pdf=data[0];
                console.log($scope.pdf);
                $("#div_descripcion").html($scope.pdf.descripcion)
                $scope.titulo_registrar = "Modificar";
                $scope.subtitulo_pagina  = "Modificar pdf";
                setTimeout(function(){
                    $('#categorias > option[value="'+$scope.pdf.id_categoria+'"]').prop('selected', true);
                },300);
                $("#titulo").prop('disabled', true);
                $("#file").prop('disabled', true);
                $("#list").css("display", "block");
                    $("#list").html([
                        '<input name="titulo_pdf" id="titulo_pdf" type="text" class="center-text text-center form-control form-control-line"placeholder="Ingrese la descripción de la categoría" ng-model="pdf.descripcion" required value="Archivo cargado" readonly>'
                    ].join('')).fadeIn("slow");
                    $scope.pdf.categoria = {id:$scope.pdf.id_categoria};
                    console.log($scope.pdf.categoria.id);
                    $scope.file ="1";
            }); 
            

            desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

        }//------------------------------------------------------------------------------

        $scope.limpiar_imagen = function(){
            $scope.pdf = {
                            "id":"",
                            "descripcion":"",
                            "titulo":"",
                            "categoria":"",
                            "url":""
            }
            $scope.file = "";
            $('#text_editor').data("wysihtml5").editor.clear();
            $("#div_descripcion").html($scope.titulo_text);
            document.getElementById('list').innerHTML='';
            $("#titulo_pdf").css("display","block");
            $(".imgbiblioteca_principal").css("display","none");
            $("#file").val("");
            $("#categorias").val("");
        }
        //---------------------------------------
        //---
        //Cuerpo de llamados
        $scope.consultarCategoriaIndividual();
        $scope.id_pdf  = $("#id_pdf").val();
        //console.log($scope.id_pdf);
        if($scope.id_pdf){
            $scope.consultarPdfIndividual();
        }
    })
    .controller("portafolioConsultaController", function($scope,$http,$location,serverDataMensajes,portafolioFactory,sesionFactory,idiomaFactory,portafolioFactory){
        $(".li-menu").removeClass("active");
        $("#li_productos").addClass("active");
        $(".a-menu").removeClass("active");
        $("#detalle_prod").addClass("active");

        $scope.titulo_pagina = "Portafolio";
        $scope.subtitulo_pagina = "Registro";
        $scope.titulo_registrar = "Registrar";
        $scope.titulo_cons = "Consultar";
        $scope.base_url = $("#base_url").val();

        $scope.id_portafolio = "";
        $scope.portafolio = {
                        'id': '',
                        'titulo':'',
                        'descripcion':'',
                        'codigo':'',
                        'cliente':'',
                        'descripcion':'',
                        'url':'',
                        'id_idioma' : '',
                        'descripcion_idioma':'',
                        'id_servicios':'',
                        'titulo_servicio': '',
                        'url' : '',
                        'fecha' : '',
                        'id_imagen': '',
        }
        ////////////////////////////////////////////////////////////////////////////////////////
        $scope.iniciar_datatable = function(){
            //--Iniciar DataTable
            $('#myTable').DataTable({
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                    "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
                "order": [[ 0, "desc" ]]
            });
        }
        ////////////////////////////////////////////////////////////////////////////////////////
        $scope.consultar_portafolio = function(){
            portafolioFactory.asignar_valores("","",$scope.base_url)
            portafolioFactory.cargar_portafolio(function(data){
                $scope.portafolio=data;
                //console.log($scope.portafolio);
            });
        }
        ////////////////////////////////////////////////////////////////////////////////////////
        $scope.ver_portafolio = function(valor){
            var id = $("#ver"+valor).attr("data");
            $scope.id_portafolio = id;
            $("#id_portafolio").val($scope.id_portafolio)
            let form = document.getElementById('formConsultaPortafolio');
            form.action = "./portafolioVer";
            form.submit();
        }
        ////////////////////////////////////////////////////////////////////////////////////////
        $scope.activar_registro = function(event){
            $scope.id_seleccionado_portafolio = []
            $scope.estatus_seleccionado_portafolio = []
            var caja = event.currentTarget.id;
            var atributos = $("#"+caja).attr("data");
            var arreglo_atributos = atributos.split("|");
            $scope.id_seleccionado_portafolio = arreglo_atributos[0];
            $scope.estatus_seleccionado_portafolio = arreglo_atributos[1];
            $("#cabecera_mensaje").text("Información:");
            //--
            if ($scope.estatus_seleccionado_portafolio==0){
                mensaje = "Desea modificar el estatus de este registro a publicado? ";
                $scope.estatus_seleccionado_portafolio=1;
            }else{
                mensaje = "Desea modificar el estatus de este registro a inactivo? ";
                $scope.estatus_seleccionado_portafolio=0
            }
            //($scope.estatus_seleccionado_categorias==1) ? $scope.estatus_seleccionado_categorias=0:$scope.estatus_seleccionado_categorias=1;

            //--
            $scope.modificar_estatus("activar_inactivar",mensaje)
        }
        //----------------------------------------------------------------
        $scope.modificar_estatus = function(opcion,mensaje){
             swal({
                  title: 'Esta seguro?',
                  text: mensaje,
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Si!',
                  cancelButtonText: 'No',
                }).then((result) => {
                      if (result.value) {
                        $scope.accion_estatus()
                      }
                })
        }
        //----------------------------------------------------------------
        $scope.accion_estatus = function(){
            //alert($scope.id_seleccionado_portafolio)
            $http.post($scope.base_url+"/Portafolio/modificarPortafolioEstatus",
            {
                 'id':$scope.id_seleccionado_portafolio,
                 'estatus':$scope.estatus_seleccionado_portafolio,

            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "modificacion_procesada"){
                    Swal(
                          'Realizado!',
                          'El proceso fue ejecutado.',
                          'success'
                    ).then((result) => {
                          if (result.value) {
                                let form = document.getElementById('formConsultaPortafolio');
                                form.action = "./consultar_portafolio";
                                form.submit();
                          }

                    });
                }else if($scope.mensajes.mensaje == "existe_carrito"){
                    Swal(
                          'No realizado!',
                          'El proceso no pudo ser ejecutado ya que el pŕoducto se encuentra asociado a una compra ',
                          'warning'
                    )
                }else{
                    Swal(
                          'No realizado!',
                          'El proceso no pudo ser ejecutado.',
                          'warning'
                    )
                }
            }).error(function(data,estatus){
                console.log(data);
            });
        }
        ////////////////////////////////////////////////////////////////////////////////////////
        $scope.eliminar_registro = function(event){
            $scope.id_seleccionado_portafolio = []
            $scope.estatus_seleccionado_portafolio = []
            var caja = event.currentTarget.id;
            //alert(caja);
            var atributos = $("#"+caja).attr("data");
            var arreglo_atributos = atributos.split("|");
            $scope.id_seleccionado_portafolio = arreglo_atributos[0];
            $scope.estatus_seleccionado_portafolio = arreglo_atributos[1];
            //console.log($scope.estatus_seleccionado_marca);
            $("#cabecera_mensaje").text("Información:");
            if ($scope.estatus_seleccionado_portafolio==2){
                mensaje = "Desea eliminar este registro? ";
            }
            $scope.modificar_estatus("eliminar",mensaje)
        }
        ////////////////////////////////////////////////////////////////////////////////////////
        setTimeout(function(){
                $scope.iniciar_datatable();
        },500);
        ////////////////////////////////////////////////////////////////////////////////////////
        $scope.consultar_portafolio();
    })
    .controller("PortafolioController", function($scope,$http,$location,serverDataMensajes,sesionFactory,idiomaFactory,serviciosFactory,galeriaFactory,portafolioFactory){
        $(".li-menu").removeClass("active");
        $("#li_productos").addClass("active");
        $(".a-menu").removeClass("active");
        $("#detalle_prod").addClass("active");

        $scope.titulo_pagina = "Portafolio";
        $scope.subtitulo_pagina = "Registro";
        $scope.titulo_registrar = "Registrar";
        $scope.titulo_cons = "Consultar";
        $scope.base_url = $("#base_url").val();
        $scope.titulo_text = "Pulse aquí para ingresas el contenido"

        $scope.currentTab = 'datos_basicos'
        $scope.activo_img = "inactivo";
        $scope.inhabilitarImg = false

        $scope.portafolio = {
                        'id': '',
                        'idioma': '',
                        'id_idioma' : '',
                        'descripcion':'',
                        'servicios': {
                            "id":"",
                            "titulo":"",
                            "descripcion":""
                        },
                        'id_servicios':'',
                        'fecha': '',
                        'url' : '',
                        'codigo' : '',
                        'id_imagen': '',
                        'cliente':''
        }

        $scope.borrar_imagen = []
        $scope.galeria_portafolio = []
        $scope.imagenes_portafolio = []
        $scope.activo_img_soportes = "inactivo"
        $scope.path_imagen_seleccionada = []
        $scope.galery = []
        SuperFecha("fechaPortafolio")
        ///////////////////////////////////////////////////////////////
        $scope.consultar_idioma = function(){
            idiomaFactory.asignar_valores("",$scope.base_url)
            idiomaFactory.cargar_idioma(function(data){
                $scope.idioma=data;
                $.each( $scope.idioma, function( indice, elemento ){
                      agregarOptions("#idioma", elemento.id, elemento.descripcion)
                });
                $('#idioma > option[value="0"]').prop('selected', true);

            });
        }
        $scope.capturar_idioma = function(){
            //$scope.portafolio.id_idioma = $('#idioma').val();

        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        $scope.consultar_servicios = function(){

            eliminarOptions("servicios")

            $scope.portafolio.id_idioma = $('#idioma').val();
            serviciosFactory.asignar_valores("","",$scope.portafolio.id_idioma,$scope.base_url);//ya que es galeria de imagenes
            serviciosFactory.cargar_servicios(function(data){
                $scope.servicios=data;
                //console.log($scope.servicios);
                
                $.each($scope.servicios, function( indice, elemento ){
                      agregarOptions("#servicios", elemento.id, elemento.titulo)
                });
                $('#servicios > option[value=""]').prop('selected', true);
            });
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        $scope.agregarWisi = function(){
            $scope.portafolio.descripcion = $(".textarea_editor").val()
            $("#div_descripcion").html($scope.portafolio.descripcion)
            $("#cerrarModal").click();
        }
        $scope.wisi_modal = function(){
            $("#wisiModal").modal("show")
            $(".textarea_editor").data("wysihtml5").editor.setValue($scope.portafolio.descripcion)
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        $scope.consultar_galeria_img = function(){
            galeriaFactory.asignar_valores('8','',$scope.base_url);//id 8 es el perteneciente a portafolio
            galeriaFactory.cargar_galeria_fa(function(data){
                $scope.galeria_m=data;
                //console.log($scope.galeria_m);
            });
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        $scope.seleccione_img_portafolio = function(){
            if(($scope.clonar!=1)&&($scope.inhabilitarImg==false)){
                //-----------------------------------
                $("#modal_img2").modal("show");
                var arreglo_portafolio = $scope.galeria_portafolio;
                //console.log( $scope.galeria_portafolio)
                var galeria_soporte = $scope.galeria_m
                var galeryx = new Array();
                for (i=0;i<galeria_soporte.length;i++){
                    galeryx[i] = galeria_soporte[i]["ruta"];
                }
                //console.log(arreglo_portafolio)
                //Marco c/u
                for(j=0;j<arreglo_portafolio.length;j++){
                    posicion = galeryx.indexOf(arreglo_portafolio[j]);
                    //alert(arreglo_portafolio[j]);
                    //alert(posicion);
                    $scope.seleccionar_imagen_soportes_individual("img_soporte"+posicion);
                }
                //-----------------------------------
            }
        }

        $scope.seleccionar_imagen_soportes_individual = function(imagen){
            /*
            *    INicializa tags de menu
            */
            $(".li-menu").removeClass("active");
            $("#li_productos").addClass("active");
            $(".a-menu").removeClass("active");
            $("#det_prod").addClass("active");
            /*
            *
            */
            uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

            var vec = $("#"+imagen).attr("data");
            var vector_data = vec.split("|")
            var id_imagen = vector_data[0];
            var ruta = vector_data[1];
            //--
            $("#"+imagen).addClass("marcado");
            if($scope.imagenes_portafolio.indexOf(id_imagen)==-1){
                $scope.imagenes_portafolio.push(id_imagen);//manejo los id
            }
            if($scope.galeria_portafolio.indexOf(ruta)==-1){
                $scope.galeria_portafolio.push(ruta); //maneja las rutas
            }
            $scope.activo_img_soportes = "activo"
            desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

        }

        $scope.limpiar_arreglos = function(){
            $scope.imagenes_portafolio = []
            $scope.galeria_portafolio = []
            $(".imgbiblioteca").removeClass("marcado");
            //console.log($scope.galeria_portafolio)
            //console.log($scope.imagenes_portafolio)
        }

        $scope.seleccionar_imagen_soportes = function(event){
            var imagen = event.target.id;//Para capturar id
            var vec = $("#"+imagen).attr("data");
            var vector_data = vec.split("|")
            var id_imagen = vector_data[0];
            var ruta = vector_data[1];
            //console.log(ruta);
            if(($("#"+imagen).hasClass("marcado"))==true){
                $("#"+imagen).removeClass("marcado");
                $indice = $scope.imagenes_portafolio.indexOf(id_imagen);
                $scope.imagenes_portafolio.splice($indice,1);
                $indice_ruta = $scope.galeria_portafolio.indexOf(ruta);
                $scope.galeria_portafolio.splice($indice_ruta,1);
                if($scope.galeria_portafolio.length==0){
                    $scope.activo_img_soportes = "inactivo"
                }
            }else{
                $("#"+imagen).addClass("marcado");
                $scope.imagenes_portafolio.push(id_imagen);//manejo los id
                $scope.galeria_portafolio.push(ruta); //maneja las rutas
                $scope.activo_img_soportes = "activo"
            }
            //console.log($scope.galeria_portafolio)
            //console.log($scope.imagenes_portafolio)
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        $scope.registrarPortafolio = function(){    //Para guardar y modificar

            uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

            //servicios:
            if(($scope.portafolio.id!="")&&($scope.portafolio.id!=undefined)){
                $scope.portafolio_servicios_id = $("#servicios").val();
            }else{
                $scope.portafolio_servicios_id = $scope.portafolio.servicios;
            }
            //--    
            if($scope.validar_form()==true){
                if(($scope.portafolio.id!="")&&($scope.portafolio.id!=undefined)){
                    $scope.modificar_portafolio();
                }else{
                    $scope.insertar_portafolio();
                }
            }

            //desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        $scope.validar_form = function(){
            if(($scope.portafolio.id_idioma=="")||($scope.portafolio.id_idioma=="")){
                mostrar_notificacion("Campos no validos","Debe seleccionar el idioma","warning");
                return false;
            }else if(($scope.portafolio_servicios_id=="")||($scope.portafolio_servicios_id==undefined)){
                mostrar_notificacion("Campos no validos","Debe seleccionar un tipo de servicio","warning");
                return false;
            }else if(($scope.portafolio.fecha=="")||($scope.portafolio.fecha=="dd/mm/yyyy")){
                mostrar_notificacion("Campos no validos","Debe ingresar la fecha del producto","warning");
                return false;
            }else if($scope.portafolio.codigo==""){
                mostrar_notificacion("Campos no validos","Debe ingresar el código","warning");
                return false;
            }else if(($scope.portafolio.titulo=="")||($scope.portafolio.titulo==undefined)){
                mostrar_notificacion("Campos no validos","Debe ingresar el título","warning");
                return false;
            }else if($scope.portafolio.url==""){
                mostrar_notificacion("Campos no validos","Debe ingresar la url","warning");
                return false;
            }else if(($scope.portafolio.cliente=="")||($scope.portafolio.cliente==undefined)){
                mostrar_notificacion("Campos no validos","Debe ingresar el nombre del cliente","warning");
                return false;
            }else if(($scope.portafolio.descripcion=="NULL")||($scope.portafolio.descripcion=="")){
                mostrar_notificacion("Campos no validos","Debe ingresar una descripción","warning");
                return false;
            }else if($scope.imagenes_portafolio.length==0){
                mostrar_notificacion("Campos no validos","Debe ingresar al menos una imagen","warning");
                return false;
            }else{
                return true;
            }
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        $scope.limpiar_cajas_portafolio = function(){
            
                //--Limpio solo algunos campos
                $scope.portafolio = {
                    'id': '',
                    'idioma': '',
                    'id_idioma' : '',
                    'descripcion':'',
                    'servicios': {
                        "id":"",
                        "titulo":"",
                        "descripcion":""
                    },
                    'fecha': '',
                    'url' : '',
                    'codigo' : '',
                    'id_imagen': '',
                    'cliente':'',
                }

                //console.log($scope.portafolio)
                $scope.limpiar_arreglos();
                $scope.activo_img_soportes = "inactivo"
            
            
            $("#div_descripcion").html($scope.titulo_text);
            $('#textarea_editor').data("wysihtml5").editor.clear();
            $("#idioma > option[value='']").removeAttr('selected', 'selected');
            $("#idioma > option[value='1']").attr('selected', 'selected');
            $scope.portafolio.id_idioma.id = 1
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        $scope.insertar_portafolio = function(){
            $http.post($scope.base_url+"/Portafolio/registrarportafolio",
            {
                'id'                  : $scope.portafolio.id,
                'id_idioma'          : $scope.portafolio.id_idioma,
                'titulo'              : $scope.portafolio.titulo,
                'servicio'             : $scope.portafolio.servicios,
                'fecha'             : $scope.portafolio.fecha,
                'url'                 : $scope.portafolio.url,
                'codigo'             : $scope.portafolio.codigo,
                'descripcion'        : $scope.portafolio.descripcion,
                'id_imagen'         : $scope.imagenes_portafolio,
                'cliente'            : $scope.portafolio.cliente,
            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "registro_procesado"){
                    mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
                    if($scope.clonar==0)
                        $scope.limpiar_cajas_portafolio();
                    else
                        $("#btn-registrar,#btn-limpiar").prop("disabled",true)
                }else if($scope.mensajes.mensaje == "titulo"){
                    mostrar_notificacion("Mensaje","Ya existe una producto con ese título","warning");
                }else if($scope.mensajes.mensaje == "codigo"){
                    mostrar_notificacion("Mensaje","Ya existe una producto con ese código","warning");
                }else if($scope.mensajes.mensaje == "ambos"){
                    mostrar_notificacion("Mensaje","Ya existe una producto con ese título y código","warning");
                }
                else{
                    mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
                }
            }).error(function(data,estatus){
                console.log(data);
            });
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        $scope.consultaPortafolioIndividual = function(){
            uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
            portafolioFactory.asignar_valores("",$scope.id_portafolio,$scope.base_url)
            portafolioFactory.cargar_portafolio(function(data){
                
                $scope.portafolio=data[0];
                //console.log($scope.portafolio)
                $("#div_descripcion").html($scope.portafolio.descripcion)

                $scope.activo_img_soportes = "activo"

                arreglo_marca = $scope.portafolio.imagen[0]['ruta']
                //-Cambio 10022020
                //------------------------------------------------------                    
                if($scope.portafolio.imagen[0]['ruta']!=""){
                    $scope.galeria_portafolio  = arreglo_marca.split("|")
                }else{
                    $scope.galeria_portafolio=[];
                }
                //------------------------------------------------------    
                arreglo_id = $scope.portafolio.imagen[0]['id_imagen']
                //-------------------------------------------------------
                //-Cambio 10022020
                if((arreglo_id!="")&&(arreglo_id!="0")){
                    $scope.imagenes_portafolio  = arreglo_id.split("|")
                    $scope.galeria_portafolio.length>0 ? $scope.activo_img_soportes = "activo" : $scope.activo_img_soportes = "inactivo"
                }else{
                    $scope.imagenes_portafolio = []
                    $scope.activo_img_soportes = "inactivo"
                }
                //-----------------------------------------------------
                console.log($scope.imagenes_portafolio);
                /*console.log(arreglo_marca);*/
                $("#codigo").prop('disabled', true);
                $("#idioma").prop('disabled', true);
                $scope.titulo_registrar = "Modificar";
                $scope.subtitulo_pagina  = "Modificar portafolio";
                //------------------------------------------------------------------
                    //--Select de idioma
                    if($scope.portafolio.id_idioma){
                        setTimeout(function(){
                            $('#idioma').val($scope.portafolio.id_idioma);
                            $('#idioma > option[value="'+$scope.portafolio.id_idioma+'"]').attr('selected', 'selected');
                        },2000);
                    }
                    //--Select de categoria----
                    if($scope.portafolio.id_servicios){
                        eliminarOptions("servicios")

                        serviciosFactory.asignar_valores("","",$scope.portafolio.id_idioma,$scope.base_url);//ya que es galeria de imagenes
                        serviciosFactory.cargar_servicios(function(data){
                            $scope.servicios=data;
                            //console.log($scope.servicios);

                            $.each($scope.servicios, function( indice, elemento ){
                                  agregarOptions("#servicios", elemento.id, elemento.titulo)
                            });
                        })    
                        setTimeout(function(){
                            $('#servicios').val($scope.portafolio.id_servicios);
                            $('#servicios > option[value="'+$scope.portafolio.id_servicios+'"]').attr('selected', 'selected');
                            desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

                        },2000);
                    }
                    //-------------------------
            });
        }
        /*
        *    DeshabilitarCampos
        */
        $scope.deshabilitarCampos = function(){
            $("#servicios").prop('disabled', true);
            $("#fecha").prop('disabled', true);
            $("#url").prop('disabled', true);
            $("#codigo").prop('disabled', true);
            $("#img_galeria").prop('disabled',true);
        }
        /*
        *    Habilitar campos
        */
        $scope.habilitarCampos = function(){
            $("#servicios").prop('disabled', true);
            $("#fecha").prop('disabled', true);
            $("#url").prop('disabled', true);
            $("#codigo").prop('disabled', true);
            $("#img_galeria").prop('disabled',true);
        }
        
        /////////////////////////////////////////////////////////////////////valida//////////////////////////////////

        $scope.modificar_portafolio = function(){
            $http.post($scope.base_url+"/Portafolio/modificar_portafolio",
            {
                'id'                  : $scope.portafolio.id,
                'id_idioma'          : $scope.portafolio.id_idioma,
                'titulo'              : $scope.portafolio.titulo,
                'servicio'             : $scope.portafolio_servicios_id,
                'fecha'             : $scope.portafolio.fecha,
                'url'                 : $scope.portafolio.url,
                'codigo'             : $scope.portafolio.codigo,
                'descripcion'        : $scope.portafolio.descripcion,
                'id_imagen'         : $scope.imagenes_portafolio,
                'cliente'            : $scope.portafolio.cliente
            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "modificacion_procesada"){
                    mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
                    //$scope.limpiar_cajas_portafolio();
                }else if($scope.mensajes.mensaje == "existe"){
                    mostrar_notificacion("Mensaje","Existe un producto con ese código ","warning");
                }else{
                    mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
                }
            }).error(function(data,estatus){
                console.log(data);
            });
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        $scope.quitarDisabled = function(){
            $("#btn-registrar").prop("disabled",false)
            $("#btn-limpiar").prop("disabled",false)
            $("#servicios").prop('disabled', true);
            $("#fecha").prop('disabled', true);
            $("#url").prop('disabled', true);
            $("#codigo").prop('disabled', true);
            $("#img_galeria").prop('disabled',true);
            $("#img_galeria").prop('disabled',false);
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        $scope.consultar_idioma();
        $scope.consultar_galeria_img();
        /* $scope.consultar_talla(); */
        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        $('#servicios > option[value=""]').attr('selected', 'selected');
        //---
        $scope.id_portafolio  = $("#id_portafolio").val();
        if($scope.id_portafolio){
            $scope.consultaPortafolioIndividual();
            setTimeout(function(){
                desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
            },2000)
        }

        //---------------------------------------------------------------
        /*if(($scope.portafolio.id=="")||($scope.portafolio.id==undefined)){
            $scope.quitarDisabled();
        }*/
        //---------------------------------------------------------------    
    })
    .controller("productosConsultaController", function($scope,$http,$location,serverDataMensajes,categoriasFactory,sesionFactory,idiomaFactory,productosFactory){
        $(".li-menu").removeClass("active");
        $("#li_productos1").addClass("active");    
        $(".a-menu").removeClass("active");
        $("#producto").addClass("active");    
        $scope.titulo_pagina = "Consulta de Productos";
        $scope.activo_img = "inactivo";
        $scope.productos = {
                            'id': '',
                            'idioma': '',
                            'id_idioma' : '',
                            'somos': '',
                            'digital_agency': '',
                            'estatus' : '',
                            'id_imagen' : '',
                            'imagen' : ''
        }
        $scope.nosotros_menu = "1";
        $scope.id_nosotros = "";
        $scope.base_url = $("#base_url").val();

        $scope.iniciar_datatable = function(){
            //--Iniciar DataTable
            $('#myTable').DataTable({
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                    "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
                "order": [[0, "asc" ],[2, "asc" ]]


            });
            //console.log($scope.nosotros);
        }

        $scope.consultar_productos
         = function(){
            productosFactory.asignar_valores("","",$scope.base_url)
            productosFactory.cargar_productos(function(data){
                $scope.productos = data;
                //console.log($scope.productos);
            });
        }

        $scope.ver_producto = function(valor){
            var id = $("#ver"+valor).attr("data");
            $scope.id_productos = id;
            //console.log($scope.id_productos);
            $("#id_productos").val($scope.id_productos);
            let form = document.getElementById('formConsultaProductos');
            form.action = "./productosVer";
            form.submit();
        }

        $scope.activar_registro = function(event){
                    $scope.id_seleccionado_productos = []
                    $scope.estatus_seleccionado_productos = []

                    var caja = event.currentTarget.id;
                    var atributos = $("#"+caja).attr("data");
                    var arreglo_atributos = atributos.split("|");

                    $scope.id_seleccionado_productos = arreglo_atributos[0];
                    $scope.estatus_seleccionado_productos = arreglo_atributos[1];

                    $("#cabecera_mensaje").text("Información:");

                    if ($scope.estatus_seleccionado_productos==0){
                        mensaje = "Desea modificar el estatus de este registro a publicado? ";
                        $scope.estatus_seleccionado_productos=1;
                    }else{
                        mensaje = "Desea modificar el estatus de este registro a inactivo? ";
                        $scope.estatus_seleccionado_productos=0
                    }
                    $scope.modificar_estatus("activar_inactivar",mensaje)
                }
                //----------------------------------------------------------------
                $scope.modificar_estatus = function(opcion,mensaje){
                     swal({
                          title: 'Esta seguro?',
                          text: mensaje,
                          type: 'warning',
                          showCancelButton: true,
                          confirmButtonColor: '#3085d6',
                          cancelButtonColor: '#d33',
                          confirmButtonText: 'Si!',
                          cancelButtonText: 'No',
                        }).then((result) => {
                              if (result.value) {
                                  $scope.accion_estatus()
                              }
                        })
                }
                //----------------------------------------------------------------
                $scope.accion_estatus = function(){

                    $http.post($scope.base_url+"/Productos/modificarProductosEstatus",
                    {
                         'id':$scope.id_seleccionado_productos,
                         'estatus':$scope.estatus_seleccionado_productos,

                    }).success(function(data, estatus, headers, config){
                        $scope.mensajes  = data;
                        
                        if($scope.mensajes.mensaje == "modificacion_procesada"){
                            Swal(
                                  'Realizado!',
                                  'El proceso fue ejecutado.',
                                  'success'
                            ).then((result) => {
                                  if (result.value) {
                                        let form = document.getElementById('formConsultaProductos');
                                        form.action = "./consultar_productos";
                                        form.submit();
                                  }

                            });
                        }else{
                            Swal(
                                  'No realizado!',
                                  'El proceso no pudo ser ejecutado.',
                                  'warning'
                            )
                        }
                    }).error(function(data,estatus){
                        console.log(data);
                    });
                }
                //----------------------------------------------------------------
                //----------------------------------------------------------------
        $scope.eliminar_registro = function(event){
            $scope.id_seleccionado_productos = []
            $scope.estatus_seleccionado_productos = []
            var caja = event.currentTarget.id;
            //alert(caja);
            var atributos = $("#"+caja).attr("data");
            var arreglo_atributos = atributos.split("|");
            $scope.id_seleccionado_productos = arreglo_atributos[0];
            $scope.estatus_seleccionado_productos = arreglo_atributos[1];
            //console.log($scope.estatus_seleccionado_productos);
            $("#cabecera_mensaje").text("Información:");
            if ($scope.estatus_seleccionado_productos==2){
                mensaje = "Desea eliminar este registro? ";
            }
            $scope.modificar_estatus("eliminar",mensaje)
        }
        //----------------------------------------------------------------

        setTimeout(function(){
                        $scope.iniciar_datatable();
                },500);

        $scope.consultar_productos
        ();
    })
    .controller("productosController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,ordenFactory,idiomaFactory,productosFactory){
        //Cuerpo declaraciones
        $(".li-menu").removeClass("active");
        $("#li_productos1").addClass("active");    
        $(".a-menu").removeClass("active");
        $("#producto").addClass("active");    
        $scope.titulo_pagina = "Productos";
        $scope.subtitulo_pagina  = "Registrar Productos";
        $scope.activo_img = "inactivo";
        $scope.productos = {
                                'id':'',
                                'id_idioma':'',
                                'titulo':'',
                                'descripcion':'',
                                'estatus':'',
                                'id_imagen':'',
                                'imagen':''
        }
        $scope.id_productos = ""
        $scope.searchProductos = []
        $scope.borrar_imagen = []
        $scope.currentTab = 'datos_basicos'
        $scope.titulo_registrar = "Registrar";
        $scope.base_url = $("#base_url").val();
        $scope.titulo_text = "Pulse aquí para ingresar el contenido del producto"


        //--------------------------------------------------------
        //Cuerpo de metodos
        $scope.consultar_idioma = function(){
            idiomaFactory.asignar_valores("",$scope.base_url)
            idiomaFactory.cargar_idioma(function(data){
                $scope.idioma=data;
                $.each( $scope.idioma, function( indice, elemento ){
                      agregarOptions("#idioma", elemento.id, elemento.descripcion)
                });
                $('#idioma > option[value="0"]').prop('selected', true);

                //console.log($scope.idioma);
            });
        }
        $scope.agregarWisi = function(){
            //$('#text_editor').data("wysihtml5").editor.clear();
            $scope.productos.descripcion = $(".textarea_editor").val()
            $("#div_descripcion").html($scope.productos.descripcion)
            $("#cerrarModal").click();
        }
        $scope.wisi_modal = function(){
            $("#wisiModal").modal("show")
            $(".textarea_editor").data("wysihtml5").editor.setValue($scope.productos.descripcion)
        }
        //--
        $scope.consultar_galeria_img = function(){
            galeriaFactory.asignar_valores('26','',$scope.base_url);//ya que es galeria de imagenes
            galeriaFactory.cargar_galeria_fa(function(data){
                $scope.galery=data;
                //console.log($scope.galery);
            });
        }

        $scope.seleccione_img_principal = function(){
            $("#modal_img1").modal("show");
        }

        $scope.seleccionar_imagen = function(event){
            var imagen = event.target.id;//Para capturar id
            var vec = $("#"+imagen).attr("data");
            var vector_data = vec.split("|")
            var id_imagen = vector_data[0];
            var ruta = vector_data[1];
            //console.log(vector_data[1]);

            $scope.borrar_imagen.push(id_imagen);
            $scope.activo_img = "activo"
            $scope.productos.id_imagen = id_imagen
            $scope.productos.imagen = ruta
            //--
            $("#modal_img1").modal("hide");
            //--
        }

        $scope.registrarproductos = function(){
            
            uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
            $scope.productos.orden = $("#orden").val();

            if($scope.validar_form()==true){
                //Para guardar
                if(($scope.productos.id!="")&&($scope.productos.id!=undefined)){
                    $scope.modificar_productos();
                }else{
                    $scope.insertar_productos();
                }

            }

            //desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

        }

        $scope.insertar_productos = function(){
            $http.post($scope.base_url+"/Productos/registrarProductos",
            {
                'id'         : $scope.productos.id,
                'titulo'     : $scope.productos.titulo,
                'id_imagen'  : $scope.productos.id_imagen,
                'descripcion': $scope.productos.descripcion,
                'id_idioma'  : $scope.productos.id_idioma.id,
                'id_idioma'  : $scope.productos.id_idioma.id,
                'orden'         : $scope.productos.orden,
            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "registro_procesado"){
                    mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
                    $scope.limpiar_cajas_productos();
                }else if($scope.mensajes.mensaje == "existe"){
                    mostrar_notificacion("Mensaje","Ya existe un producto con ese titulo","warning");
                }
                else{
                    mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
                }
            }).error(function(data,estatus){
                console.log(data);
            });
        }

        $scope.modificar_productos = function(){
            $http.post($scope.base_url+"/Productos/modificarProductos",
            {
                'id':$scope.productos.id,
                'titulo': $scope.productos.titulo,
                'id_imagen':$scope.productos.id_imagen,
                'descripcion':$scope.productos.descripcion,
                'id_idioma':$scope.productos.id_idioma,
                'orden':$scope.productos.orden,
                'inicial':$scope.productos.inicial,

            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "registro_procesado"){
                    mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
                    $scope.productos.inicial = $scope.productos.orden
                }else if($scope.mensajes.mensaje == "existe"){
                    mostrar_notificacion("Mensaje","Ya existe una noticia con ese titulo","warning");
                }
                else{
                    mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
                }
            }).error(function(data,estatus){
                console.log(data);
            });
        }

        $scope.validar_form = function(){
            //console.log($scope.productos)
            if(($scope.productos.id_idioma=="NULL")||($scope.productos.id_idioma=="")){
                mostrar_notificacion("Campos no validos","Debe seleccionar el idioma","warning");
                return false;
            } else if($scope.productos.titulo==""){
                mostrar_notificacion("Campos no validos","Debe ingresar el titulo","warning");
                return false;
            }else if(($scope.productos.orden=="NULL")||($scope.productos.orden=="")){
                mostrar_notificacion("Campos no validos","Debe seleccionar el orden del servicio","warning");
                return false;
            }else if(($scope.productos.id_imagen=="NULL")||($scope.productos.id_imagen=="")){
                mostrar_notificacion("Campos no validos","Debe seleccionar la imagen","warning");
                return false;
            }
            else{
                return true;
            }
        }

        $scope.limpiar_cajas_productos = function(){
            $scope.productos = {
                                'id':'',
                                'id_idioma':'',
                                'titulo':'',
                                'descripcion':'',
                                'estatus':'',
                                'id_imagen':''
            }
            $scope.activo_img = "inactivo";

            $('#text_editor').data("wysihtml5").editor.clear();
            $("#div_descripcion").html($scope.titulo_text);
            eliminarOptions("orden")
            $('#orden > option[value=""]').prop('selected', true);
        }
        //--
        $scope.consultarProductoIndividual = function(){

            uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

            productosFactory.asignar_valores("",$scope.id_productos,$scope.base_url)
            productosFactory.cargar_productos(function(data){
                $scope.productos=data[0];
                //console.log(data[0]);
                $("#div_descripcion").html($scope.productos.descripcion)
                $scope.borrar_imagen.push($scope.productos.id_imagen);
                $scope.activo_img = "activo"
                ordenFactory.asignar_valores($scope.productos.id_idioma,$scope.base_url,"2")
                ordenFactory.cargar_orden_productos(function(data){
                    $scope.ordenes=data;
                    //console.log($scope.ordenes)
                    //-Elimino el select
                    eliminarOptions("orden")
                    $.each( $scope.ordenes, function( indice, elemento ){
                          agregarOptions("#orden", elemento.orden, elemento.orden)
                    });
                });
                //-Cambio 10022020
                
                $scope.productos.imagen = $scope.productos.ruta
                $scope.titulo_registrar = "Modificar";
                $scope.subtitulo_pagina  = "Modificar producto";
                setTimeout(function(){
                    $('#idioma > option[value="'+$scope.productos.id_idioma+'"]').prop('selected', true);
                    $('#orden > option[value="'+$scope.productos.orden+'"]').prop('selected', true);
                    $scope.productos.inicial = $scope.productos.orden;
                },300);
                $("#idioma").prop('disabled', true);
            });

            desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

        }//------------------------------------------------------------------------------
        $scope.cargarOrden = function(){
            ordenFactory.asignar_valores($scope.productos.id_idioma.id,$scope.base_url,"1")
            ordenFactory.cargar_orden_productos(function(data){
                $scope.ordenes=data;
                //-Elimino el select
                eliminarOptions("orden")
                $.each( $scope.ordenes, function( indice, elemento ){
                      agregarOptions("#orden", elemento.orden, elemento.orden)
                });
                $('#orden > option[value=""]').prop('selected', true);

            });
        }
        //------------------------------------------------------------------------------    
        //---
        //--------------------------------------------------------
        //Cuerpo de llamados
        $scope.consultar_idioma();
        $scope.consultar_galeria_img();
        //--Si tiene id categoria .... consulto los datos relacionados a ese id, esto es cuando proviene de una consulta
        $scope.id_productos  = $("#id_productos").val();
        //console.log($scope.id_productos);

        if($scope.id_productos){
            $scope.consultarProductoIndividual();
        }
        //--------------------------------------------------------

    })
    .controller("quienesSomosConsultaController", function($scope,$http,$location,serverDataMensajes,categoriasProdFactory,sesionFactory,idiomaFactory,nosotrosFactory){
        $(".li-menu").removeClass("active");
        $(".a-menu").removeClass("active");
        $("#nosotros").addClass("active");  
        $("#li_empresa").addClass("active");    
        $scope.titulo_pagina = "Consulta de quienes somos";
        $scope.id_categoria_prod = "";
        $scope.base_url = $("#base_url").val();
        $scope.categoria_prod  = {
                                    'id' : '',
                                    'titulo' : '',
                                    'descripcion' : '',
                                    'id_idioma' : '',
                                    'idioma' : '',
                                    'icono':'',
                                    'estatus':''
        }
        //////////////////////////////////////////////////////////////////////////////
        $scope.iniciar_datatable = function(){
            //--Iniciar DataTable
            $('#myTable').DataTable({
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                    "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
                "order": [[ 2, "asc" ]]
            });
            //console.log($scope.categoria_prod);
        }
        //////////////////////////////////////////////////////////////////////////////
        $scope.consultar_nosotros = function(){
            nosotrosFactory.asignar_valores("","",$scope.base_url)
            nosotrosFactory.cargar_nosotros(function(data){
                $scope.nosotros=data;
                //console.log($scope.categoria_prod);
            });
        }
        //////////////////////////////////////////////////////////////////////////////
        $scope.ver_nosotros = function(valor){
            var id = $("#ver"+valor).attr("data");
            $scope.id_nosotros = id;
            $("#id_nosotros").val($scope.id_nosotros)
            let form = document.getElementById('formConsultaNosotros');
            form.action = "./quienes_somos_ver";
            form.submit();
        }
        //////////////////////////////////////////////////////////////////////////////
        $scope.eliminar_registro = function(event){
            $scope.id_seleccionado_catprod = []
            $scope.estatus_seleccionado_catprod = []
            var caja = event.currentTarget.id;
            var atributos = $("#"+caja).attr("data")
            var arreglo_atributos = atributos.split("|");
            $scope.id_seleccionado_catprod = arreglo_atributos[0];
            $scope.estatus_seleccionado_catprod = arreglo_atributos[1];
            $("#cabecera_mensaje").text("Información:");
            if ($scope.estatus_seleccionado_catprod    ==2){
                mensaje = "Desea eliminar este registro? ";
            }
            $scope.modificar_estatus("eliminar",mensaje)
        }
        //////////////////////////////////////////////////////////////////////////////
        $scope.modificar_estatus = function(opcion,mensaje){
             swal({
                  title: 'Esta seguro?',
                  text: mensaje,
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Si!',
                  cancelButtonText: 'No',
                }).then((result) => {
                      if (result.value) {
                          $scope.accion_estatus()
                      }
                })
        }
        //////////////////////////////////////////////////////////////////////////////
        $scope.accion_estatus = function(){
            $http.post($scope.base_url+"QuienesSomos/modificarNosotrosEstatus",
            {
                'id' : $scope.id_seleccionado_catprod,
                'estatus' : $scope.estatus_seleccionado_catprod,
            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "modificacion_procesada"){
                    Swal(
                          'Realizado!',
                          'El proceso fue ejecutado.',
                          'success'
                    ).then((result) => {
                          if (result.value) {
                                let form = document.getElementById('formConsultaNosotros');
                                form.action = "./consultar_quienes_somos";
                                form.submit();
                          }

                    });
                }else{
                    Swal(
                          'No realizado!',
                          'El proceso no pudo ser ejecutado.',
                          'warning'
                    )
                }
            }).error(function(data,estatus){
                console.log(data);
            });
        }
        //////////////////////////////////////////////////////////////////////////////
        $scope.activar_registro = function(event){
            $scope.id_seleccionado_catprod = []
            $scope.estatus_seleccionado_catprod = []
            var caja = event.currentTarget.id;
            var atributos = $("#"+caja).attr("data");
            var arreglo_atributos = atributos.split("|");
            $scope.id_seleccionado_catprod = arreglo_atributos[0];
            $scope.estatus_seleccionado_catprod = arreglo_atributos[1];
            $("#cabecera_mensaje").text("Información:");
            //--
            if ($scope.estatus_seleccionado_catprod==0){
                mensaje = "Desea modificar el estatus de este registro a publicado? ";
                $scope.estatus_seleccionado_catprod=1;
            }else{
                mensaje = "Desea modificar el estatus de este registro a inactivo? ";
                $scope.estatus_seleccionado_catprod=0
            }
            //($scope.estatus_seleccionado_categorias==1) ? $scope.estatus_seleccionado_categorias=0:$scope.estatus_seleccionado_categorias=1;

            //--
            $scope.modificar_estatus("activar_inactivar",mensaje)
        }
        //////////////////////////////////////////////////////////////////////////////
        setTimeout(function(){
                $scope.iniciar_datatable();
        },500);
        $scope.consultar_nosotros();
    })
    .controller("quienesSomosController" , function($scope,$http,$location,serverDataMensajes,nosotrosFactory,sesionFactory,idiomaFactory,ordenFactory,galeriaFactory){
        //Cuerpo declaraciones
        $(".li-menu").removeClass("active");
        $(".a-menu").removeClass("active");
        
        $("#li_empresa").addClass("active");    
        $("#nosotros").addClass("active");    
        
        $scope.titulo_pagina = "Quienes Somos";
        $scope.subtitulo_pagina  = "Registrar";
        $scope.activo_img = "inactivo";
        $scope.borrar_imagen = []
        $scope.currentTab = 'datos_basicos'
        $scope.titulo_registrar = "Registrar";
        $scope.titulo_cons = "Consultar";
        $scope.base_url = $("#base_url").val();
        $scope.titulo_text = "Pulse aquí para ingresar el contenido"
        //////////////////////////////////////////////////////////////////////////////
        $scope.nosotros  = {
                                    'id' : '',
                                    'titulo' : '',
                                    'descripcion' : '',
                                    'id_idioma' : '',
                                    'icono':'',
                                    'idioma' : '',
                                    'id_imagen':'',
        }
        $scope.lista_iconos = "";
        //////////////////////////////////////////////////////////////////////////////
        /*--Iconos --*/
        $scope.iconos = [
            {'icono':'<i class="icon-plus"></i>'},
            {'icono':'<i class="icon-plus-1"></i>'},
            {'icono':'<i class="icon-minus"></i>'},
            {'icono':'<i class="icon-minus-1"></i>'},
            {'icono':'<i class="icon-info"></i>'},
            {'icono':'<i class="icon-left-thin"></i>'},
            {'icono':'<i class="icon-left-1"></i>'},
            {'icono':'<i class="icon-up-thin"></i>'},
            {'icono':'<i class="icon-up-1"></i>'},
            {'icono':'<i class="icon-right-thin"></i>'},
            {'icono':'<i class="icon-right-1"></i>'},
            {'icono':'<i class="icon-down-thin"></i>'},
            {'icono':'<i class="icon-down-1"></i>'},
            {'icono':'<i class="icon-level-up"></i>'},
            {'icono':'<i class="icon-level-down"></i>'},
            {'icono':'<i class="icon-switch"></i>'},
            {'icono':'<i class="icon-infinity"></i>'},
            {'icono':'<i class="icon-plus-squared"></i>'},
            {'icono':'<i class="icon-minus-squared"></i>'},
            {'icono':'<i class="icon-home"></i>'},
            {'icono':'<i class="icon-home-1"></i>'},
            {'icono':'<i class="icon-keyboard"></i>'},
            {'icono':'<i class="icon-erase"></i>'},
            {'icono':'<i class="icon-pause"></i>'},
            {'icono':'<i class="icon-pause-1"></i>'},
            {'icono':'<i class="icon-fast-forward"></i>'},
            {'icono':'<i class="icon-fast-fw"></i>'},
            {'icono':'<i class="icon-fast-backward"></i>'},
            {'icono':'<i class="icon-fast-bw"></i>'},
            {'icono':'<i class="icon-to-end"></i>'},
            {'icono':'<i class="icon-to-end-1"></i>'},
            {'icono':'<i class="icon-to-start"></i>'},
            {'icono':'<i class="icon-to-start-1"></i>'},
            {'icono':'<i class="icon-hourglass"></i>'},
            {'icono':'<i class="icon-stop"></i>'},
            {'icono':'<i class="icon-stop-1"></i>'},
            {'icono':'<i class="icon-up-dir"></i>'},
            {'icono':'<i class="icon-up-dir-1"></i>'},
            {'icono':'<i class="icon-play"></i>'},
            {'icono':'<i class="icon-play-1"></i>'},
            {'icono':'<i class="icon-right-dir"></i>'},
            {'icono':'<i class="icon-right-dir-1"></i>'},
            {'icono':'<i class="icon-down-dir"></i>'},
            {'icono':'<i class="icon-down-dir-1"></i>'},
            {'icono':'<i class="icon-left-dir"></i>'},
            {'icono':'<i class="icon-left-dir-1"></i>'},
            {'icono':'<i class="icon-adjust"></i>'},
            {'icono':'<i class="icon-cloud"></i>'},
            {'icono':'<i class="icon-cloud-1"></i>'},
            {'icono':'<i class="icon-umbrella"></i>'},
            {'icono':'<i class="icon-star"></i>'},
            {'icono':'<i class="icon-star-1"></i>'},
            {'icono':'<i class="icon-star-empty"></i>'},
            {'icono':'<i class="icon-star-empty-1"></i>'},
            {'icono':'<i class="icon-check-1"></i>'},
            {'icono':'<i class="icon-cup"></i>'},
            {'icono':'<i class="icon-left-hand"></i>'},
            {'icono':'<i class="icon-up-hand"></i>'},
            {'icono':'<i class="icon-right-hand"></i>'},
            {'icono':'<i class="icon-down-hand"></i>'},
            {'icono':'<i class="icon-menu"></i>'},
            {'icono':'<i class="icon-th-list"></i>'},
            {'icono':'<i class="icon-moon"></i>'},
            {'icono':'<i class="icon-heart-empty"></i>'},
            {'icono':'<i class="icon-heart-empty-1"></i>'},
            {'icono':'<i class="icon-heart"></i>'},
            {'icono':'<i class="icon-heart-1"></i>'},
            {'icono':'<i class="icon-note"></i>'},
            {'icono':'<i class="icon-note-beamed"></i>'},
            {'icono':'<i class="icon-music-1"></i>'},
            {'icono':'<i class="icon-layout"></i>'},
            {'icono':'<i class="icon-th"></i>'},
            {'icono':'<i class="icon-flag"></i>'},
            {'icono':'<i class="icon-flag-1"></i>'},
            {'icono':'<i class="icon-tools"></i>'},
            {'icono':'<i class="icon-cog"></i>'},
            {'icono':'<i class="icon-cog-1"></i>'},
            {'icono':'<i class="icon-attention"></i>'},
            {'icono':'<i class="icon-attention-1"></i>'},
            {'icono':'<i class="icon-flash"></i>'},
            {'icono':'<i class="icon-flash-1"></i>'},
            {'icono':'<i class="icon-record"></i>'},
            {'icono':'<i class="icon-cloud-thunder"></i>'},
            {'icono':'<i class="icon-cog-alt"></i>'},
            {'icono':'<i class="icon-scissors"></i>'},
            {'icono':'<i class="icon-tape"></i>'},
            {'icono':'<i class="icon-flight"></i>'},
            {'icono':'<i class="icon-flight-1"></i>'},
            {'icono':'<i class="icon-mail"></i>'},
            {'icono':'<i class="icon-mail-1"></i>'},
            {'icono':'<i class="icon-edit"></i>'},
            {'icono':'<i class="icon-pencil"></i>'},
            {'icono':'<i class="icon-pencil-1"></i>'},
            {'icono':'<i class="icon-feather"></i>'},
            {'icono':'<i class="icon-check"></i>'},
            {'icono':'<i class="icon-ok"></i>'},
            {'icono':'<i class="icon-ok-circle"></i>'},
            {'icono':'<i class="icon-cancel"></i>'},
            {'icono':'<i class="icon-cancel-1"></i>'},
            {'icono':'<i class="icon-cancel-circled"></i>'},
            {'icono':'<i class="icon-cancel-circle"></i>'},
            {'icono':'<i class="icon-asterisk"></i>'},
            {'icono':'<i class="icon-cancel-squared"></i>'},
            {'icono':'<i class="icon-help"></i>'},
            {'icono':'<i class="icon-attention-circle"></i>'},
            {'icono':'<i class="icon-quote"></i>'},
            {'icono':'<i class="icon-plus-circled"></i>'},
            {'icono':'<i class="icon-plus-circle"></i>'},
            {'icono':'<i class="icon-minus-circled"></i>'},
            {'icono':'<i class="icon-minus-circle"></i>'},
            {'icono':'<i class="icon-right"></i>'},
            {'icono':'<i class="icon-direction"></i>'},
            {'icono':'<i class="icon-forward"></i>'},
            {'icono':'<i class="icon-forward-1"></i>'},
            {'icono':'<i class="icon-ccw"></i>'},
            {'icono':'<i class="icon-cw"></i>'},
            {'icono':'<i class="icon-cw-1"></i>'},
            {'icono':'<i class="icon-left"></i>'},
            {'icono':'<i class="icon-up"></i>'},
            {'icono':'<i class="icon-down"></i>'},
            {'icono':'<i class="icon-resize-vertical"></i>'},
            {'icono':'<i class="icon-resize-horizontal"></i>'},
            {'icono':'<i class="icon-eject"></i>'},
            {'icono':'<i class="icon-list-add"></i>'},
            {'icono':'<i class="icon-list"></i>'},
            {'icono':'<i class="icon-left-bold"></i>'},
            {'icono':'<i class="icon-right-bold"></i>'},
            {'icono':'<i class="icon-up-bold"></i>'},
            {'icono':'<i class="icon-down-bold"></i>'},
            {'icono':'<i class="icon-user-add"></i>'},
            {'icono':'<i class="icon-star-half"></i>'},
            {'icono':'<i class="icon-ok-circle2"></i>'},
            {'icono':'<i class="icon-cancel-circle2"></i>'},
            {'icono':'<i class="icon-help-circled"></i>'},
            {'icono':'<i class="icon-help-circle"></i>'},
            {'icono':'<i class="icon-info-circled"></i>'},
            {'icono':'<i class="icon-info-circle"></i>'},
            {'icono':'<i class="icon-th-large"></i>'},
            {'icono':'<i class="icon-eye"></i>'},
            {'icono':'<i class="icon-eye-1"></i>'},
            {'icono':'<i class="icon-eye-off"></i>'},
            {'icono':'<i class="icon-tag"></i>'},
            {'icono':'<i class="icon-tag-1"></i>'},
            {'icono':'<i class="icon-tags"></i>'},
            {'icono':'<i class="icon-camera-alt"></i>'},
            {'icono':'<i class="icon-upload-cloud"></i>'},
            {'icono':'<i class="icon-reply"></i>'},
            {'icono':'<i class="icon-reply-all"></i>'},
            {'icono':'<i class="icon-code"></i>'},
            {'icono':'<i class="icon-export"></i>'},
            {'icono':'<i class="icon-export-1"></i>'},
            {'icono':'<i class="icon-print"></i>'},
            {'icono':'<i class="icon-print-1"></i>'},
            {'icono':'<i class="icon-retweet"></i>'},
            {'icono':'<i class="icon-retweet-1"></i>'},
            {'icono':'<i class="icon-comment"></i>'},
            {'icono':'<i class="icon-comment-1"></i>'},
            {'icono':'<i class="icon-chat"></i>'},
            {'icono':'<i class="icon-chat-1"></i>'},
            {'icono':'<i class="icon-vcard"></i>'},
            {'icono':'<i class="icon-address"></i>'},
            {'icono':'<i class="icon-location"></i>'},
            {'icono':'<i class="icon-location-1"></i>'},
            {'icono':'<i class="icon-map"></i>'},
            {'icono':'<i class="icon-compass"></i>'},
            {'icono':'<i class="icon-trash"></i>'},
            {'icono':'<i class="icon-trash-1"></i>'},
            {'icono':'<i class="icon-doc"></i>'},
            {'icono':'<i class="icon-doc-text-inv"></i>'},
            {'icono':'<i class="icon-docs"></i>'},
            {'icono':'<i class="icon-doc-landscape"></i>'},
            {'icono':'<i class="icon-archive"></i>'},
            {'icono':'<i class="icon-rss"></i>'},
            {'icono':'<i class="icon-share"></i>'},
            {'icono':'<i class="icon-basket"></i>'},
            {'icono':'<i class="icon-basket-1"></i>'},
            {'icono':'<i class="icon-shareable"></i>'},
            {'icono':'<i class="icon-login"></i>'},
            {'icono':'<i class="icon-login-1"></i>'},
            {'icono':'<i class="icon-logout"></i>'},
            {'icono':'<i class="icon-logout-1"></i>'},
            {'icono':'<i class="icon-volume"></i>'},
            {'icono':'<i class="icon-resize-full"></i>'},
            {'icono':'<i class="icon-resize-full-1"></i>'},
            {'icono':'<i class="icon-resize-small"></i>'},
            {'icono':'<i class="icon-resize-small-1"></i>'},
            {'icono':'<i class="icon-popup"></i>'},
            {'icono':'<i class="icon-publish"></i>'},
            {'icono':'<i class="icon-window"></i>'},
            {'icono':'<i class="icon-arrow-combo"></i>'},
            {'icono':'<i class="icon-zoom-in"></i>'},
            {'icono':'<i class="icon-chart-pie"></i>'},
            {'icono':'<i class="icon-zoom-out"></i>'},
            {'icono':'<i class="icon-language"></i>'},
            {'icono':'<i class="icon-air"></i>'},
            {'icono':'<i class="icon-database"></i>'},
            {'icono':'<i class="icon-drive"></i>'},
            {'icono':'<i class="icon-bucket"></i>'},
            {'icono':'<i class="icon-thermometer"></i>'},
            {'icono':'<i class="icon-down-circled"></i>'},
            {'icono':'<i class="icon-down-circle2"></i>'},
            {'icono':'<i class="icon-left-circled"></i>'},
            {'icono':'<i class="icon-right-circled"></i>'},
            {'icono':'<i class="icon-up-circled"></i>'},
            {'icono':'<i class="icon-up-circle2"></i>'},
            {'icono':'<i class="icon-down-open"></i>'},
            {'icono':'<i class="icon-down-open-1"></i>'},
            {'icono':'<i class="icon-left-open"></i>'},
            {'icono':'<i class="icon-left-open-1"></i>'},
            {'icono':'<i class="icon-right-open"></i>'},
            {'icono':'<i class="icon-right-open-1"></i>'},
            {'icono':'<i class="icon-up-open"></i>'},
            {'icono':'<i class="icon-up-open-1"></i>'},
            {'icono':'<i class="icon-down-open-mini"></i>'},
            {'icono':'<i class="icon-arrows-cw"></i>'},
            {'icono':'<i class="icon-left-open-mini"></i>'},
            {'icono':'<i class="icon-play-circle2"></i>'},
            {'icono':'<i class="icon-right-open-mini"></i>'},
            {'icono':'<i class="icon-to-end-alt"></i>'},
            {'icono':'<i class="icon-up-open-mini"></i>'},
            {'icono':'<i class="icon-to-start-alt"></i>'},
            {'icono':'<i class="icon-down-open-big"></i>'},
            {'icono':'<i class="icon-left-open-big"></i>'},
            {'icono':'<i class="icon-right-open-big"></i>'},
            {'icono':'<i class="icon-up-open-big"></i>'},
            {'icono':'<i class="icon-progress-0"></i>'},
            {'icono':'<i class="icon-progress-1"></i>'},
            {'icono':'<i class="icon-progress-2"></i>'},
            {'icono':'<i class="icon-progress-3"></i>'},
            {'icono':'<i class="icon-back-in-time"></i>'},
            {'icono':'<i class="icon-network"></i>'},
            {'icono':'<i class="icon-inbox"></i>'},
            {'icono':'<i class="icon-inbox-1"></i>'},
            {'icono':'<i class="icon-install"></i>'},
            {'icono':'<i class="icon-font"></i>'},
            {'icono':'<i class="icon-bold"></i>'},
            {'icono':'<i class="icon-italic"></i>'},
            {'icono':'<i class="icon-text-height"></i>'},
            {'icono':'<i class="icon-text-width"></i>'},
            {'icono':'<i class="icon-align-left"></i>'},
            {'icono':'<i class="icon-align-center"></i>'},
            {'icono':'<i class="icon-align-right"></i>'},
            {'icono':'<i class="icon-align-justify"></i>'},
            {'icono':'<i class="icon-list-1"></i>'},
            {'icono':'<i class="icon-indent-left"></i>'},
            {'icono':'<i class="icon-indent-right"></i>'},
            {'icono':'<i class="icon-lifebuoy"></i>'},
            {'icono':'<i class="icon-mouse"></i>'},
            {'icono':'<i class="icon-dot"></i>'},
            {'icono':'<i class="icon-dot-2"></i>'},
            {'icono':'<i class="icon-dot-3"></i>'},
            {'icono':'<i class="icon-suitcase"></i>'},
            {'icono':'<i class="icon-off"></i>'},
            {'icono':'<i class="icon-road"></i>'},
            {'icono':'<i class="icon-flow-cascade"></i>'},
            {'icono':'<i class="icon-list-alt"></i>'},
            {'icono':'<i class="icon-flow-branch"></i>'},
            {'icono':'<i class="icon-qrcode"></i>'},
            {'icono':'<i class="icon-flow-tree"></i>'},
            {'icono':'<i class="icon-barcode"></i>'},
            {'icono':'<i class="icon-flow-line"></i>'},
            {'icono':'<i class="icon-ajust"></i>'},
            {'icono':'<i class="icon-tint"></i>'},
            {'icono':'<i class="icon-brush"></i>'},
            {'icono':'<i class="icon-paper-plane"></i>'},
            {'icono':'<i class="icon-magnet"></i>'},
            {'icono':'<i class="icon-magnet-1"></i>'},
            {'icono':'<i class="icon-gauge"></i>'},
            {'icono':'<i class="icon-traffic-cone"></i>'},
            {'icono':'<i class="icon-cc"></i>'},
            {'icono':'<i class="icon-cc-by"></i>'},
            {'icono':'<i class="icon-cc-nc"></i>'},
            {'icono':'<i class="icon-cc-nc-eu"></i>'},
            {'icono':'<i class="icon-cc-nc-jp"></i>'},
            {'icono':'<i class="icon-cc-sa"></i>'},
            {'icono':'<i class="icon-cc-nd"></i>'},
            {'icono':'<i class="icon-cc-pd"></i>'},
            {'icono':'<i class="icon-cc-zero"></i>'},
            {'icono':'<i class="icon-cc-share"></i>'},
            {'icono':'<i class="icon-cc-remix"></i>'},
            {'icono':'<i class="icon-move"></i>'},
            {'icono':'<i class="icon-link-ext"></i>'},
            {'icono':'<i class="icon-check-empty"></i>'},
            {'icono':'<i class="icon-bookmark-empty"></i>'},
            {'icono':'<i class="icon-phone-squared"></i>'},
            {'icono':'<i class="icon-twitter"></i>'},
            {'icono':'<i class="icon-facebook"></i>'},
            {'icono':'<i class="icon-github"></i>'},
            {'icono':'<i class="icon-rss-1"></i>'},
            {'icono':'<i class="icon-hdd"></i>'},
            {'icono':'<i class="icon-certificate"></i>'},
            {'icono':'<i class="icon-left-circled-1"></i>'},
            {'icono':'<i class="icon-right-circled-1"></i>'},
            {'icono':'<i class="icon-up-circled-1"></i>'},
            {'icono':'<i class="icon-down-circled-1"></i>'},
            {'icono':'<i class="icon-tasks"></i>'},
            {'icono':'<i class="icon-filter"></i>'},
            {'icono':'<i class="icon-resize-full-alt"></i>'},
            {'icono':'<i class="icon-beaker"></i>'},
            {'icono':'<i class="icon-docs-1"></i>'},
            {'icono':'<i class="icon-blank"></i>'},
            {'icono':'<i class="icon-menu-1"></i>'},
            {'icono':'<i class="icon-list-bullet"></i>'},
            {'icono':'<i class="icon-list-numbered"></i>'},
            {'icono':'<i class="icon-strike"></i>'},
            {'icono':'<i class="icon-underline"></i>'},
            {'icono':'<i class="icon-table"></i>'},
            {'icono':'<i class="icon-magic"></i>'},
            {'icono':'<i class="icon-pinterest-circled-1"></i>'},
            {'icono':'<i class="icon-pinterest-squared"></i>'},
            {'icono':'<i class="icon-gplus-squared"></i>'},
            {'icono':'<i class="icon-gplus"></i>'},
            {'icono':'<i class="icon-money"></i>'},
            {'icono':'<i class="icon-columns"></i>'},
            {'icono':'<i class="icon-sort"></i>'},
            {'icono':'<i class="icon-sort-down"></i>'},
            {'icono':'<i class="icon-sort-up"></i>'},
            {'icono':'<i class="icon-mail-alt"></i>'},
            {'icono':'<i class="icon-linkedin"></i>'},
            {'icono':'<i class="icon-gauge-1"></i>'},
            {'icono':'<i class="icon-comment-empty"></i>'},
            {'icono':'<i class="icon-chat-empty"></i>'},
            {'icono':'<i class="icon-sitemap"></i>'},
            {'icono':'<i class="icon-paste"></i>'},
            {'icono':'<i class="icon-user-md"></i>'},
            {'icono':'<i class="icon-s-github"></i>'},
            {'icono':'<i class="icon-github-squared"></i>'},
            {'icono':'<i class="icon-github-circled"></i>'},
            {'icono':'<i class="icon-s-flickr"></i>'},
            {'icono':'<i class="icon-twitter-squared"></i>'},
            {'icono':'<i class="icon-s-vimeo"></i>'},
            {'icono':'<i class="icon-vimeo-circled"></i>'},
            {'icono':'<i class="icon-facebook-squared-1"></i>'},
            {'icono':'<i class="icon-s-twitter"></i>'},
            {'icono':'<i class="icon-twitter-circled"></i>'},
            {'icono':'<i class="icon-s-facebook"></i>'},
            {'icono':'<i class="icon-linkedin-squared"></i>'},
            {'icono':'<i class="icon-facebook-circled"></i>'},
            {'icono':'<i class="icon-s-gplus"></i>'},
            {'icono':'<i class="icon-gplus-circled"></i>'},
            {'icono':'<i class="icon-s-pinterest"></i>'},
            {'icono':'<i class="icon-pinterest-circled"></i>'},
            {'icono':'<i class="icon-s-tumblr"></i>'},
            {'icono':'<i class="icon-tumblr-circled"></i>'},
            {'icono':'<i class="icon-s-linkedin"></i>'},
            {'icono':'<i class="icon-linkedin-circled"></i>'},
            {'icono':'<i class="icon-s-dribbble"></i>'},
            {'icono':'<i class="icon-dribbble-circled"></i>'},
            {'icono':'<i class="icon-s-stumbleupon"></i>'},
            {'icono':'<i class="icon-stumbleupon-circled"></i>'},
            {'icono':'<i class="icon-s-lastfm"></i>'},
            {'icono':'<i class="icon-lastfm-circled"></i>'},
            {'icono':'<i class="icon-rdio"></i>'},
            {'icono':'<i class="icon-rdio-circled"></i>'},
            {'icono':'<i class="icon-spotify"></i>'},
            {'icono':'<i class="icon-s-spotify-circled"></i>'},
            {'icono':'<i class="icon-qq"></i>'},
            {'icono':'<i class="icon-s-instagrem"></i>'},
            {'icono':'<i class="icon-dropbox"></i>'},
            {'icono':'<i class="icon-s-evernote"></i>'},
            {'icono':'<i class="icon-flattr"></i>'},
            {'icono':'<i class="icon-s-skype"></i>'},
            {'icono':'<i class="icon-skype-circled"></i>'},
            {'icono':'<i class="icon-renren"></i>'},
            {'icono':'<i class="icon-sina-weibo"></i>'},
            {'icono':'<i class="icon-s-paypal"></i>'},
            {'icono':'<i class="icon-s-picasa"></i>'},
            {'icono':'<i class="icon-s-soundcloud"></i>'},
            {'icono':'<i class="icon-s-behance"></i>'},
            {'icono':'<i class="icon-google-circles"></i>'},
            {'icono':'<i class="icon-vkontakte"></i>'},
            {'icono':'<i class="icon-smashing"></i>'},
            {'icono':'<i class="icon-db-shape"></i>'},
            {'icono':'<i class="icon-sweden"></i>'},
            {'icono':'<i class="icon-logo-db"></i>'},
            {'icono':'<i class="icon-picture"></i>'},
            {'icono':'<i class="icon-picture-1"></i>'},
            {'icono':'<i class="icon-globe"></i>'},
            {'icono':'<i class="icon-globe-1"></i>'},
            {'icono':'<i class="icon-leaf-1"></i>'},
            {'icono':'<i class="icon-lemon"></i>'},
            {'icono':'<i class="icon-glass"></i>'},
            {'icono':'<i class="icon-gift"></i>'},
            {'icono':'<i class="icon-graduation-cap"></i>'},
            {'icono':'<i class="icon-mic"></i>'},
            {'icono':'<i class="icon-videocam"></i>'},
            {'icono':'<i class="icon-headphones"></i>'},
            {'icono':'<i class="icon-palette"></i>'},
            {'icono':'<i class="icon-ticket"></i>'},
            {'icono':'<i class="icon-video"></i>'},
            {'icono':'<i class="icon-video-1"></i>'},
            {'icono':'<i class="icon-target"></i>'},
            {'icono':'<i class="icon-target-1"></i>'},
            {'icono':'<i class="icon-music"></i>'},
            {'icono':'<i class="icon-trophy"></i>'},
            {'icono':'<i class="icon-award"></i>'},
            {'icono':'<i class="icon-thumbs-up"></i>'},
            {'icono':'<i class="icon-thumbs-up-1"></i>'},
            {'icono':'<i class="icon-thumbs-down"></i>'},
            {'icono':'<i class="icon-thumbs-down-1"></i>'},
            {'icono':'<i class="icon-bag"></i>'},
            {'icono':'<i class="icon-user"></i>'},
            {'icono':'<i class="icon-user-1"></i>'},
            {'icono':'<i class="icon-users"></i>'},
            {'icono':'<i class="icon-users-1"></i>'},
            {'icono':'<i class="icon-lamp"></i>'},
            {'icono':'<i class="icon-alert"></i>'},
            {'icono':'<i class="icon-water"></i>'},
            {'icono':'<i class="icon-droplet"></i>'},
            {'icono':'<i class="icon-credit-card"></i>'},
            {'icono':'<i class="icon-credit-card-1"></i>'},
            {'icono':'<i class="icon-monitor"></i>'},
            {'icono':'<i class="icon-briefcase"></i>'},
            {'icono':'<i class="icon-briefcase-1"></i>'},
            {'icono':'<i class="icon-floppy"></i>'},
            {'icono':'<i class="icon-floppy-1"></i>'},
            {'icono':'<i class="icon-cd"></i>'},
            {'icono':'<i class="icon-folder"></i>'},
            {'icono':'<i class="icon-folder-1"></i>'},
            {'icono':'<i class="icon-folder-open"></i>'},
            {'icono':'<i class="icon-doc-text"></i>'},
            {'icono':'<i class="icon-doc-1"></i>'},
            {'icono':'<i class="icon-calendar"></i>'},
            {'icono':'<i class="icon-calendar-1"></i>'},
            {'icono':'<i class="icon-chart-line"></i>'},
            {'icono':'<i class="icon-chart-bar"></i>'},
            {'icono':'<i class="icon-chart-bar-1"></i>'},
            {'icono':'<i class="icon-clipboard"></i>'},
            {'icono':'<i class="icon-pin"></i>'},
            {'icono':'<i class="icon-attach"></i>'},
            {'icono':'<i class="icon-attach-1"></i>'},
            {'icono':'<i class="icon-bookmarks"></i>'},
            {'icono':'<i class="icon-book"></i>'},
            {'icono':'<i class="icon-book-1"></i>'},
            {'icono':'<i class="icon-book-open"></i>'},
            {'icono':'<i class="icon-phone"></i>'},
            {'icono':'<i class="icon-phone-1"></i>'},
            {'icono':'<i class="icon-megaphone"></i>'},
            {'icono':'<i class="icon-megaphone-1"></i>'},
            {'icono':'<i class="icon-upload"></i>'},
            {'icono':'<i class="icon-upload-1"></i>'},
            {'icono':'<i class="icon-download"></i>'},
            {'icono':'<i class="icon-download-1"></i>'},
            {'icono':'<i class="icon-box"></i>'},
            {'icono':'<i class="icon-newspaper"></i>'},
            {'icono':'<i class="icon-mobile"></i>'},
            {'icono':'<i class="icon-signal"></i>'},
            {'icono':'<i class="icon-signal-1"></i>'},
            {'icono':'<i class="icon-camera"></i>'},
            {'icono':'<i class="icon-camera-1"></i>'},
            {'icono':'<i class="icon-shuffle"></i>'},
            {'icono':'<i class="icon-shuffle-1"></i>'},
            {'icono':'<i class="icon-loop"></i>'},
            {'icono':'<i class="icon-arrows-ccw"></i>'},
            {'icono':'<i class="icon-light-down"></i>'},
            {'icono':'<i class="icon-light-up"></i>'},
            {'icono':'<i class="icon-mute"></i>'},
            {'icono':'<i class="icon-volume-off"></i>'},
            {'icono':'<i class="icon-volume-down"></i>'},
            {'icono':'<i class="icon-sound"></i>'},
            {'icono':'<i class="icon-volume-up"></i>'},
            {'icono':'<i class="icon-battery"></i>'},
            {'icono':'<i class="icon-search"></i>'},
            {'icono':'<i class="icon-search-1"></i>'},
            {'icono':'<i class="icon-key"></i>'},
            {'icono':'<i class="icon-key-1"></i>'},
            {'icono':'<i class="icon-lock"></i>'},
            {'icono':'<i class="icon-lock-1"></i>'},
            {'icono':'<i class="icon-lock-open"></i>'},
            {'icono':'<i class="icon-lock-open-1"></i>'},
            {'icono':'<i class="icon-bell"></i>'},
            {'icono':'<i class="icon-bell-1"></i>'},
            {'icono':'<i class="icon-bookmark"></i>'},
            {'icono':'<i class="icon-bookmark-1"></i>'},
            {'icono':'<i class="icon-link"></i>'},
            {'icono':'<i class="icon-link-1"></i>'},
            {'icono':'<i class="icon-back"></i>'},
            {'icono':'<i class="icon-fire"></i>'},
            {'icono':'<i class="icon-flashlight"></i>'},
            {'icono':'<i class="icon-wrench"></i>'},
            {'icono':'<i class="icon-hammer"></i>'},
            {'icono':'<i class="icon-chart-area"></i>'},
            {'icono':'<i class="icon-clock"></i>'},
            {'icono':'<i class="icon-clock-1"></i>'},
            {'icono':'<i class="icon-rocket"></i>'},
            {'icono':'<i class="icon-truck"></i>'},
            {'icono':'<i class="icon-block"></i>'},
            {'icono':'<i class="icon-block-1"></i>'},
            {'icono':'<i class="icon-s-rss"></i>'},
            {'icono':'<i class="icon-s-twitter"></i>'},
            {'icono':'<i class="icon-s-facebook"></i>'},
            {'icono':'<i class="icon-s-dribbble"></i>'},
            {'icono':'<i class="icon-s-pinterest"></i>'},
            {'icono':'<i class="icon-s-flickr"></i>'},
            {'icono':'<i class="icon-s-vimeo"></i>'},
            {'icono':'<i class="icon-s-youtube"></i>'},
            {'icono':'<i class="icon-s-skype"></i>'},
            {'icono':'<i class="icon-s-tumblr"></i>'},
            {'icono':'<i class="icon-s-linkedin"></i>'},
            {'icono':'<i class="icon-s-behance"></i>'},
            {'icono':'<i class="icon-s-github"></i>'},
            {'icono':'<i class="icon-s-delicious"></i>'},
            {'icono':'<i class="icon-s-500px"></i>'},
            {'icono':'<i class="icon-s-grooveshark"></i>'},
            {'icono':'<i class="icon-s-forrst"></i>'},
            {'icono':'<i class="icon-s-digg"></i>'},
            {'icono':'<i class="icon-s-blogger"></i>'},
            {'icono':'<i class="icon-s-klout"></i>'},
            {'icono':'<i class="icon-s-dropbox"></i>'},
            {'icono':'<i class="icon-s-songkick"></i>'},
            {'icono':'<i class="icon-s-posterous"></i>'},
            {'icono':'<i class="icon-s-appnet"></i>'},
            {'icono':'<i class="icon-s-github"></i>'},
            {'icono':'<i class="icon-s-gplus"></i>'},
            {'icono':'<i class="icon-s-stumbleupon"></i>'},
            {'icono':'<i class="icon-s-lastfm"></i>'},
            {'icono':'<i class="icon-s-spotify"></i>'},
            {'icono':'<i class="icon-s-instagram"></i>'},
            {'icono':'<i class="icon-s-evernote"></i>'},
            {'icono':'<i class="icon-s-paypal"></i>'},
            {'icono':'<i class="icon-s-picasa"></i>'},
            {'icono':'<i class="icon-s-soundcloud"></i>'},
        ]
        //////////////////////////////////////////////////////////////////////////////
        $scope.consultar_idioma = function(){
            idiomaFactory.asignar_valores("",$scope.base_url)
            idiomaFactory.cargar_idioma(function(data){
                $scope.idioma=data;
                //console.log($scope.idioma);
            });
        }
        //////////////////////////////////////////////////////////////////////////////
        $scope.agregarWisi = function(){
            $scope.nosotros.descripcion = $(".textarea_editor").val()
            $("#div_descripcion").html($scope.nosotros.descripcion)
            $("#cerrarModal").click();
        }
        $scope.wisi_modal = function(){
            $("#wisiModal").modal("show")
            $(".textarea_editor").data("wysihtml5").editor.setValue($scope.nosotros.descripcion)
        }
        //--
        $scope.consultar_galeria_img = function(){
            galeriaFactory.asignar_valores('3','',$scope.base_url);//ya que es galeria de imagenes
            galeriaFactory.cargar_galeria_fa(function(data){
                $scope.galery=data;
                //console.log($scope.galery);
            });
        }

        $scope.seleccione_img_principal = function(){
            $("#modal_img1").modal("show");
        }

        $scope.seleccionar_imagen = function(event){
            var imagen = event.target.id;//Para capturar id
            var vec = $("#"+imagen).attr("data");
            var vector_data = vec.split("|")
            var id_imagen = vector_data[0];
            var ruta = vector_data[1];

            $scope.borrar_imagen.push(id_imagen);
            $scope.activo_img = "activo"
            $scope.nosotros.id_imagen = id_imagen
            $scope.nosotros.imagen = ruta
            //--
            $("#modal_img1").modal("hide");
            //--
        }

        //--Modal de iconos
        $scope.icon_modal = function(){
            $("#iconModal").modal("show")
        }
        //--
        $scope.agregarIcon = function(){
            $("#cerrarModal").click();
        }
        /////////////////////////////////////////////////////////////////////////////////////
        $scope.registrarCategoriasProd = function(){
            uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

            $scope.nosotros.orden = $("#orden").val();

            if($scope.validar_form()==true){
                //Para guardar
                $scope.nosotros.id = $scope.id_nosotros;

                if(($scope.nosotros.id!="")&&($scope.nosotros.id!=undefined)){
                    $scope.modificar_nosotros();
                }else{
                    $scope.insertar_nosotros();
                }

            }

            //desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

        }
        //////////////////////////
        $scope.validar_form = function(){
                        
            if(($scope.nosotros.id_idioma=="")||($scope.nosotros.id_idioma=="")){
                mostrar_notificacion("Campos no validos","Debe seleccionar un idioma","warning");
                return false;
            }else if(($scope.nosotros.titulo=="")||($scope.nosotros.titulo=="")){
                mostrar_notificacion("Campos no validos","Debe registrar un título","warning");
                return false;
            }else if(($scope.nosotros.descripcion=="NULL")||($scope.nosotros.descripcion=="")){
                mostrar_notificacion("Campos no validos","Debe registrar una descripción","warning");
                return false;
            }/*
            else if(($scope.nosotros.icono=="NULL")||($scope.nosotros.icono=="")){
                mostrar_notificacion("Campos no validos","Debe seleccionar un icono","warning");
                return false;
            }*/else if(($scope.nosotros.orden=="NULL")||($scope.nosotros.orden=="")){
                mostrar_notificacion("Campos no validos","Debe seleccionar el orden del servicio","warning");
                return false;
            }
            else{
                return true;
            }
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        $scope.limpiar_cajas_quienes_somos = function(){
            $scope.nosotros  = {
                                        'id' : '',
                                        'titulo' : '',
                                        'descripcion' : '',
                                        'id_idioma' : '',
                                        'idioma' : '',
                                        'icono':'',
                                        'id_imagen':'',
            }
            $scope.activo_img = "inactivo";

            $scope.lista_iconos = ""
            $(".iconoSeleccionado").html("")
            $("#div_descripcion").html($scope.titulo_text);
            $('#textarea_editor').data("wysihtml5").editor.clear();
            eliminarOptions("orden")
            $('#orden > option[value=""]').prop('selected', true);
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        $scope.insertar_nosotros = function(){
            $http.post($scope.base_url+"/QuienesSomos/registrarnosotros",
            {
                'titulo' : $scope.nosotros.titulo,
                'descripcion': $scope.nosotros.descripcion,
                'id_idioma' : $scope.nosotros.id_idioma.id,
                'icono': $scope.nosotros.icono,
                'orden': $scope.nosotros.orden,
                'id_imagen': $scope.nosotros.id_imagen
            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "registro_procesado"){
                    mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
                    $scope.limpiar_cajas_quienes_somos();
                }else if($scope.mensajes.mensaje == "existe"){
                    mostrar_notificacion("Mensaje","Ya existe una categoría con esa descripción","warning");
                }
                else{
                    mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
                }
            }).error(function(data,estatus){
                console.log(data);
            });
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        $scope.consultarnosotrosIndividual = function(){
            
            uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

            //console.log($scope.id_nosotros);
            nosotrosFactory.asignar_valores($scope.nosotros.id_idioma,$scope.id_nosotros,$scope.base_url)
            nosotrosFactory.cargar_nosotros(function(data){
                $scope.nosotros=data[0];
                $scope.borrar_imagen.push($scope.nosotros.id_imagen);
                //console.log($scope.borrar_imagen)
                $scope.activo_img = "activo"
                ordenFactory.asignar_valores($scope.nosotros.id_idioma,$scope.base_url,"2")
                ordenFactory.cargar_orden_nosotros(function(data){
                    $scope.ordenes=data;
                    //console.log($scope.ordenes)
                    //-Elimino el select
                    eliminarOptions("orden")
                    $.each( $scope.ordenes, function( indice, elemento ){
                          agregarOptions("#orden", elemento.orden, elemento.orden)
                    });
                });

                //console.log(data[0]);

                $("#div_descripcion").html($scope.nosotros.descripcion)
                setTimeout(function(){
                    $('#idioma > option[value="'+$scope.nosotros.id_idioma+'"]').prop('selected', true);
                    $('#orden > option[value="'+$scope.nosotros.orden+'"]').prop('selected', true);
                    $scope.nosotros.inicial = $scope.nosotros.orden;
                },300);
                $("#idioma").prop('disabled', true);

                $scope.lista_iconos = $scope.nosotros.icono
                
                $scope.titulo_registrar = "Modificar";
                
                $scope.subtitulo_pagina  = "Modificar categorías";
            
            });
            desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////
        $scope.modificar_nosotros = function(){
            //console.log($scope.nosotros.id_idioma);
            $http.post($scope.base_url+"/QuienesSomos/modificarnosotros",
            {
                'id'           : $scope.nosotros.id,
                'titulo'      : $scope.nosotros.titulo,
                'descripcion': $scope.nosotros.descripcion,
                'id_idioma'  : $scope.nosotros.id_idioma,
                'icono': $scope.nosotros.icono,
                'orden': $scope.nosotros.orden,
                'inicial':$scope.nosotros.inicial,
                'id_imagen':$scope.nosotros.id_imagen,
            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "modificacion_procesada"){
                    mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
                    //$scope.limpiar_cajas_quienes_somos();
                }else{
                    mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
                }
            }).error(function(data,estatus){
                console.log(data);
            });
        }
        $scope.consultar_idioma();
        $scope.consultar_galeria_img();

        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        $scope.id_nosotros  = $("#id_nosotros").val();
        //console.log($scope.id_nosotros);
            if($scope.id_nosotros){
                $scope.consultarnosotrosIndividual();
            }
        //------------------------------------------------------------------------------
        $scope.seleccionarIcono = function(event){

            //var id_icono = event.target.id;//Para capturar id

            var text_icono = $("#super_icono"+event).attr("data");

            $scope.lista_iconos= text_icono;

            $scope.nosotros.icono = text_icono;

            $("#iconModal").modal("hide");
            
        }
        //------------------------------------------------------------------------------
        $scope.cargarOrden = function(){
            ordenFactory.asignar_valores($scope.nosotros.id_idioma.id,$scope.base_url,"1")
            ordenFactory.cargar_orden_nosotros(function(data){
                $scope.ordenes=data;
                //console.log($scope.ordenes);
                //-Elimino el select
                eliminarOptions("orden")
                $.each( $scope.ordenes, function( indice, elemento ){
                      agregarOptions("#orden", elemento.orden, elemento.orden)
                });
                $('#orden > option[value=""]').prop('selected', true);

            });
        }
        //------------------------------------------------------------------------------    
    })
    .controller("ReaseguradorasConsultasController", function($scope,$http,$location,serverDataMensajes,reaseguradorasFactory,sesionFactory,idiomaFactory){
        $(".li-menu").removeClass("active");
        $(".a-menu").removeClass("active");
        
        $("#li_empresa").addClass("active");    
        $("#reaseguradoras").addClass("active");    

        $scope.titulo_pagina = "Consulta de Reaseguradoras";
        $scope.activo_img = "inactivo";
        $scope.reaseguradoras = {
                        'id': '',
                        'idioma': '',
                        'id_idioma' : '',
                        'titulo' : '',
                        'descripcion' : '',
                        'id_imagen' : '',
                        'imagen' : 'assets/images/logo_peque.png',
                        'orden':''
        }
        $scope.categorias_menu = "1";
        $scope.id_reaseguradoras = "";
        $scope.base_url = $("#base_url").val();
        //-----------------------------------------------------
        //--Cuerpo de metodos  --/
        $scope.iniciar_datatable = function(){
            //--Iniciar DataTable
            $('#myTable').DataTable({
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                    "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
            });
            //console.log($scope.categorias);
        }
        $scope.consultar_reaseguradoras = function(){
            reaseguradorasFactory.asignar_valores("","",$scope.base_url)
            reaseguradorasFactory.cargar_reaseguradoras(function(data){
                $scope.reaseguradoras=data;
                //console.log($scope.reaseguradoras);                
            });
        }
        //---------------------------------------------------------------
        $scope.ver_reaseguradoras = function(valor){
            var id = $("#ver"+valor).attr("data");
            $scope.id_reaseguradoras = id;    
            $("#id_reaseguradoras").val($scope.id_reaseguradoras)
            let form = document.getElementById('formConsultaReaseguradoras');
            form.action = "./reaseguradoras_ver";
            form.submit();
        }
        //----------------------------------------------------------------
        $scope.activar_registro = function(event){
            $scope.id_seleccionado_reaseguradoras = []
            $scope.estatus_seleccionado_reaseguradoras = []
            var caja = event.currentTarget.id;
            var atributos = $("#"+caja).attr("data");
            var arreglo_atributos = atributos.split("|");
            $scope.id_seleccionado_reaseguradoras = arreglo_atributos[0];
            $scope.estatus_seleccionado_reaseguradoras = arreglo_atributos[1];
            $("#cabecera_mensaje").text("Información:");            
            //--
            if ($scope.estatus_seleccionado_reaseguradoras==0){
                mensaje = "Desea modificar el estatus de este registro a publicado? ";
                $scope.estatus_seleccionado_reaseguradoras=1;
            }else{
                mensaje = "Desea modificar el estatus de este registro a inactivo? ";
                $scope.estatus_seleccionado_reaseguradoras=0
            }
            //($scope.estatus_seleccionado_categorias==1) ? $scope.estatus_seleccionado_categorias=0:$scope.estatus_seleccionado_categorias=1;

            //--
            $scope.modificar_estatus("activar_inactivar",mensaje)
        }
        //----------------------------------------------------------------
        $scope.modificar_estatus = function(opcion,mensaje){
             swal({
                  title: 'Esta seguro?',
                  text: mensaje,
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Si!',
                  cancelButtonText: 'No',
                }).then((result) => {
                      if (result.value) {
                          $scope.accion_estatus()
                      }
                })
        }
        //----------------------------------------------------------------
        $scope.accion_estatus = function(){
            $http.post($scope.base_url+"/reaseguradoras/modificarReaseguradorasEstatus",
            {
                 'id':$scope.id_seleccionado_reaseguradoras,    
                 'estatus':$scope.estatus_seleccionado_reaseguradoras,     

            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                //console.log($scope.mensajes)
                if($scope.mensajes.mensaje == "modificacion_procesada"){
                    Swal(
                          'Realizado!',
                          'El proceso fue ejecutado.',
                          'success'
                    ).then((result) => {
                          if (result.value) {
                                let form = document.getElementById('formConsultaReaseguradoras');
                                form.action = "./consultar_reaseguradoras";
                                form.submit();
                          }
                    
                    });
                }else{
                    Swal(
                          'No realizado!',
                          'El proceso no pudo ser ejecutado.',
                          'warning'
                    )
                }
            }).error(function(data,estatus){
                console.log(data);
            });    
        }
        //----------------------------------------------------------------
        $scope.eliminar_registro = function(event){
            $scope.id_seleccionado_reaseguradoras = []
            $scope.estatus_seleccionado_reaseguradoras = []
            var caja = event.currentTarget.id;
            //alert(caja);
            var atributos = $("#"+caja).attr("data");
            var arreglo_atributos = atributos.split("|");
            $scope.id_seleccionado_reaseguradoras = arreglo_atributos[0];
            $scope.estatus_seleccionado_reaseguradoras = arreglo_atributos[1];
            //console.log($scope.estatus_seleccionado_reaseguradoras);
            $("#cabecera_mensaje").text("Información:");
            if ($scope.estatus_seleccionado_reaseguradoras==2){
                mensaje = "Desea eliminar este registro? ";
            }
            $scope.modificar_estatus("eliminar",mensaje)
        }
        //----------------------------------------------------------------
        //-- Cuerpo de funciones--/
        setTimeout(function(){
                $scope.iniciar_datatable();
        },500);
        $scope.consultar_reaseguradoras();
        
        //-----------------------------------------------------
    })
    .controller("ReaseguradorasController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,sesionFactory,idiomaFactory,reaseguradorasFactory,ordenFactory){
        //---------------------------------------------------------------------
        //Cuerpo declaraciones
        $(".li-menu").removeClass("active");
        $(".a-menu").removeClass("active");
        $("#li_empresa").addClass("active");    
        $("#reaseguradoras").addClass("active");    

        $scope.titulo_pagina = "Reaseguradoras";
        $scope.subtitulo_pagina  = "Registrar Reaseguradoras";
        $scope.activo_img = "inactivo";

        $scope.reaseguradoras = {
                        'id': '',
                        'idioma': '',
                        'id_idioma' : '',
                        'titulo' : '',
                        'url' : '',
                        'descripcion' : '',
                        'id_imagen' : '',
                        'imagen' : 'assets/images/logo_peque.png',
                        'orden':''
        }
        
        
        $scope.titulo_registrar = "Registrar";
        $scope.titulo_cons = "Consultar";
        $scope.borrar_imagen = []
        $scope.currentTab = 'datos_basicos'
        $scope.titulo_text = "Pulse aquí para ingresar la descripción de la reaseguradoras"

        $scope.opcion = ''
        $scope.seccion = ''
        $scope.base_url = $("#base_url").val();
        
        //alert($scope.base_url);
        //Cuerpo de metodos
        $scope.consultar_idioma = function(){
            idiomaFactory.asignar_valores("",$scope.base_url)
            idiomaFactory.cargar_idioma(function(data){
                $scope.idioma=data;

                $.each( $scope.idioma, function( indice, elemento ){
                      agregarOptions("#idioma", elemento.id, elemento.descripcion)
                });
                $('#idioma > option[value="'+$scope.reaseguradoras.id_idioma+'"]').prop('selected', true);
            });
        }
        //---------------------------------
        //
        $scope.cargarOrden = function(){
            ordenFactory.asignar_valores($scope.reaseguradoras.id_idioma,$scope.base_url,"1")
            ordenFactory.cargar_orden_reaseguradoras(function(data){
                $scope.ordenes=data;
                //console.log($scope.ordenes);
                //-Elimino el select
                eliminarOptions("orden")
                $.each( $scope.ordenes, function( indice, elemento ){
                      agregarOptions("#orden", elemento.orden, elemento.orden)
                });
                $('#orden > option[value=""]').prop('selected', true);

            });
        }
        //WISIMODAL
        $scope.agregarWisi = function(){
            //$('#text_editor').data("wysihtml5").editor.clear();
            $scope.reaseguradoras.descripcion = $(".textarea_editor").val()
            $("#div_descripcion").html($scope.reaseguradoras.descripcion)
            $("#cerrarModal").click();
        }
        //--
        
        $scope.wisi_modal = function(){
            $("#wisiModal").modal("show")
            $(".textarea_editor").data("wysihtml5").editor.setValue($scope.reaseguradoras.descripcion)
        }
        //--
        $scope.agregar_contenido = function(){
            if ($scope.reaseguradoras==undefined) {
                    $scope.reaseguradoras = {
                        'id': '',
                        'idioma': '',
                        'id_idioma' : '',
                        'titulo' : '',
                        'url' : '',
                        'descripcion' : '',
                        'id_imagen' : '',
                        'imagen' : 'assets/images/logo_peque.png',
                        'orden':''
                    }
            }

        }
        //------------------------------------------CONSULTA DE IMAGEN SEGUN ID DE CATEGORIA
        $scope.consultar_galeria_img = function(){
            galeriaFactory.asignar_valores('24','',$scope.base_url);//ya que es galeria de imagenes
            galeriaFactory.cargar_galeria_fa(function(data){
                $scope.galery=data;
                //console.log($scope.galery);
            });
        }
        //MODAL DE IMG
        $scope.seleccione_img_principal = function(){
            $("#modal_img1").modal("show");
        }
        //PARA SELECCIONAR UNA IMAGEN Y MOSTRARLA EN EL CAMPO DE IMG
        $scope.seleccionar_imagen = function(event){
                var imagen = event.target.id;//Para capturar id
                //console.log(imagen);
                var vec = $("#"+imagen).attr("data");
                var vector_data = vec.split("|")
                var id_imagen = vector_data[0];
                var ruta = vector_data[1];

                $scope.borrar_imagen.push(id_imagen);
                $scope.activo_img = "activo"
                $scope.reaseguradoras.id_imagen = id_imagen
                $scope.reaseguradoras.imagen = ruta
                //alert($scope.reaseguradoras.id_imagen);
                //--
                $("#modal_img1").modal("hide");
                //--
        }
        /////////////////////////////////////////////////////////////////////////////////////
        $scope.registrarReaseguradoras = function(){
            $scope.reaseguradoras.orden = $("#orden").val();
            uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
            if($scope.validar_form()==true){
                //Para guardar
                if(($scope.reaseguradoras.id!="")&&($scope.reaseguradoras.id!=undefined)){
                    $scope.modificar_reaseguradoras();
                }else{
                    $scope.insertar_reaseguradoras();
                }
            }
            //desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

        }
        ///////////////////////////////////////////////////////////////////////////////////
        $scope.insertar_reaseguradoras = function(){
            $http.post($scope.base_url+"/Reaseguradoras/registrarReaseguradoras",
            {
                'id'          : $scope.reaseguradoras.id,
                'id_idioma'  : $scope.reaseguradoras.id_idioma,
                'titulo'     : $scope.reaseguradoras.titulo,
                'url'     : $scope.reaseguradoras.url,
                'descripcion': $scope.reaseguradoras.descripcion,
                'id_imagen'  : $scope.reaseguradoras.id_imagen,
                'orden':  $scope.reaseguradoras.orden
            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "registro_procesado"){
                    mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
                    $scope.limpiar_cajas_reaseguradoras();
                }else if($scope.mensajes.mensaje == "existe"){
                    mostrar_notificacion("Mensaje","Ya existe una noticia con ese titulo","warning");
                }
                else{
                    mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
                }

            }).error(function(data,estatus){
                console.log(data);
            });
        }
        //////////////////////////
        $scope.modificar_reaseguradoras = function(){
            $scope.reaseguradoras.orden = $("#orden").val();
            alert($scope.reaseguradoras.orden)
            $http.post($scope.base_url+"/Reaseguradoras/modificarReaseguradoras",
            {
                'id'          : $scope.reaseguradoras.id,
                'id_idioma'  : $scope.reaseguradoras.id_idioma,
                'titulo'     : $scope.reaseguradoras.titulo,
                'url'     : $scope.reaseguradoras.url,
                'descripcion': $scope.reaseguradoras.descripcion,
                'id_imagen'  : $scope.reaseguradoras.id_imagen,
                'boton'      : $scope.reaseguradoras.boton,
                'url'        : $scope.reaseguradoras.url,
                'direccion' : $scope.reaseguradoras.direccion,
                'orden'     : $scope.reaseguradoras.orden,
                'inicial'    : $scope.reaseguradoras.inicial
            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "modificacion_procesada"){
                    mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
                    //$scope.limpiar_cajas_reaseguradoras();
                    $scope.reaseguradoras.inicial = $scope.reaseguradoras.orden 
                    desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

                }else{
                    mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
                    desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

                }
                //desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

            }).error(function(data,estatus){
                //console.log(data);
            });
        }
        //////////////////////////
        $scope.validar_form = function(){
            //console.log($scope.reaseguradoras)
            if(($scope.reaseguradoras.id_idioma=="")||($scope.reaseguradoras.id_idioma=="")){
                mostrar_notificacion("Campos no validos","Debe seleccionar el idioma","warning");
                return false;
            }/*else if(($scope.reaseguradoras.descripcion=="")||($scope.reaseguradoras.descripcion=="")){
                mostrar_notificacion("Campos no validos","Debe seleccionar la dirección del texto del slide","warning");
                return false;
            }*/ else if($scope.reaseguradoras.titulo==""){
                mostrar_notificacion("Campos no validos","Debe ingresar el titulo","warning");
                return false;
            }else if(($scope.reaseguradoras.orden=="NULL")||($scope.reaseguradoras.orden=="")){
                mostrar_notificacion("Campos no validos","Debe seleccionar el orden","warning");
                return false;
            }
            else if(($scope.reaseguradoras.id_imagen=="NULL")||($scope.reaseguradoras.id_imagen=="")){
                mostrar_notificacion("Campos no validos","Debe seleccionar la imagen","warning");
                return false;
            }
            else{
                return true;
            }
        }
        /////////////////////////
        $scope.limpiar_cajas_reaseguradoras = function(){
            
            $scope.reaseguradoras = {
                        'id': '',
                        'idioma': '',
                        'id_idioma' : '',
                        'titulo' : '',
                        'url' : '',
                        'descripcion' : '',
                        'id_imagen' : '',
                        'imagen' : 'assets/images/logo_peque.png',
                        'orden':''
            }    

            $scope.activo_img = "inactivo";
            $("#div_descripcion").html($scope.titulo_text);
            $('#textarea_editor').data("wysihtml5").editor.clear();
            $("#idioma").removeAttr("disabled");
            $scope.titulo_registrar = "Registrar";
            $scope.subtitulo_pagina  = "Registrar reaseguradoras";
            $("#nuevo").css({"display":"none"})
        }
        //--
        $scope.consultarReaseguradorasIndividual = function(){
            uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
            reaseguradorasFactory.asignar_valores("",$scope.id_reaseguradoras,$scope.base_url)
            reaseguradorasFactory.cargar_reaseguradoras(function(data){
                
                $scope.reaseguradoras=data[0];
                                
                
                //alert($scope.reaseguradoras.orden)
                $scope.reaseguradoras=data[0];
                //--
                ordenFactory.asignar_valores($scope.reaseguradoras.id_idioma,$scope.base_url,"2")
                ordenFactory.cargar_orden_reaseguradoras(function(data){
                    $scope.ordenes=data;
                    //console.log($scope.ordenes)
                    //-Elimino el select
                    eliminarOptions("orden")
                    $.each( $scope.ordenes, function( indice, elemento ){
                          agregarOptions("#orden", elemento.orden, elemento.orden)
                    });
                });
                //--
                //console.log(data[0]);
                $("#div_descripcion").html($scope.reaseguradoras.descripcion)
                $scope.borrar_imagen.push($scope.reaseguradoras.id_imagen);
                $scope.activo_img = "activo"
                $scope.reaseguradoras.imagen = $scope.reaseguradoras.ruta
                $scope.titulo_registrar = "Modificar";
                $scope.subtitulo_pagina  = "Modificar reaseguradoras";
            
                setTimeout(function(){
                    $('#idioma > option[value="'+$scope.reaseguradoras.id_idioma+'"]').prop('selected', true);
                    $('#orden > option[value="'+$scope.reaseguradoras.orden+'"]').prop('selected', true);
                    $scope.reaseguradoras.inicial = $scope.reaseguradoras.orden;
                },500);
                
                //$("#idioma").attr("disabled");
                $("#idioma").prop('disabled', true);
            
                /*setTimeout(function(){
                    $('#idioma > option[value="'+$scope.reaseguradoras.id_idioma+'"]').prop('selected', true);
                    $('#direccion_slide > option[value="'+$scope.reaseguradoras.direccion+'"]').prop('selected', true);

                },300);*/
                //$("#idioma").attr("disabled");
                //$("#idioma").prop('disabled', true);
                desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
            });
        }
        //---
        ///////////////////////////////////////////////////
        $scope.consultar_idioma();
        $scope.consultar_galeria_img();

        $scope.id_reaseguradoras = $("#id_reaseguradoras").val();
            if($scope.id_reaseguradoras){
                $scope.consultarReaseguradorasIndividual();
            }else{
                $("#idioma").removeAttr("disabled");
            }
        //-----------------------------------------------------------------------
    })
    .controller("RedesSocialesController", function($scope,$http,$location,serverDataMensajes,sesionFactory,idiomaFactory,redesFactory){
        //Cuerpo declaraciones
        $(".li-menu").removeClass("active");
        $("#li_contactos").addClass("active");
        $(".a-menu").removeClass("active");
        $("#redes_sociales").addClass("active");    
        $scope.titulo_pagina = "Redes Sociales";
        $scope.subtitulo_pagina  = "Registrar redes sociales";
        $scope.activo_img = "inactivo";
        $scope.redes_sociales = {
                                'id':'',
                                'id_tipo_red':'',
                                'url':''
        }
        $scope.id_tipo_red = '';
        $scope.titulo_registrar = "Registrar";
        $scope.categorias_menu = "1";
        $scope.base_url = $("#base_url").val();
        //Cuerpo de metodos
        //---------------------------------------------------------------
        $scope.limpiar_cajas_categorias = function(){
            $scope.redes_sociales = {
                                'id':'',
                                'id_tipo_red':'',
                                'url':''
            }
            $("#redes").val("");
        }
        //--
        $scope.validar_form = function(){
            if($scope.id_tipo_red=="0"){
                mostrar_notificacion("Campos no validos","Debe seleccionar una red","warning");
                return false;
            }if($scope.redes_sociales.url_red==""){
                mostrar_notificacion("Campos no validos","Debe ingresar la url","warning");
                return false;
            }
            else{
                return true;
            }
        }
        //--
        /*$scope.consultarCategoriaIndividual = function(){
            redesFactory.asignar_valores("",$scope.id_categoria,$scope.base_url)
            redesFactory.cargar_categorias(function(data){
                $scope.categorias=data[0];
                $scope.titulo_registrar = "Modificar";
                $scope.subtitulo_pagina  = "Modificar categorías";
            });
        }*/
        //--
        $scope.consultarRedes = function(){
            redesFactory.asignar_valores("","",$scope.base_url)
            redesFactory.cargar_redes(function(data){
                $scope.tipo_red=data;
                //console.log($scope.tipo_red);
            });
        }
        //--
        $scope.consultarRedesUrl = function(){
            $scope.titulo_registrar = "Registrar";
            $scope.subtitulo_pagina = "Registrar Redes";
            redesFactory.asignar_valores("",$scope.id_tipo_red.id,$scope.base_url)
            redesFactory.cargar_redes_url(function(data){
                $scope.redes_sociales=data[0]
                //alert($scope.redes_sociales.url_red)
                if($scope.redes_sociales.url_red!=""){
                    $scope.titulo_registrar = "Modificar";
                    $scope.subtitulo_pagina = "Modificar Redes";
                }
                //console.log($scope.redes_sociales)
            });
        }
        //--
        $scope.registrarRedes = function(){

            uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

            if($scope.validar_form()==true){
                //Para guardar
                //alert("id_personas:"+$scope.doctor.id_personas);
                //alert("id_doctor:"+$scope.doctor.id);
                $scope.redes_sociales.id_tipo_red = $scope.id_tipo_red.id

                if(($scope.redes_sociales.id!=undefined)&&($scope.redes_sociales.id!="")){
                    $scope.modificar_redes();    
                }else{
                    $scope.insertar_redes();
                }        
            }

            //desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

        }
        //---
        $scope.insertar_redes = function(){
            $http.post($scope.base_url+"/RedesSociales/registrarRedes",
            {
                'id_tipo_red':$scope.redes_sociales.id_tipo_red,
                'url_red':$scope.redes_sociales.url_red,     

            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "registro_procesado"){
                    mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
                    $scope.limpiar_cajas_categorias();
                }
                else{
                    mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
                }
            }).error(function(data,estatus){
                console.log(data);
            });    
        }
        //---

        $scope.modificar_redes = function(){
            $http.post($scope.base_url+"/RedesSociales/modificarRedes",
            {
                'id_red':$scope.redes_sociales.id,    
                'id_tipo_red':$scope.redes_sociales.id_tipo_red,
                'url_red':$scope.redes_sociales.url_red,     
            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "modificacion_procesada"){
                    mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
                }else{
                    mostrar_notificacion("Mensaje","Ocurrió un error inesperado","warning");
                }
            }).error(function(data,estatus){
                console.log(data);
            });    
        }
        
        $(".tst1").click(function(){
          

     });
        //---------------------------------------------------------------
        //Cuerpo de Llamados a metodos
        //--Si tiene id categoria .... consulto los datos relacionados a ese id, esto es cuando proviene de una consulta
        $scope.id_categoria  = $("#id_categoria").val();
        if($scope.id_categoria){
            $scope.consultarCategoriaIndividual();
        }
        //---------------------------------------------------------
        $scope.consultarRedes()
    })
    .controller("RegistroCMS_Controller", function($scope,$http,$location,serverDataMensajes,sesionFactory,galeriaFactory,idiomaFactory,tipos_usuariosFactory,coloresFactory,consultaUsuarioFactory){

        $(".li-menu").removeClass("active");
        $("#li_configuracion").addClass("active");
        $(".a-menu").removeClass("active");
        $("#registro_cms").addClass("active");

        $scope.titulo_pagina = "Registrar Usuarios CMS";
        $scope.subtitulo_pagina = "Registro";
        $scope.titulo_registrar = "Registrar";
        $scope.titulo_cons = "Consultar";
        $scope.base_url = $("#base_url").val();
        $scope.titulo_text = "Pulse aquí para ingresar el contenido"
        $scope.activo_img = "inactivo";
        $scope.searchMarcas = []
        $scope.borrar_imagen = []
        $scope.currentTab = 'datos_basicos'

        $scope.persona = {
                        'id': '',
                        'cedula': '',
                        'login' : '',
                        'nombres_apellidos' : '',
                        'id_tipo_usuario' : '',
                        'tipo_usuario' : '',
                        'telefono' : '',
                        'telefono_confirmar' : '',
                        'email' : '',
                        'clave' : '',
                        'repite_clave' : '',
                        'id_imagen':'',
                        'imagen':'',
        }
        //console.log($scope.id_personas);

        $scope.consultar_tipo_usuarios = function(){
            tipos_usuariosFactory.asignar_valores("",$scope.base_url)
            tipos_usuariosFactory.cargar_tipos(function(data){
                $scope.tipo_user=data;
                //console.log($scope.idioma);
            });
        }
        ///////////////////////////////////////////////////////////////
        $scope.agregarWisi = function(){
            //$('#text_editor').data("wysihtml5").editor.clear();
            $scope.color.descripcion = $(".textarea_editor").val()
            $("#div_descripcion").html($scope.color.descripcion)
            $("#cerrarModal").click();
        }
        $scope.wisi_modal = function(){
            $("#wisiModal").modal("show")
            $(".textarea_editor").data("wysihtml5").editor.setValue($scope.color.descripcion)
        }
        $scope.consultar_galeria_img = function(){
            galeriaFactory.asignar_valores('25','',$scope.base_url);//ya que es galeria de imagenes
            galeriaFactory.cargar_galeria_fa(function(data){
                $scope.galery=data;
                //console.log($scope.galery);
            });
        }

        $scope.seleccione_img_principal = function(){
            $("#modal_img1").modal("show");
        }

        $scope.seleccionar_imagen = function(event){
            var imagen = event.target.id;//Para capturar id
            var vec = $("#"+imagen).attr("data");
            var vector_data = vec.split("|")
            var id_imagen = vector_data[0];
            var ruta = vector_data[1];

            $scope.borrar_imagen.push(id_imagen);
            $scope.activo_img = "activo"
            $scope.noticias.id_imagen = id_imagen
            $scope.noticias.imagen = ruta
            //--
            $("#modal_img1").modal("hide");
            //--
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        
        $scope.seleccione_img_principal = function(){
            $("#modal_img1").modal("show");
        }
        //PARA SELECCIONAR UNA IMAGEN Y MOSTRARLA EN EL CAMPO DE IMG
        $scope.seleccionar_imagen = function(event){
                var imagen = event.target.id;//Para capturar id
                //console.log(imagen);
                var vec = $("#"+imagen).attr("data");
                var vector_data = vec.split("|")
                var id_imagen = vector_data[0];
                var ruta = vector_data[1];

                $scope.borrar_imagen.push(id_imagen);
                $scope.activo_img = "activo"
                $scope.persona.id_imagen = id_imagen
                $scope.persona.imagen = ruta
                //alert($scope.color.id_imagen);
                //--
                $("#modal_img1").modal("hide");
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        $scope.limpiar_cajas_usuario = function(){
            $scope.persona = {
                'id': '',
                'cedula': '',
                'login' : '',
                'nombres_apellidos' : '',
                'id_tipo_usuario' : '',
                'tipo_usuario' : '',
                'telefono' : '',
                'email' : '',
                'clave' : '',
                'repite_clave' : '',
                'id_imagen':'',
                'imagen':'',
            }

            $scope.activo_img = "inactivo";
            $("#div_descripcion").html($scope.titulo_text);
            $('#textarea_editor').data("wysihtml5").editor.clear();
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        $scope.registrarUsuario = function(){
            uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
             if($scope.validar_form()==true){
                //Para guardar
                if(($scope.persona.id!="")&&($scope.persona.id!=undefined)){
                    $scope.modificar_personas();
                }else{
                    $scope.insertar_personas();
                }

            }
            //desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
         }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        $scope.validar_form = function(){
            //console.log($scope.color)
            if($scope.persona.cedula==""){
                mostrar_notificacion("Campos no validos","Debe ingresar la cedula","warning");
                return false;
            }else if($scope.persona.login==""){
                mostrar_notificacion("Campos no validos","Debe ingresar un nombre de usuario","warning");
                return false;
            }else if($scope.persona.nombres_apellidos==""){
                mostrar_notificacion("Campos no validos","Debe ingresar el nombre completo","warning");
                return false;
            }else if($scope.persona.id_tipo_usuario==""){
                mostrar_notificacion("Campos no validos","Debe seleccionar un tipo de usuario","warning");
                return false;
            }else if($scope.persona.email==""){
                mostrar_notificacion("Campos no validos","Debe ingresar un correo","warning");
                return false;
            }
            var exr = /^\w+[a-z_0-9\-\.]+@\w+[0-9a-z\-\.]+\.[a-z]{2,4}$/i;
            if(!(exr.test($scope.persona.email))){
                $scope.persona.email="";
                mostrar_notificacion("Campos no validos","Debe ingresar una dirección de correo v&aacute;lida: xxxxxxxxxxxxxx@host.com","warning");
            }else if($scope.persona.telefono==""){
                mostrar_notificacion("Campos no validos","Debe ingresar un numero teléfono","warning");
                return false;
            }else if($scope.persona.telefono_confirmar==""){
                mostrar_notificacion("Campos no validos","Debes confirmar el numero de teléfono","warning");
                return false;
            }else if($scope.persona.telefono!=$scope.persona.telefono_confirmar){
                mostrar_notificacion("Campos no validos","Los numeros de telefono no coinciden","warning");
                return false;
            }
            //------------ Si es registrar:---------------------------------
            else if(($scope.persona.clave=="")&&($scope.persona.id=="")){
                mostrar_notificacion("Campos no validos","Debe ingresar una contraseña","warning");
                return false;
            }else if(($scope.persona.repite_clave=="")&&($scope.persona.id=="")){
                mostrar_notificacion("Campos no validos","Debe repetir la contraseña","warning");
                return false;
            }else if(($scope.persona.repite_clave!=$scope.persona.clave)&&($scope.persona.id=="")){
                mostrar_notificacion("Campos no validos","Las contraseñas no coinciden","warning");
                return false;
            }
            //----------------------------------------------------------------
            else if(($scope.persona.id_imagen=="NULL")||($scope.persona.id_imagen=="")){
                mostrar_notificacion("Campos no validos","Debe seleccionar la imagen","warning");
                return false;
            }
            else{
                return true;
            }
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        $scope.insertar_personas = function(){
            $http.post($scope.base_url+"/RegistroCMS/registrarPersonas",
            {
                'id': $scope.persona.id,
                'cedula': $scope.persona.cedula,
                'login' : $scope.persona.login,
                'nombres_apellidos' : $scope.persona.nombres_apellidos,
                'id_tipo_usuario' : $scope.persona.id_tipo_usuario.id,
                'telefono' : $scope.persona.telefono,
                'email' : $scope.persona.email,
                'clave' : $scope.persona.clave,
                'repite_clave' : $scope.persona.repite_clave,
                'id_idioma'  : $scope.persona.id_idioma,
                'id_imagen':$scope.persona.id_imagen,
            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "registro_procesado"){
                    mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
                    $scope.limpiar_cajas_usuario();
                }else if($scope.mensajes.mensaje == "cedula"){
                    mostrar_notificacion("Mensaje","Ya existe un usuario con la misma cedula","warning");
                }else if($scope.mensajes.mensaje == "login"){
                    mostrar_notificacion("Mensaje","Nombre de usuario no disponible","warning");
                }else if($scope.mensajes.mensaje == "ambos"){
                    mostrar_notificacion("Mensaje","Cedula y nombre de usuario ya existente","warning");
                }
                else{
                    mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
                }
                $mensajes["mensaje"] = "no_registro";

            }).error(function(data,estatus){
                console.log(data);
            });
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        $scope.consultarPersonaIndividual = function(){
            //console.log($scope.id_personas);
            consultaUsuarioFactory.asignar_valores("",$scope.id_personas,$scope.base_url)
            consultaUsuarioFactory.cargar_personas(function(data){
                $scope.persona=data[0];
                //console.log(data[0]);
                $scope.borrar_imagen.push($scope.persona.id_imagen);
                $scope.activo_img = "activo"
                //-Cambio 10022020
                
                $scope.persona.imagen = $scope.persona.ruta
                $scope.titulo_pagina = "Modificar Usuario CMS";
                $scope.subtitulo_pagina  = "Modificar";
                $scope.titulo_registrar = "Modificar";
                setTimeout(function(){
                    $('#tipo_usuario > option[value="'+$scope.persona.id_tipo_usuario+'"]').prop('selected', true);
                },300);
                $("#tipo_usuario").prop('disabled', true);
                //$("#clave").prop('disabled', true);
                //$("#repite_clave").prop('disabled', true);
            });
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        $scope.modificar_personas = function(){
            $http.post($scope.base_url+"/RegistroCMS/modificarUsuario",
            {
                'id': $scope.persona.id,
                'cedula': $scope.persona.cedula,
                'login' : $scope.persona.login,
                'nombres_apellidos' : $scope.persona.nombres_apellidos,
                'id_tipo_usuario' : $scope.persona.id_tipo_usuario.id,
                'telefono' : $scope.persona.telefono,
                'email' : $scope.persona.email,
                'clave' : $scope.persona.clave,
                'repite_clave' : $scope.persona.repite_clave,
                'id_idioma':$scope.persona.id_idioma,
                'id_imagen':$scope.persona.id_imagen,
            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "modificacion_procesada"){
                    mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
                    //$scope.limpiar_cajas_usuario();
                }else if($scope.mensajes.mensaje == "cedula"){
                    mostrar_notificacion("Mensaje","Ya existe un usuario con la misma cedula","warning");
                }else if($scope.mensajes.mensaje == "login"){
                    mostrar_notificacion("Mensaje","Nombre de usuario no disponible","warning");
                }else if($scope.mensajes.mensaje == "ambos"){
                    mostrar_notificacion("Mensaje","Cedula y nombre de usuario ya existente","warning");
                }
                else{
                    mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
                }
                $mensajes["mensaje"] = "no_registro";

            }).error(function(data,estatus){
                //console.log(data);
            });
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        $scope.consultar_tipo_usuarios();
        $scope.consultar_galeria_img();
        $scope.id_personas  = $("#id_personas").val();
        if($scope.id_personas){
            

            $scope.consultarPersonaIndividual();
        }
    })
    .controller("registroCMSConsultasController", function($scope,$http,$location,serverDataMensajes,sesionFactory,idiomaFactory,coloresFactory,consultaUsuarioFactory){
        $(".li-menu").removeClass("active");
        $("#li_configuracion").addClass("active");
        $(".a-menu").removeClass("active");
        $("#registro_cms").addClass("active");

        $scope.titulo_pagina = "Consulta de Usuarios del CMS";
        $scope.activo_img = "inactivo";

        $scope.color = {
                        'id': '',
                        'idioma': '',
                        'id_idioma' : '',
                        'titulo' : '',
                        'descripcion' : '',
                        'id_imagen' : '',
                        'imagen' : ''
        }
        $scope.id_personas = ""
        $scope.categorias_menu = "2";
        $scope.id_marca = "";
        $scope.base_url = $("#base_url").val();

        $scope.iniciar_datatable = function(){
            //--Iniciar DataTable
            $('#myTable').DataTable({
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                    "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
                "order": [[ 0, "desc" ]]

            });
            //console.log($scope.personas);
        }
        //////////////////////////////////////////////////////////////////////////////////////
        $scope.consultarUsuariosTodos = function(){
            consultaUsuarioFactory.asignar_valores("","",$scope.base_url)
            consultaUsuarioFactory.cargar_personas(function(data){
                $scope.personas=data;
                //console.log($scope.personas);
            });
        }
        //////////////////////////////////////////////////////////////////////////////////////
        $scope.ver_personas = function(valor){
            var id = $("#ver"+valor).attr("data");
            $scope.id_personas = id;
            //console.log($scope.id_personas);

            $("#id_personas").val($scope.id_personas)
            let form = document.getElementById('formConsultaPersonas');
            form.action = "./consulta_usuariosCMS/modificar";
            form.submit();
        }
        //////////////////////////////////////////////////////////////////////////////////////
        $scope.activar_registro = function(event){
            $scope.id_seleccionado_colores = []
            $scope.estatus_seleccionado_colores = []
            var caja = event.currentTarget.id;
            var atributos = $("#"+caja).attr("data");
            var arreglo_atributos = atributos.split("|");
            $scope.id_seleccionado_colores = arreglo_atributos[0];
            $scope.estatus_seleccionado_colores = arreglo_atributos[1];
            $("#cabecera_mensaje").text("Información:");
            //--
            if ($scope.estatus_seleccionado_colores==0){
                mensaje = "Desea modificar el estatus de este registro a publicado? ";
                $scope.estatus_seleccionado_colores=1;
            }else{
                mensaje = "Desea modificar el estatus de este registro a inactivo? ";
                $scope.estatus_seleccionado_colores=0
            }
            //($scope.estatus_seleccionado_categorias==1) ? $scope.estatus_seleccionado_categorias=0:$scope.estatus_seleccionado_categorias=1;

            //--
            $scope.modificar_estatus("activar_inactivar",mensaje)
        }
        //----------------------------------------------------------------
        $scope.modificar_estatus = function(opcion,mensaje){
             swal({
                  title: 'Esta seguro?',
                  text: mensaje,
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Si!',
                  cancelButtonText: 'No',
                }).then((result) => {
                      if (result.value) {
                        $scope.accion_estatus()
                      }
                })
        }
        //----------------------------------------------------------------
        $scope.accion_estatus = function(){

            $http.post($scope.base_url+"/RegistroCMS/modificarUsuarioEstatus",
            {
                 'id':$scope.id_seleccionado_colores,
                 'estatus':$scope.estatus_seleccionado_colores,

            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "modificacion_procesada"){
                    Swal(
                          'Realizado!',
                          'El proceso fue ejecutado.',
                          'success'
                    ).then((result) => {
                          if (result.value) {
                                let form = document.getElementById('formConsultaPersonas');
                                form.action = "./consulta_usuariosCMS";
                                form.submit();
                          }

                    });
                }else{
                    Swal(
                          'No realizado!',
                          'El proceso no pudo ser ejecutado.',
                          'warning'
                    )
                }
            }).error(function(data,estatus){
                console.log(data);
            });
        }
        //////////////////////////////////////////////////////////////////////////////////////
        $scope.eliminar_registro = function(event){
            $scope.id_seleccionado_colores = []
            $scope.estatus_seleccionado_colores = []
            var caja = event.currentTarget.id;
            //alert(caja);
            var atributos = $("#"+caja).attr("data");
            var arreglo_atributos = atributos.split("|");
            $scope.id_seleccionado_colores = arreglo_atributos[0];
            $scope.estatus_seleccionado_colores = arreglo_atributos[1];
            //console.log($scope.estatus_seleccionado_colores);
            $("#cabecera_mensaje").text("Información:");
            if ($scope.estatus_seleccionado_colores==2){
                mensaje = "Desea eliminar este registro? ";
            }
            $scope.modificar_estatus("eliminar",mensaje)
        }
        //////////////////////////////////////////////////////////////////////////////////////
        setTimeout(function(){
                $scope.iniciar_datatable();
        },500);
        $scope.consultarUsuariosTodos();
    })
    .controller("serviciosConsultaController", function($scope,$http,$location,serverDataMensajes,categoriasFactory,sesionFactory,idiomaFactory,serviciosFactory){
        $(".li-menu").removeClass("active");
        $("#li_servicios1").addClass("active");    
        $(".a-menu").removeClass("active");
        $("#servicio").addClass("active");    
        $scope.titulo_pagina = "Consulta de Servicios";
        $scope.activo_img = "inactivo";
        $scope.productos = {
                            'id': '',
                            'idioma': '',
                            'id_idioma' : '',
                            'somos': '',
                            'digital_agency': '',
                            'estatus' : '',
                            'id_imagen' : '',
                            'imagen' : ''
        }
        $scope.nosotros_menu = "1";
        $scope.id_nosotros = "";
        $scope.base_url = $("#base_url").val();

        $scope.iniciar_datatable = function(){
            //--Iniciar DataTable
            $('#myTable').DataTable({
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                    "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
            });
            //console.log($scope.nosotros);
        }

        $scope.consultar_productos= function(){
            serviciosFactory.asignar_valores("","",$scope.base_url)
            serviciosFactory.cargar_servicios(function(data){
                $scope.servicios = data;
                //console.log($scope.servicios);
            });
        }

        $scope.ver_producto = function(valor){
            var id = $("#ver"+valor).attr("data");
            $scope.id_servicios = id;
            //console.log($scope.id_servicios);
            $("#id_servicios").val($scope.id_servicios);
            let form = document.getElementById('formConsultaProductos');
            form.action = "./serviciosVer";
            form.submit();
        }

        $scope.activar_registro = function(event){
                    $scope.id_seleccionado_servicios = []
                    $scope.estatus_seleccionado_servicios = []

                    var caja = event.currentTarget.id;
                    var atributos = $("#"+caja).attr("data");
                    var arreglo_atributos = atributos.split("|");

                    $scope.id_seleccionado_servicios = arreglo_atributos[0];
                    $scope.estatus_seleccionado_servicios = arreglo_atributos[1];

                    $("#cabecera_mensaje").text("Información:");

                    if ($scope.estatus_seleccionado_servicios==0){
                        mensaje = "Desea modificar el estatus de este registro a publicado? ";
                        $scope.estatus_seleccionado_servicios=1;
                    }else{
                        mensaje = "Desea modificar el estatus de este registro a inactivo? ";
                        $scope.estatus_seleccionado_servicios=0
                    }
                    $scope.modificar_estatus("activar_inactivar",mensaje)
                }
                //----------------------------------------------------------------
                $scope.modificar_estatus = function(opcion,mensaje){
                     swal({
                          title: 'Esta seguro?',
                          text: mensaje,
                          type: 'warning',
                          showCancelButton: true,
                          confirmButtonColor: '#3085d6',
                          cancelButtonColor: '#d33',
                          confirmButtonText: 'Si!',
                          cancelButtonText: 'No',
                        }).then((result) => {
                              if (result.value) {
                                  $scope.accion_estatus()
                              }
                        })
                }
                //----------------------------------------------------------------
                $scope.accion_estatus = function(){

                    $http.post($scope.base_url+"/Servicios/modificarServiciosEstatus",
                    {
                         'id':$scope.id_seleccionado_servicios,
                         'estatus':$scope.estatus_seleccionado_servicios,

                    }).success(function(data, estatus, headers, config){
                        $scope.mensajes  = data;
                        
                        if($scope.mensajes.mensaje == "modificacion_procesada"){
                            Swal(
                                  'Realizado!',
                                  'El proceso fue ejecutado.',
                                  'success'
                            ).then((result) => {
                                  if (result.value) {
                                        let form = document.getElementById('formConsultaProductos');
                                        form.action = "./consultar_servicios";
                                        form.submit();
                                  }

                            });
                        }else{
                            Swal(
                                  'No realizado!',
                                  'El proceso no pudo ser ejecutado.',
                                  'warning'
                            )
                        }
                    }).error(function(data,estatus){
                        console.log(data);
                    });
                }
                //----------------------------------------------------------------
                //----------------------------------------------------------------
        $scope.eliminar_registro = function(event){
            $scope.id_seleccionado_servicios = []
            $scope.estatus_seleccionado_servicios = []
            var caja = event.currentTarget.id;
            //alert(caja);
            var atributos = $("#"+caja).attr("data");
            var arreglo_atributos = atributos.split("|");
            $scope.id_seleccionado_servicios = arreglo_atributos[0];
            $scope.estatus_seleccionado_servicios = arreglo_atributos[1];
            //console.log($scope.estatus_seleccionado_servicios);
            $("#cabecera_mensaje").text("Información:");
            if ($scope.estatus_seleccionado_servicios==2){
                mensaje = "Desea eliminar este registro? ";
            }
            $scope.modificar_estatus("eliminar",mensaje)
        }
        //----------------------------------------------------------------

        setTimeout(function(){
                        $scope.iniciar_datatable();
                },500);

        $scope.consultar_productos
        ();
    })
    .controller("serviciosController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,ordenFactory ,idiomaFactory,serviciosFactory){
        //Cuerpo declaraciones
        $(".li-menu").removeClass("active");
        $("#li_servicios1").addClass("active");    
        $(".a-menu").removeClass("active");
        $("#servicio").addClass("active");    
        $scope.titulo_pagina = "Servicios";
        $scope.subtitulo_pagina  = "Registrar Servicios";
        $scope.activo_img = "inactivo";
        $scope.servicios = {
                                'id':'',
                                'id_idioma':'',
                                'titulo':'',
                                'descripcion':'',
                                'estatus':'',
                                'id_imagen':'',
                                'imagen':''
        }
        $scope.id_servicios = ""
        $scope.searchProductos = []
        $scope.borrar_imagen = []
        $scope.currentTab = 'datos_basicos'
        $scope.titulo_registrar = "Registrar";
        $scope.base_url = $("#base_url").val();
        $scope.titulo_text = "Pulse aquí para ingresar el contenido del servicio"


        //--------------------------------------------------------
        //Cuerpo de metodos
        $scope.consultar_idioma = function(){
            idiomaFactory.asignar_valores("",$scope.base_url)
            idiomaFactory.cargar_idioma(function(data){
                $scope.idioma=data;
                $.each( $scope.idioma, function( indice, elemento ){
                      agregarOptions("#idioma", elemento.id, elemento.descripcion)
                });
                $('#idioma > option[value="0"]').prop('selected', true);

                //console.log($scope.idioma);
            });
        }
        $scope.agregarWisi = function(){
            //$('#text_editor').data("wysihtml5").editor.clear();
            $scope.servicios.descripcion = $(".textarea_editor").val()
            $("#div_descripcion").html($scope.servicios.descripcion)
            $("#cerrarModal").click();
        }
        $scope.wisi_modal = function(){
            $("#wisiModal").modal("show")
            $(".textarea_editor").data("wysihtml5").editor.setValue($scope.servicios.descripcion)
        }
        //--
        $scope.consultar_galeria_img = function(){
            galeriaFactory.asignar_valores('7','',$scope.base_url);//ya que es galeria de imagenes
            galeriaFactory.cargar_galeria_fa(function(data){
                $scope.galery=data;
                //console.log($scope.galery);
            });
        }

        $scope.seleccione_img_principal = function(){
            $("#modal_img1").modal("show");
        }

        $scope.seleccionar_imagen = function(event){
            var imagen = event.target.id;//Para capturar id
            var vec = $("#"+imagen).attr("data");
            var vector_data = vec.split("|")
            var id_imagen = vector_data[0];
            var ruta = vector_data[1];
            //console.log(vector_data[1]);

            $scope.borrar_imagen.push(id_imagen);
            $scope.activo_img = "activo"
            $scope.servicios.id_imagen = id_imagen
            $scope.servicios.imagen = ruta
            //--
            $("#modal_img1").modal("hide");
            //--
        }

        $scope.registrarproductos = function(){
            
            uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
            $scope.servicios.orden = $("#orden").val();

            if($scope.validar_form()==true){
                //Para guardar
                if(($scope.servicios.id!="")&&($scope.servicios.id!=undefined)){
                    $scope.modificar_productos();
                }else{
                    $scope.insertar_productos();
                }

            }

            //desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

        }

        $scope.insertar_productos = function(){
            $http.post($scope.base_url+"/Servicios/registrarServicios",
            {
                'id'         : $scope.servicios.id,
                'titulo'     : $scope.servicios.titulo,
                'id_imagen'  : $scope.servicios.id_imagen,
                'descripcion': $scope.servicios.descripcion,
                'id_idioma'  : $scope.servicios.id_idioma.id,
                'orden': $scope.servicios.orden,

            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "registro_procesado"){
                    mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
                    $scope.limpiar_cajas_productos();
                }else if($scope.mensajes.mensaje == "existe"){
                    mostrar_notificacion("Mensaje","Ya existe un servicio con ese titulo","warning");
                }
                else{
                    mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
                }
            }).error(function(data,estatus){
                console.log(data);
            });
        }

        $scope.modificar_productos = function(){
            $http.post($scope.base_url+"/Servicios/modificarServicios",
            {
                'id':$scope.servicios.id,
                'titulo': $scope.servicios.titulo,
                'id_imagen':$scope.servicios.id_imagen,
                'descripcion':$scope.servicios.descripcion,
                'id_idioma':$scope.servicios.id_idioma,
                'orden': $scope.servicios.orden,
                'inicial': $scope.servicios.inicial,

            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "registro_procesado"){
                    mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
                    $scope.servicios.inicial = $scope.servicios.orden;
                }else if($scope.mensajes.mensaje == "existe"){
                    mostrar_notificacion("Mensaje","Ya existe una noticia con ese titulo","warning");
                }
                else{
                    mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
                }
            }).error(function(data,estatus){
                console.log(data);
            });
        }

        $scope.validar_form = function(){
            //console.log($scope.servicios)
            if(($scope.servicios.id_idioma=="NULL")||($scope.servicios.id_idioma=="")){
                mostrar_notificacion("Campos no validos","Debe seleccionar el idioma","warning");
                return false;
            } else if($scope.servicios.titulo==""){
                mostrar_notificacion("Campos no validos","Debe ingresar el titulo","warning");
                return false;
            }else if(($scope.servicios.orden=="NULL")||($scope.servicios.orden=="")){
                mostrar_notificacion("Campos no validos","Debe seleccionar el orden del servicio","warning");
                return false;
            }else if(($scope.servicios.id_imagen=="NULL")||($scope.servicios.id_imagen=="")){
                mostrar_notificacion("Campos no validos","Debe seleccionar la imagen","warning");
                return false;
            }
            else{
                return true;
            }
        }

        $scope.limpiar_cajas_productos = function(){
            $scope.servicios = {
                                'id':'',
                                'id_idioma':'',
                                'titulo':'',
                                'descripcion':'',
                                'estatus':'',
                                'id_imagen':''
            }
            $scope.activo_img = "inactivo";

            $('#text_editor').data("wysihtml5").editor.clear();
            $("#div_descripcion").html($scope.titulo_text);
            eliminarOptions("orden")
            $('#orden > option[value=""]').prop('selected', true);
        }
        //--
        $scope.consultarProductoIndividual = function(){

            uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

            serviciosFactory.asignar_valores("",$scope.id_servicios,$scope.base_url)
            serviciosFactory.cargar_servicios(function(data){
                $scope.servicios=data[0];
                //console.log(data[0]);
                $("#div_descripcion").html($scope.servicios.descripcion)
                $scope.borrar_imagen.push($scope.servicios.id_imagen);
                $scope.activo_img = "activo"
            ordenFactory.asignar_valores($scope.servicios.id_idioma,$scope.base_url,"2")
            ordenFactory.cargar_orden_servicios(function(data){
                $scope.ordenes=data;
                //console.log($scope.ordenes)
                //-Elimino el select
                eliminarOptions("orden")
                $.each( $scope.ordenes, function( indice, elemento ){
                    agregarOptions("#orden", elemento.orden, elemento.orden)
                });
            });
                //-Cambio 10022020

                $scope.servicios.imagen = $scope.servicios.ruta
                $scope.titulo_registrar = "Modificar";
                $scope.subtitulo_pagina  = "Modificar servicios";
                setTimeout(function(){
                    $('#idioma > option[value="'+$scope.servicios.id_idioma+'"]').prop('selected', true);
                    $('#orden > option[value="'+$scope.servicios.orden+'"]').prop('selected', true);
                    $scope.servicios.inicial = $scope.servicios.orden;
                },300);
                $("#idioma").prop('disabled', true);
            });

            desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

        }
        //---
        $scope.cargarOrden = function(){
            ordenFactory.asignar_valores($scope.servicios.id_idioma.id,$scope.base_url,"1")
            ordenFactory.cargar_orden_servicios(function(data){
                $scope.ordenes=data;
                //-Elimino el select
                eliminarOptions("orden")
                $.each( $scope.ordenes, function( indice, elemento ){
                      agregarOptions("#orden", elemento.orden, elemento.orden)
                });
                $('#orden > option[value=""]').prop('selected', true);

            });
        }
        //--------------------------------------------------------
        //Cuerpo de llamados
        $scope.consultar_idioma();
        $scope.consultar_galeria_img();
        //--Si tiene id categoria .... consulto los datos relacionados a ese id, esto es cuando proviene de una consulta
        $scope.id_servicios  = $("#id_servicios").val();
        //console.log($scope.id_servicios);

        if($scope.id_servicios){
            $scope.consultarProductoIndividual();
        }
        //--------------------------------------------------------

    })
    .controller("sliderConsultasController", function($scope,$http,$location,serverDataMensajes,sliderFactory,sesionFactory,idiomaFactory){
        //Cuerpo declaraciones
        $(".li-menu").removeClass("active");
        $("#li_slider").addClass("active");    
        $(".a-menu").removeClass("active");
        $("#slider").addClass("active");    
        $scope.titulo_pagina = "Consulta de Slider";
        $scope.activo_img = "inactivo";
        $scope.slider = {
                            'id': '',
                            'idioma': '',
                            'id_idioma' : '',
                            'titulo' : '',
                            'descripcion' : '',
                            'boton' : '',
                            'url' : '',
                            'estatus' : '',
                            'id_imagen' : '',
                            'imagen' : ''
        }
        $scope.categorias_menu = "1";
        $scope.id_slider = "";
        $scope.base_url = $("#base_url").val();
        //-----------------------------------------------------
        //--Cuerpo de metodos  --/
        $scope.iniciar_datatable = function(){
            //--Iniciar DataTable
            $('#myTable').DataTable({
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                    "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
            });
            //console.log($scope.categorias);
        }
        $scope.consultar_slider = function(){
            sliderFactory.asignar_valores("","",$scope.base_url)
            sliderFactory.cargar_slider(function(data){
                $scope.slider=data;
                //console.log($scope.slider);                
            });
        }
        //---------------------------------------------------------------
        $scope.ver_slider = function(valor){
            var id = $("#ver"+valor).attr("data");
            $scope.id_slider = id;    
            $("#id_slider").val($scope.id_slider)
            let form = document.getElementById('formConsultaSlider');
            form.action = "./sliderVer";
            form.submit();
        }
        //----------------------------------------------------------------
        $scope.activar_registro = function(event){
            $scope.id_seleccionado_slider = []
            $scope.estatus_seleccionado_slider = []
            var caja = event.currentTarget.id;
            var atributos = $("#"+caja).attr("data");
            var arreglo_atributos = atributos.split("|");
            $scope.id_seleccionado_slider = arreglo_atributos[0];
            $scope.estatus_seleccionado_slider = arreglo_atributos[1];
            $("#cabecera_mensaje").text("Información:");            
            //--
            if ($scope.estatus_seleccionado_slider==0){
                mensaje = "Desea modificar el estatus de este registro a publicado? ";
                $scope.estatus_seleccionado_slider=1;
            }else{
                mensaje = "Desea modificar el estatus de este registro a inactivo? ";
                $scope.estatus_seleccionado_slider=0
            }
            //($scope.estatus_seleccionado_categorias==1) ? $scope.estatus_seleccionado_categorias=0:$scope.estatus_seleccionado_categorias=1;

            //--
            $scope.modificar_estatus("activar_inactivar",mensaje)
        }
        //----------------------------------------------------------------
        $scope.modificar_estatus = function(opcion,mensaje){
             swal({
                  title: 'Esta seguro?',
                  text: mensaje,
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Si!',
                  cancelButtonText: 'No',
                }).then((result) => {
                      if (result.value) {
                          $scope.accion_estatus()
                      }
                })
        }
        //----------------------------------------------------------------
        $scope.accion_estatus = function(){

            $http.post($scope.base_url+"/Slider/modificarSliderEstatus",
            {
                 'id':$scope.id_seleccionado_slider,    
                 'estatus':$scope.estatus_seleccionado_slider,     

            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                //console.log($scope.mensajes)
                if($scope.mensajes.mensaje == "modificacion_procesada"){
                    Swal(
                          'Realizado!',
                          'El proceso fue ejecutado.',
                          'success'
                    ).then((result) => {
                          if (result.value) {
                                let form = document.getElementById('formConsultaSlider');
                                form.action = "./consultar_slider";
                                form.submit();
                          }
                    
                    });
                }else{
                    Swal(
                          'No realizado!',
                          'El proceso no pudo ser ejecutado.',
                          'warning'
                    )
                }
            }).error(function(data,estatus){
                console.log(data);
            });    
        }
        //----------------------------------------------------------------
        $scope.eliminar_registro = function(event){
            $scope.id_seleccionado_slider = []
            $scope.estatus_seleccionado_slider = []
            var caja = event.currentTarget.id;
            //alert(caja);
            var atributos = $("#"+caja).attr("data");
            var arreglo_atributos = atributos.split("|");
            $scope.id_seleccionado_slider = arreglo_atributos[0];
            $scope.estatus_seleccionado_slider = arreglo_atributos[1];
            //console.log($scope.estatus_seleccionado_slider);
            $("#cabecera_mensaje").text("Información:");
            if ($scope.estatus_seleccionado_slider==2){
                mensaje = "Desea eliminar este registro? ";
            }
            $scope.modificar_estatus("eliminar",mensaje)
        }
        //----------------------------------------------------------------
        //-- Cuerpo de funciones--/
        setTimeout(function(){
                $scope.iniciar_datatable();
        },500);
        $scope.consultar_slider();
        
        //-----------------------------------------------------
    })
    .controller("SliderController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,sesionFactory,idiomaFactory,sliderFactory,ordenFactory){
        //---------------------------------------------------------------------
        //Cuerpo declaraciones
        $(".li-menu").removeClass("active");
        $("#li_slider").addClass("active");
        $(".a-menu").removeClass("active");
        $("#slider").addClass("active");
        $scope.titulo_pagina = "Slider";
        $scope.subtitulo_pagina  = "Registrar Slider";
        $scope.activo_img = "inactivo";

        $scope.slider = {
                        'id': '',
                        'idioma': '',
                        'id_idioma' : '',
                        'titulo' : '',
                        'descripcion' : '',
                        'boton' : '',
                        'url' : '',
                        'estatus' : '',
                        'id_imagen' : '',
                        'imagen' : 'assets/images/logo_peque.png',
                        'direccion':'',
                        'vertical':'',
                        'orden':''
        }
        
        
        $scope.titulo_registrar = "Registrar";
        $scope.titulo_cons = "Consultar";
        //$scope.searchSlider = []
        $scope.borrar_imagen = []
        $scope.currentTab = 'datos_basicos'
        $scope.titulo_text = "Pulse aquí para ingresar la descripción del slider"

        $scope.opcion = ''
        $scope.seccion = ''
        $scope.base_url = $("#base_url").val();
        
        //alert($scope.base_url);
        //Cuerpo de metodos
        $scope.consultar_idioma = function(){
            idiomaFactory.asignar_valores("",$scope.base_url)
            idiomaFactory.cargar_idioma(function(data){
                $scope.idioma=data;

                $.each( $scope.idioma, function( indice, elemento ){
                      agregarOptions("#idioma", elemento.id, elemento.descripcion)
                });
                $('#idioma > option[value="'+$scope.slider.id_idioma+'"]').prop('selected', true);
            });
        }
        //---------------------------------
        //
        $scope.cargarOrden = function(){
            ordenFactory.asignar_valores($scope.slider.id_idioma,$scope.base_url,"1")
            ordenFactory.cargar_orden(function(data){
                $scope.ordenes=data;
                //console.log($scope.ordenes);
                //-Elimino el select
                eliminarOptions("orden")
                $.each( $scope.ordenes, function( indice, elemento ){
                      agregarOptions("#orden", elemento.orden, elemento.orden)
                });
                $('#orden > option[value=""]').prop('selected', true);

            });
        }
        //WISIMODAL
        $scope.agregarWisi = function(){
            //$('#text_editor').data("wysihtml5").editor.clear();
            $scope.slider.descripcion = $(".textarea_editor").val()
            $("#div_descripcion").html($scope.slider.descripcion)
            $("#cerrarModal").click();
        }
        //--
        
        $scope.wisi_modal = function(){
            $("#wisiModal").modal("show")
            $(".textarea_editor").data("wysihtml5").editor.setValue($scope.slider.descripcion)
        }
        //--
        $scope.agregar_contenido = function(){
            if ($scope.slider==undefined) {
                    $scope.slider = {
                                    'id': '',
                                    'idioma': '',
                                    'id_idioma' : '',
                                    'titulo' : '',
                                    'descripcion' : '',
                                    'boton' : '',
                                    'url' : '',
                                    'estatus' : '',
                                    'id_imagen' : '',
                                    'imagen' : 'assets/images/logo_peque.png',
                                    'direccion':'',
                                    'vertical':'',
                                    'orden':'',
                    }
            }

        }
        //------------------------------------------CONSULTA DE IMAGEN SEGUN ID DE CATEGORIA
        $scope.consultar_galeria_img = function(){
            galeriaFactory.asignar_valores('1','',$scope.base_url);//ya que es galeria de imagenes
            galeriaFactory.cargar_galeria_fa(function(data){
                $scope.galery=data;
                //console.log($scope.galery);
            });
        }
        //MODAL DE IMG
        $scope.seleccione_img_principal = function(){
            $("#modal_img1").modal("show");
        }
        //PARA SELECCIONAR UNA IMAGEN Y MOSTRARLA EN EL CAMPO DE IMG
        $scope.seleccionar_imagen = function(event){
                var imagen = event.target.id;//Para capturar id
                //console.log(imagen);
                var vec = $("#"+imagen).attr("data");
                var vector_data = vec.split("|")
                var id_imagen = vector_data[0];
                var ruta = vector_data[1];

                $scope.borrar_imagen.push(id_imagen);
                $scope.activo_img = "activo"
                $scope.slider.id_imagen = id_imagen
                $scope.slider.imagen = ruta
                //alert($scope.slider.id_imagen);
                //--
                $("#modal_img1").modal("hide");
                //--
        }
        /////////////////////////////////////////////////////////////////////////////////////
        $scope.registrarSlider = function(){
            $scope.slider.orden = $("#orden").val();
            uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
            if($scope.validar_form()==true){
                //Para guardar
                if(($scope.slider.id!="")&&($scope.slider.id!=undefined)){
                    $scope.modificar_slider();
                }else{
                    $scope.insertar_slider();
                }
            }
            //desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

        }
        ///////////////////////////////////////////////////////////////////////////////////
        $scope.insertar_slider = function(){
            $http.post($scope.base_url+"/Slider/registrarSlider",
            {
                'id'          : $scope.slider.id,
                'id_idioma'  : $scope.slider.id_idioma,
                'titulo'     : $scope.slider.titulo,
                'descripcion': $scope.slider.descripcion,
                'id_imagen'  : $scope.slider.id_imagen,
                'boton'      : $scope.slider.boton,
                'url'        : $scope.slider.url,
                'id_imagen'  :$scope.slider.id_imagen,
                'direccion' : $scope.slider.direccion,
                'vertical' : $scope.slider.vertical,
                'orden':  $scope.slider.orden
            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "registro_procesado"){
                    mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
                    $scope.limpiar_cajas_slider();
                }else if($scope.mensajes.mensaje == "existe"){
                    mostrar_notificacion("Mensaje","Ya existe una noticia con ese titulo","warning");
                }
                else{
                    mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
                }

            }).error(function(data,estatus){
                console.log(data);
            });
        }
        //////////////////////////
        $scope.modificar_slider = function(){
            $scope.slider.orden = $("#orden").val();
            $http.post($scope.base_url+"/Slider/modificarSlider",
            {
                'id'          : $scope.slider.id,
                'id_idioma'  : $scope.slider.id_idioma,
                'titulo'     : $scope.slider.titulo,
                'descripcion': $scope.slider.descripcion,
                'id_imagen'  : $scope.slider.id_imagen,
                'boton'      : $scope.slider.boton,
                'url'        : $scope.slider.url,
                'direccion' : $scope.slider.direccion,
                'vertical' : $scope.slider.vertical,
                'orden'     : $scope.slider.orden,
                'inicial'    : $scope.slider.inicial
            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "modificacion_procesada"){
                    mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
                    //$scope.limpiar_cajas_slider();
                    $scope.slider.inicial = $scope.slider.orden
                    desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

                }else{
                    mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
                    desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

                }
                //desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

            }).error(function(data,estatus){
                console.log(data);
            });
        }
        //////////////////////////
        $scope.validar_form = function(){
            //console.log($scope.slider)
            alert($scope.slider.orden);
            if(($scope.slider.id_idioma=="")||($scope.slider.id_idioma=="")){
                mostrar_notificacion("Campos no validos","Debe seleccionar el idioma","warning");
                return false;
            }else if(($scope.slider.direccion=="")||($scope.slider.direccion=="")){
                mostrar_notificacion("Campos no validos","Debe seleccionar la dirección del texto del slide","warning");
                return false;
            } else if($scope.slider.titulo==""){
                mostrar_notificacion("Campos no validos","Debe ingresar el titulo","warning");
                return false;
            }
            else if(($scope.slider.orden=="NULL")||($scope.slider.orden=="")){
                mostrar_notificacion("Campos no validos","Debe seleccionar el orden","warning");
                return false;
            }
            /*else if($scope.slider.descripcion==""){
                mostrar_notificacion("Campos no validos","Debe ingresar la descripción","warning");
                return false;
            }else if($scope.slider.boton==""){
                mostrar_notificacion("Campos no validos","Debe ingresar la descripción para el Botón","warning");
                return false;
            }else if($scope.slider.url==""){
                mostrar_notificacion("Campos no validos","Debe ingresar la descripción para la URL","warning");
                return false;
            }*/else if(($scope.slider.id_imagen=="NULL")||($scope.slider.id_imagen=="")){
                mostrar_notificacion("Campos no validos","Debe seleccionar la imagen","warning");
                return false;
            }
            else{
                return true;
            }
        }
        /////////////////////////
        $scope.limpiar_cajas_slider = function(){
            $scope.slider = {
                            'id': '',
                            'idioma': '',
                            'id_idioma' : '',
                            'titulo' : '',
                            'descripcion' : '',
                            'boton' : '',
                            'url' : '',
                            'estatus' : '',
                            'id_imagen' : '',
                            'imagen' : 'assets/images/logo_peque.png',
                            'direccion':'',
                            'vertical':'',
            }

            $scope.activo_img = "inactivo";
            $("#div_descripcion").html($scope.titulo_text);
            $('#textarea_editor').data("wysihtml5").editor.clear();
            $("#idioma").removeAttr("disabled");
            $scope.titulo_registrar = "Registrar";
            $scope.subtitulo_pagina  = "Registrar slider";
            $("#nuevo").css({"display":"none"})
        }
        //--
        $scope.consultarSliderIndividual = function(){
            uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
            sliderFactory.asignar_valores("",$scope.id_slider,$scope.base_url)
            sliderFactory.cargar_slider(function(data){
                
                $scope.slider=data[0];
                                
                
                //alert($scope.slider.orden)
                $scope.slider=data[0];
                //--
                ordenFactory.asignar_valores($scope.slider.id_idioma,$scope.base_url,"2")
                ordenFactory.cargar_orden(function(data){
                    $scope.ordenes=data;
                    //console.log($scope.ordenes)
                    //-Elimino el select
                    eliminarOptions("orden")
                    $.each( $scope.ordenes, function( indice, elemento ){
                          agregarOptions("#orden", elemento.orden, elemento.orden)
                    });
                });
                //--
                //console.log(data[0]);
                $("#div_descripcion").html($scope.slider.descripcion)
                $scope.borrar_imagen.push($scope.slider.id_imagen);
                $scope.activo_img = "activo"
                $scope.slider.imagen = $scope.slider.ruta
                $scope.titulo_registrar = "Modificar";
                $scope.subtitulo_pagina  = "Modificar slider";
                setTimeout(function(){
                    $('#idioma > option[value="'+$scope.slider.id_idioma+'"]').prop('selected', true);
                    $('#direccion_slide > option[value="'+$scope.slider.direccion+'"]').prop('selected', true);
                    $('#direccion_vertical > option[value="'+$scope.slider.vertical+'"]').prop('selected', true);
                    $('#orden > option[value="'+$scope.slider.orden+'"]').prop('selected', true);
                    $scope.slider.inicial = $scope.slider.orden;
                },500);
                
                //$("#idioma").attr("disabled");
                $("#idioma").prop('disabled', true);
            
                /*setTimeout(function(){
                    $('#idioma > option[value="'+$scope.slider.id_idioma+'"]').prop('selected', true);
                    $('#direccion_slide > option[value="'+$scope.slider.direccion+'"]').prop('selected', true);

                },300);*/
                //$("#idioma").attr("disabled");
                //$("#idioma").prop('disabled', true);
                desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
            });
        }
        //---
        ///////////////////////////////////////////////////
        $scope.consultar_idioma();
        $scope.consultar_galeria_img();

        $scope.id_slider = $("#id_slider").val();
            if($scope.id_slider){
                $scope.consultarSliderIndividual();
            }else{
                $("#idioma").removeAttr("disabled");
            }
        //-----------------------------------------------------------------------
    })
    .controller("tproductosConsultaController", function($scope,$http,$location,serverDataMensajes,categoriasFactory,sesionFactory,idiomaFactory,tproductosFactory){
        $(".li-menu").removeClass("active");
        $("#li_productos1").addClass("active");    
        $(".a-menu").removeClass("active");
        $("#t_producto").addClass("active");    
        $scope.titulo_pagina = "Consulta de Tipos de Productos";
        $scope.activo_img = "inactivo";
        $scope.productos = {
                            'id': '',
                            'idioma': '',
                            'id_idioma' : '',
                            'somos': '',
                            'digital_agency': '',
                            'estatus' : '',
                            'id_imagen' : '',
                            'imagen' : ''
        }
        $scope.nosotros_menu = "1";
        $scope.id_nosotros = "";
        $scope.base_url = $("#base_url").val();

        $scope.iniciar_datatable = function(){
            //--Iniciar DataTable
            $('#myTable').DataTable({
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                    "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
                "order": [[4, "asc" ]]


            });
            //console.log($scope.nosotros);
        }

        $scope.consultar_tipo_productos = function(){
            tproductosFactory.asignar_valores("","",$scope.base_url)
            tproductosFactory.cargar_productos(function(data){
                $scope.productos = data;
                //console.log($scope.productos);
            });
        }

        $scope.ver_producto = function(valor){
            var id = $("#ver"+valor).attr("data");
            $scope.id_tproductos = id;
            //console.log($scope.id_tproductos);
            $("#id_tproductos").val($scope.id_tproductos);
            let form = document.getElementById('formConsultaProductos');
            form.action = "./tproductosVer";
            form.submit();
        }

        $scope.activar_registro = function(event){
            $scope.id_seleccionado_productos = []
            $scope.estatus_seleccionado_productos = []
            $scope.id_seleccionado_tipo_productos = []
            var caja = event.currentTarget.id;
            var atributos = $("#"+caja).attr("data");
            var arreglo_atributos = atributos.split("|");

            $scope.id_seleccionado_productos = arreglo_atributos[0];
            $scope.estatus_seleccionado_productos = arreglo_atributos[1];
            $scope.id_seleccionado_tipo_productos =arreglo_atributos[2];
            $("#cabecera_mensaje").text("Información:");

            if ($scope.estatus_seleccionado_productos==0){
                mensaje = "Desea modificar el estatus de este registro a publicado? ";
                $scope.estatus_seleccionado_productos=1;
            }else{
                mensaje = "Desea modificar el estatus de este registro a inactivo? ";
                $scope.estatus_seleccionado_productos=0
            }
            $scope.modificar_estatus("activar_inactivar",mensaje)
        }
        //----------------------------------------------------------------
        $scope.modificar_estatus = function(opcion,mensaje){
             swal({
                  title: 'Esta seguro?',
                  text: mensaje,
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Si!',
                  cancelButtonText: 'No',
                }).then((result) => {
                      if (result.value) {
                          $scope.accion_estatus()
                      }
                })
        }
        //----------------------------------------------------------------
        $scope.accion_estatus = function(){
            $http.post($scope.base_url+"/Tproductos/modificarProductosEstatus",
            {
                 'id':$scope.id_seleccionado_productos,
                 'estatus':$scope.estatus_seleccionado_productos,
                 'id_productos':$scope.id_seleccionado_tipo_productos

            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                
                if($scope.mensajes.mensaje == "modificacion_procesada"){
                    Swal(
                          'Realizado!',
                          'El proceso fue ejecutado.',
                          'success'
                    ).then((result) => {
                          if (result.value) {
                                let form = document.getElementById('formConsultaProductos');
                                form.action = "./consultar_tproductos";
                                form.submit();
                          }

                    });
                }else{
                    Swal(
                          'No realizado!',
                          'El proceso no pudo ser ejecutado.',
                          'warning'
                    )
                }
            }).error(function(data,estatus){
                console.log(data);
            });
        }
        //----------------------------------------------------------------
        //----------------------------------------------------------------
        $scope.eliminar_registro = function(event){
            $scope.id_seleccionado_productos = []
            $scope.estatus_seleccionado_productos = []
            var caja = event.currentTarget.id;
            //alert(caja);
            var atributos = $("#"+caja).attr("data");
            var arreglo_atributos = atributos.split("|");
            $scope.id_seleccionado_productos = arreglo_atributos[0];
            $scope.estatus_seleccionado_productos = arreglo_atributos[1];
            $scope.id_seleccionado_tipo_productos = arreglo_atributos[2];
            //console.log($scope.estatus_seleccionado_productos);
            $("#cabecera_mensaje").text("Información:");
            if ($scope.estatus_seleccionado_productos==2){
                mensaje = "Desea eliminar este registro? ";
            }
            $scope.modificar_estatus("eliminar",mensaje)
        }
        //----------------------------------------------------------------

        setTimeout(function(){
                        $scope.iniciar_datatable();
                },500);

        $scope.consultar_tipo_productos();
    })
    .controller("tproductosController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,sesionFactory,idiomaFactory,tproductosFactory,buscarproductosFactory,ordenProductosFactory){
        //Cuerpo declaraciones
        $(".li-menu").removeClass("active");
        $("#li_productos1").addClass("active");    
        $(".a-menu").removeClass("active");
        $("#t_producto").addClass("active");    
        $scope.titulo_pagina = "Tipo de productos";
        $scope.subtitulo_pagina  = "Registrar Tipo de productos";
        $scope.activo_img = "inactivo";
        $scope.productos = {
                                'id':'',
                                'id_idioma':'',
                                'titulo':'',
                                'descripcion':'',
                                'estatus':'',
                                'id_imagen':'',
                                'imagen':'',
                                'id_producto':''
        }
        $scope.id_productos = ""
        $scope.searchProductos = []
        $scope.borrar_imagen = []
        $scope.currentTab = 'datos_basicos'
        $scope.titulo_registrar = "Registrar";
        $scope.base_url = $("#base_url").val();
        $scope.titulo_text = "Pulse aquí para ingresar el contenido del producto"
        //console.log($scope.productos);


        //--------------------------------------------------------
        //Cuerpo de metodos
        $scope.consultar_idioma = function(){
            idiomaFactory.asignar_valores("",$scope.base_url)
            idiomaFactory.cargar_idioma(function(data){
                $scope.idioma=data;
                $.each( $scope.idioma, function( indice, elemento ){
                      agregarOptions("#idioma", elemento.id, elemento.descripcion)
                });
                $('#idioma > option[value="0"]').prop('selected', true);

                //console.log($scope.idioma);
            });
        }
        $scope.buscarProducto = function(){
            if(!$scope.productos.id_idioma){
                $("#tipos_producto option").remove();
                agregarOptions("#tipos_producto", "", "--Seleccione un producto--")            
            }
            //console.log($scope.productos.id_idioma);

            buscarproductosFactory.asignar_valores($scope.productos.id_idioma.id,$scope.base_url)
            buscarproductosFactory.cargar_productos(function(data){
                //console.log(data);            
                $scope.mensajes = data;
                if($scope.mensajes.mensaje == "error"){
                    //console.log($scope.mensajes.mensaje);

                    $("#tipos_producto option").remove();
                    agregarOptions("#tipos_producto", "", "--Seleccione un producto--")

                }else{
                    $("#tipos_producto option").remove();
                    $scope.tipos_producto=data;
                    agregarOptions("#tipos_producto", "", "--Seleccione un producto--")

                $.each( $scope.tipos_producto, function( indice, elemento ){
                    agregarOptions("#tipos_producto", elemento.id, elemento.titulo)
                });
                $('#tipos_producto > option[value="0"]').prop('selected', true);
            }});
            
        }
        $scope.agregarWisi = function(){
            //$('#text_editor').data("wysihtml5").editor.clear();
            $scope.productos.descripcion = $(".textarea_editor").val()
            $("#div_descripcion").html($scope.productos.descripcion)
            $("#cerrarModal").click();
        }
        $scope.wisi_modal = function(){
            $("#wisiModal").modal("show")
            $(".textarea_editor").data("wysihtml5").editor.setValue($scope.productos.descripcion)
        }
        //--
        $scope.consultar_galeria_img = function(){
            galeriaFactory.asignar_valores('27','',$scope.base_url);//ya que es galeria de imagenes
            galeriaFactory.cargar_galeria_fa(function(data){
                $scope.galery=data;
                //console.log($scope.galery);
            });
        }

        $scope.seleccione_img_principal = function(){
            $("#modal_img1").modal("show");
        }

        $scope.seleccionar_imagen = function(event){
            var imagen = event.target.id;//Para capturar id
            var vec = $("#"+imagen).attr("data");
            var vector_data = vec.split("|")
            var id_imagen = vector_data[0];
            var ruta = vector_data[1];
            //console.log(vector_data[1]);

            $scope.borrar_imagen.push(id_imagen);
            $scope.activo_img = "activo"
            $scope.productos.id_imagen = id_imagen
            $scope.productos.imagen = ruta
            //--
            $("#modal_img1").modal("hide");
            //--
        }

        $scope.registrarproductos = function(){
            
            uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
            $scope.productos.orden = $("#orden").val();
            if($scope.validar_form()==true){
                //Para guardar
                if(($scope.productos.id!="")&&($scope.productos.id!=undefined)){
                    $scope.modificar_productos();
                }else{
                    $scope.insertar_productos();
                }

            }

            //desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

        }

        $scope.insertar_productos = function(){
            //console.log($scope.productos);

            $http.post($scope.base_url+"/Tproductos/registrarProductos",
            {
                'id'         : $scope.productos.id,
                'titulo'     : $scope.productos.titulo,
                'id_imagen'  : $scope.productos.id_imagen,
                'descripcion': $scope.productos.descripcion,
                'id_idioma'  : $scope.productos.id_idioma.id,
                'id_productos'  : $scope.productos.id_productos.id,
                'orden'  : $scope.productos.orden,
            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "registro_procesado"){
                    mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
                    $scope.limpiar_cajas_productos();
                }else if($scope.mensajes.mensaje == "existe"){
                    mostrar_notificacion("Mensaje","Ya existe un tipo de producto con ese titulo","warning");
                }
                else{
                    mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
                }
            }).error(function(data,estatus){
                console.log(data);
            });
        }

        $scope.modificar_productos = function(){
            //console.log($scope.productos);

            $http.post($scope.base_url+"/Tproductos/modificarProductos",
            {
                'id'         : $scope.productos.id,
                'titulo'     : $scope.productos.titulo,
                'id_imagen'  : $scope.productos.id_imagen,
                'descripcion': $scope.productos.descripcion,
                'id_productos': $scope.productos.id_productos,
                'orden':$scope.productos.orden,
                'inicial':$scope.productos.inicial,
                'id_idioma':$scope.productos.id_idioma,
                
            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "registro_procesado"){
                    mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
                    $scope.productos.inicial = $scope.productos.orden

                }else if($scope.mensajes.mensaje == "existe"){
                    mostrar_notificacion("Mensaje","Ya existe una noticia con ese titulo","warning");
                }
                else{
                    mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
                }
            }).error(function(data,estatus){
                console.log(data);
            });
        }

        $scope.validar_form = function(){
            //console.log($scope.productos)
            if(($scope.productos.id_idioma=="NULL")||($scope.productos.id_idioma=="")||(!$scope.productos.id_idioma)){
                mostrar_notificacion("Campos no validos","Debe seleccionar el idioma","warning");
                return false;
            }else if(($scope.productos.id_productos=="NULL")||($scope.productos.id_productos=="")||($scope.productos.id_productos=="0")){
                mostrar_notificacion("Campos no validos","Debe seleccionar un producto","warning");
                return false;
            }else if($scope.productos.titulo==""){
                mostrar_notificacion("Campos no validos","Debe ingresar el titulo","warning");
                return false;
            }else if(($scope.productos.orden=="NULL")||($scope.productos.orden=="")){
                mostrar_notificacion("Campos no validos","Debe seleccionar el orden del servicio","warning");
                return false;
            }else if(($scope.productos.id_imagen=="NULL")||($scope.productos.id_imagen=="")){
                mostrar_notificacion("Campos no validos","Debe seleccionar la imagen","warning");
                return false;
            }
            else{
                return true;
            }
        }

        $scope.limpiar_cajas_productos = function(){
            $scope.productos = {
                                'id':'',
                                'id_idioma':'',
                                'titulo':'',
                                'descripcion':'',
                                'estatus':'',
                                'id_imagen':'',
                                'id_producto':''

            }
            $scope.activo_img = "inactivo";

            $('#text_editor').data("wysihtml5").editor.clear();
            $("#div_descripcion").html($scope.titulo_text);
            eliminarOptions("orden")
            $('#orden > option[value=""]').prop('selected', true);
            eliminarOptions("tipos_producto")
            $('#tipos_producto > option[value=""]').prop('selected', true);

        }
        //--
        $scope.consultarProductoIndividual = function(){

            uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

            tproductosFactory.asignar_valores("",$scope.id_tproductos,$scope.base_url)
            tproductosFactory.cargar_productos(function(data){
                $scope.productos=data[0];
                //console.log(data[0]);
                $("#div_descripcion").html($scope.productos.descripcion)
                $scope.borrar_imagen.push($scope.productos.id_imagen);
                $scope.activo_img = "activo"
                ordenProductosFactory.asignar_valores($scope.productos.id_idioma,$scope.base_url,"2",$scope.productos.id_productos)
                ordenProductosFactory.cargar_orden_tproductos(function(data){
                    $scope.ordenes=data;
                    //console.log($scope.ordenes)
                    //-Elimino el select
                    eliminarOptions("orden")
                    $.when(
                        $.each( $scope.ordenes, function( indice, elemento ){
                              agregarOptions("#orden", elemento.orden, elemento.orden)
                        })
                    ).then(function(){
                        $('#orden > option[value="'+$scope.productos.orden+'"]').prop('selected', true);
                    });
                });
                //-Cambio 10022020
            

            buscarproductosFactory.asignar_valores("",$scope.base_url)
            buscarproductosFactory.cargar_productos(function(data){
                $scope.tipos_producto=data;
                $scope.productos.id_producto= $scope.tipos_producto;
                
                $.when(
                    $.each($scope.tipos_producto, function( indice, elemento ){
                        agregarOptions("#tipos_producto", elemento.id, elemento.titulo)
                    })
                    //$('#tipos_producto > option[value="0"]').prop('selected', true)
                ).then(function(){
                    $('#tipos_producto > option[value="'+$scope.productos.id_productos+'"]').prop('selected', true);
                    desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar")
                });
            });
            //console.log($scope.productos);

                $scope.productos.imagen = $scope.productos.ruta
                $scope.titulo_registrar = "Modificar";
                $scope.subtitulo_pagina  = "Modificar tipo de producto";
                setTimeout(function(){
                    $('#idioma > option[value="'+$scope.productos.id_idioma+'"]').prop('selected', true);
                    //$('#orden > option[value="'+$scope.productos.orden+'"]').prop('selected', true);
                    //$('#tipos_producto > option[value="'+$scope.productos.id_productos+'"]').prop('selected', true);
                    $scope.productos.inicial = $scope.productos.orden;
                },1000);
                $("#idioma").prop('disabled', true);
                $("#tipos_producto").prop('disabled', true);


            });

            //desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

        }
        //---
        $scope.cargarOrden = function(){
            if($scope.productos.id_productos.id){
                $scope.id_productos=$scope.productos.id_productos.id
            }else{
                $scope.id_productos=$scope.productos.id_productos
            }
            //---
            //if(($scope.productos.id_idioma.id!="")&&($scope.id_productos)){
            //------------------------------------------------------------
            ordenProductosFactory.asignar_valores($scope.productos.id_idioma.id,$scope.base_url,"1",$scope.id_productos,"")
            ordenProductosFactory.cargar_orden_tproductos(function(data){
                $scope.ordenes=data;
                //-Elimino el select
                eliminarOptions("orden")
                $.each( $scope.ordenes, function( indice, elemento ){
                      agregarOptions("#orden", elemento.orden, elemento.orden)
                });
                $('#orden > option[value=""]').prop('selected', true);

            });
            //------------------------------------------------------------
            //}
            //---
        }
        //--------------------------------------------------------
        //Cuerpo de llamados
        $scope.consultar_idioma();
        $scope.consultar_galeria_img();
        //--Si tiene id categoria .... consulto los datos relacionados a ese id, esto es cuando proviene de una consulta
        $scope.id_tproductos  = $("#id_tproductos").val();
        //.id_tproductos);

        if($scope.id_tproductos){
            $scope.consultarProductoIndividual()
        }
        //--------------------------------------------------------

    })
    .controller("tserviciosConsultaController", function($scope,$http,$location,serverDataMensajes,categoriasFactory,sesionFactory,idiomaFactory,tserviciosFactory){
        $(".li-menu").removeClass("active");
        $("#li_servicios1").addClass("active");    
        $(".a-menu").removeClass("active");
        $("#t_servicio").addClass("active");    

        $scope.titulo_pagina = "Consulta de Tipos de Productos";
        $scope.activo_img = "inactivo";
        $scope.productos = {
                            'id': '',
                            'idioma': '',
                            'id_idioma' : '',
                            'somos': '',
                            'digital_agency': '',
                            'estatus' : '',
                            'id_imagen' : '',
                            'imagen' : ''
        }
        $scope.nosotros_menu = "1";
        $scope.id_nosotros = "";
        $scope.base_url = $("#base_url").val();

        $scope.iniciar_datatable = function(){
            //--Iniciar DataTable
            $('#myTable').DataTable({
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                    "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
                "order": [[4, "asc" ]]

            });
            //console.log($scope.nosotros);
        }

        $scope.consultar_tipo_productos = function(){
            tserviciosFactory.asignar_valores("","",$scope.base_url)
            tserviciosFactory.cargar_servicios(function(data){
                $scope.servicios = data;
                //console.log($scope.productos);
            });
        }

        $scope.ver_producto = function(valor){
            var id = $("#ver"+valor).attr("data");
            $scope.id_tservicio = id;
            //console.log($scope.id_tservicio);
            $("#id_tservicio").val($scope.id_tservicio);
            let form = document.getElementById('formConsultaProductos');
            form.action = "./tserviciosVer";
            form.submit();
        }

        $scope.activar_registro = function(event){
                    $scope.id_seleccionado_productos = []
                    $scope.estatus_seleccionado_productos = []
                    $scope.id_seleccionado_tipo_productos = []

                    var caja = event.currentTarget.id;
                    var atributos = $("#"+caja).attr("data");
                    var arreglo_atributos = atributos.split("|");

                    $scope.id_seleccionado_productos = arreglo_atributos[0];
                    $scope.estatus_seleccionado_productos = arreglo_atributos[1];
                    $scope.id_seleccionado_tipo_productos =arreglo_atributos[2];


                    $("#cabecera_mensaje").text("Información:");

                    if ($scope.estatus_seleccionado_productos==0){
                        mensaje = "Desea modificar el estatus de este registro a publicado? ";
                        $scope.estatus_seleccionado_productos=1;
                    }else{
                        mensaje = "Desea modificar el estatus de este registro a inactivo? ";
                        $scope.estatus_seleccionado_productos=0
                    }
                    $scope.modificar_estatus("activar_inactivar",mensaje)
                }
                //----------------------------------------------------------------
                $scope.modificar_estatus = function(opcion,mensaje){
                     swal({
                          title: 'Esta seguro?',
                          text: mensaje,
                          type: 'warning',
                          showCancelButton: true,
                          confirmButtonColor: '#3085d6',
                          cancelButtonColor: '#d33',
                          confirmButtonText: 'Si!',
                          cancelButtonText: 'No',
                        }).then((result) => {
                              if (result.value) {
                                  $scope.accion_estatus()
                              }
                        })
                }
                //----------------------------------------------------------------
                $scope.accion_estatus = function(){

                    $http.post($scope.base_url+"/Tservicios/modificarServiciosEstatus",
                    {
                         'id':$scope.id_seleccionado_productos,
                         'estatus':$scope.estatus_seleccionado_productos,
                         'id_servicios':$scope.id_seleccionado_tipo_productos


                    }).success(function(data, estatus, headers, config){
                        $scope.mensajes  = data;
                        
                        if($scope.mensajes.mensaje == "modificacion_procesada"){
                            Swal(
                                  'Realizado!',
                                  'El proceso fue ejecutado.',
                                  'success'
                            ).then((result) => {
                                  if (result.value) {
                                        let form = document.getElementById('formConsultaProductos');
                                        form.action = "./consultar_tservicios";
                                        form.submit();
                                  }

                            });
                        }else{
                            Swal(
                                  'No realizado!',
                                  'El proceso no pudo ser ejecutado.',
                                  'warning'
                            )
                        }
                    }).error(function(data,estatus){
                        console.log(data);
                    });
                }
                //----------------------------------------------------------------
                //----------------------------------------------------------------
        $scope.eliminar_registro = function(event){
            $scope.id_seleccionado_productos = []
            $scope.estatus_seleccionado_productos = []
            var caja = event.currentTarget.id;
            //alert(caja);
            var atributos = $("#"+caja).attr("data");
            var arreglo_atributos = atributos.split("|");
            $scope.id_seleccionado_productos = arreglo_atributos[0];
            $scope.estatus_seleccionado_productos = arreglo_atributos[1];
            $scope.id_seleccionado_tipo_productos = arreglo_atributos[2];

            //console.log($scope.estatus_seleccionado_productos);
            $("#cabecera_mensaje").text("Información:");
            if ($scope.estatus_seleccionado_productos==2){
                mensaje = "Desea eliminar este registro? ";
            }
            $scope.modificar_estatus("eliminar",mensaje)
        }
        //----------------------------------------------------------------

        setTimeout(function(){
                        $scope.iniciar_datatable();
                },500);

        $scope.consultar_tipo_productos();
    })
    .controller("tserviciosController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,ordenProductosFactory,idiomaFactory,tserviciosFactory,buscarserviciosFactory){
        //Cuerpo declaraciones
        $(".li-menu").removeClass("active");
        $("#li_servicios1").addClass("active");    
        $(".a-menu").removeClass("active");
        $("#t_servicio").addClass("active");    
        $scope.titulo_pagina = "Tipo de servicios";
        $scope.subtitulo_pagina  = "Registrar Tipo de servicios";
        $scope.activo_img = "inactivo";
        $scope.servicios = {
                                'id':'',
                                'id_idioma':'',
                                'titulo':'',
                                'descripcion':'',
                                'estatus':'',
                                'id_imagen':'',
                                'imagen':'',
                                'id_servicio':''
        }
        $scope.id_servicios = ""
        $scope.searchProductos = []
        $scope.borrar_imagen = []
        $scope.currentTab = 'datos_basicos'
        $scope.titulo_registrar = "Registrar";
        $scope.base_url = $("#base_url").val();
        $scope.titulo_text = "Pulse aquí para ingresar el contenido del servicio"
        //console.log($scope.servicios);


        //--------------------------------------------------------
        //Cuerpo de metodos
        $scope.consultar_idioma = function(){
            idiomaFactory.asignar_valores("",$scope.base_url)
            idiomaFactory.cargar_idioma(function(data){
                $scope.idioma=data;
                $.each( $scope.idioma, function( indice, elemento ){
                      agregarOptions("#idioma", elemento.id, elemento.descripcion)
                });
                $('#idioma > option[value="0"]').prop('selected', true);

                //console.log($scope.idioma);
            });
        }
        $scope.buscarServicio = function(){
            if(!$scope.servicios.id_idioma){
                $("#tipos_servicio option").remove();
                agregarOptions("#tipos_servicio", "", "--Seleccione un servicio--")            
            }
            //console.log($scope.servicios.id_idioma);

            buscarserviciosFactory.asignar_valores($scope.servicios.id_idioma.id,$scope.base_url)
            buscarserviciosFactory.cargar_servicio(function(data){
                //console.log(data);
                //console.log($scope.idioma);

                $scope.mensajes = data;
                if($scope.mensajes.mensaje == "error"){
                    //console.log("xd");
                    $("#tipos_servicio option").remove();
                    agregarOptions("#tipos_servicio", "", "--Seleccione un servicio--")

                }else{
                    $("#tipos_servicio option").remove();
                    $scope.tipos_servicio=data;
                    agregarOptions("#tipos_servicio", "", "--Seleccione un servicio--")

                $.each( $scope.tipos_servicio, function( indice, elemento ){
                    agregarOptions("#tipos_servicio", elemento.id, elemento.titulo)
                });
                $('#tipos_servicio > option[value="0"]').prop('selected', true);
            }});
            
        }
        $scope.agregarWisi = function(){
            //$('#text_editor').data("wysihtml5").editor.clear();
            $scope.servicios.descripcion = $(".textarea_editor").val()
            $("#div_descripcion").html($scope.servicios.descripcion)
            $("#cerrarModal").click();
        }
        $scope.wisi_modal = function(){
            $("#wisiModal").modal("show")
            $(".textarea_editor").data("wysihtml5").editor.setValue($scope.servicios.descripcion)
        }
        //--
        $scope.consultar_galeria_img = function(){
            galeriaFactory.asignar_valores('29','',$scope.base_url);//ya que es galeria de imagenes
            galeriaFactory.cargar_galeria_fa(function(data){
                $scope.galery=data;
                //console.log($scope.galery);
            });
        }

        $scope.seleccione_img_principal = function(){
            $("#modal_img1").modal("show");
        }

        $scope.seleccionar_imagen = function(event){
            var imagen = event.target.id;//Para capturar id
            var vec = $("#"+imagen).attr("data");
            var vector_data = vec.split("|")
            var id_imagen = vector_data[0];
            var ruta = vector_data[1];
            //console.log(vector_data[1]);

            $scope.borrar_imagen.push(id_imagen);
            $scope.activo_img = "activo"
            $scope.servicios.id_imagen = id_imagen
            $scope.servicios.imagen = ruta
            //--
            $("#modal_img1").modal("hide");
            //--
        }

        $scope.registrarservicios = function(){
            //console.log($scope.servicios);

            uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
            $scope.servicios.orden = $("#orden").val();

            if($scope.validar_form()==true){
                //Para guardar
                if(($scope.servicios.id!="")&&($scope.servicios.id!=undefined)){
                    $scope.modificar_servicios();
                }else{
                    $scope.insertar_servicios();
                }

            }

            //desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

        }

        $scope.insertar_servicios = function(){
            //console.log($scope.servicios);

            $http.post($scope.base_url+"/Tservicios/registrarServicios",
            {
                'id'         : $scope.servicios.id,
                'titulo'     : $scope.servicios.titulo,
                'id_imagen'  : $scope.servicios.id_imagen,
                'descripcion': $scope.servicios.descripcion,
                'id_idioma'  : $scope.servicios.id_idioma.id,
                'id_servicios'  : $scope.servicios.id_servicio.id,
                'orden': $scope.servicios.orden,

            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "registro_procesado"){
                    mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
                    $scope.limpiar_cajas_productos();
                }else if($scope.mensajes.mensaje == "existe"){
                    mostrar_notificacion("Mensaje","Ya existe un tipo de servicio con ese titulo","warning");
                }
                else{
                    mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
                }
            }).error(function(data,estatus){
                console.log(data);
            });
        }

        $scope.modificar_servicios = function(){
            //console.log($scope.servicios);

            $http.post($scope.base_url+"/Tservicios/modificarServicios",
            {
                'id'         : $scope.servicios.id,
                'titulo'     : $scope.servicios.titulo,
                'id_imagen'  : $scope.servicios.id_imagen,
                'descripcion': $scope.servicios.descripcion,
                'id_servicios': $scope.servicios.id_servicios,
                'id_idioma': $scope.servicios.id_idioma,
                'orden': $scope.servicios.orden,
                'inicial': $scope.servicios.inicial,
            }).success(function(data, estatus, headers, config){
                $scope.mensajes  = data;
                if($scope.mensajes.mensaje == "registro_procesado"){
                    mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
                    $scope.servicios.inicial = $scope.servicios.orden;
                }else if($scope.mensajes.mensaje == "existe"){
                    mostrar_notificacion("Mensaje","Ya existe una noticia con ese titulo","warning");
                }
                else{
                    mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
                }
            }).error(function(data,estatus){
                console.log(data);
            });
        }

        $scope.validar_form = function(){
            //console.log($scope.servicios)
            if(($scope.servicios.id_idioma=="NULL")||($scope.servicios.id_idioma=="")||(!$scope.servicios.id_idioma)){
                mostrar_notificacion("Campos no validos","Debe seleccionar el idioma","warning");
                return false;
            }else if(($scope.servicios.id_servicio=="NULL")||($scope.servicios.id_servicio=="")||($scope.servicios.id_servicio=="0")){
                mostrar_notificacion("Campos no validos","Debe seleccionar un servicio","warning");
                return false;
            }else if($scope.servicios.titulo==""){
                mostrar_notificacion("Campos no validos","Debe ingresar el titulo","warning");
                return false;
            }else if(($scope.servicios.orden=="NULL")||($scope.servicios.orden=="")){
                mostrar_notificacion("Campos no validos","Debe seleccionar el orden del servicio","warning");
                return false;
            }else if(($scope.servicios.id_imagen=="NULL")||($scope.servicios.id_imagen=="")){
                mostrar_notificacion("Campos no validos","Debe seleccionar la imagen","warning");
                return false;
            }
            else{
                return true;
            }
        }

        $scope.limpiar_cajas_productos = function(){
            $scope.servicios = {
                                'id':'',
                                'id_idioma':'',
                                'titulo':'',
                                'descripcion':'',
                                'estatus':'',
                                'id_imagen':'',
                                'id_servicio':''

            }
            $scope.activo_img = "inactivo";

            $('#text_editor').data("wysihtml5").editor.clear();
            $("#div_descripcion").html($scope.titulo_text);
            eliminarOptions("orden")
            $('#orden > option[value=""]').prop('selected', true);
            eliminarOptions("tipos_servicio")
            $('#tipos_servicio > option[value=""]').prop('selected', true);
        }
        //--
        $scope.consultarProductoIndividual = function(){

            uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

            tserviciosFactory.asignar_valores("",$scope.id_tservicio,$scope.base_url)
            tserviciosFactory.cargar_servicios(function(data){
                $scope.servicios=data[0];
                //console.log($scope.servicios);
                ordenProductosFactory.asignar_valores($scope.servicios.id_idioma,$scope.base_url,"2",$scope.servicios.id_servicios)
                ordenProductosFactory.cargar_orden_tservicios(function(data){
                    $scope.ordenes=data;
                    //console.log($scope.ordenes)
                    //-Elimino el select
                    eliminarOptions("orden")
                    $.when(
                        $.each( $scope.ordenes, function( indice, elemento ){
                            agregarOptions("#orden", elemento.orden, elemento.orden)
                        })
                    ).then(function(){
                        $('#orden > option[value="'+$scope.servicios.orden+'"]').prop('selected', true)
                    })
                    
                });



                
                //-Cambio 10022020
            

            buscarserviciosFactory.asignar_valores("",$scope.base_url)
            buscarserviciosFactory.cargar_servicio(function(data){
                $scope.tipos_servicio=data;
                $scope.servicios.id_servicio= $scope.tipos_servicio;
                $.when(
                    $.each($scope.tipos_servicio, function( indice, elemento ){
                        agregarOptions("#tipos_servicio", elemento.id, elemento.titulo)
                    })
                ).then(function(){
                    $('#tipos_servicio > option[value="'+$scope.servicios.id_servicios+'"]').prop('selected', true);
                })
                //$('#tipos_servicio > option[value="0"]').prop('selected', true);
            });
            //console.log($scope.productos);
            $scope.servicios.imagen = $scope.servicios.ruta

                $scope.titulo_registrar = "Modificar";
                $scope.subtitulo_pagina  = "Modificar tipo de producto";
                setTimeout(function(){
                    $('#idioma > option[value="'+$scope.servicios.id_idioma+'"]').prop('selected', true);
                    //$('#tipos_servicio > option[value="'+$scope.servicios.id_servicios+'"]').prop('selected', true);
                    //$('#orden > option[value="'+$scope.servicios.orden+'"]').prop('selected', true);
                    $scope.servicios.inicial = $scope.servicios.orden;
                },300);
                $("#idioma").prop('disabled', true);
                $("#tipos_servicio").prop('disabled', true);


            });

            desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

        }
        //---
        $scope.cargarOrden = function(){
            if($scope.servicios.id_servicio.id){
                $scope.id_servicio=$scope.servicios.id_servicio.id
            }else{
                $scope.id_servicio=$scope.servicios.id_servicio
            }
            ordenProductosFactory.asignar_valores($scope.servicios.id_idioma.id,$scope.base_url,"1",$scope.id_servicio,"")
            ordenProductosFactory.cargar_orden_tservicios(function(data){
                $scope.ordenes=data;
                //-Elimino el select
                eliminarOptions("orden")
                $.each( $scope.ordenes, function( indice, elemento ){
                      agregarOptions("#orden", elemento.orden, elemento.orden)
                });
                $('#orden > option[value=""]').prop('selected', true);

            });
        }
        //--------------------------------------------------------
        //Cuerpo de llamados
        $scope.consultar_idioma();
        $scope.consultar_galeria_img();
        //--Si tiene id categoria .... consulto los datos relacionados a ese id, esto es cuando proviene de una consulta
        $scope.id_tservicio  = $("#id_tservicio").val();
        //.id_tservicio);

        if($scope.id_tservicio){
            $scope.consultarProductoIndividual();
        }
        //--------------------------------------------------------

    })