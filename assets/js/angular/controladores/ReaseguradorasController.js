angular.module("ContentManagerApp")
	.controller("ReaseguradorasController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,sesionFactory,idiomaFactory,reaseguradorasFactory,ordenFactory){
		//---------------------------------------------------------------------
		//Cuerpo declaraciones
		$(".li-menu").removeClass("active");
        $(".a-menu").removeClass("active");
        $("#li_empresa").addClass("active");	
        $("#reaseguradoras").addClass("active");	

		$scope.titulo_pagina = "Reaseguradoras";
		$scope.subtitulo_pagina  = "Registrar Reaseguradoras";
		$scope.activo_img = "inactivo";

		$scope.reaseguradoras = {
						'id': '',
						'idioma': '',
						'id_idioma' : '',
						'titulo' : '',
						'url' : '',
						'descripcion' : '',
						'id_imagen' : '',
						'imagen' : 'assets/images/logo_peque.png',
						'orden':''
		}
		
		
		$scope.titulo_registrar = "Registrar";
		$scope.titulo_cons = "Consultar";
		$scope.borrar_imagen = []
		$scope.currentTab = 'datos_basicos'
		$scope.titulo_text = "Pulse aquí para ingresar la descripción de la reaseguradoras"

		$scope.opcion = ''
		$scope.seccion = ''
		$scope.base_url = $("#base_url").val();
		
		//alert($scope.base_url);
		//Cuerpo de metodos
		$scope.consultar_idioma = function(){
			idiomaFactory.asignar_valores("",$scope.base_url)
			idiomaFactory.cargar_idioma(function(data){
				$scope.idioma=data;

				$.each( $scope.idioma, function( indice, elemento ){
				  	agregarOptions("#idioma", elemento.id, elemento.descripcion)
				});
				$('#idioma > option[value="'+$scope.reaseguradoras.id_idioma+'"]').prop('selected', true);
			});
		}
		//---------------------------------
		//
		$scope.cargarOrden = function(){
			ordenFactory.asignar_valores($scope.reaseguradoras.id_idioma,$scope.base_url,"1")
			ordenFactory.cargar_orden_reaseguradoras(function(data){
				$scope.ordenes=data;
				//console.log($scope.ordenes);
				//-Elimino el select
				eliminarOptions("orden")
				$.each( $scope.ordenes, function( indice, elemento ){
				  	agregarOptions("#orden", elemento.orden, elemento.orden)
				});
				$('#orden > option[value=""]').prop('selected', true);

			});
		}
		//WISIMODAL
		$scope.agregarWisi = function(){
			//$('#text_editor').data("wysihtml5").editor.clear();
			$scope.reaseguradoras.descripcion = $(".textarea_editor").val()
			$("#div_descripcion").html($scope.reaseguradoras.descripcion)
			$("#cerrarModal").click();
		}
		//--
		
		$scope.wisi_modal = function(){
			$("#wisiModal").modal("show")
			$(".textarea_editor").data("wysihtml5").editor.setValue($scope.reaseguradoras.descripcion)
		}
		//--
		$scope.agregar_contenido = function(){
			if ($scope.reaseguradoras==undefined) {
					$scope.reaseguradoras = {
						'id': '',
						'idioma': '',
						'id_idioma' : '',
						'titulo' : '',
						'url' : '',
						'descripcion' : '',
						'id_imagen' : '',
						'imagen' : 'assets/images/logo_peque.png',
						'orden':''
					}
			}

		}
		//------------------------------------------CONSULTA DE IMAGEN SEGUN ID DE CATEGORIA
		$scope.consultar_galeria_img = function(){
			galeriaFactory.asignar_valores('24','',$scope.base_url);//ya que es galeria de imagenes
			galeriaFactory.cargar_galeria_fa(function(data){
				$scope.galery=data;
				//console.log($scope.galery);
			});
		}
		//MODAL DE IMG
		$scope.seleccione_img_principal = function(){
			$("#modal_img1").modal("show");
		}
		//PARA SELECCIONAR UNA IMAGEN Y MOSTRARLA EN EL CAMPO DE IMG
		$scope.seleccionar_imagen = function(event){
				var imagen = event.target.id;//Para capturar id
				//console.log(imagen);
				var vec = $("#"+imagen).attr("data");
				var vector_data = vec.split("|")
				var id_imagen = vector_data[0];
				var ruta = vector_data[1];

				$scope.borrar_imagen.push(id_imagen);
				$scope.activo_img = "activo"
				$scope.reaseguradoras.id_imagen = id_imagen
				$scope.reaseguradoras.imagen = ruta
				//alert($scope.reaseguradoras.id_imagen);
				//--
				$("#modal_img1").modal("hide");
				//--
		}
		/////////////////////////////////////////////////////////////////////////////////////
		$scope.registrarReaseguradoras = function(){
			$scope.reaseguradoras.orden = $("#orden").val();
			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
			if($scope.validar_form()==true){
				//Para guardar
				if(($scope.reaseguradoras.id!="")&&($scope.reaseguradoras.id!=undefined)){
					$scope.modificar_reaseguradoras();
				}else{
					$scope.insertar_reaseguradoras();
				}
			}
			//desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

		}
		///////////////////////////////////////////////////////////////////////////////////
		$scope.insertar_reaseguradoras = function(){
			$http.post($scope.base_url+"/Reaseguradoras/registrarReaseguradoras",
			{
				'id' 	     : $scope.reaseguradoras.id,
				'id_idioma'  : $scope.reaseguradoras.id_idioma,
				'titulo'     : $scope.reaseguradoras.titulo,
				'url'     : $scope.reaseguradoras.url,
				'descripcion': $scope.reaseguradoras.descripcion,
				'id_imagen'  : $scope.reaseguradoras.id_imagen,
				'orden':  $scope.reaseguradoras.orden
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "registro_procesado"){
					mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
					$scope.limpiar_cajas_reaseguradoras();
				}else if($scope.mensajes.mensaje == "existe"){
					mostrar_notificacion("Mensaje","Ya existe una noticia con ese titulo","warning");
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}

			}).error(function(data,estatus){
				console.log(data);
			});
		}
		//////////////////////////
		$scope.modificar_reaseguradoras = function(){
			$scope.reaseguradoras.orden = $("#orden").val();
			alert($scope.reaseguradoras.orden)
			$http.post($scope.base_url+"/Reaseguradoras/modificarReaseguradoras",
			{
				'id' 	     : $scope.reaseguradoras.id,
				'id_idioma'  : $scope.reaseguradoras.id_idioma,
				'titulo'     : $scope.reaseguradoras.titulo,
				'url'     : $scope.reaseguradoras.url,
				'descripcion': $scope.reaseguradoras.descripcion,
				'id_imagen'  : $scope.reaseguradoras.id_imagen,
				'boton'      : $scope.reaseguradoras.boton,
				'url'        : $scope.reaseguradoras.url,
				'direccion' : $scope.reaseguradoras.direccion,
				'orden' 	: $scope.reaseguradoras.orden,
				'inicial'	: $scope.reaseguradoras.inicial
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "modificacion_procesada"){
					mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
					//$scope.limpiar_cajas_reaseguradoras();
					$scope.reaseguradoras.inicial = $scope.reaseguradoras.orden 
					desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

				}else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
					desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

				}
				//desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

			}).error(function(data,estatus){
				//console.log(data);
			});
		}
		//////////////////////////
		$scope.validar_form = function(){
			//console.log($scope.reaseguradoras)
			if(($scope.reaseguradoras.id_idioma=="")||($scope.reaseguradoras.id_idioma=="")){
				mostrar_notificacion("Campos no validos","Debe seleccionar el idioma","warning");
				return false;
			}/*else if(($scope.reaseguradoras.descripcion=="")||($scope.reaseguradoras.descripcion=="")){
				mostrar_notificacion("Campos no validos","Debe seleccionar la dirección del texto del slide","warning");
				return false;
			}*/ else if($scope.reaseguradoras.titulo==""){
				mostrar_notificacion("Campos no validos","Debe ingresar el titulo","warning");
				return false;
			}else if(($scope.reaseguradoras.orden=="NULL")||($scope.reaseguradoras.orden=="")){
				mostrar_notificacion("Campos no validos","Debe seleccionar el orden","warning");
				return false;
			}
			else if(($scope.reaseguradoras.id_imagen=="NULL")||($scope.reaseguradoras.id_imagen=="")){
				mostrar_notificacion("Campos no validos","Debe seleccionar la imagen","warning");
				return false;
			}
			else{
				return true;
			}
		}
		/////////////////////////
		$scope.limpiar_cajas_reaseguradoras = function(){
			
			$scope.reaseguradoras = {
						'id': '',
						'idioma': '',
						'id_idioma' : '',
						'titulo' : '',
						'url' : '',
						'descripcion' : '',
						'id_imagen' : '',
						'imagen' : 'assets/images/logo_peque.png',
						'orden':''
			}	

			$scope.activo_img = "inactivo";
			$("#div_descripcion").html($scope.titulo_text);
			$('#textarea_editor').data("wysihtml5").editor.clear();
			$("#idioma").removeAttr("disabled");
			$scope.titulo_registrar = "Registrar";
			$scope.subtitulo_pagina  = "Registrar reaseguradoras";
			$("#nuevo").css({"display":"none"})
		}
		//--
		$scope.consultarReaseguradorasIndividual = function(){
			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
			reaseguradorasFactory.asignar_valores("",$scope.id_reaseguradoras,$scope.base_url)
			reaseguradorasFactory.cargar_reaseguradoras(function(data){
				
				$scope.reaseguradoras=data[0];
								
				
				//alert($scope.reaseguradoras.orden)
				$scope.reaseguradoras=data[0];
				//--
				ordenFactory.asignar_valores($scope.reaseguradoras.id_idioma,$scope.base_url,"2")
				ordenFactory.cargar_orden_reaseguradoras(function(data){
					$scope.ordenes=data;
					//console.log($scope.ordenes)
					//-Elimino el select
					eliminarOptions("orden")
					$.each( $scope.ordenes, function( indice, elemento ){
					  	agregarOptions("#orden", elemento.orden, elemento.orden)
					});
				});
				//--
				//console.log(data[0]);
				$("#div_descripcion").html($scope.reaseguradoras.descripcion)
				$scope.borrar_imagen.push($scope.reaseguradoras.id_imagen);
				$scope.activo_img = "activo"
				$scope.reaseguradoras.imagen = $scope.reaseguradoras.ruta
				$scope.titulo_registrar = "Modificar";
				$scope.subtitulo_pagina  = "Modificar reaseguradoras";
			
				setTimeout(function(){
					$('#idioma > option[value="'+$scope.reaseguradoras.id_idioma+'"]').prop('selected', true);
					$('#orden > option[value="'+$scope.reaseguradoras.orden+'"]').prop('selected', true);
					$scope.reaseguradoras.inicial = $scope.reaseguradoras.orden;
				},500);
				
				//$("#idioma").attr("disabled");
				$("#idioma").prop('disabled', true);
			
				/*setTimeout(function(){
					$('#idioma > option[value="'+$scope.reaseguradoras.id_idioma+'"]').prop('selected', true);
					$('#direccion_slide > option[value="'+$scope.reaseguradoras.direccion+'"]').prop('selected', true);

				},300);*/
				//$("#idioma").attr("disabled");
				//$("#idioma").prop('disabled', true);
				desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
			});
		}
		//---
		///////////////////////////////////////////////////
		$scope.consultar_idioma();
		$scope.consultar_galeria_img();

		$scope.id_reaseguradoras = $("#id_reaseguradoras").val();
			if($scope.id_reaseguradoras){
				$scope.consultarReaseguradorasIndividual();
			}else{
				$("#idioma").removeAttr("disabled");
			}
		//-----------------------------------------------------------------------
	});	