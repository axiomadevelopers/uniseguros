angular.module("ContentManagerApp")
	.controller("DetalleTtproductosController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,ordenProductosFactory,idiomaFactory,tproductosFactory,buscarproductosFactory,buscartproductosFactory,detalleproductosFactory){
		//Cuerpo declaraciones
		$(".li-menu").removeClass("active");
        $("#li_productos1").addClass("active");	
        $(".a-menu").removeClass("active");
        $("#detalle_t_producto").addClass("active");	
		$scope.titulo_pagina = "Detalles de productos";
		$scope.subtitulo_pagina  = "Registrar Detalles de productos";
		$scope.activo_img = "inactivo";
		$scope.productos = {
								'id':'',
								'id_idioma':'',
								'titulo':'',
								'descripcion':'',
								'estatus':'',
								'id_productos':'',
								'id_tipo_producto':''
		}
		$scope.searchProductos = []
		$scope.borrar_imagen = []
		$scope.currentTab = 'datos_basicos'
		$scope.titulo_registrar = "Registrar";
		$scope.base_url = $("#base_url").val();
		$scope.titulo_text = "Pulse aquí para ingresar el contenido del producto"


		//--------------------------------------------------------
		//Cuerpo de metodos
		$scope.consultar_idioma = function(){
			idiomaFactory.asignar_valores("",$scope.base_url)
			idiomaFactory.cargar_idioma(function(data){
				$scope.idioma=data;
				$.each( $scope.idioma, function( indice, elemento ){
				  	agregarOptions("#idioma", elemento.id, elemento.descripcion)
				});
				$('#idioma > option[value="0"]').prop('selected', true);

				//console.log($scope.idioma);
			});
		}
		$scope.buscarProducto = function(){
			//console.log($scope.productos.id_idioma);			

			if(!$scope.productos.id_idioma){
				$("#productosTodos option").remove();
				agregarOptions("#productosTodos", "", "--Seleccione un producto--")
				$("#tipos_producto option").remove();
				agregarOptions("#tipos_producto", "", "--Seleccione tipo de producto--")

			}
			buscarproductosFactory.asignar_valores($scope.productos.id_idioma.id,$scope.base_url)
			buscarproductosFactory.cargar_productos(function(data){
				//console.log(data);			
				$scope.mensajes = data;
				if($scope.mensajes.mensaje == "error"){
					$("#productosTodos option").remove();
					agregarOptions("#productosTodos", "", "--Seleccione un producto--")
					$("#tipos_producto option").remove();
					agregarOptions("#tipos_producto", "", "--Seleccione tipo de producto--")
				}else{
					if($scope.condicional=$scope.productos.id_idioma){}
					$("#productosTodos option").remove();
					$("#tipos_producto option").remove();
					$scope.productosTodos=data;
					agregarOptions("#productosTodos", "", "--Seleccione un producto--")
					agregarOptions("#tipos_producto", "", "--Seleccione tipo de producto--")
					$.each( $scope.productosTodos, function( indice, elemento ){
						agregarOptions("#productosTodos", elemento.id, elemento.titulo)
					});
				$scope.condicional = $scope.productos.id_idioma;
			}});
			
		}
		$scope.buscarTproducto = function(){
			
			if(!$scope.productos.id_productos){
				$("#tipos_producto option").remove();
				agregarOptions("#tipos_producto", "", "--Seleccione tipo de producto--")			
			}
			buscartproductosFactory.asignar_valores($scope.productos.id_productos.id,$scope.base_url)
			buscartproductosFactory.cargar_productos(function(data){
				$scope.mensajes = data;
				if($scope.mensajes.mensaje == "error"){
					$("#tipos_producto option").remove();
					agregarOptions("#tipos_producto", "", "--Seleccione tipo de producto--")
				}else{
					$("#tipos_producto option").remove();
					agregarOptions("#tipos_producto", "", "--Seleccione tipo de producto--")
					$scope.tipos_producto=data;

				$.each( $scope.tipos_producto, function( indice, elemento ){
					agregarOptions("#tipos_producto", elemento.id, elemento.titulo)
				});
				$('#tipos_producto > option[value="0"]').prop('selected', true);
			}});
			
		}
		$scope.agregarWisi = function(){
			//$('#text_editor').data("wysihtml5").editor.clear();
			$scope.productos.descripcion = $(".textarea_editor").val()
			$("#div_descripcion").html($scope.productos.descripcion)
			$("#cerrarModal").click();
		}
		$scope.wisi_modal = function(){
			$("#wisiModal").modal("show")
			$(".textarea_editor").data("wysihtml5").editor.setValue($scope.productos.descripcion)
		}
		//--
		$scope.consultar_galeria_img = function(){
			galeriaFactory.asignar_valores('22','',$scope.base_url);//ya que es galeria de imagenes
			galeriaFactory.cargar_galeria_fa(function(data){
				$scope.galery=data;
				//console.log($scope.galery);
			});
		}

		$scope.seleccione_img_principal = function(){
			$("#modal_img1").modal("show");
		}

		$scope.seleccionar_imagen = function(event){
			var imagen = event.target.id;//Para capturar id
			var vec = $("#"+imagen).attr("data");
			var vector_data = vec.split("|")
			var id_imagen = vector_data[0];
			var ruta = vector_data[1];
			//console.log(vector_data[1]);

			$scope.borrar_imagen.push(id_imagen);
			$scope.activo_img = "activo"
			$scope.productos.id_imagen = id_imagen
			$scope.productos.imagen = ruta
			//--
			$("#modal_img1").modal("hide");
			//--
		}

		$scope.registrarproductos = function(){
			
			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
			$scope.productos.orden = $("#orden").val();

			if($scope.validar_form()==true){
				//Para guardar
				if(($scope.productos.id!="")&&($scope.productos.id!=undefined)){
					$scope.modificar_productos();
				}else{
					$scope.insertar_productos();
				}

			}

			//desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

		}

		$scope.insertar_productos = function(){

			$http.post($scope.base_url+"/DetalleTproductos/registrarProductos",
			{
				'id'	     : $scope.productos.id,
				'titulo'     : $scope.productos.titulo,
				'descripcion': $scope.productos.descripcion,
				'id_idioma'  : $scope.productos.id_idioma.id,
				'id_productos'  : $scope.productos.id_productos.id,
				'id_tipo_producto'  : $scope.productos.id_tipo_producto.id,
				'orden'  : $scope.productos.orden,

			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "registro_procesado"){
					mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
					$scope.limpiar_cajas_productos();
				}else if($scope.mensajes.mensaje == "existe"){
					mostrar_notificacion("Mensaje","Ya existe un detalle con ese titulo","warning");
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}

		$scope.modificar_productos = function(){
			//console.log($scope.productos);

			$http.post($scope.base_url+"/DetalleTproductos/modificarProductos",
			{
				'id'	     : $scope.productos.id,
				'titulo'     : $scope.productos.titulo,
				'descripcion': $scope.productos.descripcion,
				'id_idioma'  : $scope.productos.id_idioma,
				'id_productos'  : $scope.productos.id_productos,
				'id_tipo_producto'  : $scope.productos.id_tipo_producto,
				'orden':$scope.productos.orden,
				'inicial':$scope.productos.inicial,
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "registro_procesado"){
					mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
					$scope.productos.inicial = $scope.productos.orden

				}else if($scope.mensajes.mensaje == "existe"){
					mostrar_notificacion("Mensaje","Ya existe una producto con ese titulo","warning");
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}

		$scope.validar_form = function(){
			//console.log($scope.productos)
			if(($scope.productos.id_idioma=="NULL")||($scope.productos.id_idioma=="")||(!$scope.productos.id_idioma)){
				mostrar_notificacion("Campos no validos","Debe seleccionar el idioma","warning");
				return false;
			}else if(($scope.productos.id_productos=="NULL")||($scope.productos.id_productos=="")||(!$scope.productos.id_productos)){
				mostrar_notificacion("Campos no validos","Debe seleccionar un producto","warning");
				return false;
			}else if(($scope.productos.id_tipo_producto=="NULL")||($scope.productos.id_tipo_producto=="")||(!$scope.productos.id_tipo_producto)){
				mostrar_notificacion("Campos no validos","Debe seleccionar tipo de producto","warning");
				return false;
			}else if($scope.productos.titulo==""){
				mostrar_notificacion("Campos no validos","Debe ingresar el titulo","warning");
				return false;
			}else if(($scope.productos.orden=="NULL")||($scope.productos.orden=="")){
				mostrar_notificacion("Campos no validos","Debe seleccionar el orden del servicio","warning");
				return false;
			}else if($scope.productos.descripcion==""){
				mostrar_notificacion("Campos no validos","Debe ingresar la descripción","warning");
				return false;
			}
			else{
				return true;
			}
		}

		$scope.limpiar_cajas_productos = function(){
			$scope.productos = {
				'id':'',
				'id_idioma':'',
				'titulo':'',
				'descripcion':'',
				'estatus':'',
				'id_producto':'',
				'id_tipo_producto':''
			}
			$scope.activo_img = "inactivo";

			$('#text_editor').data("wysihtml5").editor.clear();
			$("#div_descripcion").html($scope.titulo_text);
			eliminarOptions("orden")
			$('#orden > option[value=""]').prop('selected', true);
			eliminarOptions("productosTodos")
			$('#productosTodos > option[value=""]').prop('selected', true);
			eliminarOptions("tipos_producto")
			$('#tipos_producto > option[value=""]').prop('selected', true);
			
		}
		//--
		$scope.consultarProductoIndividual = function(){

			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

			detalleproductosFactory.asignar_valores("",$scope.id_detalle_productos,$scope.base_url)
			detalleproductosFactory.cargar_productos(function(data){
				$scope.productos=data[0];
				//console.log(data[0]);
				$("#div_descripcion").html($scope.productos.descripcion)
				$scope.borrar_imagen.push($scope.productos.id_imagen);
				$scope.activo_img = "activo"
				ordenProductosFactory.asignar_valores($scope.productos.id_idioma,$scope.base_url,"2",$scope.productos.id_productos,$scope.productos.id_tipo_producto)
				ordenProductosFactory.cargar_orden_detalle_productos(function(data){
					$scope.ordenes=data;
					//console.log($scope.ordenes)
					//-Elimino el select
					eliminarOptions("orden")
					$.when(
						//---
						$.each( $scope.ordenes, function( indice, elemento ){
					  		agregarOptions("#orden", elemento.orden, elemento.orden)
						})
						//---
					).then(function(){
						$('#orden > option[value="'+$scope.productos.orden+'"]').prop('selected', true)
					});
					
				});
				//-Cambio 10022020
				buscarproductosFactory.asignar_valores($scope.productos.id_idioma,$scope.base_url)
				buscarproductosFactory.cargar_productos(function(data){
					$scope.productosTodos=data;
					$.when(
						$.each($scope.productosTodos, function( indice, elemento ){
							agregarOptions("#productosTodos", elemento.id, elemento.titulo)
						})
					).then(function(){
						$('#productosTodos > option[value="'+$scope.productos.id_productos+'"]').prop('selected', true);
					})
					
				});
				buscartproductosFactory.asignar_valores($scope.productos.id_productos,$scope.base_url)
				buscartproductosFactory.cargar_productos(function(data){
					$scope.tipos_producto=data;
					$.when(
						$.each($scope.tipos_producto, function( indice, elemento ){
							agregarOptions("#tipos_producto", elemento.id, elemento.titulo)
						})
					).then(function(){
						$('#tipos_producto > option[value="'+$scope.productos.id_tproducto+'"]').prop('selected', true)
					})
					
					$('#tipos_producto > option[value="0"]').prop('selected', true);
					//console.log($scope.tipos_producto);
				});

				$scope.productos.imagen = $scope.productos.ruta
				$scope.titulo_registrar = "Modificar";
				$scope.subtitulo_pagina  = "Modificar detalle de tipo de producto";
				setTimeout(function(){
					$('#idioma > option[value="'+$scope.productos.id_idioma+'"]').prop('selected', true);
					//$('#orden > option[value="'+$scope.productos.orden+'"]').prop('selected', true);
					//$('#productosTodos > option[value="'+$scope.productos.id_productos+'"]').prop('selected', true);
					//$('#tipos_producto > option[value="'+$scope.productos.id_tproducto+'"]').prop('selected', true);
					$scope.productos.inicial = $scope.productos.orden;
				},300);
				
				$scope.productos.id_tproducto.id = $scope.productos.id_tproducto;
				$("#tipos_producto").prop('disabled', true);
				//console.log($scope.tipos_producto);

			});
			
			desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

		}
		//---
		$scope.cargarOrden = function(){
			//console.log($scope.productos);

			if($scope.productos.id_productos.id){
				$scope.id_productos=$scope.productos.id_productos.id
				$scope.id_tipo_producto=$scope.productos.id_tipo_producto.id
			}else{
				$scope.id_tipo_producto=$scope.productos.id_tipo_producto
				$scope.id_productos=$scope.productos.id_productos

			}
			ordenProductosFactory.asignar_valores($scope.productos.id_idioma.id,$scope.base_url,"1",$scope.id_productos,$scope.id_tipo_producto)
			ordenProductosFactory.cargar_orden_detalle_productos(function(data){
				$scope.ordenes=data;
				//-Elimino el select
				eliminarOptions("orden")
				$.each( $scope.ordenes, function( indice, elemento ){
				  	agregarOptions("#orden", elemento.orden, elemento.orden)
				});
				$('#orden > option[value=""]').prop('selected', true);

			});
		}
		//--------------------------------------------------------
		//Cuerpo de llamados
		$scope.consultar_idioma();
		$scope.consultar_galeria_img();
		//--Si tiene id categoria .... consulto los datos relacionados a ese id, esto es cuando proviene de una consulta
		$scope.id_detalle_productos  = $("#id_detalle_productos").val();
		//.id_detalle_productos);

		if($scope.id_detalle_productos){
			$scope.consultarProductoIndividual();
		}
		//--------------------------------------------------------

	});
