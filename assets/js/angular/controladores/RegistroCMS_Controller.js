angular.module("ContentManagerApp")
	.controller("RegistroCMS_Controller", function($scope,$http,$location,serverDataMensajes,sesionFactory,galeriaFactory,idiomaFactory,tipos_usuariosFactory,coloresFactory,consultaUsuarioFactory){

		$(".li-menu").removeClass("active");
		$("#li_configuracion").addClass("active");
		$(".a-menu").removeClass("active");
		$("#registro_cms").addClass("active");

		$scope.titulo_pagina = "Registrar Usuarios CMS";
		$scope.subtitulo_pagina = "Registro";
		$scope.titulo_registrar = "Registrar";
		$scope.titulo_cons = "Consultar";
		$scope.base_url = $("#base_url").val();
		$scope.titulo_text = "Pulse aquí para ingresar el contenido"
		$scope.activo_img = "inactivo";
		$scope.searchMarcas = []
		$scope.borrar_imagen = []
		$scope.currentTab = 'datos_basicos'

		$scope.persona = {
						'id': '',
						'cedula': '',
						'login' : '',
						'nombres_apellidos' : '',
						'id_tipo_usuario' : '',
						'tipo_usuario' : '',
						'telefono' : '',
						'telefono_confirmar' : '',
						'email' : '',
						'clave' : '',
						'repite_clave' : '',
						'id_imagen':'',
						'imagen':'',
		}
		//console.log($scope.id_personas);

		$scope.consultar_tipo_usuarios = function(){
			tipos_usuariosFactory.asignar_valores("",$scope.base_url)
			tipos_usuariosFactory.cargar_tipos(function(data){
				$scope.tipo_user=data;
				//console.log($scope.idioma);
			});
		}
		///////////////////////////////////////////////////////////////
		$scope.agregarWisi = function(){
			//$('#text_editor').data("wysihtml5").editor.clear();
			$scope.color.descripcion = $(".textarea_editor").val()
			$("#div_descripcion").html($scope.color.descripcion)
			$("#cerrarModal").click();
		}
		$scope.wisi_modal = function(){
			$("#wisiModal").modal("show")
			$(".textarea_editor").data("wysihtml5").editor.setValue($scope.color.descripcion)
		}
		$scope.consultar_galeria_img = function(){
			galeriaFactory.asignar_valores('25','',$scope.base_url);//ya que es galeria de imagenes
			galeriaFactory.cargar_galeria_fa(function(data){
				$scope.galery=data;
				//console.log($scope.galery);
			});
		}

		$scope.seleccione_img_principal = function(){
			$("#modal_img1").modal("show");
		}

		$scope.seleccionar_imagen = function(event){
			var imagen = event.target.id;//Para capturar id
			var vec = $("#"+imagen).attr("data");
			var vector_data = vec.split("|")
			var id_imagen = vector_data[0];
			var ruta = vector_data[1];

			$scope.borrar_imagen.push(id_imagen);
			$scope.activo_img = "activo"
			$scope.noticias.id_imagen = id_imagen
			$scope.noticias.imagen = ruta
			//--
			$("#modal_img1").modal("hide");
			//--
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		
		$scope.seleccione_img_principal = function(){
			$("#modal_img1").modal("show");
		}
		//PARA SELECCIONAR UNA IMAGEN Y MOSTRARLA EN EL CAMPO DE IMG
		$scope.seleccionar_imagen = function(event){
				var imagen = event.target.id;//Para capturar id
				//console.log(imagen);
				var vec = $("#"+imagen).attr("data");
				var vector_data = vec.split("|")
				var id_imagen = vector_data[0];
				var ruta = vector_data[1];

				$scope.borrar_imagen.push(id_imagen);
				$scope.activo_img = "activo"
				$scope.persona.id_imagen = id_imagen
				$scope.persona.imagen = ruta
				//alert($scope.color.id_imagen);
				//--
				$("#modal_img1").modal("hide");
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.limpiar_cajas_usuario = function(){
			$scope.persona = {
				'id': '',
				'cedula': '',
				'login' : '',
				'nombres_apellidos' : '',
				'id_tipo_usuario' : '',
				'tipo_usuario' : '',
				'telefono' : '',
				'email' : '',
				'clave' : '',
				'repite_clave' : '',
				'id_imagen':'',
				'imagen':'',
			}

			$scope.activo_img = "inactivo";
			$("#div_descripcion").html($scope.titulo_text);
			$('#textarea_editor').data("wysihtml5").editor.clear();
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.registrarUsuario = function(){
			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
			 if($scope.validar_form()==true){
				//Para guardar
				if(($scope.persona.id!="")&&($scope.persona.id!=undefined)){
					$scope.modificar_personas();
				}else{
					$scope.insertar_personas();
				}

			}
			//desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
		 }
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.validar_form = function(){
			//console.log($scope.color)
			if($scope.persona.cedula==""){
				mostrar_notificacion("Campos no validos","Debe ingresar la cedula","warning");
				return false;
			}else if($scope.persona.login==""){
				mostrar_notificacion("Campos no validos","Debe ingresar un nombre de usuario","warning");
				return false;
			}else if($scope.persona.nombres_apellidos==""){
				mostrar_notificacion("Campos no validos","Debe ingresar el nombre completo","warning");
				return false;
			}else if($scope.persona.id_tipo_usuario==""){
				mostrar_notificacion("Campos no validos","Debe seleccionar un tipo de usuario","warning");
				return false;
			}else if($scope.persona.email==""){
				mostrar_notificacion("Campos no validos","Debe ingresar un correo","warning");
				return false;
			}
			var exr = /^\w+[a-z_0-9\-\.]+@\w+[0-9a-z\-\.]+\.[a-z]{2,4}$/i;
			if(!(exr.test($scope.persona.email))){
				$scope.persona.email="";
				mostrar_notificacion("Campos no validos","Debe ingresar una dirección de correo v&aacute;lida: xxxxxxxxxxxxxx@host.com","warning");
			}else if($scope.persona.telefono==""){
				mostrar_notificacion("Campos no validos","Debe ingresar un numero teléfono","warning");
				return false;
			}else if($scope.persona.telefono_confirmar==""){
				mostrar_notificacion("Campos no validos","Debes confirmar el numero de teléfono","warning");
				return false;
			}else if($scope.persona.telefono!=$scope.persona.telefono_confirmar){
				mostrar_notificacion("Campos no validos","Los numeros de telefono no coinciden","warning");
				return false;
			}
			//------------ Si es registrar:---------------------------------
			else if(($scope.persona.clave=="")&&($scope.persona.id=="")){
				mostrar_notificacion("Campos no validos","Debe ingresar una contraseña","warning");
				return false;
			}else if(($scope.persona.repite_clave=="")&&($scope.persona.id=="")){
				mostrar_notificacion("Campos no validos","Debe repetir la contraseña","warning");
				return false;
			}else if(($scope.persona.repite_clave!=$scope.persona.clave)&&($scope.persona.id=="")){
				mostrar_notificacion("Campos no validos","Las contraseñas no coinciden","warning");
				return false;
			}
			//----------------------------------------------------------------
			else if(($scope.persona.id_imagen=="NULL")||($scope.persona.id_imagen=="")){
				mostrar_notificacion("Campos no validos","Debe seleccionar la imagen","warning");
				return false;
			}
			else{
				return true;
			}
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.insertar_personas = function(){
			$http.post($scope.base_url+"/RegistroCMS/registrarPersonas",
			{
				'id': $scope.persona.id,
				'cedula': $scope.persona.cedula,
				'login' : $scope.persona.login,
				'nombres_apellidos' : $scope.persona.nombres_apellidos,
				'id_tipo_usuario' : $scope.persona.id_tipo_usuario.id,
				'telefono' : $scope.persona.telefono,
				'email' : $scope.persona.email,
				'clave' : $scope.persona.clave,
				'repite_clave' : $scope.persona.repite_clave,
				'id_idioma'  : $scope.persona.id_idioma,
				'id_imagen':$scope.persona.id_imagen,
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "registro_procesado"){
					mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
					$scope.limpiar_cajas_usuario();
				}else if($scope.mensajes.mensaje == "cedula"){
					mostrar_notificacion("Mensaje","Ya existe un usuario con la misma cedula","warning");
				}else if($scope.mensajes.mensaje == "login"){
					mostrar_notificacion("Mensaje","Nombre de usuario no disponible","warning");
				}else if($scope.mensajes.mensaje == "ambos"){
					mostrar_notificacion("Mensaje","Cedula y nombre de usuario ya existente","warning");
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
				$mensajes["mensaje"] = "no_registro";

			}).error(function(data,estatus){
				console.log(data);
			});
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.consultarPersonaIndividual = function(){
			//console.log($scope.id_personas);
			consultaUsuarioFactory.asignar_valores("",$scope.id_personas,$scope.base_url)
			consultaUsuarioFactory.cargar_personas(function(data){
				$scope.persona=data[0];
				//console.log(data[0]);
				$scope.borrar_imagen.push($scope.persona.id_imagen);
				$scope.activo_img = "activo"
				//-Cambio 10022020
				
				$scope.persona.imagen = $scope.persona.ruta
				$scope.titulo_pagina = "Modificar Usuario CMS";
				$scope.subtitulo_pagina  = "Modificar";
				$scope.titulo_registrar = "Modificar";
				setTimeout(function(){
					$('#tipo_usuario > option[value="'+$scope.persona.id_tipo_usuario+'"]').prop('selected', true);
				},300);
				$("#tipo_usuario").prop('disabled', true);
				//$("#clave").prop('disabled', true);
				//$("#repite_clave").prop('disabled', true);
			});
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.modificar_personas = function(){
			$http.post($scope.base_url+"/RegistroCMS/modificarUsuario",
			{
				'id': $scope.persona.id,
				'cedula': $scope.persona.cedula,
				'login' : $scope.persona.login,
				'nombres_apellidos' : $scope.persona.nombres_apellidos,
				'id_tipo_usuario' : $scope.persona.id_tipo_usuario.id,
				'telefono' : $scope.persona.telefono,
				'email' : $scope.persona.email,
				'clave' : $scope.persona.clave,
				'repite_clave' : $scope.persona.repite_clave,
				'id_idioma':$scope.persona.id_idioma,
				'id_imagen':$scope.persona.id_imagen,
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "modificacion_procesada"){
					mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
					//$scope.limpiar_cajas_usuario();
				}else if($scope.mensajes.mensaje == "cedula"){
					mostrar_notificacion("Mensaje","Ya existe un usuario con la misma cedula","warning");
				}else if($scope.mensajes.mensaje == "login"){
					mostrar_notificacion("Mensaje","Nombre de usuario no disponible","warning");
				}else if($scope.mensajes.mensaje == "ambos"){
					mostrar_notificacion("Mensaje","Cedula y nombre de usuario ya existente","warning");
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
				$mensajes["mensaje"] = "no_registro";

			}).error(function(data,estatus){
				//console.log(data);
			});
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.consultar_tipo_usuarios();
		$scope.consultar_galeria_img();
        $scope.id_personas  = $("#id_personas").val();
		if($scope.id_personas){
			

			$scope.consultarPersonaIndividual();
		}
	})
