angular.module("ContentManagerApp")
	.controller("ReaseguradorasConsultasController", function($scope,$http,$location,serverDataMensajes,reaseguradorasFactory,sesionFactory,idiomaFactory){
		$(".li-menu").removeClass("active");
        $(".a-menu").removeClass("active");
		
		$("#li_empresa").addClass("active");	
        $("#reaseguradoras").addClass("active");	

		$scope.titulo_pagina = "Consulta de Reaseguradoras";
		$scope.activo_img = "inactivo";
		$scope.reaseguradoras = {
						'id': '',
						'idioma': '',
						'id_idioma' : '',
						'titulo' : '',
						'descripcion' : '',
						'id_imagen' : '',
						'imagen' : 'assets/images/logo_peque.png',
						'orden':''
		}
		$scope.categorias_menu = "1";
		$scope.id_reaseguradoras = "";
		$scope.base_url = $("#base_url").val();
		//-----------------------------------------------------
		//--Cuerpo de metodos  --/
		$scope.iniciar_datatable = function(){
			//--Iniciar DataTable
			$('#myTable').DataTable({
			    language: {
			        "decimal": "",
			        "emptyTable": "No hay información",
			        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
			        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
			        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
			        "infoPostFix": "",
			        "thousands": ",",
			        "lengthMenu": "Mostrar _MENU_ Entradas",
			        "loadingRecords": "Cargando...",
			        "processing": "Procesando...",
			        "search": "Buscar:",
			        "zeroRecords": "Sin resultados encontrados",
			        "paginate": {
			            "first": "Primero",
			            "last": "Ultimo",
			            "next": "Siguiente",
			            "previous": "Anterior"
			        }
			    },
			});
			//console.log($scope.categorias);
		}
		$scope.consultar_reaseguradoras = function(){
			reaseguradorasFactory.asignar_valores("","",$scope.base_url)
			reaseguradorasFactory.cargar_reaseguradoras(function(data){
				$scope.reaseguradoras=data;
				//console.log($scope.reaseguradoras);				
			});
		}
		//---------------------------------------------------------------
		$scope.ver_reaseguradoras = function(valor){
			var id = $("#ver"+valor).attr("data");
			$scope.id_reaseguradoras = id;	
			$("#id_reaseguradoras").val($scope.id_reaseguradoras)
			let form = document.getElementById('formConsultaReaseguradoras');
			form.action = "./reaseguradoras_ver";
			form.submit();
		}
		//----------------------------------------------------------------
		$scope.activar_registro = function(event){
			$scope.id_seleccionado_reaseguradoras = []
			$scope.estatus_seleccionado_reaseguradoras = []
			var caja = event.currentTarget.id;
			var atributos = $("#"+caja).attr("data");
			var arreglo_atributos = atributos.split("|");
			$scope.id_seleccionado_reaseguradoras = arreglo_atributos[0];
			$scope.estatus_seleccionado_reaseguradoras = arreglo_atributos[1];
			$("#cabecera_mensaje").text("Información:");			
			//--
			if ($scope.estatus_seleccionado_reaseguradoras==0){
				mensaje = "Desea modificar el estatus de este registro a publicado? ";
				$scope.estatus_seleccionado_reaseguradoras=1;
			}else{
				mensaje = "Desea modificar el estatus de este registro a inactivo? ";
				$scope.estatus_seleccionado_reaseguradoras=0
			}
			//($scope.estatus_seleccionado_categorias==1) ? $scope.estatus_seleccionado_categorias=0:$scope.estatus_seleccionado_categorias=1;

			//--
			$scope.modificar_estatus("activar_inactivar",mensaje)
		}
		//----------------------------------------------------------------
		$scope.modificar_estatus = function(opcion,mensaje){
			 swal({
				  title: 'Esta seguro?',
				  text: mensaje,
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Si!',
				  cancelButtonText: 'No',
				}).then((result) => {
					  if (result.value) {
					  	$scope.accion_estatus()
					  }
				})
		}
		//----------------------------------------------------------------
		$scope.accion_estatus = function(){
			$http.post($scope.base_url+"/reaseguradoras/modificarReaseguradorasEstatus",
			{
				 'id':$scope.id_seleccionado_reaseguradoras,	
				 'estatus':$scope.estatus_seleccionado_reaseguradoras, 	

			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				//console.log($scope.mensajes)
				if($scope.mensajes.mensaje == "modificacion_procesada"){
					Swal(
					      'Realizado!',
					      'El proceso fue ejecutado.',
					      'success'
				    ).then((result) => {
						  if (result.value) {
							    let form = document.getElementById('formConsultaReaseguradoras');
								form.action = "./consultar_reaseguradoras";
								form.submit();
						  }
				    
					});
				}else{
					Swal(
					      'No realizado!',
					      'El proceso no pudo ser ejecutado.',
					      'warning'
				    )
				}
			}).error(function(data,estatus){
				console.log(data);
			});	
		}
		//----------------------------------------------------------------
		$scope.eliminar_registro = function(event){
			$scope.id_seleccionado_reaseguradoras = []
			$scope.estatus_seleccionado_reaseguradoras = []
			var caja = event.currentTarget.id;
			//alert(caja);
			var atributos = $("#"+caja).attr("data");
			var arreglo_atributos = atributos.split("|");
			$scope.id_seleccionado_reaseguradoras = arreglo_atributos[0];
			$scope.estatus_seleccionado_reaseguradoras = arreglo_atributos[1];
			//console.log($scope.estatus_seleccionado_reaseguradoras);
			$("#cabecera_mensaje").text("Información:");
			if ($scope.estatus_seleccionado_reaseguradoras==2){
				mensaje = "Desea eliminar este registro? ";
			}
			$scope.modificar_estatus("eliminar",mensaje)
		}
		//----------------------------------------------------------------
		//-- Cuerpo de funciones--/
		setTimeout(function(){
				$scope.iniciar_datatable();
		},500);
		$scope.consultar_reaseguradoras();
		
		//-----------------------------------------------------
	});	