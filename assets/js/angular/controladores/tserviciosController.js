angular.module("ContentManagerApp")
	.controller("tserviciosController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,ordenProductosFactory,idiomaFactory,tserviciosFactory,buscarserviciosFactory){
		//Cuerpo declaraciones
		$(".li-menu").removeClass("active");
        $("#li_servicios1").addClass("active");	
        $(".a-menu").removeClass("active");
        $("#t_servicio").addClass("active");	
		$scope.titulo_pagina = "Tipo de servicios";
		$scope.subtitulo_pagina  = "Registrar Tipo de servicios";
		$scope.activo_img = "inactivo";
		$scope.servicios = {
								'id':'',
								'id_idioma':'',
								'titulo':'',
								'descripcion':'',
								'estatus':'',
								'id_imagen':'',
								'imagen':'',
								'id_servicio':''
		}
		$scope.id_servicios = ""
		$scope.searchProductos = []
		$scope.borrar_imagen = []
		$scope.currentTab = 'datos_basicos'
		$scope.titulo_registrar = "Registrar";
		$scope.base_url = $("#base_url").val();
		$scope.titulo_text = "Pulse aquí para ingresar el contenido del servicio"
		//console.log($scope.servicios);


		//--------------------------------------------------------
		//Cuerpo de metodos
		$scope.consultar_idioma = function(){
			idiomaFactory.asignar_valores("",$scope.base_url)
			idiomaFactory.cargar_idioma(function(data){
				$scope.idioma=data;
				$.each( $scope.idioma, function( indice, elemento ){
				  	agregarOptions("#idioma", elemento.id, elemento.descripcion)
				});
				$('#idioma > option[value="0"]').prop('selected', true);

				//console.log($scope.idioma);
			});
		}
		$scope.buscarServicio = function(){
			if(!$scope.servicios.id_idioma){
				$("#tipos_servicio option").remove();
				agregarOptions("#tipos_servicio", "", "--Seleccione un servicio--")			
			}
			//console.log($scope.servicios.id_idioma);

			buscarserviciosFactory.asignar_valores($scope.servicios.id_idioma.id,$scope.base_url)
			buscarserviciosFactory.cargar_servicio(function(data){
				//console.log(data);
				//console.log($scope.idioma);

				$scope.mensajes = data;
				if($scope.mensajes.mensaje == "error"){
					//console.log("xd");
					$("#tipos_servicio option").remove();
					agregarOptions("#tipos_servicio", "", "--Seleccione un servicio--")

				}else{
					$("#tipos_servicio option").remove();
					$scope.tipos_servicio=data;
					agregarOptions("#tipos_servicio", "", "--Seleccione un servicio--")

				$.each( $scope.tipos_servicio, function( indice, elemento ){
					agregarOptions("#tipos_servicio", elemento.id, elemento.titulo)
				});
				$('#tipos_servicio > option[value="0"]').prop('selected', true);
			}});
			
		}
		$scope.agregarWisi = function(){
			//$('#text_editor').data("wysihtml5").editor.clear();
			$scope.servicios.descripcion = $(".textarea_editor").val()
			$("#div_descripcion").html($scope.servicios.descripcion)
			$("#cerrarModal").click();
		}
		$scope.wisi_modal = function(){
			$("#wisiModal").modal("show")
			$(".textarea_editor").data("wysihtml5").editor.setValue($scope.servicios.descripcion)
		}
		//--
		$scope.consultar_galeria_img = function(){
			galeriaFactory.asignar_valores('29','',$scope.base_url);//ya que es galeria de imagenes
			galeriaFactory.cargar_galeria_fa(function(data){
				$scope.galery=data;
				//console.log($scope.galery);
			});
		}

		$scope.seleccione_img_principal = function(){
			$("#modal_img1").modal("show");
		}

		$scope.seleccionar_imagen = function(event){
			var imagen = event.target.id;//Para capturar id
			var vec = $("#"+imagen).attr("data");
			var vector_data = vec.split("|")
			var id_imagen = vector_data[0];
			var ruta = vector_data[1];
			//console.log(vector_data[1]);

			$scope.borrar_imagen.push(id_imagen);
			$scope.activo_img = "activo"
			$scope.servicios.id_imagen = id_imagen
			$scope.servicios.imagen = ruta
			//--
			$("#modal_img1").modal("hide");
			//--
		}

		$scope.registrarservicios = function(){
			//console.log($scope.servicios);

			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
			$scope.servicios.orden = $("#orden").val();

			if($scope.validar_form()==true){
				//Para guardar
				if(($scope.servicios.id!="")&&($scope.servicios.id!=undefined)){
					$scope.modificar_servicios();
				}else{
					$scope.insertar_servicios();
				}

			}

			//desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

		}

		$scope.insertar_servicios = function(){
			//console.log($scope.servicios);

			$http.post($scope.base_url+"/Tservicios/registrarServicios",
			{
				'id'	     : $scope.servicios.id,
				'titulo'     : $scope.servicios.titulo,
				'id_imagen'  : $scope.servicios.id_imagen,
				'descripcion': $scope.servicios.descripcion,
				'id_idioma'  : $scope.servicios.id_idioma.id,
				'id_servicios'  : $scope.servicios.id_servicio.id,
				'orden': $scope.servicios.orden,

			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "registro_procesado"){
					mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
					$scope.limpiar_cajas_productos();
				}else if($scope.mensajes.mensaje == "existe"){
					mostrar_notificacion("Mensaje","Ya existe un tipo de servicio con ese titulo","warning");
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}

		$scope.modificar_servicios = function(){
			//console.log($scope.servicios);

			$http.post($scope.base_url+"/Tservicios/modificarServicios",
			{
				'id'	     : $scope.servicios.id,
				'titulo'     : $scope.servicios.titulo,
				'id_imagen'  : $scope.servicios.id_imagen,
				'descripcion': $scope.servicios.descripcion,
				'id_servicios': $scope.servicios.id_servicios,
				'id_idioma': $scope.servicios.id_idioma,
				'orden': $scope.servicios.orden,
				'inicial': $scope.servicios.inicial,
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "registro_procesado"){
					mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
					$scope.servicios.inicial = $scope.servicios.orden;
				}else if($scope.mensajes.mensaje == "existe"){
					mostrar_notificacion("Mensaje","Ya existe una noticia con ese titulo","warning");
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}

		$scope.validar_form = function(){
			//console.log($scope.servicios)
			if(($scope.servicios.id_idioma=="NULL")||($scope.servicios.id_idioma=="")||(!$scope.servicios.id_idioma)){
				mostrar_notificacion("Campos no validos","Debe seleccionar el idioma","warning");
				return false;
			}else if(($scope.servicios.id_servicio=="NULL")||($scope.servicios.id_servicio=="")||($scope.servicios.id_servicio=="0")){
				mostrar_notificacion("Campos no validos","Debe seleccionar un servicio","warning");
				return false;
			}else if($scope.servicios.titulo==""){
				mostrar_notificacion("Campos no validos","Debe ingresar el titulo","warning");
				return false;
			}else if(($scope.servicios.orden=="NULL")||($scope.servicios.orden=="")){
				mostrar_notificacion("Campos no validos","Debe seleccionar el orden del servicio","warning");
				return false;
			}else if(($scope.servicios.id_imagen=="NULL")||($scope.servicios.id_imagen=="")){
				mostrar_notificacion("Campos no validos","Debe seleccionar la imagen","warning");
				return false;
			}
			else{
				return true;
			}
		}

		$scope.limpiar_cajas_productos = function(){
			$scope.servicios = {
								'id':'',
								'id_idioma':'',
								'titulo':'',
								'descripcion':'',
								'estatus':'',
								'id_imagen':'',
								'id_servicio':''

			}
			$scope.activo_img = "inactivo";

			$('#text_editor').data("wysihtml5").editor.clear();
			$("#div_descripcion").html($scope.titulo_text);
			eliminarOptions("orden")
			$('#orden > option[value=""]').prop('selected', true);
			eliminarOptions("tipos_servicio")
			$('#tipos_servicio > option[value=""]').prop('selected', true);
		}
		//--
		$scope.consultarProductoIndividual = function(){

			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

			tserviciosFactory.asignar_valores("",$scope.id_tservicio,$scope.base_url)
			tserviciosFactory.cargar_servicios(function(data){
				$scope.servicios=data[0];
				//console.log($scope.servicios);
				ordenProductosFactory.asignar_valores($scope.servicios.id_idioma,$scope.base_url,"2",$scope.servicios.id_servicios)
				ordenProductosFactory.cargar_orden_tservicios(function(data){
					$scope.ordenes=data;
					//console.log($scope.ordenes)
					//-Elimino el select
					eliminarOptions("orden")
					$.when(
						$.each( $scope.ordenes, function( indice, elemento ){
							agregarOptions("#orden", elemento.orden, elemento.orden)
						})
					).then(function(){
						$('#orden > option[value="'+$scope.servicios.orden+'"]').prop('selected', true)
					})
					
				});



				
				//-Cambio 10022020
			

			buscarserviciosFactory.asignar_valores("",$scope.base_url)
			buscarserviciosFactory.cargar_servicio(function(data){
				$scope.tipos_servicio=data;
				$scope.servicios.id_servicio= $scope.tipos_servicio;
				$.when(
					$.each($scope.tipos_servicio, function( indice, elemento ){
						agregarOptions("#tipos_servicio", elemento.id, elemento.titulo)
					})
				).then(function(){
					$('#tipos_servicio > option[value="'+$scope.servicios.id_servicios+'"]').prop('selected', true);
				})
				//$('#tipos_servicio > option[value="0"]').prop('selected', true);
			});
			//console.log($scope.productos);
			$scope.servicios.imagen = $scope.servicios.ruta

				$scope.titulo_registrar = "Modificar";
				$scope.subtitulo_pagina  = "Modificar tipo de producto";
				setTimeout(function(){
					$('#idioma > option[value="'+$scope.servicios.id_idioma+'"]').prop('selected', true);
					//$('#tipos_servicio > option[value="'+$scope.servicios.id_servicios+'"]').prop('selected', true);
					//$('#orden > option[value="'+$scope.servicios.orden+'"]').prop('selected', true);
					$scope.servicios.inicial = $scope.servicios.orden;
				},300);
				$("#idioma").prop('disabled', true);
				$("#tipos_servicio").prop('disabled', true);


			});

			desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

		}
		//---
		$scope.cargarOrden = function(){
			if($scope.servicios.id_servicio.id){
				$scope.id_servicio=$scope.servicios.id_servicio.id
			}else{
				$scope.id_servicio=$scope.servicios.id_servicio
			}
			ordenProductosFactory.asignar_valores($scope.servicios.id_idioma.id,$scope.base_url,"1",$scope.id_servicio,"")
			ordenProductosFactory.cargar_orden_tservicios(function(data){
				$scope.ordenes=data;
				//-Elimino el select
				eliminarOptions("orden")
				$.each( $scope.ordenes, function( indice, elemento ){
				  	agregarOptions("#orden", elemento.orden, elemento.orden)
				});
				$('#orden > option[value=""]').prop('selected', true);

			});
		}
		//--------------------------------------------------------
		//Cuerpo de llamados
		$scope.consultar_idioma();
		$scope.consultar_galeria_img();
		//--Si tiene id categoria .... consulto los datos relacionados a ese id, esto es cuando proviene de una consulta
		$scope.id_tservicio  = $("#id_tservicio").val();
		//.id_tservicio);

		if($scope.id_tservicio){
			$scope.consultarProductoIndividual();
		}
		//--------------------------------------------------------

	});
