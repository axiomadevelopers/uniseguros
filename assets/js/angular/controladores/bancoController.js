angular.module("ContentManagerApp")
	.controller("bancoController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,sesionFactory,idiomaFactory,bancosFactory){
		//Cuerpo declaraciones
		$(".li-menu").removeClass("active");
        $("#li_contactos").addClass("active");	
        $(".a-menu").removeClass("active");
        $("#banco").addClass("active");		
        $scope.titulo_pagina = "Banco";
		$scope.subtitulo_pagina  = "Registrar banco";
		$scope.activo_img = "inactivo";
		$scope.banco = {
								'id':'',
								'id_idioma':'',
								'descripcion':'',
		}
		$scope.id_banco = ""
		$scope.searchBanco = []
		$scope.borrar_imagen = []
		$scope.currentTab = 'datos_basicos'
		$scope.titulo_registrar = "Registrar";
		$scope.titulo_text = "Pulse aquí para ingresar el contenido  del banco"
		$scope.base_url = $("#base_url").val();
		//--------------------------------------------------------
		//Cuerpo de metodos
		$scope.consultar_idioma = function(){
			idiomaFactory.asignar_valores("",$scope.base_url)
			idiomaFactory.cargar_idioma(function(data){
				$scope.idioma=data;
				//console.log($scope.idioma);
			});
		}
		$scope.agregarWisi = function(){
			//$('#text_editor').data("wysihtml5").editor.clear();
			$scope.banco.descripcion = $(".textarea_editor").val()
			$("#div_descripcion").html($scope.banco.descripcion)
			$("#cerrarModal").click();
		}
		$scope.wisi_modal = function(){
			$("#wisiModal").modal("show")
			$(".textarea_editor").data("wysihtml5").editor.setValue($scope.banco.descripcion)
		}
		//--
		$scope.registrarBanco = function(){

			//uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
			console.log($scope.banco);

			if($scope.validar_form()==true){
				//Para guardar
				if(($scope.banco.id!="")&&($scope.banco.id!=undefined)){
					$scope.modificar_banco();
				}else{
					$scope.insertar_banco();
				}

			}
			
			//desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
		}

		$scope.insertar_banco = function(){
			$http.post($scope.base_url+"/Banco/registrarBanco",
			{
				'descripcion': $scope.banco.descripcion,
				//'id_idioma'  : $scope.banco.id_idioma.id,
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "registro_procesado"){
					mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
					$scope.limpiar_cajas_banco();
				}else if($scope.mensajes.mensaje == "existe"){
					mostrar_notificacion("Mensaje","Ya existe un banco con ese titulo","warning");
					$scope.limpiar_cajas_banco();
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}

		$scope.modificar_banco = function(){
			$http.post($scope.base_url+"/Banco/modificarBanco",
			{
				'id'	     : $scope.banco.id,
				'descripcion': $scope.banco.descripcion,
				//'id_idioma'  : $scope.banco.id_idioma,
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "registro_procesado"){
					mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
				}else if($scope.mensajes.mensaje == "no_existe"){
					mostrar_notificacion("Mensaje","No existe el banco","warning");
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}

		$scope.validar_form = function(){
			//console.log($scope.noticias)
			//if(($scope.banco.id_idioma=="NULL")||($scope.banco.id_idioma=="")){
			//	mostrar_notificacion("Campos no validos","Debe seleccionar el idioma","warning");
			//	return false;
			//}else 
			if($scope.banco.descripcion==""){
				mostrar_notificacion("Campos no validos","Debe ingresar la descripción del banco","warning");
				return false;
			}
			else{
				return true;
			}
		}

		$scope.limpiar_cajas_banco = function(){
			$scope.banco = {
								'id':'',
								'id_idioma':'',
								'descripcion':'',
			}

			$('#text_editor').data("wysihtml5").editor.clear();
			$("#div_descripcion").html($scope.titulo_text);
		}
		//--
		$scope.consultarBancoIndividual = function(){

			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

			bancosFactory.asignar_valores("",$scope.id_banco,$scope.base_url)
			bancosFactory.cargar_banco(function(data){
				$scope.banco=data[0];
				//console.log(data[0]);
				$("#div_descripcion").html($scope.banco.descripcion)
				$scope.titulo_registrar = "Modificar";
				$scope.subtitulo_pagina  = "Modificar banco";
				setTimeout(function(){
					$('#idioma > option[value="'+$scope.banco.id_idioma+'"]').prop('selected', true);
				},300);
				$("#idioma").prop('disabled', true);
			});

			desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

		}
		//---
		//--------------------------------------------------------
		//Cuerpo de llamados
		$scope.consultar_idioma();
		//--Si tiene id categoria .... consulto los datos relacionados a ese id, esto es cuando proviene de una consulta
		$scope.id_banco  = $("#id_banco").val();
		if($scope.id_banco){
			$scope.consultarBancoIndividual();
		}else{
			$("#idioma").prop('disabled', false);
		}
		//--------------------------------------------------------
	});