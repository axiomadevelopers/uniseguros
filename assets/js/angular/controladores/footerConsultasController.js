angular.module("ContentManagerApp")
	.controller("footerConsultasController", function($scope,$http,$location,serverDataMensajes,footerFactory,sesionFactory,idiomaFactory){
		//Cuerpo declaraciones
		$(".li-menu").removeClass("active");
        $("#li_contactos").addClass("active");	
        $(".a-menu").removeClass("active");
        $("#footer").addClass("active");	
		$scope.titulo_pagina = "Consulta de Footer";
		$scope.footer = {
								'id':'',
								'id_idioma':'',
								'correo':'',
								'descripcion':'',
								'estatus':'',
		}
		$scope.categorias_menu = "1";
		$scope.id_footer = "";
		$scope.base_url = $("#base_url").val();
		//-----------------------------------------------------
		//--Cuerpo de metodos  --/
		$scope.iniciar_datatable = function(){
			//--Iniciar DataTable
			$('#myTable').DataTable({
			    language: {
			        "decimal": "",
			        "emptyTable": "No hay información",
			        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
			        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
			        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
			        "infoPostFix": "",
			        "thousands": ",",
			        "lengthMenu": "Mostrar _MENU_ Entradas",
			        "loadingRecords": "Cargando...",
			        "processing": "Procesando...",
			        "search": "Buscar:",
			        "zeroRecords": "Sin resultados encontrados",
			        "paginate": {
			            "first": "Primero",
			            "last": "Ultimo",
			            "next": "Siguiente",
			            "previous": "Anterior"
			        }
			    },
			    "order": [[ 0, "desc" ]]
			});
			//console.log($scope.categorias);
		}
		$scope.consultar_footer = function(){
			footerFactory.asignar_valores("","",$scope.base_url)
			footerFactory.cargar_footer(function(data){
				$scope.footer=data;
				//console.log($scope.footer);				
			});
		}
		//---------------------------------------------------------------
		$scope.ver_footer = function(valor){
			var id = $("#ver"+valor).attr("data");
			$scope.id_footer = id;	
			$("#id_footer").val($scope.id_footer)
			let form = document.getElementById('formConsultaFooter');
			form.action = "./footerVer";
			form.submit();
		}
		//----------------------------------------------------------------
		$scope.activar_registro = function(event){
			$scope.id_seleccionado_footer = []
			$scope.estatus_seleccionado_footer = []
			var caja = event.currentTarget.id;
			var atributos = $("#"+caja).attr("data");
			var arreglo_atributos = atributos.split("|");
			$scope.id_seleccionado_footer = arreglo_atributos[0];
			$scope.estatus_seleccionado_footer = arreglo_atributos[1];
			$("#cabecera_mensaje").text("Información:");			
			//--
			if ($scope.estatus_seleccionado_footer==0){
				mensaje = "Desea modificar el estatus de este registro a publicado? ";
				$scope.estatus_seleccionado_footer=1;
			}else{
				mensaje = "Desea modificar el estatus de este registro a inactivo? ";
				$scope.estatus_seleccionado_footer=0
			}
			//($scope.estatus_seleccionado_categorias==1) ? $scope.estatus_seleccionado_categorias=0:$scope.estatus_seleccionado_categorias=1;

			//--
			$scope.modificar_estatus("activar_inactivar",mensaje)
		}
		//----------------------------------------------------------------
		$scope.modificar_estatus = function(opcion,mensaje){
			 swal({
				  title: 'Esta seguro?',
				  text: mensaje,
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Si!',
				  cancelButtonText: 'No',
				}).then((result) => {
					  if (result.value) {
					  	$scope.accion_estatus()
					  }
				})
		}
		//----------------------------------------------------------------
		$scope.accion_estatus = function(){

			$http.post($scope.base_url+"/Footer/modificarFooterEstatus",
			{
				 'id':$scope.id_seleccionado_footer,	
				 'estatus':$scope.estatus_seleccionado_footer, 	
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "modificacion_procesada"){
					Swal(
					      'Realizado!',
					      'El proceso fue ejecutado.',
					      'success'
				    ).then((result) => {
						  if (result.value) {
							    let form = document.getElementById('formConsultaFooter');
								form.action = "./consultarFooter";
								form.submit();
						  }
				    
					});
				}else{
					Swal(
					      'No realizado!',
					      'El proceso no pudo ser ejecutado.',
					      'warning'
				    )
				}
			}).error(function(data,estatus){
				console.log(data);
			});	
		}
		//----------------------------------------------------------------
		$scope.eliminar_registro = function(event){
			$scope.id_seleccionado_footer = []
			$scope.estatus_seleccionado_footer = []
			var caja = event.currentTarget.id;
			//alert(caja);
			var atributos = $("#"+caja).attr("data");
			var arreglo_atributos = atributos.split("|");
			$scope.id_seleccionado_footer = arreglo_atributos[0];
			$scope.estatus_seleccionado_footer = arreglo_atributos[1];
			//console.log($scope.estatus_seleccionado_footer);
			$("#cabecera_mensaje").text("Información:");
			if ($scope.estatus_seleccionado_footer==2){
				mensaje = "Desea eliminar este registro? ";
			}
			$scope.modificar_estatus("eliminar",mensaje)
		}
		//----------------------------------------------------------------
		//-- Cuerpo de funciones--/
		setTimeout(function(){
				$scope.iniciar_datatable();
		},500);
		$scope.consultar_footer();
		
		//-----------------------------------------------------
	});	