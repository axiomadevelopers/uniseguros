angular.module("ContentManagerApp")
    .controller("tserviciosConsultaController", function($scope,$http,$location,serverDataMensajes,categoriasFactory,sesionFactory,idiomaFactory,tserviciosFactory){
		$(".li-menu").removeClass("active");
        $("#li_servicios1").addClass("active");	
        $(".a-menu").removeClass("active");
        $("#t_servicio").addClass("active");	

		$scope.titulo_pagina = "Consulta de Tipos de Productos";
        $scope.activo_img = "inactivo";
        $scope.productos = {
    						'id': '',
    						'idioma': '',
    						'id_idioma' : '',
    						'somos': '',
    						'digital_agency': '',
    						'estatus' : '',
    						'id_imagen' : '',
    						'imagen' : ''
    	}
        $scope.nosotros_menu = "1";
        $scope.id_nosotros = "";
        $scope.base_url = $("#base_url").val();

        $scope.iniciar_datatable = function(){
			//--Iniciar DataTable
			$('#myTable').DataTable({
			    language: {
			        "decimal": "",
			        "emptyTable": "No hay información",
			        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
			        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
			        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
			        "infoPostFix": "",
			        "thousands": ",",
			        "lengthMenu": "Mostrar _MENU_ Entradas",
			        "loadingRecords": "Cargando...",
			        "processing": "Procesando...",
			        "search": "Buscar:",
			        "zeroRecords": "Sin resultados encontrados",
			        "paginate": {
			            "first": "Primero",
			            "last": "Ultimo",
			            "next": "Siguiente",
			            "previous": "Anterior"
			        }
				},
                "order": [[4, "asc" ]]

			});
			//console.log($scope.nosotros);
		}

        $scope.consultar_tipo_productos = function(){
            tserviciosFactory.asignar_valores("","",$scope.base_url)
            tserviciosFactory.cargar_servicios(function(data){
                $.when(
                    $scope.servicios = data)
                .then(function(){
                    $scope.iniciar_datatable()
                });
            });
        }

        $scope.ver_producto = function(valor){
            var id = $("#ver"+valor).attr("data");
            $scope.id_tservicio = id;
            //console.log($scope.id_tservicio);
            $("#id_tservicio").val($scope.id_tservicio);
            let form = document.getElementById('formConsultaProductos');
            form.action = "./tserviciosVer";
            form.submit();
        }

        $scope.activar_registro = function(event){
                    $scope.id_seleccionado_productos = []
					$scope.estatus_seleccionado_productos = []
					$scope.id_seleccionado_tipo_productos = []

                    var caja = event.currentTarget.id;
                    var atributos = $("#"+caja).attr("data");
                    var arreglo_atributos = atributos.split("|");

                    $scope.id_seleccionado_productos = arreglo_atributos[0];
					$scope.estatus_seleccionado_productos = arreglo_atributos[1];
					$scope.id_seleccionado_tipo_productos =arreglo_atributos[2];


                    $("#cabecera_mensaje").text("Información:");

                    if ($scope.estatus_seleccionado_productos==0){
        				mensaje = "Desea modificar el estatus de este registro a publicado? ";
        				$scope.estatus_seleccionado_productos=1;
        			}else{
        				mensaje = "Desea modificar el estatus de este registro a inactivo? ";
        				$scope.estatus_seleccionado_productos=0
        			}
        			$scope.modificar_estatus("activar_inactivar",mensaje)
                }
                //----------------------------------------------------------------
        		$scope.modificar_estatus = function(opcion,mensaje){
        			 swal({
        				  title: 'Esta seguro?',
        				  text: mensaje,
        				  type: 'warning',
        				  showCancelButton: true,
        				  confirmButtonColor: '#3085d6',
        				  cancelButtonColor: '#d33',
        				  confirmButtonText: 'Si!',
        				  cancelButtonText: 'No',
        				}).then((result) => {
        					  if (result.value) {
        					  	$scope.accion_estatus()
        					  }
        				})
        		}
                //----------------------------------------------------------------
        		$scope.accion_estatus = function(){

        			$http.post($scope.base_url+"/Tservicios/modificarServiciosEstatus",
        			{
        				 'id':$scope.id_seleccionado_productos,
						 'estatus':$scope.estatus_seleccionado_productos,
						 'id_servicios':$scope.id_seleccionado_tipo_productos


        			}).success(function(data, estatus, headers, config){
        				$scope.mensajes  = data;
        				
                        if($scope.mensajes.mensaje == "modificacion_procesada"){
        					Swal(
        					      'Realizado!',
        					      'El proceso fue ejecutado.',
        					      'success'
        				    ).then((result) => {
        						  if (result.value) {
        							    let form = document.getElementById('formConsultaProductos');
        								form.action = "./consultar_tservicios";
        								form.submit();
        						  }

        					});
        				}else{
        					Swal(
        					      'No realizado!',
        					      'El proceso no pudo ser ejecutado.',
        					      'warning'
        				    )
        				}
        			}).error(function(data,estatus){
        				console.log(data);
        			});
        		}
        		//----------------------------------------------------------------
                //----------------------------------------------------------------
		$scope.eliminar_registro = function(event){
			$scope.id_seleccionado_productos = []
			$scope.estatus_seleccionado_productos = []
			var caja = event.currentTarget.id;
			//alert(caja);
			var atributos = $("#"+caja).attr("data");
			var arreglo_atributos = atributos.split("|");
			$scope.id_seleccionado_productos = arreglo_atributos[0];
			$scope.estatus_seleccionado_productos = arreglo_atributos[1];
			$scope.id_seleccionado_tipo_productos = arreglo_atributos[2];

			//console.log($scope.estatus_seleccionado_productos);
			$("#cabecera_mensaje").text("Información:");
			if ($scope.estatus_seleccionado_productos==2){
				mensaje = "Desea eliminar este registro? ";
			}
			$scope.modificar_estatus("eliminar",mensaje)
		}
		//----------------------------------------------------------------

        $scope.consultar_tipo_productos();
    });
