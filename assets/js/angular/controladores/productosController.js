angular.module("ContentManagerApp")
	.controller("productosController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,ordenFactory,idiomaFactory,productosFactory){
		//Cuerpo declaraciones
		$(".li-menu").removeClass("active");
        $("#li_productos1").addClass("active");	
        $(".a-menu").removeClass("active");
        $("#producto").addClass("active");	
		$scope.titulo_pagina = "Productos";
		$scope.subtitulo_pagina  = "Registrar Productos";
		$scope.activo_img = "inactivo";
		$scope.productos = {
								'id':'',
								'id_idioma':'',
								'titulo':'',
								'descripcion':'',
								'estatus':'',
								'id_imagen':'',
								'imagen':''
		}
		$scope.id_productos = ""
		$scope.searchProductos = []
		$scope.borrar_imagen = []
		$scope.currentTab = 'datos_basicos'
		$scope.titulo_registrar = "Registrar";
		$scope.base_url = $("#base_url").val();
		$scope.titulo_text = "Pulse aquí para ingresar el contenido del producto"


		//--------------------------------------------------------
		//Cuerpo de metodos
		$scope.consultar_idioma = function(){
			idiomaFactory.asignar_valores("",$scope.base_url)
			idiomaFactory.cargar_idioma(function(data){
				$scope.idioma=data;
				$.each( $scope.idioma, function( indice, elemento ){
				  	agregarOptions("#idioma", elemento.id, elemento.descripcion)
				});
				$('#idioma > option[value="0"]').prop('selected', true);

				//console.log($scope.idioma);
			});
		}
		$scope.agregarWisi = function(){
			//$('#text_editor').data("wysihtml5").editor.clear();
			$scope.productos.descripcion = $(".textarea_editor").val()
			$("#div_descripcion").html($scope.productos.descripcion)
			$("#cerrarModal").click();
		}
		$scope.wisi_modal = function(){
			$("#wisiModal").modal("show")
			$(".textarea_editor").data("wysihtml5").editor.setValue($scope.productos.descripcion)
		}
		//--
		$scope.consultar_galeria_img = function(){
			galeriaFactory.asignar_valores('26','',$scope.base_url);//ya que es galeria de imagenes
			galeriaFactory.cargar_galeria_fa(function(data){
				$scope.galery=data;
				//console.log($scope.galery);
			});
		}

		$scope.seleccione_img_principal = function(){
			$("#modal_img1").modal("show");
		}

		$scope.seleccionar_imagen = function(event){
			var imagen = event.target.id;//Para capturar id
			var vec = $("#"+imagen).attr("data");
			var vector_data = vec.split("|")
			var id_imagen = vector_data[0];
			var ruta = vector_data[1];
			//console.log(vector_data[1]);

			$scope.borrar_imagen.push(id_imagen);
			$scope.activo_img = "activo"
			$scope.productos.id_imagen = id_imagen
			$scope.productos.imagen = ruta
			//--
			$("#modal_img1").modal("hide");
			//--
		}

		$scope.registrarproductos = function(){
			
			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
			$scope.productos.orden = $("#orden").val();

			if($scope.validar_form()==true){
				//Para guardar
				if(($scope.productos.id!="")&&($scope.productos.id!=undefined)){
					$scope.modificar_productos();
				}else{
					$scope.insertar_productos();
				}

			}

			//desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

		}

		$scope.insertar_productos = function(){
			$http.post($scope.base_url+"/Productos/registrarProductos",
			{
				'id'	     : $scope.productos.id,
				'titulo'     : $scope.productos.titulo,
				'id_imagen'  : $scope.productos.id_imagen,
				'descripcion': $scope.productos.descripcion,
				'id_idioma'  : $scope.productos.id_idioma.id,
				'id_idioma'  : $scope.productos.id_idioma.id,
				'orden'		 : $scope.productos.orden,
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "registro_procesado"){
					mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
					$scope.limpiar_cajas_productos();
				}else if($scope.mensajes.mensaje == "existe"){
					mostrar_notificacion("Mensaje","Ya existe un producto con ese titulo","warning");
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}

		$scope.modificar_productos = function(){
			$http.post($scope.base_url+"/Productos/modificarProductos",
			{
				'id':$scope.productos.id,
				'titulo': $scope.productos.titulo,
				'id_imagen':$scope.productos.id_imagen,
				'descripcion':$scope.productos.descripcion,
				'id_idioma':$scope.productos.id_idioma,
				'orden':$scope.productos.orden,
				'inicial':$scope.productos.inicial,

			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "registro_procesado"){
					mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
					$scope.productos.inicial = $scope.productos.orden
				}else if($scope.mensajes.mensaje == "existe"){
					mostrar_notificacion("Mensaje","Ya existe una noticia con ese titulo","warning");
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}

		$scope.validar_form = function(){
			//console.log($scope.productos)
			if(($scope.productos.id_idioma=="NULL")||($scope.productos.id_idioma=="")){
				mostrar_notificacion("Campos no validos","Debe seleccionar el idioma","warning");
				return false;
			} else if($scope.productos.titulo==""){
				mostrar_notificacion("Campos no validos","Debe ingresar el titulo","warning");
				return false;
			}else if(($scope.productos.orden=="NULL")||($scope.productos.orden=="")){
				mostrar_notificacion("Campos no validos","Debe seleccionar el orden del servicio","warning");
				return false;
			}else if(($scope.productos.id_imagen=="NULL")||($scope.productos.id_imagen=="")){
				mostrar_notificacion("Campos no validos","Debe seleccionar la imagen","warning");
				return false;
			}
			else{
				return true;
			}
		}

		$scope.limpiar_cajas_productos = function(){
			$scope.productos = {
								'id':'',
								'id_idioma':'',
								'titulo':'',
								'descripcion':'',
								'estatus':'',
								'id_imagen':''
			}
			$scope.activo_img = "inactivo";

			$('#text_editor').data("wysihtml5").editor.clear();
			$("#div_descripcion").html($scope.titulo_text);
			eliminarOptions("orden")
			$('#orden > option[value=""]').prop('selected', true);
		}
		//--
		$scope.consultarProductoIndividual = function(){

			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

			productosFactory.asignar_valores("",$scope.id_productos,$scope.base_url)
			productosFactory.cargar_productos(function(data){
				$scope.productos=data[0];
				//console.log(data[0]);
				$("#div_descripcion").html($scope.productos.descripcion)
				$scope.borrar_imagen.push($scope.productos.id_imagen);
				$scope.activo_img = "activo"
				ordenFactory.asignar_valores($scope.productos.id_idioma,$scope.base_url,"2")
				ordenFactory.cargar_orden_productos(function(data){
					$scope.ordenes=data;
					//console.log($scope.ordenes)
					//-Elimino el select
					eliminarOptions("orden")
					$.each( $scope.ordenes, function( indice, elemento ){
					  	agregarOptions("#orden", elemento.orden, elemento.orden)
					});
				});
				//-Cambio 10022020
				
				$scope.productos.imagen = $scope.productos.ruta
				$scope.titulo_registrar = "Modificar";
				$scope.subtitulo_pagina  = "Modificar producto";
				setTimeout(function(){
					$('#idioma > option[value="'+$scope.productos.id_idioma+'"]').prop('selected', true);
					$('#orden > option[value="'+$scope.productos.orden+'"]').prop('selected', true);
					$scope.productos.inicial = $scope.productos.orden;
				},300);
				$("#idioma").prop('disabled', true);
			});

			desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

		}//------------------------------------------------------------------------------
		$scope.cargarOrden = function(){
			ordenFactory.asignar_valores($scope.productos.id_idioma.id,$scope.base_url,"1")
			ordenFactory.cargar_orden_productos(function(data){
				$scope.ordenes=data;
				//-Elimino el select
				eliminarOptions("orden")
				$.each( $scope.ordenes, function( indice, elemento ){
				  	agregarOptions("#orden", elemento.orden, elemento.orden)
				});
				$('#orden > option[value=""]').prop('selected', true);

			});
		}
		//------------------------------------------------------------------------------	
		//---
		//--------------------------------------------------------
		//Cuerpo de llamados
		$scope.consultar_idioma();
		$scope.consultar_galeria_img();
		//--Si tiene id categoria .... consulto los datos relacionados a ese id, esto es cuando proviene de una consulta
		$scope.id_productos  = $("#id_productos").val();
		//console.log($scope.id_productos);

		if($scope.id_productos){
			$scope.consultarProductoIndividual();
		}
		//--------------------------------------------------------

	});
