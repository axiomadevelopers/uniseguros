angular.module("ContentManagerApp")
	.controller("quienesSomosController" , function($scope,$http,$location,serverDataMensajes,nosotrosFactory,sesionFactory,idiomaFactory,ordenFactory,galeriaFactory){
		//Cuerpo declaraciones
		$(".li-menu").removeClass("active");
        $(".a-menu").removeClass("active");
		
		$("#li_empresa").addClass("active");	
        $("#nosotros").addClass("active");	
		
		$scope.titulo_pagina = "Quienes Somos";
		$scope.subtitulo_pagina  = "Registrar";
		$scope.activo_img = "inactivo";
		$scope.borrar_imagen = []
		$scope.currentTab = 'datos_basicos'
		$scope.titulo_registrar = "Registrar";
		$scope.titulo_cons = "Consultar";
		$scope.base_url = $("#base_url").val();
		$scope.titulo_text = "Pulse aquí para ingresar el contenido"
		//////////////////////////////////////////////////////////////////////////////
		$scope.nosotros  = {
									'id' : '',
									'titulo' : '',
									'descripcion' : '',
									'id_idioma' : '',
									'icono':'',
									'idioma' : '',
									'id_imagen':'',
		}
		$scope.lista_iconos = "";
		//////////////////////////////////////////////////////////////////////////////
		/*--Iconos --*/
		$scope.iconos = [
		    {'icono':'<i class="icon-plus"></i>'},
		    {'icono':'<i class="icon-plus-1"></i>'},
		    {'icono':'<i class="icon-minus"></i>'},
		    {'icono':'<i class="icon-minus-1"></i>'},
		    {'icono':'<i class="icon-info"></i>'},
		    {'icono':'<i class="icon-left-thin"></i>'},
		    {'icono':'<i class="icon-left-1"></i>'},
		    {'icono':'<i class="icon-up-thin"></i>'},
		    {'icono':'<i class="icon-up-1"></i>'},
		    {'icono':'<i class="icon-right-thin"></i>'},
		    {'icono':'<i class="icon-right-1"></i>'},
		    {'icono':'<i class="icon-down-thin"></i>'},
		    {'icono':'<i class="icon-down-1"></i>'},
		    {'icono':'<i class="icon-level-up"></i>'},
		    {'icono':'<i class="icon-level-down"></i>'},
		    {'icono':'<i class="icon-switch"></i>'},
		    {'icono':'<i class="icon-infinity"></i>'},
		    {'icono':'<i class="icon-plus-squared"></i>'},
		    {'icono':'<i class="icon-minus-squared"></i>'},
		    {'icono':'<i class="icon-home"></i>'},
		    {'icono':'<i class="icon-home-1"></i>'},
		    {'icono':'<i class="icon-keyboard"></i>'},
		    {'icono':'<i class="icon-erase"></i>'},
		    {'icono':'<i class="icon-pause"></i>'},
		    {'icono':'<i class="icon-pause-1"></i>'},
		    {'icono':'<i class="icon-fast-forward"></i>'},
		    {'icono':'<i class="icon-fast-fw"></i>'},
		    {'icono':'<i class="icon-fast-backward"></i>'},
		    {'icono':'<i class="icon-fast-bw"></i>'},
		    {'icono':'<i class="icon-to-end"></i>'},
		    {'icono':'<i class="icon-to-end-1"></i>'},
		    {'icono':'<i class="icon-to-start"></i>'},
		    {'icono':'<i class="icon-to-start-1"></i>'},
		    {'icono':'<i class="icon-hourglass"></i>'},
		    {'icono':'<i class="icon-stop"></i>'},
		    {'icono':'<i class="icon-stop-1"></i>'},
		    {'icono':'<i class="icon-up-dir"></i>'},
		    {'icono':'<i class="icon-up-dir-1"></i>'},
		    {'icono':'<i class="icon-play"></i>'},
		    {'icono':'<i class="icon-play-1"></i>'},
		    {'icono':'<i class="icon-right-dir"></i>'},
		    {'icono':'<i class="icon-right-dir-1"></i>'},
		    {'icono':'<i class="icon-down-dir"></i>'},
		    {'icono':'<i class="icon-down-dir-1"></i>'},
		    {'icono':'<i class="icon-left-dir"></i>'},
		    {'icono':'<i class="icon-left-dir-1"></i>'},
		    {'icono':'<i class="icon-adjust"></i>'},
		    {'icono':'<i class="icon-cloud"></i>'},
		    {'icono':'<i class="icon-cloud-1"></i>'},
		    {'icono':'<i class="icon-umbrella"></i>'},
		    {'icono':'<i class="icon-star"></i>'},
		    {'icono':'<i class="icon-star-1"></i>'},
		    {'icono':'<i class="icon-star-empty"></i>'},
		    {'icono':'<i class="icon-star-empty-1"></i>'},
		    {'icono':'<i class="icon-check-1"></i>'},
		    {'icono':'<i class="icon-cup"></i>'},
		    {'icono':'<i class="icon-left-hand"></i>'},
		    {'icono':'<i class="icon-up-hand"></i>'},
		    {'icono':'<i class="icon-right-hand"></i>'},
		    {'icono':'<i class="icon-down-hand"></i>'},
		    {'icono':'<i class="icon-menu"></i>'},
		    {'icono':'<i class="icon-th-list"></i>'},
		    {'icono':'<i class="icon-moon"></i>'},
		    {'icono':'<i class="icon-heart-empty"></i>'},
		    {'icono':'<i class="icon-heart-empty-1"></i>'},
		    {'icono':'<i class="icon-heart"></i>'},
		    {'icono':'<i class="icon-heart-1"></i>'},
		    {'icono':'<i class="icon-note"></i>'},
		    {'icono':'<i class="icon-note-beamed"></i>'},
		    {'icono':'<i class="icon-music-1"></i>'},
		    {'icono':'<i class="icon-layout"></i>'},
		    {'icono':'<i class="icon-th"></i>'},
		    {'icono':'<i class="icon-flag"></i>'},
		    {'icono':'<i class="icon-flag-1"></i>'},
		    {'icono':'<i class="icon-tools"></i>'},
		    {'icono':'<i class="icon-cog"></i>'},
		    {'icono':'<i class="icon-cog-1"></i>'},
		    {'icono':'<i class="icon-attention"></i>'},
		    {'icono':'<i class="icon-attention-1"></i>'},
		    {'icono':'<i class="icon-flash"></i>'},
		    {'icono':'<i class="icon-flash-1"></i>'},
		    {'icono':'<i class="icon-record"></i>'},
		    {'icono':'<i class="icon-cloud-thunder"></i>'},
		    {'icono':'<i class="icon-cog-alt"></i>'},
		    {'icono':'<i class="icon-scissors"></i>'},
		    {'icono':'<i class="icon-tape"></i>'},
		    {'icono':'<i class="icon-flight"></i>'},
		    {'icono':'<i class="icon-flight-1"></i>'},
		    {'icono':'<i class="icon-mail"></i>'},
		    {'icono':'<i class="icon-mail-1"></i>'},
		    {'icono':'<i class="icon-edit"></i>'},
		    {'icono':'<i class="icon-pencil"></i>'},
		    {'icono':'<i class="icon-pencil-1"></i>'},
		    {'icono':'<i class="icon-feather"></i>'},
		    {'icono':'<i class="icon-check"></i>'},
		    {'icono':'<i class="icon-ok"></i>'},
		    {'icono':'<i class="icon-ok-circle"></i>'},
		    {'icono':'<i class="icon-cancel"></i>'},
		    {'icono':'<i class="icon-cancel-1"></i>'},
		    {'icono':'<i class="icon-cancel-circled"></i>'},
		    {'icono':'<i class="icon-cancel-circle"></i>'},
		    {'icono':'<i class="icon-asterisk"></i>'},
		    {'icono':'<i class="icon-cancel-squared"></i>'},
		    {'icono':'<i class="icon-help"></i>'},
		    {'icono':'<i class="icon-attention-circle"></i>'},
		    {'icono':'<i class="icon-quote"></i>'},
		    {'icono':'<i class="icon-plus-circled"></i>'},
		    {'icono':'<i class="icon-plus-circle"></i>'},
		    {'icono':'<i class="icon-minus-circled"></i>'},
		    {'icono':'<i class="icon-minus-circle"></i>'},
		    {'icono':'<i class="icon-right"></i>'},
		    {'icono':'<i class="icon-direction"></i>'},
		    {'icono':'<i class="icon-forward"></i>'},
		    {'icono':'<i class="icon-forward-1"></i>'},
		    {'icono':'<i class="icon-ccw"></i>'},
		    {'icono':'<i class="icon-cw"></i>'},
		    {'icono':'<i class="icon-cw-1"></i>'},
		    {'icono':'<i class="icon-left"></i>'},
		    {'icono':'<i class="icon-up"></i>'},
		    {'icono':'<i class="icon-down"></i>'},
		    {'icono':'<i class="icon-resize-vertical"></i>'},
		    {'icono':'<i class="icon-resize-horizontal"></i>'},
		    {'icono':'<i class="icon-eject"></i>'},
		    {'icono':'<i class="icon-list-add"></i>'},
		    {'icono':'<i class="icon-list"></i>'},
		    {'icono':'<i class="icon-left-bold"></i>'},
		    {'icono':'<i class="icon-right-bold"></i>'},
		    {'icono':'<i class="icon-up-bold"></i>'},
		    {'icono':'<i class="icon-down-bold"></i>'},
		    {'icono':'<i class="icon-user-add"></i>'},
		    {'icono':'<i class="icon-star-half"></i>'},
		    {'icono':'<i class="icon-ok-circle2"></i>'},
		    {'icono':'<i class="icon-cancel-circle2"></i>'},
		    {'icono':'<i class="icon-help-circled"></i>'},
		    {'icono':'<i class="icon-help-circle"></i>'},
		    {'icono':'<i class="icon-info-circled"></i>'},
		    {'icono':'<i class="icon-info-circle"></i>'},
		    {'icono':'<i class="icon-th-large"></i>'},
		    {'icono':'<i class="icon-eye"></i>'},
		    {'icono':'<i class="icon-eye-1"></i>'},
		    {'icono':'<i class="icon-eye-off"></i>'},
		    {'icono':'<i class="icon-tag"></i>'},
		    {'icono':'<i class="icon-tag-1"></i>'},
		    {'icono':'<i class="icon-tags"></i>'},
		    {'icono':'<i class="icon-camera-alt"></i>'},
		    {'icono':'<i class="icon-upload-cloud"></i>'},
		    {'icono':'<i class="icon-reply"></i>'},
		    {'icono':'<i class="icon-reply-all"></i>'},
		    {'icono':'<i class="icon-code"></i>'},
		    {'icono':'<i class="icon-export"></i>'},
		    {'icono':'<i class="icon-export-1"></i>'},
		    {'icono':'<i class="icon-print"></i>'},
		    {'icono':'<i class="icon-print-1"></i>'},
		    {'icono':'<i class="icon-retweet"></i>'},
		    {'icono':'<i class="icon-retweet-1"></i>'},
		    {'icono':'<i class="icon-comment"></i>'},
		    {'icono':'<i class="icon-comment-1"></i>'},
		    {'icono':'<i class="icon-chat"></i>'},
		    {'icono':'<i class="icon-chat-1"></i>'},
		    {'icono':'<i class="icon-vcard"></i>'},
		    {'icono':'<i class="icon-address"></i>'},
		    {'icono':'<i class="icon-location"></i>'},
		    {'icono':'<i class="icon-location-1"></i>'},
		    {'icono':'<i class="icon-map"></i>'},
		    {'icono':'<i class="icon-compass"></i>'},
		    {'icono':'<i class="icon-trash"></i>'},
		    {'icono':'<i class="icon-trash-1"></i>'},
		    {'icono':'<i class="icon-doc"></i>'},
		    {'icono':'<i class="icon-doc-text-inv"></i>'},
		    {'icono':'<i class="icon-docs"></i>'},
		    {'icono':'<i class="icon-doc-landscape"></i>'},
		    {'icono':'<i class="icon-archive"></i>'},
		    {'icono':'<i class="icon-rss"></i>'},
		    {'icono':'<i class="icon-share"></i>'},
		    {'icono':'<i class="icon-basket"></i>'},
		    {'icono':'<i class="icon-basket-1"></i>'},
		    {'icono':'<i class="icon-shareable"></i>'},
		    {'icono':'<i class="icon-login"></i>'},
		    {'icono':'<i class="icon-login-1"></i>'},
		    {'icono':'<i class="icon-logout"></i>'},
		    {'icono':'<i class="icon-logout-1"></i>'},
		    {'icono':'<i class="icon-volume"></i>'},
		    {'icono':'<i class="icon-resize-full"></i>'},
		    {'icono':'<i class="icon-resize-full-1"></i>'},
		    {'icono':'<i class="icon-resize-small"></i>'},
		    {'icono':'<i class="icon-resize-small-1"></i>'},
		    {'icono':'<i class="icon-popup"></i>'},
		    {'icono':'<i class="icon-publish"></i>'},
		    {'icono':'<i class="icon-window"></i>'},
		    {'icono':'<i class="icon-arrow-combo"></i>'},
		    {'icono':'<i class="icon-zoom-in"></i>'},
		    {'icono':'<i class="icon-chart-pie"></i>'},
		    {'icono':'<i class="icon-zoom-out"></i>'},
		    {'icono':'<i class="icon-language"></i>'},
		    {'icono':'<i class="icon-air"></i>'},
		    {'icono':'<i class="icon-database"></i>'},
		    {'icono':'<i class="icon-drive"></i>'},
		    {'icono':'<i class="icon-bucket"></i>'},
		    {'icono':'<i class="icon-thermometer"></i>'},
		    {'icono':'<i class="icon-down-circled"></i>'},
		    {'icono':'<i class="icon-down-circle2"></i>'},
		    {'icono':'<i class="icon-left-circled"></i>'},
		    {'icono':'<i class="icon-right-circled"></i>'},
		    {'icono':'<i class="icon-up-circled"></i>'},
		    {'icono':'<i class="icon-up-circle2"></i>'},
		    {'icono':'<i class="icon-down-open"></i>'},
		    {'icono':'<i class="icon-down-open-1"></i>'},
		    {'icono':'<i class="icon-left-open"></i>'},
		    {'icono':'<i class="icon-left-open-1"></i>'},
		    {'icono':'<i class="icon-right-open"></i>'},
		    {'icono':'<i class="icon-right-open-1"></i>'},
		    {'icono':'<i class="icon-up-open"></i>'},
		    {'icono':'<i class="icon-up-open-1"></i>'},
		    {'icono':'<i class="icon-down-open-mini"></i>'},
		    {'icono':'<i class="icon-arrows-cw"></i>'},
		    {'icono':'<i class="icon-left-open-mini"></i>'},
		    {'icono':'<i class="icon-play-circle2"></i>'},
		    {'icono':'<i class="icon-right-open-mini"></i>'},
		    {'icono':'<i class="icon-to-end-alt"></i>'},
		    {'icono':'<i class="icon-up-open-mini"></i>'},
		    {'icono':'<i class="icon-to-start-alt"></i>'},
		    {'icono':'<i class="icon-down-open-big"></i>'},
		    {'icono':'<i class="icon-left-open-big"></i>'},
		    {'icono':'<i class="icon-right-open-big"></i>'},
		    {'icono':'<i class="icon-up-open-big"></i>'},
		    {'icono':'<i class="icon-progress-0"></i>'},
		    {'icono':'<i class="icon-progress-1"></i>'},
		    {'icono':'<i class="icon-progress-2"></i>'},
		    {'icono':'<i class="icon-progress-3"></i>'},
		    {'icono':'<i class="icon-back-in-time"></i>'},
		    {'icono':'<i class="icon-network"></i>'},
		    {'icono':'<i class="icon-inbox"></i>'},
		    {'icono':'<i class="icon-inbox-1"></i>'},
		    {'icono':'<i class="icon-install"></i>'},
		    {'icono':'<i class="icon-font"></i>'},
		    {'icono':'<i class="icon-bold"></i>'},
		    {'icono':'<i class="icon-italic"></i>'},
		    {'icono':'<i class="icon-text-height"></i>'},
		    {'icono':'<i class="icon-text-width"></i>'},
		    {'icono':'<i class="icon-align-left"></i>'},
		    {'icono':'<i class="icon-align-center"></i>'},
		    {'icono':'<i class="icon-align-right"></i>'},
		    {'icono':'<i class="icon-align-justify"></i>'},
		    {'icono':'<i class="icon-list-1"></i>'},
		    {'icono':'<i class="icon-indent-left"></i>'},
		    {'icono':'<i class="icon-indent-right"></i>'},
		    {'icono':'<i class="icon-lifebuoy"></i>'},
		    {'icono':'<i class="icon-mouse"></i>'},
		    {'icono':'<i class="icon-dot"></i>'},
		    {'icono':'<i class="icon-dot-2"></i>'},
		    {'icono':'<i class="icon-dot-3"></i>'},
		    {'icono':'<i class="icon-suitcase"></i>'},
		    {'icono':'<i class="icon-off"></i>'},
		    {'icono':'<i class="icon-road"></i>'},
		    {'icono':'<i class="icon-flow-cascade"></i>'},
		    {'icono':'<i class="icon-list-alt"></i>'},
		    {'icono':'<i class="icon-flow-branch"></i>'},
		    {'icono':'<i class="icon-qrcode"></i>'},
		    {'icono':'<i class="icon-flow-tree"></i>'},
		    {'icono':'<i class="icon-barcode"></i>'},
		    {'icono':'<i class="icon-flow-line"></i>'},
		    {'icono':'<i class="icon-ajust"></i>'},
		    {'icono':'<i class="icon-tint"></i>'},
		    {'icono':'<i class="icon-brush"></i>'},
		    {'icono':'<i class="icon-paper-plane"></i>'},
		    {'icono':'<i class="icon-magnet"></i>'},
		    {'icono':'<i class="icon-magnet-1"></i>'},
		    {'icono':'<i class="icon-gauge"></i>'},
		    {'icono':'<i class="icon-traffic-cone"></i>'},
		    {'icono':'<i class="icon-cc"></i>'},
		    {'icono':'<i class="icon-cc-by"></i>'},
		    {'icono':'<i class="icon-cc-nc"></i>'},
		    {'icono':'<i class="icon-cc-nc-eu"></i>'},
		    {'icono':'<i class="icon-cc-nc-jp"></i>'},
		    {'icono':'<i class="icon-cc-sa"></i>'},
		    {'icono':'<i class="icon-cc-nd"></i>'},
		    {'icono':'<i class="icon-cc-pd"></i>'},
		    {'icono':'<i class="icon-cc-zero"></i>'},
		    {'icono':'<i class="icon-cc-share"></i>'},
		    {'icono':'<i class="icon-cc-remix"></i>'},
		    {'icono':'<i class="icon-move"></i>'},
		    {'icono':'<i class="icon-link-ext"></i>'},
		    {'icono':'<i class="icon-check-empty"></i>'},
		    {'icono':'<i class="icon-bookmark-empty"></i>'},
		    {'icono':'<i class="icon-phone-squared"></i>'},
		    {'icono':'<i class="icon-twitter"></i>'},
		    {'icono':'<i class="icon-facebook"></i>'},
		    {'icono':'<i class="icon-github"></i>'},
		    {'icono':'<i class="icon-rss-1"></i>'},
		    {'icono':'<i class="icon-hdd"></i>'},
		    {'icono':'<i class="icon-certificate"></i>'},
		    {'icono':'<i class="icon-left-circled-1"></i>'},
		    {'icono':'<i class="icon-right-circled-1"></i>'},
		    {'icono':'<i class="icon-up-circled-1"></i>'},
		    {'icono':'<i class="icon-down-circled-1"></i>'},
		    {'icono':'<i class="icon-tasks"></i>'},
		    {'icono':'<i class="icon-filter"></i>'},
		    {'icono':'<i class="icon-resize-full-alt"></i>'},
		    {'icono':'<i class="icon-beaker"></i>'},
		    {'icono':'<i class="icon-docs-1"></i>'},
		    {'icono':'<i class="icon-blank"></i>'},
		    {'icono':'<i class="icon-menu-1"></i>'},
		    {'icono':'<i class="icon-list-bullet"></i>'},
		    {'icono':'<i class="icon-list-numbered"></i>'},
		    {'icono':'<i class="icon-strike"></i>'},
		    {'icono':'<i class="icon-underline"></i>'},
		    {'icono':'<i class="icon-table"></i>'},
		    {'icono':'<i class="icon-magic"></i>'},
		    {'icono':'<i class="icon-pinterest-circled-1"></i>'},
		    {'icono':'<i class="icon-pinterest-squared"></i>'},
		    {'icono':'<i class="icon-gplus-squared"></i>'},
		    {'icono':'<i class="icon-gplus"></i>'},
		    {'icono':'<i class="icon-money"></i>'},
		    {'icono':'<i class="icon-columns"></i>'},
		    {'icono':'<i class="icon-sort"></i>'},
		    {'icono':'<i class="icon-sort-down"></i>'},
		    {'icono':'<i class="icon-sort-up"></i>'},
		    {'icono':'<i class="icon-mail-alt"></i>'},
		    {'icono':'<i class="icon-linkedin"></i>'},
		    {'icono':'<i class="icon-gauge-1"></i>'},
		    {'icono':'<i class="icon-comment-empty"></i>'},
		    {'icono':'<i class="icon-chat-empty"></i>'},
		    {'icono':'<i class="icon-sitemap"></i>'},
		    {'icono':'<i class="icon-paste"></i>'},
		    {'icono':'<i class="icon-user-md"></i>'},
		    {'icono':'<i class="icon-s-github"></i>'},
		    {'icono':'<i class="icon-github-squared"></i>'},
		    {'icono':'<i class="icon-github-circled"></i>'},
		    {'icono':'<i class="icon-s-flickr"></i>'},
		    {'icono':'<i class="icon-twitter-squared"></i>'},
		    {'icono':'<i class="icon-s-vimeo"></i>'},
		    {'icono':'<i class="icon-vimeo-circled"></i>'},
		    {'icono':'<i class="icon-facebook-squared-1"></i>'},
		    {'icono':'<i class="icon-s-twitter"></i>'},
		    {'icono':'<i class="icon-twitter-circled"></i>'},
		    {'icono':'<i class="icon-s-facebook"></i>'},
		    {'icono':'<i class="icon-linkedin-squared"></i>'},
		    {'icono':'<i class="icon-facebook-circled"></i>'},
		    {'icono':'<i class="icon-s-gplus"></i>'},
		    {'icono':'<i class="icon-gplus-circled"></i>'},
		    {'icono':'<i class="icon-s-pinterest"></i>'},
		    {'icono':'<i class="icon-pinterest-circled"></i>'},
		    {'icono':'<i class="icon-s-tumblr"></i>'},
		    {'icono':'<i class="icon-tumblr-circled"></i>'},
		    {'icono':'<i class="icon-s-linkedin"></i>'},
		    {'icono':'<i class="icon-linkedin-circled"></i>'},
		    {'icono':'<i class="icon-s-dribbble"></i>'},
		    {'icono':'<i class="icon-dribbble-circled"></i>'},
		    {'icono':'<i class="icon-s-stumbleupon"></i>'},
		    {'icono':'<i class="icon-stumbleupon-circled"></i>'},
		    {'icono':'<i class="icon-s-lastfm"></i>'},
		    {'icono':'<i class="icon-lastfm-circled"></i>'},
		    {'icono':'<i class="icon-rdio"></i>'},
		    {'icono':'<i class="icon-rdio-circled"></i>'},
		    {'icono':'<i class="icon-spotify"></i>'},
		    {'icono':'<i class="icon-s-spotify-circled"></i>'},
		    {'icono':'<i class="icon-qq"></i>'},
		    {'icono':'<i class="icon-s-instagrem"></i>'},
		    {'icono':'<i class="icon-dropbox"></i>'},
		    {'icono':'<i class="icon-s-evernote"></i>'},
		    {'icono':'<i class="icon-flattr"></i>'},
		    {'icono':'<i class="icon-s-skype"></i>'},
		    {'icono':'<i class="icon-skype-circled"></i>'},
		    {'icono':'<i class="icon-renren"></i>'},
		    {'icono':'<i class="icon-sina-weibo"></i>'},
		    {'icono':'<i class="icon-s-paypal"></i>'},
		    {'icono':'<i class="icon-s-picasa"></i>'},
		    {'icono':'<i class="icon-s-soundcloud"></i>'},
		    {'icono':'<i class="icon-s-behance"></i>'},
		    {'icono':'<i class="icon-google-circles"></i>'},
		    {'icono':'<i class="icon-vkontakte"></i>'},
		    {'icono':'<i class="icon-smashing"></i>'},
		    {'icono':'<i class="icon-db-shape"></i>'},
		    {'icono':'<i class="icon-sweden"></i>'},
		    {'icono':'<i class="icon-logo-db"></i>'},
		    {'icono':'<i class="icon-picture"></i>'},
		    {'icono':'<i class="icon-picture-1"></i>'},
		    {'icono':'<i class="icon-globe"></i>'},
		    {'icono':'<i class="icon-globe-1"></i>'},
		    {'icono':'<i class="icon-leaf-1"></i>'},
		    {'icono':'<i class="icon-lemon"></i>'},
		    {'icono':'<i class="icon-glass"></i>'},
		    {'icono':'<i class="icon-gift"></i>'},
		    {'icono':'<i class="icon-graduation-cap"></i>'},
		    {'icono':'<i class="icon-mic"></i>'},
		    {'icono':'<i class="icon-videocam"></i>'},
		    {'icono':'<i class="icon-headphones"></i>'},
		    {'icono':'<i class="icon-palette"></i>'},
		    {'icono':'<i class="icon-ticket"></i>'},
		    {'icono':'<i class="icon-video"></i>'},
		    {'icono':'<i class="icon-video-1"></i>'},
		    {'icono':'<i class="icon-target"></i>'},
		    {'icono':'<i class="icon-target-1"></i>'},
		    {'icono':'<i class="icon-music"></i>'},
		    {'icono':'<i class="icon-trophy"></i>'},
		    {'icono':'<i class="icon-award"></i>'},
		    {'icono':'<i class="icon-thumbs-up"></i>'},
		    {'icono':'<i class="icon-thumbs-up-1"></i>'},
		    {'icono':'<i class="icon-thumbs-down"></i>'},
		    {'icono':'<i class="icon-thumbs-down-1"></i>'},
		    {'icono':'<i class="icon-bag"></i>'},
		    {'icono':'<i class="icon-user"></i>'},
		    {'icono':'<i class="icon-user-1"></i>'},
		    {'icono':'<i class="icon-users"></i>'},
		    {'icono':'<i class="icon-users-1"></i>'},
		    {'icono':'<i class="icon-lamp"></i>'},
		    {'icono':'<i class="icon-alert"></i>'},
		    {'icono':'<i class="icon-water"></i>'},
		    {'icono':'<i class="icon-droplet"></i>'},
		    {'icono':'<i class="icon-credit-card"></i>'},
		    {'icono':'<i class="icon-credit-card-1"></i>'},
		    {'icono':'<i class="icon-monitor"></i>'},
		    {'icono':'<i class="icon-briefcase"></i>'},
		    {'icono':'<i class="icon-briefcase-1"></i>'},
		    {'icono':'<i class="icon-floppy"></i>'},
		    {'icono':'<i class="icon-floppy-1"></i>'},
		    {'icono':'<i class="icon-cd"></i>'},
		    {'icono':'<i class="icon-folder"></i>'},
		    {'icono':'<i class="icon-folder-1"></i>'},
		    {'icono':'<i class="icon-folder-open"></i>'},
		    {'icono':'<i class="icon-doc-text"></i>'},
		    {'icono':'<i class="icon-doc-1"></i>'},
		    {'icono':'<i class="icon-calendar"></i>'},
		    {'icono':'<i class="icon-calendar-1"></i>'},
		    {'icono':'<i class="icon-chart-line"></i>'},
		    {'icono':'<i class="icon-chart-bar"></i>'},
		    {'icono':'<i class="icon-chart-bar-1"></i>'},
		    {'icono':'<i class="icon-clipboard"></i>'},
		    {'icono':'<i class="icon-pin"></i>'},
		    {'icono':'<i class="icon-attach"></i>'},
		    {'icono':'<i class="icon-attach-1"></i>'},
		    {'icono':'<i class="icon-bookmarks"></i>'},
		    {'icono':'<i class="icon-book"></i>'},
		    {'icono':'<i class="icon-book-1"></i>'},
		    {'icono':'<i class="icon-book-open"></i>'},
		    {'icono':'<i class="icon-phone"></i>'},
		    {'icono':'<i class="icon-phone-1"></i>'},
		    {'icono':'<i class="icon-megaphone"></i>'},
		    {'icono':'<i class="icon-megaphone-1"></i>'},
		    {'icono':'<i class="icon-upload"></i>'},
		    {'icono':'<i class="icon-upload-1"></i>'},
		    {'icono':'<i class="icon-download"></i>'},
		    {'icono':'<i class="icon-download-1"></i>'},
		    {'icono':'<i class="icon-box"></i>'},
		    {'icono':'<i class="icon-newspaper"></i>'},
		    {'icono':'<i class="icon-mobile"></i>'},
		    {'icono':'<i class="icon-signal"></i>'},
		    {'icono':'<i class="icon-signal-1"></i>'},
		    {'icono':'<i class="icon-camera"></i>'},
		    {'icono':'<i class="icon-camera-1"></i>'},
		    {'icono':'<i class="icon-shuffle"></i>'},
		    {'icono':'<i class="icon-shuffle-1"></i>'},
		    {'icono':'<i class="icon-loop"></i>'},
		    {'icono':'<i class="icon-arrows-ccw"></i>'},
		    {'icono':'<i class="icon-light-down"></i>'},
		    {'icono':'<i class="icon-light-up"></i>'},
		    {'icono':'<i class="icon-mute"></i>'},
		    {'icono':'<i class="icon-volume-off"></i>'},
		    {'icono':'<i class="icon-volume-down"></i>'},
		    {'icono':'<i class="icon-sound"></i>'},
		    {'icono':'<i class="icon-volume-up"></i>'},
		    {'icono':'<i class="icon-battery"></i>'},
		    {'icono':'<i class="icon-search"></i>'},
		    {'icono':'<i class="icon-search-1"></i>'},
		    {'icono':'<i class="icon-key"></i>'},
		    {'icono':'<i class="icon-key-1"></i>'},
		    {'icono':'<i class="icon-lock"></i>'},
		    {'icono':'<i class="icon-lock-1"></i>'},
		    {'icono':'<i class="icon-lock-open"></i>'},
		    {'icono':'<i class="icon-lock-open-1"></i>'},
		    {'icono':'<i class="icon-bell"></i>'},
		    {'icono':'<i class="icon-bell-1"></i>'},
		    {'icono':'<i class="icon-bookmark"></i>'},
		    {'icono':'<i class="icon-bookmark-1"></i>'},
		    {'icono':'<i class="icon-link"></i>'},
		    {'icono':'<i class="icon-link-1"></i>'},
		    {'icono':'<i class="icon-back"></i>'},
		    {'icono':'<i class="icon-fire"></i>'},
		    {'icono':'<i class="icon-flashlight"></i>'},
		    {'icono':'<i class="icon-wrench"></i>'},
		    {'icono':'<i class="icon-hammer"></i>'},
		    {'icono':'<i class="icon-chart-area"></i>'},
		    {'icono':'<i class="icon-clock"></i>'},
		    {'icono':'<i class="icon-clock-1"></i>'},
		    {'icono':'<i class="icon-rocket"></i>'},
		    {'icono':'<i class="icon-truck"></i>'},
		    {'icono':'<i class="icon-block"></i>'},
		    {'icono':'<i class="icon-block-1"></i>'},
		    {'icono':'<i class="icon-s-rss"></i>'},
		    {'icono':'<i class="icon-s-twitter"></i>'},
		    {'icono':'<i class="icon-s-facebook"></i>'},
		    {'icono':'<i class="icon-s-dribbble"></i>'},
		    {'icono':'<i class="icon-s-pinterest"></i>'},
		    {'icono':'<i class="icon-s-flickr"></i>'},
		    {'icono':'<i class="icon-s-vimeo"></i>'},
		    {'icono':'<i class="icon-s-youtube"></i>'},
		    {'icono':'<i class="icon-s-skype"></i>'},
		    {'icono':'<i class="icon-s-tumblr"></i>'},
		    {'icono':'<i class="icon-s-linkedin"></i>'},
		    {'icono':'<i class="icon-s-behance"></i>'},
		    {'icono':'<i class="icon-s-github"></i>'},
		    {'icono':'<i class="icon-s-delicious"></i>'},
		    {'icono':'<i class="icon-s-500px"></i>'},
		    {'icono':'<i class="icon-s-grooveshark"></i>'},
		    {'icono':'<i class="icon-s-forrst"></i>'},
		    {'icono':'<i class="icon-s-digg"></i>'},
		    {'icono':'<i class="icon-s-blogger"></i>'},
		    {'icono':'<i class="icon-s-klout"></i>'},
		    {'icono':'<i class="icon-s-dropbox"></i>'},
		    {'icono':'<i class="icon-s-songkick"></i>'},
		    {'icono':'<i class="icon-s-posterous"></i>'},
		    {'icono':'<i class="icon-s-appnet"></i>'},
		    {'icono':'<i class="icon-s-github"></i>'},
		    {'icono':'<i class="icon-s-gplus"></i>'},
		    {'icono':'<i class="icon-s-stumbleupon"></i>'},
		    {'icono':'<i class="icon-s-lastfm"></i>'},
		    {'icono':'<i class="icon-s-spotify"></i>'},
		    {'icono':'<i class="icon-s-instagram"></i>'},
		    {'icono':'<i class="icon-s-evernote"></i>'},
		    {'icono':'<i class="icon-s-paypal"></i>'},
		    {'icono':'<i class="icon-s-picasa"></i>'},
		    {'icono':'<i class="icon-s-soundcloud"></i>'},
		]
		//////////////////////////////////////////////////////////////////////////////
		$scope.consultar_idioma = function(){
			idiomaFactory.asignar_valores("",$scope.base_url)
			idiomaFactory.cargar_idioma(function(data){
				$scope.idioma=data;
				//console.log($scope.idioma);
			});
		}
		//////////////////////////////////////////////////////////////////////////////
		$scope.agregarWisi = function(){
			$scope.nosotros.descripcion = $(".textarea_editor").val()
			$("#div_descripcion").html($scope.nosotros.descripcion)
			$("#cerrarModal").click();
		}
		$scope.wisi_modal = function(){
			$("#wisiModal").modal("show")
			$(".textarea_editor").data("wysihtml5").editor.setValue($scope.nosotros.descripcion)
		}
		//--
		$scope.consultar_galeria_img = function(){
			galeriaFactory.asignar_valores('3','',$scope.base_url);//ya que es galeria de imagenes
			galeriaFactory.cargar_galeria_fa(function(data){
				$scope.galery=data;
				//console.log($scope.galery);
			});
		}

		$scope.seleccione_img_principal = function(){
			$("#modal_img1").modal("show");
		}

		$scope.seleccionar_imagen = function(event){
			var imagen = event.target.id;//Para capturar id
			var vec = $("#"+imagen).attr("data");
			var vector_data = vec.split("|")
			var id_imagen = vector_data[0];
			var ruta = vector_data[1];

			$scope.borrar_imagen.push(id_imagen);
			$scope.activo_img = "activo"
			$scope.nosotros.id_imagen = id_imagen
			$scope.nosotros.imagen = ruta
			//--
			$("#modal_img1").modal("hide");
			//--
		}

		//--Modal de iconos
		$scope.icon_modal = function(){
			$("#iconModal").modal("show")
		}
		//--
		$scope.agregarIcon = function(){
			$("#cerrarModal").click();
		}
		/////////////////////////////////////////////////////////////////////////////////////
		$scope.registrarCategoriasProd = function(){
			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

			$scope.nosotros.orden = $("#orden").val();

			if($scope.validar_form()==true){
				//Para guardar
				$scope.nosotros.id = $scope.id_nosotros;

				if(($scope.nosotros.id!="")&&($scope.nosotros.id!=undefined)){
					$scope.modificar_nosotros();
				}else{
					$scope.insertar_nosotros();
				}

			}

			//desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

		}
		//////////////////////////
		$scope.validar_form = function(){
						
			if(($scope.nosotros.id_idioma=="")||($scope.nosotros.id_idioma=="")){
				mostrar_notificacion("Campos no validos","Debe seleccionar un idioma","warning");
				return false;
			}else if(($scope.nosotros.titulo=="")||($scope.nosotros.titulo=="")){
				mostrar_notificacion("Campos no validos","Debe registrar un título","warning");
				return false;
			}else if(($scope.nosotros.descripcion=="NULL")||($scope.nosotros.descripcion=="")){
				mostrar_notificacion("Campos no validos","Debe registrar una descripción","warning");
				return false;
			}/*
			else if(($scope.nosotros.icono=="NULL")||($scope.nosotros.icono=="")){
				mostrar_notificacion("Campos no validos","Debe seleccionar un icono","warning");
				return false;
			}*/else if(($scope.nosotros.orden=="NULL")||($scope.nosotros.orden=="")){
				mostrar_notificacion("Campos no validos","Debe seleccionar el orden del servicio","warning");
				return false;
			}
			else{
				return true;
			}
		}
		////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.limpiar_cajas_quienes_somos = function(){
			$scope.nosotros  = {
										'id' : '',
										'titulo' : '',
										'descripcion' : '',
										'id_idioma' : '',
										'idioma' : '',
										'icono':'',
										'id_imagen':'',
			}
			$scope.activo_img = "inactivo";

			$scope.lista_iconos = ""
			$(".iconoSeleccionado").html("")
			$("#div_descripcion").html($scope.titulo_text);
			$('#textarea_editor').data("wysihtml5").editor.clear();
			eliminarOptions("orden")
			$('#orden > option[value=""]').prop('selected', true);
		}
		////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.insertar_nosotros = function(){
			$http.post($scope.base_url+"/QuienesSomos/registrarnosotros",
			{
				'titulo' : $scope.nosotros.titulo,
				'descripcion': $scope.nosotros.descripcion,
				'id_idioma' : $scope.nosotros.id_idioma.id,
				'icono': $scope.nosotros.icono,
				'orden': $scope.nosotros.orden,
				'id_imagen': $scope.nosotros.id_imagen
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "registro_procesado"){
					mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
					$scope.limpiar_cajas_quienes_somos();
				}else if($scope.mensajes.mensaje == "existe"){
					mostrar_notificacion("Mensaje","Ya existe una categoría con esa descripción","warning");
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}
		////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.consultarnosotrosIndividual = function(){
			
			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

			//console.log($scope.id_nosotros);
			nosotrosFactory.asignar_valores($scope.nosotros.id_idioma,$scope.id_nosotros,$scope.base_url)
			nosotrosFactory.cargar_nosotros(function(data){
				$scope.nosotros=data[0];
				$scope.borrar_imagen.push($scope.nosotros.id_imagen);
				//console.log($scope.borrar_imagen)
				$scope.activo_img = "activo"
				ordenFactory.asignar_valores($scope.nosotros.id_idioma,$scope.base_url,"2")
				ordenFactory.cargar_orden_nosotros(function(data){
					$scope.ordenes=data;
					//console.log($scope.ordenes)
					//-Elimino el select
					eliminarOptions("orden")
					$.each( $scope.ordenes, function( indice, elemento ){
					  	agregarOptions("#orden", elemento.orden, elemento.orden)
					});
				});

				//console.log(data[0]);

				$("#div_descripcion").html($scope.nosotros.descripcion)
				setTimeout(function(){
					$('#idioma > option[value="'+$scope.nosotros.id_idioma+'"]').prop('selected', true);
					$('#orden > option[value="'+$scope.nosotros.orden+'"]').prop('selected', true);
					$scope.nosotros.inicial = $scope.nosotros.orden;
				},300);
				$("#idioma").prop('disabled', true);

				$scope.lista_iconos = $scope.nosotros.icono
				
				$scope.titulo_registrar = "Modificar";
				
				$scope.subtitulo_pagina  = "Modificar categorías";
			
			});
			desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.modificar_nosotros = function(){
			//console.log($scope.nosotros.id_idioma);
			$http.post($scope.base_url+"/QuienesSomos/modificarnosotros",
			{
				'id' 	 	 : $scope.nosotros.id,
				'titulo' 	 : $scope.nosotros.titulo,
				'descripcion': $scope.nosotros.descripcion,
				'id_idioma'  : $scope.nosotros.id_idioma,
				'icono': $scope.nosotros.icono,
				'orden': $scope.nosotros.orden,
				'inicial':$scope.nosotros.inicial,
				'id_imagen':$scope.nosotros.id_imagen,
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "modificacion_procesada"){
					mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
					//$scope.limpiar_cajas_quienes_somos();
				}else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}
		$scope.consultar_idioma();
		$scope.consultar_galeria_img();

		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.id_nosotros  = $("#id_nosotros").val();
		//console.log($scope.id_nosotros);
			if($scope.id_nosotros){
				$scope.consultarnosotrosIndividual();
			}
		//------------------------------------------------------------------------------
		$scope.seleccionarIcono = function(event){

			//var id_icono = event.target.id;//Para capturar id

			var text_icono = $("#super_icono"+event).attr("data");

			$scope.lista_iconos= text_icono;

			$scope.nosotros.icono = text_icono;

			$("#iconModal").modal("hide");
			
		}
		//------------------------------------------------------------------------------
		$scope.cargarOrden = function(){
			ordenFactory.asignar_valores($scope.nosotros.id_idioma.id,$scope.base_url,"1")
			ordenFactory.cargar_orden_nosotros(function(data){
				$scope.ordenes=data;
				//console.log($scope.ordenes);
				//-Elimino el select
				eliminarOptions("orden")
				$.each( $scope.ordenes, function( indice, elemento ){
				  	agregarOptions("#orden", elemento.orden, elemento.orden)
				});
				$('#orden > option[value=""]').prop('selected', true);

			});
		}
		//------------------------------------------------------------------------------	
	})
