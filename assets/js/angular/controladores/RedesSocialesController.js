angular.module("ContentManagerApp")
	.controller("RedesSocialesController", function($scope,$http,$location,serverDataMensajes,sesionFactory,idiomaFactory,redesFactory){
		//Cuerpo declaraciones
		$(".li-menu").removeClass("active");
        $("#li_contactos").addClass("active");
        $(".a-menu").removeClass("active");
        $("#redes_sociales").addClass("active");	
		$scope.titulo_pagina = "Redes Sociales";
		$scope.subtitulo_pagina  = "Registrar redes sociales";
		$scope.activo_img = "inactivo";
		$scope.redes_sociales = {
								'id':'',
								'id_tipo_red':'',
								'url':''
		}
		$scope.id_tipo_red = '';
		$scope.titulo_registrar = "Registrar";
		$scope.categorias_menu = "1";
		$scope.base_url = $("#base_url").val();
		//Cuerpo de metodos
		//---------------------------------------------------------------
		$scope.limpiar_cajas_categorias = function(){
			$scope.redes_sociales = {
								'id':'',
								'id_tipo_red':'',
								'url':''
			}
			$("#redes").val("");
		}
		//--
		$scope.validar_form = function(){
			if($scope.id_tipo_red=="0"){
				mostrar_notificacion("Campos no validos","Debe seleccionar una red","warning");
				return false;
			}if($scope.redes_sociales.url_red==""){
				mostrar_notificacion("Campos no validos","Debe ingresar la url","warning");
				return false;
			}
			else{
				return true;
			}
		}
		//--
		/*$scope.consultarCategoriaIndividual = function(){
			redesFactory.asignar_valores("",$scope.id_categoria,$scope.base_url)
			redesFactory.cargar_categorias(function(data){
				$scope.categorias=data[0];
				$scope.titulo_registrar = "Modificar";
				$scope.subtitulo_pagina  = "Modificar categorías";
			});
		}*/
		//--
		$scope.consultarRedes = function(){
			redesFactory.asignar_valores("","",$scope.base_url)
			redesFactory.cargar_redes(function(data){
				$scope.tipo_red=data;
				//console.log($scope.tipo_red);
			});
		}
		//--
		$scope.consultarRedesUrl = function(){
			$scope.titulo_registrar = "Registrar";
			$scope.subtitulo_pagina = "Registrar Redes";
			redesFactory.asignar_valores("",$scope.id_tipo_red.id,$scope.base_url)
			redesFactory.cargar_redes_url(function(data){
				$scope.redes_sociales=data[0]
				//alert($scope.redes_sociales.url_red)
				if($scope.redes_sociales.url_red!=""){
					$scope.titulo_registrar = "Modificar";
					$scope.subtitulo_pagina = "Modificar Redes";
				}
				//console.log($scope.redes_sociales)
			});
		}
		//--
		$scope.registrarRedes = function(){

			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

			if($scope.validar_form()==true){
				//Para guardar
				//alert("id_personas:"+$scope.doctor.id_personas);
				//alert("id_doctor:"+$scope.doctor.id);
				$scope.redes_sociales.id_tipo_red = $scope.id_tipo_red.id

				if(($scope.redes_sociales.id!=undefined)&&($scope.redes_sociales.id!="")){
					$scope.modificar_redes();	
				}else{
					$scope.insertar_redes();
				}		
			}

			//desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

		}
		//---
		$scope.insertar_redes = function(){
			$http.post($scope.base_url+"/RedesSociales/registrarRedes",
			{
				'id_tipo_red':$scope.redes_sociales.id_tipo_red,
				'url_red':$scope.redes_sociales.url_red, 	

			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "registro_procesado"){
					mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
					$scope.limpiar_cajas_categorias();
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
			}).error(function(data,estatus){
				console.log(data);
			});	
		}
		//---

		$scope.modificar_redes = function(){
			$http.post($scope.base_url+"/RedesSociales/modificarRedes",
			{
				'id_red':$scope.redes_sociales.id,	
				'id_tipo_red':$scope.redes_sociales.id_tipo_red,
				'url_red':$scope.redes_sociales.url_red, 	
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "modificacion_procesada"){
					mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
				}else{
					mostrar_notificacion("Mensaje","Ocurrió un error inesperado","warning");
				}
			}).error(function(data,estatus){
				console.log(data);
			});	
		}
		
		$(".tst1").click(function(){
          

     });
		//---------------------------------------------------------------
		//Cuerpo de Llamados a metodos
		//--Si tiene id categoria .... consulto los datos relacionados a ese id, esto es cuando proviene de una consulta
		$scope.id_categoria  = $("#id_categoria").val();
		if($scope.id_categoria){
			$scope.consultarCategoriaIndividual();
		}
		//---------------------------------------------------------
		$scope.consultarRedes()
	});