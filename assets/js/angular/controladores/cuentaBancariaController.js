angular.module("ContentManagerApp")
	.controller("cuentaBancariaController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,sesionFactory,idiomaFactory,bancosFactory,cuentaBancariaFactory){
		//Cuerpo declaraciones
		$(".li-menu").removeClass("active");
        $("#li_contactos").addClass("active");	
        $(".a-menu").removeClass("active");
        $("#cuenta_bancaria").addClass("active");		
        $scope.titulo_pagina = "Cuentas Bancarias";
		$scope.subtitulo_pagina  = "Registrar banco";
		$scope.activo_img = "inactivo";
		$scope.cuenta_bancaria = {
								'id':'',
								'id_banco':'',
								'descripcion':'',
		}
		$scope.id_cuenta = ""
		$scope.searchBanco = []
		$scope.borrar_imagen = []
		$scope.currentTab = 'datos_basicos'
		$scope.titulo_registrar = "Registrar";
		$scope.titulo_text = "Pulse aquí para ingresar el contenido  del banco"
		$scope.base_url = $("#base_url").val();
		//--------------------------------------------------------
		//Cuerpo de metodos
		$scope.consultar_idioma = function(){
			idiomaFactory.asignar_valores("",$scope.base_url)
			idiomaFactory.cargar_idioma(function(data){
				$scope.idioma=data;
				//console.log($scope.idioma);
			});
		}
		//Bancos registrados
		$scope.consultar_bancos = function(){
			bancosFactory.asignar_valores("","",$scope.base_url)
			bancosFactory.cargar_bancos_registrados(function(data){
				$scope.bancos_registrados=data;
				//console.log($scope.idioma);
			});
		}
		$scope.agregarWisi = function(){
			//$('#text_editor').data("wysihtml5").editor.clear();
			$scope.cuenta_bancaria.descripcion = $(".textarea_editor").val()
			$("#div_descripcion").html($scope.cuenta_bancaria.descripcion)
			$("#cerrarModal").click();
		}
		$scope.wisi_modal = function(){
			$("#wisiModal").modal("show")
			$(".textarea_editor").data("wysihtml5").editor.setValue($scope.cuenta_bancaria.descripcion)
		}
		//--
		$scope.registrarBanco = function(){

			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

			if($scope.validar_form()==true){
				//Para guardar
				if(($scope.cuenta_bancaria.id!="")&&($scope.cuenta_bancaria.id!=undefined)){
					$scope.modificar_banco();
				}else{
					$scope.insertar_banco();
				}

			}
			
			//desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
		}

		$scope.insertar_banco = function(){
			$http.post($scope.base_url+"/CuentaBancaria/registrarBanco",
			{
				'descripcion': $scope.cuenta_bancaria.descripcion,
				'id_banco'  : $scope.cuenta_bancaria.id_banco.id,
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "registro_procesado"){
					mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
					$scope.limpiar_cajas_banco();
				}else if($scope.mensajes.mensaje == "existe"){
					mostrar_notificacion("Mensaje","Ya existe la cuenta bancaria","warning");
					$scope.limpiar_cajas_banco();
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}

		$scope.modificar_banco = function(){
			$http.post($scope.base_url+"/CuentaBancaria/modificarBanco",
			{
				'id'	     : $scope.cuenta_bancaria.id,
				'descripcion': $scope.cuenta_bancaria.descripcion,
				//'id_idioma'  : $scope.cuenta_bancaria.id_idioma,
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "registro_procesado"){
					mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
				}else if($scope.mensajes.mensaje == "no_existe"){
					mostrar_notificacion("Mensaje","No existe el banco","warning");
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}

		$scope.validar_form = function(){
			console.log($scope.cuenta_bancaria);

			if(($scope.cuenta_bancaria.id_banco=="NULL")||($scope.cuenta_bancaria.id_banco=="")){
				mostrar_notificacion("Campos no validos","Debe seleccionar el banco","warning");
				return false;
			}else if($scope.cuenta_bancaria.descripcion==""){
				mostrar_notificacion("Campos no validos","Debe ingresar la cuenta bancaria","warning");
				return false;
			}
			else{
				return true;
			}
		}

		$scope.limpiar_cajas_banco = function(){
			$scope.cuenta_bancaria = {
								'id':'',
								'id_banco':'',
								'descripcion':'',
			}

			$('#text_editor').data("wysihtml5").editor.clear();
			$("#div_descripcion").html($scope.titulo_text);
		}
		//--
		$scope.consultarBancoIndividual = function(){

			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

			cuentaBancariaFactory.asignar_valores("",$scope.id_cuenta,$scope.base_url)
			cuentaBancariaFactory.cargar_banco(function(data){
				$scope.cuenta_bancaria=data[0];
				$("#div_descripcion").html($scope.cuenta_bancaria.descripcion)
				$scope.titulo_registrar = "Modificar";
				$scope.subtitulo_pagina  = "Modificar banco";
				setTimeout(function(){
					$('#banco_registrado > option[value="'+$scope.cuenta_bancaria.id_banco+'"]').prop('selected', true);
				},300);
				console.log($scope.cuenta_bancaria);
				$("#banco_registrado").prop('disabled', true);		
				console.log($scope.cuenta_bancaria);

			});

			desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

		}
		//---
		//--------------------------------------------------------
		//Cuerpo de llamados
		$scope.consultar_idioma();
		$scope.consultar_bancos();
		//--Si tiene id categoria .... consulto los datos relacionados a ese id, esto es cuando proviene de una consulta
		$scope.id_cuenta  = $("#id_cuenta").val();
		if($scope.id_cuenta){
			$scope.consultarBancoIndividual();
		}
		//--------------------------------------------------------
	});