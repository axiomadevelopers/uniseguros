angular.module("ContentManagerApp")
    .controller("auditoriaController", function($scope,$http,$location,serverDataMensajes,categoriasFactory,sesionFactory,idiomaFactory,auditoriaFactory){
       
		$(".li-menu").removeClass("active");
        $(".a-menu").removeClass("active");
        $("#auditoria").addClass("active");   
        $scope.titulo_pagina = "Consulta de auditoria";
        $scope.activo_img = "inactivo";
        //---------------------------------------------
        $scope.contactos = {
                                'id':'',
                                'number':'',
                                'nombre_usuario':'',
                                'modulo':'',
                                'accion':'',
                                'ip':'',
                                'fecha_hora':''
        }
      
        $scope.titulo_mensaje = [];
        //-----------------------------------------------
        $scope.base_url = $("#base_url").val();

        $scope.iniciar_datatable = function(){
			//--Iniciar DataTable
			$('#myTable').DataTable({
			    language: {
			        "decimal": "",
			        "emptyTable": "No hay información",
			        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
			        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
			        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
			        "infoPostFix": "",
			        "thousands": ",",
			        "lengthMenu": "Mostrar _MENU_ Entradas",
			        "loadingRecords": "Cargando...",
			        "processing": "Procesando...",
			        "search": "Buscar:",
			        "zeroRecords": "Sin resultados encontrados",
			        "paginate": {
			            "first": "Primero",
			            "last": "Ultimo",
			            "next": "Siguiente",
			            "previous": "Anterior"
			        }
			    },
			});
		}
        $scope.consultar_auditoria = function(){
            auditoriaFactory.asignar_valores($scope.base_url)
            auditoriaFactory.cargar_auditoria(function(data){
                $.when(
                    $scope.auditoria=data
                ).then(function(){
                    $scope.iniciar_datatable();
                })
                //console.log($scope.auditoria)
            });
        }
     
        $scope.consultar_auditoria();

		//----------------------------------------------------------------
        /*setTimeout(function(){
        	$scope.iniciar_datatable();
        },500);*/
        //----------------------------------------------------------------
    });
