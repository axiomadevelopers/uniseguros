angular.module("ContentManagerApp")
	.controller("DirectivaController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,sesionFactory,idiomaFactory,directivaFactory,ordenFactory){
		//---------------------------------------------------------------------
		//Cuerpo declaraciones
		$(".li-menu").removeClass("active");
        $(".a-menu").removeClass("active");
        $("#li_empresa").addClass("active");	
        $("#directiva").addClass("active");	

		$scope.titulo_pagina = "Directiva";
		$scope.subtitulo_pagina  = "Registrar Directiva";
		$scope.activo_img = "inactivo";

		$scope.directiva = {
						'id': '',
						'idioma': '',
						'id_idioma' : '',
						'titulo' : '',
						'descripcion' : '',
						'id_imagen' : '',
						'imagen' : 'assets/images/logo_peque.png',
						'orden':''
		}
		
		
		$scope.titulo_registrar = "Registrar";
		$scope.titulo_cons = "Consultar";
		$scope.borrar_imagen = []
		$scope.currentTab = 'datos_basicos'
		$scope.titulo_text = "Pulse aquí para ingresar la descripción de la directiva"

		$scope.opcion = ''
		$scope.seccion = ''
		$scope.base_url = $("#base_url").val();
		
		//alert($scope.base_url);
		//Cuerpo de metodos
		$scope.consultar_idioma = function(){
			idiomaFactory.asignar_valores("",$scope.base_url)
			idiomaFactory.cargar_idioma(function(data){
				$scope.idioma=data;

				$.each( $scope.idioma, function( indice, elemento ){
				  	agregarOptions("#idioma", elemento.id, elemento.descripcion)
				});
				$('#idioma > option[value="'+$scope.directiva.id_idioma+'"]').prop('selected', true);
			});
		}
		//---------------------------------
		//
		$scope.cargarOrden = function(){
			ordenFactory.asignar_valores($scope.directiva.id_idioma,$scope.base_url,"1")
			ordenFactory.cargar_orden_directiva(function(data){
				$scope.ordenes=data;
				//console.log($scope.ordenes);
				//-Elimino el select
				eliminarOptions("orden")
				$.each( $scope.ordenes, function( indice, elemento ){
				  	agregarOptions("#orden", elemento.orden, elemento.orden)
				});
				$('#orden > option[value=""]').prop('selected', true);

			});
		}
		//WISIMODAL
		$scope.agregarWisi = function(){
			//$('#text_editor').data("wysihtml5").editor.clear();
			$scope.directiva.descripcion = $(".textarea_editor").val()
			$("#div_descripcion").html($scope.directiva.descripcion)
			$("#cerrarModal").click();
		}
		//--
		
		$scope.wisi_modal = function(){
			$("#wisiModal").modal("show")
			$(".textarea_editor").data("wysihtml5").editor.setValue($scope.directiva.descripcion)
		}
		//--
		$scope.agregar_contenido = function(){
			if ($scope.directiva==undefined) {
					$scope.directiva = {
						'id': '',
						'idioma': '',
						'id_idioma' : '',
						'titulo' : '',
						'descripcion' : '',
						'id_imagen' : '',
						'imagen' : 'assets/images/logo_peque.png',
						'orden':''
					}
			}

		}
		//------------------------------------------CONSULTA DE IMAGEN SEGUN ID DE CATEGORIA
		$scope.consultar_galeria_img = function(){
			galeriaFactory.asignar_valores('23','',$scope.base_url);//ya que es galeria de imagenes
			galeriaFactory.cargar_galeria_fa(function(data){
				$scope.galery=data;
				//console.log($scope.galery);
			});
		}
		//MODAL DE IMG
		$scope.seleccione_img_principal = function(){
			$("#modal_img1").modal("show");
		}
		//PARA SELECCIONAR UNA IMAGEN Y MOSTRARLA EN EL CAMPO DE IMG
		$scope.seleccionar_imagen = function(event){
				var imagen = event.target.id;//Para capturar id
				//console.log(imagen);
				var vec = $("#"+imagen).attr("data");
				var vector_data = vec.split("|")
				var id_imagen = vector_data[0];
				var ruta = vector_data[1];

				$scope.borrar_imagen.push(id_imagen);
				$scope.activo_img = "activo"
				$scope.directiva.id_imagen = id_imagen
				$scope.directiva.imagen = ruta
				//alert($scope.directiva.id_imagen);
				//--
				$("#modal_img1").modal("hide");
				//--
		}
		/////////////////////////////////////////////////////////////////////////////////////
		$scope.registrarDirectiva = function(){
			$scope.directiva.orden = $("#orden").val();
			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
			if($scope.validar_form()==true){
				//Para guardar
				if(($scope.directiva.id!="")&&($scope.directiva.id!=undefined)){
					$scope.modificar_directiva();
				}else{
					$scope.insertar_directiva();
				}
			}
			//desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

		}
		///////////////////////////////////////////////////////////////////////////////////
		$scope.insertar_directiva = function(){
			$http.post($scope.base_url+"/Directiva/registrarDirectiva",
			{
				'id' 	     : $scope.directiva.id,
				'id_idioma'  : $scope.directiva.id_idioma,
				'titulo'     : $scope.directiva.titulo,
				'descripcion': $scope.directiva.descripcion,
				'id_imagen'  : $scope.directiva.id_imagen,
				'orden':  $scope.directiva.orden
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "registro_procesado"){
					mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
					$scope.limpiar_cajas_directiva();
				}else if($scope.mensajes.mensaje == "existe"){
					mostrar_notificacion("Mensaje","Ya existe una noticia con ese titulo","warning");
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}

			}).error(function(data,estatus){
				console.log(data);
			});
		}
		//////////////////////////
		$scope.modificar_directiva = function(){
			$scope.directiva.orden = $("#orden").val();
			$http.post($scope.base_url+"/Directiva/modificarDirectiva",
			{
				'id' 	     : $scope.directiva.id,
				'id_idioma'  : $scope.directiva.id_idioma,
				'titulo'     : $scope.directiva.titulo,
				'descripcion': $scope.directiva.descripcion,
				'id_imagen'  : $scope.directiva.id_imagen,
				'boton'      : $scope.directiva.boton,
				'url'        : $scope.directiva.url,
				'direccion' : $scope.directiva.direccion,
				'orden' 	: $scope.directiva.orden,
				'inicial'	: $scope.directiva.inicial
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "modificacion_procesada"){
					mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
					$scope.directiva.inicial = $scope.directiva.orden;
					desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

				}else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
					desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

				}
				//desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

			}).error(function(data,estatus){
				console.log(data);
			});
		}
		//////////////////////////
		$scope.validar_form = function(){
			//console.log($scope.directiva)
			if(($scope.directiva.id_idioma=="")||($scope.directiva.id_idioma=="")){
				mostrar_notificacion("Campos no validos","Debe seleccionar el idioma","warning");
				return false;
			}/*else if(($scope.directiva.descripcion=="")||($scope.directiva.descripcion=="")){
				mostrar_notificacion("Campos no validos","Debe seleccionar la dirección del texto del slide","warning");
				return false;
			}*/ else if($scope.directiva.titulo==""){
				mostrar_notificacion("Campos no validos","Debe ingresar el titulo","warning");
				return false;
			}else if(($scope.directiva.orden=="NULL")||($scope.directiva.orden=="")){
				mostrar_notificacion("Campos no validos","Debe seleccionar el orden","warning");
				return false;
			}
			else if(($scope.directiva.id_imagen=="NULL")||($scope.directiva.id_imagen=="")){
				mostrar_notificacion("Campos no validos","Debe seleccionar la imagen","warning");
				return false;
			}
			else{
				return true;
			}
		}
		/////////////////////////
		$scope.limpiar_cajas_directiva = function(){
			
			$scope.directiva = {
						'id': '',
						'idioma': '',
						'id_idioma' : '',
						'titulo' : '',
						'descripcion' : '',
						'id_imagen' : '',
						'imagen' : 'assets/images/logo_peque.png',
						'orden':''
			}	

			$scope.activo_img = "inactivo";
			$("#div_descripcion").html($scope.titulo_text);
			$('#textarea_editor').data("wysihtml5").editor.clear();
			$("#idioma").removeAttr("disabled");
			$scope.titulo_registrar = "Registrar";
			$scope.subtitulo_pagina  = "Registrar directiva";
			$("#nuevo").css({"display":"none"})
		}
		//--
		$scope.consultarDirectivaIndividual = function(){
			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
			directivaFactory.asignar_valores("",$scope.id_directiva,$scope.base_url)
			directivaFactory.cargar_directiva(function(data){
				
				$scope.directiva=data[0];
								
				
				//alert($scope.directiva.orden)
				$scope.directiva=data[0];
				//--
				ordenFactory.asignar_valores($scope.directiva.id_idioma,$scope.base_url,"2")
				ordenFactory.cargar_orden_directiva(function(data){
					$scope.ordenes=data;
					//console.log($scope.ordenes)
					//-Elimino el select
					eliminarOptions("orden")
					$.each( $scope.ordenes, function( indice, elemento ){
					  	agregarOptions("#orden", elemento.orden, elemento.orden)
					});
				});
				//--
				//console.log(data[0]);
				$("#div_descripcion").html($scope.directiva.descripcion)
				$scope.borrar_imagen.push($scope.directiva.id_imagen);
				$scope.activo_img = "activo"
				$scope.directiva.imagen = $scope.directiva.ruta
				$scope.titulo_registrar = "Modificar";
				$scope.subtitulo_pagina  = "Modificar directiva";
			
				setTimeout(function(){
					$('#idioma > option[value="'+$scope.directiva.id_idioma+'"]').prop('selected', true);
					$('#orden > option[value="'+$scope.directiva.orden+'"]').prop('selected', true);
					$scope.directiva.inicial = $scope.directiva.orden;
				},500);
				
				//$("#idioma").attr("disabled");
				$("#idioma").prop('disabled', true);
			
				/*setTimeout(function(){
					$('#idioma > option[value="'+$scope.directiva.id_idioma+'"]').prop('selected', true);
					$('#direccion_slide > option[value="'+$scope.directiva.direccion+'"]').prop('selected', true);

				},300);*/
				//$("#idioma").attr("disabled");
				//$("#idioma").prop('disabled', true);
				desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
			});
		}
		//---
		///////////////////////////////////////////////////
		$scope.consultar_idioma();
		$scope.consultar_galeria_img();

		$scope.id_directiva = $("#id_directiva").val();
			if($scope.id_directiva){
				$scope.consultarDirectivaIndividual();
			}else{
				$("#idioma").removeAttr("disabled");
			}
		//-----------------------------------------------------------------------
	});	