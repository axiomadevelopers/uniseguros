angular.module("ContentManagerApp")
	.controller("DetalleTserviciosController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,ordenProductosFactory,idiomaFactory,tproductosFactory,buscarserviciosFactory,buscartserviciosFactory,detalleserviciosFactory ){
		//Cuerpo declaraciones
		$(".li-menu").removeClass("active");
        $("#li_servicios1").addClass("active");	
        $(".a-menu").removeClass("active");
        $("#detalle_t_servicio").addClass("active");	
		$scope.titulo_pagina = "Detalles de servicio";
		$scope.subtitulo_pagina  = "Registrar Detalles de servicio";
		$scope.activo_img = "inactivo";
		$scope.servicios = {
								'id':'',
								'id_idioma':'',
								'titulo':'',
								'descripcion':'',
								'estatus':'',
								'id_servicios':'',
								'id_tipo_servicio':''
		}
		$scope.searchProductos = []
		$scope.borrar_imagen = []
		$scope.currentTab = 'datos_basicos'
		$scope.titulo_registrar = "Registrar";
		$scope.base_url = $("#base_url").val();
		$scope.titulo_text = "Pulse aquí para ingresar el contenido del producto"


		//--------------------------------------------------------
		//Cuerpo de metodos
		$scope.consultar_idioma = function(){
			idiomaFactory.asignar_valores("",$scope.base_url)
			idiomaFactory.cargar_idioma(function(data){
				$scope.idioma=data;
				$.each( $scope.idioma, function( indice, elemento ){
				  	agregarOptions("#idioma", elemento.id, elemento.descripcion)
				});
				$('#idioma > option[value="0"]').prop('selected', true);

				//console.log($scope.idioma);
			});
		}
		$scope.buscarProducto = function(){
			//console.log($scope.servicios.id_idioma);			

			if(!$scope.servicios.id_idioma){
				$("#serviciosTodos option").remove();
				agregarOptions("#serviciosTodos", "", "--Seleccione un servicio--")
				$("#tipos_servicios option").remove();
				agregarOptions("#tipos_servicios", "", "--Seleccione tipo de servicio--")

			}
			buscarserviciosFactory.asignar_valores($scope.servicios.id_idioma.id,$scope.base_url)
			buscarserviciosFactory.cargar_servicio(function(data){
				//console.log(data);			
				$scope.mensajes = data;
				if($scope.mensajes.mensaje == "error"){
					$("#serviciosTodos option").remove();
					agregarOptions("#serviciosTodos", "", "--Seleccione un servicio--")
					$("#tipos_servicios option").remove();
					agregarOptions("#tipos_servicios", "", "--Seleccione tipo de servicio--")
				}else{
					if($scope.condicional=$scope.servicios.id_idioma){}
					$("#serviciosTodos option").remove();
					$("#tipos_servicios option").remove();
					$scope.serviciosTodos=data;
					agregarOptions("#serviciosTodos", "", "--Seleccione un servicio--")
					agregarOptions("#tipos_servicios", "", "--Seleccione tipo de servicio--")
					$.each( $scope.serviciosTodos, function( indice, elemento ){
						agregarOptions("#serviciosTodos", elemento.id, elemento.titulo)
					});
				$scope.condicional = $scope.servicios.id_idioma;
			}});
			
		}
		$scope.buscarTservicios = function(){
			
			if(!$scope.servicios.id_servicios){
				$("#tipos_servicios option").remove();
				agregarOptions("#tipos_servicios", "", "--Seleccione tipo de servicio--")			
			}
			buscartserviciosFactory.asignar_valores($scope.servicios.id_servicios.id,$scope.base_url)
			buscartserviciosFactory.cargar_servicios(function(data){
				$scope.mensajes = data;
				if($scope.mensajes.mensaje == "error"){
					$("#tipos_servicios option").remove();
					agregarOptions("#tipos_servicios", "", "--Seleccione tipo de servicio--")
				}else{
					$("#tipos_servicios option").remove();
					agregarOptions("#tipos_servicios", "", "--Seleccione tipo de servicio--")
					$scope.tipos_servicios=data;

				$.each( $scope.tipos_servicios, function( indice, elemento ){
					agregarOptions("#tipos_servicios", elemento.id, elemento.titulo)
				});
				$('#tipos_servicios > option[value="0"]').prop('selected', true);
			}});
			
		}
		$scope.agregarWisi = function(){
			//$('#text_editor').data("wysihtml5").editor.clear();
			$scope.servicios.descripcion = $(".textarea_editor").val()
			$("#div_descripcion").html($scope.servicios.descripcion)
			$("#cerrarModal").click();
		}
		$scope.wisi_modal = function(){
			$("#wisiModal").modal("show")
			$(".textarea_editor").data("wysihtml5").editor.setValue($scope.servicios.descripcion)
		}
		//--
		$scope.consultar_galeria_img = function(){
			galeriaFactory.asignar_valores('22','',$scope.base_url);//ya que es galeria de imagenes
			galeriaFactory.cargar_galeria_fa(function(data){
				$scope.galery=data;
				//console.log($scope.galery);
			});
		}

		$scope.seleccione_img_principal = function(){
			$("#modal_img1").modal("show");
		}

		$scope.seleccionar_imagen = function(event){
			var imagen = event.target.id;//Para capturar id
			var vec = $("#"+imagen).attr("data");
			var vector_data = vec.split("|")
			var id_imagen = vector_data[0];
			var ruta = vector_data[1];
			//console.log(vector_data[1]);

			$scope.borrar_imagen.push(id_imagen);
			$scope.activo_img = "activo"
			$scope.servicios.id_imagen = id_imagen
			$scope.servicios.imagen = ruta
			//--
			$("#modal_img1").modal("hide");
			//--
		}

		$scope.registrarServicios = function(){
			
			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
			$scope.servicios.orden = $("#orden").val();

			if($scope.validar_form()==true){
				//Para guardar
				if(($scope.servicios.id!="")&&($scope.servicios.id!=undefined)){
					$scope.modificar_productos();
				}else{
					$scope.insertar_productos();
				}

			}

			//desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

		}

		$scope.insertar_productos = function(){

			$http.post($scope.base_url+"/DetalleTservicios/registrarServicios",
			{
				'id'	     : $scope.servicios.id,
				'titulo'     : $scope.servicios.titulo,
				'descripcion': $scope.servicios.descripcion,
				'id_idioma'  : $scope.servicios.id_idioma.id,
				'id_servicios'  : $scope.servicios.id_servicios.id,
				'id_tipo_servicio'  : $scope.servicios.id_tipo_servicio.id,
				'orden'  : $scope.servicios.orden,

			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "registro_procesado"){
					mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
					$scope.limpiar_cajas_servicios();
				}else if($scope.mensajes.mensaje == "existe"){
					mostrar_notificacion("Mensaje","Ya existe un detalle con ese titulo","warning");
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}

		$scope.modificar_productos = function(){
			//console.log($scope.servicios);

			$http.post($scope.base_url+"/DetalleTservicios/modificarServicios",
			{
				'id'	     : $scope.servicios.id,
				'titulo'     : $scope.servicios.titulo,
				'descripcion': $scope.servicios.descripcion,
				'id_idioma'  : $scope.servicios.id_idioma,
				'id_servicios'  : $scope.servicios.id_servicios,
				'id_tipo_servicio'  : $scope.servicios.id_tipo_servicio,
				'orden'  : $scope.servicios.orden,
				'inicial'  : $scope.servicios.inicial,
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "registro_procesado"){
					mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
					$scope.servicios.inicial = $scope.servicios.orden
				}else if($scope.mensajes.mensaje == "existe"){
					mostrar_notificacion("Mensaje","Ya existe una servicio con ese titulo","warning");
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}

		$scope.validar_form = function(){
			//console.log($scope.servicios)
			if(($scope.servicios.id_idioma=="NULL")||($scope.servicios.id_idioma=="")||(!$scope.servicios.id_idioma)){
				mostrar_notificacion("Campos no validos","Debe seleccionar el idioma","warning");
				return false;
			}else if(($scope.servicios.id_servicios=="NULL")||($scope.servicios.id_servicios=="")||(!$scope.servicios.id_servicios)){
				mostrar_notificacion("Campos no validos","Debe seleccionar un servicio","warning");
				return false;
			}else if(($scope.servicios.id_tipo_servicio=="NULL")||($scope.servicios.id_tipo_servicio=="")||(!$scope.servicios.id_tipo_servicio)){
				mostrar_notificacion("Campos no validos","Debe seleccionar tipo de servicio","warning");
				return false;
			}else if($scope.servicios.titulo==""){
				mostrar_notificacion("Campos no validos","Debe ingresar el titulo","warning");
				return false;
			}else if(($scope.servicios.orden=="NULL")||($scope.servicios.orden=="")){
				mostrar_notificacion("Campos no validos","Debe seleccionar el orden del servicio","warning");
				return false;
			}else if($scope.servicios.descripcion==""){
				mostrar_notificacion("Campos no validos","Debe ingresar la descripción","warning");
				return false;
			}
			else{
				return true;
			}
		}

		$scope.limpiar_cajas_servicios = function(){
			$scope.servicios = {
				'id':'',
				'id_idioma':'',
				'titulo':'',
				'descripcion':'',
				'estatus':'',
				'id_producto':'',
				'id_tipo_servicio':''
			}
			$scope.activo_img = "inactivo";

			$('#text_editor').data("wysihtml5").editor.clear();
			$("#div_descripcion").html($scope.titulo_text);
			$('#orden > option[value=""]').prop('selected', true);
			eliminarOptions("serviciosTodos")
			$('#serviciosTodos > option[value=""]').prop('selected', true);
			eliminarOptions("tipos_servicios")
			$('#tipos_servicios > option[value=""]').prop('selected', true);
		}
		//--
		$scope.consultarProductoIndividual = function(){

			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

			detalleserviciosFactory.asignar_valores("",$scope.id_detalle_servicios,$scope.base_url)
			detalleserviciosFactory.cargar_servicios(function(data){
				$scope.servicios=data[0];
				//console.log(data[0]);
				$("#div_descripcion").html($scope.servicios.descripcion)
				$scope.borrar_imagen.push($scope.servicios.id_imagen);
				$scope.activo_img = "activo"
				//-Cambio 10022020
				buscarserviciosFactory.asignar_valores($scope.servicios.id_idioma,$scope.base_url)
				buscarserviciosFactory.cargar_servicio(function(data){
					$scope.serviciosTodos=data;
					$.when(
						$.each($scope.serviciosTodos, function( indice, elemento ){
							agregarOptions("#serviciosTodos", elemento.id, elemento.titulo)
						})
					).then(function(){
						$('#serviciosTodos > option[value="'+$scope.servicios.id_servicios+'"]').prop('selected', true);
					})
					
					//$('#serviciosTodos > option[value="0"]').prop('selected', true);
				});
				buscartserviciosFactory.asignar_valores($scope.servicios.id_servicios,$scope.base_url)
				buscartserviciosFactory.cargar_servicios(function(data){
					$scope.tipos_servicios=data;
					$.when(
						$.each($scope.tipos_servicios, function( indice, elemento ){
							agregarOptions("#tipos_servicios", elemento.id, elemento.titulo)
						})
					).then(function(){
						$('#tipos_servicios > option[value="'+$scope.servicios.id_tservicio+'"]').prop('selected', true);
					})
					//$('#tipos_servicios > option[value="0"]').prop('selected', true);
					//console.log($scope.tipos_servicios);
				});
				ordenProductosFactory.asignar_valores($scope.servicios.id_idioma,$scope.base_url,"2",$scope.servicios.id_servicios,$scope.servicios.id_tipo_servicio)
				ordenProductosFactory.cargar_orden_detalle_servicios(function(data){
					$scope.ordenes=data;
					//console.log($scope.ordenes)
					//-Elimino el select
					eliminarOptions("orden")
					$.when(
						$.each( $scope.ordenes, function( indice, elemento ){
						  	agregarOptions("#orden", elemento.orden, elemento.orden)
						})
					).then(function(){
						$('#orden > option[value="'+$scope.servicios.orden+'"]').prop('selected', true)
					})
					
				});
				$scope.servicios.imagen = $scope.servicios.ruta
				$scope.titulo_registrar = "Modificar";
				$scope.subtitulo_pagina  = "Modificar detalle de tipo de servicio";
				setTimeout(function(){
					$('#idioma > option[value="'+$scope.servicios.id_idioma+'"]').prop('selected', true);
					//$('#orden > option[value="'+$scope.servicios.orden+'"]').prop('selected', true);
					$scope.servicios.inicial = $scope.servicios.orden;
				},300);
				$("#idioma").prop('disabled', true);
				/*
				setTimeout(function(){
					$('#serviciosTodos > option[value="'+$scope.servicios.id_servicios+'"]').prop('selected', true);
				},300);
				*/
				$("#serviciosTodos").prop('disabled', true);
				/*
				setTimeout(function(){
					$('#tipos_servicios > option[value="'+$scope.servicios.id_tservicio+'"]').prop('selected', true);
				},300);
				*/
				$scope.servicios.id_tservicio.id = $scope.servicios.id_tservicio;
				$("#tipos_servicios").prop('disabled', true);
				//console.log($scope.tipos_servicios);

			});
			
			desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

		}
		//---
		$scope.cargarOrden = function(){
			//console.log($scope.servicios);

			if($scope.servicios.id_servicios.id){
				$scope.id_servicios=$scope.servicios.id_servicios.id
				$scope.id_tipo_servicio=$scope.servicios.id_tipo_servicio.id
			}else{
				$scope.id_tipo_servicio=$scope.servicios.id_tipo_servicio
				$scope.id_servicios=$scope.servicios.id_servicios

			}
			ordenProductosFactory.asignar_valores($scope.servicios.id_idioma.id,$scope.base_url,"1",$scope.id_servicios,$scope.id_tipo_servicio)
			ordenProductosFactory.cargar_orden_detalle_servicios(function(data){
				$scope.ordenes=data;
				//-Elimino el select
				eliminarOptions("orden")
				$.each( $scope.ordenes, function( indice, elemento ){
				  	agregarOptions("#orden", elemento.orden, elemento.orden)
				});
				$('#orden > option[value=""]').prop('selected', true);

			});
		}
		//--------------------------------------------------------
		//Cuerpo de llamados
		$scope.consultar_idioma();
		$scope.consultar_galeria_img();
		//--Si tiene id categoria .... consulto los datos relacionados a ese id, esto es cuando proviene de una consulta
		$scope.id_detalle_servicios  = $("#id_detalle_servicios").val();
		//.id_detalle_servicios);

		if($scope.id_detalle_servicios){
			$scope.consultarProductoIndividual();
		}
		//--------------------------------------------------------

	});
