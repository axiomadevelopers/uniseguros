angular.module("ContentManagerApp")
	.controller("PdfMultimediaController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,sesionFactory,idiomaFactory,categoriasFactory,upload,pdfsFactory){
		//Cuerpo declaraciones
		$(".li-menu").removeClass("active");
        $("#li_configuracion").addClass("active");	
        $(".a-menu").removeClass("active");
        $("#carga_pdf").addClass("active");	
		$scope.titulo_pagina = "Pdf Multimedia";
		$scope.subtitulo_pagina  = "Administrar Pdf";
		$scope.activo_img = "inactivo";
		$scope.pdf = {
							"id":"",
							"descripcion":"",
							"categoria":"",
							"titulo":"",
							"url":""
		}
		$scope.id_pdf = ""

		$scope.titulo_registrar = "Registrar";
		$scope.categorias_menu = "1";
		$scope.base_url = $("#base_url").val();
		$scope.currentTab = 'datos_basicos'
		$scope.titulo_text = "Pulse aquí para ingresar la descripción del pdf"


		$scope.wisi_modal = function(){
			$("#wisiModal").modal("show")
			$(".textarea_editor").data("wysihtml5").editor.setValue($scope.pdf.descripcion)
		}
		$scope.agregarWisi = function(){
			//$('#text_editor').data("wysihtml5").editor.clear();
			$scope.pdf.descripcion = $(".textarea_editor").val()
			$("#div_descripcion").html($scope.pdf.descripcion)
			$("#cerrarModal").click();
		}
		//Cuerpo de metodos
		//--
		$scope.consultarCategoriaIndividual = function(){
			categoriasFactory.asignar_valores("","",$scope.base_url)
			categoriasFactory.cargar_categorias(function(data){
				$scope.categorias=data;
				//console.log($scope.file);
			});
		}
		$("#formDropZone").dropzone({ url: $scope.base_url+"/GaleriaMultimedia/Upload" });
		//--
		$scope.validar_form = function(){
			if(($scope.pdf.titulo=="NULL")||($scope.pdf.titulo=="")||(!$scope.pdf.titulo)){
				mostrar_notificacion("Campos no validos","Debe ingresar el titulo","warning");
				return false;
			}else if(($scope.pdf.categoria=="")||($scope.pdf.categoria==undefined)||($scope.pdf.categoria==0)){
				mostrar_notificacion("Campos no validos","Debe seleccionar la categoría","warning");
				return false;
			}else if($scope.pdf.descripcion==""){
				mostrar_notificacion("Campos no validos","Debe ingresar la descripción","warning");
				return false;
			}else if(($scope.file==undefined)||($scope.file=="")){
				mostrar_notificacion("Campos no validos","Debe Seleccionar un archivo pdf","warning");
				return false;
			}
			else{
				return true;
			}
		}
		//--
		$scope.registrarPdf = function(){

			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
			console.log($scope.pdf);
			console.log($scope.file);

			if($scope.validar_form()==true){
				//Para guardar
				//alert("id_personas:"+$scope.doctor.id_personas);
				//alert("id_doctor:"+$scope.doctor.id);
				if(($scope.pdf.id!=undefined)&&($scope.pdf.id!="")){
					$scope.modificar_pdf();	
				}else{
					$scope.insertar_pdf();
				}		
			}

			//desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

		}
		//--

		$scope.modificar_pdf = function(){
			
			$http.post($scope.base_url+"/CargarPdf/modificarPdf",
			{
				'id':$scope.pdf.id,
				'id_categoria': $scope.pdf.categoria.id,
				'descripcion':$scope.pdf.descripcion,
				'titulo':$scope.pdf.titulo,

			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "registro_procesado"){
					mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
				}else if($scope.mensajes.mensaje == "existe"){
					mostrar_notificacion("Mensaje","Ya existe un PDF con ese titulo","warning");
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}


		$scope.insertar_pdf = function (){
			$scope.uploadFile();
			
			console.log($scope.pdf);

		}
		//---
		//---------------------------------------
		//--Metodo para subir acrhivos
		$scope.uploadFile = function(){
			var file = $scope.file;
			var categoria = $scope.pdf.categoria.id;
			var descripcion = $scope.pdf.descripcion;
			var titulo = $scope.pdf.titulo;
			var base_url = $scope.base_url;
			//-
			//console.log(res.data.mensaje);

			upload.uploadFilePDF(file,categoria,descripcion,titulo,base_url).then(function(res){
				//console.log(res.data.mensaje);
				if(res.data.mensaje=="registro_procesado"){
					mostrar_notificacion("Mensaje","El PDF fue cargada de manera exitosa!","info");
					//$scope.consultar_galeria();
					$scope.limpiar_imagen();
				}else if(res.data.mensaje=="existe"){
					mostrar_notificacion("Mensaje","Existe un pdf con ese titulo!","warning");
					//$scope.consultar_galeria();
				}else{
					mostrar_notificacion("Mensaje"," Error al subir tipo de archivo, solo puede subir pdf con un peso menor a 1 mb","warning");
				}
				//--------------------------------
			});
			//-
		}
		//--
		$scope.consultarPdfIndividual = function(){

			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

			pdfsFactory.asignar_valores("",$scope.id_pdf,$scope.base_url)
			pdfsFactory.cargar_pdf(function(data){
				$scope.pdf=data[0];
				console.log($scope.pdf);
				$("#div_descripcion").html($scope.pdf.descripcion)
				$scope.titulo_registrar = "Modificar";
				$scope.subtitulo_pagina  = "Modificar pdf";
				setTimeout(function(){
					$('#categorias > option[value="'+$scope.pdf.id_categoria+'"]').prop('selected', true);
				},300);
				$("#titulo").prop('disabled', true);
				$("#file").prop('disabled', true);
				$("#list").css("display", "block");
                    $("#list").html([
                        '<input name="titulo_pdf" id="titulo_pdf" type="text" class="center-text text-center form-control form-control-line"placeholder="Ingrese la descripción de la categoría" ng-model="pdf.descripcion" required value="Archivo cargado" readonly>'
					].join('')).fadeIn("slow");
					$scope.pdf.categoria = {id:$scope.pdf.id_categoria};
					console.log($scope.pdf.categoria.id);
					$scope.file ="1";
			}); 
			

			desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

		}//------------------------------------------------------------------------------

		$scope.limpiar_imagen = function(){
			$scope.pdf = {
							"id":"",
							"descripcion":"",
							"titulo":"",
							"categoria":"",
							"url":""
			}
			$scope.file = "";
			$('#text_editor').data("wysihtml5").editor.clear();
			$("#div_descripcion").html($scope.titulo_text);
			document.getElementById('list').innerHTML='';
			$("#titulo_pdf").css("display","block");
			$(".imgbiblioteca_principal").css("display","none");
			$("#file").val("");
			$("#categorias").val("");
		}
		//---------------------------------------
		//---
		//Cuerpo de llamados
		$scope.consultarCategoriaIndividual();
		$scope.id_pdf  = $("#id_pdf").val();
		//console.log($scope.id_pdf);
		if($scope.id_pdf){
			$scope.consultarPdfIndividual();
		}
	});	