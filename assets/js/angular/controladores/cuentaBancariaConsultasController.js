angular.module("ContentManagerApp")
	.controller("cuentaBancariaConsultasController", function($scope,$http,$location,serverDataMensajes,sesionFactory,idiomaFactory,bancosFactory,cuentaBancariaFactory){
		//Cuerpo declaraciones
		$(".li-menu").removeClass("active");
        $("#li_contactos").addClass("active");	
        $(".a-menu").removeClass("active");
        $("#cuenta_bancaria").addClass("active");	
		$scope.titulo_pagina = "Consulta Cuentas Bancarias";
		$scope.banco = {
								'id':'',
								'id_idioma':'',
								'correo':'',
								'descripcion':'',
								'estatus':'',
		}
		$scope.categorias_menu = "1";
		$scope.id_cuenta = "";
		$scope.base_url = $("#base_url").val();
		//-----------------------------------------------------
		//--Cuerpo de metodos  --/
		$scope.iniciar_datatable = function(){
			//--Iniciar DataTable
			$('#myTable').DataTable({
			    language: {
			        "decimal": "",
			        "emptyTable": "No hay información",
			        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
			        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
			        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
			        "infoPostFix": "",
			        "thousands": ",",
			        "lengthMenu": "Mostrar _MENU_ Entradas",
			        "loadingRecords": "Cargando...",
			        "processing": "Procesando...",
			        "search": "Buscar:",
			        "zeroRecords": "Sin resultados encontrados",
			        "paginate": {
			            "first": "Primero",
			            "last": "Ultimo",
			            "next": "Siguiente",
			            "previous": "Anterior"
			        }
			    },
			    "order": [[ 0, "asc" ]]
			});
			//console.log($scope.categorias);
		}
		$scope.consultar_banco = function(){
			cuentaBancariaFactory.asignar_valores("","",$scope.base_url)
			cuentaBancariaFactory.cargar_banco(function(data){
				$scope.banco=data;
				//console.log($scope.banco);				
			});
		}
		//---------------------------------------------------------------
		$scope.ver_banco = function(valor){
			var id = $("#ver"+valor).attr("data");
			$scope.id_cuenta = id;	
			$("#id_cuenta").val($scope.id_cuenta)
			let form = document.getElementById('formConsultaBanco');
			form.action = "./cuentasVer";
			form.submit();
		}
		//----------------------------------------------------------------
		$scope.activar_registro = function(event){
			$scope.id_seleccionado_banco = []
			$scope.estatus_seleccionado_banco = []
			var caja = event.currentTarget.id;
			var atributos = $("#"+caja).attr("data");
			var arreglo_atributos = atributos.split("|");
			$scope.id_seleccionado_banco = arreglo_atributos[0];
			$scope.estatus_seleccionado_banco = arreglo_atributos[1];
			$("#cabecera_mensaje").text("Información:");			
			//--
			if ($scope.estatus_seleccionado_banco==0){
				mensaje = "Desea modificar el estatus de este registro a publicado? ";
				$scope.estatus_seleccionado_banco=1;
			}else{
				mensaje = "Desea modificar el estatus de este registro a inactivo? ";
				$scope.estatus_seleccionado_banco=0
			}
			//($scope.estatus_seleccionado_categorias==1) ? $scope.estatus_seleccionado_categorias=0:$scope.estatus_seleccionado_categorias=1;

			//--
			$scope.modificar_estatus("activar_inactivar",mensaje)
		}
		//----------------------------------------------------------------
		$scope.modificar_estatus = function(opcion,mensaje){
			 swal({
				  title: 'Esta seguro?',
				  text: mensaje,
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Si!',
				  cancelButtonText: 'No',
				}).then((result) => {
					  if (result.value) {
					  	$scope.accion_estatus()
					  }
				})
		}
		//----------------------------------------------------------------
		$scope.accion_estatus = function(){

			$http.post($scope.base_url+"/CuentaBancaria/modificarBancoEstatus",
			{
				 'id':$scope.id_seleccionado_banco,	
				 'estatus':$scope.estatus_seleccionado_banco, 	
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "modificacion_procesada"){
					Swal(
					      'Realizado!',
					      'El proceso fue ejecutado.',
					      'success'
				    ).then((result) => {
						  if (result.value) {
							    let form = document.getElementById('formConsultaBanco');
								form.action = "./consultarCuentas";
								form.submit();
						  }
				    
					});
				}else{
					Swal(
					      'No realizado!',
					      'El proceso no pudo ser ejecutado.',
					      'warning'
				    )
				}
			}).error(function(data,estatus){
				console.log(data);
			});	
		}
		//----------------------------------------------------------------
		$scope.eliminar_registro = function(event){
			$scope.id_seleccionado_banco = []
			$scope.estatus_seleccionado_banco = []
			var caja = event.currentTarget.id;
			//alert(caja);
			var atributos = $("#"+caja).attr("data");
			var arreglo_atributos = atributos.split("|");
			$scope.id_seleccionado_banco = arreglo_atributos[0];
			$scope.estatus_seleccionado_banco = arreglo_atributos[1];
			//console.log($scope.estatus_seleccionado_banco);
			$("#cabecera_mensaje").text("Información:");
			if ($scope.estatus_seleccionado_banco==2){
				mensaje = "Desea eliminar este registro? ";
			}
			$scope.modificar_estatus("eliminar",mensaje)
		}
		//----------------------------------------------------------------
		//-- Cuerpo de funciones--/
		setTimeout(function(){
				$scope.iniciar_datatable();
		},500);
		$scope.consultar_banco();
		
		//-----------------------------------------------------
	});	