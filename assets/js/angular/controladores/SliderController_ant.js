angular.module("ContentManagerApp")
	.controller("SliderController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,sesionFactory,idiomaFactory,sliderFactory,ordenFactory){
		//Cuerpo declaraciones
		$(".li-menu").removeClass("active");
        $("#li_slider").addClass("active");
        $(".a-menu").removeClass("active");
        $("#slider").addClass("active");
		$scope.titulo_pagina = "Slider";
		$scope.subtitulo_pagina  = "Registrar Slider";
		$scope.activo_img = "inactivo";

		$scope.slider = {
						'id': '',
						'idioma': '',
						'id_idioma' : '',
						'titulo' : '',
						'descripcion' : '',
						'boton' : '',
						'url' : '',
						'estatus' : '',
						'id_imagen' : '',
						'imagen' : '',
						'direccion':'',
						'orden':''
		}

		$scope.titulo_registrar = "Registrar";
		$scope.titulo_cons = "Consultar";
		$scope.searchSlider = []
		$scope.borrar_imagen = []
		$scope.currentTab = 'datos_basicos'
		$scope.titulo_text = "Pulse aquí para ingresar la descripción del slider"
		$scope.base_url = $("#base_url").val();
		$scope.opcion = ''
		$scope.seccion = ''
		//Cuerpo de metodos
		$scope.consultar_idioma = function(){
			idiomaFactory.asignar_valores("",$scope.base_url)
			idiomaFactory.cargar_idioma(function(data){
				$scope.idioma=data;
				$.each( $scope.idioma, function( indice, elemento ){
				  	agregarOptions("#idioma", elemento.id, elemento.descripcion)
				});
				$('#idioma > option[value="'+$scope.slider.id_idioma+'"]').prop('selected', true);
				//console.log($scope.idioma);
			});
		}
		//
		$scope.cargarOrden = function(){
			ordenFactory.asignar_valores($scope.slider.id_idioma,$scope.base_url)
			ordenFactory.cargar_orden(function(data){
				$scope.ordenes=data;
				console.log($scope.ordenes);
				$.each( $scope.ordenes, function( indice, elemento ){
				  	agregarOptions("#ordenes", elemento.id, elemento.descripcion)
				});
				$('#ordenes > option[value=""]').prop('selected', true);

			});
		}
		//WISIMODAL
		$scope.agregarWisi = function(){
			//$('#text_editor').data("wysihtml5").editor.clear();
			$scope.slider.descripcion = $(".textarea_editor").val()
			$("#div_descripcion").html($scope.slider.descripcion)
			$("#cerrarModal").click();
		}
		$scope.wisi_modal = function(){
			$("#wisiModal").modal("show")
			$(".textarea_editor").data("wysihtml5").editor.setValue($scope.slider.descripcion)
		}
		//--
		$scope.agregar_contenido = function(){
			if ($scope.slider==undefined) {
					$scope.slider = {
									'id': '',
									'idioma': '',
									'id_idioma' : '',
									'titulo' : '',
									'descripcion' : '',
									'boton' : '',
									'url' : '',
									'estatus' : '',
									'id_imagen' : '',
									'imagen' : '',
									'orden':'',
					}
			}

		}
		//------------------------------------------CONSULTA DE IMAGEN SEGUN ID DE CATEGORIA
		$scope.consultar_galeria_img = function(){
			galeriaFactory.asignar_valores('1','',$scope.base_url);//ya que es galeria de imagenes
			galeriaFactory.cargar_galeria_fa(function(data){
				$scope.galery=data;
				console.log($scope.galery);
			});
		}
		//MODAL DE IMG
		$scope.seleccione_img_principal = function(){
			$("#modal_img1").modal("show");
		}
		//PARA SELECCIONAR UNA IMAGEN Y MOSTRARLA EN EL CAMPO DE IMG
		$scope.seleccionar_imagen = function(event){
				var imagen = event.target.id;//Para capturar id
				console.log(imagen);
				var vec = $("#"+imagen).attr("data");
				var vector_data = vec.split("|")
				var id_imagen = vector_data[0];
				var ruta = vector_data[1];

				$scope.borrar_imagen.push(id_imagen);
				$scope.activo_img = "activo"
				$scope.slider.id_imagen = id_imagen
				$scope.slider.imagen = ruta
				//alert($scope.slider.id_imagen);
				//--
				$("#modal_img1").modal("hide");
				//--
		}
		/////////////////////////////////////////////////////////////////////////////////////
		$scope.registrarSlider = function(){
			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
			if($scope.validar_form()==true){
				//Para guardar
				if(($scope.slider.id!="")&&($scope.slider.id!=undefined)){
					$scope.modificar_slider();
				}else{
					$scope.insertar_slider();
				}
			}
			desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
			
		}
		///////////////////////////////////////////////////////////////////////////////////
		$scope.insertar_slider = function(){
			$http.post($scope.base_url+"/Slider/registrarSlider",
			{
				'id' 	     : $scope.slider.id,
				'id_idioma'  : $scope.slider.id_idioma.id,
				'titulo'     : $scope.slider.titulo,
				'descripcion': $scope.slider.descripcion,
				'id_imagen'  : $scope.slider.id_imagen,
				'boton'      : $scope.slider.boton,
				'url'        : $scope.slider.url,
				'id_imagen'  :$scope.slider.id_imagen,
				'direccion' : $scope.slider.direccion,
				'orden':  $scope.slider.orden.orden
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "registro_procesado"){
					mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
					$scope.limpiar_cajas_slider();
				}else if($scope.mensajes.mensaje == "existe"){
					mostrar_notificacion("Mensaje","Ya existe una noticia con ese titulo","warning");
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}

			}).error(function(data,estatus){
				console.log(data);
			});
		}
		//////////////////////////
		$scope.modificar_slider = function(){
			$scope.slider.orden = $("#orden").val();
			$http.post($scope.base_url+"/Slider/modificarSlider",
			{
				'id' 	     : $scope.slider.id,
				'id_idioma'  : $scope.slider.id_idioma,
				'titulo'     : $scope.slider.titulo,
				'descripcion': $scope.slider.descripcion,
				'id_imagen'  : $scope.slider.id_imagen,
				'boton'      : $scope.slider.boton,
				'url'        : $scope.slider.url,
				'direccion' : $scope.slider.direccion,
				'orden' 	: $scope.slider.orden,
				'inicial'	: $scope.slider.inicial
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "modificacion_procesada"){
					mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
					//$scope.limpiar_cajas_slider();
				}else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
				//desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

			}).error(function(data,estatus){
				//console.log(data);
			});
		}
		//////////////////////////
		$scope.validar_form = function(){
			//console.log($scope.slider)
			if(($scope.slider.id_idioma=="")||($scope.slider.id_idioma=="")){
				mostrar_notificacion("Campos no validos","Debe seleccionar el idioma","warning");
				return false;
			}/*else if(($scope.slider.direccion=="")||($scope.slider.direccion=="")){
				mostrar_notificacion("Campos no validos","Debe seleccionar la dirección del slide","warning");
				return false;
			}*/ else if($scope.slider.titulo==""){
				mostrar_notificacion("Campos no validos","Debe ingresar el titulo","warning");
				return false;
			}/*else if($scope.slider.descripcion==""){
				mostrar_notificacion("Campos no validos","Debe ingresar la descripción","warning");
				return false;
			}else if($scope.slider.boton==""){
				mostrar_notificacion("Campos no validos","Debe ingresar la descripción para el Botón","warning");
				return false;
			}else if($scope.slider.url==""){
				mostrar_notificacion("Campos no validos","Debe ingresar la descripción para la URL","warning");
				return false;
			}*/else if(($scope.slider.id_imagen=="NULL")||($scope.slider.id_imagen=="")){
				mostrar_notificacion("Campos no validos","Debe seleccionar la imagen","warning");
				return false;
			}
			else{
				return true;
			}
		}
		/////////////////////////
		$scope.limpiar_cajas_slider = function(){
			$scope.slider = {
							'id': '',
							'idioma': '',
							'id_idioma' : '',
							'titulo' : '',
							'descripcion' : '',
							'boton' : '',
							'url' : '',
							'estatus' : '',
							'id_imagen' : '',
							'imagen' : '',
							'direccion':''
			}

			$scope.activo_img = "inactivo";
			$("#div_descripcion").html($scope.titulo_text);
			$('#textarea_editor').data("wysihtml5").editor.clear();
			$("#idioma").removeAttr("disabled");
			$scope.titulo_registrar = "Registrar";
			$scope.subtitulo_pagina  = "Registrar slider";
			$("#nuevo").css({"display":"none"})
		}
		//--
		$scope.consultarSliderIndividual = function(){
			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
			sliderFactory.asignar_valores("",$scope.id_slider,$scope.base_url)
			sliderFactory.cargar_slider(function(data){
				
				$scope.slider=data[0];
				
				console.log(data[0]);
				
				ordenFactory.asignar_valores($scope.slider.id_idioma,$scope.base_url)
				ordenFactory.cargar_orden(function(data){
					$scope.ordenes=data;
				});
				//alert($scope.slider.orden)
				$scope.slider=data[0];
				//console.log(data[0]);
				$("#div_descripcion").html($scope.slider.descripcion)
				$scope.borrar_imagen.push($scope.slider.id_imagen);
				$scope.activo_img = "activo"
				$scope.slider.imagen = $scope.slider.ruta
				$scope.titulo_registrar = "Modificar";
				$scope.subtitulo_pagina  = "Modificar slider";
			
				setTimeout(function(){
					$('#idioma > option[value="'+$scope.slider.id_idioma+'"]').prop('selected', true);
					$('#direccion_slide > option[value="'+$scope.slider.direccion+'"]').prop('selected', true);
					$('#orden > option[value="'+$scope.slider.orden+'"]').prop('selected', true);
					$scope.slider.inicial = $scope.slider.orden;

				},3000);
				
				//$("#idioma").attr("disabled");
				$("#idioma").prop('disabled', true);
			
				setTimeout(function(){
					$('#idioma > option[value="'+$scope.slider.id_idioma+'"]').prop('selected', true);
					$('#direccion_slide > option[value="'+$scope.slider.direccion+'"]').prop('selected', true);

				},300);
				//$("#idioma").attr("disabled");
				$("#idioma").prop('disabled', true);
				desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
			});
		}
		//---
		///////////////////////////////////////////////////
		$scope.consultar_idioma();
		$scope.consultar_galeria_img();

		$scope.id_slider = $("#id_slider").val();
			if($scope.id_slider){
				$scope.consultarSliderIndividual();
			}else{
				$("#idioma").removeAttr("disabled");
			}
			//---------------------------------------------------------
	});