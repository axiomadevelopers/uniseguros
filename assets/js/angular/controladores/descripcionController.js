angular.module("ContentManagerApp")
	.controller("descripcionController", function($scope,$http,$location,serverDataMensajes,galeriaFactory,sesionFactory,idiomaFactory,descripcionFactory){
		//Cuerpo declaraciones
		$(".li-menu").removeClass("active");
        $("#li_configuracion").addClass("active");	
        $(".a-menu").removeClass("active");
        $("#meta_description").addClass("active");		
        $scope.titulo_pagina = "Meta Descripción";
		$scope.subtitulo_pagina  = "Registrar descripción";
		$scope.descripcion = {
								'id':'',
								'descripcion':''
		}
		$scope.id = ""
		$scope.searchFooter = []
		$scope.titulo_registrar = "Registrar";
		$scope.base_url = $("#base_url").val();
		//--------------------------------------------------------
		//Cuerpo de metodos

		$scope.registrarDescripcion = function(){

			uploader_reg("#div_mensaje","#btn-limpiar,#btn-registrar");

			if($scope.validar_form()==true){
				//Para guardar
				if(($scope.descripcion.id!="")&&($scope.descripcion.id!=undefined)){
					$scope.modificar_descripcion();
				}else{
					$scope.insertar_descripcion();
				}

			}
			
			//desbloquear_pantalla("#div_mensaje","#btn-registrar");
		}

		$scope.insertar_descripcion = function(){
			$http.post($scope.base_url+"/descripcion/registrarDescripcion",
			{
				'descripcion': $scope.descripcion.descripcion,
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "registro_procesado"){
					$scope.descripcion.id = $scope.mensajes.id
					mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
					$scope.limpiar_cajas_descripcion();
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}

		$scope.modificar_descripcion = function(){
			$http.post($scope.base_url+"/descripcion/modificarDescripcion",
			{
				'id'	     : $scope.descripcion.id,
				'descripcion': $scope.descripcion.descripcion
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "modificacion_procesada"){
					mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
				}else if($scope.mensajes.mensaje == "no_existe"){
					mostrar_notificacion("Mensaje","No existe el registro","warning");
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}

		$scope.validar_form = function(){
			if($scope.descripcion.descripcion==""){
				mostrar_notificacion("Campos no validos","Debe ingresar la descripción","warning");
				return false;
			}
			else{
				return true;
			}
		}

		$scope.limpiar_cajas_descripcion = function(){
			$scope.footer = {
								'id':'',
								'id_idioma':'',
								'correo':'',
								'descripcion':'',
								'estatus':'',
			}

			$('#text_editor').data("wysihtml5").editor.clear();
			$("#div_descripcion").html($scope.titulo_text);
		}
		//--
		$scope.consultarDescripcion = function(){

			uploader_reg("#div_mensaje","#btn-registrar");

			descripcionFactory.asignar_valores("",$scope.base_url)

			descripcionFactory.cargar_descripcion(function(data){
				if(data[0]){
					$scope.descripcion=data[0];
					$scope.titulo_registrar = "Modificar";
					$scope.subtitulo_pagina  = "Modificar descripción";
				}
			});
			desbloquear_pantalla("#div_mensaje","#btn-registrar");
		}
		//---
		//--------------------------------------------------------
		//Cuerpo de llamados
		$scope.consultarDescripcion()
		//--------------------------------------------------------
	});