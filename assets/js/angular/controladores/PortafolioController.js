angular.module("ContentManagerApp")
	.controller("PortafolioController", function($scope,$http,$location,serverDataMensajes,sesionFactory,idiomaFactory,serviciosFactory,galeriaFactory,portafolioFactory){
		$(".li-menu").removeClass("active");
		$("#li_productos").addClass("active");
		$(".a-menu").removeClass("active");
		$("#detalle_prod").addClass("active");

		$scope.titulo_pagina = "Portafolio";
		$scope.subtitulo_pagina = "Registro";
		$scope.titulo_registrar = "Registrar";
		$scope.titulo_cons = "Consultar";
		$scope.base_url = $("#base_url").val();
		$scope.titulo_text = "Pulse aquí para ingresas el contenido"

		$scope.currentTab = 'datos_basicos'
		$scope.activo_img = "inactivo";
		$scope.inhabilitarImg = false

		$scope.portafolio = {
						'id': '',
						'idioma': '',
						'id_idioma' : '',
						'descripcion':'',
						'servicios': {
							"id":"",
							"titulo":"",
							"descripcion":""
						},
						'id_servicios':'',
						'fecha': '',
						'url' : '',
						'codigo' : '',
						'id_imagen': '',
						'cliente':''
		}

		$scope.borrar_imagen = []
		$scope.galeria_portafolio = []
		$scope.imagenes_portafolio = []
		$scope.activo_img_soportes = "inactivo"
		$scope.path_imagen_seleccionada = []
		$scope.galery = []
		SuperFecha("fechaPortafolio")
		///////////////////////////////////////////////////////////////
		$scope.consultar_idioma = function(){
			idiomaFactory.asignar_valores("",$scope.base_url)
			idiomaFactory.cargar_idioma(function(data){
				$scope.idioma=data;
				$.each( $scope.idioma, function( indice, elemento ){
				  	agregarOptions("#idioma", elemento.id, elemento.descripcion)
				});
				$('#idioma > option[value="0"]').prop('selected', true);

			});
		}
		$scope.capturar_idioma = function(){
			//$scope.portafolio.id_idioma = $('#idioma').val();

		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.consultar_servicios = function(){

			eliminarOptions("servicios")

			$scope.portafolio.id_idioma = $('#idioma').val();
			serviciosFactory.asignar_valores("","",$scope.portafolio.id_idioma,$scope.base_url);//ya que es galeria de imagenes
			serviciosFactory.cargar_servicios(function(data){
				$scope.servicios=data;
				//console.log($scope.servicios);
				
				$.each($scope.servicios, function( indice, elemento ){
				  	agregarOptions("#servicios", elemento.id, elemento.titulo)
				});
				$('#servicios > option[value=""]').prop('selected', true);
			});
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.agregarWisi = function(){
			$scope.portafolio.descripcion = $(".textarea_editor").val()
			$("#div_descripcion").html($scope.portafolio.descripcion)
			$("#cerrarModal").click();
		}
		$scope.wisi_modal = function(){
			$("#wisiModal").modal("show")
			$(".textarea_editor").data("wysihtml5").editor.setValue($scope.portafolio.descripcion)
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.consultar_galeria_img = function(){
			galeriaFactory.asignar_valores('8','',$scope.base_url);//id 8 es el perteneciente a portafolio
			galeriaFactory.cargar_galeria_fa(function(data){
				$scope.galeria_m=data;
				//console.log($scope.galeria_m);
			});
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.seleccione_img_portafolio = function(){
			if(($scope.clonar!=1)&&($scope.inhabilitarImg==false)){
				//-----------------------------------
				$("#modal_img2").modal("show");
				var arreglo_portafolio = $scope.galeria_portafolio;
				//console.log( $scope.galeria_portafolio)
				var galeria_soporte = $scope.galeria_m
				var galeryx = new Array();
				for (i=0;i<galeria_soporte.length;i++){
					galeryx[i] = galeria_soporte[i]["ruta"];
				}
				//console.log(arreglo_portafolio)
				//Marco c/u
				for(j=0;j<arreglo_portafolio.length;j++){
					posicion = galeryx.indexOf(arreglo_portafolio[j]);
					//alert(arreglo_portafolio[j]);
					//alert(posicion);
					$scope.seleccionar_imagen_soportes_individual("img_soporte"+posicion);
				}
				//-----------------------------------
			}
		}

		$scope.seleccionar_imagen_soportes_individual = function(imagen){
			/*
			*	INicializa tags de menu
			*/
			$(".li-menu").removeClass("active");
			$("#li_productos").addClass("active");
			$(".a-menu").removeClass("active");
			$("#det_prod").addClass("active");
			/*
			*
			*/
			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

			var vec = $("#"+imagen).attr("data");
			var vector_data = vec.split("|")
			var id_imagen = vector_data[0];
			var ruta = vector_data[1];
			//--
			$("#"+imagen).addClass("marcado");
			if($scope.imagenes_portafolio.indexOf(id_imagen)==-1){
				$scope.imagenes_portafolio.push(id_imagen);//manejo los id
			}
			if($scope.galeria_portafolio.indexOf(ruta)==-1){
				$scope.galeria_portafolio.push(ruta); //maneja las rutas
			}
			$scope.activo_img_soportes = "activo"
			desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

		}

		$scope.limpiar_arreglos = function(){
			$scope.imagenes_portafolio = []
			$scope.galeria_portafolio = []
			$(".imgbiblioteca").removeClass("marcado");
			//console.log($scope.galeria_portafolio)
			//console.log($scope.imagenes_portafolio)
		}

		$scope.seleccionar_imagen_soportes = function(event){
			var imagen = event.target.id;//Para capturar id
			var vec = $("#"+imagen).attr("data");
			var vector_data = vec.split("|")
			var id_imagen = vector_data[0];
			var ruta = vector_data[1];
			//console.log(ruta);
			if(($("#"+imagen).hasClass("marcado"))==true){
				$("#"+imagen).removeClass("marcado");
				$indice = $scope.imagenes_portafolio.indexOf(id_imagen);
				$scope.imagenes_portafolio.splice($indice,1);
				$indice_ruta = $scope.galeria_portafolio.indexOf(ruta);
				$scope.galeria_portafolio.splice($indice_ruta,1);
				if($scope.galeria_portafolio.length==0){
					$scope.activo_img_soportes = "inactivo"
				}
			}else{
				$("#"+imagen).addClass("marcado");
				$scope.imagenes_portafolio.push(id_imagen);//manejo los id
				$scope.galeria_portafolio.push(ruta); //maneja las rutas
				$scope.activo_img_soportes = "activo"
			}
			//console.log($scope.galeria_portafolio)
			//console.log($scope.imagenes_portafolio)
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.registrarPortafolio = function(){	//Para guardar y modificar

			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

			//servicios:
			if(($scope.portafolio.id!="")&&($scope.portafolio.id!=undefined)){
				$scope.portafolio_servicios_id = $("#servicios").val();
			}else{
				$scope.portafolio_servicios_id = $scope.portafolio.servicios;
			}
			//--	
			if($scope.validar_form()==true){
				if(($scope.portafolio.id!="")&&($scope.portafolio.id!=undefined)){
					$scope.modificar_portafolio();
				}else{
					$scope.insertar_portafolio();
				}
			}

			//desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.validar_form = function(){
			if(($scope.portafolio.id_idioma=="")||($scope.portafolio.id_idioma=="")){
				mostrar_notificacion("Campos no validos","Debe seleccionar el idioma","warning");
				return false;
			}else if(($scope.portafolio_servicios_id=="")||($scope.portafolio_servicios_id==undefined)){
				mostrar_notificacion("Campos no validos","Debe seleccionar un tipo de servicio","warning");
				return false;
			}else if(($scope.portafolio.fecha=="")||($scope.portafolio.fecha=="dd/mm/yyyy")){
				mostrar_notificacion("Campos no validos","Debe ingresar la fecha del producto","warning");
				return false;
			}else if($scope.portafolio.codigo==""){
				mostrar_notificacion("Campos no validos","Debe ingresar el código","warning");
				return false;
			}else if(($scope.portafolio.titulo=="")||($scope.portafolio.titulo==undefined)){
				mostrar_notificacion("Campos no validos","Debe ingresar el título","warning");
				return false;
			}else if($scope.portafolio.url==""){
				mostrar_notificacion("Campos no validos","Debe ingresar la url","warning");
				return false;
			}else if(($scope.portafolio.cliente=="")||($scope.portafolio.cliente==undefined)){
				mostrar_notificacion("Campos no validos","Debe ingresar el nombre del cliente","warning");
				return false;
			}else if(($scope.portafolio.descripcion=="NULL")||($scope.portafolio.descripcion=="")){
				mostrar_notificacion("Campos no validos","Debe ingresar una descripción","warning");
				return false;
			}else if($scope.imagenes_portafolio.length==0){
				mostrar_notificacion("Campos no validos","Debe ingresar al menos una imagen","warning");
				return false;
			}else{
				return true;
			}
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.limpiar_cajas_portafolio = function(){
			
				//--Limpio solo algunos campos
				$scope.portafolio = {
					'id': '',
					'idioma': '',
					'id_idioma' : '',
					'descripcion':'',
					'servicios': {
						"id":"",
						"titulo":"",
						"descripcion":""
					},
					'fecha': '',
					'url' : '',
					'codigo' : '',
					'id_imagen': '',
					'cliente':'',
				}

				//console.log($scope.portafolio)
				$scope.limpiar_arreglos();
				$scope.activo_img_soportes = "inactivo"
			
			
			$("#div_descripcion").html($scope.titulo_text);
			$('#textarea_editor').data("wysihtml5").editor.clear();
			$("#idioma > option[value='']").removeAttr('selected', 'selected');
			$("#idioma > option[value='1']").attr('selected', 'selected');
			$scope.portafolio.id_idioma.id = 1
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.insertar_portafolio = function(){
			$http.post($scope.base_url+"/Portafolio/registrarportafolio",
			{
				'id' 	     		: $scope.portafolio.id,
				'id_idioma'  		: $scope.portafolio.id_idioma,
				'titulo'  			: $scope.portafolio.titulo,
				'servicio' 			: $scope.portafolio.servicios,
				'fecha'     		: $scope.portafolio.fecha,
				'url'     			: $scope.portafolio.url,
				'codigo' 			: $scope.portafolio.codigo,
				'descripcion'		: $scope.portafolio.descripcion,
				'id_imagen' 		: $scope.imagenes_portafolio,
				'cliente'			: $scope.portafolio.cliente,
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "registro_procesado"){
					mostrar_notificacion("Mensaje","El registro fue realizado de manera exitosa","info");
					if($scope.clonar==0)
						$scope.limpiar_cajas_portafolio();
					else
						$("#btn-registrar,#btn-limpiar").prop("disabled",true)
				}else if($scope.mensajes.mensaje == "titulo"){
					mostrar_notificacion("Mensaje","Ya existe una producto con ese título","warning");
				}else if($scope.mensajes.mensaje == "codigo"){
					mostrar_notificacion("Mensaje","Ya existe una producto con ese código","warning");
				}else if($scope.mensajes.mensaje == "ambos"){
					mostrar_notificacion("Mensaje","Ya existe una producto con ese título y código","warning");
				}
				else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.consultaPortafolioIndividual = function(){
			uploader_reg("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
			portafolioFactory.asignar_valores("",$scope.id_portafolio,$scope.base_url)
			portafolioFactory.cargar_portafolio(function(data){
				
				$scope.portafolio=data[0];
				//console.log($scope.portafolio)
				$("#div_descripcion").html($scope.portafolio.descripcion)

				$scope.activo_img_soportes = "activo"

				arreglo_marca = $scope.portafolio.imagen[0]['ruta']
				//-Cambio 10022020
				//------------------------------------------------------					
				if($scope.portafolio.imagen[0]['ruta']!=""){
					$scope.galeria_portafolio  = arreglo_marca.split("|")
				}else{
					$scope.galeria_portafolio=[];
				}
				//------------------------------------------------------	
				arreglo_id = $scope.portafolio.imagen[0]['id_imagen']
				//-------------------------------------------------------
				//-Cambio 10022020
				if((arreglo_id!="")&&(arreglo_id!="0")){
					$scope.imagenes_portafolio  = arreglo_id.split("|")
					$scope.galeria_portafolio.length>0 ? $scope.activo_img_soportes = "activo" : $scope.activo_img_soportes = "inactivo"
				}else{
					$scope.imagenes_portafolio = []
					$scope.activo_img_soportes = "inactivo"
				}
				//-----------------------------------------------------
				console.log($scope.imagenes_portafolio);
				/*console.log(arreglo_marca);*/
				$("#codigo").prop('disabled', true);
				$("#idioma").prop('disabled', true);
				$scope.titulo_registrar = "Modificar";
				$scope.subtitulo_pagina  = "Modificar portafolio";
				//------------------------------------------------------------------
					//--Select de idioma
					if($scope.portafolio.id_idioma){
						setTimeout(function(){
							$('#idioma').val($scope.portafolio.id_idioma);
							$('#idioma > option[value="'+$scope.portafolio.id_idioma+'"]').attr('selected', 'selected');
						},2000);
					}
					//--Select de categoria----
					if($scope.portafolio.id_servicios){
						eliminarOptions("servicios")

						serviciosFactory.asignar_valores("","",$scope.portafolio.id_idioma,$scope.base_url);//ya que es galeria de imagenes
						serviciosFactory.cargar_servicios(function(data){
							$scope.servicios=data;
							//console.log($scope.servicios);

							$.each($scope.servicios, function( indice, elemento ){
							  	agregarOptions("#servicios", elemento.id, elemento.titulo)
							});
						})	
						setTimeout(function(){
							$('#servicios').val($scope.portafolio.id_servicios);
							$('#servicios > option[value="'+$scope.portafolio.id_servicios+'"]').attr('selected', 'selected');
							desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");

						},2000);
					}
					//-------------------------
			});
		}
		/*
		*	DeshabilitarCampos
		*/
		$scope.deshabilitarCampos = function(){
			$("#servicios").prop('disabled', true);
			$("#fecha").prop('disabled', true);
			$("#url").prop('disabled', true);
			$("#codigo").prop('disabled', true);
			$("#img_galeria").prop('disabled',true);
		}
		/*
		*	Habilitar campos
		*/
		$scope.habilitarCampos = function(){
			$("#servicios").prop('disabled', true);
			$("#fecha").prop('disabled', true);
			$("#url").prop('disabled', true);
			$("#codigo").prop('disabled', true);
			$("#img_galeria").prop('disabled',true);
		}
		
		/////////////////////////////////////////////////////////////////////valida//////////////////////////////////

		$scope.modificar_portafolio = function(){
			$http.post($scope.base_url+"/Portafolio/modificar_portafolio",
			{
				'id' 	     		: $scope.portafolio.id,
				'id_idioma'  		: $scope.portafolio.id_idioma,
				'titulo'  			: $scope.portafolio.titulo,
				'servicio' 			: $scope.portafolio_servicios_id,
				'fecha'     		: $scope.portafolio.fecha,
				'url'     			: $scope.portafolio.url,
				'codigo' 			: $scope.portafolio.codigo,
				'descripcion'		: $scope.portafolio.descripcion,
				'id_imagen' 		: $scope.imagenes_portafolio,
				'cliente'			: $scope.portafolio.cliente
			}).success(function(data, estatus, headers, config){
				$scope.mensajes  = data;
				if($scope.mensajes.mensaje == "modificacion_procesada"){
					mostrar_notificacion("Mensaje","El registro fue actualizado de manera exitosa","info");
					//$scope.limpiar_cajas_portafolio();
				}else if($scope.mensajes.mensaje == "existe"){
					mostrar_notificacion("Mensaje","Existe un producto con ese código ","warning");
				}else{
					mostrar_notificacion("Mensaje","Ocurrío un error inesperado","warning");
				}
			}).error(function(data,estatus){
				console.log(data);
			});
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.quitarDisabled = function(){
			$("#btn-registrar").prop("disabled",false)
			$("#btn-limpiar").prop("disabled",false)
			$("#servicios").prop('disabled', true);
			$("#fecha").prop('disabled', true);
			$("#url").prop('disabled', true);
			$("#codigo").prop('disabled', true);
			$("#img_galeria").prop('disabled',true);
			$("#img_galeria").prop('disabled',false);
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$scope.consultar_idioma();
		$scope.consultar_galeria_img();
		/* $scope.consultar_talla(); */
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		$('#servicios > option[value=""]').attr('selected', 'selected');
		//---
		$scope.id_portafolio  = $("#id_portafolio").val();
		if($scope.id_portafolio){
			$scope.consultaPortafolioIndividual();
			setTimeout(function(){
				desbloquear_pantalla("#div_mensaje","#btn-nuevo,#btn-limpiar,#btn-registrar,#btn-consultar");
			},2000)
		}

		//---------------------------------------------------------------
		/*if(($scope.portafolio.id=="")||($scope.portafolio.id==undefined)){
			$scope.quitarDisabled();
		}*/
		//---------------------------------------------------------------	
	})
