angular.module("ContentManagerApp")
//---------------------------------------------------------------------------------------
//--Bloque de servicios
//--Servicio para cargar imagenes...
.service('upload', ["$http", "$q", function ($http, $q)
{
	this.uploadFile = function(file, categoria,nombre_archivo,base_url)
	{
		var deferred = $q.defer();
		var formData = new FormData();
		formData.append("categoria", categoria);
		formData.append("file", file);
		formData.append("nombre_archivo",nombre_archivo);
		return $http.post(base_url+"GaleriaMultimedia/upload", formData, {
			headers: {
				"Content-type": undefined
			},
			transformRequest: angular.identity
		})
		.success(function(res)
		{
			deferred.resolve(res);
		})
		.error(function(msg, code)
		{
			deferred.reject(msg);
		})
		return deferred.promise;
	},
	this.uploadFileFolleto = function(file, id_negocio,nombre_archivo)
	{
		var deferred = $q.defer();
		var formData = new FormData();

		formData.append("id_negocio", id_negocio);
		formData.append("file", file);
		/*formData.append("categoria", categoria);
		formData.append("file", file);*/
		formData.append("nombre_archivo",nombre_archivo);
		return $http.post("./controladores/archivosController.php", formData, {
			headers: {
				"Content-type": undefined
			},
			transformRequest: angular.identity
		})
		.success(function(res)
		{
			deferred.resolve(res);
		})
		.error(function(msg, code)
		{
			deferred.reject(msg);
		})
		return deferred.promise;
	},
	this.uploadFileBlog = function(file, id_blog,nombre_archivo)
	{
		var deferred = $q.defer();
		var formData = new FormData();

		formData.append("id_blog", id_blog);
		formData.append("file", file);
		/*formData.append("categoria", categoria);
		formData.append("file", file);*/
		formData.append("nombre_archivo",nombre_archivo);
		return $http.post("./controladores/archivosBlogController.php", formData, {
			headers: {
				"Content-type": undefined
			},
			transformRequest: angular.identity
		})
		.success(function(res)
		{
			deferred.resolve(res);
		})
		.error(function(msg, code)
		{
			deferred.reject(msg);
		})
		return deferred.promise;
	}
	this.uploadFilePDF = function(file, categoria,descripcion,titulo,base_url)
	{
		var deferred = $q.defer();
		var formData = new FormData();
		formData.append("categoria", categoria);
		formData.append("file", file);
		formData.append("descripcion",descripcion);
		formData.append("titulo",titulo);
		return $http.post(base_url+"CargarPdf/upload", formData, {
			headers: {
				"Content-type": undefined
			},
			transformRequest: angular.identity
		})
		.success(function(res)
		{
			deferred.resolve(res);
		})
		.error(function(msg, code)
		{
			deferred.reject(msg);
		})
		return deferred.promise;
	}
}])
.factory("categoriasFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,id,url){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id!="")
					id_categoria = id;
				else
					id_categoria = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_categorias : function(callback){
				$http.post(base_url+"/Categorias/consultarCategoriasTodas", { id_idioma:id_idioma,id_categoria:id_categoria}).success(callback);
			}
	}
}])

.factory("nosotrosFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,id,url){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id!="")
					id_nosotros = id;
				else
					id_nosotros = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_nosotros : function(callback){
				$http.post(base_url+"/QuienesSomos/consultarNosotrosTodas", { id_idioma:id_idioma,id_nosotros:id_nosotros}).success(callback);
			}
	}
}])
//--Servicio para consulta de noticias
.factory("noticiasFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,id,url){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id!="")
					id_noticias = id;
				else
					id_noticias = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_noticias : function(callback){
				$http.post(base_url+"/Noticias/consultarNoticiasTodas", { id_idioma:id_idioma,id_noticias:id_noticias}).success(callback);
			}

	}
}])
//--
//--Servicio para consulta de footer
.factory("footerFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,id,url){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id!="")
					id_footer = id;
				else
					id_footer = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_footer : function(callback){
				$http.post(base_url+"/Footer/consultarFooterTodas", { id_idioma:id_idioma,id_footer:id_footer}).success(callback);
			},
			
	}
}])

//-- Servicio para la consulta de descripcion
.factory("descripcionFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,url){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},	
			cargar_descripcion : function(callback){
				$http.post(base_url+"/Descripcion/consultarDescripcion", { id_idioma:id_idioma}).success(callback);
			}
	}
}])
//-- Servicio para la consulta de palabras claves
.factory("palabrasClavesFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,url){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},	
			cargar_descripcion : function(callback){
				$http.post(base_url+"/Palabras_claves/consultarDescripcion", { id_idioma:id_idioma}).success(callback);
			}
	}
}])


//--Servicio para consulta de noticias
.factory("sliderFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,id,url){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id!="")
					id_slider = id;
				else
					id_slider = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_slider : function(callback){
				$http.post(base_url+"slider/consultarSliderTodas", { id_idioma:id_idioma,id_slider:id_slider}).success(callback);
			}
	}
}])
.factory("directivaFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,id,url){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id!="")
					id_directiva = id;
				else
					id_directiva = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_directiva : function(callback){
				$http.post(base_url+"directiva/consultarDirectivaTodas", { id_idioma:id_idioma,id_directiva:id_directiva}).success(callback);
			}
	}
}])
.factory("reaseguradorasFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,id,url){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id!="")
					id_reaseguradoras = id;
				else
					id_reaseguradoras = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_reaseguradoras : function(callback){
				$http.post(base_url+"reaseguradoras/consultarReaseguradorasTodas", { id_idioma:id_idioma,id_reaseguradoras:id_reaseguradoras}).success(callback);
			}
	}
}])

//--Servicio para compartir datos de mensajes iniciales
.service('serverDataMensajes',[function(){
	var puente = [];
	this.puenteData = function(arreglo){
		puente = arreglo;
		return puente;
	}
	this.obtener_arreglo = function(){
		return puente;
	}
	this.limpiar_arreglo_servicio = function (){
		puente = [];
		return puente;
	}
}])
//--Bloque de factorias
//Factory para verificar inicio de sesion
.factory("sesionFactory",['$http', function($http){
	return{
			datos_sesion : function(callback){
				$http.post("./controladores/fbasicController.php", { accion:'datos_sesion'}).success(callback);
			},
			sesion_usuario : function(callback){
				$http.post("./controladores/fbasicController.php", { accion:'consultar_sesion'}).success(callback);
			},
			cerrar_sesion: function(callback){
				$http.post("./controladores/fbasicController.php", { accion:'cerrar_sesion'}).success(callback);
			}
	}
}])

//Para consultar idiomas
/*.factory("idiomaFactory",['$http', function($http){
	return{
			cargar_idioma : function(callback){
				$http.post("./controladores/quienesSomosController.php", { accion:'consultar_idioma'}).success(callback);
			}
	}
}])*/

.factory("idiomaFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,url){
				//console.log(base_url);
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_idioma : function(callback){
				$http.post(base_url+"QuienesSomos/consultar_idioma", { id_idioma:id_idioma,}).success(callback);
			}
	}
}])
//Para el orden del slider
.factory("ordenFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,url,opcion){
				//console.log(base_url);
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(base_url!="")
					base_url = url;
				else
					base_url = "";
				if(opcion!="")
					tipo = opcion
				else
					tipo = "";
			},
			cargar_orden : function(callback){
				$http.post(base_url+"Slider/consultar_orden", { id_idioma:id_idioma, tipo:tipo}).success(callback);
			},
			cargar_orden_nosotros : function(callback){
				$http.post(base_url+"QuienesSomos/consultar_orden", { id_idioma:id_idioma, tipo:tipo}).success(callback);
			},
			cargar_orden_servicios : function(callback){
				$http.post(base_url+"Servicios/consultar_orden", { id_idioma:id_idioma, tipo:tipo}).success(callback);
			},
			cargar_orden_directiva : function(callback){
				$http.post(base_url+"Directiva/consultar_orden", { id_idioma:id_idioma, tipo:tipo}).success(callback);
			},
			cargar_orden_reaseguradoras : function(callback){
				$http.post(base_url+"Reaseguradoras/consultar_orden", { id_idioma:id_idioma, tipo:tipo}).success(callback);
			},
			cargar_orden_direccion : function(callback){
				$http.post(base_url+"Direccion/consultar_orden", { id_idioma:id_idioma, tipo:tipo}).success(callback);
			},
			cargar_orden_productos : function(callback){
				$http.post(base_url+"Productos/consultar_orden", { id_idioma:id_idioma, tipo:tipo}).success(callback);
			},
			cargar_orden_tproductos : function(callback){
				$http.post(base_url+"Tproductos/consultar_orden", { id_idioma:id_idioma, tipo:tipo}).success(callback);
			},
			cargar_orden_detalle_productos : function(callback){
				$http.post(base_url+"DetalleTproductos/consultar_orden", { id_idioma:id_idioma, tipo:tipo}).success(callback);
			},
			cargar_orden_servicios : function(callback){
				$http.post(base_url+"Servicios/consultar_orden", { id_idioma:id_idioma, tipo:tipo}).success(callback);
			},
			cargar_orden_tservicios : function(callback){
				$http.post(base_url+"Tservicios/consultar_orden", { id_idioma:id_idioma, tipo:tipo}).success(callback);
			},
			cargar_orden_detalle_servicios : function(callback){
				$http.post(base_url+"DetalleTservicios/consultar_orden", { id_idioma:id_idioma, tipo:tipo}).success(callback);
			},
			
	}
}])
//Para el orden del slider
.factory("ordenProductosFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,url,opcion,producto,tipoProducto){
				//console.log(base_url);
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(base_url!="")
					base_url = url;
				else
					base_url = "";
				if(opcion!="")
					tipo = opcion
				else
					tipo = "";
				if(producto!="")
					id_productos = producto
				else
					id_productos = "";
				if(tipoProducto!="")
					id_tipo_producto = tipoProducto
				else
					id_tipo_producto = "";
			},
			cargar_orden_tproductos : function(callback){
				$http.post(base_url+"Tproductos/consultar_orden", { id_idioma:id_idioma, tipo:tipo, id_productos:id_productos}).success(callback);
			},
			cargar_orden_detalle_productos : function(callback){
				$http.post(base_url+"DetalleTproductos/consultar_orden", { id_idioma:id_idioma, tipo:tipo, id_productos:id_productos, id_tipo_producto:id_tipo_producto}).success(callback);
			},
			cargar_orden_tservicios : function(callback){
				$http.post(base_url+"Tservicios/consultar_orden", { id_idioma:id_idioma, tipo:tipo, id_productos:id_productos}).success(callback);
			},
			cargar_orden_detalle_servicios : function(callback){
				$http.post(base_url+"DetalleTservicios/consultar_orden",  { id_idioma:id_idioma, tipo:tipo, id_productos:id_productos, id_tipo_producto:id_tipo_producto}).success(callback);
			},
			
	}
}])

//Para la galeria
.factory("galeriaFactory",['$http', function($http){

	return{
			asignar_valores : function (id,nombre,url){
				if(id!="")
					categoria = id;
				else
					categoria = "";

				if(nombre!=""){
					titulo_imagen = nombre;
				}
				else{
					titulo_imagen = "";
				}
				if(url !=""){
					base_url = url;
				}else{
					base_url = "";
				}
			},
			cargar_galeria_fa : function(callback){
				$http.post(base_url+"/GaleriaMultimedia/consultarGaleriaCategoria",{ categoria:categoria, nombre:titulo_imagen}).success(callback);
			},
	}
}])
//Para las citas
.factory("citasProgramadasFactory",['$http', function($http){

	return{
			valor_estatus : function (estatus_op, especialidad, doctor){
				if(estatus_op!="")
					estatus = estatus_op;
				else
					estatus = "";
				if(especialidad!="")
					id_especialidad = especialidad;
				else
					id_especialidad = "";
				if(doctor!="")
					id_doctor = doctor
				else
					id_doctor = doctor
			},
			cargar_citas_programadas : function(callback){
				$http.post("./controladores/citasController.php",{ accion:'consultar_citas_programadas', estatus:estatus, id_especialidad:id_especialidad, id_doctor:id_doctor }).success(callback);
			}
	}
}])
.factory("paisesFactory",['$http', function($http){
	return{
		asignar_valores : function (idioma){
			if(idioma!="")
				id_idioma = idioma;
			else
				id_idioma = "";
		},
		cargar_paises : function(callback){
			$http.post("./controladores/paisesController.php", {accion:'consultar_paises',id_idioma:id_idioma}).success(callback);
		}
	}
}])
.factory("estadosFactory",['$http', function($http){
	return{
		asignar_valores : function (pais){
			if(pais!="")
				id_pais = pais;
			else
				id_pais = "";
		},
		cargar_estados : function(callback){
			$http.post("./controladores/paisesController.php", {accion:'consultar_estados',id_pais:id_pais}).success(callback);
		}
	}
}])
.factory("ciudadFactory",['$http', function($http){
	return{
		asignar_valores : function (estado){
			if(estado!="")
				id_estado = estado;
			else
				id_estado = "";
		},
		cargar_ciudades : function(callback){
			$http.post("./controladores/paisesController.php", {accion:'consultar_ciudades',id_estado:id_estado}).success(callback);
		}
	}
}])
/*.factory("tiposNegociosFactory",['$http', function($http){
	return{
		valor_estatus : function (estatus_op){
			if(estatus_op!="")
				estatus = estatus_op;
			else
				estatus = "";
		},
		cargar_tiposNegocios : function(callback){
			$http.post("./controladores/tiposNegociosController.php", {accion:'consultar_tipos_negocios'}).success(callback);
		}
	}
}])*/

////
.factory("tiposNegociosFactory",['$http', function($http){
	return{
		asignar_valor : function (estatus_op,idioma){
			if(estatus_op!="")
				estatus = estatus_op;
			else
				estatus = "";
			if(idioma!="")
				id_idioma = idioma;
			else
				id_idioma = "";
		},
		cargar_tiposNegocios : function(callback){
			$http.post("./controladores/tiposNegociosController.php", {accion:'consultar_tipos_negocios',id_idioma:id_idioma,estatus:estatus}).success(callback);
		}
	}
}])

.factory("serviciosFactory",['$http', function($http){
	return{
		asignar_valores : function (id_servicios,estatus_op,id_idioma,url){
			if(estatus_op!="")
				estatus = estatus_op;
			else
				estatus = "";
			
			if(id_idioma!="")
				idioma = id_idioma;
			else
				idioma = "";
			
			if(url!="")
				base_url = url;
			else
				base_url = "";

			if(id_servicios!="")
				id = id_servicios;
			else
				id = "";
		},
		cargar_servicios : function(callback){
			$http.post(base_url+"Servicios/consultarServiciosTodas", {id:id,idioma:idioma,estatus:estatus}).success(callback);
		}
	}
}])
.factory("tiposNegociosOrdenFactory",['$http', function($http){
	return{
		asignar_valor : function (estatus_op,idioma){
			if(estatus_op!="")
				estatus = estatus_op;
			else
				estatus = "";
			if(idioma!="")
				id_idioma = idioma;
			else
				id_idioma = "";
		},
		cargar_tiposNegocios : function(callback){
			$http.post("./controladores/tiposNegociosController.php", {accion:'consultar_tipos_negocios_orden',id_idioma:id_idioma,estatus:estatus}).success(callback);
		}
	}
}])
.factory("negociosOrdenFactory",['$http', function($http){
	return{
		asignar_valores : function (idioma,tipos_negocios){
			if(idioma!="")
				id_idioma = idioma;
			else
				id_idioma = "";

			if(tipos_negocios!="")
				id_tipos_negocios = tipos_negocios;
			else
				id_tipos_negocios = "";
		},
		cargar_detallesNegocios : function(callback){
			$http.post("./controladores/detallesNegociosController.php", {accion:'consultar_tipos_negocios_orden','id_idioma':id_idioma,'tipo_negocio':id_tipos_negocios}).success(callback);
		}
	}
}])
.factory("tiposUsuariosFactory",['$http', function($http){
	return{
		cargar_usuarios : function(callback){
			$http.post("./controladores/usuariosController.php", {accion:'consultar_tipos_usuarios'}).success(callback);
		}
	}
}])
.factory("contactosFactory",['$http', function($http){
	return{
		asignar_valores : function (url){
			if(url!=""){
				base_url = url
			}else{
				base_url = ""
			}
		},
		cargar_contactos : function(callback){
			$http.post(base_url+"Contactos/consultar_contactos", {}).success(callback);
		},
		cargar_contactos_empleos : function(callback){
			$http.post(base_url+"Contactos/consultar_empleos", {}).success(callback);
		}
	}
}])
.factory("auditoriaFactory",['$http', function($http){
	return{
		asignar_valores : function (url){
			if(url!=""){
				base_url = url
			}else{
				base_url = ""
			}
		},
		cargar_auditoria : function(callback){
			$http.post(base_url+"RegistroCMS/consultar_auditoria", {}).success(callback);
		},
	}
}])

.factory("redesFactory",['$http', function($http){
	return{
		asignar_valores : function (idioma,valor_id_tipo_red,url){
			if(valor_id_tipo_red!=""){
				id_tipo_red= valor_id_tipo_red;
			}else{
				id_tipo_red = ""
			}
			if(url!=""){
				base_url = url
			}else{
				base_url = ""
			}
		},
		cargar_redes_url: function(callback){
			$http.post(base_url+"RedesSociales/consultar_redes_url", {'id_tipo_red':id_tipo_red}).success(callback);
		},
		cargar_redes : function(callback){
			$http.post(base_url+"RedesSociales/consultar_redes", {}).success(callback);
		}
	}
}])
.factory("direccionFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,id,url){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id!="")
					id_direccion = id;
				else
					id_direccion = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_direccion : function(callback){
				$http.post(base_url+"Direccion/consultarDireccionTodas", { id_idioma:id_idioma,id_direccion:id_direccion}).success(callback);
			}
	}
}])
.factory("marcasFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,id,url){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id!="")
					id_marca = id;
				else
					id_marca = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_marcas : function(callback){
				$http.post(base_url+"Marcas/consultarMarcasTodas", { id_idioma:id_idioma,id_marca:id_marca}).success(callback);
			}
	}
}])
//--- Carga los colores
.factory("coloresFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,id,url){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id!="")
					id_color = id;
				else
					id_color = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_colores : function(callback){
				$http.post(base_url+"Colores/consultarColoresTodos", { id_idioma:id_idioma,id_color:id_color}).success(callback);
			}
	}
}])
.factory("tallasFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,id,url){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id!="")
					id_talla = id;
				else
					id_talla = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_tallas: function(callback){
				$http.post(base_url+"Tallas/consultarTallasTodos", { id_idioma:id_idioma,id_talla:id_talla}).success(callback);
			}
	}
}])
.factory("categoriasProdFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,id,url){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id!="")
					id_categoria_prod = id;
				else
					id_categoria_prod = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_categorias_prod : function(callback){
				$http.post(base_url+"/categoria_prod/consultarCategoriaProdTodas", { id_idioma:id_idioma,id_categoria_prod:id_categoria_prod}).success(callback);
			}
	}
}])

.factory("detalle_form_ProdFactory",['$http', function($http){
	return{
		asignar_valor : function (estatus_op,idioma){
			if(estatus_op!="")
				estatus = estatus_op;
			else
				estatus = "";
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
		},
		cargar_detprod : function(callback){
			$http.post(base_url+"/tipo_prod/consultarDetalleProd", {id_idioma:id_idioma,estatus:estatus}).success(callback);
		}
	}
}])

.factory("marca_consFactory",['$http', function($http){
	return{
		asignar_valor : function (estatus_op,idioma){
			if(estatus_op!="")
				estatus = estatus_op;
			else
				estatus = "";
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
		},
		cargar_marca : function(callback){
			$http.post(base_url+"/Marcas/consultarmarca_idioma", {id_idioma:id_idioma,estatus:estatus}).success(callback);
		}
	}
}])
.factory("tipoProdFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,id,url){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id!="")
					id_tipo_prod = id;
				else
					id_tipo_prod = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_tipoProd : function(callback){
				$http.post(base_url+"/tipo_prod/consultarTipoProdTodas", { id_idioma:id_idioma,id_tipo_prod:id_tipo_prod}).success(callback);
			}
	}
}])

.factory("portafolioFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,id,url){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id!="")
					id_portafolio = id;
				else
					id_portafolio = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_portafolio : function(callback){
				$http.post(base_url+"/Portafolio/consultarPortafolioTodas", { id_idioma:id_idioma,id_portafolio:id_portafolio}).success(callback);
			}
	}
}])

.factory("tallaFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,url){
				//console.log(base_url);
				if(talla!="")
					id_talla = talla;
				else
					id_talla = "";
			},
			cargar_talla : function(callback){
				$http.post(base_url+"detalle_prod/consultar_talla", { id_talla:id_talla,}).success(callback);
			}
	}
}])

.factory("colorFactory",['$http', function($http){
	return{
		asignar_valor : function (estatus_op,idioma){
			if(estatus_op!="")
				estatus = estatus_op;
			else
				estatus = "";
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
		},
		cargar_color : function(callback){
			$http.post(base_url+"/detalle_prod/consultar_color", {id_idioma:id_idioma,estatus:estatus}).success(callback);
		}
	}
}])

.factory("tagsFactory",['$http', function($http){
	return{
		valor_id_idioma : function (idioma){
			if(idioma!="")
				id_idioma = idioma;
			else
				id_idioma = "";
		},
		cargar_tags : function(callback){
			$http.post("./controladores/tagsController.php", {accion:'consultar_tags2',id_idioma:id_idioma}).success(callback);
		}
	}
}])
.factory("categprodESFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,url){
				//console.log(base_url);
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_categprod : function(callback){
				$http.post(base_url+"Categoria_idiomas/consultar_categoria_producto_es", { id_idioma:id_idioma,}).success(callback);
			}
	}
}])
.factory("categprodENFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,url){
				//console.log(base_url);
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_categprod : function(callback){
				$http.post(base_url+"Categoria_idiomas/consultar_categoria_producto_en", { id_idioma:id_idioma,}).success(callback);
			}
	}
}])
.factory("coloresESfactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,url){
				//console.log(base_url);
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_color : function(callback){
				$http.post(base_url+"Categoria_idiomas/consultar_colores_es", { id_idioma:id_idioma,}).success(callback);
			}
	}
}])
.factory("coloresENfactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,url){
				//console.log(base_url);
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_color : function(callback){
				$http.post(base_url+"Categoria_idiomas/consultar_colores_en", { id_idioma:id_idioma,}).success(callback);
			}
	}
}])
.factory("marcasESfactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,url){
				//console.log(base_url);
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_marcas : function(callback){
				$http.post(base_url+"Categoria_idiomas/consultar_marcas_es", { id_idioma:id_idioma,}).success(callback);
			}
	}
}])
.factory("marcasENfactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,url){
				//console.log(base_url);
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_marcas : function(callback){
				$http.post(base_url+"Categoria_idiomas/consultar_marcas_en", { id_idioma:id_idioma,}).success(callback);
			}
	}
}])
.factory("tiposESfactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,url){
				//console.log(base_url);
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_tipos : function(callback){
				$http.post(base_url+"Categoria_idiomas/consultar_tipo_productos_es", { id_idioma:id_idioma,}).success(callback);
			}
	}
}])
.factory("tiposENfactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,url){
				//console.log(base_url);
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_tipos : function(callback){
				$http.post(base_url+"Categoria_idiomas/consultar_tipo_productos_en", { id_idioma:id_idioma,}).success(callback);
			}
	}
}])
.factory("categoriaidiomasFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,id,url){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id!="")
					id_color = id;
				else
					id_color = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_categprod : function(callback){
				$http.post(base_url+"Categoria_idiomas/consultar_CategoriaIdiomasTodos", { id_idioma:id_idioma,id_color:id_color}).success(callback);
			}
	}
}])
.factory("OrdenCompraFactory",['$http', function($http){
	return{
		asignar_valores : function (url){
			if(url!=""){
				base_url = url
			}else{
				base_url = ""
			}
		},
		cargar_ordenes : function(callback){
			$http.post(base_url+"OrdenCompra/consultar_orden_compra", {}).success(callback);
		},
		
		
	}
}])
.factory("ConsultaCarritoFactory",['$http', function($http){
	return{
		asignar_valores : function (url){
			if(url!=""){
				base_url = url
			}else{
				base_url = ""
			}
		},
		cargar_contactos : function(callback){
			$http.post(base_url+"ConsultaCarrito/consultar_carrito_compra", {}).success(callback);
		},
		
	}
}])
.factory("tipos_usuariosFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,url){
				//console.log(base_url);
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_tipos : function(callback){
				$http.post(base_url+"RegistroCMS/consultar_tipos_usuarios", { id_idioma:id_idioma,}).success(callback);
			}
	}
}])
.factory("consultaUsuarioFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,id,url){
				if(id!="")
					id_personas = id;
				else
					id_personas = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_personas : function(callback){
				$http.post(base_url+"RegistroCMS/consultarUsuariosTodos", { id_personas:id_personas}).success(callback);
			}
	}
}]).factory("existenteFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,id,color,talla,url){
				if(id!="")
					id_producto = id;
				else
					id_producto = "";
				if(color!="")
					id_color = color;
				else
					id_color = "";
				if(talla!="")
					id_talla = talla;
				else
					id_talla = "";
				if(url!="")
					base_url = url;
				else 
					base_url = "";
			},
			cargar_existentes : function(callback){
				$http.post(base_url+"AdministracionCantidad/consultaExistente", { id_producto:id_producto,id_color:id_color,id_talla:id_talla}).success(callback);
			}
	}
}]).factory("cantidadesFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,id,url){
				if(id!="")
					id_producto = id;
				else
					id_producto = "";
				
				if(url!="")
					base_url = url;
				else 
					base_url = "";
			},
			cargar_existentes : function(callback){
				$http.post(base_url+"AdministracionCantidad/consultaExistenteTodos", { id_producto:id_producto}).success(callback);
			}
	}
}]).factory("productosFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,id,url){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id!="")
					id_productos = id;
				else
					id_productos = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_productos : function(callback){
				$http.post(base_url+"/Productos/consultarProductosTodas", { id_idioma:id_idioma,id_productos:id_productos}).success(callback);
			}
	}
}]).factory("buscarproductosFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,url){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_productos : function(callback){
				$http.post(base_url+"/Tproductos/consultarProductos", { id_idioma:id_idioma}).success(callback);
			}
	}
}]).factory("tproductosFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,id,url){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id!="")
					id_productos = id;
				else
					id_productos = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_productos : function(callback){
				$http.post(base_url+"/Tproductos/consultarProductosTodas", { id_idioma:id_idioma,id_productos:id_productos}).success(callback);
			}
	}
}]).factory("buscartproductosFactory",['$http', function($http){
	return{
			asignar_valores : function (producto,url){
				if(producto!="")
					id_productos = producto;
				else
					id_productos = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_productos : function(callback){
				$http.post(base_url+"/DetalleTproductos/consultarTproductos", { id_productos:id_productos}).success(callback);
			}
	}
}]).factory("detalleproductosFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,id,url){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id!="")
					id_productos = id;
				else
					id_productos = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_productos : function(callback){
				$http.post(base_url+"/DetalleTproductos/consultarProductosTodas", { id_idioma:id_idioma,id_productos:id_productos}).success(callback);
			}
	}
}]).factory("serviciosFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,id,url){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id!="")
					id_servicios = id;
				else
					id_servicios = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_servicios : function(callback){
				$http.post(base_url+"/Servicios/consultarServiciosTodas", { id_idioma:id_idioma,id_servicios:id_servicios}).success(callback);
			}
	}
}]).factory("buscarserviciosFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,url){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_servicio  : function(callback){
				$http.post(base_url+"/Tservicios/consultarServicios", { id_idioma:id_idioma}).success(callback);
			}
	}
}]).factory("tserviciosFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,id,url){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id!="")
					id_servicios = id;
				else
					id_servicios = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_servicios : function(callback){
				$http.post(base_url+"/Tservicios/consultarServiciosTodas", { id_idioma:id_idioma,id_servicios:id_servicios}).success(callback);
			}
	}
}]).factory("buscartserviciosFactory",['$http', function($http){
	return{
			asignar_valores : function (servicio,url){
				if(servicio!="")
					id_servicios = servicio;
				else
					id_servicios = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_servicios : function(callback){
				$http.post(base_url+"/DetalleTservicios/consultarTservicios", { id_servicios:id_servicios}).success(callback);
			}
	}
}]).factory("detalleserviciosFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,id,url){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id!="")
					id_servicios = id;
				else
					id_servicios = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_servicios : function(callback){
				$http.post(base_url+"/DetalleTservicios/consultarServiciosTodas", { id_idioma:id_idioma,id_servicios:id_servicios}).success(callback);
			}
	}
}]).factory("bancosFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,id,url){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id!="")
					id_banco = id;
				else
					id_banco = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_banco : function(callback){
				$http.post(base_url+"/Banco/consultarBancoTodas", { id_idioma:id_idioma,id_banco:id_banco}).success(callback);
			},
			cargar_bancos_registrados : function(callback){
				$http.post(base_url+"/CuentaBancaria/consultarBancosRegistrados", { id_idioma:id_idioma,id_banco:id_banco}).success(callback);
			},
	}
}]).factory("cuentaBancariaFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,id,url){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id!="")
					id_cuenta = id;
				else
					id_cuenta = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_banco : function(callback){
				$http.post(base_url+"/CuentaBancaria/consultarCuentaTodas", { id_idioma:id_idioma,id_cuenta:id_cuenta}).success(callback);
			},
	}
}]).factory("pdfsFactory",['$http', function($http){
	return{
			asignar_valores : function (idioma,id,url){
				if(idioma!="")
					id_idioma = idioma;
				else
					id_idioma = "";
				if(id!="")
					id_pdf = id;
				else
					id_pdf = "";
				if(url!="")
					base_url = url;
				else
					base_url = "";
			},
			cargar_pdf : function(callback){
				$http.post(base_url+"/CargarPdf/consultarPdfTodas", { id_idioma:id_idioma,id_pdf:id_pdf}).success(callback);
			},
	}
}]);
