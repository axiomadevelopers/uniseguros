angular.module("UnisegurosApp")
	.controller("tiposServiciosController", function($scope,$http,$location,direccionFactory,redessocialesFactory,metaFactory,nosotrosFactory,directivaFactory,reaseguradorasFactory,tserviciosFactory,productosFactory){
		//--Cuerpo de declaraciones
		$scope.menu = []
		$scope.url = []
		$scope.base_url = $("#base_url").val();
		$("#span-menu3").addClass("subtitulo-menu2")
		$scope.slug_servicio  = $("#slug_producto").val();
		//console.log($scope.slug_servicio);
		$scope.consultar_servicio = function(){
			tserviciosFactory.asignar_valores("","",$scope.slug_servicio,$scope.base_url)
			tserviciosFactory.cargar_servicios(function(data){
				
				$scope.ruta_servicio=data.ruta_servicio;
				$scope.titulo=data.titulo;
				$scope.slug_base=data.slug;
				$scope.tservicios=data.tservicios;
				//console.log($scope.tservicios)

			});
		}
		$scope.consultar_algunos_productos = function(){
			productosFactory.asignar_valores("","",$scope.base_url)
			productosFactory.cargar_productos_servicios(function(data){
				
				$scope.algunos_productos=data;
				//console.log($scope.algunos_productos)

			});
		}
		$scope.consultar_algunos_productos();
		$scope.consultar_servicio();
	});