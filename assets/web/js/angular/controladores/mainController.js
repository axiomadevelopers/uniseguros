angular.module("UnisegurosApp")
	.controller("mainController", function($scope,$http,$location,direccionFactory,redessocialesFactory,metaFactory,footerFactory,productosFactory){
		//--Cuerpo de declaraciones
		$scope.menu = []
		$scope.url = []
		$scope.base_url = $("#base_url").val();

		$scope.consultar_redes = function () {
			footerFactory.asignar_valores("","",$scope.base_url);
			footerFactory.cargar_redes(function (data) {
				$scope.redes = data;
				//console.log($scope.redes);
			});
		}

		$scope.consultarFooter = function(){
			footerFactory.asignar_valores("","",$scope.base_url)
			footerFactory.cargar_footer(function(data){
				$scope.footer=data[0];
				//console.log(data[0]);
			});
		}

		$scope.consultar_meta = function () {
			footerFactory.cargar_meta_tag(function (data) {
				$scope.meta_descripcion = data.descripcion;
				$scope.meta_palabras_claves = data.palabras_claves;

				$("meta[name='keywords']").attr('content', $scope.meta_palabras_claves);
				$("meta[name='description']").attr('content', $scope.meta_descripcion);
				//console.log(data);
			});
		}
		$scope.consultar_productos = function(){
			productosFactory.asignar_valores("","",$scope.base_url)
			productosFactory.cargar_productos(function(data){
				$scope.productos_footer=data;
				console.log($scope.productos_footer)
			});
		}
		//--
		$scope.consultarFooter()
		$scope.consultar_redes()
		$scope.consultar_meta()
		$scope.consultar_productos()
		//--
	});